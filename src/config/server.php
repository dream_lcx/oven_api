<?php
/**
 * Created by PhpStorm.
 * User: zhangjincheng
 * Date: 16-7-14
 * Time: 下午1:58
 */

/**
 * 服务器设置
 */
$config['name'] = 'OVEN_SWD';
$config['server']['send_use_task_num'] = 500;
$config['server']['set'] = [
    'log_file' => LOG_DIR."/swoole.log",
    'pid_file' => PID_DIR . '/server.pid',
    'log_level' => 5,
    'reactor_num' => 2, //reactor thread num
    'worker_num' => 4,    //worker process num
    'backlog' => 128,   //listen backlog
    'open_tcp_nodelay' => 1,
    'dispatch_mode' => 2,
    'task_worker_num' => 20,
    'task_max_request' => 50,
    'enable_reuse_port' => true,
    'manual_testing_heartbeat'=> false,//是否手动检测心跳（定时任务）
    'heartbeat_idle_time' => 240,//4分钟后没消息自动释放连接
    'heartbeat_check_interval' => 200,//3分钟检测一次
    'max_connection' => 1000,  //swoole_table也使用该值，建议2的N次方65535
    'package_max_length'=>20*1024*1024//解决上传文件大小问题，默认2M,不建议过大
    //由nginx做代理所以不用swoole证书
    //'ssl_cert_file' => CONFIG_DIR.'/cert/1_share.cqthesis.cn_bundle.crt',
    //'ssl_key_file' => CONFIG_DIR.'/cert/2_share.cqthesis.cn.key',
];

//协程超时时间
$config['coroution']['timerOut'] = 20000; //单位为毫秒

//是否启用自动reload
$config['auto_reload_enable'] = false;

//是否允许访问Server中的Controller，如果不允许将禁止调用Server包中的Controller
$config['allow_ServerController'] = true;
//是否允许监控流量数据
$config['allow_MonitorFlowData'] = false;
return $config;
