<?php
/**
 * Created by PhpStorm.
 * User: zhangjincheng
 * Date: 16-7-14
 * Time: 下午1:58
 */
/**
 * timerTask定时任务
 * （选填）task名称 task_name
 * （选填）model名称 model_name  task或者model必须有一个优先匹配task
 * （必填）执行task的方法 method_name
 * （选填）执行开始时间 start_time,end_time) 格式： Y-m-d H:i:s 没有代表一直执行,一旦end_time设置后会进入1天一轮回的模式
 * （必填）执行间隔 interval_time 单位： 秒
 * （选填）最大执行次数 max_exec，默认不限次数
 * （选填）是否立即执行 delay，默认为false立即执行
 */
$config['timerTask'] = [];
//下面例子表示在每天的14点到20点间每隔1秒执行一次
/*$config['timerTask'][] = [
    //'start_time' => 'Y-m-d 19:00:00',
    //'end_time' => 'Y-m-d 20:00:00',
    'task_name' => 'TestTask',
    'method_name' => 'test',
    'interval_time' => '1',
];*/
//下面例子表示在每天的14点到15点间每隔1秒执行一次，一共执行5次
/*$config['timerTask'][] = [
    'start_time' => 'Y-m-d 14:00:00',
    'end_time' => 'Y-m-d 15:00:00',
    'task_name' => 'TestTask',
    'method_name' => 'test',
    'interval_time' => '1',
    'max_exec' => 5,
];*/
//下面例子表示在每天的0点执行1次(间隔86400秒为1天)
/*$config['timerTask'][] = [
    'start_time' => 'Y-m-d 23:59:59',
    'task_name' => 'TestTask',
    'method_name' => 'test',
    'interval_time' => '86400',
];*/
//下面例子表示在每天的0点执行1次
/*$config['timerTask'][] = [
    'start_time' => 'Y-m-d 14:53:10',
    'end_time' => 'Y-m-d 14:54:11',
    'task_name' => 'TestTask',
    'method_name' => 'test',
    'interval_time' => '1',
    'max_exec' => 1,
];*/

//检测主板是否在线
//$config['timerTask'][] = [
//    'task_name' => 'AppTask',
//    'method_name' => 'test',
//    'interval_time' => '60',
//];
//续费下发套餐
//$config['timerTask'][] = [
//    'task_name' => 'TimeTask',
//    'method_name' => 'deliveryPackage',
//    'interval_time' => '5',
////    'max_exec' => 1,
//];

///////////////
//计算主板升级监控
//$config['timerTask'][] = [
//    'task_name' => 'DeviceTask',
//    'method_name' => 'upgrade',
//    'interval_time' => '1',
//];
//续费提醒--每天9点
//$config['timerTask'][] = [
//    'start_time' => 'Y-m-d 15:00:00',
//    'task_name' => 'TimeTask',
//    'method_name' => 'renewalWarn',
//    'interval_time' => '86400',
//];


// 优惠券修改
$config['timerTask'][] = [
    'start_time' => 'Y-m-d 00:01:00',
    'task_name' => 'TimeTask',
    'method_name' => 'couponTask',
    'interval_time' => '86400',
];


// 主板下发套餐监控
//$config['timerTask'][] = [
//    'task_name' => 'DeviceTask',
//    'method_name' => 'PlanToMonitor',
//    'interval_time' => '10',
//];

// 检查市场推广是否完成业绩标准
//$config['timerTask'][] = [
//    'task_name' => 'SettlementTask',
//    'method_name' => 'qualified',
//    'interval_time' => '60',
//];






// 日程顺延
//$config['timerTask'][] = [
//    'start_time' => 'Y-m-d 2:00:00',
//    'task_name' => 'EngineerTask',
//    'method_name' => 'schedule',
//    'interval_time' => '86400',
//];

// 扣服务费
//$config['timerTask'][] = [
//    'start_time' => 'Y-m-d 2:00:00',
//    'task_name' => 'DeviceTask',
//    'method_name' => 'serviceFeePay',
//    'interval_time' => '86400',
//];
// 户号节能改造费账单出账
$config['timerTask'][] = [
    'start_time' => 'Y-m-d 0:00:00',
    'task_name' => 'TimeTask',
    'method_name' => 'outBillData',
    'interval_time' => '60',
];

//定时查询银行打款信息
$config['timerTask'][] = [
    'start_time' => 'Y-m-d 00:00:00',
    'task_name' => 'TimeTask',
    'method_name' => 'inquiryBank',
    'interval_time' => '86400',
];
//缴费提醒
$config['timerTask'][] = [
    'start_time' => 'Y-m-d 00:00:00',
    'task_name' => 'TimeTask',
    'method_name' => 'billPayWarn',
    'interval_time' => '60',
];
return $config;