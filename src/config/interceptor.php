<?php
/**
 * 拦截器  拦截器里的方法不需要token认证
 */
$config['custom']['interceptor'] = [
    'User' => [
        'User' => ['verification_promotion','marketingLand','register', 'bind','account_login', 'updateToken','alipayRegister','test','aliPhone','login','getBaiduOpenid','binds','changePassword','getParentInfo'],
        'Equipment' => ['index', 'equipmentsDetail', 'equipmentRentalPackage'],
        'Order' => ['getEvaluateList'],
        'Callback' => ['rentOrder', 'rentRenewOrder','ali_rentOrder','test','baiduRentOrder','renewCallback','feeOrder'],
        'Wx' => ['aesPhoneDecrypt'],
        'Relation' => ['levelSettlement','test','again'],
        'Spread' => ['dealerApply'],
        'Parts' => ['editEquipmentsParts','resetEquipmentsParts','delEquipmentsParts'],
        'Test'=>['test'],
        'Share' => ['shareLog'],
        'Browse' => ['browseLog'],
        'Equipmentstype' => ['getEquipmentstypeList'],
        'Spread'=>['dealerApplyResult','dealerApply','verificationExtension'],
        'Work'=>['getEnergySavingDataList']
    ],
    'Engineer' => [
        'Engineers' => ['login', 'updateToken', 'forgetPassword'],
        'Wx' => ['register'],
        'Pay' => ['test'],
        'Callback' => ['scanPayCallback', 'payCallback','rentRenewOrder','payCallbackAdd','rentRenewOrderAdd','scanPayAddOrder','renewCallback'],
        'Order' => ['abTest'],
        'Contract'=>['getContractInfo'],
        'Workorder'=>['setPartApi'],

    ],
    'House' => [
        'Issue' => [
            'bindingPackage',
            'screen_status',
            'power_off',
            'mandatory_rinse',
            'charge_as',
            'as_negative',
            'time_synchronization',
            'heartbeat',
            'filter_element_reset_modification',
            'mode_switch',
            'factory_data_reset',
            'remote_firmware',
            'version',
            'modify_mandatory_flushing_times',
            'modified_timed_washing',
            'modify_maintenance_parameters',
            'modify_control_parameter_one',
            'modify_control_parameter_two',
            'switch_test_mode',
            'computer_board_time_sync',
            'water_sync',
            'data_sync',
            'syncRedisToDevice',
            'delDevice',
            'readFile',
        ]
    ],
    'Boss'=>[
        'Company'=>[
            'getlists'
        ]
    ],
    'Achievement'=>[
        'Login'=>[
            'login'
        ],
        'Settlement'=>[
            'balance',
            'channelSettlement',
            'removeSettlement',
            'balanceRepair',
            'exportAchievement',
            'contractualRelationship',
            'teamPerformance'
        ],
        'Config'=>[
            'getCompanyInfo'
        ],
        'Dealer'=>[
            'uploadDealerContract',
            'getDealerContract',
            'auditing'
        ],
    ],
];
return $config;