<?php
/**
 * Created by PhpStorm.
 * User: LG
 * Date: 2019/6/11
 * Time: 11:33
 */

//memcached未设置开机启动，请使用命令启动：memcached -d -m 2024 -l 127.0.0.1 -p 11211 -u root
//启动/安装详情：https://www.runoob.com/memcached/memcached-install.html
$config['memcached'] = [
    [
      '127.0.0.1',
      '11211',
      '30'
    ],
];

return $config;