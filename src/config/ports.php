<?php
/**
 * Created by PhpStorm.
 * User: zhangjincheng
 * Date: 16-7-14
 * Time: 下午1:58
 */

use Server\CoreBase\PortManager;

//$config['ports'][] = [
//    'socket_type' => PortManager::SOCK_TCP,
//    'socket_name' => '0.0.0.0',
////    'socket_port' => 9091,
//    'socket_port' => 9091,
//    'pack_tool' => 'LenJsonPack',
//    'route_tool' => 'NormalRoute',
//    'middlewares' => ['MonitorMiddleware']
//];
//$config['ports'][] = [
//    'socket_type' => PortManager::SOCK_TCP,
//    'socket_name' => '0.0.0.0',
//    'socket_port' => 9993,
//    'pack_tool' => 'WaterHousePack',
//    'route_tool' => 'WateHouserRoute',
//    'middlewares' => ['MonitorMiddleware'],
//    'event_controller_name' => 'House/Base',
//    'connect_method_name' => "onConnect",
//];

$config['ports'][] = [
    'socket_type' => PortManager::SOCK_HTTP,
    'socket_name' => '0.0.0.0',
    'socket_port' => 9093,
    'route_tool' => 'NormalRoute',
    'middlewares' => ['MonitorMiddleware', 'NormalHttpMiddleware','AuthHttpMiddleware'],
    'method_prefix' => 'http_',
];
/*
$config['ports'][] = [
  'socket_type' => PortManager::SOCK_WS,
  //'socket_ssl'=>true, //当swoole需要证书时开启
  'socket_name' => '0.0.0.0',
  'socket_port' => 9093,
  'route_tool' => 'NormalRoute',
  'pack_tool' => 'NonJsonPack',
  'opcode' => PortManager::WEBSOCKET_OPCODE_TEXT,
  'middlewares' => ['MonitorMiddleware', 'NormalHttpMiddleware'],
];
*/
return $config;