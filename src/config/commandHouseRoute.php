<?php
/**
 * Created by PhpStorm.
 * User: bg
 * Date: 2018/3/5 0005
 * Time: 下午 3:59
 */



$config['commandhouse']['route'] = [
    //终端上报指令对应缴费器与方法
    //机器状态
    '01'=>['c'=>'House/Report','a'=>'machine_state'],
    //主板套餐
    '02'=>['c'=>'House/Report','a'=>'bindingPackage'],
    //屏幕关闭
    '03'=>['c'=>'House/Report','a'=>'screen_status'],
    //屏幕开启
    '04'=>['c'=>'House/Report','a'=>'screen_status_two'],
    //关机
    '05'=>['c'=>'House/Report','a'=>'power_off'],
    //开机
    '06'=>['c'=>'House/Report','a'=>'power_off_two'],
    //强制冲洗
    '07'=>['c'=>'House/Report','a'=>'mandatory_rinse'],
    //充正值
    '08'=>['c'=>'House/Report','a'=>'charge_as'],
    //充负值
    '09'=>['c'=>'House/Report','a'=>'as_negative'],
    //用水同步
    '0a'=>['c'=>'House/Report','a'=>'synchronization_with_water'],
    //用时同步(已用天数)
    'ba'=>['c'=>'House/Report','a'=>'time_synchronization'],
    //用时同步(剩余天数)
    'bb'=>['c'=>'House/Report','a'=>'time_sync'],
    //工作状态同步
    '0c'=>['c'=>'House/Report','a'=>'working_state_synchronization'],
    //滤芯复位与修改
    '0e'=>['c'=>'House/Report','a'=>'filter_element_reset_modification'],
    //模式切换
    '0f'=>['c'=>'House/Report','a'=>'mode_switch'],
    //恢复出厂设置
    '10'=>['c'=>'House/Report','a'=>'factory_data_reset'],
    //修改强制冲洗时间
    '11'=>['c'=>'House/Report','a'=>'modify_mandatory_flushing_times'],
    //定时冲洗参数修改
    '12'=>['c'=>'House/Report','a'=>'modified_timed_washing'],
    //检修参数修改
    '13'=>['c'=>'House/Report','a'=>'modify_maintenance_parameters'],
    //控制参数修改1(TDS值设置)
    '14'=>['c'=>'House/Report','a'=>'modify_control_parameter_one'],
    //控制参数修改2(联网参数修改)
    '15'=>['c'=>'House/Report','a'=>'modify_control_parameter_two'],
    //开启关闭测试模式
    '16'=>['c'=>'House/Report','a'=>'switch_test_mode'],
    //电脑板时间同步1(主板主动上传)
    '17'=>['c'=>'House/Report','a'=>'device_time_sync'],
    //电脑板时间同步2(服务端主动下发)
    '18'=>['c'=>'House/Report','a'=>'computer_board_time_sync'],
    //用水量同步(已用流量)
    'aa'=>['c'=>'House/Report','a'=>'water_used_traffic'],
    //用水量同步(剩余流量)
    'ab'=>['c'=>'House/Report','a'=>'water_remaining_traffic'],
    //远程升级请求：（获取版本号）
    '1a'=>['c'=>'House/Report','a'=>'version'],
    //远程固件下发与接收（起始帧）
    '2a'=>['c'=>'House/Report','a'=>'remote_firmware_start'],
    //远程固件下发与接收（数据帧）
    '3a'=>['c'=>'House/Report','a'=>'remote_firmware_process'],
    //远程固件下发与接收（结束帧）
    '4a'=>['c'=>'House/Report','a'=>'remote_firmware_end'],
    //屏幕显示模式切换
    '5a'=>['c'=>'House/Report','a'=>'switch_screen_display_mode'],
    //检修状态切换
    '6a'=>['c'=>'House/Report','a'=>'maintenance_state_switching'],
    //远程重启
    '7a'=>['c'=>'House/Report','a'=>'remote_restart'],
    //下次启动主板时是否运行新固件
    '8a'=>['c'=>'House/Report','a'=>'run_new_firmware'],
    //运行固件切换
    '9a'=>['c'=>'House/Report','a'=>'firmware_switch'],
    //运行固件切换
    '2c'=>['c'=>'House/Report','a'=>'buzzer'],
    //配置是否接烧水壶
    '3c'=>['c'=>'House/Report','a'=>'kettle'],

];

return $config;