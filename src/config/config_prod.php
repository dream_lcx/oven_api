<?php

/*
 * 线上环境配置--线上
 */
//域名配置
$config['callback_domain_name'] = 'https://api.hznysc.com';
$config['static_resource_host'] = 'https://api.hznysc.com/'; //静态资源路径.图片,视频等

$config['equipment_api_url'] = 'https://eq.hznysc.com';
$config['big_data_url'] = 'http://api.hznysc.com:8482/';
$config['equipment_qrcode'] = 'https://eq.hznysc.com/?code=';//设备二维码内容规则
//百度小程序
$config['baidu_weapp']= [
    'appKey'=>'MMMuHF',
    'dealId'=>'386317915'
];

return $config;
