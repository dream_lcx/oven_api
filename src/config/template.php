<?php


/*************************************重庆华灶**************************/
$config['company_1'] = [
    'user_new_order' => [
        'desc' => '用户线上下单支付成功',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => true, 'wx_template_id' => '2-6SS51P8QvDVgUbc3GAhgsAD786Sx_jwWAfcYWkptU', 'mp_template_id' => 'PBoUDM9amhwNgKW16tuB8uHi94ZCbpX7v-fBTYEvdGs', 'title' => '订单支付成功通知']
            ],
            'admin' => [
                'websocket' => ['switch' => false],
                'email' => ['switch' => true],
            ],
            'big_data' => [
                'websocket' => ['switch' => false],
            ]
        ]
    ],
    'user_appoint_success' => [
        'desc' => '用户线上预约安装成功',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => true, 'wx_template_id' => 'KNOyQaUk9fg5HA-j7jZYtE1pAeE00hMTM44YargH-Cg', 'mp_template_id' => 'h14oNYRY6TGUb0COsvvSM1ssslaoZAj0gvp6DFuSYxw', 'title' => '预约成功通知']
            ],
            'admin' => [
                'websocket' => ['switch' => true],
                'email' => ['switch' => true],
            ],
            'big_data' => [
                'websocket' => ['switch' => true],
            ]
        ]
    ],
    'engineer_go_home' => [
        'desc' => '工程师傅上门',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'big_data' => [
                'websocket' => ['switch' => false],
            ]
        ]
    ],
    'engineer_confirm_time' => [
        'desc' => '工程师傅确认预约时间',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => true, 'wx_template_id' => 'Re4HThLgxfFAfv1Bus9uXUewdTIEr-WkFaRbWDzP6PU', 'mp_template_id' => 'EwY4kjLE5XTeGiiaHtHYam04HPeMrD8_YdAcIiVu-HQ', 'title' => '确认预约时间']
            ],

        ]
    ],
    'bind_device_success' => [
        'desc' => '工程师傅给客户绑定主板成功',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => false, 'wx_template_id' => 'pseePIxMOAkCamHsNNJzmvu4R37r6KHXrNHLVhcNeX8', 'mp_template_id' => 'qI8KOueMyZpGLje7cNGcksjoviQQ2qwypiSmBkz34L4', 'title' => '绑定主板成功']
            ],
        ]
    ],
    'user_bind_device_success' => [
        'desc' => '用户绑定其他主板',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => true, 'wx_template_id' => 'pseePIxMOAkCamHsNNJzmvu4R37r6KHXrNHLVhcNeX8', 'mp_template_id' => 'qI8KOueMyZpGLje7cNGcksjoviQQ2qwypiSmBkz34L4', 'title' => '绑定主板成功']
            ],
        ]
    ],
    'contract_reminding' => [
        'desc' => '绑定主板成功后通知客户签署合同',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => true, 'wx_template_id' => 'O5kxw8V4KvszkM6DscDYMTU42lxE7GnJgJD9zkO9ewE', 'mp_template_id' => '57WZAo_ZdQcRU0QS3402hVe_l1H0I8Yp2Z84D9seKDs', 'title' => '合同签署提醒']
            ],

        ]
    ],
    'contract_sign_success' => [
        'desc' => '合同签署成功',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [

            ],
            'big_data' => [
                'websocket' => ['switch' => true],
            ]
        ]
    ],
    'renew_success' => [
        'desc' => '续费成功',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => true, 'wx_template_id' => '6t8-657Rw_5Mc2GhZ47614xmDWtgeOJ9cnL1ZCKlYvw', 'mp_template_id' => '_xI8u7G1zz9VWJLcnOVLTGZVv-BAYpYgIOc5RTBvmnM', 'title' => '续费成功']
            ],
            'engineer' => [
                'template' => ['switch' => true, 'wx_template_id' => '6t8-657Rw_5Mc2GhZ47614xmDWtgeOJ9cnL1ZCKlYvw', 'mp_template_id' => '_xI8u7G1zz9VWJLcnOVLTGZVv-BAYpYgIOc5RTBvmnM', 'title' => '续费成功']
            ],
            'admin' => [
                'websocket' => ['switch' => false],
                'email' => ['switch' => false],
            ],
            'big_data' => [
                'websocket' => ['switch' => false],
            ]
        ]
    ],
    'renew_warn' => [
        'desc' => '合同到期定时发送缴费提醒',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => true, 'wx_template_id' => '6t8-657Rw_5Mc2GhZ47614xmDWtgeOJ9cnL1ZCKlYvw', 'mp_template_id' => 'frhk6YTuZIGkvS1LnKImIZc5jj3GI3u6p-w9BpBYCrY', 'title' => '续费提醒']
            ],
            'engineer' => [

            ],
            'admin' => [
                'websocket' => ['switch' => false],
                'email' => ['switch' => false],
            ],
            'big_data' => [
                'websocket' => ['switch' => false],
            ]
        ]
    ],
    'reminder' => [
        'desc' => '客户催单',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [

            ],
            'engineer' => [
                'template' => ['switch' => true, 'wx_template_id' => 'FtlyMLdaAGImwM9-8yl68peGJ4qZZwsuwqf3II1sH5E', 'mp_template_id' => 'eDzi-pkweTHMLTzjwz3VRmHtASIZpTIsY-a0FhscX7M', 'title' => '催单通知']
            ],
            'admin' => [
                'websocket' => ['switch' => false],
                'email' => ['switch' => false],
            ],
            'big_data' => [
                'websocket' => ['switch' => false],
            ]
        ]
    ],
    'WorkOrderProcessingNotice' => [
        'desc' => '工单处理通知',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [

            ],
            'engineer' => [
                'template' => ['switch' => true, 'wx_template_id' => '8IvAmVzzH72vntpSNtOKiVjCLEIPq8czP3gSX9xA524', 'mp_template_id' => '8IvAmVzzH72vntpSNtOKiVjCLEIPq8czP3gSX9xA524', 'title' => '工单处理通知']
            ],
            'admin' => [
                'websocket' => ['switch' => false],
                'email' => ['switch' => false],
            ],
            'big_data' => [
                'websocket' => ['switch' => false],
            ]
        ]
    ],
    'TransactionReminders' => [
        'desc' => '工程师傅回单时支付成功',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [

            ],
            'engineer' => [
                'template' => ['switch' => true, 'wx_template_id' => 'ioOGCIzUYJSbZ6gHI7Meo8O1BPTRWffXc1zmjnAZ5G0', 'mp_template_id' => 'ioOGCIzUYJSbZ6gHI7Meo8O1BPTRWffXc1zmjnAZ5G0', 'title' => '交易提醒']
            ],
            'admin' => [
                'websocket' => ['switch' => false],
                'email' => ['switch' => false],
            ],
            'big_data' => [
                'websocket' => ['switch' => false],
            ]
        ]
    ],
    'core_expire' => [
        'desc' => '滤芯到期提醒',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => true, 'wx_template_id' => 'skFyuFrx1ormhbJwr-LO2J_QDaR5xg2egfiJ_AELtoU', 'mp_template_id' => 'eDzi-pkweTHMLTzjwz3VRmHtASIZpTIsY-a0FhscX7M', 'title' => '滤芯到期提醒']
            ],
            'engineer' => [

            ],
            'admin' => [
                'websocket' => ['switch' => false],
                'email' => ['switch' => false],
            ],
            'big_data' => [
                'websocket' => ['switch' => false],
            ]
        ]
    ],
    'out_bill' => [
        'desc' => '月度账单通知',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => true, 'wx_template_id' => 'skFyuFrx1ormhbJwr-LO2J_QDaR5xg2egfiJ_AELtoU', 'mp_template_id' => 'q_3ffNUqgc--gLG2P_WG-GXh2WtjVhaxjKM3WdoMAmE', 'title' => '账单出账通知']
            ],
            'engineer' => [

            ],
            'admin' => [
                'websocket' => ['switch' => false],
                'email' => ['switch' => false],
            ],
            'big_data' => [
                'websocket' => ['switch' => false],
            ]
        ]
    ],
    'check_result' => [
        'desc' => '审核结果通知',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => true, 'wx_template_id' => '', 'mp_template_id' => 'nKnnvf_7g8N7l8JORKoo-AWrFWNYHbT1ALnuyDelldI', 'title' => '审核结果通知']
            ],
            'engineer' => [
                'template' => ['switch' => false, 'wx_template_id' => '', 'mp_template_id' => 'nKnnvf_7g8N7l8JORKoo-AWrFWNYHbT1ALnuyDelldI', 'title' => '审核结果通知']
            ],
            'admin' => [
                'websocket' => ['switch' => false],
                'email' => ['switch' => false],
            ],
            'big_data' => [
                'websocket' => ['switch' => false],
            ]
        ]
    ],
    //新订单通知
    'order_notice_admin' => [
        'desc' => '新订单通知',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => true, 'wx_template_id' => '', 'mp_template_id' => 'vyX7jS_3ZUY6Ls-CclNCSQdTgR6-wXRPG-c2wij83F8', 'title' => '新订单通知']
            ],
            'engineer' => [
                'template' => ['switch' => false, 'wx_template_id' => '', 'mp_template_id' => '', 'title' => '新订单通知']
            ],
            'admin' => [
                'websocket' => ['switch' => false],
                'email' => ['switch' => false],
            ],
            'big_data' => [
                'websocket' => ['switch' => false],
            ]
        ]
    ],
    //退单通知
    'refund_notice_admin' => [
        'desc' => '退单通知',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => true, 'wx_template_id' => '', 'mp_template_id' => 'TGL4gTCTs94gSBLE2JS9odSoBsjUW2LVt91nENuOSec', 'title' => '退单通知']
            ],
            'engineer' => [
                'template' => ['switch' => false, 'wx_template_id' => '', 'mp_template_id' => '', 'title' => '退单通知']
            ],
            'admin' => [
                'websocket' => ['switch' => false],
                'email' => ['switch' => false],
            ],
            'big_data' => [
                'websocket' => ['switch' => false],
            ]
        ]
    ],
    //待处理订单通知
    'wait_handle_order' => [
        'desc' => '退单通知',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => true, 'wx_template_id' => '', 'mp_template_id' => 'LI7pHYDkrCr-uNTggqOSISzuTp1rjtos1_D5nv1jWYk', 'title' => '待处理订单通知']
            ],
            'engineer' => [
                'template' => ['switch' => false, 'wx_template_id' => '', 'mp_template_id' => '', 'title' => '待处理订单通知']
            ],
            'admin' => [
                'websocket' => ['switch' => false],
                'email' => ['switch' => false],
            ],
            'big_data' => [
                'websocket' => ['switch' => false],
            ]
        ]
    ],
    //缴费提醒
    'bill_pay_notice' => [
        'desc' => '缴费提醒',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => true, 'wx_template_id' => '', 'mp_template_id' => 'L-dYMSx910Qk7gqfUFkpicFeYMnxEgTjY852jjBZopw', 'title' => '缴费提醒']
            ],
            'engineer' => [
                'template' => ['switch' => false, 'wx_template_id' => '', 'mp_template_id' => '', 'title' => '缴费提醒']
            ],
            'admin' => [
                'websocket' => ['switch' => false],
                'email' => ['switch' => false],
            ],
            'big_data' => [
                'websocket' => ['switch' => false],
            ]
        ]
    ],
    //上门服务通知-通知市场推广
    'go_service_notice' => [
        'desc' => '缴费提醒',//场景描述
        'who' => [//发送对象,user:用户,admin:后台,big_data:大数据平台
            'user' => [
                'template' => ['switch' => false, 'wx_template_id' => '', 'mp_template_id' => 'L-dYMSx910Qk7gqfUFkpicFeYMnxEgTjY852jjBZopw', 'title' => '上门服务通知']
            ],
            'engineer' => [
                'template' => ['switch' => false, 'wx_template_id' => '', 'mp_template_id' => '', 'title' => '上门服务通知']
            ],
            'agent' => [
                'template' => ['switch' => true, 'wx_template_id' => '', 'mp_template_id' => 'pk2QD1QPbWUQKjtIkpbTt3xDfYc9CJB2JokCpA3uE0E', 'title' => '上门服务通知']
            ],
            'admin' => [
                'websocket' => ['switch' => false],
                'email' => ['switch' => false],
            ],
            'big_data' => [
                'websocket' => ['switch' => false],
            ]
        ]
    ],

];

return $config;