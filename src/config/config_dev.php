<?php

/**
 * 线下环境配置---用户开发测试
 */
$config['debug_config'] = [
    'callback_domain_name' => 'https://admin.cqthesis.cn/',
    'static_resource_host' => 'https://admin.cqthesis.cn/',
    'equipment_api_url' => 'http://106.55.241.55:8282',
    'big_data_url'=>'http://106.55.241.55:8482/',
    'equipment_qrcode'=>'https://hzeq.youheone.cn/?code=',//设备二维码内容规则
    //百度小程序
    'baidu_weapp' => [
        'appKey' => 'MMMuHF',
        'dealId' => '386317915'
    ]


];


return $config;
