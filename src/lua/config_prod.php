<?php

/*
 * 线上环境配置--线上
 */
//域名配置
$config['callback_domain_name'] = 'https://jscloud.youheone.com';
$config['static_resource_host'] = 'https://jscloud.youheone.com/'; //静态资源路径.图片,视频等
//微信配置--用户端
$config['wx'] = [
    'app_wx_name' => '优净云',
    'appId' => 'wx76e19f9eb5bcb658',
    'appSecret' => '38906e7769ded6a44d550ecde521f4d2',
    'pay_key' => 'chongqingchuanjinguanggaoyouxian',
    'mchid' => '10022427'
];
//微信配置--维修端
$config['engineer_wx'] = [
    'app_wx_name' => '优净云服务端',
    'appId' => 'wx109200d9d5977713',
    'appSecret' => '0eb7cd3e0263ad71e942c7669f12ffea',
    'pay_key' => 'chongqingchuanjinguanggaoyouxian',
    'mchid' => '10022427'
];
//微信公众号配置
$config['wechat'] = [
    'app_wx_name' => '优净云',
    'appId' => 'wx2b40e2213148a181',
    'appSecret' => 'dfeddc5ded4ffb2ea1f614209a8b3684',
    'pay_key' => 'chongqingchuanjinguanggaoyouxian',
    'mchid' => '10022427'
];
//小程序--用户端模板ID
$config['wx_template'] = [
    'pay_success' => ['id' => 'NbbQ24MUndmZTXqaTcFl0A3Y-C9u5Knl7a2lnm1a-o8', 'title' => '订单支付成功通知'],
    'after_sale' => ['id' => 'l4S2naoYdwjB_PUUgU3R58wCMo4GwR0zqApFP-ZvZl8', 'title' => '售后通知'],
    'renew_success' => ['id' => 'tIzSxCbKe6pmM1xbOMcaZM28Ojm-SWym8Ij5QlECxL0', 'title' => '续费成功通知'],
    'appoint_success' => ['id' => 'agxt7Lrr8GmXezVxRoR66XTkzDx40xYNRWsitR4VysE', 'title' => '预约成功通知'],
    'device_bind_success' => ['id' => 'g1NbKI3LmOVG-k8dnY1FcFhoruKCF7fU-IhE5dG7a7c', 'title' => '主板绑定通知'],
    'appointment_time' => ['id' => 'tvXbvFnS853HTb4yg1KEutEmiQGTGYQF4L_YQOp0wEA', 'title' => '预约时间修改通知'],
    'electronic_contract' => ['id' => 'czHVUsPt2lxX_N-VHTTV_45VO-6afgqvuBiYegBGwCI', 'title' => '电子合同签约通知'],
    'rental_contract' => ['id' => 'O4MGKobPNpsOvBET2SRWAv52Bba_Tceqpq78ZvrmU', 'title' => '租赁合同通知'],
    'contract_reminding' => ['id' => '_TZ43gwxG2KYw5yRi0uB7nbN6FM9oRU6QoyQr0lmHew', 'title' => '合同签署提醒'],
    'recevice_order' => ['id' => 'MV8xX1R0JmxwQsy9RAtA-bSuZOfxjaacUa_U9s-dqxk', 'title' => '接单成功'],
    'check_result' => ['id' => 'G91HNIw5rF-fZ3aEtffan49K_aQ0cCAucOnwkqlan7k', 'title' => '审核结果通知'],
    'renew_warn' => ['id' => '6DJI-Q1QG1FizEPKDGBd8iNBH54KkSQ9zA59hC6iBjE', 'title' => '缴费提醒'],
    
];
//小程序--维修端模板消息ID
$config['engineer_wx_template'] = [
    'new_order' => ['id' => 'SmjmTO0uJDo5yBvJtSmkssJA13sCBjEND6rqhzYFnWU', 'title' => '新订单通知'],
    'reminder' => ['id' => 'CXw8f6pfX577eLv23az0nqke_tHnjQq40-1clwAaToI', 'title' => '催单通知']
];
//公众号--用户端模板ID
$config['mp_user_template'] = [
    'pay_success' => ['id' => 'DhdmLze-8fnwd6ojQD2bekDSXtW1WcNvIfDdnN08y-I', 'title' => '订单支付成功通知'],
    'after_sale' => ['id' => '2PSWkobiWh3CIiR_GV8GbfuWnYyq3ivOMdFvZlQJ9J0', 'title' => '售后通知'],
    'renew_success' => ['id' => 'IyYxpkG1O0pCxm3rvaGSo6QTxJxSmYKewT_iRjWt0CA', 'title' => '续费成功通知'],
    'appoint_success' => ['id' => 'g5u_WF8UxjOLHRAFg4jYn-Fxg2sg2G8fpvTcidns0fM', 'title' => '预约成功通知'],
    'device_bind_success' => ['id' => '9biiEbaXur0iaJR5ryNUVz2Kz0N03e_m-EWCYHXu5ik', 'title' => '主板绑定通知'],
    'appointment_time' => ['id' => 'LRSVLs-GWLjk20DQR9NtqJaThHS_Tt26tW1ux84CWns', 'title' => '确认预约时间通知'],
    'electronic_contract' => ['id' => 'oN-DW41ArIjlWRYg0P4eLvyJjfZZQFg50R5AQuzj9SI', 'title' => '收租合同通知'],
    'rental_contract' => ['id' => 'oN-DW41ArIjlWRYg0P4eLvyJjfZZQFg50R5AQuzj9SI', 'title' => '租赁合同通知'],
    'contract_reminding' => ['id' => 'BFvFp70cWoYd0T-SfefNjasWkXasB8mK3SCjmow5ltw', 'title' => '合同签署通知'],
    'recevice_order' => ['id' => 'mOjkg0wSVXTKE109WHBdcY7rCrNA5tfwhaI4J1w1U2I', 'title' => '接单成功提醒'],
    'check_result' => ['id' => 'ScPeThw7FKhmP33WdsOfQjuC0ZSD2x-42Bb39pQy6Qo', 'title' => '审核结果通知'],
    'renew_warn' => ['id' => 'g5MRrhBFUNGPy34OImV9uu6v2Qcrb1kvGBSbyFvx-x0', 'title' => '缴费提醒'],
    'core_expire' =>['id'=>'skFyuFrx1ormhbJwr-LO2J_QDaR5xg2egfiJ_AELtoU','title'=>'滤芯到期提醒']
];
//公众号--维修端模板ID
$config['mp_engineer_template'] = [
    'new_order' => ['id' => 'fOEwotoRCZnLoljYimpxp0fzWMpixcyJWW1tVz9JUJU', 'title' => '新订单通知'],
    'reminder' => ['id' => 'WJgmPH8SY-lMh7z3SC-Rz14mUxwe2JYe3heD_QFuNMo', 'title' => '催单通知']
];

return $config;
