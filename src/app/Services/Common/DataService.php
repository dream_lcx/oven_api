<?php

namespace app\Services\Common;

/**
 * 大数据服务
 * Class HttpService
 * @package app\Services\Common\HttpService
 */
class DataService
{

    /**
     * 发送通知到多个uid
     * @param type $key
     * @param type $flag
     * @return type
     */
    public static function sendToUids(array $param)
    {
        $redis = get_instance()->loader->redis("redisPool", get_instance());
        $equipmentListModel = get_instance()->loader->model('EquipmentListsModel', get_instance());
        $data = $redis->lRange('cloud_big_data', 0, -1);
        foreach ($data as $k => $v) {
            $status = get_instance()->coroutineUidIsOnline($v);
            if (!$status) {
                $redis->lRem('cloud_big_data', $v, 1);
            } else {
                //如果存在主板编号查询主板位置
                switch ($param['command']) {
                    case 'A1':
                        $val = [];
                        $notice = [];
                        if ($param['data']['type'] == 8 || $param['data']['type'] == 4) {
                            if (!empty($param['data']['device_no'] ?? '')) {
                                $device = $equipmentListModel->findEquipmentLists(['device_no' => $param['data']['device_no']], 'lat,lng');
                                if (!empty($device)) {
                                    $val['name'] = '';
                                    $notice[] = ['title' => '主板编号', 'content' => $param['data']['device_no']];
                                    $notice[] = ['title' => '主板消息', 'content' => $param['data']['status']??''];
                                    $val['value'] = [$device['lng'], $device['lat'], "主板通知", $notice, 3];
                                }

                            }
                        } else if ($param['data']['type'] == 2) {
                            $val['name'] = '';
                            $notice[] = ['title' => '业务类型', 'content' => $param['data']['product']];
                            $notice[] = ['title' => '客户电话', 'content' => $param['data']['user_tel']];
                            $val['value'] = [$param['data']['lng'], $param['data']['lat'], "新工单通知", $notice, 3];
                        }
                        if (!empty($val)) {
                            $param['data']['info'] = [$val];
                        }
                        break;
                }
                get_instance()->sendToUid($v, json_encode($param));

            }
        }

    }

}
