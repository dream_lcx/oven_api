<?php

namespace app\Services\Common;

use app\Services\Company\CompanyService;
use app\Tasks\AppTask;
use app\Wechat\WxAuth;

/**
 * 通知服务
 * Class NotifiyService
 * @package app\Services\Common\HttpService
 */
class NotifiyService
{

    /**
     * 发送通知-异步
     * @desc 发送顺序:小程序模板消息-公众号模板消息-短信
     * @param $openid 必选  string  用户openid
     * @param $weapp_template_data 必选  string  小程序模板内容
     * @param $weapp_template_id 必选  string  小程序模板ID
     * @param $template_tel 必选  string  手机号
     * @param $template_content 必选  string  短信内容
     * @param $template_url 必选  string  模板跳转URL
     * @param $weapp_template_keyword 必选  string  放大的关键字
     * @param $mp_template_id 必选  string  公众号模板ID
     * @param $mp_template_data 必选  string  公众号模板内容
     * @param $name 必选  string  区分用户小程序和工程小程序 user用户端,engineer工程端
     * @return bool
     * @date 2019-04-26
     * @author lcx
     */
    public static function sendNotifiyAsync($openid, $weapp_template_data, $weapp_template_id, $template_tel, $template_content, $template_url, $weapp_template_keyword, $mp_template_id, $mp_template_data, $name = 'user', $company_id = 1)
    {
        $company_config = CompanyService::getCompanyConfig($company_id);
        $send_data = [$openid, $weapp_template_data, $weapp_template_id, $template_tel, $template_content, $template_url, $weapp_template_keyword, $mp_template_id, $mp_template_data, $name, $company_config];
        $unitTask = get_instance()->loader->task(AppTask::class, get_instance());
        $unitTask->startTask("sendNotifiyTask", $send_data, -1, function ($serv, $task_id, $data) {

        });
    }

    /**
     * 发送通知
     * @desc 发送顺序:小程序模板消息-公众号模板消息-短信
     * @param $openid 必选  string  用户openid
     * @param $weapp_template_data 必选  string  小程序模板内容
     * @param $weapp_template_id 必选  string  小程序模板ID
     * @param $template_tel 必选  string  手机号
     * @param $template_content 必选  string  短信内容
     * @param $template_url 必选  string  模板跳转URL
     * @param $weapp_template_keyword 必选  string  放大的关键字
     * @param $mp_template_id 必选  string  公众号模板ID
     * @param $mp_template_data 必选  string  公众号模板内容
     * @param $name 必选  string  区分用户小程序和工程小程序 user用户端,engineer工程端
     * @return bool
     * @date 2019-04-26
     * @author lcx
     */
    public static function sendNotifiy($openid, $weapp_template_data, $weapp_template_id, $template_tel, $template_content, $template_url, $weapp_template_keyword, $mp_template_id, $mp_template_data, $name = 'user', $company_config = [])
    {
        if(!get_instance()->config->get('notice_switch')){
            return true;
        }
        $wx_config = $company_config['wx_config'];
        if ($name == 'spread') {//市场推广端
            $wx_config = $company_config['spread_wx_config'];
        }else if($name=='engineer'){
            $wx_config = $company_config['engineer_wx_config'];
        }else if($name=='agent'){
            $wx_config = $company_config['spread_wx_config'];
        }
        $mp_wx_config = $company_config['mp_config'];
        $sms_config = $company_config['sms_config'];
        //$WxAuth = new WxAuth($wx_config["appId"], $wx_config["appSecret"]);
        $WxAuth = WxAuth::getInstance($wx_config['appId'], $wx_config['appSecret']);
        $WxAuth->setConfig($wx_config['appId'], $wx_config['appSecret']);
        $weapp_form_id = false;
        $mp_template_msg = array(
            'appid' => $mp_wx_config['appId'],
            'template_id' => $mp_template_id,
            'url' => 'https://www.youheone.com',
            'miniprogram' => array(
                "appid" => $wx_config['appId'],
                "pagepath" => $template_url
            ),
            'data' => $mp_template_data
        );
        $res = false;
        if (!empty($mp_template_id) && !empty($mp_template_data)) {
            $res = $WxAuth->smallSendUniformMessage($openid, array(), $mp_template_msg, $name, $company_config['company_id']);
        }
        //如果小程序模板消息和公众号模板消息都发送失败,发送短信
        $dev_mode = ConfigService::getConfig('develop_mode', false, $company_config['company_id']);
        if (!$dev_mode && !$res) {//如果是非开发模式且模板消息发送失败时发送短信
            $res = SmsService::sendBaMiSms($template_tel, $template_content, $sms_config);
        }
        return true;
    }

    /**
     * 获取formId
     * @desc 用于发送模板消息
     * @param $openid 必选  string  用户openid
     * @return bool
     * @date 2019-04-26
     * @author lcx
     */
    public static function getFormId($openid)
    {
        $redis = get_instance()->loader->redis("redisPool", get_instance());
        $key = 'cloud_water:' . $openid;
        $arr = $redis->rPop($key);
        if (empty($arr)) {
            return false;
        }
        $data = json_decode($arr, true);
        if ($data['expire_time'] > time()) {
            return $data['formId'];
        } else {
            NotifiyService::getFormId($openid);
        }
    }

}
