<?php

namespace app\Services\Common;

use think\image\Exception;

/**
 * 配置服务
 * Class HttpService
 * @package app\Services\Common\HttpService
 */
class ConfigService
{

    /**
     * 获取网站配置
     * @param type $key
     * @param type $flag
     * @return type
     */
    public static function getConfig($key = '', $flag = true, $company_id = 1, $operation_id = 0)
    {
        $redis = get_instance()->loader->redis("redisPool", get_instance());
//        $redis_data = $redis->hGet('oven_system_config', $company_id . '_' . $operation_id . '_' . $key);
//
//        if (!empty($redis_data)) {
//            return $redis_data;
//        }

        $mysql = get_instance()->config['mysql'];
        $prefix = $mysql[$mysql['active']]['prefix'] ?? '';
        $model = get_instance()->loader->model('AchievementModel', get_instance());
        $model->table = 'config';
        if (get_instance()->isTaskWorker()) {
            $model->table = $prefix . 'config';
        }
        $data = $model->findData(['name' => $key, 'company_id' => $company_id, 'operation_id' => $operation_id], 'value');
        //如果当前运营中心配置为空,则获取全局配置
        if (empty($data) && $operation_id != 0) {
            $data = $model->findData(['name' => $key, 'company_id' => $company_id, 'operation_id' => 0], 'value');
        }
        $redis->hSet('oven_system_config', $company_id . '_' . $operation_id . '_' . $key, $data['value']);
        return empty($data) ? '' : $data['value'];

    }

    /**
     * 获取结算配置
     * @param string $id 自增ID,可选
     * @param string $key 配置key,必选
     * @param int $company_id 公司ID,可选默认1
     * @param int $return_data_type 返回数据类型,1：仅value值 2整条数据
     * @return mixed|null
     * @throws \Server\CoreBase\SwooleException
     */
    public static function getBalanceConfig(string $id = '', string $key, $company_id = 1, $return_data_type = 1)
    {
        $achievementModel = get_instance()->loader->model('AchievementModel', get_instance());
        $achievementModel->table = 'oa_balance_config';
        if (!empty($id)) {
            $where['id'] = $id;
        }
        $where['`key`'] = $key;
        $where['`company_id`'] = $company_id;
        $result = $achievementModel->findData(['`key`' => $key, 'company_id' => $company_id]);
        unset($where);
        //如果不存在或者状态关闭直接返回默认值
        if (empty($result) || $result['state'] == 2) {
            $result['value'] = get_instance()->config->get($key);
            if ($return_data_type == 1) {
                return $result['value'];
            }
            return $result;

        }
        //如果设置有效期,不在有效期范围内直接返回默认值
        if ($result['type'] == 2) {
            if ((!empty($result['start_time']) && $result['start_time'] > time()) || (!empty($result['end_time']) && $result['end_time'] < time())) {
                $result['value'] = get_instance()->config->get($key);
                if ($return_data_type == 1) {
                    return $result['value'];
                }
                return $result;
            }
        }
        if ($result['format'] == 2) {
            $result['value'] = json_decode($result['value'], 1);
        }
        if ($return_data_type == 1) {
            return $result['value'];
        }
        return $result;
    }

    /**
     * 获取模板消息配置
     * @param $role
     * @param $key
     * @param $company_id
     * @return mixed 返回模板ID,如果不开启直接返回false
     */
    public static function getTemplateConfig($key, $role, $company_id)
    {
        $config = get_instance()->config->get('company_' . $company_id)[$key]['who'][$role];
        return $config;

    }


}
