<?php

namespace app\Services\Common;

use PHPMailer\PHPMailer\PHPMailer;
use app\Tasks\AppTask;
use app\Services\Common\ConfigService;

/**
 * 邮件服务
 * Class HttpService
 * @package app\Services\Common\HttpService
 */
class EmailService
{

    /**
     * 发送邮件-异步
     * @desc
     * @param $email 必选  string  邮箱
     * @param $content 必选  string  内容
     * @param $subject 必选  string  主题
     * @param $attach_path 必选  string  附件路径
     * @param $attach_name 必选  string  附件名称
     * @return bool
     * @date 2019-04-26
     * @author lcx
     */
    public static function sendEmailAsync($email, $content, $subject, $attach_path = '', $attach_name = '', $helo = '感谢使用', $company_id = 1)
    {
        $data = [$email, $content, $subject, $attach_path, $attach_name, $helo, $company_id];
        $unitTask = get_instance()->loader->task(AppTask::class, get_instance());
        $unitTask->startTask("sendEmailTask", $data, -1, function ($serv, $task_id, $data) {

        });
    }

    /**
     * 发送邮件
     * @desc
     * @param $email 必选  string  邮箱
     * @param $content 必选  string  内容
     * @param $subject 必选  string  主题
     * @param $attach_path 可选  string  附件地址
     * @param $attach_name 可选  string  附件名称
     * @return bool
     * @date 2019-04-26
     * @author lcx
     */
    public static function sendEmail($email, $content, $subject = '', $attach_path = '', $attach_name = '', $helo = '感谢使用', $company_id = 1)
    {
        if (!isEmail($email)) {
           return  returnResult(-1000, "邮箱格式不正确");
        }
        if (empty($helo)) {
            $helo = '感谢使用';
        }
        try {
            $mail = new PHPMailer();
            $mail->isSMTP();
            //smtp需要鉴权 这个必须是true
            $mail->SMTPAuth = true;
            //链接qq域名邮箱的服务器地址
            $mail->Host = ConfigService::getConfig('phpmailer_host', false, $company_id);
            //设置使用ssl加密方式登录鉴权
            $mail->SMTPSecure = ConfigService::getConfig('phpmailer_security', false, $company_id);
            //设置ssl连接smtp服务器的远程服务器端口号 可选465或587
            $mail->Port = ConfigService::getConfig('phpmailer_port', false, $company_id);
            //设置smtp的helo消息头 这个可有可无 内容任意
            $mail->Helo = $helo;
            //设置发件人的主机域 可有可无 默认为localhost 内容任意，建议使用你的域名
            $mail->Hostname = '';
            //设置发送的邮件的编码 可选GB2312 我喜欢utf-8 据说utf8在某些客户端收信下会乱码
            $mail->CharSet = ConfigService::getConfig('phpmailer_charset', false, $company_id);
            //设置发件人姓名（昵称） 任意内容，显示在收件人邮件的发件人邮箱地址前的发件人姓名
            $mail->FromName = ConfigService::getConfig('phpmailer_name', false, $company_id);
            //smtp登录的账号 这里填入字符串格式的qq号即可
            $mail->Username = ConfigService::getConfig('phpmailer_addr', false, $company_id);
            //smtp登录的密码 这里填入“独立密码” 若为设置“独立密码”则填入登录qq的密码 建议设置“独立密码”
            $mail->Password = ConfigService::getConfig('phpmailer_pass', false, $company_id);
            //设置发件人邮箱地址 这里填入上述提到的“发件人邮箱”
            $mail->From = ConfigService::getConfig('phpmailer_addr', false, $company_id);
            //邮件正文是否为html编码 注意此处是一个方法 不再是属性 true或false
            $mail->isHTML(true);
            //设置收件人邮箱地址 该方法有两个参数 第一个参数为收件人邮箱地址 第二参数为给该地址设置的昵称 不同的邮箱系统会自动进行处理变动 这里第二个参数的意义不大
            $mail->addAddress($email, ConfigService::getConfig('phpmailer_addr', false, $company_id)); //第一个参数是收件人邮箱地址
            //添加多个收件人 则多次调用方法即可
            // $mail->addAddress('xxx@163.com','晶晶在线用户');
            //添加该邮件的主题
            $mail->Subject = $subject;
            //添加邮件正文 上方将isHTML设置成了true，则可以是完整的html字符串 如：使用file_get_contents函数读取本地的html文件
            $mail->Body = $content;
            //为该邮件添加附件 该方法也有两个参数 第一个参数为附件存放的目录（相对目录、或绝对目录均可） 第二参数为在邮件附件中该附件的名称
            //$mail->addAttachment($tempname,$name.'.xls');
            //同样该方法可以多次调用 上传多个附件
            //  $mail->addAttachment('./Jlib-1.1.0.js', 'Jlib.js');
            if (!empty($attach_path)) {
                $mail->addAttachment($attach_path, $attach_name);
            }
            //发送命令 返回布尔值
            //PS：经过测试，要是收件人不存在，若不出现错误依然返回true 也就是说在发送之前 自己需要些方法实现检测该邮箱是否真实有效
            $status = $mail->send();
            var_dump($status);
        } catch (Exception $e) {
            //写日志
            var_dump($e->getMessage());
           return returnResult(1000, "发送失败" . $e->getMessage());
        }
       return returnResult(1000, "发送成功");
    }
}
