<?php


namespace app\Services\Common;


class UploadService
{
    /**
     * 上传文件
     * @param $file 文件
     * @param string $fileName 文件名称，若为空将自定义
     * @param string $filePath 文件上传目录,注意不以/开头
     * @param int $uploadWay 上传方式 local:上传到本地服务器 qiniu:上传到七牛云
     * @param int $uploadFileType 上传文件类型 file:文件 binary:文件流 base64: base64格式
     * @return array
     * @author  lcx
     * @date 2020-2-17
     */
    public static function upload($file, $fileName = '', $filePath = '', $uploadWay = 'qiniu', $uploadFileType = 'file')
    {
        if ($uploadWay == 'qiniu') {//上传到七牛云
            $bucket = get_instance()->config->get('qiniu.bucket');
            // 构建鉴权对象 #需要填写你的 Access Key 和 Secret Key
            $auth = get_instance()->QiniuAuth;
            // 生成上传 Token
            $token = $auth->uploadToken($bucket);
            $uploadMgr = get_instance()->QiniuUploadManager;
            if (empty($file)) {
                return returnResult(1, '请选择上传图片');
            }
            $key = $fileName;
            if (empty($fileName)) {
                $key = 'IMG_' . date('YmdHis') . '_' . rand(0, 9999) . '.png';
            }
            $key = $filePath . $key;
            try {

                switch ($uploadFileType) {
                    case 'file'://如果是文件
                        if (empty($file['file'] ?? '')) {
                            return returnResult(1, '请选择上传图片');
                        }
                        list($ret, $err) = $uploadMgr->putFile($token, $key, $file['file']['tmp_name']);
                        break;
                    case 'binary'://如果是文件流
                        list($ret, $err) = $uploadMgr->put($token, $key, $file);
                        break;
                    case 'base64'://如果是base64格式的
                        list($ret, $err) = $uploadMgr->put($token, $key, base64_decode($file));
                        break;
                }


            } catch (\Exception $e) {
                return returnResult(1, "上传失败" . $e->getMessage(), []);
            }
            if ($err !== null) {
                return returnResult(1, 'fail');
            } else {
                $data['url'] = $key;
                $data['all_url'] = get_instance()->config->get('qiniu.qiniu_url') . $key;
                return returnResult(0, 'success', $data);
            }
        } else {//上传到本地--待测试
            if (empty($filePath)) {
                return returnResult(1, "缺少上传路径");
            }
            $key = $fileName;
            if (empty($key)) {
                $key = 'IMG_' . date('YmdHis') . '_' . rand(0, 9999) . '.png';
            }
            switch ($uploadFileType) {
                case 'file'://如果是文件
                    $tmp = $file['tmp_name'];
                    break;
                case 'binary'://如果是文件流
                    $tmp = $file;
                    break;
                case 'base64'://如果是base64格式的
                    $tmp = $file;
                    break;
            }
            if (empty($tmp)) {
                return returnResult(1, "请选择文件");
            }
            if (!move_uploaded_file($tmp, $filePath . $key)) {
                return returnResult(1, "上传失败");
            }
            $data['url'] = $key;
            $data['all_url'] = $filePath . $key;
            return returnResult(0, 'success', $data);
        }

    }



}