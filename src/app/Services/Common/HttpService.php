<?php
namespace app\Services\Common;

use app\Services\Log\AsyncFile;

/**
 * Http请求服务
 * Class HttpService
 * @package app\Services\Common\HttpService
 */
class HttpService {
    /**
     * @param $url
     * @param string $headers 索引数组
     * @return bool|string
     */
    public static function get($url, $headers = [])
    {
        $oCurl = curl_init();
        if (stripos($url, "https://") !== FALSE) {
            curl_setopt($oCurl, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($oCurl, CURLOPT_SSL_VERIFYHOST, FALSE);
            curl_setopt($oCurl, CURLOPT_SSLVERSION, 1); // CURL_SSLVERSION_TLSv1
        }
//        $headers[] = "timestamp:1574774596";
//        $headers[] = "key:13wmEmNeAyDmx1Df0CIHuYfZrWhneNJM";
//        $headers[] = "signature:Hr+98b7RvJytnn6Lya2vKS0JAe8=";
        if (!empty($headers)) {
            foreach ($headers as $k => $v) {
                $header[] = $k . ':' . $v;
            }
            curl_setopt($oCurl, CURLOPT_HTTPHEADER, $header);
        }
        curl_setopt($oCurl, CURLOPT_URL, $url);
        curl_setopt($oCurl, CURLOPT_RETURNTRANSFER, 1);
        $sContent = curl_exec($oCurl);
        $aStatus = curl_getinfo($oCurl);
        curl_close($oCurl);
        if (intval($aStatus["http_code"]) == 200) {
            return $sContent;
        } else {
            return false;
        }
    }


    /**
     * POST 请求
     *
     * @param string $url
     * @param array $param
     * @param boolean $post_file
     *            是否文件上传
     * @return string content
     */
    public static function post($url, $param, $post_file = false,$headers="")
    {
        $oCurl = curl_init();
        if (stripos($url, "https://") !== FALSE) {
            curl_setopt($oCurl, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($oCurl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($oCurl, CURLOPT_SSLVERSION, 1); // CURL_SSLVERSION_TLSv1
        }
        if (is_string($param) || $post_file) {
            $strPOST = $param;
        } else {
            $aPOST = array();
            foreach ($param as $key => $val) {
                $aPOST[] = $key . "=" . urlencode($val);
            }
            $strPOST = join("&", $aPOST);
        }
        curl_setopt($oCurl, CURLOPT_URL, $url);
        curl_setopt($oCurl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($oCurl, CURLOPT_POST, true);
        curl_setopt($oCurl, CURLOPT_POSTFIELDS, $strPOST);
        if(!empty($headers)){
             curl_setopt($oCurl, CURLOPT_HTTPHEADER, $headers);
        }
       
        $sContent = curl_exec($oCurl);
        $aStatus = curl_getinfo($oCurl);
        curl_close($oCurl);
        if (intval($aStatus["http_code"]) == 200) {
            return json_decode($sContent,true);
        } else {
            return false;
        }
    }
	/**
	 * 给主板发送指令
	 * @param array $params 参数 ['sn'=>$equipment_number,'osn'=>$order_id,'money'=>$money]
	 * @param string $path 路径 '/Water/Water/recharge_payment'
	 */
	public static function Thrash(array $params, string $path) {
		$getIPAddressHttpClient = get_instance()->getAsynPool('BuyWater');
		$json = json_encode($params);
		$response = $getIPAddressHttpClient->httpClient
				->setData($json)
				->setMethod('post')
				->coroutineExecute($path);
		if ($response['statusCode'] == 200) {
			//echo '发送指令成功!';
			return true;
		} else {
			echo '发送指令失败!';
			return false;
		}
	}
	 /**
     * 请求boss端API
     * @param array $params 参数 ['sn'=>$equipment_number,'osn'=>$order_id,'money'=>$money]
     * @param string $path 路径 '/Water/Water/recharge_payment'
     */
    public static function requestBossApi(array $params, string $path) {
		$bossApiClient = get_instance()->getAsynPool('BossApi');
        $json = json_encode($params);
        $response = $bossApiClient->httpClient
                ->setData($json)
                ->setMethod('post')
                ->coroutineExecute($path);
        if ($response['statusCode'] == 200) {
           // echo '发送成功!';
            return json_decode($response['body'],true);
        } else {
            echo '发送失败!';
			return '';
        }
    }
    public static function postBami($url, $param, $post_file = false, $headers = "")
    {
        $oCurl = curl_init();
        if (stripos($url, "https://") !== FALSE) {
            curl_setopt($oCurl, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($oCurl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($oCurl, CURLOPT_SSLVERSION, 1); // CURL_SSLVERSION_TLSv1
        }
        if (is_string($param) || $post_file) {
            $strPOST = $param;
        } else {
            $aPOST = array();
            foreach ($param as $key => $val) {
                $aPOST[] = $key . "=" . $val;
            }
            $strPOST = join("&", $aPOST);
        }
        curl_setopt($oCurl, CURLOPT_URL, $url);
        curl_setopt($oCurl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($oCurl, CURLOPT_POST, true);
        curl_setopt($oCurl, CURLOPT_POSTFIELDS, $strPOST);
        if (!empty($headers)) {
            curl_setopt($oCurl, CURLOPT_HTTPHEADER, $headers);
        }

        $sContent = curl_exec($oCurl);
        $aStatus = curl_getinfo($oCurl);
        curl_close($oCurl);
        if (intval($aStatus["http_code"]) == 200) {
            return self::xmlToArray($sContent);
        } else {
            return false;
        }
    }

    /*
* 将xml转换成数组
* @params xml $xml : xml数据
* return array $data : 返回数组
*/
    public  static function xmlToArray($xml)
    {
        //禁止引用外部xml实体
        libxml_disable_entity_loader(true);
        $xmlstring = simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA);
        $val = json_decode(json_encode($xmlstring), true);
        return $val;
    }


}