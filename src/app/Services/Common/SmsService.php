<?php

namespace app\Services\Common;

use app\Library\SLog;
use app\Services\Company\CompanyService;
use app\Services\Log\FileLogService;
use app\Tasks\AppTask;

/**
 * 短信服务
 * Class HttpService
 * @package app\Services\Common\HttpService
 */
class SmsService {

	/**
	 * 发送短信-异步
	 * @desc 八米短信
	 * @author lcx
	 * @param $phone 必选  string  手机号 
	 * @param $con 必选  string  内容 
	 * @return bool
	 * @date 2019-04-26
	 */
	public static function sendSmsAsync($phone, $con,$sms_config) {
		$data = [$phone, $con,$sms_config];
		$unitTask = get_instance()->loader->task(AppTask::class, get_instance());
		$unitTask->startTask("sendSmsTask", $data, -1, function ($serv, $task_id, $data) {
			
		});
	}

	/**
	 * 八米短信
	 * @param type $phone
	 * @param type $con
	 * @return boolean
	 */
	public static function sendBaMiSmsOld($phone, $con,$sms_config) {
		if (empty($phone)) {
			return returnResult(-1000, "缺少手机号");
		}
		if (!isMobile($phone)) {
			return returnResult(-1000, "手机号不合法");
		}
		if (empty($con)) {
			return returnResult(-1000, "短信内容不能为空");
		}

		try {
			//配置参数
			//$sms_config = get_instance()->config->get('sms');
			$content = urlencode($con) . "。【" . $sms_config['content'] . "】";
			//$gateway = "http://sms.bamikeji.com:8890/mtPort/mt/normal/send?uid=" . $sms_config['uid'] . "&passwd=" . md5($sms_config['passwd']) . "&phonelist=" . $phone . "&content=" . $content;
            $gateway = "http://sms.bamikeji.com:8890/mtPort/mt/bindip/send?uid=" . $sms_config['uid'] . "&passwd=" . $sms_config['passwd'] . "&phonelist=" . $phone . "&content=" . $content;
            $result = file_get_contents($gateway);
			$result = json_decode($result, true);
			//需要判断接口状态
			if (isset($result['code']) && $result['code'] == 0) {
				return returnResult(1000, "发送成功");
			} else {
				return returnResult(-1000, "发送失败" . $result['msg'] ?? '');
			}
		} catch (\Exception $e) {
			return returnResult(-1000, "发送失败" . $e->getMessage() ?? '');
		}
	}
    /**
     * 八米短信--新系统
     * @param type $phone
     * @param type $con
     * @return boolean
     */
    public static function sendBaMiSms($phone, $con,$sms_config) {

        if (empty($phone)) {
            return returnResult(-1000, "缺少手机号");
        }
        if (!isMobile($phone)) {
            return returnResult(-1000, "手机号不合法");
        }
        if (empty($con)) {
            return returnResult(-1000, "短信内容不能为空");
        }

        try {
            //配置参数
            $content = $con . "。【" . $sms_config['content'] . "】";
            // $gateway = "http://221.122.122.98:8088/sms.aspx?action=send&userid=993&account=cqrq&password=cqrq2022！&mobile=" . $phone . "&content=" . $content;
            $url = "http://221.122.122.98:8088/sms.aspx";
            $param['action'] = 'send';
            $param['userid'] = 993;
            $param['account'] = 'cqrq';
            $param['password'] = 'cqrq2022!';
            $param['mobile'] = $phone;
            $param['content'] = $content;
            $param['sendTime'] = '';
            if(!get_instance()->config->get('notice_switch')){
                return returnResult(1000, "发送成功,验证码:".$content);
            }
            $result = HttpService::postBami($url,$param);
            //需要判断接口状态
            if (isset($result['returnstatus']) && $result['returnstatus'] == 'Success') {
                return returnResult(1000, "发送成功");
            } else {
                return returnResult(-1000, "发送失败" . $result['message'] ?? '');
            }
        } catch (\Exception $e) {
            return returnResult(-1000, "发送失败" . $e->getMessage() ?? '');
        }
    }


    /**
     * 短信通知后台管理人员
     * @param $key 类型 市场推广申请审核marketing，识别表identification_table，合同审核contract，工单审核work_order
     * @param $content 发送内容
     * @throws \Server\CoreBase\SwooleException
     */
    public static function smsNotification($key, $content,$company)
    {
        dump($key);
        dump($content);
        $sms_notification = ConfigService::getBalanceConfig('', trim('sms_notification'), $company, 1);
        if (empty($sms_notification)) return;
        $phone_data = empty($sms_notification[$key]) ? [] : explode(',', $sms_notification[$key]);
        if (empty($phone_data)) return [];
        $company_config = CompanyService::getCompanyConfig($company);
        $sms_config = $company_config['sms_config'];
        dump($phone_data);
        foreach ($phone_data as $key => $value) {
            $send = self::sendBaMiSms($value, $content, $sms_config);
            dump($send);
        }

    }


}
