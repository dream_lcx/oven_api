<?php
/**
 * Created by PhpStorm.
 * User: cdjyj21
 * Date: 2018/9/4
 * Time: 11:57
 */

namespace app\Services\Common;

use Endroid\QrCode\ErrorCorrectionLevel;
use Endroid\QrCode\LabelAlignment;
use Endroid\QrCode\QrCode;

class QrcodeService
{
    protected $_qr;
    protected $_encoding        = 'UTF-8';              // 编码类型
    protected $_size            = 300;                  // 二维码大小
    protected $_logo            = false;                // 是否需要带logo的二维码
    protected $_logo_url        = '';                   // logo图片路径
    protected $_logo_size       = 200;                   // logo大小
    protected $_title           = false;                // 是否需要二维码title
    protected $_title_content   = '';                   // title内容
    protected $_generate        = 'display';            // display-直接显示  writefile-写入文件


    protected $_file_name       = 'qrcode';    // 写入文件路径
    //const MARGIN           = 10;                        // 二维码内容相对于整张图片的外边距
    const MARGIN           = [
        't' => -15,
        'r' => 32,
        'b' => 27,
        'l' => 32,
    ];
    const WRITE_NAME       = 'png';                     // 写入文件的后缀名
    const FOREGROUND_COLOR = ['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0];          // 前景色
    const BACKGROUND_COLOR = ['r' => 255, 'g' => 255, 'b' => 255, 'a' => 0];    // 背景色

    public function __construct($config) {
        isset($config['generate'])      &&  $this->_generate        = $config['generate'];
        isset($config['encoding'])      &&  $this->_encoding        = $config['encoding'];
        isset($config['size'])          &&  $this->_size            = $config['size'];
        isset($config['logo'])          &&  $this->_logo            = $config['logo'];
        isset($config['logo_url'])      &&  $this->_logo_url        = $config['logo_url'];
        isset($config['logo_size'])     &&  $this->_logo_size       = $config['logo_size'];
        isset($config['title'])         &&  $this->_title           = $config['title'];
        isset($config['title_content']) &&  $this->_title_content   = $config['title_content'];
        isset($config['file_name'])     &&  $this->_file_name       = $config['file_name'];
    }

    /**
     * 生成二维码
     * @param $content //需要写入的内容
     * @return array | page input
     */
    public function createServer($content) {
        $this->_qr = new QrCode($content);
        $this->_qr->setSize($this->_size);
        $this->_qr->setWriterByName(self::WRITE_NAME);
        $this->_qr->setMargin(self::MARGIN);
        $this->_qr->setEncoding($this->_encoding);
        $this->_qr->setErrorCorrectionLevel(new ErrorCorrectionLevel(ErrorCorrectionLevel::MEDIUM));   // 容错率
        $this->_qr->setForegroundColor(self::FOREGROUND_COLOR);
        $this->_qr->setBackgroundColor(self::BACKGROUND_COLOR);
        // 是否需要title
        if ($this->_title) {
            $this->_qr->setLabel($this->_title_content, 36, null, LabelAlignment::CENTER,[5,5,10,10]);
        }
        // 是否需要logo
        if ($this->_logo) {
            $this->_qr->setLogoPath($this->_logo_url);
            $this->_qr->setLogoWidth($this->_logo_size);
        }

        $this->_qr->setValidateResult(false);
        if ($this->_generate == 'display') {
            // 展示二维码
            // 前端调用 例：<img src="http://localhost/qr.php?url=base64_url_string">
            header('Content-Type: ' . $this->_qr->getContentType());
            return $this->_qr->writeString();
        } else if ($this->_generate == 'writefile') {
            //$file_name = $this->_file_name;
            return $this->generateImg();
        } else {
            return ['success' => false, 'message' => 'the generate type not found', 'data' => ''];
        }
    }

    /**
     * 生成文件
     * @param $file_name //目录文件 例: /tmp
     * @return array
     */
    public function generateImg() {
        $file_name = uniqid() . '.' . self::WRITE_NAME;//5f45c15670d96.png

        // 写入文件 www——/mnt/dev/www/cloud_api/src/www/qrcode
        if(!file_exists(WWW.$this->_file_name))
        {
            mkdir (WWW.$this->_file_name,0777,true);
        }

        try {
            $this->_qr->writeFile(WWW.$this->_file_name.'/'.$file_name);
            $data = [
                'url' => $file_name,
                'ext' => self::WRITE_NAME,
            ];
            //dump($data);
            return ['success' => true, 'message' => 'write qrimg success', 'data' => $data];
        } catch (\Exception $e) {
            return ['success' => false, 'message' => $e->getMessage(), 'data' => ''];
        }
    }



}
