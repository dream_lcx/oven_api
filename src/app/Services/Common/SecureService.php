<?php

namespace app\Services\Common;

use app\Services\Common\ConfigService;
use app\Services\Company\CompanyService;
use app\Wechat\WxAuth;

/**
 * 安全服务-文字内容安全,图片安全等
 * Class HttpService
 * @package app\Services\Common\HttpService
 */
class SecureService
{
    /**
     * 微信小程序文字内容检测
     * @param type $content
     * @return type $name 用户端user 工程端engineers
     */
    public static function checkContent($content, $name = 'user', $company_id = 1)
    {
        $company_config = CompanyService::getCompanyConfig($company_id);
        $wx_config = $company_config['wx_config'];
        if ($name != 'user') {//获取用户小程序配置
            $wx_config = $company_config['engineer_wx_config'];
        }
        //$WxAuth = new WxAuth($wx_config['appId'], $wx_config['appSecret']);
        $WxAuth = WxAuth::getInstance($wx_config['appId'], $wx_config['appSecret']);
        $WxAuth->setConfig($wx_config['appId'], $wx_config['appSecret']);

        $res = $WxAuth->imgMsfCheck($content, $name);
        return $res;
    }
}