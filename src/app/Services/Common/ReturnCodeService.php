<?php
namespace app\Services\Common;

/**
 * 返回码定义,自定义的码请以2开头,比如2000
 * Class ConstService
 * @package app\common\service
 */
class ReturnCodeService
{
    //公共
    const SUCCESS = 1000;//成功
    const FAIL = -1000;//失败,
    const ILLEGAL_REQUEST = -1001;//非法请求
    const ILLEGAL_PARAM = -1002;//非法参数
    const NO_DATA = -1003;//无数据
    const NO_LOGIN = -1004;//未登录
    const ERROR_USER_DATA= -1005;//用户信息错误

    //公章部分
    const NO_REALNAME= -1201;//未实名认证,请先实名
    const CREATE_SEAL= -1202;//需要生成印章
    const ERROR_SEAL_DATA= -1203;//公章信息错误



}