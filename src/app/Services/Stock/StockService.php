<?php
namespace app\Services\Stock;

use Server\CoreBase\Controller;

/**
 * 库存服务
 * Class StockService
 * @package app\Services\Stock
 */
class StockService
{
    /**
     * 库存变动
     * @param role int 必选 角色1总后台2运营中心3合伙人4市场推广
     * @param uid int 必选 角色对应id
     * @param change_volume int 必选 变动数量
     * @param equipments_id int 必选 产品id
     * @param company_id int 必选 公司ID
     * @param type int 必选 库存变动类型1增加2减少
     */
    public static function changeStock($role,$uid,$equipments_id,$change_volume,$company_id=1,$type=2) {
        $stockModel = get_instance()->loader->model('StockModel', get_instance());
        //查询库存信息
        $where['role']=$role;
        $where['uid']=$uid;
        $where['equipments_id']=$equipments_id;
        $where['company_id']=$company_id;
        $stock_info=$stockModel->getOne($where);

        $insert_id='';
        if(empty($stock_info)){
            $add_stock=[
                'company_id'=>$company_id,
                'role'=>$role,
                'uid'=>$uid,
                'equipments_id'=>$equipments_id,
                'stock'=>0,
                'surplus'=>0,
                'create_time'=>time(),
            ];
            $insert_id=$stockModel->add($add_stock);
        }
        $stock_id=!empty($stock_info)?$stock_info['stock_id']:$insert_id;

        if($type==2){
            //减少，可以为负
            $edit_stock=[
                'sales_volume'=>$stock_info['sales_volume']+$change_volume,//销量
                'out_volume'=>$stock_info['out_volume']+$change_volume,//出库
                'surplus'=>$stock_info['surplus']-$change_volume,//剩余库存
                'update_time'=>time(),
            ];
            $res=$stockModel->save(['stock_id'=>$stock_id],$edit_stock);
        }else{//增加
            $edit_stock=[
                'stock'=>$stock_info['stock']+$change_volume,//库存
                'surplus'=>$stock_info['surplus']+$change_volume,//剩余库存
                'update_time'=>time(),
            ];
            $res=$stockModel->save(['stock_id'=>$stock_id],$edit_stock);

        }

        return $res;
    }




    /**
     * showdoc
     * @title 新增库存变动记录
     * @param role int 角色1总后台2运营中心3城市合伙人4市场推广
     * @param uid int 角色对应id
     * @param equipments_id int 产品id
     * @param equipments_name string 产品名称
     * @param type int 类别1自增2自减3划拨4下单购买
     * @param change_type int 1增加2减少
     * @param original_stock int 原库存
     * @param change_volume int 变动数量
     * @param now_stock int 现库存
     * @param supplier int 供应商
     * @param buy_price int 进货价
     * @param batch int 批次
     * @param source_role int 来源角色1总后台2运营中心3城市合伙人4市场推广
     * @param source_id int 来源id
     * @param remark int 备注
     * @param stock_time string 入库/出库时间
     * @param create_time string 创建时间
     */

    public static function addStockRecord($role,$uid,$equipments_id,$original_stock,$change_volume,$type=1,$change_type=1,$source_role=1,$source_id=0,$company_id=1,$remark='',$stock_time=0,$supplier='',$buy_price=0,$batch='') {
        $stockRecordModel = get_instance()->loader->model('StockRecordModel', get_instance());
        if($change_type==1){
            //增加
            $now_stock=$original_stock+$change_volume;
        }else{
            $now_stock=$original_stock-$change_volume;
        }
        $add_data=[
            'company_id'=>$company_id,
            'role'=>$role,
            'uid'=>$uid,
            'equipments_id'=>$equipments_id,
            'type'=>$type,//1自增 2自减 3划拨 4下单购买
            'change_type'=>$change_type,
            'original_stock'=>$original_stock,//原库存
            'change_volume'=>$change_volume,//数量
            'now_stock'=>$now_stock,//现库存
            'supplier'=>$supplier,
            'buy_price'=>$buy_price,
            'batch'=>$batch,
            'source_role'=>$source_role,
            'source_id'=>$source_id,
            'remark'=>$remark,
            'stock_time'=>empty($stock_time)?time():$stock_time,//出库时间
            'create_time'=>time(),
        ];
        $res=$stockRecordModel->add($add_data);

        return $res;
    }

























}