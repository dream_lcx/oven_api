<?php
/**
 * Created by PhpStorm.
 * User: bg
 * Date: 2019-04-23
 * Time: 上午 11:27
 */

namespace app\Services\User;

/**
 * 用户逻辑
 * Class UserService
 * @package app\Services\User
 */
class UserService
{


    /**
     * 用户账户变动
     * @param $user_id 用户id
     * @param $money 变动金额,单位元
     * @param $type 变动类型 1:增加 2:减少
     * @param $account_type 变动账户类型 1:可兑换账户 2:冻结账户
     * @param $source 变动类型 1:渠道业绩结算 2:渠道推广员业绩结算 3:分成给渠道推广员 4兑换
     * @param $order_id 关联订单ID，若source=1,2,3时传工单ID
     *
     */
    public function changeAccount($user_id, $money, $source = 1, $order_id = '', $remarks = '', $type = 1, $account_type = 1, $add_time = '')
    {
        if (empty($user_id)) {
            return false;
        }
        if (empty($add_time)) {
            $add_time = time();
        }
        $customerAccountModel = get_instance()->loader->model('CustomerAccountModel', get_instance());
        $customerAccountBillModel = get_instance()->loader->model('CustomerAccountBillModel', get_instance());
        $money = formatMoney($money, 1);//将元转为分
        //判断是否创建了账户
        $account_info = $customerAccountModel->getOne(['user_id' => $user_id], 'id,total_money,available_money,frozen_money');
        if (!empty($account_info)) {
            $id = $account_info['id'];
            $total_money = $account_info['total_money'];
            $available_money = $account_info['available_money'];
            $frozen_money = $account_info['frozen_money'];
        } else {
            //创建账户
            $account_data['user_id'] = $user_id;
            $account_data['total_money'] = 0;
            $account_data['available_money'] = 0;
            $account_data['frozen_money'] = 0;
            $account_data['create_time'] = $add_time;
            $id = $customerAccountModel->add($account_data);

            $total_money = 0;
            $available_money = 0;
            $frozen_money = 0;
        }

        //更新账户余额
        if ($account_type == 1) {
            //可兑换账户
            switch ($type) {
                case 1:
                    $edit_account_data['total_money'] = $total_money + $money;
                    $edit_account_data['available_money'] = $available_money + $money;
                    break;

                case 2:
                    $edit_account_data['available_money'] = $available_money - $money;
                    break;
            }
        } else if ($account_type == 2) {
            //冻结账户
            switch ($type) {
                case 1:
                    $edit_account_data['frozen_money'] = $frozen_money + $money;
                    break;

                case 2:
                    $edit_account_data['frozen_money'] = $frozen_money - $money;
                    break;
            }
        }
        $edit_account_result = $customerAccountModel->save(['id' => $id], $edit_account_data);
        //写入账户记录
        $bill_data['user_id'] = $user_id;
        $bill_data['money'] = $money;
        $bill_data['change_type'] = $type;
        $bill_data['account_type'] = $account_type;
        $bill_data['source'] = $source;
        //$bill_data['work_order_id'] = $order_id;
        $bill_data['remarks'] = $remarks;
        $bill_data['create_time'] = $add_time;
        $add_bill_result = $customerAccountBillModel->add($bill_data);
        return $edit_account_result && $add_bill_result;
    }

    /**
     * 用户与运营中心,城市合伙人绑定关系
     * @param $user_id 用户ID
     * @param $provice_code 省份code,如是根据地址划分则必传
     * @param $city_code 城市code,如是根据地址划分则必传
     * @param $area_code 区域code,如是根据地址划分则必传
     * @param $company_id 所属公司ID
     * @return bool
     */
    public function addUserCenter($user_id, $provice_code, $city_code, $area_code, $company_id)
    {
        $customerAdministrativeCenterModel = get_instance()->loader->model('CustomerAdministrativeCenterModel', get_instance());
        //默认根据安装地址划分区域[用户下单时绑定]
        $info = $this->getOwnArea($user_id, '', $provice_code, $city_code, $area_code, $company_id);
        $data['administrative_id'] = $info['a_id'];
        $data['operation_id'] = $info['operation'];
        $data['user_id'] = $user_id;
        $data['update_time'] = time();
        $has = $customerAdministrativeCenterModel->getOne(array('user_id' => $user_id, 'administrative_id' => $data['administrative_id']), '*');

        if (!empty($has)) {
            return true;
        }
        $res = $customerAdministrativeCenterModel->add($data);
        if ($res) {
            return true;
        }
        return false;
    }

    /**
     * 获取用户所属区域
     * @param $user_id
     * @param $pid
     * @param $provice_code
     * @param $city_code
     * @param $area_code
     * @param int $company_id
     * @return bool|null
     * @throws \Server\CoreBase\SwooleException
     * @throws \Throwable
     */
    public function getOwnArea($user_id, $pid = '', $provice_code = '', $city_code = '', $area_code = '', $company_id = 1)
    {
        //默认:根据地址划分
        $db = get_instance()->loader->mysql("mysqlPool", get_instance());
        $mysql = get_instance()->config['mysql'];
        $prefix = $mysql[$mysql['active']]['prefix'] ?? '';
        $map['province_id'] = $provice_code;
        $map['city_id'] = $city_code;
        $map['area_id'] = $area_code;
        $sql = "select * from " . $prefix . "administrative_info where locate('," . $area_code . ",',area_id) AND locate('," . $city_code . ",',city_id) AND locate('," . $provice_code . ",',province_id) AND company_id=" . $company_id;
        $info = $db->query($sql)->row();
        //若存在区级,直接返回
        if (!empty($info['a_id'])) {
            return $info;
        }
        $info['a_id'] = 0;
        //找市级
        $sql_b = "select o_id from " . $prefix . "operation_info where locate('," . $provice_code . ",',province_id) AND locate('," . $city_code . ",',city_id) AND company_id=" . $company_id;
        $info_b = $db->query($sql_b)->row();
        if (!empty($info_b['o_id'])) {
            $info['operation'] = $info_b['o_id'];
            return $info;
        }
        //找省级
        $sql_c = "select o_id as a_id from " . $prefix. "operation_info where locate('," . $provice_code . ",',province_id) AND company_id=" . $company_id;
        $info_c = $db->query($sql_c)->row();
        if (!empty($info_c)) {
            $info['operation'] = $info_c['a_id'];
            return $info;
        }
        return false;
    }


    public function getGegion($administrative_id, $operation_id){
        $db = get_instance()->loader->mysql("mysqlPool", get_instance());
        $mysql = get_instance()->config['mysql'];
        $prefix = $mysql[$mysql['active']]['prefix'] ?? '';
        $province_name = ''; //省
        $city_name = ''; //市
        $area_name = ''; //区
        //市级
        $province_sql = "select company_name,pid from " . $prefix . "operation_info where o_id=" . $operation_id . ' LIMIT 1';
        $province_data = $db->query($province_sql)->row();
        if (!empty($province_data)){
            $province_name = $province_data['company_name'] ?? '';
            //省级
            if ($province_data['pid'] > 0) {
                $city_sql = "select company_name,pid from " . $prefix . "operation_info where o_id=" . $province_data['pid'] . ' LIMIT 1';
                $city_data = $db->query($city_sql)->row();
                if (!empty($city_data)) $city_name = '-'.$city_data['company_name'];
            }
        }
        //区省
        $area_sql = "select company_name from " . $prefix . "administrative_info where a_id=" . $administrative_id . ' LIMIT 1';
        $area_data = $db->query($area_sql)->row();
        if (!empty($area_data)) $area_name = '-'.$area_data['company_name'];
        $name = $province_name.$city_name.$area_name;
        return $name;
    }


    /*
    * 获取用户的推荐人身份信息
    * @param 必选 $user_id int 用户ID
    * @param 必选 company_id int 公司ID
    * @return_param role int 角色3合伙人4市场推广
    * @return_param uid int 角色对应id
    */
    public function getUserBelongTo($user_id, $company_id = 1)
    {
        $customerModel = get_instance()->loader->model('CustomerModel', get_instance());
        $oaWorkersModel = get_instance()->loader->model('OaWorkersModel', get_instance());
        //查询用户推荐人信息
        $user_info = $customerModel->getOne(['user_id' => $user_id], 'source_is_valid,source');

        $data = [];
        if (!empty($user_info)) {//有推荐人用户推荐人的身份是否是市场推广

            $workers_info = $oaWorkersModel->getOne(['user_id' => $user_info['source'], 'is_delete' => 0], 'workers_id');
            if (!empty($workers_info)) {
                $data['role'] = 4;//市场推广
                $data['uid'] = $workers_info['workers_id'];
                return $data;
            } else {
                //如果推荐人不是市场推广，则查看推荐人属于哪个合伙人下
                $info = $this->getOwnArea($user_info['source'], '', '', '', '', $company_id);
                if (!empty($info)) {
                    $data['role'] = 3;//合伙人
                    $data['uid'] = $info['a_id'];
                    return $data;
                }

            }

        }

        return $data;

    }


}