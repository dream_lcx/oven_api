<?php


namespace app\Services\Todo;


use app\Services\Common\ConfigService;
use app\Services\Common\NotifiyService;
use app\Services\Company\CompanyService;
use app\Services\Log\FileLogService;

class ToDoListService
{
    const MOUDLE_CONFIG = [1 => 'I', 2 => 'W', 3 => 'C'];
    const LABELS_CONFIG = [1=>'M',2=>'H',3=>'E'];
    const TASK = [
        'I_CHECK' => '有一份新的识别表待审核',
        'C_UPLOAD' => '有一份新的合同需要上传',
    ];

    /**
     * 添加待办
     */
    public function add($param, $task_code, $notice_param = [], $notice = true)
    {
        //添加待办
        $toDoListModel = get_instance()->loader->model('ToDoListModel', get_instance());
        $add_data = $param;
        $add_data['sn'] = self::LABELS_CONFIG[$param['label']] . self::MOUDLE_CONFIG[$param['moudle']] . '_' . date('YmdHis') . rand(10, 99);
        $add_data['create_date'] = date('Y-m-d');
        $add_data['create_time'] = time();
        $add_data['status'] = 1;
        $add_data['task'] = self::TASK[$task_code];
        $toDoListModel->add($add_data);
        if ($notice) {
            $this->sendMsg($add_data['sn'], $notice_param['data'], $notice_param['openids'], $notice_param['company_id']);
        }
        return true;

    }

    //发送通知消息
    public function sendMsg($sn, $param, $openids = [], $company_id = 1)
    {
        if (!empty($openids)) {
            foreach ($openids as $key => $val) {
                if (empty($val)) {
                    continue;
                }
                $user_notice_config = ConfigService::getTemplateConfig('wait_handle_order', 'user', 1);
                if (!empty($user_notice_config) && !empty($user_notice_config['template']) && $user_notice_config['template']['switch']) {
                    $template_id = $user_notice_config['template']['wx_template_id'];
                    $mp_template_id = $user_notice_config['template']['mp_template_id'];
                    $template_url = '';
                    $weapp_template_keyword = '';
                    $mp_template_data = [
                        'first' => ['value' => '有新的任务待处理。'],
                        'keyword1' => ['value' => $sn, 'color' => '#4e4747'],
                        'keyword2' => ['value' => $param['remark'], 'color' => '#4e4747'],
                        'remark' => ['value' => '请及时登录后台处理', 'color' => '#173177'],
                    ];

                    $company_config = CompanyService::getCompanyConfig($company_id);
                    $sendTemplateMessage = NotifiyService::sendNotifiy($val, [], $template_id, '', '', $template_url, $weapp_template_keyword, $mp_template_id, $mp_template_data, 'user', $company_config);
                    var_dump($sendTemplateMessage);
                    return $sendTemplateMessage;
                }
            }
        }
        return false;
    }
    //发送审核结果消息
    public function sendCheckResultMsg($title, $status, $remark, $openids = [], $template_url = '', $company_id = 1, $role = 'user',$sms_tel='',$sms_content='')
    {
        if (!empty($openids)) {
            foreach ($openids as $key => $val) {
                if (empty($val)) {
                    continue;
                }
                $user_notice_config = ConfigService::getTemplateConfig('check_result', 'user', 1);
                if (!empty($user_notice_config) && !empty($user_notice_config['template']) && $user_notice_config['template']['switch']) {
                    $template_id = $user_notice_config['template']['wx_template_id'];
                    $mp_template_id = $user_notice_config['template']['mp_template_id'];
                    $weapp_template_keyword = '';
                    $mp_template_data = [
                        'first' => ['value' => '您的审核请求已处理'],
                        'keyword1' => ['value' => $title, 'color' => '#4e4747'],
                        'keyword2' => ['value' => $status, 'color' => '#4e4747'],
                        'keyword3' => ['value' => date('Y年m月d日 H:i', time()), 'color' => '#4e4747'],
                        'remark' => ['value' => $remark, 'color' => '#173177'],
                    ];

                    $company_config = CompanyService::getCompanyConfig($company_id);
                    $sendTemplateMessage = NotifiyService::sendNotifiy($val, [], $template_id, $sms_tel, $sms_content, $template_url, $weapp_template_keyword, $mp_template_id, $mp_template_data, $role, $company_config);
                    var_dump($sendTemplateMessage);
                    return $sendTemplateMessage;
                }
            }
        }
        return false;
    }

    //获取后台某角色的openid集合
    public function getAdminOpenid($role)
    {
        $adminUserModel = get_instance()->loader->model('AdminUserModel', get_instance());
        $join = [
            ['admin_auth_group aag', 'aag.id=rq_admin_user.groupid', 'left']
        ];
        $data = $adminUserModel->getAll(['rq_admin_user.status' => 1, 'rq_admin_user.is_delete' => 0, 'aag.role' => $role], 'notice_openid', $join);
        if (empty($data)) {
            return [];
        }
        $openids = array_column($data, 'notice_openid');

        return $openids;
    }
}