<?php

/**
 * Created by PhpStorm.
 * User: bg
 * Date: 2019-04-24
 * Time: 下午 2:26
 */

namespace app\Services\Device;

use app\Services\Common\HttpService;
use app\Services\Log\AsyncFile;
use Server\CoreBase\SwooleException;

/**
 * 主板操作逻辑
 * Class DeviceService
 * @package app\Services\
 */
class DeviceService
{

    /**
     * 激活主板
     * @desc 1.下发套餐,2.数据数据
     * @param $device_no 必选  string  主板编号
     * @param $package_mode 必选 int 工作模式
     * @param $eq_filter_element_num 必选 array 滤芯值
     * @param $sync_data 必选 array 下发数据(已用天数,剩余天数,剩余天数,剩余流量)
     * @param $contract_id 可选 int 合同ID
     * @return bool
     * @date 2019-04-25
     * @author lcx
     */
    public static function activeDevice($device_no, $package_mode, $eq_filter_element_num, $sync_data, $contract_id = 0)
    {
        $equipmentWaterRecordModel = get_instance()->loader->model('EquipmentWaterRecordModel', get_instance());
        $equipmentListModel = get_instance()->loader->model('EquipmentListsModel', get_instance());
        $contractPackageRecordModel = get_instance()->loader->model('ContractPackageRecordModel', get_instance());
        $redis = get_instance()->loader->redis("redisPool", get_instance());

        if (empty($device_no) || empty($eq_filter_element_num) || empty($sync_data)) {
            throw new SwooleException("参数错误");
        }
        //查询主板信息
        $eq_info = $equipmentListModel->findEquipmentLists(['device_no' => $device_no], 'equipment_id');
        if (empty($eq_info)) {
            throw new SwooleException("主板信息错误");
        }
        try {

            //下发-绑定套餐
            $params = ['sn' => $device_no, 'working_mode' => $package_mode, 'filter_element_max' => $eq_filter_element_num];
            $path = '/House/Issue/bindingPackage';
            HttpService::Thrash($params, $path);
            sleepCoroutine(2000);
            //下发--数据同步
            $data_sync_params = $sync_data;
            $data_sync_params['sn'] = $device_no;
            //如果是时长模式并且设置已用流量为0(大于0表示强制设置),统一计算已用流量值
            if ($package_mode == 1 && empty($data_sync_params['used_traffic'])) {
                $equipment_water_record_sum = $equipmentWaterRecordModel->getOne(['equipment_id' => $eq_info['equipment_id']], 'sum(water) total_water');  // 总用水量
                $data_sync_params['used_traffic'] = $equipment_water_record_sum['total_water'] ?? 0; //已用流量
            }
            $data_sync_path = '/House/Issue/data_sync';
            HttpService::Thrash($data_sync_params, $data_sync_path);

            sleepCoroutine(2000);


            //请求心跳
            $state_params['sn'] = $device_no;
            $state_path = '/House/Issue/heartbeat';
            HttpService::Thrash($state_params, $state_path);

            //保存主板数据
            $equipmentListModel->updateEquipmentLists(array('remaining_traffic' => $data_sync_params['remaining_traffic'], 'remaining_days' => $data_sync_params['remaining_days'], 'used_traffic' => $data_sync_params['used_traffic'], 'used_days' => $data_sync_params['used_days']), array('equipment_id' => $eq_info['equipment_id']));

            //保存套餐信息
            $package_record['contract_id'] = $contract_id;
            $package_record['equipment_id'] = $eq_info['equipment_id'];
            $package_record['used_days'] = $data_sync_params['used_days'];
            $package_record['remaining_days'] = $data_sync_params['remaining_days'];
            $package_record['used_traffic'] = $data_sync_params['used_traffic'];
            $package_record['remaining_traffic'] = $data_sync_params['remaining_traffic'];
            $package_record['working_mode'] = $package_mode;
            $package_record['filter_element_max'] = json_encode($eq_filter_element_num);
            $package_record['hand_time'] = time();
            $package_record['last_hand_time'] = time();
            $package_record['status'] = 1;
            $contractPackageRecordModel->add($package_record);

            //将套餐信息放入Redis中，实时监测套餐是否下发成功
            $PlanToMonitor = $redis->get('PlanToMonitor');
            if ($PlanToMonitor) {
                $PlanToMonitor = json_decode($PlanToMonitor, 1);
            } else {
                $PlanToMonitor = [];
            }
            array_push($PlanToMonitor, $package_record);
            $redis->set('PlanToMonitor', json_encode($PlanToMonitor));
        } catch (SwooleException $ex) {
            dump($ex->getMessage());
        }
    }

    //激活购买主板--保存主板配件滤芯信息并下发套餐
    public static function activeBuyDevice($device_no, $equipments_id)
    {
        $equipmentListModel = get_instance()->loader->model('EquipmentListsModel', get_instance());
        $equipmentModel = get_instance()->loader->model('EquipmentModel', get_instance());
        $eqPartModel = get_instance()->loader->model('EquipmentsPartsModel', get_instance());
        $bindModel = get_instance()->loader->model('CustomerBindEquipmentModel', get_instance());
        $eq_info = $equipmentModel->getDetail(['equipments_id' => $equipments_id], 'original_parts,filter_element');
        $eq_list_info = $equipmentListModel->findEquipmentLists(['device_no' => $device_no], 'equipment_id');
        $eq_part_lists = array();
        $eq_filter_element = array();
        $eq_filter_element_num = array();
        $db_eq_filter_element_num = []; //主板表滤芯值
        $original_parts = array();
        if (!empty($eq_info)) {
            //原始配件
            if (!empty($eq_info['original_parts'])) {
                $original_parts = json_decode($eq_info['original_parts'], true);
                if (!empty($original_parts)) {
                    foreach ($original_parts as $kk => $vv) {
                        $arr['equipment_id'] = $eq_list_info['equipment_id'];
                        $arr['parts_id'] = $vv['parts_id'];
                        $arr['is_original'] = 1;
                        $arr['create_time'] = time();
                        $arr['cycle'] = $vv['cycle'];
                        $arr['last_replace_time'] = time();
                        $arr['expire_time'] = $arr['last_replace_time'] + $arr['cycle'] * 86400;
                        $arr['parts_number'] = 1;
                        $eq_part_lists[] = $arr;
                    }
                }
            }
            //滤芯
            if (!empty($eq_info['filter_element'])) {
                $filter_element = json_decode($eq_info['filter_element'], true);
                if (!empty($filter_element)) {
                    foreach ($filter_element as $kk => $vv) {
                        $arr2['equipment_id'] = $eq_list_info['equipment_id'];
                        $arr2['parts_id'] = $vv['parts_id'];
                        $arr2['is_filter'] = 1;
                        $arr2['create_time'] = time();
                        $arr2['cycle'] = $vv['cycle'];
                        $arr2['last_replace_time'] = time();
                        $arr2['expire_time'] = $arr2['last_replace_time'] + $arr2['cycle'] * 86400;
                        $arr2['parts_number'] = 1;
                        $arr2['filter_level'] = $kk + 1;
                        $eq_filter_element[] = $arr2;
                        array_push($eq_filter_element_num, $vv['cycle']);
                        $db_arr['residual_value'] = $vv['cycle'];
                        $db_arr['max'] = $vv['cycle'];
                        array_push($db_eq_filter_element_num, $db_arr);
                    }
                }
                //处理滤芯5级
                $all_len = count($eq_filter_element_num);
                if ($all_len < 5) {
                    $len = 5 - $all_len;
                    for ($i = 0; $i < $len; $i++) {
                        array_push($eq_filter_element_num, 1000);
                        $db_arr['residual_value'] = 1000;
                        $db_arr['max'] = 1000;
                        array_push($db_eq_filter_element_num, $db_arr);
                    }
                } else if ($all_len > 5) {
                    $eq_filter_element_num = array_slice($eq_filter_element_num, 0, 5);
                    $db_eq_filter_element_num = array_slice($db_eq_filter_element_num, 0, 5);
                }
            }
        }
        //主板配件表
        $eqPartModel->del(['equipment_id' => $eq_list_info['equipment_id']]); //删除当前主板配件信息
        if (!empty($eq_part_lists)) {
            foreach ($eq_part_lists as $k => $v) {
                $eqPartModel->add($v);
            }
        }
        if (!empty($eq_filter_element)) {
            foreach ($eq_filter_element as $k => $v) {
                $eqPartModel->add($v);
            }
        }
        $equipmentListModel->updateEquipmentLists(['start_time' => time(), 'end_time' => time() + 100 * 86400, 'working_mode' => 1], ['equipment_id' => $eq_list_info['equipment_id']]);
        /*		 * ****************************修改主板租赁状态****************** */
        $bindModel->save(['equipment_id' => $eq_list_info['equipment_id'], 'is_owner' => 2], ['status' => 1]);
        $sync_data['used_days'] = 0;
        $sync_data['used_traffic'] = 0;
        $sync_data['remaining_days'] = 999999999;
        $sync_data['remaining_traffic'] = 999999999;

        DeviceService::activeDevice($device_no, 1, $eq_filter_element_num, $sync_data);
    }

    public static function syncFilterData($device_no)
    {
        $redis = get_instance()->loader->redis("redisPool", get_instance());
        if (empty($device_no)) {
            return false;
        }
        //如果确实到期直接返回
        $residual_value = $redis->hget('cloud_device_filter_element_true', $device_no);
        if ($residual_value < 0) {
            return false;
        }
        unset($residual_value);
        //避免每次查询数据库,首选判断redis是否存在;如果欠费和滤芯待复位同时存在,将过期的滤芯值设为固定值2
        $redis_data = $redis->hget('cloud_device_filter_element', $device_no);
        if (!empty($redis_data)) {
            $redis_data = json_decode($redis_data, true);
            if ($redis_data[0]['date'] == date('Y-m-d')) {//每天只计算一次
//                if (date('i') % 5 != 0) {//每5分钟执行一次
//                    return false;
//                }
                $redis_arr = [];
                foreach ($redis_data as $k => $v) {
                    $path = '/House/Issue/filter_element_reset_modification';
                    $params = $v;
                    if ($params['is_exipre'] == 1 && $params['value'] < 0) {
                        $params['value'] = 30;
                    }
                    unset($params['date']);
                    unset($params['is_exipre']);
                    HttpService::Thrash($params, $path);
                    sleepCoroutine(600);
                    //请求心跳
                    $redis->del('device_heartbeat:' . $device_no);//删除redis心跳
                    $state_params['sn'] = $device_no;
                    $state_path = '/House/Issue/heartbeat';
                    HttpService::Thrash($state_params, $state_path);
                    sleepCoroutine(2000);
                    $redis_arr[] = $params;
                }
                unset($redis_arr);
                return true;
            }
        }
        //查询主板是否到期
        $achievementModel = get_instance()->loader->model('AchievementModel', get_instance());
        $where = [
            'device_no' => $device_no
        ];
        $join = [
            ['contract', 'rq_equipment_lists.contract_id = rq_contract.contract_id', 'LEFT']
        ];
        $achievementModel->table = 'equipment_lists';
        $result = $achievementModel->findJoinData($where, 'exire_date,effect_time', $join);
        $is_exipre = 0;
        if (!empty($result) && $result['exire_date'] < time()) {//已经到期
            $is_exipre = 1;
        }
        unset($where);
        $map['device_no'] = $device_no;
        $equipmentListModel = get_instance()->loader->model('EquipmentListsModel', get_instance());
        $partsModel = get_instance()->loader->model('PartsModel', get_instance());
        $device_info = $equipmentListModel->findEquipmentLists($map, 'equipment_id,device_no');
        if (empty($device_info)) {
            return false;
        }
        $join = [
            ['equipments_parts ep', 'ep.parts_id = rq_parts.parts_id', 'left'],  // 主板配件表 -- 配件表
        ];
        $field = 'ep.id,rq_parts.parts_id,rq_parts.parts_name,ep.cycle,ep.expire_time';
        // 获取主板滤芯
        $where['ep.equipment_id'] = $device_info['equipment_id'];
        $where['ep.is_filter'] = 1;  // 1表示滤芯
        $where['ep.is_delete'] = 0;  // 0表示未删除
        $equipments_parts = $partsModel->getJoinAll($where, $field, $join);
        unset($where);
        if ($equipments_parts) {
            $redis_arr = [];
            foreach ($equipments_parts as $kk => &$vv) {
                $residual_value = floor(($vv['expire_time'] - time()) / 86400); // 剩余天数，当前仅根据时间计算，算法待定
                $params = ['sn' => $device_no, 'type' => 2, 'key' => $kk + 1, 'value' => $residual_value + 1];
                if ($residual_value < 0) {
                    if ($is_exipre == 1) {
                        $params['value'] = 30;
                    } else {
                        $redis->hset('cloud_device_filter_element_true', $device_no, $residual_value);//保存确实到期了的
                        $redis->expire('cloud_device_filter_element_true', 86400);
                        break;
                    }
                }
                $path = '/House/Issue/filter_element_reset_modification';
                HttpService::Thrash($params, $path);
                sleepCoroutine(600);
                //请求心跳
                $redis->del('device_heartbeat:' . $device_no);//删除redis心跳
                $state_params['sn'] = $device_no;
                $state_path = '/House/Issue/heartbeat';
                HttpService::Thrash($state_params, $state_path);
                sleepCoroutine(2000);
                $params['date'] = date('Y-m-d');
                $params['is_exipre'] = $is_exipre;
                $redis_arr[] = $params;
            }
            //存入redis下一次直接读取
            if (!empty($redis_arr)) {
                AsyncFile::write('machineStateSyncFilter', date('Y-m-d H:i:s', time()) . '-' . $device_no . ':同步内容db' . json_encode($redis_arr));
                $redis->hset('cloud_device_filter_element', $device_no, json_encode($redis_arr));
                $redis->expire('cloud_device_filter_element', 86400);
            }
            unset($redis_arr);
        }


    }


    //生成非智能主板，主板编号
    public static function createNonEqNo()
    {
        $equipmentListModel = get_instance()->loader->model('EquipmentListsModel', get_instance());
        $str = date('YmdHis') . rand(10, 99);
        $device_no = 'yho' . $str;
        $checkNumber = $equipmentListModel->findEquipmentLists(['device_no' => $device_no]);
        if (!$checkNumber) {
            return $device_no;
        }
        DeviceService::createNonEqNo();
    }


    //非智能主板新装工单生成时绑定
    public static function bindNonEquipment($work_order_id, $user_id, $equipments_id)
    {
        if (empty($work_order_id)) {
            return false;
        }
        $workOrderModel = get_instance()->loader->model('WorkOrderModel', get_instance());
        $workOrderBusinessModel = get_instance()->loader->model('WorkOrderBusinessModel', get_instance());
        $equipmentModel = get_instance()->loader->model('EquipmentModel', get_instance());
        $orderModel = get_instance()->loader->model('OrderModel', get_instance());
        $equipmentListsModel = get_instance()->loader->model('EquipmentListsModel', get_instance());
        $customerBindEquipmentModel = get_instance()->loader->model('CustomerBindEquipmentModel', get_instance());
        $contractEquipmentModel = get_instance()->loader->model('ContractEquipmentModel', get_instance());
        $contractModel = get_instance()->loader->model('ContractModel', get_instance());
        $workEquipmentModel = get_instance()->loader->model('WorkEquipmentModel', get_instance());
        $contractRankModel = get_instance()->loader->model('ContractRankModel', get_instance());

        //查询工单信息
        $wo_map['work_order_id'] = $work_order_id;
        $work_order_info = $workOrderModel->getWorkDetail($wo_map, array(), '*');
        if (empty($work_order_info)) {
            return false;
        }
        //如果非新装不进行绑定主板
        $business = $workOrderBusinessModel->getOne(['work_order_id' => $work_order_info['work_order_id'], 'business_id' => 3], 'work_order_business_id');
        if (empty($business)) {
            return true;
        }
        $warranty_time['start'] = '';
        $warranty_time['end'] = '';
        //查询新产品信息
        $eq_info = $equipmentModel->getDetail(array('equipments_id' => $equipments_id), 'type,equipments_name');
        //如果新产品不是非智能主板,不走绑定流程
        if ($eq_info['type'] != 2) {
            return true;
        }
        $device_no = DeviceService::createNonEqNo(); //生成主板编号

        //如果是后台添加的工单,没有订单数据
        if (!empty($work_order_info['order_id'])) {
            //判断该用户是否租赁该类型主板
            $or_map['equipments_id'] = $work_order_info['equipments_id'];
            $or_map['user_id'] = $work_order_info['user_id'];
            $or_map['order_id'] = $work_order_info['order_id'];
            $order = $orderModel->getOne($or_map, 'warranty_start_time,warranty_end_time,opertaor,pay_status');
            if (!empty($order) && $order['opertaor'] == 1 && $order['pay_status'] != 2) {
                //return $this->jsonend(-1103, "该用户订单未完成支付,不能绑定");
                return false;
            }
            if ($eq_info['type'] == 2) {
                $warranty_time['start'] = $order['warranty_start_time'];
                $warranty_time['end'] = $order['warranty_end_time'];
            }
        }

        //判断主板是否已经存在
        $equipment_info = $equipmentListsModel->findEquipmentLists(array('device_no' => $device_no), 'equipment_id');

        if (!empty($equipment_info)) {
            //判断用户是否重复绑定主板
            $bd_map['equipment_id'] = $equipment_info['equipment_id'];
            $bd_map['is_owner'] = 2;
            $bind_info = $customerBindEquipmentModel->getOne($bd_map, '*');
            if (!empty($bind_info)) {
                //return $this->jsonend(-1104, "该主板已被绑定过，不能重复绑定");
                return false;
            }
        }
        //判断用户是否已经绑定完
        $bind_map['equipments_id'] = $work_order_info['equipments_id'];
        $bind_map['is_owner'] = 2;
        $bind_map['user_id'] = $work_order_info['user_id'];
        $bind_map['work_order_id'] = $work_order_id;
        $bind_list = $customerBindEquipmentModel->getAll($bind_map, '*');
        if (!empty($bind_list) && count($bind_list) >= $work_order_info['equipment_num']) {
            //return $this->jsonend(-1105, '该用户租赁该类型主板' . $work_order_info['equipment_num'] . '台，已绑定' . count($bind_list) . '台');
            return false;
        }

        //非智能主板所有状态正常
        $eq_data['status'] = 2;
        $eq_data['device_status'] = 0;
        $eq_data['working_mode'] = 1;

        $eq_data['province'] = $work_order_info['province'];
        $eq_data['city'] = $work_order_info['city'];
        $eq_data['area'] = $work_order_info['area'];
        $eq_data['province_code'] = $work_order_info['province_code'];
        $eq_data['city_code'] = $work_order_info['city_code'];
        $eq_data['area_code'] = $work_order_info['area_code'];
        $eq_data['address'] = $work_order_info['service_address'];
        $eq_data['lng'] = $work_order_info['lng'];
        $eq_data['lat'] = $work_order_info['lat'];
        $eq_data['contact_number'] = $work_order_info['contact_number'];
        $eq_data['contact'] = $work_order_info['contacts'];
        $eq_data['company_id'] = $work_order_info['company_id'];

        //查询工单主板关联表
        $work_eq = $workEquipmentModel->getOne(['work_order_id' => $work_order_id], 'work_order_id,equipment_id,customer_code');
        if (empty($work_eq)) {
            //return $this->jsonend(-1003, '工单主板信息错误');
            return false;
        }

        if (empty($equipment_info)) {
            //新增主板记录
            $eq_data['equipments_id'] = $work_order_info['equipments_id'];
            $eq_data['device_no'] = $device_no;
            $eq_data['create_time'] = time();
            $eq_data['status'] = 1;
            $eq_data['customer_code'] = $work_eq['customer_code'];

            $equipment_id = $equipmentListsModel->insertEquipmentLists($eq_data);
        } else {
            //修改主板信息
            $equipment_id = $equipment_info['equipment_id'];
            $eq_data['equipments_id'] = $work_order_info['equipments_id'];
            $eq_data['customer_code'] = $work_eq['customer_code'];
            $equipmentListsModel->updateEquipmentLists($eq_data, array('equipment_id' => $equipment_id));
        }
        //添加绑定记录
        $bind_data['equipments_id'] = $work_order_info['equipments_id'];
        $bind_data['work_order_id'] = $work_order_id;
        $bind_data['equipment_id'] = $equipment_id;
        $bind_data['user_id'] = $work_order_info['user_id'];
        $bind_data['create_time'] = time();
        $bind_data['is_owner'] = 2;
        $bind_data['status'] = 1;
        $bind_data['customer_code'] = $work_eq['customer_code'];

        $customerBindEquipmentModel->addCustomerBindEquipment($bind_data);

        //添加合同主板关联记录
        if (!empty($work_order_info['contract_id'])) {
            $contract_eq_data['contract_id'] = $work_order_info['contract_id'];
            $contract_eq_data['equipment_id'] = $equipment_id;
            $contract_eq_data['addtime'] = time();
            $contractEquipmentModel->add($contract_eq_data);
        }

        //修改工单主板信息
        $workEquipmentModel->save(['work_order_id' => $work_eq['work_order_id']], ['equipment_id' => $equipment_id, 'customer_code' => $work_eq['customer_code']]);

        //已判定完修改相应状态
        $bind_map['equipments_id'] = $work_order_info['equipments_id'];
        $bind_map['user_id'] = $work_order_info['user_id'];
        $bind_map['is_owner'] = 2;
        $bind_map['work_order_id'] = $work_order_id;
        $bind_lists = $customerBindEquipmentModel->getAll($bind_map, '*');

        if (count($bind_lists) >= $work_order_info['equipment_num']) {
            $workOrderModel->editWork(array('work_order_id' => $work_order_id), array('is_bind_equipment' => 1));
            //合同状态
            //修改合同状态
            $contractModel->save(['contract_id' => $work_order_info['contract_id']], ['status' => 4, 'is_effect' => 2, 'effect_time' => time(), 'sign_date' => time(), 'installed_time' => time()]);
            //添加合同日志
            DeviceService::addContractLog($work_order_info['contract_id'], $user_id, 4, "非智能产品已上门,合同状态修改为生效中");

            //新增----------2019年1月22日11:00:46----------------ligang
            //绑定主板完成后，更改合同关系表主板数
            $contract = $contractModel->getOne(['contract_id' => $work_order_info['contract_id']], 'contract_no');
            $contract_rank = $contractRankModel->getOne(['contract_no' => $contract['contract_no']], 'id');
            $contractRankModel->save(['id' => $contract_rank['id']], ['device_number' => $work_order_info['equipment_num']]);
        }
        return true;

    }


    /**
     * @desc   添加合同操作日志
     * @param $status 1增2删3改4解绑'
     * @date   2018-07-19
     * @author lcx
     * @return [type]     [description]
     */
    public static function addContractLog($contract_id, $user_id, $status = 1, $remark = '')
    {
        $contractLogModel = get_instance()->loader->model('ContractLogModel', get_instance());
        $data['contract_id'] = $contract_id;
        $data['do_id'] = $user_id;
        $data['do_type'] = $status;
        $data['terminal_type'] = 3;
        $data['do_time'] = time();
        $data['remark'] = $remark;
        $res = $contractLogModel->add($data);
        return $res;
    }

    /**给设备发送指令
     * @param array $params 参数 ['sn'=>$equipment_number,'osn'=>$order_id,'money'=>$money]
     * @param string $command
     * @param $equipment_number
     * @return bool
     */
    public static function Thrash(array $params, string $instruction)
    {

        if (empty($instruction)) {
            return false;
        }
        if (empty($params)) {
            return false;
        }
        $path = '';
        switch ($instruction) {
            case 'binding_package'://绑定套餐
                $path = '/Equipment/Send/binding_package';
                break;
            case 'set_qrcode'://下发设备二维码
                $path = '/Equipment/Send/set_qrcode';
                break;
            case 'switch_control'://开关机
                $path = '/Equipment/Send/switch_control';
                break;
            case 'iccid'://请求ICCID
                $path = '/Equipment/Send/iccid';
                break;
            case 'read_running_time'://读取上电运行总时间
                $path = '/Equipment/Send/read_running_time';
                break;
            case 'clear_running_time'://清零上电总运行时间
                $path = '/Equipment/Send/clear_running_time';
                break;
        }
        if (empty($path)) {
            return true;
        }
        $GetIPAddressHttpClient = get_instance()->getAsynPool('EquipmentApi');
        $json = json_encode($params);
        $response = $GetIPAddressHttpClient->httpClient
            ->setData($json)
            ->setMethod('post')
            ->coroutineExecute($path);
        if ($response['statusCode'] == 200) {
            $body = json_decode($response['body'], true);
            return $body;
        } else {
            return ['code' => -1000, 'message' => '发送失败', 'data' => []];
        }
    }


}
