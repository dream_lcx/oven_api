<?php

namespace app\Services\Company;

use app\Services\Common\HttpService;

/**
 * 配置服务
 * Class HttpService
 * @package app\Services\Common\HttpService
 */
class CompanyService
{
    /**
     * 根据ID获取编号，根据编号获取ID
     * @param $company_id
     * @param string $company_no
     */
    public function getIdOrNo($company_id, $company_no = '')
    {
        if (empty($company_id) && empty($company_no)) {
            return false;
        }
        $where = [];
        if (!empty($company_id)) {
            $where['company_id'] = $company_id;
        }
        if (!empty($company_no)) {
            $where['company_no'] = $company_no;
        }
//        $company_info = HttpService::requestBossApi($where, '/api/Company/getCompany');
//        if (!empty($company_info) && $company_info['code'] == 1000) {
//            return $company_info['data']['company_no'] ?? false;
//        }
        $companyModel = get_instance()->loader->model('CompanyModel', get_instance());
        $company_info = $companyModel->getOne($where);
        if (empty($company_info)) {
            return false;
        }
        $company_info['company_seal'] = empty($company_info['company_seal']) ? '' : get_instance()->config->get('qiniu.qiniu_url') . $company_info['company_seal'];
        return $company_info;
    }

    /**
     * 获取公司基本信息
     * @param $company_id
     * @return bool|mixed
     */
    public static function getCompanyInfo($company_id)
    {
        if (empty($company_id)) {
            return false;
        }
        //请求boss后台接口获取入驻商信息
        $redis = get_instance()->loader->redis("redisPool", get_instance());
//        $redis_data = $redis->hGet('cloud_company_info_data',$company_id);
//        if (!empty($redis_data)) {
//            return json_decode($redis_data, true);
//        }
//        $company_info = HttpService::requestBossApi(['company_id' => $company_id], '/api/Company/getCompany');
//        if (!empty($company_info) && $company_info['code'] == 1000) {
//            $redis->hSet('cloud_company_info_data', $company_id, json_encode($company_info['data']));
//            return $company_info['data'];
//        }
        $where['company_id'] = $company_id;
        $companyModel = get_instance()->loader->model('CompanyModel', get_instance());
        $company_info = $companyModel->getOne($where);
        if (empty($company_info)) {
            return false;
        }
        $company_info['company_seal'] = empty($company_info['company_seal']) ? '' : get_instance()->config->get('qiniu.qiniu_url') . $company_info['company_seal'];
        return $company_info;
    }

    /**
     * 获取公司配置
     * @param $company_id
     * @return bool|mixed
     */
    public static function getCompanyConfig($company_id)
    {
        if (empty($company_id)) {
            return false;
        }
        //请求boss后台接口获取入驻商信息
        $redis = get_instance()->loader->redis("redisPool", get_instance());
        $redis_data = $redis->hGet('oven_company_config_data', $company_id);
//        if (!empty($redis_data)) {
//            return json_decode($redis_data, true);
//        }
//        $company_info = HttpService::requestBossApi(['company_id' => $company_id], '/api/Company/getCompayConfig');
//        if (!empty($company_info) && $company_info['code'] == 1) {
//            $redis->hSet('oven_company_config_data', $company_id, json_encode($company_info['data']));
//            return $company_info['data'];
//        }
        $where['company_id'] = $company_id;
        $companyConfigModel = get_instance()->loader->model('CompanyConfigModel', get_instance());
        $data = $companyConfigModel->getOne($where);
        if (empty($data)) {
            return false;
        }
        $data['link_config'] = empty($data['link_config']) ? '' : json_decode($data['link_config'], true);
        $img_config = empty($data['img_config']) ? '' : json_decode($data['img_config'], true);
        $data['mp_config'] = empty($data['mp_config']) ? '' : json_decode($data['mp_config'], true);
        $data['wx_config'] = empty($data['wx_config']) ? '' : json_decode($data['wx_config'], true);
        $data['spread_wx_config'] = empty($data['spread_wx_config']) ? '' : json_decode($data['spread_wx_config'], true);
        $data['esign_config'] = empty($data['esign_config']) ? '' : json_decode($data['esign_config'], true);
        $data['sms_config'] = empty($data['sms_config']) ? '' : json_decode($data['sms_config'], true);
        $data['engineer_wx_config'] = empty($data['engineer_wx_config']) ? '' : json_decode($data['engineer_wx_config'], true);
        $data['pay_config'] = empty($data['pay_config']) ? '' : json_decode($data['pay_config'], true);
        $img_config['admin_image'] = empty($img_config['admin_image']) ? '' : uploadImgPath($img_config['admin_image']);
        $img_config['image'] = empty($img_config['image']) ? '' : uploadImgPath($img_config['image']);
        $data['img_config'] = $img_config;

        return $data;

    }

}
