<?php
/**
 * Created by PhpStorm.
 * User: LG
 * Date: 2019/7/1
 * Time: 17:36
 */

namespace app\Services\Log;


use think\image\Exception;

class Redis
{
    static $redis;
    static $redis_pool = [];

    public static function init()
    {
        self::connect();
    }

    protected static function connect()
    {
        $config = get_instance()->config->get('redis', null);
        if (empty($config)) {
            throw new Exception('缺少redis配置');
        }
        $config = $config[$config['active']];
        $cli = new \Redis();
        $conn = $cli->pconnect($config['ip'], $config['port']);
        if ($conn == false) {
            throw new Exception('redis连接失败，失败原因：' . $cli->getLastError());
        }
        $auth = $cli->auth($config['password']);
        if ($auth == false) {
            throw new Exception('redis密码错误');
        }
        $cli->select($config['select']);

        self::$redis = $cli;
        return self::$redis;
    }
}