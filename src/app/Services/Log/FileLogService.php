<?php


namespace app\Services\Log;


use app\Library\SLog;

class FileLogService
{
    /**
     * 写入日志文件
     * @param string $sn 主板编号
     * @param array $data 日志内容
     * @param string $class 日志种类
     * @date 2018/8/30 17:14
     * @author ligang
     */
    public static function WriteLog(string $class = SLog::DEVICE_HEARTBEAT_LOG, string $sn, $data,$flag=false)
    {
        $debug_log = get_instance()->config->get('debug_log');
        if ($debug_log || $flag) {
            SLog::SL($class, $sn)->info(__FUNCTION__, [$sn, $data]);
        }
    }
}