<?php
/**
 * Created by PhpStorm.
 * User: LG
 * Date: 2019/6/27
 * Time: 13:57
 */

namespace app\Services\Log;


class Memcached
{
    static $Memcached;
    static private $instance;

    static public function init()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function __construct()
    {
        self::$Memcached = new \Memcached('Memcached');
        if (empty($memcached_pool)){
            $config = get_instance()->config->get('memcached');
            self::$Memcached->addServers($config);
        }
        self::$Memcached->setOption(self::$Memcached::OPT_LIBKETAMA_COMPATIBLE,true);
        return self::$Memcached;
    }

    public function add ($key, $value, $expiration = 0) {
        return self::$Memcached->add($key, $value, $expiration);
    }

}