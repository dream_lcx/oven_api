<?php

namespace app\Services\Contract;

use app\Services\Common\HttpService;

/**
 * 合同服务
 * Class ContractService
 * @package app\Services\Common\ContractService
 */
class ContractService
{
    /**
     * 获取合同模板-
     */
    public static function getTemplate($company_id, $business_id, $contrct_class, $o_id = 0)
    {
        $contractTemplateModel = get_instance()->loader->model('ContractTemplateModel', get_instance());
        //1同公司同服务同运营中心
        $temp_map['company_id'] = $company_id;
        $temp_map['business_id'] = $business_id;
        $temp_map['contract_class'] = $contrct_class;
        $temp_map['o_id'] = $o_id;
        $temp_info = $contractTemplateModel->getOne($temp_map, '*');
        if (!empty($temp_info)) {
            return $temp_info;
        }
        //1同公司同服务运营中心通用
        $temp_map['o_id'] = 0;
        $temp_info = $contractTemplateModel->getOne($temp_map, '*');
        if (!empty($temp_info)) {
            return $temp_info;
        }
        //2同公司所有服务通用
        $temp_map['business_id'] = 0;
        $temp_info2 = $contractTemplateModel->getOne($temp_map, '*');
        if (!empty($temp_info2)) {
            return $temp_info2;
        }
        // 3 所有公司同服务
        $temp_map['company_id'] = 0;
        $temp_map['business_id'] = $business_id;
        $temp_info3 = $contractTemplateModel->getOne($temp_map, '*');
        if (!empty($temp_info3)) {
            return $temp_info3;
        }
        // 4 所有公司所有服务
        $temp_map['company_id'] = 0;
        $temp_map['business_id'] = 0;
        $temp_info4 = $contractTemplateModel->getOne($temp_map, '*');
        if (!empty($temp_info4)) {
            return $temp_info4;
        }
        return false;
    }
}
