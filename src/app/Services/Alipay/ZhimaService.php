<?php


namespace app\Services\Alipay;

use app\Library\Alipay\aop\request\AlipaySystemOauthTokenRequest;
use app\Library\Alipay\aop\AopClient;
use app\Library\Alipay\aop\request\AlipayUserInfoAuthRequest;
use app\Library\Alipay\aop\request\ZhimaCreditScoreGetRequest;
use app\Library\Alipay\aop\request\AlipayUserCertifyOpenInitializeRequest;

class ZhimaService
{
    /**
     *  芝麻认证初始化
     * @date 2019-02-22 上午 11:21
     */
    public static function zhimaInitialize($name, $card_id) {
        $aop = new AopClient ();
        $alipay = get_instance()->config->get('alipay');
        $aop->gatewayUrl = $alipay['gatewayUrl'];
        $aop->appId = $alipay['appId'];
        $aop->rsaPrivateKey = $alipay['rsaPrivateKey'];
        $aop->alipayrsaPublicKey = $alipay['alipayrsaPublicKey'];
        $aop->apiVersion = '1.0';
        $aop->signType = 'RSA2';
        $aop->postCharset = 'UTF-8';
        $aop->format = 'json';
        $request = new \ZhimaCustomerCertificationInitializeRequest ();
        $param['transaction_id'] = 'YHYZ' . date('YmdHis', time()) . rand(1000, 9999);
        $param['product_code'] = 'w1010100000000002978';
        $param['biz_code'] = "FACE";
        $param['identity_param'] = [
            "identity_type" => "CERT_INFO",
            "cert_type" => "IDENTITY_CARD",
            "cert_name" => $name,
            "cert_no" => $card_id
        ];
        $request->setBizContent(json_encode($param));
        $result = $aop->execute($request);
        $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
        $ret = json_decode(json_encode($result), true);
        $retData = $ret['zhima_customer_certification_initialize_response'];
        return $retData;
    }

}