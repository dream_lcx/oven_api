<?php

namespace app;

use app\Wechat\wxcrypt\WXBizDataCrypt;
use Endroid\QrCode\QrCode;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Qiniu\Auth;
use Qiniu\Storage\UploadManager;
use Server\Asyn\HttpClient\HttpClientPool;
use Server\Asyn\Redis\RedisAsynPool;
use Server\CoreBase\HttpInput;
use Server\CoreBase\Loader;
use Server\SwooleDistributedServer;
use Server\Components\Process\ProcessManager;
use app\Process\SimProcess;
use Server\Asyn\Mysql\MysqlAsynPool;
use app\Services\Common\ConfigService;
use tech\core\eSign;

/**
 * Created by PhpStorm.
 * User: zhangjincheng
 * Date: 16-9-19
 * Time: 下午2:36
 */
class AppServer extends SwooleDistributedServer
{
    public $Memcached;
    public $tmp_table;
    public $Spreadsheet;
    public $QrCode;
    public $QiniuAuth;
    public $QiniuUploadManager;
    public $WXBizDataCrypt;
    public $eSign;

    /**
     * 可以在这里自定义Loader，但必须是ILoader接口
     * AppServer constructor.
     */
    public function __construct()
    {
        $this->setLoader(new Loader());
        parent::__construct();
    }


    /**
     * 开服初始化(支持协程)
     * @return mixed
     */
    public function onOpenServiceInitialization()
    {
        parent::onOpenServiceInitialization();
    }

    /**
     * 这里可以进行额外的异步连接池，比如另一组redis/mysql连接
     * @param $workerId
     * @return void
     * @throws \Server\CoreBase\SwooleException
     * @throws \Exception
     */
    public function initAsynPools($workerId)
    {
        parent::initAsynPools($workerId);
        //开发者模式
        $dev_mode = ConfigService::getConfig('develop_mode');
        //获取微信配置
        if ($dev_mode == 1) {
            $callback_domain_name = $this->config->get('debug_config.equipment_api_url');

        } else {
            $callback_domain_name = $this->config->get('equipment_api_url');

        }
        $this->addAsynPool('EquipmentApi', new HttpClientPool($this->config, $callback_domain_name));

        //增加redis备用连接池
        $this->addAsynPool('redisPoolSpare', new RedisAsynPool($this->config, $this->config->get('redis.active')));
    }

    /**
     * 用户进程
     * @throws \Exception
     */
    public function startProcess()
    {
        parent::startProcess();
        //ProcessManager::getInstance()->addProcess(SimProcess::class,'sim');
    }

    /**
     * 可以在这验证WebSocket连接,return true代表可以握手，false代表拒绝
     * @param HttpInput $httpInput
     * @return bool
     */
    public function onWebSocketHandCheck(HttpInput $httpInput)
    {
        return true;
    }

    /**
     * @return string
     */
    public function getCloseMethodName()
    {
        return 'onClose';
    }

    /**
     * @return string
     */
    public function getEventControllerName()
    {
        return 'AppController';
    }

    /**
     * @return string
     */
    public function getConnectMethodName()
    {
        return 'onConnect';
    }

    public function beforeSwooleStart()
    {
        parent::beforeSwooleStart();
        /*
        $this->tmp_table = new \swoole_table(1024);
        $this->tmp_table->column('id', \swoole_table::TYPE_INT, 8);
        $this->tmp_table->column('name', \swoole_table::TYPE_STRING, 50);
        $this->tmp_table->create();
        */

        //加载excel扩展
        $this->Spreadsheet = new Spreadsheet();

        //加载二维码扩展
        $this->QrCode = new QrCode();

        //七牛云
        $this->QiniuAuth = new Auth($this->config->get('qiniu.accessKey'), $this->config->get('qiniu.secrectKey'));
        $this->QiniuUploadManager = new UploadManager();

        //微信小程序用户加密数据的解密
        $this->WXBizDataCrypt = new WXBizDataCrypt();

//        //E签宝===
        $debug = false;//线上需要改成false
        if (!$debug) {
            require_once APP_DIR . '/Library/eSign/API/eSignOpenAPI.php';
            $this->eSign = new eSign();
        }

    }

}
