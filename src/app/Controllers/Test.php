<?php

namespace app\Controllers;

use app\Library\SLog;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup;
use PhpOffice\PhpSpreadsheet\Writer\Pdf;
use PhpOffice\PhpSpreadsheet\Writer\Pdf\Mpdf;
use Server\Asyn\Mysql\MysqlAsynPool;
use Server\Components\Process\ProcessManager;
use Server\CoreBase\Controller;
use Server\Coroutine\CoroutineBase;
use Server\Memory\Pool;

class Test extends Controller {

    protected $SimStateHttpClient;

    public function http_getStatus() {
        $path = '/device/info/';
        $path = '/v1/openapi' . $path . '89860429161891321670';
        $header = [];
        $header['timestamp'] = time();
        $header['key'] = '13wmEmNeAyDmx1Df0CIHuYfZrWhneNJM';
        $str = 'GET/v1/openapi/device/info/89860429161891321670' . $header['timestamp'] . '13wmEmNeAyDmx1Df0CIHuYfZrWhneNJMEOOlodNP32C3L7RRr0G0';
        $st1 = 'GET/v1/openapi/card/status/89860429161891321670154699741913wmEmNeAyDmx1Df0CIHuYfZrWhneNJMEOOlodNP32C3L7RRr0G0';
        $sha = sha1($str,true);
        $sign = base64_encode($sha);
        $header['signature'] = $sign;
        $this->SimStateHttpClient = get_instance()->getAsynPool('SimState');
        $response = $this->SimStateHttpClient->httpClient
                ->addHeader('timestamp', $header['timestamp'])
                ->addHeader('key', $header['key'])
                ->addHeader('signature', $header['signature'])
                //->setData('89860429161891321670')
                ->setMethod('GET')
                ->coroutineExecute($path);
        if ($response['statusCode'] == 0) {
            echo '成功';
        } else {
            echo '失败!';
        }
    }
    
    public function http_video(){
        $file = WWW_DIR.'/face/video/201812031748315002351992013033401543830511_6595.mp4';
        var_dump(file_exists('/www/cloud_api/src/www/face/video/201812031748315002351992013033401543830511_6595.mp4'));
        dump($file);       
        $str = file_get_contents($file);
        var_dump(base64_encode($str));
        //$erFile = file('/www/cloud_api/src/www/face/video/201809122209435003821994111892161536761383_3401.mp4');
        //var_dump($erFile);
        //var_dump(base64_encode($erFile));
    }

    public function http_index() {

        $this->http_output->end('index'); 
    }

    public function ping() {
        var_dump("11");
    }

    public function bind_uid() {
        var_dump($this->client_data);
        var_dump($this->fd);
        $this->bindUid($this->client_data->data);
    }

    public function efficiency_test2() {
        var_dump($this->client_data);
        var_dump($this->uid);
        $this->sendToUid('111112', 'test');
    }

    /**
     * @param $a1
     * @param $a2
     * @return mixed
     */
    protected function helpMerge(&$a1, $a2)
    {
        foreach ($a2 as $key => $value) {
            if (array_key_exists($key, $a1)) {
                $a1[$key] += $a2[$key];
            } else {
                $a1[$key] = $a2[$key];
            }
        }
        return $a1;
    }
    /**
     *
     * 请求参数     请求类型      是否必须      说明
     * @author ligang
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @date time
     */
    public function http_ligang() {

        $set = $this->config->get('server.set', []);
        $worker_num = $set['worker_num'];
        $status = ['pool' => [], 'model_pool' => [], 'controller_pool' => [], 'coroutine_num' => 0];

        for ($i = 0; $i < $worker_num; $i++) {
            $result = ProcessManager::getInstance()->getRpcCallWorker($i)->getPoolStatus();
            if (empty($result)) return;
            $this->helpMerge($status['pool'], $result['pool']);
            $this->helpMerge($status['model_pool'], $result['model_pool']);
            $this->helpMerge($status['controller_pool'], $result['controller_pool']);
            $status['coroutine_num'] += $result['coroutine_num'];
        }
        var_dump($status);
//        $result = $this->db->mysql_pool;
//        dump($result);
//        $pool = new Pool();
//        $result = $pool->getStatus();

//        $this->db->mysql_pool->pool_chan;
//        CoroutineBase::chan()->stats();
//        $inputFileName = str_replace('..', '', MYROOT) . '/pdf/2.pdf';
//        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($inputFileName);
//        var_dump($spreadsheet);

//        require MYROOT.'/vendor/phpoffice/phpspreadsheet/samples/Header.php';
//        $spreadsheet = require MYROOT.'/vendor/phpoffice/phpspreadsheet/samples/templates/sampleSpreadsheet.php';
//        $helper->log('Hide grid lines');
//        $spreadsheet->getActiveSheet()->setShowGridLines(false);
//
//        $helper->log('Set orientation to landscape');
//        $spreadsheet->getActiveSheet()->getPageSetup()->setOrientation(PageSetup::ORIENTATION_LANDSCAPE);
//
//        $className = \PhpOffice\PhpSpreadsheet\Writer\Pdf\Tcpdf::class;
//        $helper->log("Write to PDF format using {$className}");
//        IOFactory::registerWriter('Pdf', $className);
//        $writer = IOFactory::createWriter($spreadsheet,'Tcpdf');
//        $writer->save(str_replace('..','',MYROOT).'/pdf/1.pdf');
//        // Save
//        $helper->write($spreadsheet, __FILE__, ['Pdf']);
//        $helper->log('Hide grid lines');
//        $spreadsheet->getActiveSheet()->setShowGridLines(false);
//
//        $helper->log('Set orientation to landscape');
//        $spreadsheet->getActiveSheet()->getPageSetup()->setOrientation(PageSetup::ORIENTATION_LANDSCAPE);
//        $spreadsheet = new Spreadsheet();
//        $Tcpdf = new Pdf\Tcpdf($spreadsheet);
//        $Tcpdf->save('1.pdf');
//        $writer = IOFactory::createWriter($spreadsheet,'Tcpdf');
//        $writer->save(str_replace('..','',MYROOT).'/pdf/1.pdf');
//        var_dump($Tcpdf);
//        $className = \PhpOffice\PhpSpreadsheet\Writer\Pdf\Tcpdf::class;
//        $helper->log("Write to PDF format using {$className}");
//        IOFactory::registerWriter('Pdf', $className);
// Save
//        $helper->write($spreadsheet, __FILE__, ['Pdf']);
//        $class = \PhpOffice\PhpSpreadsheet\Writer\Pdf\Mpdf::class;
//        \PhpOffice\PhpSpreadsheet\IOFactory::registerWriter('Pdf', $class);
//        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Pdf');
//        $writer->save('1.pdf');
    }


}
