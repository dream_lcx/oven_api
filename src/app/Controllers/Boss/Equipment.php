<?php

namespace app\Controllers\Boss;

use app\Models\EquipmentLibraryModel;
use app\Services\Common\HttpService;
use PhpOffice\PhpSpreadsheet\IOFactory;

/**
 * 主板API
 */
class Equipment extends Base
{
    protected $equipmentLibraryModel;
    protected $equipmentListsModel;

    public function initialization($controller_name, $method_name)
    {
        parent::initialization($controller_name, $method_name);
        $this->equipmentLibraryModel = $this->loader->model('EquipmentLibraryModel', $this);
        $this->equipmentListsModel = $this->loader->model('EquipmentListsModel', $this);
    }

    /**
     * 主板库
     */
    public function http_libaray()
    {
        $page = $this->parm['page'] ?? 1;
        $pageSize = $this->parm['pageSize'] ?? 10;
        $map = [];
        if (isset($this->parm['sim_status'])) {
            $map['sim_status'] = $this->parm['sim_status'];
        }
        if (!empty($this->parm['device_no'])) {
            $map['device_no|sim_sn'] = ['like', "%" . $this->parm['device_no'] . "%"];
        }
        $data = $this->equipmentLibraryModel->getAll($map, '*', $page, $pageSize);
        if (empty($data)) {
            return $this->bossJsonend(0, '暂无数据');
        }
        foreach ($data as $k => $v) {
            $data[$k]['activa_time'] = empty($v['activa_time']) ? '' : date('Y-m-d H:i:s', $v['activa_time']);
            $data[$k]['expire_time'] = empty($v['expire_time']) ? '' : date('Y-m-d H:i:s', $v['expire_time']);
            $data[$k]['add_time'] = empty($v['add_time']) ? '' : date('Y-m-d H:i:s', $v['add_time']);
            $data[$k]['sim_status_desc'] = $v['sim_status'] == null ? '' : $this->config->get('sim_status')[$v['sim_status']];
        }
        $info['limit'] = $pageSize;
        $info['page_current'] = $page;
        $count = $this->equipmentLibraryModel->count($map);
        $info['page_sum'] = ceil($count / $pageSize);

        return $this->bossJsonend(0, '获取成功', $data, $count, $info);
    }

    /**
     * 主板列表
     */
    public function http_getLists()
    {
        $page = $this->parm['page'] ?? 1;
        $pageSize = $this->parm['pageSize'] ?? 10;
        $map = [];
        if (!empty($this->parm['device_no'])) {
            $map['device_no'] = ['like', "%" . $this->parm['device_no'] . "%"];
        }
        $data = $this->equipmentListsModel->selectJoinEquipmentLists($map, [],'equipment_id,device_no,status,device_status,province,city,area,address',[],$page,$pageSize);
        if (empty($data)) {
            return $this->bossJsonend(1, '暂无数据');
        }
        foreach ($data as $k => $v) {
            $data[$k]['device_status_name'] = empty($v['device_status']) ? '' : $this->config->get('device_status')[$v['device_status']];
        }
        $info['limit'] = $pageSize;
        $info['page_current'] = $page;
        $count = $this->equipmentListsModel->count($map);
        $info['page_sum'] = ceil($count / $pageSize);
        return $this->bossJsonend(0, '获取数据成功', $data, $count, $info);
    }



    /*
     * 添加主板库
     * */
    public function http_add(){
        //接收参数
        $id = $this->parm['id'] ?? '';
        $model_id = $this->parm['model_id'] ?? '';
        $device_no = $this->parm['device_no'] ?? '';

        //查询信息
        if(!empty($id)){
            $library_info=$this->equipmentLibraryModel->getOne(['id'=>$id],'id');
            if(empty($library_info)){
                return $this->bossJsonend(0, '主板信息错误');
            }
        }
        $add_data=[
            'model_id'=>$model_id,
            'device_no'=>$device_no,
        ];

        $add_status = false;
        $this->db->begin(function () use (&$add_status, &$add_data,&$id) {
            if(empty($id)){
                $add_data['add_time']=time();
                $this->equipmentLibraryModel->add($add_data);
            }else{
                $this->equipmentLibraryModel->save(['id'=>$id],$add_data);
            }

            $add_status = true;
        });

        if ($add_status) {
            return $this->bossJsonend(1, "操作成功");
        }
        return $this->bossJsonend(0, "操作失败");


    }


    /*
         * 导入主板库
         * */
    public function http_imports()
    {
        //接收参数
        $model_id = $this->parm['model_id'] ?? '';
        $array = $this->parm['arr'] ?json_decode($this->parm['arr'],true): [];

        if(empty($array) || empty($model_id)){
            return $this->bossJsonend(0, "缺少参数");
        }

        $add_status=false;
        $this->db->begin(function () use (&$add_status,&$model_id,&$array,&$file_path) {
            foreach ($array as $key => $value) {
                if (empty($value['A'])) {
                    continue;
                }
                $tmp = [];
                $tmp['sim_sn'] = $value['B'] ?? '';
                $tmp['imsi'] = $value['C'] ?? '';
                $equipment = $this->equipmentLibraryModel->getOne(['device_no' => $value['A']], 'id');
                if (!empty($equipment)) {
                    $this->equipmentLibraryModel->save(['id' => $equipment['id']], $tmp);
                } else {
                    $tmp['model_id'] = $model_id;
                    $tmp['device_no'] = $value['A'];
                    $tmp['add_time'] = time();
                    $tmp['company_id'] = 0;
                    $this->equipmentLibraryModel->add($tmp);
                }

            }
            $add_status=true;
        }, function ($e) {
            return $this->bossJsonend(0, '导入失败', $e->error);
        });
        if ($add_status) {
            return $this->bossJsonend(1, "操作成功");
        }
        return $this->bossJsonend(0, "操作失败");



    }








}
