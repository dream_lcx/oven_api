<?php

namespace app\Controllers\Boss;

/**
 * 产品API
 */
class Equipments extends Base
{
    protected $equipmentsModel;

    public function initialization($controller_name, $method_name)
    {
        parent::initialization($controller_name, $method_name);
        $this->equipmentsModel = $this->loader->model('EquipmentModel', $this);
    }

    /**
     * 产品列表
     */
    public function http_getLists()
    {
        $page = $this->parm['page'] ?? 1;
        $pageSize = $this->parm['pageSize'] ?? 10;

        $map = [];
        if (!empty($this->parm['company_id'])) {
            $map['company_id'] = ['=', $this->parm['company_id']];
        }
        if (!empty($this->parm['equipments_sn'])) {
            $map['equipments_sn'] = ['like', "%" . $this->parm['equipments_sn'] . "%"];
        }
        $data = $this->equipmentsModel->getAll($map,$page,$pageSize,'equipments_id,company_id,equipments_sn,equipments_type,equipments_name,equipments_money');
        if (empty($data)) {
            return $this->bossJsonend(1, '暂无数据');
        }

        $info['limit'] = $pageSize;
        $info['page_current'] = $page;
        $count = $this->equipmentsModel->getCount($map);
        $info['page_sum'] = ceil($count / $pageSize);
        return $this->bossJsonend(0, '获取产品成功', $data, $count, $info);

    }

    /**
     * 产品详情
     */
    public function http_getEquipmentsInfo(){
        //接收参数
        $equipments_sn=$this->parm['equipments_sn']??'';
        if(empty($equipments_sn)){
            return $this->bossJsonend(1, '缺少参数equipments_sn');
        }
        $data=$this->equipmentsModel->getDetail(['equipments_sn'=>$equipments_sn],'equipments_id,equipments_sn,equipments_type,equipments_name,create_time');
        return $this->bossJsonend(0, '获取产品成功', $data);
    }



}
