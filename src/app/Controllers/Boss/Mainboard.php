<?php
namespace app\Controllers\Boss;

/**
 * 主板PI
 */
class Mainboard extends Base
{
    protected $AchievementModel;

    public function initialization($controller_name, $method_name)
    {
        parent::initialization($controller_name, $method_name);
        $this->AchievementModel = $this->loader->model('AchievementModel', $this);
    }

    /**
     * 主板列表
     */
    public function http_getLists()
    {

        $page = $this->parm['page'] ?? 1;
        $pageSize = $this->parm['pageSize'] ?? 10;

        $map = [];
        if (!empty($this->parm['company_id'])) {
            $map['company_id'] = ['=', $this->parm['company_id']];
        }
        if (!empty($this->parm['equipments_sn'])) {
            $map['model'] = ['like', "%" . $this->parm['model'] . "%"];
        }
        $map['is_delete']=1;//未删除

        $this->AchievementModel->table='mainboard';
        $data = $this->AchievementModel->selectPageData($map,'*',[], $pageSize,$page);
        if (empty($data)) {
            return $this->bossJsonend(1, '暂无数据');
        }
        foreach($data as $k=>$v){
            $data[$k]['status_name']=$v['status']==1?'正常':'禁用';
            $data[$k]['create_time']=$v['create_time']?date('Y-m-d H:i',$v['create_time']):'--';
        }

        $info['limit'] = $pageSize;
        $info['page_current'] = $page;
        $count = $this->AchievementModel->getCount($map);
        $info['page_sum'] = ceil($count / $pageSize);
        return $this->bossJsonend(0, '获取主板成功', $data, $count, $info);

    }


    /*
     * 添加主板型号
     * */
    public function http_add(){
        //接收参数
        $model = $this->parm['model'] ?? '';
        $image = $this->parm['image'] ?? '';
        $edition = $this->parm['edition'] ?? '';
        $source = $this->parm['source'] ?? '';
        $status = $this->parm['status'] ?? 1;
        $num = $this->parm['num'] ?? 0;
        $text = $this->parm['text'] ?? '';

        //查询信息
        $this->AchievementModel->table='mainboard';
        if(!empty($model)){
            $where['model']=$model;
        }
        $mainboard_info=$this->AchievementModel->findData($where,'mainboard_id');
        if(!empty($mainboard_info)){
            return $this->bossJsonend(0, "主板编号已经存在");
        }

        $add_data=[
            'model'=>$model,
            'image'=>$image,
            'edition'=>$edition,
            'source'=>$source,
            'status'=>$status,
            'num'=>$num,
            'text'=>$text,
        ];

        $add_status = false;
        $this->db->begin(function () use (&$add_status, &$add_data,$mainboard_info) {
            $add_data['create_time']=time();
            $this->AchievementModel->insertData($add_data);

            $add_status = true;
        });

        if ($add_status) {
            return $this->bossJsonend(1, "操作成功");
        }
        return $this->bossJsonend(0, "操作失败");


    }


    /*
     * 编辑主板型号
     * */
    public function http_edit(){
        //接收参数
        $mainboard_id = $this->parm['mainboard_id'] ?? '';
        $model = $this->parm['model'] ?? '';
        $image = $this->parm['image'] ?? '';
        $edition = $this->parm['edition'] ?? '';
        $source = $this->parm['source'] ?? '';
        $status = $this->parm['status'] ?? 1;
        $num = $this->parm['num'] ?? 0;
        $text = $this->parm['text'] ?? '';
        if(empty($mainboard_id)){
            return $this->bossJsonend(0, "缺少参数");
        }
        //查询信息
        $this->AchievementModel->table='mainboard';
        $where['mainboard_id']=$mainboard_id;
        $mainboard_info=$this->AchievementModel->findData($where,'mainboard_id');
        if(empty($mainboard_info)){
            return $this->bossJsonend(0, "主板信息错误");
        }

        $edit_data=[
            'model'=>$model,
            'image'=>$image,
            'edition'=>$edition,
            'source'=>$source,
            'status'=>$status,
            'num'=>$num,
            'text'=>$text,
        ];

        $add_status = false;
        $this->db->begin(function () use (&$add_status, &$edit_data,$mainboard_info) {
            $this->AchievementModel->updateData(['mainboard_id'=>$mainboard_info['mainboard_id']],$edit_data);

            $add_status = true;
        });

        if ($add_status) {
            return $this->bossJsonend(1, "操作成功");
        }
        return $this->bossJsonend(0, "操作失败");


    }

    /*
     * 获取主板详情
     * */
    public function http_getInfo(){
        //接收参数
        $mainboard_id = $this->parm['mainboard_id'] ?? '';
        if(empty($mainboard_id)){
            return $this->bossJsonend(1, '缺少参数');
        }
        //查询信息
        $this->AchievementModel->table='mainboard';
        $data=$this->AchievementModel->findData(['mainboard_id'=>$mainboard_id],'*');
        if(empty($data)){
            return $this->bossJsonend(1, '主板信息错误');

        }
        $data['status_name']=$data['status']==1?'正常':'禁用';
        $data['create_time']=$data['create_time']?date('Y-m-d H:i',$data['create_time']):'--';
        $data['image_url']=$data['image']?UploadImgPath($data['image']):'';

        return $this->bossJsonend(0, '获取主板成功', $data);

    }

    /*
     * 删除主板
     * */
    public function http_del(){
        //接收参数
        //$mainboard_id = $this->parm['mainboard_id'] ?? '';
        $post=$this->request->post;
        $mainboard_id=$post['mainboard_id'];

        if(empty($mainboard_id)){
            return $this->bossJsonend(0, '缺少参数');
        }

        //查询信息
        $this->AchievementModel->table='mainboard';
        $data=$this->AchievementModel->findData(['mainboard_id'=>$mainboard_id],'mainboard_id,is_delete');
        if(empty($data)){
            return $this->bossJsonend(0, '主板信息错误');

        }
        if($data['is_delete']==2){
            return $this->bossJsonend(0, '该主板已经被删除');

        }
        $edit_data['is_delete']=2;
        $add_status = false;
        $this->db->begin(function () use (&$add_status, &$edit_data,&$mainboard_id) {

            $this->AchievementModel->updateData(['mainboard_id'=>$mainboard_id],$edit_data);

            $add_status = true;
        });

        if ($add_status) {
            return $this->bossJsonend(1, "操作成功");
        }
        return $this->bossJsonend(0, "操作失败");

    }

    /*
     * 更换状态
     * */
    public function http_changeStatus(){
        //接收参数
        $mainboard_id = $this->parm['mainboard_id'] ?? '';
        /*$post=$this->request->post;
        $mainboard_id=$post['mainboard_id'];*/

        if(empty($mainboard_id)){
            return $this->bossJsonend(0, '缺少参数');
        }

        //查询信息
        $this->AchievementModel->table='mainboard';
        $data=$this->AchievementModel->findData(['mainboard_id'=>$mainboard_id,'is_delete'=>1],'mainboard_id,status,is_delete');
        if(empty($data)){
            return $this->bossJsonend(0, '主板信息错误');

        }
        $edit_data['status']=1;
        if($data['status']==1){
            $edit_data['status']=2;
        }
        $add_status = false;
        $this->db->begin(function () use (&$add_status, &$edit_data,&$mainboard_id) {

            $this->AchievementModel->updateData(['mainboard_id'=>$mainboard_id],$edit_data);

            $add_status = true;
        });

        if ($add_status) {
            return $this->bossJsonend(1, "操作成功");
        }
        return $this->bossJsonend(0, "操作失败");




    }






}