<?php

namespace app\Controllers\Boss;

/**
 * 公司API
 */
class Company extends Base
{
    protected $admin_user_model;
    protected $CompanyConfigModel;
    protected $AchievementUserModel;
    protected $ContractTemplateModel;
    protected $configModel;
    protected $oaBalanceConfigModel;
    protected $customerModel;
    protected $businessTypeModel;
    protected $operationInfoModel;
    protected $administrativeInfoModel;


    public function initialization($controller_name, $method_name)
    {
        parent::initialization($controller_name, $method_name);
        $this->admin_user_model = $this->loader->model('AdminUserModel', $this);
        $this->CompanyConfigModel = $this->loader->model('CompanyConfigModel', $this);
        $this->AchievementUserModel = $this->loader->model('AchievementUserModel', $this);
        $this->ContractTemplateModel = $this->loader->model('ContractTemplateModel', $this);
        $this->configModel = $this->loader->model('ConfigModel', $this);
        $this->oaBalanceConfigModel = $this->loader->model('OaBalanceConfigModel', $this);
        $this->customerModel = $this->loader->model('CustomerModel', $this);
        $this->businessTypeModel = $this->loader->model('BusinessTypeModel', $this);
        $this->operationInfoModel = $this->loader->model('OperationInfoModel', $this);
        $this->administrativeInfoModel = $this->loader->model('AdministrativeInfoModel', $this);

    }

    /**
     * showdoc
     * @catalog API文档/BOSS端/公司相关
     * @title 新增CRM总后台账户
     * @description
     * @method POST
     * @url /boss/Company/addAccount
     * @param company_id 必选 int 公司ID
     * @return
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-12-5
     */
    public function http_addAccount()
    {
        // 接收参数
        $admin['account'] = $this->parm['account'] ?? '';
        //判断账号唯一性
        $info = $this->admin_user_model->getOne(['account' => $this->parm['account'], 'company_id' => $this->parm['company_id'], 'is_delete' => 0], 'company_id');
        if (!empty($info)) {
            return $this->jsonend(-1100, "账号已被占用,请重新设置");
        }
        $pwd = $this->parm['password'] ?? '';
        $password = setPassword(trim($pwd));
        $admin['password'] = $password['password'];
        $admin['strict'] = $password['strict'];
        $admin['company_id'] = $this->parm['company_id'] ?? '';
        $admin['phone'] = $this->parm['phone'] ?? '';
        $admin['name'] = $this->parm['name'] ?? '';
        $admin['status'] = 1;
        $admin['is_admin'] = 1;
        $admin['add_time'] = $this->parm['add_time'] ?? '';
        $admin['address'] = $this->parm['address'] ?? '';

        $img_config = $this->parm['img_config']??'';
        $contract['company_id'] = $this->parm['company_id'];

        //要加三条合同模板数据
        if (!empty($img_config['admin_file1'])) {//新装购买模板
            $contract['local_path'] = $img_config['admin_file1'];
            $contract['template_name'] = '新装购买合同模板';
            $contract['contract_class'] = 2;
            $contract['business_id'] = 3;
            $this->ContractTemplateModel->add($contract);
        }
        if (!empty($img_config['admin_file2'])) {//新装租赁模板
            $contract['local_path'] = $img_config['admin_file2'];
            $contract['template_name'] = '新装租赁合同模板';
            $contract['contract_class'] = 1;
            $contract['business_id'] = 3;
            $this->ContractTemplateModel->add($contract);
        }
        if (!empty($img_config['admin_file3'])) {//移机模板
            $contract['local_path'] = $img_config['admin_file3'];
            $contract['template_name'] = '移机合同模板';
            $contract['business_id'] = 5;
            $contract['contract_class'] = 1;
            $this->ContractTemplateModel->add($contract);
        }

        $add_status = false;
        $this->db->begin(function () use (&$add_status, &$admin) {
            $this->admin_user_model->add($admin);//CRM总后台账户
            //新增业绩系统账户
            $admin['account'] = $admin['account'] . '@yj';//业绩系统默认账户
            $this->AchievementUserModel->add($admin);
            //初始化系统配置数据-根据第一个入驻公司
            $system_config_defalut = $this->configModel->getAll(['company_id' => $this->config->get('default_company_id')], 1, -1);
            if (!empty($system_config_defalut)) {
                foreach ($system_config_defalut as $k => $v) {
                    $system_config_defalut[$k]['id'] = null;
                    $system_config_defalut[$k]['company_id'] = $this->parm['company_id'];
                    $system_config_defalut[$k]['value'] = $v['default_value'];
                    $this->configModel->add($system_config_defalut[$k]);
                }

            }
            //初始化业绩配置
            $oa_config_defalut = $this->oaBalanceConfigModel->getAll(['company_id' => $this->config->get('default_company_id')], 1, -1);
            if (!empty($oa_config_defalut)) {
                foreach ($oa_config_defalut as $k => $v) {
                    $oa_config_defalut[$k]['id'] = null;
                    $oa_config_defalut[$k]['company_id'] = $this->parm['company_id'];
                    $oa_config_defalut[$k]['value'] = $v['default_value'];
                    $this->oaBalanceConfigModel->add($oa_config_defalut[$k]);
                }
            }
            //新增初始化业务类型配置
            $buiness_defalut=$this->businessTypeModel->getAll(['company_id'=>$this->config->get('default_company_id')],'*');
            if (!empty($buiness_defalut)) {
                foreach ($buiness_defalut as $k => $v) {
                    $buiness_defalut[$k]['id'] = null;
                    $buiness_defalut[$k]['company_id'] = $this->parm['company_id'];
                    $this->businessTypeModel->add($buiness_defalut[$k]);
                }
            }
            //新公司入驻添加系统默认推荐人用户
            $user_data=[
                'user_belong_to_company'=>$this->parm['company_id'],
                'is_auth'=>2,
                'source_is_default'=>2,
                'account'=>randAccountNo(),
                'username'=>$this->parm['company_name']??''.'总账号',
                'realname'=>$this->parm['company_name']??''.'总账号',
                'add_mode'=>2,//后台添加
                'remark'=>'新公司入驻系统自动生成',
                'add_time'=>time()
            ];
            $insert_id=$this->customerModel->add($user_data);

            //修改公司配置默认推荐人
            $value=[
                'user_id'=>$insert_id,
                'account'=>$user_data['account'],
                'telphone'=>'',
                'id_card'=>'',
                'name'=>$this->parm['company_name']??''.'总账号',
            ];
            $value=json_encode($value);
            $this->oaBalanceConfigModel->save(['company_id'=>$this->parm['company_id'],'`key`'=>'default_inviter'],['value'=>$value]);

            $add_status = true;
        });


        if ($add_status) {
            return $this->jsonend(1000, "新增成功");
        }
        return $this->jsonend(-1000, "新增失败");
    }

    /**
     * showdoc
     * @catalog API文档/BOSS端/公司相关
     * @title 编辑CRM总后台账户
     * @description
     * @method POST
     * @url /boss/Company/editAccount
     * @param company_id 必选 int 公司ID
     * @return
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-12-5
     */
    public function http_editAccount()
    {
        if (empty($this->parm['company_id'] ?? '') || empty($this->parm['account'] ?? '')) {
            return $this->jsonend(-1001, "缺少参数");
        }
        // 接收参数
        $admin['account'] = $this->parm['account'] ?? '';
        //判断账号唯一性
        $count = $this->admin_user_model->count(['account' => $this->parm['account'], 'company_id' => $this->parm['company_id'], 'is_delete' => 0]);
        if ($count > 1) {
            return $this->jsonend(-1100, "账号已被占用,请重新设置");
        }
        if (!empty($this->parm['password'] ?? '')) {
            $password = setPassword(trim($this->parm['password']));
            $admin['password'] = $password['password'];
            $admin['strict'] = $password['strict'];
        }
        $admin['phone'] = $this->parm['phone'] ?? '';
        $admin['name'] = $this->parm['name'] ?? '';
        $admin['status'] = 1;
        //$admin['add_time'] = $this->parm['add_time'] ?? '';
        $admin['update_time'] = time();
        $admin['address'] = $this->parm['address'] ?? '';
        $add_status = false;
        $this->db->begin(function () use (&$add_status, &$admin) {
            //修改账户信息
            $this->admin_user_model->save(['account' => $this->parm['account'], 'company_id' => $this->parm['company_id']], $admin);
            //初始化系统配置数据-根据第一个入驻公司
            $own_config_count = $this->configModel->count(['company_id' => $this->parm['company_id']]);
            if ($own_config_count == 0) {
                $system_config_defalut = $this->configModel->getAll(['company_id' => $this->config->get('default_company_id')], 1, -1);
                if (!empty($system_config_defalut)) {
                    foreach ($system_config_defalut as $k => $v) {
                        $system_config_defalut[$k]['id'] = null;
                        $system_config_defalut[$k]['company_id'] = $this->parm['company_id'];
                        $system_config_defalut[$k]['value'] = $v['default_value'];
                        $this->configModel->add($system_config_defalut[$k]);
                    }

                }
            }

            //初始化业绩配置
            $own_oa_config_count = $this->oaBalanceConfigModel->count(['company_id' => $this->parm['company_id'],'is_delete'=>0]);
            if ($own_oa_config_count == 0) {
                $oa_config_defalut = $this->oaBalanceConfigModel->getAll(['company_id' => $this->config->get('default_company_id'),'is_delete'=>0], 1, -1);
                if (!empty($oa_config_defalut)) {
                    foreach ($oa_config_defalut as $k => $v) {
                        $oa_config_defalut[$k]['id'] = null;
                        $oa_config_defalut[$k]['company_id'] = $this->parm['company_id'];
                        $oa_config_defalut[$k]['value'] = $v['default_value'];
                        $this->oaBalanceConfigModel->add($oa_config_defalut[$k]);
                    }
                }
            }
            //合同模板配置
            $img_config = $this->parm['img_config'];
            $contract['company_id'] = $this->parm['company_id'];
            if (!empty($img_config['admin_file1'])) {
                $contract['local_path'] = $img_config['admin_file1'];
                $template_id = $this->ContractTemplateModel->getOne(['business_id' => 3, 'contract_class' => 2, 'company_id' => $this->parm['company_id']], 'template_id');
                if (empty($template_id)) {
                    $contract['local_path'] = $img_config['admin_file1'];
                    $contract['template_name'] = '新装购买合同模板';
                    $contract['contract_class'] = 2;
                    $contract['business_id'] = 3;
                    $this->ContractTemplateModel->add($contract);//新装购买模板
                } else {
                    $this->ContractTemplateModel->save(['business_id' => 3, 'contract_class' => 2, 'company_id' => $this->parm['company_id']], $contract);//新装购买模板
                }

            }
            if (!empty($img_config['admin_file2'])) {
                $contract['local_path'] = $img_config['admin_file2'];
                $template_id = $this->ContractTemplateModel->getOne(['business_id' => 3, 'contract_class' => 1, 'company_id' => $this->parm['company_id']], 'template_id');
                if (empty($template_id)) {
                    $contract['local_path'] = $img_config['admin_file2'];
                    $contract['template_name'] = '新装租赁合同模板';
                    $contract['contract_class'] = 1;
                    $contract['business_id'] = 3;
                    $this->ContractTemplateModel->add($contract);//新装租赁模板
                } else {
                    $this->ContractTemplateModel->save(['business_id' => 3, 'contract_class' => 1, 'company_id' => $this->parm['company_id']], $contract);//新装租赁模板
                }

            }
            if (!empty($img_config['admin_file3'])) {
                $contract['local_path'] = $img_config['admin_file3'];
                $template_id = $this->ContractTemplateModel->getOne(['business_id' => 5, 'contract_class' => 1, 'company_id' => $this->parm['company_id']], 'template_id');
                if (empty($template_id)) {
                    $contract['local_path'] = $img_config['admin_file3'];
                    $contract['template_name'] = '移机合同模板';
                    $contract['business_id'] = 5;
                    $contract['contract_class'] = 1;
                    $this->ContractTemplateModel->add($contract);//移机模板
                } else {
                    $this->ContractTemplateModel->save(['business_id' => 5, 'contract_class' => 1, 'company_id' => $this->parm['company_id']], $contract);//移机模板
                }

            }
            $add_status = true;
        });

        if ($add_status) {
            return $this->jsonend(1000, "编辑成功");
        }
        return $this->jsonend(-1000, "编辑失败");
    }


    /**
     * showdoc
     * @catalog API文档/BOSS端/公司相关
     * @title 获取运营中心和城市合伙人的信息
     * @description
     * @method POST
     * @url /boss/Company/getOperationAdministrativeInfo
     * @param company_id 必选 int 公司ID
     * @param o_sn 可选 int 运营中心编号
     * @param a_sn 可选 int 城市合伙人编号
     * @return
     * @remark
     * @number 0
     * @author tx
     * @date 2020/8/20
     */

    public function http_getOperationAdministrativeInfo(){
        //接收参数
        $company_id=$this->parm['company_id']??'';
        $o_sn=$this->parm['o_sn']??'';
        $a_sn=$this->parm['a_sn']??'';
        if(empty($company_id)){
            return $this->bossJsonend(-1000, "缺少公司参数");
        }
        $data=[];

        if(!empty($o_sn)){
            $data=$this->operationInfoModel->getOne(['o_sn'=>$o_sn,'company_id'=>$company_id],'o_id,o_sn,company_name,name,phone,address');

        }
        if(!empty($a_sn)){
            $data=$this->administrativeInfoModel->getOne(['a_sn'=>$a_sn,'company_id'=>$company_id],'a_id,a_sn,company_name,name,phone,address');

        }
        return $this->bossJsonend(1000, "获取成功",$data);

    }





}
