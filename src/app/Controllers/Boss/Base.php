<?php

/// +----------------------------------------------------------------------
// | BOSS后台接口
// +----------------------------------------------------------------------
// | Author: lcx <916450508@qq.com >
// +----------------------------------------------------------------------
// | Date: 2018/12/5
// +----------------------------------------------------------------------

namespace app\Controllers\Boss;

use app\Library\SLog;
use app\Services\Log\FileLogService;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Server\CoreBase\Controller;

/*
 * 基础类
 */

class Base extends Controller {

    protected $parm;
    protected $GetIPAddressHttpClient;

    public function initialization($controller_name, $method_name) {
      
//        if($this->http_output->server->request_method=='POST'){
//           
//        }
        $this->http_output->setHeader('Access-Control-Allow-Origin', '*');
//            $this->http_output->setHeader('Access-Control-Allow-Credentials', true);
        $this->http_output->setHeader("Access-Control-Allow-Headers", " Origin, X-Requested-With, Content-Type, Accept,token");
        $this->http_output->setHeader("Access-Control-Allow-Methods", " POST");
        $this->http_output->setHeader('Content-Type','application/json; charset=utf-8');
        parent::initialization($controller_name, $method_name);
        $this->parm = json_decode($this->http_input->getRawContent(), true);
        /*if($this->request->method()!='POST'){
            return false;
        }*/
    }

    public function __construct() {
        parent::__construct();
        $this->GetIPAddressHttpClient = get_instance()->getAsynPool('BuyWater');
    }

    /**
     * 异常的回调(如果需要继承$autoSendAndDestroy传flase)
     * @param \Throwable $e
     * @param callable $handle
     */
    public function onExceptionHandle(\Throwable $e, $handle = null) {
        //必须的代码
        if ($e instanceof SwooleRedirectException) {
            $this->http_output->setStatusHeader($e->getCode());
            $this->http_output->setHeader('Location', $e->getMessage());
            $this->http_output->end('end');
            return;
        }
        if ($e instanceof SwooleException) {
            FileLogService::WriteLog(SLog::ERROR_LOG,'error',$e->getMessage() . "\n" . $e->getTraceAsString());
        }
        //可以重写的代码
        if ($handle == null) {
            switch ($this->request_type) {
                case SwooleMarco::HTTP_REQUEST:
                    $e->request = $this->request;
                    $data = get_instance()->getWhoops()->handleException($e);
                    $this->http_output->setStatusHeader(500);
                    $this->http_output->end($data);
                    break;
                case SwooleMarco::TCP_REQUEST:
                    $this->send($e->getMessage());
                    break;
            }
        } else {
            \co::call_user_func($handle, $e);
        }
    }

    /**
     * layui table数据 -API返回
     * @param int $code 返回码 0成功 1失败
     * @param string $msg 提示消息
     * @param array $count 总数量
     * @param array $info ['limit' => $limit, 'page_current' => $page, 'page_sum'  => ceil($count / $limit)] 分页信息【每页条数，当前页数，总页数】
     * @param array $data 数据
     * @return type
     */
    public function tableJsonend($code = 0, $msg = '获取成功', $data = [], $count = 1, $info = []) {
        $output = ['code' => $code, 'msg' => $msg, 'data' => $data, 'count' => $count,'info'=>$info];
        if (!$this->canEnd()) {
            return;
        }
        if (!get_instance()->config->get('http.gzip_off', false)) {
            //低版本swoole的gzip方法存在效率问题
//            if ($gzip) {
//                $this->response->gzip(1);
//            }
            //压缩备用方案
            /* if ($gzip) {
              $this->response->header('Content-Encoding', 'gzip');
              $this->response->header('Vary', 'Accept-Encoding');
              $output = gzencode($output . " \n", 9);
              } */
        }
        if (is_array($output) || is_object($output)) {
            $this->response->header('Content-Type', 'text/html; charset=UTF-8');
            $output = json_encode($output, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            //$output = "<pre>$output</pre>";
        }

        $this->response->end($output);
        $this->endOver();
    }

    /**
     * 返回json格式
     */
    public function jsonend($code = '', $message = '', $data = '', $gzip = true) {
        $output = array('code' => $code, 'message' => $message, 'data' => $data);
        if (!$this->canEnd()) {
            return;
        }
        if (!get_instance()->config->get('http.gzip_off', false)) {
            //低版本swoole的gzip方法存在效率问题
//            if ($gzip) {
//                $this->response->gzip(1);
//            }
            //压缩备用方案
            /* if ($gzip) {
              $this->response->header('Content-Encoding', 'gzip');
              $this->response->header('Vary', 'Accept-Encoding');
              $output = gzencode($output . " \n", 9);
              } */
        }
        if (is_array($output) || is_object($output)) {
            $this->response->header('Content-Type', 'text/html; charset=UTF-8');
            $output = json_encode($output, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            //$output = "<pre>$output</pre>";
        }

        $this->response->end($output);
        $this->endOver();
    }
    public function bossJsonend($code = '', $message = '', $data = '', $conut='',$info=''){
        $output = array('code' => $code, 'msg' => $message, 'data' => $data,'count'=>$conut,'info'=>$info);
        if (!$this->canEnd()) {
            return;
        }
        if (is_array($output) || is_object($output)) {
            $this->response->header('Content-Type', 'text/html; charset=UTF-8');
            $output = json_encode($output, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            //$output = "<pre>$output</pre>";
        }

        $this->response->end($output);
        $this->endOver();
    }

    /**
     * 公共导出方法
     * @param array $tableTile 表头
     * @param array $field 字段名称，与表头对应
     * @param string $sheet_title sheet名称
     * @param string $title 内容标题
     * @param array $data 导出数据
     * @param string $file_name 文件名
     * @param string $file_path 文件储存路径
     * @param string $suffix 文件后缀
     * @param bool $output 是否输出
     * @return string
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @date 2019/1/28 16:36
     * @author ligang
     */
    public function exports(array $tableTile, array $field, string $sheet_title, string $title, array $data, string $file_name, string $file_path, string $suffix = 'xlsx', bool $output = false)
    {
        $filename = $file_name . '.' . strtolower($suffix);
        switch (strtolower($suffix)) {
            case 'xlsx':
                $writerType = 'Xlsx';
                break;
            case 'xls':
                $writerType = 'Xls';
                break;
            case 'ods':
                $writerType = 'Ods';
                break;
            case 'csv':
                $writerType = 'Csv';
                break;
            case 'html':
                $writerType = 'Html';
                break;
            case 'tcpdf':
                $writerType = 'Tcpdf';
                break;
            case 'dompdf':
                $writerType = 'Dompdf';
                break;
            case 'mpdf':
                $writerType = 'Mpdf';
                break;
            default:
                throw new \Exception('文件格式不存在：' . $suffix, 500);
        }
        if (stripos($file_path, $filename) === false) {
            if (!file_exists($file_path)) {
                throw new \Exception('目录不存在：' . $file_path, 500);
            }
            $save_path = $file_path . $filename;
        } else {
            $save_path = $file_path;
        }

        $spreadsheet = get_instance()->Spreadsheet;
        //$Spreadsheet = new Spreadsheet();

        $spreadsheet->getProperties()
            ->setCreator("Maarten Balliauw")
            ->setLastModifiedBy("Maarten Balliauw")
            ->setTitle("Office 2007 XLSX Test Document")
            ->setSubject("Office 2007 XLSX Test Document")
            ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
            ->setKeywords("office 2007 openxml php")
            ->setCategory("Test result file");

        //设置sheet名
        $spreadsheet->getActiveSheet()->setTitle($sheet_title);

        //设置表头
        $spreadsheet->getActiveSheet()->fromArray($tableTile, null, 'A2');

        $letter = 'A';
        for ($i = 0; $i < count($tableTile) - 1; $i++) {
            if ($letter == 'A') {
                $spreadsheet->getActiveSheet()->getColumnDimension($letter)->setWidth(22);
            }
            //操过Z后是从AA-AZ，BA-BZ,CA-CZ....
            $letter++;
            if ($letter != 'A') {
                $spreadsheet->getActiveSheet()->getColumnDimension($letter)->setWidth(22);
            }
        }

        $titleStyleArray = [
            'font' => [
                'bold' => true,
                'size' => 14
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
            ],
        ];
        //合并单元格，并设置标题和样式
        $spreadsheet->getActiveSheet()->mergeCells('A1:' . $letter . '1');
        $spreadsheet->getActiveSheet()->setCellValue('A1', $title);
        $spreadsheet->getActiveSheet()->getStyle('A1')->applyFromArray($titleStyleArray);

        //设置数据部分样式
        $dataStyleArray = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
            ],
        ];
        $spreadsheet->getActiveSheet()->getStyle('A2:' . $letter . (count($data) + 3))->applyFromArray($dataStyleArray);

        //设置行高(参数：pRow是表的行数，从一开始)
        $spreadsheet->getActiveSheet()->getRowDimension('1')->setRowHeight(50);

        //设置列宽
        //$spreadsheet->getActiveSheet()->getColumnDimension($letter)->setWidth(22);
        //设置默认列宽
        //$spreadsheet->getActiveSheet()->getColumnDimension($letter)->setAutoSize(true);

        $k = 3;
        foreach ($data as $key => $value) {
            $curr_letter = 'A';
            for ($j = 0; $j < count($tableTile); $j++) {
                $spreadsheet->getActiveSheet()->setCellValue($curr_letter . $k, $value[$field[$j]]);
                if ((is_numeric($value[$field[$j]]) || is_float($value[$field[$j]])) && $value[$field[$j]] < 9999) {
                    //设置单元格格式
                    $spreadsheet->getActiveSheet()->getStyle($curr_letter . $k)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
                }
//            elseif (strtotime($value[$field[$j]])){
//                $spreadsheet->getActiveSheet()->getStyle($curr_letter . $k)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_DATE_YYYYMMDDSLASH);
//            }
                $curr_letter++;
            }
            $k++;
        }

        if ($output) {
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="' . $filename . '"');
            header('Cache-Control: max-age=0');
            $writer = IOFactory::createWriter($spreadsheet, $writerType);
            $writer->save('php://output');
            return false;
        } else {
            $writer = IOFactory::createWriter($spreadsheet, $writerType);
            $writer->save($save_path);
            return $filename;
        }
    }

    /**
     * 删除文件
     * @param string $path 文件或目录，目录以/结尾
     * @param string $suffix 文件后缀，空为所有文件，区分大小写，目录有效
     * @param bool $today 是否检查当天文件，true当天文件不会被删除，目录有效
     * @return bool
     * @date 2019/1/29 9:57
     * @author ligang
     */
    public function delFile(string $path, string $suffix = '', bool $today = true)
    {
        if (is_dir($path)) {
            $date = date('Ymd', time());
            //是个目录
            $file_array = scandir($path);
            foreach ($file_array as $key => $value) {
                if ($value == '.' || $value == '..') {
                    continue;
                }
                $file_path = $path . $value;
                if ($today) {
                    $filectime = filectime($file_path);
                    if ($filectime !== false && date('Ymd', $filectime) == $date) {
                        continue;
                    }
                }

                if (!empty($suffix)) {
                    if (stripos($value, $suffix) === false) {
                        continue;
                    }
                    unlink($file_path);
                } else {
                    unlink($file_path);
                }
            }
            return true;
        }
        unlink($path);
        return true;
    }






}
