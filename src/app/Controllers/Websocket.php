<?php

namespace app\Controllers;

use app\Library\SLog;
use Server\CoreBase\Controller;

class Websocket extends Controller
{
    protected $equipmentlistsModel;
    protected $workOrderModel;
    protected $orderModel;
    protected $customerModel;

    public function initialization($controller_name, $method_name)
    {
        parent::initialization($controller_name, $method_name);
        $this->equipmentlistsModel = $this->loader->model('EquipmentListsModel', $this);
        $this->workOrderModel = $this->loader->model('WorkOrderModel', $this);
        $this->orderModel = $this->loader->model('OrderModel', $this);
        $this->customerModel = $this->loader->model('CustomerModel', $this);
    }

    public function ping()
    {
        $this->deviceStatus();//主板数量统计
        $this->statisticsTotal();//总数统计
        $this->deviceAreaDistribution();//主板地域分布
        $this->cityTds();//城市TDS
        //  $this->changeFilter();//换芯周期
        $this->deviceDistribution();//主板分布
        $this->userAgeAndSex();//用户画像
        $this->waterMaking();//制水分析

    }

    public function bind_uid()
    {
        $data = $this->client_data->data;
        $type = $data->type ?? 0;
        if ($type == 1) {
            $this->redis->lPush('cloud_big_data', $data->uid);
            $this->bindUid($data->uid);
        }

    }

    public function efficiency_test2()
    {
        $this->sendToUid('youheone_admin', 'test');
    }

    public function login_state()
    {
        $this->bindUid($this->client_data->data);
        $data['type'] = 1;
        $this->sendToUid($this->client_data->data, json_encode($data));
    }

    //主板数量统计-A2
    public function deviceStatus()
    {
        try {
            //大数据Uid
            $big_data_count = $this->redis->lRange('cloud_big_data', 0, -1);
            $all_count = get_instance()->coroutineCountOnline() - count($big_data_count);
            $join = [
                ['equipments', 'rq_equipment_lists.equipments_id = rq_equipments.equipments_id', 'LEFT'],
            ];
            $map['type'] = ['IN', [1, null]];//排除非物联网的
            $device_total = $this->equipmentlistsModel->count($map, $join);//主板总数
            $data['total'] = $device_total;
            $data['online'] = $all_count < 0 ? 0 : $all_count;//在线主板台数
            $data['offline'] = $device_total - $data['online'];//离线主板台数
            $data['ratio'] = $device_total == 0 ? 100 : round(($data['online'] / $device_total) * 100, 2);//在线率
            $result = json_encode(['command' => 'A2', 'data' => $data]);
            $this->send($result);
        } catch (\Exception $e) {
            var_dump("A2获取主板数量发生异常:" . $e->getMessage());
        }
    }

    //制水时间段-A3
    public function waterMaking()
    {
        try {
            $redis_data = $this->redis->get('water_making_state_analysis');
            if (empty($redis_data)) {//如果redis没有记录重新分析计算
                $join = [
                    ['equipments', 'rq_equipment_lists.equipments_id = rq_equipments.equipments_id', 'LEFT'],
                ];
                $map['type'] = ['IN', [1, null]];//排除非物联网的
                $device_list = $this->equipmentlistsModel->selectEquipmentLists([], 'device_no', [], '', $join);
                $data = [];
                foreach ($device_list as $k => $v) {//循环读取制水日志
                    if (empty($v['device_no'])) {
                        continue;
                    }
                    $info = $this->readSLog('device_work_state', $v['device_no']);
                    if (empty($info)) {
                        continue;
                    }
                    $data = array_merge($data, $info);
                }
                $result[0] = [];
                $result[1] = [];
                $result[2] = [];
                $result[3] = [];
                $result[4] = [];
                $result[5] = [];
                //将数据划分到时间段
                foreach ($data as $k => $v) {
                    //如果是09移除0
                    $start = substr($v['date'], 0, 1);
                    $date = $v['date'];
                    if ($start <= 0) {
                        $date = substr($v['date'], 1, 1);
                    }
                    if (empty($date)) {
                        continue;
                    }
                    if ($date <= 3) {
                        $result[0][] = $v;
                    } else if ($date >= 4 && $date <= 7) {
                        $result[1][] = $v;
                    } else if ($date >= 8 && $date <= 11) {
                        $result[2][] = $v;
                    } else if ($date >= 12 && $date <= 15) {
                        $result[3][] = $v;
                    } else if ($date >= 16 && $date <= 19) {
                        $result[4][] = $v;
                    } else if ($date >= 20 && $date <= 23) {
                        $result[5][] = $v;
                    }

                }
                foreach ($result as $k => $v) {//移除同一时段同一主板重复项
                    $result[$k] = $this->second_array_unique_bykey($v, "device_no");
                }
                $return['title'] = ["0-3点", "4-7点", "8-11点", "12-15点", "16-19点", "20-23点"];
                $return['data'] = [count($result[0]), count($result[1]), count($result[2]), count($result[3]), count($result[4]), count($result[5])];
                $this->redis->set('water_making_state_analysis', json_encode($return), 10800);
            } else {
                $return = json_decode($redis_data, true);
            }
            $result = json_encode(['command' => 'A6', 'data' => $return]);
            $this->send($result);
        } catch (\Exception $e) {
            var_dump("A6获取制水段分布发生异常:" . $e->getMessage());
        }

    }

    public function second_array_unique_bykey($arr, $key)
    {
        $tmp_arr = array();
        foreach ($arr as $k => $v) {
            if (in_array($v[$key], $tmp_arr))  //搜索$v[$key]是否在$tmp_arr数组中存在，若存在返回true
            {
                unset($arr[$k]); //销毁一个变量 如果$tmp_arr中已存在相同的值就删除该值
            } else {
                $tmp_arr[$k] = $v[$key]; //将不同的值放在该数组中保存
            }
        }
//ksort($arr); //ksort函数对数组进行排序(保留原键值key) sort为不保留key值
        return $arr;
    }


//读取工作状态日志
    protected function readSLog(string $type, string $filename, string $Suffix = 'log')
    {
        $file_path = SLog::logConf()[$type][0] . $filename . '.' . $Suffix;
        if (!file_exists($file_path)) {
            return [];
        }
        //cat 862057040275320.log | grep 'water_making_state":1'
        $command = 'cat ' . SLog::logConf()[$type][0] . $filename . '.' . $Suffix . " | grep 'water_making_state" . '"' . ":1'";
        exec($command, $output, $return_var);
        $results = [];
        foreach ($output as $key => $value) {
            $date = substr($value, 12, 2);//截取日期
            $star = stripos($value, 'WriteLog');
            $string = substr($value, $star + 9, -3);
            $decode = json_decode($string, 1);
            $decode = $decode ? $decode : $string;
            $result['date'] = $date;
            $result['device_no'] = $decode[0] ?? '';
            $result['water_making_state'] = empty($decode[1] ?? '') ? '' : $decode[1]['water_making_state'];
            $results[] = $result;
        }
        return $results;
    }

//用户画像-A4
    public function userAgeAndSex()
    {
        try {
            $redis_data = $this->redis->get('user_portrait_analysis');
            if (empty($redis_data)) {
                //性别
                $sex['man'] = $this->customerModel->getCount(['gender' => 1]);
                $sex['woman'] = $this->customerModel->getCount(['gender' => 2]);
                $sex['none'] = $this->customerModel->getCount(['gender' => 0]);
                $return['sex'] = $sex;
                //年龄0-17,18-24,25-29,30-39,40-49,50以上
                $age = [];
                $title = ["0-17岁", "18-24岁", "25-29岁", "30-39岁", "40-49岁", "50岁以上","未知"];

                //17岁以下
                $step17_time = strtotime(date("Y-12-31 23:59:59", strtotime("-17 year")));
                $map['birthday'] = ['BETWEEN',[$step17_time,time()]];
                array_push($age, $this->customerModel->getCount($map));
                //18-24
                $step18_time = strtotime(date("Y-01-01 00:00:00", strtotime("-18 year")));
                $step24_time = strtotime(date("Y-12-31 23:59:59", strtotime("-25 year")));
                $map['birthday'] = ['BETWEEN', [$step24_time-1, $step18_time]];
                array_push($age, $this->customerModel->getCount($map));

                //25-29
                $step25_time = strtotime(date("Y-01-01 00:00:00", strtotime("-25 year")));
                $step29_time = strtotime(date("Y-12-31 23:59:59", strtotime("-30 year")));
                $map['birthday'] = ['BETWEEN', [$step29_time-1, $step25_time]];
                array_push($age, $this->customerModel->getCount($map));
                //30-39
                $step30_time = strtotime(date("Y-01-01 00:00:00", strtotime("-30 year")));
                $step39_time = strtotime(date("Y-12-31 23:59:59", strtotime("-40 year")));
                $map['birthday'] = ['BETWEEN', [$step39_time-1, $step30_time]];
                array_push($age, $this->customerModel->getCount($map));
                //40-49
                $step40_time = strtotime(date("Y-01-01 00:00:00", strtotime("-40 year")));
                $step49_time = strtotime(date("Y-12-31 23:59:59", strtotime("-50 year")));
                $map['birthday'] = ['BETWEEN', [$step49_time-1, $step40_time]];
                array_push($age, $this->customerModel->getCount($map));
                //50以上
                $step50_time = strtotime(date("Y-12-31 23:59:59", strtotime("-50 year")));
                $map['birthday'] = ['<', $step50_time];
                array_push($age, $this->customerModel->getCount($map));
                //未知
                $total = $this->customerModel->getCount([]);
                $know = array_sum($age);
                array_push($age, $total-$know);
                $return['age'] = ['title' => $title, 'data' => $age];

                //终端机型
                $type_map['last_login_device'] = ['LIKE', '%Android%'];
                $terminal_type['android'] = $this->customerModel->getCount($type_map);//android
                $type_map['last_login_device'] = ['LIKE', '%iOS%'];
                $terminal_type['ios'] = $this->customerModel->getCount($type_map);//ios
                $return['terminal_type'] = $terminal_type;
                $this->redis->set('user_portrait_analysis', json_encode($return), 300);
            } else {
                $return = json_decode($redis_data, true);
            }
            $result = json_encode(['command' => 'A4', 'data' => $return]);
            $this->send($result);
        } catch (\Exception $e) {
            var_dump("A4获取用户画像发生异常:" . $e->getMessage());
        }

    }

//总数统计-A5
    public function statisticsTotal()
    {
        try {
            $redis_data = $this->redis->get('statistics_total_analysis');
            if (empty($redis_data)) {
                //工单总数
                $return['work_order_num'] = $this->workOrderModel->getCount([]);
                //订单总数
                $order_map['order_status'] = ['IN', [2, 3, 4, 6]];
                $return['order_num'] = $this->orderModel->count([]);
                //用户总数
                $return['user_num'] = $this->customerModel->getCount([]);
                //日均装机
                $device_total = $this->equipmentlistsModel->count([]);////主板总数
                //计算天数
                $day = (time() - 1548604800) / 86400;
                $return['daily_install_device'] = ceil($device_total / $day);
                $this->redis->set('statistics_total_analysis', json_encode($return), 300);
            } else {
                $return = json_decode($redis_data, true);
            }
            $result = json_encode(['command' => 'A5', 'data' => $return]);
            $this->send($result);
        } catch (\Exception $e) {
            var_dump("A5获取总数统计发生异常:" . $e->getMessage());
        }
    }

//滤芯更换周期-A6
    public function changeFilter()
    {
        $return['title'] = [getMonth(6), getMonth(5), getMonth(4), getMonth(3), getMonth(2), getMonth(1), getMonth(0)];
        $return['data'] = [10, 200, 30, 80, 100, 120, 12];
        $result = json_encode(['command' => 'A6', 'data' => $return]);
        $this->send($result);
    }

//城市TDS(取当前区域平均值)-A7
    public function cityTds()
    {
        try {
            $redis_data = $this->redis->get('citytds_analysis');
            if (empty($redis_data)) {
                mb_internal_encoding('UTF-8');
                $value = [];
                //重庆
                $map['province_code'] = 23;
                $map['city_code'] = 271;
                $cq_data = $this->equipmentlistsModel->selectEquipmentLists($map, 'count(*) as value,area as name,lat,lng,sum(water_purification_TDS) as total', [], 'area_code');
                foreach ($cq_data as $k => $v) {
                    $average = floor($v['total'] / $v['value']);
                    array_push($value, ['title' => mb_substr($v['name'], 0, -1), 'data' => $average]);
                }
                //四川
                $sc_map['province_code'] = 24;
                $sc_data = $this->equipmentlistsModel->selectEquipmentLists($sc_map, 'count(*) as value,city as name,lat,lng,sum(water_purification_TDS) as total', [], 'city_code');
                foreach ($sc_data as $k => $v) {
                    $average = floor($v['total'] / $v['value']);
                    array_push($value, ['title' => mb_substr($v['name'], 0, -1), 'data' => $average]);
                }
                $value = array_slice($value, 0, 8);//取前10
                $title = [];
                $data = [];
                if (!empty($value)) {
                    foreach ($value as $k => $v) {
                        array_push($title, $v['title']);
                        array_push($data, $v['data']);
                    }
                }
                $return['title'] = $title;
                $return['data'] = $data;
                $this->redis->set('citytds_analysis', json_encode($return), 300);
            } else {
                $return = json_decode($redis_data, true);
            }
            $result = json_encode(['command' => 'A7', 'data' => $return]);
            $this->send($result);
        } catch (\Exception $e) {
            var_dump("A7获取城市TDS发生异常:" . $e->getMessage());
        }
    }

//主板地域分布排行-A8
    public function deviceAreaDistribution()
    {

        try {
            $redis_data = $this->redis->get('device_area_distribution_analysis');
            if (empty($redis_data)) {
                $device_total = $this->equipmentlistsModel->count(['province_code' => ['IN', [23, 24]]]);//主板总数
                $value = [];
                //重庆
                $map['province_code'] = 23;
                $map['city_code'] = 271;
                $cq_data = $this->equipmentlistsModel->selectEquipmentLists($map, 'count(*) as value,area as name,lat,lng', [], 'area_code');

                if (!empty($cq_data)) {
                    //重组数据
                    foreach ($cq_data as $k => $v) {
                        $percent = ceil($v['value'] / $device_total * 100);
                        array_push($value, ['percent' => $percent, 'title' => $v['name']]);
                    }
                }
                //四川
                $sc_map['province_code'] = 24;
                $sc_data = $this->equipmentlistsModel->selectEquipmentLists($sc_map, 'count(*) as value,city as name,lat,lng', [], 'city_code');
                if (!empty($sc_data)) {
                    //重组数据
                    foreach ($sc_data as $k => $v) {
                        array_push($value, ['percent' => $percent, 'title' => $v['name']]);
                    }
                }
                $value = mymArrsort($value, 'percent', 'DESC');//根据占比排序
                $value = array_slice($value, 0, 10);//取前10
                $value = mymArrsort($value, 'percent', 'ASC');//根据占比排序
                $title = [];
                $data = [];
                if (!empty($value)) {
                    foreach ($value as $k => $v) {
                        array_push($title, $v['title']);
                        array_push($data, $v['percent']);
                    }
                }
                $return['title'] = $title;
                $return['data'] = $data;
                $this->redis->set('device_area_distribution_analysis', json_encode($return), 300);
            } else {
                $return = json_decode($redis_data, true);
            }
            $result = json_encode(['command' => 'A8', 'data' => $return]);
            $this->send($result);
        } catch
        (\Exception $e) {
            var_dump("A8获取主板地域分布发生异常:" . $e->getMessage());
        }
    }

//主板分布-A9
    public function deviceDistribution()
    {
        try {
            $redis_data = $this->redis->get('device_distribution_analysis');
            if (empty($redis_data)) {
                //重庆
                $map['province_code'] = 23;
                $map['city_code'] = 271;
                $cq_data = $this->equipmentlistsModel->selectEquipmentLists($map, 'status,lat,lng,device_no', []);
                $chongqing = [];
                if (!empty($cq_data)) {
                    foreach ($cq_data as $k => $v) {
                        $value = [$v['lng'], $v['lat'], $v['device_no'], $v['status']];
                        array_push($chongqing, ['name' => '', 'value' => $value]);
                    }
                }
                $return['chongqing'] = $chongqing;

                //四川
                $sc_map['province_code'] = 24;
                $cq_data = $this->equipmentlistsModel->selectEquipmentLists($sc_map, 'status,lat,lng,device_no', []);
                $sichuan = [];
                if (!empty($cq_data)) {
                    foreach ($cq_data as $k => $v) {
                        $value = [$v['lng'], $v['lat'], $v['device_no'], $v['status']];
                        array_push($sichuan, ['name' => '', 'value' => $value]);
                    }
                }
                $return['sichuan'] = $sichuan;
                $this->redis->set('device_distribution_analysis', json_encode($return), 300);
            } else {
                $return = json_decode($redis_data, true);
            }
            $result = json_encode(['command' => 'A9', 'data' => $return]);
            $this->send($result);
        } catch (\Exception $e) {
            var_dump("A9获取主板分布发生异常:" . $e->getMessage());
        }
    }


}
