<?php

namespace app\Controllers\User;

use app\Library\Alipay\aop\AopClient;
use app\Library\Alipay\aop\request\AlipayTradeCreateRequest;
use app\Services\Common\EmailService;
use app\Services\Common\HttpService;
use app\Services\Common\ReturnCodeService;
use app\Services\Common\SmsService;
use app\Services\Common\WxPayService;
use app\Services\Device\DeviceService;
use app\Services\Common\ConfigService;
use app\Services\Common\NotifiyService;
use app\Services\Common\CommonService;
use app\Services\Todo\ToDoListService;
use app\Services\User\UserService;
use app\Services\Stock\StockService;
use app\Wechat\WxAuth;
use Server\CoreBase\SwooleException;


/**
 * 订单模块API
 */
class Order extends Base
{

    protected $user_model;
    protected $order_model;
    protected $equip_model;
    protected $rent_package_model;
    protected $addr_model;
    protected $evaluate_model;
    protected $evaluate_label_model;
    protected $evaluate_to_label_model;
    protected $work_model;
    protected $order_invoice_model;
    protected $user_invoice_model;
    protected $order_logistics_model;
    protected $coupon_model;
    protected $equipment_lists_model;
    protected $bind_model;
    protected $finance_record_model;
    protected $equipment_model;
    protected $admin_user_model;
    protected $op_user_model;
    protected $administrative_user_model;
    private $user_center_model;
    protected $userService;
    protected $stockModel;
    protected $WorkOrderTimeModel;
    protected $WorkOrderModel;
    protected $contract_model;
    protected $work_order_business_model;
    protected $CustomerCodeMode;
    protected $Achievement;
    protected $CompanyConfigModel;
    protected $AdministrativeInfoModel;
    protected $OperationUserModel;
    protected $OperationInfoModel;
    protected $AdministrativeUserModel;
    protected $toDoListService;


    public function initialization($controller_name, $method_name)
    {
        parent::initialization($controller_name, $method_name);
        $this->CustomerCodeMode = $this->loader->model('CustomerModel',$this);
        $this->user_model = $this->loader->model('CustomerModel', $this);
        $this->order_model = $this->loader->model('OrderModel', $this);
        $this->equip_model = $this->loader->model('EquipmentModel', $this);
        $this->rent_package_model = $this->loader->model('RentalPackageModel', $this);
        $this->addr_model = $this->loader->model('UseraddressModel', $this);
        $this->evaluate_model = $this->loader->model('EvaluateModel', $this);
        $this->evaluate_label_model = $this->loader->model('EvaluateLabelModel', $this);
        $this->evaluate_to_label_model = $this->loader->model('EvaluateToLabelModel', $this);
        $this->work_model = $this->loader->model('WorkOrderModel', $this);
        $this->order_invoice_model = $this->loader->model('OrderInvoiceModel', $this);
        $this->user_invoice_model = $this->loader->model('UserinvoiceModel', $this);
        $this->order_logistics_model = $this->loader->model('OrderLogisticsModel', $this);
        $this->coupon_model = $this->loader->model('CouponModel', $this);
        $this->equipment_lists_model = $this->loader->model('EquipmentListsModel', $this);
        $this->bind_model = $this->loader->model('CustomerBindEquipmentModel', $this);
        $this->finance_record_model = $this->loader->model('FinanceRecordModel', $this);
        $this->equipment_model = $this->loader->model('EquipmentModel', $this);
        $this->admin_user_model = $this->loader->model('AdminUserModel', $this);
        $this->op_user_model = $this->loader->model('OperationUserModel', $this);
        $this->administrative_user_model = $this->loader->model('AdministrativeUserModel', $this);
        $this->user_center_model = $this->loader->model('CustomerAdministrativeCenterModel', $this);
        $this->stockModel = $this->loader->model('StockModel', $this);
        $this->WorkOrderTimeModel = $this->loader->model('WorkOrderTimeModel', $this);
        $this->WorkOrderModel = $this->loader->model('WorkOrderModel', $this);
        $this->work_order_business_model = $this->loader->model('WorkOrderBusinessModel', $this);
        $this->contract_model = $this->loader->model('ContractModel', $this);
        $this->CustomerCodeMode = $this->loader->model('CustomerCodeModel', $this);
        $this->Achievement = $this->loader->model('AchievementModel', $this);
        $this->CompanyConfigModel = $this->loader->model('CompanyConfigModel', $this);
        $this->OperationUserModel = $this->loader->model('OperationUserModel', $this);
        $this->OperationInfoModel = $this->loader->model('OperationInfoModel', $this);
        $this->AdministrativeInfoModel = $this->loader->model('AdministrativeInfoModel', $this);
        $this->AdministrativeUserModel = $this->loader->model('AdministrativeUserModel', $this);
        $this->userService = new UserService();
        $this->toDoListService = new ToDoListService();
    }


    /**
     * showdoc
     * Author: Xuleni
     * DateTime: 2020/9/9 9:57
     * @catalog API文档/用户端/订单相关
     * @title 预约订单
     * @description 预约订单支持市场推广报单
     * @method POST
     * @url User/Order/addOrder
     * @param id 可选 int 识别表ID，编辑时传入
     * @param shop_name 必选 string 店名
     * @param contact_name 必选 string 联系人姓名
     * @param contact_phone 必选 string 联系人电话
     * @param gas_fee 必选 int 每月燃气费用
     * @param province 必选 string 省份
     * @param province_code 必选 int 省份code
     * @param city 必选 string 市
     * @param city_code 必选 int 市code
     * @param area 必选 string 区
     * @param area_code 必选 int 区code
     * @param address 必选 string 详细地址
     * @param scene_type 必选 int 服务场景ID
     * @param source_customer_code 可选 int 户号,仅用户报单时存在此字段
     * @param stoves_number 必选 array 各炉具数量,fry_num:炒灶数量，big_pot_num:大锅灶数量，short_soup_num:矮汤灶数量，other_num:其他炉具数量
     * @param expect_transform_stoves_number 必选 array 预计改造炉具数量，fry_num:炒灶数量，big_pot_num:大锅灶数量，short_soup_num:矮汤灶数量
     * @param use_fan 必选 int 是否使用风机,1:是，2:否
     * @param survey_info 必选 array 识别信息-测量信息，二维json字符串，fry:炒灶，big_pot:大锅灶，short_soup:矮汤灶，每种锅两个数据,gas:燃气m³,time:时间/s
     * @param environment_pic 必选 array 环境照片，每个位置最多三张，json字符串，position_1,position_2,position_3表示位置1，位置2，位置3
     * @param environment_video 必选 string 环境视频
     * @remark  {"code": 1000,"message": "报单成功","data": 41}
     */
    public function http_addOrder()
    {
        //验证是否缺少必填参数
        $id = $this->parm['id'] ?? 0; //识别表ID
        $this->parm['scene_type'] = empty($this->parm['scene_type']) ? 0 : $this->parm['scene_type'];
        $this->parm['use_fan'] = empty($this->parm['use_fan']) ? 0 : $this->parm['use_fan'];
        if (empty($this->parm['contact_name']) ?? '') return $this->jsonend(ReturnCodeService::FAIL, '参数缺失:联系人');
        if (empty($this->parm['contact_phone']) ?? '') return $this->jsonend(ReturnCodeService::FAIL, '参数缺失:联系电话');
        if (!pregMatchInput($this->parm['contact_phone'], 'mobile_phone')) return $this->jsonend(ReturnCodeService::FAIL, '联系人手机号错误');
        if (empty($this->parm['shop_name']) ?? '') return $this->jsonend(ReturnCodeService::FAIL, '参数缺失:店名');
        //验证炉具预计改造数量不大于炉具总数
        //炉具总数
        $stoves_number['fry_num'] = $this->parm['stoves_number'][0]['fry_num']; //炒灶数量
        $stoves_number['big_pot_num'] = $this->parm['stoves_number'][1]['big_pot_num']; //大锅灶数量
        //预计改造炉具数据
        $expect_transform_stoves_number['fry_num'] = $this->parm['expect_transform_stoves_number'][0]['fry_num']; //炒灶数量
        $expect_transform_stoves_number['big_pot_num'] = $this->parm['expect_transform_stoves_number'][1]['big_pot_num']; //大锅灶数量
        $expect_transform_stoves_number = $expect_transform_stoves_number['fry_num'] + $expect_transform_stoves_number['big_pot_num'];
        if ($expect_transform_stoves_number <= 0) return $this->jsonend(ReturnCodeService::FAIL, '请输入预计改造数量');
        $expect_transform_stoves_number = $expect_transform_stoves_number['fry_num'] + $expect_transform_stoves_number['big_pot_num'];
        if ($expect_transform_stoves_number < 0) return $this->jsonend(ReturnCodeService::FAIL, '请输入预计改造数量');
        if ($expect_transform_stoves_number['fry_num'] > $stoves_number['fry_num']) {
            return $this->jsonend(ReturnCodeService::FAIL, '预约改造炒灶数量不能大于炒灶总数量');
        }
        if ($expect_transform_stoves_number['big_pot_num'] > $stoves_number['big_pot_num']) {
            return $this->jsonend(ReturnCodeService::FAIL, '预约改造大锅灶数量不能大于大锅灶总数量');
        }
        //验证户号是否存在
        $workers_id = 0;
        if ($this->role == 4 && !empty($this->parm['source_customer_code'])) {
            $code_data = $this->CustomerCodeMode->getOne(['code' => $this->parm['source_customer_code']], 'user_id');
            if (empty($code_data)) return $this->jsonend(ReturnCodeService::FAIL, '户号不存在');
            //查询市场推广
            $this->Achievement->table = 'oa_workers';
            $join = [['oa_workers_contract wb', 'rq_oa_workers.workers_id = wb.workers_id', 'LEFT']];
            $workers_data = $this->Achievement->findData(['rq_oa_workers.user_id' => $code_data['user_id']], 'wb.workers_id,wb.status', [], $join);
            if (empty($workers_data)) {
                //不存在则向上找
                $source_data = $this->user_model->getOne(['user_id' => $code_data['user_id']], 'source');
                $this->Achievement->table = 'oa_workers';
                $join = [['oa_workers_contract wb', 'rq_oa_workers.workers_id = wb.workers_id', 'LEFT']];
                $workers_data = $this->Achievement->findData(['rq_oa_workers.user_id' => $source_data['source']], 'wb.workers_id,wb.status', [], $join);
            }
//            if ($workers_data['status'] != 2) return $this->jsonend(ReturnCodeService::FAIL, '市场推广合同状态不正确');
            $workers_id = $workers_data['workers_id'];
        }
        //处理讲解人
        $explainers = [];
        if(!empty($this->parm['explainer'])){
            foreach($this->parm['explainer'] as $k=>$v){
                if(empty($v['telphone'])){
                    continue;
                }
                if(!isMobile($v['telphone'])){
                    return $this->jsonend(ReturnCodeService::FAIL, '请输入正确的讲解人手机号');
                }
                $explainer = $this->getExplainersByTel($v['telphone']);
                if(!$explainer){
                    return $this->jsonend(ReturnCodeService::FAIL, '讲解人手机号:'.$v['telphone'].'未在系统注册');
                }
                array_push($explainers,$explainer);
            }
        }
        if ($this->role == 6) {
            //市场推广报单
            $this->Achievement->table = 'oa_workers';
            $join = [['oa_workers_contract wb', 'rq_oa_workers.workers_id = wb.workers_id', 'LEFT']];
            $workers_data = $this->Achievement->findData(['rq_oa_workers.user_id' => $this->user_id], 'wb.workers_id,wb.status', [], $join);
            if (empty($workers_data)) return $this->jsonend(ReturnCodeService::FAIL, '市场推广申请信息不存在');
            if ($workers_data['status'] != 2) return $this->jsonend(ReturnCodeService::FAIL, '市场推广合同状态不正确');
            $workers_id = $workers_data['workers_id'];
        }
        //获取行政中心信息
        $admin_info = $this->userService->getOwnArea($this->user_id, '', $this->parm['province_code'], $this->parm['city_code'], $this->parm['area_code'], $this->company);
        if (!$admin_info) {
            return $this->jsonend(ReturnCodeService::FAIL, '该地址暂未开通服务');
        } else {
            $administrative_id = $admin_info['a_id'];
            $operation_id = $admin_info['operation'];
        }
        //查询用户名称
        $user_data = $this->user_model->getOne(['user_id' => $this->user_id], 'username,realname,telphone');
        //识别号
        $prefix = 'SJ'; //识别号前缀
        $reporter_role = 2; //角色
        $name = (empty($user_data['realname']) ? $user_data['username'] : $user_data['realname']);
        $telphone = $user_data['telphone'];
        $content = '【市场推广】推广员：' . '【' . $name . '】【' . $telphone . '】';
        if ($this->role == 4) {
            //查询经销商名称
            $this->Achievement->table = 'oa_workers';
            $oa_workers = $this->Achievement->findData(['user_id' => $this->user_id], 'workers_name,workers_phone');
            if (!empty($oa_workers)) {
                $name = $oa_workers['workers_name'];
                $telphone = $oa_workers['workers_phone'];
            }
            $prefix = 'SY';
            $reporter_role = 1;
            $content = '【用户发起】推广员：' . '【' . $name . '】【' . $telphone . '】';
        }
        //获取配置
        $is_relevant_information = $this->config->get('is_relevant_information');
        $_info_data = [
            'reporter_role' => $reporter_role, //报单人角色
            'administrative_id' => $administrative_id,
            'operation_id' => $operation_id,
            'reporter_uid' => $this->user_id,
            'workers_id' => $workers_id,
            'login_phone'=>$this->parm['login_phone'],
            'shop_name' => $this->parm['shop_name'],
            'contact_name' => $this->parm['contact_name'],
            'contact_phone' => $this->parm['contact_phone'],
            //  'gas_fee' => formatMoney($this->parm['gas_fee'], 1),
            'transform_province_code' => $this->parm['province_code'],
            'transform_province_name' => $this->parm['province'],
            'transform_city_code' => $this->parm['city_code'],
            'transform_city_name' => $this->parm['city'],
            'transform_area_code' => $this->parm['area_code'],
            'transform_area_name' => $this->parm['area'],
            'transform_detailed_address' => $this->parm['address'],
            'service_scene' => $this->parm['scene_type'],
            'source_customer_code' => $this->parm['source_customer_code'] ?? 0,
            'stoves_number' => json_encode($this->parm['stoves_number']),
            'expect_transform_stoves_number' => json_encode($this->parm['expect_transform_stoves_number']),
            'use_fan' => $this->parm['use_fan'],
            'survey_info' => json_encode($this->parm['survey_info']),
            'environment_pic' => json_encode($this->parm['environment_pic']),
            'environment_video' => $this->parm['environment_video'],
            'status' => $is_relevant_information == true ? 2 : 1, //true自动生成合同、工单相关数据,
            'explainer'=>implode(',',$explainers)
        ];
        $add_status = false;
        $identification_id = 0;
        $this->db->begin(function () use (&$workers_id,&$administrative_id, &$operation_id, &$is_relevant_information, &$add_status, &$_info_data, $name, $content, &$id, $prefix, &$identification_id) {
            if (!empty($id)) {
                $this->Achievement->table = 'identification_info';
                $this->Achievement->updateData(['id' => $id], $_info_data);
                $identification_id = $id;
                $content = $content . '编辑后发起';
                $open_remarks = '重新发起识别表';
                $info = $this->Achievement->findData(['id' => $identification_id], 'sn,create_time');
                $_info_data['sn'] = $info['sn'] ?? '';
                $_info_data['create_time'] = $info['create_time'] ?? '';
            } else {
                //如果自动生成合同、工单则新增用户
                if($is_relevant_information == true){
                    $_info_data['company_id'] = $this->company;
                    $add_user = $this->user_model->addUser($_info_data);
                    unset($_info_data['company_id']);
                }
                $_info_data['sn'] = CommonService::createSn('oven_work_order_no', $prefix);
                $_info_data['create_time'] = time();
                $this->Achievement->table = 'identification_info';
                $identification_id = $this->Achievement->insertData($_info_data);
                $content = $content . '发起识别表';
                $open_remarks = '发起识别表';
                if ($is_relevant_information == true) {
                    $this->parm['identification_id'] = $identification_id;
                    $this->parm['administrative_id'] = $administrative_id;
                    $this->parm['operation_id'] = $operation_id;
                    $this->parm['workers_id'] = $workers_id;
                    $this->parm['explainer'] = $_info_data['explainer'];
                    $this->parm['transform_stove_number'] = $this->parm['expect_transform_stoves_number'][0]['fry_num'] + $this->parm['expect_transform_stoves_number'][1]['big_pot_num'];
                    $this->parm['user_id'] = $add_user['related_id'] ?? 0;
                    $this->parm['code'] = $add_user['code'] ?? 0;
                    self::generate_relevant_information($this->parm);
                }
            }
            //添加轨迹
            $identification_log_data = ['identification_id' => $identification_id, 'open_remarks' => $open_remarks, 'content' => $content, 'log_type' => 1, 'operator_role' => $this->role, 'operator_uid' => $this->user_id, 'create_time' => time()];
            $this->Achievement->table = 'identification_log';
            $id = $this->Achievement->insertData($identification_log_data);
            $add_status = true;
        }, function ($e) {
            return $this->jsonend(ReturnCodeService::FAIL, '报单失败', $e->error);
        });
        if (!$add_status) {
            return $this->jsonend(ReturnCodeService::FAIL, '报单失败');
        }
        //发送模板消息
//        $notice_data['role'] = $reporter_role;
//        $notice_data['administrative_id'] = $administrative_id;
//        $notice_data['operation_id'] = $operation_id;
//        $notice_data['contact_name'] = $this->parm['shop_name'];
//        $notice_data['contact_phone'] = $this->parm['contact_phone'];
//        $notice_data['reporter'] = $name.'(电话:'.$telphone.')';
//        $this->addOrderTemplateMessage($notice_data);

        //新增待办===通知市场部审核
        $do_data['from_role'] = $reporter_role == 1 ? 9 : 8;
        $do_data['from_uid'] = $this->user_id;
        $do_data['role'] = 2;
        $do_data['uid'] = -1;
        $do_data['label'] = 2;
        $do_data['type'] = 1;
        $do_data['moudle'] = 1;
        $do_data['relation_id'] = $identification_id;
        $do_data['relation_sn'] = $_info_data['sn'];
        $notice_data['remark'] = '您有一份新的识别表需要审核！识别表单号：' . $_info_data['sn'] . '报单人:' . $name . '(电话:' . $telphone . ')';
        $this->toDoListService->add($do_data, 'I_CHECK', ['data' => $notice_data, 'openids' => $this->toDoListService->getAdminOpenid(5), 'company_id' => $this->company]);

        //发送短信通知给审核人员
        $key = 'identification_table';
        $content = $notice_data['remark'];
        SmsService::smsNotification($key, $content, $this->company);

        return $this->jsonend(ReturnCodeService::SUCCESS, '报单成功', $identification_id);
    }

    /**
     * 生成相关信息(合同、工单、工单类型、工单日志)
     */
    public function generate_relevant_information($data)
    {
        //新增合同数据
        $add_contract = [
            'contact_type' => 3,
            'company_id' => $this->company,
            'business_id' => 3,//改造
            'explainer'=>$data['explainer'] ?? '', //讲解人
            'workers_id'=>$data['workers_id'] ?? '', //推广人
            'shop_name'=>$data['shop_name'], //店铺名称
            'administrative_id' => $data['administrative_id'],//城市合伙人
            'operation_id' => $data['operation_id'],//运营合伙人
            'province' => $data['province'], //省
            'province_code' => $data['province_code'],//省code
            'city' => $data['city'],//市
            'city_code' => $data['city_code'],//市code
            'area' => $data['area'],//区
            'trial_stove_number'=>$data['trial_stove_number'] ?? 0,//试用炉具数量
            'transform_stove_number'=>$data['transform_stove_number'] ?? 0, //改造炉具数量
            'area_code' => $data['area_code'],//区code
            'address' => $data['address'], //详细地址
            'shop_name' => $data['shop_name'], //店铺名称
            'contact_person' => $data['contact_name'], //联系人
            'first_party_assign_contact_name'=>$data['contact_name'], //甲方指派联系人
            'first_party_assign_contact_phone'=>$data['contact_phone'], //甲方指派联系人电话
            'first_party_name'=>$data['shop_name'], //甲方名称(用能单位或服务费支付人)
            'first_party_address'=>$data['province'].$data['city'].$data['area'].$data['address'], //甲方通讯地址
            'first_party_contact_name'=>$data['contact_name'], //甲方联系人
            'first_party_contact_phone'=>$data['contact_phone'], //甲方联系人
            'related_name'=>$data['contact_name'], //甲方联系人
            'login_phone' => $data['login_phone'], //登陆电话
            'transform_end_time' => strtotime(date('Y-m-d', strtotime('+7days'))),
        ];
        //计算识别表炉具安装数量
        $expect_transform_stoves_number_sum = $data['expect_transform_stoves_number'][0]['fry_num'] + $data['expect_transform_stoves_number'][1]['big_pot_num'];

        //添加合同
        $add_contract['identification_id'] = $data['identification_id'];
        $operation_id = $data['operation_id'];
        $administrative_id = $data['administrative_id'];
        unset($data['operation_id']);
        unset($data['administrative_id']);
        $contract_id = $this->contract_model->addContract($add_contract, $data['user_id'], $administrative_id, $operation_id, $this->company);
        //新增工单数据
        $add_work_order = [
            'order_number' => CommonService::createSn('oven_work_order_no'),
            'company_id' => $this->company,
            'contract_id' => $contract_id,
            'user_id'=>$data['user_id'],
            'stove_number' => $expect_transform_stoves_number_sum,
            'work_order_status' => 3,//工单状态
            'operation_id' => $operation_id, //运营合伙人
            'administrative_id' => $administrative_id,//城市合伙人
            'contact_number' => $data['contact_phone'],
            'contacts' => $data['contact_name'],
            'reception_time' => time(),
            'province' => $data['province'],
            'province_code' => $data['province_code'],
            'city' => $data['city'],
            'city_code' => $data['city_code'],
            'area' => $data['area'],
            'appointment_start_time' => strtotime(date('Y-m-d H:i:s', time())),
            'appointment_end_time' => strtotime(date('Y-m-d H:i:s', strtotime(' +7days'))),
            'area_code' => $data['area_code'],
            'service_address' => $data['address'],
            'order_time'=>time(),
        ];
        $work_order_id = $this->work_model->add($add_work_order);
        //工单关联表
        $add_work_order_business = ['work_order_id' => $work_order_id, 'business_id' => 3];
        $this->work_order_business_model->add($add_work_order_business);
        //写入工单日志表
        $add_work_order_log = [
            'work_order_id' => $work_order_id,
            'create_work_time' => time(),
            'operating_time' => time(),
            'operating_type' => 0,
            'operating_status' => 3,
            'remarks' => '【系统】新增工单',
            'open_remarks' => '待指派工程人员',
        ];
        $this->work_log_model->add($add_work_order_log);
        $this->CustomerCodeMode->save(['code'=>$data['code']],['contract_id'=>$contract_id,'identification_id'=>$data['identification_id']]);
    }


    /**
     * @param
     * @return_param
     * @remark
     * @param administrative_id int 省市合伙人ID
     * @param operation_id int 区级合伙人ID
     * @param int $operation_id
     * @author：稍微等哈！
     * @date：2021-03-29 17:28
     * @catalog
     * @title 发送模板消息
     * @description
     * @method POST
     * @url
     */
    public function addOrderTemplateMessage($_info_data)
    {
        $user_data = $_info_data['contact_name'] . $_info_data['contact_phone'];
        //处理发送角色
        //区级
        $administrative_user_array = $this->AdministrativeUserModel->getOne(['a_id' => $_info_data['administrative_id']]);
        $notice_openid_array = [];
        if (!empty($administrative_user_array['notice_openid'])) {
            $notice_openid_array[] = $administrative_user_array['notice_openid'];
        }
        //市级
        $operation_info_array = $this->OperationInfoModel->getOne(['o_id' => $_info_data['operation_id']]);
        if (!empty($operation_info_array)) {
            //查询市级推送的openid
            $operation_user_array = $this->OperationUserModel->getOne(['o_id' => $_info_data['operation_id']]);
            if (!empty($operation_user_array['notice_openid'])) {
                $notice_openid_array[] = $operation_user_array['notice_openid'];
            }
            //查询省级推送openid
            if (!empty($operation_info_array['pid'])) {
                $p_operation_user_array = $this->OperationUserModel->getOne(['o_id' => $operation_user_array['pid']]);
                if (!empty($p_operation_user_array)) {
                    $notice_openid_array[] = $p_operation_user_array['notice_openid'];
                }
            }
        }
        if (!empty($notice_openid_array)) {
            foreach ($notice_openid_array as $key => $value) {
                $this->OrderTemplateMessage($user_data, $value, $_info_data['role'], $_info_data['reporter']);
            }
        }
    }


    public function OrderTemplateMessage($data, $openid, $reporter_role, $reporter)
    {
        $user_notice_config = ConfigService::getTemplateConfig('order_notice_admin', 'user', 1);
        if (!empty($user_notice_config) && !empty($user_notice_config['template']) && $user_notice_config['template']['switch']) {
            $template_id = $user_notice_config['template']['wx_template_id'];
            $mp_template_id = $user_notice_config['template']['mp_template_id'];
            $template_url = '';
            $weapp_template_keyword = '';
            $reporter_role_name = ($reporter_role == 1 ? '用户报单' : '市场推广报单');
            $mp_template_data = [
                'first' => ['value' => '您收到一条新的订单消息'],
                'tradeDateTime' => ['value' => date('Y-m-d H:i:s', time()), 'color' => '#4e4747'],
                'orderType' => ['value' => $reporter_role_name, 'color' => '#4e4747'],
                'customerInfo' => ['value' => $data, 'color' => '#4e4747'],
                'orderItemName' => ['value' => '市场推广', 'color' => '#4e4747'],
                'orderItemData' => ['value' => $reporter, 'color' => '#4e4747'],
                'remark' => ['value' => '请尽快登录后台处理', 'color' => '#173177'],
            ];

            $company_config_ = $this->CompanyConfigModel->getOne(['company_id' => 1]);
            $company_config['company_id'] = $company_config_['company_id'];
            $company_config['wx_config'] = json_decode($company_config_['wx_config'], true);
            $company_config['spread_wx_config'] = json_decode($company_config_['spread_wx_config'], true);
            $company_config['engineer_wx_config'] = json_decode($company_config_['engineer_wx_config'], true);
            $company_config['mp_config'] = json_decode($company_config_['mp_config'], true);
            $company_config['sms_config'] = json_decode($company_config_['sms_config'], true);

            $sendTemplateMessage = NotifiyService::sendNotifiy($openid, [], $template_id, '', '', $template_url, $weapp_template_keyword, $mp_template_id, $mp_template_data, 'user', $company_config);
            return $sendTemplateMessage;
        }

    }


    /**
     * showdoc
     * Author: Xuleni
     * DateTime: 2020/9/9 9:57
     * @catalog API文档/用户端/订单相关
     * @title 预约订单
     * @description 预约订单支持市场推广报单
     * @method POST
     * @url User/Order/query_code
     * @remark  {"code": 1000,"message": "报单成功","data": 41}
     */
    public
    function http_query_code()
    {
        $user_id = $this->user_id;
        if (empty($user_id)) return $this->jsonend(ReturnCodeService::FAIL, '参数缺失：用户编码');
        $this->Achievement->table = 'customer_bind_equipment';
        $where = ['rq_customer_bind_equipment.user_id' => $user_id, 'is_owner' => 2, 'e.state' => 1];
        $field = 'customer_code as code';
        $join = [['customer_code e', 'e.code = rq_customer_bind_equipment.customer_code']];
        $code_data = $this->Achievement->selectData($where, $field, $join);
        if (empty($code_data)) return $this->jsonend(ReturnCodeService::FAIL, '暂无数据');
        return $this->jsonend(ReturnCodeService::SUCCESS, '获取成功', $code_data);
    }


    /**
     * showdoc
     * Author: Xuleni
     * DateTime: 2020/9/9 9:57
     * @catalog API文档/用户端/订单相关
     * @title 识别表列表
     * @description
     * @method POST
     * @url User/Order/identification_info_list
     * @return_param id int 识别ID
     * @return_param sn int 识别编号
     * @return_param shop_name string 店名
     * @return_param contact_name string 联系人名称
     * @return_param contact_phone string 联系人电话
     * @return_param transform_province_name string 改造地址省名称
     * @return_param transform_city_name string 改造地址城市名称
     * @return_param transform_area_name string 改造地址区域名称
     * @return_param transform_detailed_address string 详细地址
     * @return_param address string 完整详细地址详细地址
     * @return_param create_time string 创建时间
     * @return_param check_time string 最后一次审核时间
     * @return_param check_remark string 最后一次审核备注
     * @return_param status int 状态编码1待审核、2审核通过、3审核拒绝
     * @return_param status_name string 状态
     * @remark  {"code": 1000,"message": "获取成功","data": []}
     */
    public
    function http_identification_info_list()
    {
        $page = $this->parm['page'] ?? 1;
        $pageSize = $this->parm['pageSize'] ?? 10;
        $keyword = $this->parm['keyword'] ?? '';
        if (!empty($keyword)) $where['shop_name|contact_name|contact_phone|transform_detailed_address'] = ['like', "%" . $keyword . "%"];
        $this->Achievement->table = 'identification_info';
        $reporter_role = 2; //角色
        if ($this->role == 4) $reporter_role = 1;
        $join = [
            ['contract t', 't.identification_id = rq_identification_info.id', 'left'],
            ['customer_code c', 'c.contract_id = t.contract_id', 'left'],
        ];
        $where = ['reporter_uid' => $this->user_id, 'reporter_role' => $reporter_role];
        $field = 't.status as contract_status,id,sn,rq_identification_info.check_time,rq_identification_info.check_remark,rq_identification_info.shop_name,contact_name,contact_phone,transform_province_name,transform_city_name,transform_area_name,transform_detailed_address,rq_identification_info.create_time,rq_identification_info.status,c.code';
        $identification_data = $this->Achievement->selectPageData($where, $field, $join, $pageSize, $page, ['create_time' => 'desc']);
        if (empty($identification_data)) return $this->jsonend(ReturnCodeService::FAIL, '暂无数据');
        $identification_info_status = $this->config->get('identification_info_status');
        foreach ($identification_data as $key => $value) {
            $identification_data[$key]['address'] = $value['transform_province_name'] . $value['transform_city_name'] . $value['transform_area_name'] . $value['transform_detailed_address'];
            $identification_data[$key]['create_time'] = date('Y-m-d H:i:s', $value['create_time']);
            $identification_data[$key]['check_time'] = empty($value['check_time']) ? '' : date('Y-m-d H:i:s', $value['check_time']);
            $identification_data[$key]['status_name'] = $identification_info_status[$value['status']];
        }
        return $this->jsonend(ReturnCodeService::SUCCESS, '获取成功', $identification_data);
    }


    /**
     * @title 识别表详情
     * showdoc
     * Author: Xuleni
     * DateTime: 2020/9/9 9:57
     * @catalog API文档/用户端/订单相关
     * @description
     * @method POST
     * @url User/Order/identification_info_details
     * @param id int 识别ID
     * @return_param id int 识别ID
     * @return_param sn int 识别编号
     * @return_param source_customer_code int 户号
     * @return_param shop_name string 店名
     * @return_param contact_name string 联系人名称
     * @return_param contact_phone string 联系人电话
     * @return_param gas_fee string 每月燃气费用
     * @return_param service_scene string 服务场景ID
     * @return_param service_scene_name string 服务场景
     * @return_param transform_province_name string 改造地址省名称
     * @return_param transform_city_name string 改造地址城市名称
     * @return_param transform_area_name string 改造地址区域名称
     * @return_param transform_detailed_address string 详细地址
     * @return_param address string 完整详细地址详细地址
     * @return_param create_time string 创建时间
     * @return_param status int 状态编码1待审核、2审核通过、3审核拒绝
     * @return_param status_name string 审核状态
     * @return_param survey_info array 识别信息
     * @return_param stoves_number array 各炉具数量
     * @return_param environment_pic array 环境照片
     * @return_param environment_pic array 环境照片完整路径
     * @return_param environment_video string 视频地址
     * @return_param environment_video_url string 视频地址完整路径
     * @return_param expect_transform_stoves_number array 预计改造炉具数量
     * @remark  {"code": 1000,"message": "获取成功","data": []}
     */
    public
    function http_identification_info_details()
    {
        if (empty($this->parm['id'])) return $this->jsonend(ReturnCodeService::FAIL, '参数缺失：识别ID');
        $this->Achievement->table = 'identification_info';
        $where = ['id' => $this->parm['id']];
        $field = 'source_customer_code,id,sn,shop_name,contact_name,contact_phone,gas_fee,service_scene,transform_province_name,transform_city_name,transform_area_name,transform_detailed_address,transform_province_code,transform_city_code,transform_area_code,create_time,status,stoves_number,expect_transform_stoves_number,use_fan,survey_info,environment_pic,environment_video,explainer';
        $identification_data = $this->Achievement->findData($where, $field, ['create_time' => 'desc']);
        if (empty($identification_data)) return $this->jsonend(ReturnCodeService::FAIL, '暂无数据');
        $service_scene = $this->config->get('scene_type');
        $identification_info_status = $this->config->get('identification_info_status');
        $identification_data['gas_fee'] = formatMoney($identification_data['gas_fee']);
        $identification_data['service_scene_name'] = '';
        foreach ($service_scene as $key => $value) { //服务场景
            if ($identification_data['service_scene'] == $value['id']) {
                $identification_data['service_scene_name'] = $value['name'];
                break;
            }
        }
        $identification_data['address'] = $identification_data['transform_province_name'] . $identification_data['transform_city_name'] . $identification_data['transform_area_name'] . $identification_data['transform_detailed_address'];
        $identification_data['status_name'] = $identification_info_status[$identification_data['status']];
        $identification_data['survey_info'] = json_decode($identification_data['survey_info'], true);
        $identification_data['stoves_number'] = json_decode($identification_data['stoves_number'], true);
        $identification_data['environment_pic'] = json_decode($identification_data['environment_pic'], true);
        $identification_data['environment_pic_url']['position_1'] = [];
        $identification_data['environment_pic_url']['position_2'] = [];
        $identification_data['environment_pic_url']['position_3'] = [];
        if (!empty($identification_data['environment_pic']['position_1'])) {
            foreach ($identification_data['environment_pic']['position_1'] as $k1 => $v1) {
                $identification_data['environment_pic_url']['position_1'][$k1] = UploadImgPath($v1);
            }
        }
        if (!empty($identification_data['environment_pic']['position_2'])) {
            foreach ($identification_data['environment_pic']['position_2'] as $k1 => $v1) {
                $identification_data['environment_pic_url']['position_2'][$k1] = UploadImgPath($v1);
            }
        }
        if (!empty($identification_data['environment_pic']['position_3'])) {
            foreach ($identification_data['environment_pic']['position_2'] as $k1 => $v1) {
                $identification_data['environment_pic_url']['position_3'][$k1] = UploadImgPath($v1);
            }
        }
        $identification_data['create_time'] = date('Y-m-d H:i:s', $identification_data['create_time']);
        $identification_data['environment_video_url'] = UploadImgPath($identification_data['environment_video'], 3);
        $identification_data['expect_transform_stoves_number'] = json_decode($identification_data['expect_transform_stoves_number'], true);
        //查询识别表轨迹
        $this->Achievement->table = 'identification_log';
        $log_where = ['identification_id' => $this->parm['id']];
        $log_field = 'content as remarks,open_remarks,create_time as operating_time';
        $identification_log_data = $this->Achievement->selectPageData($log_where, $log_field, [], 10000, 1, ['create_time' => 'desc']);
        if (!empty($identification_log_data)) {
            foreach ($identification_log_data as $key => $value) {
                $identification_log_data[$key]['operating_time'] = date('Y-m-d H:i:s', $value['operating_time']);
            }
        }
        $identification_data['work_order_log'] = $identification_log_data;
        $identification_data['explainer_info'] = $this->getExplainerInfo($identification_data['explainer']);
        return $this->jsonend(ReturnCodeService::SUCCESS, '获取成功', $identification_data);
    }


    /**
     * @title 编辑报单
     * showdoc
     * Author: Xuleni
     * DateTime: 2020/9/9 9:57
     * @catalog API文档/用户端/订单相关
     * @description 用户端编辑订单、报单
     * @method POST
     * @url User/Order/editOrder
     * @param work_order_id 必选 int 工单id
     * @param client 必选 int 1用户端5市场推广
     * @param equipments_id  必选 int 产品ID
     * @param contacts 必选 string 联系人姓名
     * @param appointment_date 可选 string 预约时间 年月日
     * @param appointment_time_id 可选 int 预约时间段ID
     * @param scene_type 可选 int 服务场景
     * @param cooker_type 可选 string 测量信息json格式。示例：[{"cooker_type":1,"water_quantity":5,"gas_consumption":0.1,"second":60},{"cooker_type":2,"water_quantity":5,"gas_consumption":0.1,"second":60}]。注释water_quantity：水量，gas_consumption：用气量，second：使用时间秒，cooker_type：炉具类型
     * @param cooker_brand 可选 string 炉具品牌
     * @param cooker_model_number 可选 string 炉具型号
     * @param cooker_num 可选 string 炉具数量
     * @param expected_renovation_num 可选 int 预计改造数量
     * @param cooker_purchase_time 可选 int 炉具购买年限
     * @param gas_fee 可选 int 平均每月缴费金额
     * @param province 必选 string 安装地址省
     * @param province_code 必选 int 安装地址省code
     * @param city 必选 string 安装地址市
     * @param city_code 必选 int 安装地址市code
     * @param area 必选 string 安装地址区
     * @param area_code 必选 int 安装地址区code
     * @param address 必选 string 安装详细地址
     * @param lat 必选 string 服务地址 纬度
     * @param lng 必选 string 服务地址 纬度
     * @param user_remarks 可选 string 用户备注
     * @param img 必选 string 环境照片json格式（必须大于三张）
     * @param video 必选 string 环境视频
     * @param draught_fan 可选 string 风机信息json格式。示例：[{"power":50,"focal_eye":1},{"power":50,"focal_eye":1}]。注释：power：功率、focal_eye：几灶眼
     * @remark  {"code": 1000,"message": "预约成功","data": 41}
     */
    public
    function http_editOrder()
    {
        //接收参数
        $work_order_id = $this->parm['work_order_id'] ?? '';
        $client = $this->parm['client'] ?? 1;
        $operater_role = 1;
        if ($client == 5) {
            $operater_role = 4;
        }
        if (empty($work_order_id)) {
            return $this->jsonend(ReturnCodeService::FAIL, '缺少参数');
        }
        //验证是否缺少必填参数
        if (empty($this->parm['contacts']) ?? '') {
            return $this->jsonend(ReturnCodeService::FAIL, '你还未填写联系人信息');
        }
        $cooker_num = empty($this->parm['cooker_num']) ? 0 : $this->parm['cooker_num'];
        if (!is_numeric($cooker_num)) {
            return $this->jsonend(ReturnCodeService::FAIL, '炉具数量只能为数字');
        }
        $expected_renovation_num = empty($this->parm['expected_renovation_num']) ? 0 : $this->parm['expected_renovation_num'];
        if (!is_numeric($expected_renovation_num)) {
            return $this->jsonend(ReturnCodeService::FAIL, '预计改造数量只能为数字');
        }
        $gas_fee = empty($this->parm['gas_fee']) ? 0 : $this->parm['gas_fee'];
        if (!is_numeric($gas_fee)) {
            return $this->jsonend(ReturnCodeService::FAIL, '每月的燃气费用只能是数字');
        }
        if (empty($this->parm['equipments_id'] ?? '')) {
            return $this->jsonend(ReturnCodeService::FAIL, '预约产品不存在');
        }
        if (empty($this->parm['province']) || empty($this->parm['city']) || empty($this->parm['area']) || empty($this->parm['address'])) {
            return $this->jsonend(ReturnCodeService::FAIL, '你还未选择安装地址');
        }
        if (empty($this->parm['scene_type'])) {
            return $this->jsonend(ReturnCodeService::FAIL, '请选择服务场景');
        }
        /* if (empty($this->parm['appointment_date']) && empty($this->parm['appointment_time_id'])) {
             return $this->jsonend(ReturnCodeService::FAIL, '请选择上门日期和时间');
         }
         //验证选择的时间段是否有效
         if (!empty($this->parm['appointment_date']) && !empty($this->parm['appointment_time_id'])) {
             $time_info = $this->WorkOrderTimeModel->getOne(['time_id' => $this->parm['appointment_time_id']]);
             if (empty($time_info)) return $this->jsonend(ReturnCodeService::FAIL, "预约时间不是有效时间段");
             //处理预约时间
             $time = $this->parm['appointment_date'] . ' ' . $time_info['start'] . '-' . $time_info['end'];
             $time_start = strtotime($this->parm['appointment_date'] . ' ' . $time_info['start']);
             $time_end = strtotime($this->parm['appointment_date'] . ' ' . $time_info['end']);
             if ($time_start < time()) {
                 return $this->jsonend(ReturnCodeService::FAIL, "预约时间不能小于当前时间");
             }
         }*/
        //查询工单信息
        $where['work_order_id'] = $work_order_id;
        /*if($client==5){
            $where['p_work_order_id']=$work_order_id;
        }*/
        $data = $this->work_model->getOne($where, 'work_order_id,user_id,order_id,contract_id,reception_user,reception_uid');
        if (empty($data)) {
            return $this->jsonend(ReturnCodeService::FAIL, "工单信息不存在");
        }
        /*if($data['reception_user'] !=$client || $data['reception_uid'] !=$this->user_id){
            return $this->jsonend(ReturnCodeService::FAIL, "很抱歉，您没有操作权限");
        }*/
        //验证商品是否有效
        $product_info = $this->equip_model->getDetail(array('equipments_id' => $this->parm['equipments_id']), 'is_shelves,equipments_money');
        if (empty($product_info)) return $this->jsonend(ReturnCodeService::FAIL, '产品不存在');
        if ($product_info['is_shelves'] == 2) return $this->jsonend(ReturnCodeService::FAIL, '该产品已下架,请选择其他产品');
        //判断工单来源

        //获取行政中心信息
        $admin_info = $this->userService->getOwnArea($data['user_id'], '', $this->parm['province_code'], $this->parm['city_code'], $this->parm['area_code'], $this->company);
        if (empty($admin_info)) {
            return $this->jsonend(ReturnCodeService::FAIL, '该地址暂未开通服务');
        } else {
            $administrative_id = $admin_info['a_id'];
            $operation_id = $admin_info['operation'];
        }
        //组装订单表数据
        $order_data = [
            'equipments_id' => $this->parm['equipments_id'], //产品编码
            'province' => $this->parm['province'], //安装地址省
            'city' => $this->parm['city'], //安装地址市
            'area' => $this->parm['area'], //安装地址区
            'province_code' => $this->parm['province_code'], //安装地址省code
            'city_code' => $this->parm['city_code'], //安装地址市code
            'area_code' => $this->parm['area_code'], //安装地址区code,
            'address' => $this->parm['address'], //安装详细地址
            'contact' => $this->parm['contacts'], //安装联系人
            'lat' => $this->parm['lat'], //服务地址 纬度
            'lng' => $this->parm['lng'], //服务地址 经度
            'equipments_num' => 1, //产品数量
            'equipments_single_money' => $product_info['equipments_money'], //产品单价
        ];
        //判断是否使用风机
        $is_fan = 1;
        if (empty($this->parm['draught_fan'])) $is_fan = 0;
        //组装炉具数据
        $cooker_information = [
            'is_fan' => $is_fan, //是否使用风机
            'img' => $this->parm['img'], //环境照片 json
            'video' => $this->parm['video'] ?? '', //视频地址
            'gas_fee' => formatMoney($this->parm['gas_fee'], 1), //平均每月缴费金额
            'cooker_num' => $this->parm['cooker_num'], //炉具数量
            'scene_type' => $this->parm['scene_type'], //服务场景
            'cooker_type' => $this->parm['cooker_type'] ?? [], //炉具类型
            'cooker_brand' => $this->parm['cooker_brand'], //炉具品牌
            'user_remarks' => $this->parm['user_remarks'] ?? '', //用户备注
            'repair_remarks' => '', //师傅备注
            'shop_name' => $this->parm['shop_name'], //店铺名称
            'cooker_model_number' => $this->parm['cooker_model_number'], //炉具型号
            'expected_renovation_num' => $this->parm['expected_renovation_num'], //预计改造数量
            'cooker_purchase_time' => $this->parm['cooker_purchase_time'] ?? 0, //炉具使用年限
            'draught_fan' => $this->parm['draught_fan'], //风机信息json串
        ];
        $work_data = [
            'work_order_status' => 1, //工单状态(必须要修改工单状态)
            'equipments_id' => $this->parm['equipments_id'], //产品ID
            'province' => $this->parm['province'], //服务地址省
            'city' => $this->parm['city'], //服务地址市
            'area' => $this->parm['area'], //服务地址区
            'province_code' => $this->parm['province_code'] ?? 0, //服务地址省code
            'city_code' => $this->parm['city_code'] ?? 0, //服务地址市code
            'area_code' => $this->parm['area_code'] ?? 0, //服务地址区code
            'service_address' => $this->parm['address'] ?? '', //服务详细地址
            'lat' => $this->parm['lat'], //服务地址纬度
            'lng' => $this->parm['lng'], //服务地址经度
            //'appointment_time_id' => $this->parm['appointment_time_id'], //预约时间段ID
            /*'appointment_time' => $time, //预约时间
            'appointment_start_time' => $time_start,//预约开始时间
            'appointment_end_time' => $time_end,//appointment_end_time*/
            'cooker_information' => json_encode($cooker_information), //炉具信息
            'administrative_id' => $administrative_id, //城市合伙人
            'operation_id' => $operation_id, //运营合伙人
            'update_time' => time(), //更新时间
            'contacts' => $this->parm['contacts'], //联系人
            'remarks' => ($client == 1 ? '用户端' : '推广') . '修改工单信息', //备注信息

        ];
        //勘测单没有合同查找对应新装单合同
        if ($client == 5 || $data['contract_id'] == null) {
            $work_info = $this->work_model->getOne(['p_work_order_id' => $data['work_order_id']], 'contract_id');
            if (!empty($work_info)) {
                $data['contract_id'] = $work_info['contract_id'];
            }
        }

        $add_status = false;
        $work_id = 0;
        $this->db->begin(function () use ($order_data, &$add_status, $work_data,/*$time,$time_start,$time_end,*/ $administrative_id, $operation_id, $operater_role, &$work_id, $data, $client) {

            //修改订单信息
            if (!empty($data['order_id'])) {
                $this->order_model->save(['order_id' => $data['order_id']], $order_data);

                $order_remark = '【用户端】客户ID为:' . $data['user_id'] . '】修改订单信息';
                if ($client == 5) {
                    $order_remark = '【推广】修改客户ID为:' . $this->user_id . '的订单信息';
                }
                //添加订单日志
                $this->addOrderLog($data['order_id'], 1, $order_remark, 0, $operater_role);

            }
            //添加用户与行政中心关系
            $this->userService->addUserCenter($data['user_id'], $this->parm['province_code'], $this->parm['city_code'], $this->parm['area_code'], $this->company);
            //修改工单信息------添加工单日志
            $this->WorkOrderModel->save(['work_order_id' => $data['work_order_id']], $work_data);
            $log['operating_type'] = 3;
            $log['remarks'] = '【用户端】客户ID为:' . $data['user_id'] . '】修改工单信息';
            if ($client == 5) {
                if (empty($this->parm['type'])) {
                    return $this->jsonend(ReturnCodeService::FAIL, '请选择合同类型');
                }
                $log['operating_type'] = 6;
                $log['remarks'] = '【推广】修改客户ID为:' . $data['user_id'] . '】的工单信息';
                $contract['type'] = $this->parm['type'] ?? 0;
            }
            $this->addWorkOrderLog($data['work_order_id'], $work_data['update_time'], 1, $log['remarks'], '', $log['operating_type']);

            //修改合同信息-------新增合同日志
            if (!empty($data['contract_id'])) {
                $contract['administrative_id'] = $administrative_id;
                $contract['operation_id'] = $operation_id;
                $contract['province'] = $work_data['province'] ?? '';
                $contract['city'] = $work_data['city'] ?? '';
                $contract['area'] = $work_data['area'] ?? '';
                $contract['province_code'] = $work_data['province_code'] ?? '';
                $contract['city_code'] = $work_data['city_code'] ?? '';
                $contract['area_code'] = $work_data['area_code'] ?? '';
                $contract['address'] = $this->parm['address'] ?? '';
                $contract['contact_person'] = $this->parm['contacts'];
                $contract['update_time'] = time();

                $contract_remark = '【用户端】客户ID为:' . $data['user_id'] . '】修改合同信息';
                if ($client == 5) {
                    $contract_remark = '【推广】修改客户ID为:' . $this->user_id . '的修改合同信息';
                }
                $contract['remark'] = $contract_remark;

                $this->contract_model->save(['contract_id' => $data['contract_id']], $contract);
                $this->addContractLog($data['contract_id'], 1, $contract_remark);
            }

            $add_status = true;
            $work_id = $data['work_order_id'];
        }, function ($e) {
            return $this->jsonend(-1000, "操作失败" . $e->error);
        });

        if ($add_status) {
            return $this->jsonend(ReturnCodeService::SUCCESS, '操作成功', $work_id);
        }
        return $this->jsonend(ReturnCodeService::FAIL, '操作失败');
    }


    //4对公转账回调
    public
    function transferOrder($order_id)
    {
        if (empty($order_id)) {
            return false;
        }

        //查询订单
        $order = $this->order_model->getOne(['order_id' => $order_id], '*');

        if (empty($order)) {
            return false;
        }
        $status = false;
        $this->db->begin(function () use ($order, &$status) {
            //修改订单状态,如果走发货流程 order_status=2 待发货 反之 order_status=3 待安装
            $order_status = 3;//待安装
            $deliver_way = 0;//未发货
            if ($order['order_type'] == 2) {//购买订单
                $order_status = 2;//待发货
                $deliver_way = 2;//不需物流
            }
            //如果已经是已安装不修改订单状态
            if ($order['order_status'] == 4) {
                $order_status = 4;
            }
            $this->order_model->save(['order_id' => $order['order_id']], ['order_status' => $order_status, 'pay_status' => 2, 'pay_time' => time(), 'wx_callback_num' => $order['wx_callback_num'], 'deliver_way' => $deliver_way]);
            //添加订单日志
            $this->addOrderLog($order['order_id'], $order_status, "您的订单已支付成功,我们将尽快进行处理,订单编号:" . $order['order_no'], 1);
            //添加用户与行政中心关系
            $this->userService->addUserCenter($order['user_id'], $order['province_code'], $order['city_code'], $order['area_code'], $order['company_id']);
            //添加物流信息
            $this->addOrderLogistics($order['order_id'], 1, "您的订单开始处理", $order['user_id']);
            //新增资金记录
            $finance_record['type'] = 1;
            $finance_record['order_id'] = $order['order_id'];
            $finance_record['user_id'] = $order['user_id'];
            $finance_record['money'] = formatMoney($order['order_actual_money'], 1);
            $finance_record['create_time'] = time();
            $finance_record['is_online_pay'] = 2;//线下支付
            $finance_record['pay_way'] = 4;
            $finance_record['callback_num'] = '';
            $finance_record['payment_method'] = 1;
            $finance_record['payment_uid'] = $order['user_id'];
            $finance_record['o_id'] = $order['operation_id'];
            $finance_record['a_id'] = $order['administrative_id'];
            $this->finance_record_model->add($finance_record);
            $status = true;
        });
        if ($status) {
            $user_info = $this->user_model->getOne(array('user_id' => $order['user_id']), 'openid,telphone,user_id,source');
            $eq_info = $this->equipment_model->getDetail(array('equipments_id' => $order['equipments_id']), 'equipments_name,model');
            if (empty($user_info) || empty($eq_info)) {
                return $this->response->end('fail');
            }
            $a_info = $this->userService->getOwnArea($user_info['user_id'], $user_info['source'], $order['province_code'], $order['city_code'], $order['area_code'], $this->company);
            //发邮件通知
            if (1 == 0) {//暂时关闭此功能
                $order_time = date('Y-m-d H:i:s', $order['create_time']);
                $opertaor = $this->config->get('order_opertaor')[$order['opertaor']];//订单来源
                $content = '<h2>新订单通知</h2><br/><div>订单编号:' . $order['order_no'] . '</div><div>订单来源:' . $opertaor . '</div><div>客户电话:' . $user_info['telphone'] . '</div><div>商品名称:' . $eq_info['equipments_name'] . '(数量:' . $order['equipment_num'] . ')</div><div>订单金额:' . $order['order_actual_money'] . '元</div><div>下单时间:' . $order_time . '</div><div style="color:#808080">客户已完成支付,请进入管理后台查看详情!</div>';
                //总后台
                $admin_email = $this->admin_user_model->getOne(['is_admin' => 1, 'is_delete' => 0], 'notice_email');
                if (!empty($admin_email) && !empty($admin_email['notice_email'])) {
                    EmailService::sendEmailAsync($admin_email['notice_email'], $content, "有新订单啦", '', '', '', $order['company_id']);
                }
                if (!empty($a_info)) {
                    //对应运营中心
                    $op_email = $this->op_user_model->getOne(['o_id' => $a_info['operation'], 'is_delete' => 0, 'is_admin' => 1], 'notice_email');
                    if (!empty($op_email) && !empty($op_email['notice_email'])) {
                        EmailService::sendEmailAsync($op_email['notice_email'], $content, "有新订单啦", '', '', '', $order['company_id']);
                    }
                    //对应合伙人
                    $administrative_email = $this->administrative_user_model->getOne(['a_id' => $a_info['a_id'], 'is_delete' => 0, 'is_admin' => 1], 'notice_email');
                    if (!empty($administrative_email) && !empty($administrative_email['notice_email'])) {
                        EmailService::sendEmailAsync($administrative_email['notice_email'], $content, "有新订单啦", '', '', '', $order['company_id']);
                    }
                }
            }
            return true;
        }
        return false;

    }


    /**
     * showdoc
     * @catalog API文档/用户端/订单相关
     * @title 订单支付
     * @description
     * @method POST
     * @url User/Order/pay
     * @param order_id 必选 int 订单ID
     * @param pay_way 可选 int 支付方式1微信支付，默认1当 2:支付宝支付
     * @return
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public
    function http_pay()
    {

        if (empty($this->parm['order_id'] ?? '')) {
            return $this->jsonend(-1001, "请选择订单");
        }
        if (empty($this->parm['pay_way'] ?? '')) {
            return $this->jsonend(-1001, "请选择支付方式");
        }
        if (!in_array($this->parm['pay_way'], [1, 2, 3, 4])) {
            return $this->jsonend(-1103, "支付方式错误");
        }

        //查询订单
        $order_info = $this->order_model->getOne(array('order_id' => $this->parm['order_id']), '*');
        if (empty($order_info)) {
            return $this->jsonend(-1101, "订单不存在");
        }
        if ($order_info['order_status'] != 1 && $order_info['pay_status'] == 2) {
            return $this->jsonend(-1102, "该订单已经支付过,不能重复支付");
        }
        //用户信息
        $user_info = $this->user_model->getOne(array('user_id' => $this->user_id), 'openid,ali_uid');
        if ($this->parm['pay_way'] == 1) {//微信
            if (empty($user_info) || empty($user_info['openid'])) {
                return $this->jsonend(-1103, "用户信息错误");
            }
        } else if ($this->parm['pay_way'] == 2) {//支付宝
            if (empty($user_info) || empty($user_info['ali_uid'])) {
                return $this->jsonend(-1103, "用户信息错误");
            }
            $order_info['buyer_id'] = $user_info['ali_uid'];
        } else if ($this->parm['pay_way'] == 4) {//对公转账
            if (empty($user_info)) {
                return $this->jsonend(-1103, "用户信息错误");
            }
        }

        $add_status = false;
        $jsapi = '';
        $this->db->begin(function () use (&$add_status, &$jsapi, $order_info, $user_info) {
            $order_data['pay_way'] = $this->parm['pay_way'];
            $order_data['pay_mode'] = $this->pay_config['mode'] ?? 1;
            $order_data['pay_mchid'] = $this->pay_config['mode'] == 2 ? $this->pay_config['sub_mch_id'] : $this->wx_config['mchid'];
            $this->order_model->save(array('order_id' => $this->parm['order_id']), $order_data);
            //调起微信支付
            if ($this->parm['pay_way'] == 1) {
                $pay_data['order_sn'] = $order_info['order_no'];
                $pay_data['money'] = $order_info['order_actual_money'];
                $notify_url = $this->config->get('callback_domain_name') . '/User/Callback/rentOrder';
                if ($this->dev_mode == 1) {
                    $pay_data['money'] = 0.01;
                    $notify_url = $this->config->get('debug_config.callback_domain_name') . '/User/Callback/rentOrder';
                }
                $jsapi = WxPayService::pay($this->wx_config, $user_info['openid'], $pay_data, $notify_url, $this->pay_config['sub_mch_id'] ?? '', $this->pay_config['mode'] ?? 1);

            } else if ($this->parm['pay_way'] == 2) {//支付宝支付
                $response = $this->aliPay($order_info, 'User/Callback/ali_rentOrder');
                $resultCode = $response->code;
                if (!empty($resultCode) && $resultCode == 10000) {
                    echo "成功";
                    $responseData = [];
                    $responseData['order_id'] = $this->parm['order_id'];
                    $responseData['out_trade_no'] = $response->out_trade_no;
                    $responseData['trade_no'] = $response->trade_no;

                    return $this->jsonend(1000, '操作成功', $responseData);
                } else {
                    echo "失败";
                    return $this->jsonend(-1000, '支付失败');
                }
            } else if ($this->parm['pay_way'] == 3) {//百度小程序支付
                $jsapi = $this->baiduPay($order_info);

            } else if ($this->parm['pay_way'] == 4) {//对公转账
                $result = $this->transferOrder($order_info['order_id']);
                if ($result) {
                    $add_status = true;
                }
            }
            $add_status = true;
        });
        if ($add_status) {
            $order_data['order_id'] = $this->parm['order_id'];
            $order_data['jsapi'] = $jsapi;
            return $this->jsonend(1000, '操作成功', $order_data);
        }
        return $this->jsonend(-1000, '操作失败');
    }

    /**
     * 支付宝支付
     * @param type $order
     */
    public
    function aliPay($order, $url)
    {
        $aop = new AopClient ();
        $aop->gatewayUrl = $this->alipay['gatewayUrl'];
        $aop->appId = $this->alipay['appId'];
        $aop->rsaPrivateKey = $this->alipay['rsaPrivateKey'];
        $aop->alipayrsaPublicKey = $this->alipay['alipayrsaPublicKey'];
        $aop->apiVersion = '1.0';
        $aop->signType = 'RSA2';
        $aop->postCharset = 'UTF-8';
        $aop->format = 'json';
        $request = new AlipayTradeCreateRequest();
        if ($this->dev_mode) {
            $notifyUrl = $this->config->get('debug_config.callback_domain_name');
        } else {
            $notifyUrl = $this->config->get('callback_domain_name');
        }
        $request->setNotifyUrl($notifyUrl . '/' . $url);
        $payData = [];
        $payData['out_trade_no'] = $order['order_no'];
        $payData['total_amount'] = $order['order_actual_money'];
        if ($this->app_debug == 1) {
            $payData['total_amount'] = 0.01;
        }
        $payData['subject'] = '购买商品';
        $payData['buyer_id'] = $order['buyer_id'];
        $payData = json_encode($payData);
        $request->setBizContent($payData);
        $result = $aop->execute($request);

        $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
        return $result->$responseNode;
    }

    //百度支付
    public
    function baiduPay($order_info)
    {
        $this->http_output->setHeader('Content-type: text/html', 'charset=utf-8');
        require_once APP_DIR . '/Library/baiduWeapp/nuomi-openplatform-php-sdk-1.0.3/Autoloader.php';
        if (empty($order_info)) {
            return false;
        }
        /**
         * 第一部分：从公私钥文件路径中读取出公私钥文件内容
         */
        $rsaPrivateKeyFilePath = APP_DIR . '/Library/baiduWeapp/rsa/rsa_private_key.pem';
        $rsaPublicKeyFilePath = APP_DIR . '/Library/baiduWeapp/rsa/rsa_public_key.pem';

        if (!file_exists($rsaPrivateKeyFilePath) || !is_readable($rsaPrivateKeyFilePath) ||
            !file_exists($rsaPublicKeyFilePath) || !is_readable($rsaPublicKeyFilePath)) {
            return false;
        }
        $rsaPrivateKeyStr = file_get_contents($rsaPrivateKeyFilePath);
        $rsaPublicKeyStr = file_get_contents($rsaPublicKeyFilePath);

        $tpOrderId = $order_info['order_no'];
        $totalAmount = 1;//订单金额
        if ($this->app_debug != 1) {
            $totalAmount = intval(round($order_info['order_actual_money'] * 100));
        }

        /**
         * 第二部分：生成签名 DEMO
         */
        $requestParamsArr = array(
            'appKey' => $this->baidu_weapp_config['appKey'],
            'dealId' => $this->baidu_weapp_config['dealId'],
            'tpOrderId' => $tpOrderId,
            'totalAmount' => $totalAmount
        );
        $rsaSign = \NuomiRsaSign::genSignWithRsa($requestParamsArr, $rsaPrivateKeyStr);
        $cashierTopBlock = [];
        $tpData['displayData'] = ['cashierTopBlock' => $cashierTopBlock];
        $tpData['appKey'] = $this->baidu_weapp_config['appKey'];
        $tpData['dealId'] = $this->baidu_weapp_config['dealId'];
        $tpData['tpOrderId'] = $tpOrderId;
        $tpData['rsaSign'] = $rsaSign;
        $tpData['totalAmount'] = $totalAmount;
        $tpData['returnData'] = [];

        $data["dealId"] = $this->baidu_weapp_config['dealId'];
        $data["appKey"] = $this->baidu_weapp_config['appKey'];
        $data["totalAmount"] = $totalAmount;
        $data["tpOrderId"] = $tpOrderId;
        $data["dealTitle"] = "订单支付";
        $data["signFieldsRange"] = "1";
        $data["rsaSign"] = $rsaSign;
        $data["bizInfo"] = ['tpData' => $tpData];

        return $data;
    }


    /**
     * showdoc
     * @catalog API文档/用户端/订单相关
     * @title 获取订单详情
     * @description
     * @method POST
     * @url User/Order/getOrderDetail
     * @param order_id 必选 int 订单ID
     * @return "data":{"order_id":"3","order_no":"CQR917731088221618","user_id":"3","order_type":"1","business_type_id":"3","express_name":null,"express_number":null,"warranty_start_time":null,"warranty_end_time":null,"equipments_id":"1","administrative_id":"23","operation_id":"1","package_mode":"1","package_value":"55","package_money":"600.00","package_cycle":"180","province":"重庆市","city":"重庆市","area":"渝北区","province_code":"23","city_code":"271","area_code":"2503","address":"加州城市花园12栋11-6","contact":"傅丽宇","contact_tel":"18324191816","lat":"29.59344","lng":"106.51857","order_status":"3","pay_status":"2","logistics_status":null,"is_show":"1","wx_callback_num":"4200000164201809173918037104","pay_way":"1","create_time":"2018-09-17 16:31:48","pay_time":"2018-09-17 16:32:16","cancel_time":null,"equipment_num":"1","equipment_money":"600.00","equipment_total_money":"600.00","coupon_id":null,"coupon_money":null,"single_deposit":"400.00","total_deposit":"400.00","order_total_money":"1000.00","order_actual_money":"1000.00","remarks":"","is_invoice":"1","opertaor":"1","delivergoods_time":"","collectgoods_time":"","main_pic":"https:\/\/qn.youheone.com\/a037f201809171621201333.jpg","equipments_name":"史密斯牌DR75-C5型直饮水机","model":"Product 7","scale":"台","book_status":1,"book_work_order_id":"2","order_status_name":"待安装"}
     * @return_param order_id int 订单ID
     * @return_param order_type int 订单类型1租赁订单2购买订单3积分订单
     * @return_param order_no string 订单编号
     * @return_param equipment_num string 产品数量i
     * @return_param order_status_name string 订单状态
     * @return_param order_total_money float 订单总价
     * @return_param package_mode int 套餐模式 1时长 2流量
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public
    function http_getOrderDetail()
    {
        if (empty($this->parm['order_id'] ?? '')) {
            return $this->jsonend(-1001, "缺少参数订单ID");
        }
        $map['order_id'] = $this->parm['order_id'];
        $join = [
            ['equipments as e', 'e.equipments_id = rq_orders.equipments_id', 'inner']   // 主板分类表对应订单表
        ];
        $data = $this->order_model->getOne($map, 'rq_orders.*,e.main_pic,e.equipments_name,e.model,e.scale', $join);
        if (empty($data)) {
            return $this->jsonend(-1003, "没有相关订单信息");
        }
        $data['delivergoods_time'] = strlen($data['delivergoods_time']) > 5 ? date('Y-m-d H:i:s', $data['delivergoods_time']) : '';
        $data['collectgoods_time'] = strlen($data['collectgoods_time']) > 5 ? date('Y-m-d H:i:s', $data['collectgoods_time']) : '';
        $data['create_time'] = date('Y-m-d H:i:s', $data['create_time']);
        $data['pay_time'] = empty($data['pay_time']) ? '' : date('Y-m-d H:i:s', $data['pay_time']);
        if ($data['package_mode'] == 2) {
            $data['package_value'] = empty($data['package_value']) ? '' : number_format($data['package_value'] / 1000, 3);
        }
        //是否预约安装
        $work_order = $this->work_model->getWorkDetail(array('wb.business_id' => 3, 'order_id' => $data['order_id']), [['work_order_business wb', 'rq_work_order.work_order_id = wb.work_order_id', 'LEFT']], 'rq_work_order.work_order_id');
        $data['book_status'] = 0;
        $data['book_work_order_id'] = 0;
        if (!empty($work_order)) {
            $data['book_status'] = 1;
            $data['book_work_order_id'] = $work_order['work_order_id'];
        }
        //发票信息
        if ($data['is_invoice'] == 2) {
            $data['invoice_info'] = $this->order_invoice_model->getOne(array('order_id' => $this->parm['order_id']), '*');
        }
        $data['main_pic'] = $this->config->get('qiniu.qiniu_url') . $data['main_pic'];
        $data['order_status_name'] = $this->config->get('order_status')[$data['order_status']];
        if ($data['order_status'] == 4 && $data['is_show'] == 1) {
            $data['order_status_name'] = '待评价';
        } else if ($data['order_status'] == 4 && $data['is_show'] == 2) {
            $data['order_status_name'] = '已评价';
        }
        return $this->jsonend(1000, "获取数据成功", $data);
    }

    /**
     * showdoc
     * @catalog API文档/用户端/订单相关
     * @title 获取用户订单列表
     * @description 支持分页，状态查询
     * @method POST
     * @url User/Order/getOrderList
     * @param page 可选 int 页数，默认1
     * @param pageSize 可选 int 每页条数，默认10
     * @param order_status 可选 int 状态ID,默认全部 1待付款 2待发货 3待安装 4已安装5已取消 6待收货
     * @return {"code":1000,"message":"获取订单列表成功","data":[{"order_id":"83","order_type":"2","order_no":"CQRA10477995674933","equipment_num":"1","order_total_money":"1598.00","order_status":"3","user_id":"6","package_mode":"1","package_value":"36500","package_money":"1598.00","package_cycle":"36500","main_pic":"https:\/\/qn.youheone.com\/d8c61201810081031598328.jpg","equipments_name":"测试产品202","scale":"台","model":"product2","is_show":"1","book_status":1,"book_work_order_id":"104","order_status_name":"待安装"}]}
     * @return_param order_id int 订单ID
     * @return_param order_type int 订单类型1租赁订单 2购买订单 3积分订单
     * @return_param order_no string 订单编号
     * @return_param equipment_num string 产品数量
     * @return_param order_status_name string 订单状态
     * @return_param order_total_money float 订单总价
     * @return_param package_mode int 套餐模式 1时长 2流量
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public
    function http_getOrderList()
    {
        // 接收参数
        $user_id = $this->user_id;
        $page = $this->parm['page'] ?? 1;
        $pageSize = $this->parm['pageSize'] ?? 10;
        $order_status = $this->parm['order_status'] ?? '';

        // 连表
        $join = [
            ['equipments as e', 'e.equipments_id = rq_orders.equipments_id', 'inner']   // 主板分类表对应订单表
        ];
        // 查询条件
        $where['rq_orders.user_id'] = $user_id;
        // 判断是否传订单状态
        if (!empty($order_status)) {
            $where['rq_orders.order_status'] = $order_status;
        }
        //是否评价 1否 2是
        if (!empty($this->parm['is_show'] ?? '')) {
            $where['is_show'] = $this->parm['is_show'];
            if (empty($order_status)) {
                $where['order_status'] = array('not in', [1]);
            }
        }
        // 查询字段
        $field = 'rq_orders.order_id,order_type,rq_orders.order_no,rq_orders.order_total_money,rq_orders.order_status,rq_orders.user_id,
                  e.main_pic,e.equipments_name,is_show';
        // 排序方式
        $order = ['rq_orders.create_time' => 'DESC'];
        // 调用模型获取订单列表
        $orderData = $this->order_model->getAll($where, $page, $pageSize, $field, $join, $order);
        // 判断结果--返回
        if ($orderData) {
            foreach ($orderData as $k => $v) {
                $orderData[$k]['main_pic'] = $this->config->get('qiniu.qiniu_url') . $v['main_pic'];
                //是否预约安装
                $work_order = $this->work_model->getWorkDetail(array('wb.business_id' => 3, 'order_id' => $v['order_id']), [['work_order_business wb', 'rq_work_order.work_order_id = wb.work_order_id', 'LEFT'], ['contract', 'rq_work_order.contract_id = rq_contract.contract_id', 'LEFT']], 'rq_work_order.work_order_id,rq_contract.contract_id,rq_contract.status AS contract_status');
                $orderData[$k]['contract_status'] = $work_order['contract_status'] ?? 0;
                $orderData[$k]['contract_id'] = $work_order['contract_id'] ?? '';
                $orderData[$k]['book_status'] = 0;
                $orderData[$k]['book_work_order_id'] = 0;
                if (!empty($work_order)) {
                    $orderData[$k]['book_status'] = 1;
                    $orderData[$k]['book_work_order_id'] = $work_order['work_order_id'];
                }
                //订单状态
                $orderData[$k]['order_status_name'] = $this->config->get('order_status')[$v['order_status']];
                if ($v['order_status'] == 4 && $v['is_show'] == 1) {
                    $orderData[$k]['order_status_name'] = '待评价';
                } else if ($v['order_status'] == 4 && $v['is_show'] == 2) {
                    $orderData[$k]['order_status_name'] = '已评价';
                }
            }

            return $this->jsonend(1000, '获取订单列表成功', $orderData);
        } else {
            return $this->jsonend(-1000, '暂无订单', $orderData);
        }
    }

    /**
     * showdoc
     * @catalog API文档/用户端/订单相关
     * @title 根据状态统计订单
     * @description
     * @method POST
     * @url User/Order/getOrderStatus
     * @return {"code":1000,"message":"获取订单状态数量成功","data":{"n_pay":0,"substituting":1,"n_install":2,"y_install":1,"collection":0,"n_evaluate":1}}
     * @return_param n_pay int 待支付订单数量
     * @return_param substituting int 待发货订单数量
     * @return_param n_install int 待安装订单数量
     * @return_param y_install int 已安装订单数量
     * @return_param collection int 待收货订单数量
     * @return_param n_evaluate int 待评价订单数量
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public
    function http_getOrderStatus()
    {
        // 接收参数
        $user_id = $this->user_id;

        // 获取用户四种状态
        // 待付款--条件
        $where = ['order_status' => 1, 'user_id' => $user_id];
        $orderStatusNum['n_pay'] = $this->order_model->OrderStatusNum($where);
        unset($where);
        // 待发货--条件
        $where = ['order_status' => 2, 'user_id' => $user_id];
        $orderStatusNum['substituting'] = $this->order_model->OrderStatusNum($where);
        unset($where);
        // 待安装--条件
        $where = ['order_status' => 3, 'user_id' => $user_id];
        $orderStatusNum['n_install'] = $this->order_model->OrderStatusNum($where);
        unset($where);
        // 已安装--条件
        $where = ['user_id' => $user_id, 'order_status' => 4];
        $orderStatusNum['y_install'] = $this->order_model->OrderStatusNum($where);
        unset($where);
        // 待收货--条件
        $where = ['user_id' => $user_id, 'order_status' => 6];
        $orderStatusNum['collection'] = $this->order_model->OrderStatusNum($where);
        unset($where);
        // 待评价--条件
        $where = ['rq_orders.user_id' => $user_id, 'order_status' => 4, 'is_show' => 1];
        // 连表
        $join = [
            ['evaluate e', 'e.order_id = rq_orders.order_id', 'left']
        ];
        $orderStatusNum['n_evaluate'] = $this->order_model->count($where, $join);

        // 判断结果--返回
        if ($orderStatusNum) {
            return $this->jsonend(1000, '获取订单状态数量成功', $orderStatusNum);
        } else {
            return $this->jsonend(-1000, '获取订单状态数量失败', $orderStatusNum);
        }
    }

    /**
     * showdoc
     * @catalog API文档/用户端/订单相关
     * @title 获取评价标签
     * @description
     * @method POST
     * @url User/Order/getEvaluateLabel
     * @param type 可选  int 类型 1对订单评价表桥 2对工程人员评价标签,默认1
     * @return {"code":1000,"message":"获取数据成功","data":[{"label_id":"1","label_name":"好评","label_type":"1","create_time":"1537179443","status":"1","sort":"0","is_del":"0"}]}
     * @return_param label_id int 标签ID
     * @return_param label_name int 标签名称
     * @return_param label_type int 标签类型 1对订单评价表桥 2对工程人员评价标签
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public
    function http_getEvaluateLabel()
    {
        //$map['label_type'] = empty($this->parm['type'] ?? '') ? 1 : $this->parm['type'];
        $map['status'] = 1;
        $data = $this->evaluate_label_model->getAll($map, '*');
        if (empty($data)) {
            return $this->jsonend(-1003, "暂无相关数据");
        }
        return $this->jsonend(1000, "获取数据成功", $data);
    }

    /**
     * showdoc
     * @catalog API文档/用户端/订单相关
     * @title 提交评价
     * @description
     * @method POST
     * @url User/Order/addUserEvaluation
     * @param type 必选 int 1对订单评价表桥 2对工程人员评价标签
     * @param engineer_id 可选 int 工程人员ID,若type=2必传
     * @param work_order_id 可选 int 工单ID,若type=2必传
     * @param order_id 可选 int 订单ID,若type=1必传
     * @param evaluate_content 必选 string 内容
     * @param label 必选 array 评价标签
     * @param evaluate_stars 可选 int 评轮等级1-5，默认0
     * @param evaluate_image 可选 array 图片
     * @return
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public
    function http_addUserEvaluation()
    {
        // 接收参数
        $data['user_id'] = $this->user_id;
        // 判断 评价类型
        if (!empty($this->parm['type'] ?? '') && $this->parm['type'] == 2) {
            $data['type'] = 2;
            if (empty($this->parm['engineer_id'] ?? '')) {
                return $this->jsonend(-1001, "缺少参数,工程人员ID");
            }
            if (empty($this->parm['work_order_id'] ?? '')) {
                return $this->jsonend(-1001, "缺少参数,工单ID");
            }
            $data['engineer_id'] = $this->parm['engineer_id'];
            $data['work_order_id'] = $this->parm['work_order_id'];
        } else {
            $data['type'] = 1;
            if (empty($this->parm['equipments_id'] ?? '')) {
                return $this->jsonend(-1001, "缺少参数equipments_id");
            }
            if (empty($this->parm['order_id'] ?? '')) {
                return $this->jsonend(-1001, "缺少参数order_id");
            }
            $data['equipments_id'] = $this->parm['equipments_id'];
            $data['order_id'] = $this->parm['order_id'];
        }
        if (empty($this->parm['evaluate_content'] ?? '')) {
            return $this->jsonend(-1001, "请输入评价内容");
        }
        if (empty($this->parm['label'] ?? '')) {
            return $this->jsonend(-1001, "至少选择一项标签");
        }
        $evaluate_stars = $this->parm['evaluate_stars'] ?? 0;
        $evaluate_image = empty($this->parm['evaluate_image'] ?? '') ? '' : json_encode($this->parm['evaluate_image']);
        // 数据组装
        $data['evaluate_stars'] = $evaluate_stars;
        $data['evaluate_content'] = $this->parm['evaluate_content'];
        $data['evaluate_image'] = $evaluate_image;
        $data['evaluate_time'] = time();
        $add_status = false;
        $this->db->begin(function () use ($data, &$add_status) {
            $evaluate_id = $this->evaluate_model->add($data);
            if (!empty($this->parm['label'])) {
                foreach ($this->parm['label'] as $k => $v) {
                    $arr['evaluate_id'] = $evaluate_id;
                    $arr['label_id'] = $v;
                    $arr['create_time'] = time();
                    $this->evaluate_to_label_model->add($arr);
                }
            }
            //修改订单/工单状态
            if (!empty($this->parm['type'] ?? '') && $this->parm['type'] == 2) {
                $work_order_data['work_order_status'] = 12;
                $this->work_model->editWork(array('work_order_id' => $this->parm['work_order_id']), $work_order_data);
                //添加工单日志
                $this->addWorkOrderLog($this->parm['work_order_id'], time(), 12, "感谢您的评价");
            } else {
                $order['is_show'] = 2;
                $this->order_model->save(array('order_id' => $this->parm['order_id']), $order);
            }
            $add_status = true;
        });
        // 判断结果--返回
        if ($add_status) {
            return $this->jsonend(1000, '评论成功');
        } else {
            return $this->jsonend(-1000, '评论失败');
        }
    }

    /**
     * showdoc
     * @catalog API文档/用户端/订单相关
     * @title 获取商品评论列表
     * @description 支持分页
     * @method POST
     * @url User/Order/getEvaluateList
     * @param equipments_id 必选 int 主板ID
     * @param page 可选 int 页数，默认1
     * @param pageSize 可选 int 每页条数，默认10
     * @param type 可选 int 评价类型 1对订单评价 2对工程人员评价,默认1
     * @return {"code":1000,"message":"获取评价成功","data":[{"evaluate_id":"5","user_id":"3","type":"1","engineer_id":null,"work_order_id":null,"equipments_id":"1","order_id":"2","evaluate_stars":"5","evaluate_content":"还行吧","evaluate_image":[],"status":"1","evaluate_time":"2018-09-25 14:22:47","username":"fly傅丽宇","avatar_img":"https:\/\/wx.qlogo.cn\/mmopen\/vi_32\/4oenloc5C5pJWHfxAEVMzuDtz69ILQxrdOdbWgyvTj6qQfAtmnfnC5tEy0yoILAZws9cial5eG1UC48RgDzoy8A\/132","create_time":"2018-09-17 16:26:28"}]}
     * @return_param evaluate_id int 评论ID
     * @return_param evaluate_stars int 评论星级
     * @return_param evaluate_content string 评论内容
     * @return_param evaluate_image array 评论图片
     * @return_param evaluate_time string 评论时间
     * @return_param username string 用户名称
     * @return_param avatar_img string 用户头像
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public
    function http_getEvaluateList()
    {
        // 接收参数
        $equipments_id = $this->parm['equipments_id'] ?? '';
        $page = $this->parm['page'] ?? 1;
        $pageSize = $this->parm['pageSize'] ?? 10;
        $type = empty($this->parm['type'] ?? '') ? 1 : $this->parm['type'];
        // 判断参数
        if (empty($equipments_id)) {
            return $this->jsonend(-1001, '主板分类ID不能为空');
        }

        // 主表 -- 评价表
        $dbName = 'evaluate a';
        // 条件
        $where['a.equipments_id'] = $equipments_id;
        $where['a.type'] = $type;
        // 连表
        $join = [
            ['customer c', 'c.user_id = a.user_id', 'left'], // 用户表对应评论表
            ['orders o', 'o.order_id = a.order_id', 'left']
        ];
        // 查询字段
        $field = 'a.*,c.username,c.avatar_img,o.create_time';
        // 排序方式
        $order = ['evaluate_time' => 'DESC'];
        // 调用模型获取商品对应评价
        $evaluateData = $this->evaluate_model->getAll($where, $join, $page, $pageSize, $field, $order, $dbName);
        // 判断结果--返回
        if ($evaluateData) {
            foreach ($evaluateData as $k => $v) {
                $evaluateData[$k]['avatar_img'] = UploadImgPath($v['avatar_img'], 1);
                $evaluateData[$k]['username'] = empty($v['username']) ? $this->config->get('system_defalut')['username'] : $v['username'];
                $evaluateData[$k]['evaluate_time'] = date('Y-m-d H:i:s', $v['evaluate_time']);
                $evaluateData[$k]['create_time'] = date('Y-m-d H:i:s', $v['create_time']);
                $pic = array();
                if (!empty($v['evaluate_image'])) {
                    $evaluate_image = json_decode($v['evaluate_image'], true);
                    if (!empty($evaluate_image)) {
                        foreach ($evaluate_image as $kk => $vv) {
                            $pic[] = $this->config->get('qiniu.qiniu_url') . $vv;
                        }
                    }
                }
                $evaluateData[$k]['evaluate_image'] = $pic;
            }
            return $this->jsonend(1000, '获取评价成功', $evaluateData);
        } else {
            return $this->jsonend(-1000, '获取评价失败', $evaluateData);
        }
    }

    /**
     * showdoc
     * @catalog API文档/用户端/订单相关
     * @title 获取订单动态
     * @description
     * @method POST
     * @url User/Order/userOrderDynamic
     * @param page 可选 int 页数，默认1
     * @param pageSize 可选 int 每页条数，默认10
     * @return {"code":1000,"message":"获取用户订单动态成功","data":[{"operating_status":"10","order_number":"CQWA10492827921095","order_id":"104","main_pic":"https:\/\/qn.youheone.com\/d8c61201810081031598328.jpg","status":"服务中","create_time":"2018-10-10 13:29","remark":"工程人员已上门，正在服务中！工单编号:CQWA10492827921095","dynamic_type":2}]}
     * @return_param operating_status int 状态码
     * @return_param order_number string 订单编号
     * @return_param status string 状态
     * @return_param remark string 备注
     * @return_param create_time date 时间
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public
    function http_userOrderDynamic()
    {
        // 接收参数
        $user_id = $this->user_id;
        $page = $this->parm['page'] ?? 1;
        $pageSize = $this->parm['pageSize'] ?? 10;
        // 主表
        $dbName = 'orders_log a';
        // 条件
        $where['o.user_id'] = $user_id;
        $where['is_to_user'] = 1;
        // 连表
        $join = [
            ['orders o', 'o.order_id = a.order_id', 'left'], // 订单表对应订单日志表
            ['equipments e', 'e.equipments_id = o.equipments_id', 'left']  // 主板分类表对应订单表
        ];
        // 查询字段
        $field = 'e.equipments_name,e.main_pic,a.*,o.order_no as order_number';
        // 排序方式
        $order = ['a.create_time' => 'DESC'];
        // 调用模型查询用户动态状态
        $orderLogData = $this->log_model->getAll($where, 1, -1, $join, $field, $order, $dbName);
        // 判断结果-- 返回
        if ($orderLogData) {
            foreach ($orderLogData as $k => $v) {
                $orderLogData[$k]['status'] = $this->config->get('order_status')[$v['status']];
                $orderLogData[$k]['dynamic_type'] = 1;

//                $orderLogData[$k]['main_pic'] = $this->config['qiniu']['qiniu_url'] . $v['main_pic'];
//                $orderLogData[$k]['create_time'] = empty($v['create_time'])?'':date('Y-m-d H:i', $v['create_time']);
            }
        }
        //查询工单动态
        $work_map['wo.user_id'] = $this->user_id;
        $work_map['wl.is_to_user'] = 1;
        $work_join = $join = [
            ['work_order wo', 'wo.work_order_id = wl.work_order_id', 'left'],
            ['equipments e', 'e.equipments_id = wo.equipments_id', 'left']  // 主板分类表对应订单表
        ];
        $work_order_log = $this->work_log_model->getAll($work_map, 1, -1, $work_join, 'wl.operating_status,wo.order_number,wo.work_order_id as order_id,e.main_pic,wo.work_order_status as status,operating_time as create_time,wl.remarks as remark', ['wl.operating_time' => 'DESC'], 'work_order_log wl');
        if (!empty($work_order_log)) {
            foreach ($work_order_log as $k => $v) {
                $work_order_log[$k]['status'] = empty($v['operating_status']) ? '' : $this->config->get('user_work_order_status')[$v['operating_status']];
                $work_order_log[$k]['dynamic_type'] = 2;
                $work_order_log[$k]['remark'] = empty($v['operating_status']) ? '' : $this->config->get('work_order_status_desc')[$v['operating_status']] . '！工单编号:' . $v['order_number'];
            }
        }
        //数组合并
        $new_arr = array_merge($orderLogData, $work_order_log);
        //数组排序
        $new_arr2 = mymArrsort($new_arr, 'create_time');
        //数组分页
        $final_arr = arrPage($new_arr2, $page, $pageSize);
        if (!empty($final_arr)) {
            foreach ($final_arr as $k => $v) {
                $final_arr[$k]['create_time'] = empty($v['create_time']) ? '' : date('Y-m-d H:i', $v['create_time']);
                $final_arr[$k]['main_pic'] = $this->config['qiniu']['qiniu_url'] . $v['main_pic'];
            }
            return $this->jsonend(1000, '获取用户订单动态成功', $final_arr);
        }
        return $this->jsonend(-1003, '暂无用户订单动态');
    }

    /**
     * showdoc
     * @catalog API文档/用户端/订单相关
     * @title 取消订单
     * @description
     * @method POST
     * @url User/Order/cancelOrder
     * @param order_id 必选 int 订单ID
     * @return {"code":1000,"message":"取消成功","data":true}
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public
    function http_cancelOrder()
    {
        //接收参数
        $client = $this->parm['client'] ?? 1;
        $operater_role = 1;
        if ($client == 5) {
            $operater_role = 4;//市场推广
        }
        if (empty($this->parm['order_id'] ?? '')) {
            return $this->jsonend(-1001, "缺少参数订单ID");
        }
        //查询订单信息
        $map['order_id'] = $this->parm['order_id'];
        $map['user_id'] = $this->user_id;
        $field = 'pay_status,order_status';
        $order_info = $this->order_model->getOne($map, $field);
        if (empty($order_info)) {
            return $this->jsonend(-1100, "订单不存在");
        }
        if (!in_array($order_info['order_status'], [1, 3])) {
            return $this->jsonend(-1102, "该订单已在处理中，不能取消");
        }
        if ($order_info['order_status'] == 15) {
            return $this->jsonend(-1102, "该订单已取消,不能重复进行取消");
        }
        //修改订单状态
        $edit_data['order_status'] = 5;
        $edit_data['cancel_time'] = time();
        $add_status = false;
        $this->db->begin(function () use ($edit_data, &$add_status, $map, $operater_role) {
            $this->order_model->save($map, $edit_data);
            $this->addOrderLog($this->parm['order_id'], $edit_data['order_status'], "用户【用户ID:" . $this->user_id . "】已取消订单", 0, $operater_role);
            $add_status = true;
        });
        if ($add_status) {
            return $this->jsonend(1000, "取消成功");
        }
        return $this->jsonend(-1000, "取消失败");
    }


    /**
     * showdoc
     * @catalog API文档/用户端/订单相关
     * @title 取消识别单
     * @description
     * @method POST
     * @url User/Order/cancelIdentificationTable
     * @param identification_id 必选 int 订单ID
     * @return {"code":1000,"message":"取消成功","data":true}
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_cancelIdentificationTable()
    {
        if (empty($this->parm['identification_id'])) return $this->jsonend(ReturnCodeService::FAIL, '参数缺失：识别表ID');
        //查询识别表信息
        $this->Achievement->table = 'identification_info';
        $where = ['id' => $this->parm['identification_id']];
        $field = 'source_customer_code,id,sn,shop_name,contact_name,contact_phone,gas_fee,service_scene,transform_province_name,transform_city_name,transform_area_name,transform_detailed_address,transform_province_code,transform_city_code,transform_area_code,create_time,status,stoves_number,expect_transform_stoves_number,use_fan,survey_info,environment_pic,environment_video';
        $identification_data = $this->Achievement->findData($where, $field, ['create_time' => 'desc']);
        if (empty($identification_data)) return $this->jsonend(ReturnCodeService::FAIL, '暂无识别表数据');
        if ($identification_data['status'] == 2) return $this->jsonend(ReturnCodeService::FAIL, '工单后台已处理，不能关闭');
        if ($identification_data['status'] == 4) return $this->jsonend(ReturnCodeService::FAIL, '识别表已取消，请勿重复操作');
        $add_status = false;
        $this->db->begin(function () use (&$add_status) {
            //查询用户名称
            $user_data = $this->user_model->getOne(['user_id' => $this->user_id], 'username,realname,telphone');
            $name = (empty($user_data['realname']) ? $user_data['username'] : $user_data['realname']);
            $telphone = $user_data['telphone'];
            if ($this->role == 4) {
                //用户
                $content = '【用户】用户：' . '【' . $name . '】【' . $telphone . '】';
            } else {
                //市场推广
                $this->Achievement->table = 'oa_workers';
                $oa_workers = $this->Achievement->findData(['user_id' => $this->user_id], 'workers_name,workers_phone');
                if (!empty($oa_workers)) {
                    $name = $oa_workers['workers_name'];
                    $telphone = $oa_workers['workers_phone'];
                }
                $content = '【市场推广】市场推广：' . '【' . $name . '】【' . $telphone . '】';
            }
            $this->Achievement->table = 'identification_info';
            $this->Achievement->updateData(['id' => $this->parm['identification_id']], ['status' => 4]);
            //添加轨迹
            $content = $content . '关闭识别表';
            $identification_log_data = ['open_remarks' => '关闭识别表', 'identification_id' => $this->parm['identification_id'], 'content' => $content, 'log_type' => 1, 'operator_role' => $this->role, 'operator_uid' => $this->user_id, 'create_time' => time()];
            $this->Achievement->table = 'identification_log';
            $this->Achievement->insertData($identification_log_data);
            $add_status = true;
        });
        if ($add_status) {
            return $this->jsonend(1000, "取消成功");
        }
        return $this->jsonend(-1000, "取消失败");

    }


    /**
     * showdoc
     * @catalog API文档/用户端/订单相关
     * @title 确认收货
     * @description
     * @method POST
     * @url User/Order/collectGoods
     * @param order_id 必选 int 订单ID
     * @return {"code":1000,"message":"确认收货成功","data":true}
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public
    function http_collectGoods()
    {
        // 接收参数--验证参数
        $order_id = $this->parm['order_id'] ?? '';
        $user_id = $this->user_id;
        if (empty($order_id)) {
            return $this->jsonend(-1001, '订单ID不能为空');
        }
        // 获取保修时间
        $join = [
            ['equipments e', 'e.equipments_id = rq_orders.equipments_id', 'left'],
        ];
        $field = 'e.warranty_time';
        $orderInfo = $this->order_model->getOne(['order_id' => $order_id], $field, $join);
        $warranty_end_time = 0;
        $warranty_start_time = time();
        if ($orderInfo) {
            $warranty_end_time = date('Y-m-d H:i:s', strtotime('+' . $orderInfo['warranty_time'] . 'day'));
        }

        // 开启事务
        $add_status = false;
        $this->db->begin(function () use (&$add_status, &$order_id, $user_id, $warranty_start_time, $warranty_end_time) {
            // 1-- 修改订单状态
            $where['order_id'] = $order_id;
            $data['order_status'] = 3; // 待安装
            $data['warranty_start_time'] = $warranty_start_time; // 保修开始时间
            $data['warranty_end_time'] = strtotime($warranty_end_time);  // 保修结束时间
            $this->order_model->save($where, $data);
            // 2-- 新增物流信息
            //$this->addOrderLogistics($order_id, 7, '确认收货', $user_id); //找不到对应方法
            // 3-- 新增订单日志
            $this->addOrderLog($order_id, 3, '用户【用户ID:' . $user_id . '】已收货');
            $add_status = true;
        });
        if ($add_status) {
            return $this->jsonend(1000, '确认收货成功', $add_status);
        }
        return $this->jsonend(-1000, '确认收货失败,请稍后再试', $add_status);
    }

    /**
     * showdoc
     * @catalog API文档/用户端/订单相关
     * @title 查询物流信息
     * @description
     * @method POST
     * @url User/Order/orderLogistics
     * @param page 可选 int 页数，默认1
     * @param pageSize 可选 int 每页条数，默认10
     * @param order_id 必选 int 订单ID
     * @return {"code":1000,"message":"获取数据成功","data":{"base_info":{"logistics_status":"7","express_name":"暂无物流信息","express_number":"暂无物流信息","equipments_name":"购买产品1","main_pic":"https:\/\/qn.youheone.com\/20d01201809201045299934.png","company_hotline":"18883880448","logistics_status_name":"已签收"},"logistics":[{"logistics_id":"5","order_id":"18","status_code":"7","status_name":"已签收","remark":"确认收货","create_time":"09-20 11:34"}]}}
     * @return_param base_info object 基本信息（logistics_status 物流状态express_name快递名称 express_number快递编号 equipments_name产品名称 main_pic产品主图）
     * @return_param logistics array 物流信息（status_name 状态名称）
     * @remark {"order_id":18}
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public
    function http_orderLogistics()
    {
        if (empty($this->parm['order_id'] ?? '')) {
            return $this->jsonend(-1001, "缺少参数order_id");
        }
        //订单信息
        $join = [
            ['equipments as e', 'e.equipments_id = rq_orders.equipments_id', 'inner']   // 主板分类表对应订单表
        ];
        $order = $this->order_model->getOne(array('order_id' => $this->parm['order_id']), 'logistics_status,express_name,express_number,e.equipments_name,e.main_pic', $join);
        if (empty($order)) {
            return $this->jsonend(-1000, "订单信息错误");
        }
        $page = empty($this->parm['page'] ?? '') ? 1 : $this->parm['page'];
        $pageSize = empty($this->parm['pageSize'] ?? '') ? 10 : $this->parm['pageSize'];
        $map['order_id'] = $this->parm['order_id'];
        $data = $this->order_logistics_model->getAll($map, 'logistics_id,order_id,status_code,status_name,remark,create_time', $page, $pageSize);
        if (empty($data)) {
            return $this->jsonend(-1003, "暂无相关数据");
        }

        //处理订单基本信息
        $order['main_pic'] = empty($order['main_pic']) ? '' : $this->config->get('qiniu.qiniu_url') . $order['main_pic'];
        $order['company_hotline'] = ConfigService::getConfig('company_hotline', false, $this->company);
        $order['logistics_status_name'] = $this->config->get('order_logistics_code')[$order['logistics_status']];
        $order['express_name'] = empty($order['express_name']) ? '暂无物流信息' : $order['express_name'];
        $order['express_number'] = empty($order['express_number']) ? '暂无物流信息' : $order['express_number'];
        $arr['base_info'] = $order;
        //处理物流信息
        foreach ($data as $k => $v) {
            $data[$k]['create_time'] = empty($v['create_time']) ? '' : date('m-d H:i', $v['create_time']);
            $data[$k]['status_name'] = $this->config->get('order_logistics_code')[$v['status_code']];
        }
        $arr['logistics'] = $data;
        return $this->jsonend(1000, "获取数据成功", $arr);
    }

    /**
     * showdoc
     * @catalog API文档/用户端/主板相关
     * @title 绑定主板
     * @description
     * @method POST
     * @url User/Order/activeBuyDevice
     * @param order_id 必选 sting 订单ID
     * @param device_no 必选 sting 主板编号
     * @return {"code": 1000,"message": "绑定成功"}
     * @return_param
     * @remark {"work_order_id":"12","device_no":"a5465"}
     * @number 0
     * @author lcx
     * @date 2018-10-19
     */
    public
    function http_activeBuyDevice()
    {
        if (empty($this->parm['device_no'] ?? '')) {
            return $this->jsonend(-1001, "请输入主板编号");
        }
        if (empty($this->parm['order_id'] ?? '')) {
            return $this->jsonend(-1001, "缺少参数订单ID");
        }
        //查询订单信息
        $order_info = $this->order_model->getOne(['order_id' => $this->parm['order_id']], '*');
        if (empty($order_info)) {
            return $this->jsonend(-1101, "订单信息错误");
        }

        //判断主板是否存在于主板库
        $li_map['device_no'] = $this->parm['device_no'];
        $libary = $this->equipment_lists_model->findEquipmentLibrary($li_map, '*');

        if (empty($libary)) {
            return $this->jsonend(-1101, "请输入正确的主板号");
        }
        //查询产品信息
        $eq_info = $this->equip_model->getDetail(array('equipments_id' => $order_info['equipments_id']), 'equipments_name,model,equipments_type,warranty_time,mainboard_id');
        if ($eq_info['equipments_type'] != 2) {
            return $this->jsonend(-1101, "仅支持激活售卖的产品");
        }
        /* if ($eq_info['mainboard_id'] != $libary['model_id']) {
             //主板和产品的主板型号不一致
             return $this->jsonend(-1101, "主板和产品的主板型号不一致");
         }*/

        $warranty_time['start'] = $order_info['warranty_start_time'];
        $warranty_time['end'] = $order_info['warranty_end_time'];
        //判断主板是否已经存在
        $equipment_info = $this->equipment_lists_model->findEquipmentLists(array('device_no' => $this->parm['device_no']), 'equipment_id,status');
        if ($this->dev_mode == 0) {//正式环境才进行联网状态验证
            if (!empty($equipment_info)) {
                //判断用户是否重复绑定主板
                $bd_map['equipment_id'] = $equipment_info['equipment_id'];
                $bd_map['is_owner'] = 2;
                $bind_info = $this->bind_model->getOne($bd_map, '*');
                if (!empty($bind_info)) {
                    return $this->jsonend(-1104, "该主板已被绑定过，不能重复绑定");
                }
            } else {
                return $this->jsonend(-1104, "未获取到主板信息,请等待主板联网成功后再试");
            }
        }
        //判断用户是否已经绑定完
        $bind_map['equipments_id'] = $order_info['equipments_id'];
        $bind_map['is_owner'] = 2;
        $bind_map['user_id'] = $order_info['user_id'];
        $bind_list = $this->bind_model->getAll($bind_map, '*');
        if (!empty($bind_list) && count($bind_list) >= $order_info['equipment_num']) {
            return $this->jsonend(-1105, '该用户租赁该类型主板' . $order_info['equipment_num'] . '台，已绑定' . count($bind_list) . '台');
        }
        // 获取用户信息
        $user_info = $this->user_model->getOne(['user_id' => $order_info['user_id']], 'openid,telphone');
        $equipment_id = '';
        $add_status = false;
        $this->db->begin(function () use (&$add_status, $libary, $order_info, $user_info, $equipment_info, $eq_info, &$equipment_id, $warranty_time) {
            $eq_data['province'] = $order_info['province'];
            $eq_data['city'] = $order_info['city'];
            $eq_data['area'] = $order_info['area'];
            $eq_data['province_code'] = $order_info['province_code'];
            $eq_data['city_code'] = $order_info['city_code'];
            $eq_data['area_code'] = $order_info['area_code'];
            $eq_data['address'] = $order_info['address'];
            $eq_data['lng'] = $order_info['lng'];
            $eq_data['lat'] = $order_info['lat'];
            $eq_data['contact_number'] = $order_info['contact_tel'];
            $eq_data['contact'] = $order_info['contact'];
            $eq_data['alias_name'] = $eq_info['equipments_name'];

            $eq_data['company_id'] = $order_info['company_id'];
            if (empty($equipment_info)) {
                //新增主板记录
                $eq_data['equipments_id'] = $order_info['equipments_id'];
                $eq_data['device_no'] = $this->parm['device_no'];
                $eq_data['create_time'] = time();
                $eq_data['status'] = 1;
                $eq_data['contract_id'] = '';
                $equipment_id = $this->equipment_lists_model->insertEquipmentLists($eq_data);
            } else {
                //修改主板信息
                $eq_data['contract_id'] = '';
                $equipment_id = $equipment_info['equipment_id'];
                $eq_data['equipments_id'] = $order_info['equipments_id'];
                $this->equipment_lists_model->updateEquipmentLists($eq_data, array('equipment_id' => $equipment_id));
            }
            //添加绑定记录
            $bind_data['equipments_id'] = $order_info['equipments_id'];
            $bind_data['order_id'] = $this->parm['order_id'];
            $bind_data['equipment_id'] = $equipment_id;
            $bind_data['user_id'] = $order_info['user_id'];
            $bind_data['create_time'] = time();
            $bind_data['is_owner'] = 2;
            $this->bind_model->addCustomerBindEquipment($bind_data);
            //已判定完修改相应状态
            $bind_map['equipments_id'] = $order_info['equipments_id'];
            $bind_map['user_id'] = $order_info['user_id'];
            $bind_map['is_owner'] = 2;
            $bind_map['order_id'] = $this->parm['order_id'];
            $bind_list = $this->bind_model->getAll($bind_map, '*');
            if (count($bind_list) >= $order_info['equipment_num']) {
                //如果绑定完主板修改订单状态
                $this->order_model->save(['order_id' => $this->parm['order_id']], ['order_status' => 4]);
            }
            //如果是购买产品直接下发套餐以及数据同步
            if ($eq_info['equipments_type'] == 2) {
                DeviceService::activeBuyDevice($this->parm['device_no'], $order_info['equipments_id']);
            }
            $add_status = true;
        }, function ($e) {
            secho('[ERROR] 用户激活主板错误', $e->error);
            return $this->jsonend(-1000, "用户激活主板失败,请重试!" . $e->error);
        });

        if ($add_status) {

            // 发送模板消息
            //通知到用户
            $user_notice_config = ConfigService::getTemplateConfig('user_bind_device_success', 'user', $this->company);
            if (!empty($user_notice_config) && !empty($user_notice_config['template']) && $user_notice_config['template']['switch']) {
                $template_id = $user_notice_config['template']['wx_template_id'];
                $mp_template_id = $user_notice_config['template']['mp_template_id'];
                $template_data = [
                    'keyword1' => ['value' => $this->parm['device_no']], // 主板编号
                    'keyword2' => ['value' => $eq_info['model']], // 品牌型号
                    'keyword3' => ['value' => date('Y-m-d H:i:s')], // 绑定时间
                    'keyword4' => ['value' => 1], // 绑定数量
                    'keyword5' => ['value' => '恭喜!您已经成功绑定了一台主板'], // 绑定提醒
                ];
                $weapp_template_keyword = '';
                $mp_template_data = [
                    'first' => ['value' => '恭喜!您已经成功绑定了一台主板', 'color' => '#4e4747'],
                    'keyword1' => ['value' => 1, 'color' => '#4e4747'],
                    'keyword2' => ['value' => 0, 'color' => '#4e4747'],
                    'keyword3' => ['value' => $eq_info['equipments_name'] . "(主板编号:" . $this->parm['device_no'] . ")", 'color' => '#4e4747'],
                    'remark' => ['value' => '感谢您一直以来对' . $this->user_wx_name . '的支持，' . $this->user_wx_name . '永远都是维护您健康的坚实后盾!若有疑问可致电' . $this->user_wx_name . '官方客服,客服电话:' . ConfigService::getConfig('company_hotline', false, $this->company), 'color' => '#173177'],
                ];
                $template_tel = $user_info['telphone'];  // 电话
                $template_content = '您已经成功绑定了一台主板。主板编号为' . $this->parm['device_no'] . '，绑定时间' . date('Y-m-d H:i:s') . '。打开微信进入' . $this->user_wx_name . '小程序可查看主板详情';
                // 调用模板消息发送
                $template_url = '';
                NotifiyService::sendNotifiyAsync($user_info['openid'], $template_data, $template_id, $template_tel, $template_content, $template_url, $weapp_template_keyword, $mp_template_id, $mp_template_data, 'user', $this->company);
            }


            return $this->jsonend(1000, "激活成功");
        }
        return $this->jsonend(-1000, "激活失败,请重试");
    }


    /**
     * @desc   添加订单物流信息
     * @param $status
     * @date   2018-07-19
     * @return [type]     [description]
     * @author lcx
     */
    public
    function addOrderLogistics($order_id, $status_code, $remark, $operator_uid)
    {
        $data['order_id'] = $order_id;
        $data['status_code'] = $status_code;
        $data['status_name'] = $this->config->get('order_logistics_code')[$status_code];
        $data['remark'] = $remark;
        $data['create_time'] = time();
        $data['operator_role'] = 3;
        $data['operator_uid'] = $operator_uid;
        $res = $this->order_logistics_model->add($data);
        //修改订单物流状态
        $this->order_model->save(array('order_id' => $order_id), array('logistics_status' => $status_code));
        return $res;
    }


}
