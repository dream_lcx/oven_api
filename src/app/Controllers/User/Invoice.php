<?php

namespace app\Controllers\User;

/**
 * 用户端/发票相关API
 */
class Invoice extends Base {

	protected $financeRecordModel;
	protected $orderInvoiceModel;
	protected $userInvoiceModel;
	protected $customer_address_model;

	public function initialization($controller_name, $method_name) {
		parent::initialization($controller_name, $method_name);
		$this->financeRecordModel = $this->loader->model('FinanceRecordModel', $this);
		$this->orderInvoiceModel = $this->loader->model('OrderInvoiceModel', $this);
		$this->userInvoiceModel = $this->loader->model('UserinvoiceModel', $this);
		$this->customer_address_model = $this->loader->model('CustomerAddressModel', $this);
	}

	/**
	 * showdoc
	 * @catalog API文档/用户端/发票相关
	 * @title 发票抬头详情
	 * @description 
	 * @method POST
	 * @url User/Invoice/editInvoice
	 * @param invoice_id 必选 int 发票ID
	 * @return {"code":1000,"message":"获取发票成功","data":{"invoice_id":"3","type":"1","user_id":"6","name":"刘春霞","tel":"18883880448","tax_no":null,"address":null,"bank":null,"bank_account":null,"create_time":"1538203711","update_time":null}}
	 * @return_param invoice_id int 发票ID
	 * @return_param type int 发票类型 1个人 2单位
	 * @return_param name string 个人姓名/单位名称
	 * @return_param user_id int 用户ID
	 * @return_param tel string 联系电话
	 * @return_param tax_no string 企业税务号
	 * @return_param address string 企业地址
	 * @return_param bank_account string 企业账户
	 * @return_param bank string 企业银行名称
	 * @remark 
	 * @number 0
	 * @author lcx
	 * @date 2018-10-18
	 */
	public function http_editInvoice() {
		// 接收参数
		$user_id = $this->user_id;
		$invoice_id = $this->parm['invoice_id'] ?? '';
		// 判断参数
		if (empty($invoice_id)) {
			return $this->jsonend(-1001, '发票ID不能为空');
		}
		// 查询条件
		$where = ['user_id' => $user_id, 'invoice_id' => $invoice_id];
		// 查询字段
		$field = '*';
		// 调用模型查询数据
		$invoiceDetail = $this->userInvoiceModel->getOne($where, $field);
		// 判断结果-- 返回
		if ($invoiceDetail) {
			return $this->jsonend(1000, '获取发票成功', $invoiceDetail);
		} else {
			return $this->jsonend(-1000, '获取发票失败', $invoiceDetail);
		}
	}

	/**
	 * showdoc
	 * @catalog API文档/用户端/发票相关
	 * @title 新增/编辑发票抬头
	 * @description 
	 * @method POST
	 * @url User/Invoice/addEditInvoice
	 * @param type 必选 int 发票类型 1个人 2单位
	 * @param name 必选 string 个人姓名/单位名称  
	 * @param tel 必选 string 电话  
	 * @param tax_no 可选 string 企业税务号 若type=2必传
	 * @param address 可选 string 企业地址  若type=2必传
	 * @param bank 可选 string 企业银行名称  若type=2必传
	 * @param bank_account 可选 string 企业银行账户  若type=2必传
	 * @return 
	 * @remark 
	 * @number 0
	 * @author lcx
	 * @date 2018-10-18
	 */
	public function http_addEditInvoice() {
		// 接收参数
		$user_id = $this->user_id;
		$type = $this->parm['type'] ?? '';
		$name = $this->parm['name'] ?? '';
		$tel = $this->parm['tel'] ?? '';
		$tax_no = $this->parm['tax_no'] ?? '';
		$address = $this->parm['address'] ?? '';
		$bank = $this->parm['bank'] ?? '';
		$bank_account = $this->parm['bank_account'] ?? '';
		// 判断参数
		if (empty($name)) {
			return $this->jsonend(-1001, '名称或姓名不能为空');
		}
		if (empty($type)) {
			return $this->jsonend(-1001, '发票类型不能为空');
		}
		// 个人
		if ($type == 1) {
			if (empty($tel)) {
				return $this->jsonend(-1001, '电话不能为空');
			}
            if(!preg_match('/^1[3-9][0-9]\d{8}$/',$tel)){
                return $this->jsonend(-1001, '请输入正确的电话号码');
            }

			$data['tel'] = $tel;
		}
		// 企业
		elseif ($type == 2) {
			if (empty($tax_no)) {
				return $this->jsonend(-1001, '企业税务号不能为空');
			}
			$data['tax_no'] = $tax_no;
			$data['address'] = $address;
			$data['tel'] = $tel;
			$data['bank'] = $bank;
			$data['bank_account'] = $bank_account;
		}
		// 数据组装
		$data['user_id'] = $user_id;
		$data['type'] = $type;
		$data['name'] = $name;
		// 查询条件
		$where = ['user_id' => $user_id, 'type' => $type];
		// 查询字段
		$field = 'invoice_id';
		// 调用模型查询发票数据
		$is_invoice = $this->userInvoiceModel->getOne($where, $field);
		// 有发票执行编辑操作
		if (!empty($is_invoice)) {
			$data['update_time'] = time();
			// 调用模型修改发票
			$editResult = $this->userInvoiceModel->editInvoice($where, $data);
			// 判断修改结果-- 返回
			if ($editResult) {
				return $this->jsonend(1000, '修改发票成功', array('invoice_id' => $is_invoice['invoice_id']));
			} else {
				return $this->jsonend(-1000, '修改发票失败');
			}
		}
		$data['create_time'] = time();
		// 调用模型添加发票
		$addResult = $this->userInvoiceModel->addInvoice($data);
		// 判断结果返回
		if ($addResult) {
			return $this->jsonend(1000, '添加发票成功', array('invoice_id' => $addResult));
		} else {
			return $this->jsonend(-1000, '添加发票失败');
		}
	}

	/**
	 * showdoc
	 * @catalog API文档/用户端/发票相关
	 * @title 获取发票抬头列表
	 * @description 
	 * @method POST
	 * @url User/Invoice/invoiceList
	 * @param page 可选 int 页数，默认1  
	 * @param pageSize 可选 int 每页条数，默认10  
	 * @return {"code":1000,"message":"获取发票列表成功","data":[{"invoice_id":"3","type":"1","user_id":"6","name":"刘春霞","tel":"18883880448","tax_no":null,"address":null,"bank":null,"bank_account":null,"create_time":"1538203711","update_time":null}]}
	 * @return_param invoice_id int 发票ID
	 * @return_param type int 发票类型 1个人 2单位
	 * @return_param name string 个人姓名/单位名称
	 * @return_param user_id int 用户ID
	 * @return_param tel string 联系电话
	 * @return_param tax_no string 企业税务号
	 * @return_param address string 企业地址
	 * @return_param bank_account string 企业账户
	 * @return_param bank string 企业银行名称
	 * @remark 
	 * @number 0
	 * @author lcx
	 * @date 2018-10-18
	 */
	public function http_invoiceList() {
		// 接收参数
		$user_id = $this->user_id;
		$page = $this->parm['page'] ?? 1;
		$pageSize = $this->parm['pageSize'] ?? 10;
		// 查询条件
		$where = ['user_id' => $user_id];
		// 查询字段
		$field = '*';
		// 排序方式
		$order = ['create_time' => 'DESC'];
		// 调用模型查询发票数据
		$invoiceData = $this->userInvoiceModel->getAll($where, $field, $page, $pageSize, $order);
		// 判断结果-- 返回
		if ($invoiceData) {
			return $this->jsonend(1000, '获取发票列表成功', $invoiceData);
		} else {
			return $this->jsonend(-1000, '暂无发票', $invoiceData);
		}
	}

	/**
	 * showdoc
	 * @catalog API文档/用户端/发票相关
	 * @title 获取可开票金额
	 * @description 获取当前可开票金额
	 * @method POST
	 * @url User/Invoice/getInvoiceableAmount
	 * @return {"code":1000,"message":"获取成功","data":{"total_money":"0.06","opened_money":0,"remain_money":"0.06"}}
	 * @return_param total_money float  可开发票总金额,单位元
	 * @return_param opened_money float  已开发票金额,单位元
	 * @return_param remain_money float  当前可开发票金额,单位元
	 * @remark 
	 * @number 0
	 * @author lcx
	 * @date 2019-04-23
	 */
	public function http_getInvoiceableAmount() {
		$return = $this->statistics();
		return $this->jsonend(1000, "获取成功", $return);
	}

	/**
	 * showdoc
	 * @catalog API文档/用户端/发票相关
	 * @title 申请开票
	 * @description 申请开发票
	 * @method POST
	 * @param invoice_type 可选 int 发票类型1纸质发票2电子发票,默认1
	 * @param money 必选 float 开票金额,单位元
	 * @param invoice_id int 必选  发票抬头ID
	 * @param invoice_recevice_name 必选  string 收票人姓名
	 * @param invoice_recevice_tel 必选 string 收票人电话
	 * @param invoice_recevice_address 可选 string  收票地址,如果是纸质发票必填
	 * @param invoice_recevice_email 可选 string 收票邮箱,如果是电子发票必填
	 * @url User/Invoice/applyOpen
	 * @return {"code":1000,"message":"申请成功","data":""}
	 * @remark 
	 * @number 0
	 * @author lcx
	 * @date 2019-04-23
	 */
	public function http_applyOpen() {
		$invoice_type = $this->parm['invoice_type'] ?? 1;
		if (empty($this->parm['money'] ?? '')) {
			return $this->jsonend(-1001, "请选择开票金额");
		}
		//判断金额是否不大于可开票金额
		$info = $this->statistics();
		if (floatval($info['remain_money']) < floatval($this->parm['money'])) {
			return $this->jsonend(-1101, "最多可开票" . $info['remain_money'] . '元');
		}

		if (empty($this->parm['invoice_id'] ?? '')) {
			return $this->jsonend(-1001, "请选择发票抬头");
		}

		//如果是纸质发票必须填写收票地址
		if ($invoice_type == 1 && empty($this->parm['invoice_recevice_address'] ?? '')) {
			return $this->jsonend(-1001, "请填写收票地址");
		}

		// 根据地址ID查询收票人信息
        $where['address_id'] = $this->parm['invoice_recevice_address'];
		$field = 'address_id,provice,city,area,address,contact,contact_tel';
        $customer_address_info = $this->customer_address_model->getOne($where, $field);
        unset($where);

		//如果是电子发票必须填写收票邮箱
		if ($invoice_type == 2 && empty($this->parm['invoice_recevice_email'] ?? '')) {
			return $this->jsonend(-1001, "请填写收票邮箱");
		}
		if (!empty($this->parm['invoice_recevice_email'] ?? '')) {
			if (!isEmail($this->parm['invoice_recevice_email'])) {
				return $this->jsonend(-1002, "收票人邮箱不合法");
			}
		}

		//发票抬头信息
		$invoice_info = $this->userInvoiceModel->getOne(array('invoice_id' => $this->parm['invoice_id']), '*');
		if (empty($invoice_info)) {
			return $this->jsonend(-1001, "发票抬头错误");
		}

		$invoice_data['invoice_type'] = $invoice_type;
		$invoice_data['name'] = $invoice_info['name'];
		$invoice_data['tel'] = $invoice_info['tel'];
		$invoice_data['tax_no'] = $invoice_info['tax_no'];
		$invoice_data['address'] = $invoice_info['address'];
		$invoice_data['bank'] = $invoice_info['bank'];
		$invoice_data['bank_account'] = $invoice_info['bank_account'];
		$invoice_data['invoice_title_type'] = $invoice_info['type'];
		$invoice_data['invoice_recevice_email'] = $this->parm['invoice_recevice_email'] ?? '';

        $invoice_data['invoice_recevice_address'] = $customer_address_info['provice'].$customer_address_info['city'].$customer_address_info['area'].$customer_address_info['address'];
		$invoice_data['invoice_recevice_name'] = $customer_address_info['contact'] ?? '';
		$invoice_data['invoice_recevice_tel'] = $customer_address_info['contact_tel'];

		$invoice_data['create_time'] = time();
		$invoice_data['money'] = formatMoney($this->parm['money'], 1);
		$invoice_data['user_id'] = $this->user_id;
		$result = $this->orderInvoiceModel->add($invoice_data);
		if ($result) {
			return $this->jsonend(1000, "申请成功");
		}
		return $this->jsonend(-1000, "申请失败");
	}

	/**
	 * showdoc
	 * @catalog API文档/用户端/发票相关
	 * @title 获取开票记录
	 * @description 
	 * @method POST
	 * @url User/Invoice/getRecord
	 * @param page int 可选 页数,默认1
	 * @param pageSize int 可选  每页条数,默认20
	 * @return {"code":1000,"message":"获取成功","data":[{"id":"2","order_id":null,"user_id":"648","invoice_type":"1","invoice_title_type":"2","money":"1","name":"乔治企业","tel":"4003821945","tax_no":"500123456789","address":"重庆乔治","bank":"乔治银行","bank_account":"2001324564","invoice_recevice_name":"lcx","invoice_recevice_tel":"18883880448","invoice_recevice_email":"","invoice_recevice_address":"111","create_time":"2019-04-23 16:17:24","status":"1","remarks":null,"check_time":null,"send_time":null,"logistics_no":"","logistics_name":"","status_desc":"待审核"}]}
	 * @return_param invoice_type int 发票类型1纸质发票2电子发票,默认1
	 * @return_param money float  开票金额,单位元
	 * @return_param invoice_id int  发票抬头ID
	 * @return_param invoice_recevice_name string 收票人姓名
	 * @return_param invoice_recevice_tel string  收票人电话
	 * @return_param invoice_recevice_address string  收票地址
	 * @return_param invoice_recevice_email string  收票邮箱
	 * @return_param invoice_title_type int 抬头类型1个人2单位
	 * @return_param create_time  string 开票时间
	 * @return_param status int 状态
	 * @return_param status_desc  string 状态说明
	 * @return_param logistics_no  string 物流单号
	 * @return_param logistics_name  string 物流名称
	 * @remark 
	 * @number 0
	 * @author lcx
	 * @date 2019-04-23
	 */
	public function http_getRecord() {
		$page = $this->parm['page'] ?? 1;
		$pageSize = $this->parm['pageSize'] ?? 20;
		$data = $this->orderInvoiceModel->getAll(['user_id' => $this->user_id,'is_del'=>0], 'create_time,source,invoice_title,electronic_invoice,invoice_type,user_id', $page, $pageSize);
		if (empty($data)) {
			return $this->jsonend(-1003, "暂无数据");
		}
		foreach ($data as $k => $v) {
			$data[$k]['create_time'] = empty($v['create_time']) ? '' : date('Y-m-d H:i:s', $v['create_time']);
			$data[$k]['electronic_invoice'] = empty($v['electronic_invoice'])?'':uploadImgPath($v['electronic_invoice']);
		}
		return $this->jsonend(1000, "获取成功", $data);
	}

	// 开票记录详情
    public function http_getRecordDetail()
    {
        // 接收参数
        $id = $this->parm['invoice_id'] ?? '';
        if (empty($id)) {
            return $this->jsonend(-1001, '缺少发票ID');
        }

        $where['id'] = $id;
        $field = '*';
        // 发票详情查询
        $data = $this->orderInvoiceModel->getOne($where, $field);
        if ($data) {
            $data['create_time'] = empty($data['create_time']) ? '' : date('Y-m-d H:i:s', $data['create_time']);
            $data['status_desc'] = empty($data['status']) ? '' : $this->config->get('invoice_status')[$data['status']];
            $data['money'] = formatMoney($data['money']);
            return $this->jsonend(1000, '获取成功', $data);
        }
            return $this->jsonend(-1000, '获取失败');
    }


	// 计算发票金额,包括总金额,已开金额,剩余可开金额
	public function statistics() {
		//可开票总金额（包括订单支付,工单支付,续费）
		$map['type'] = ['IN', [1, 2, 3]];
		$map['user_id'] = $this->user_id;
		$total_info = $this->financeRecordModel->getAll($map, 'SUM(money) AS total', 1, -1);

		//计算已开票金额
		$opened_map['user_id'] = $this->user_id;
		$opened_map['status'] = ['IN', [1, 2, 3]];
		$opened_info = $this->orderInvoiceModel->getAll($opened_map, 'SUM(money) AS total', 1, -1);
		if (!empty($total_info)) {
			$total_money = $total_info[0]['total'] ?? 0;
		}
		if (!empty($opened_info)) {
			$opened_money = $opened_info[0]['total'] ?? 0;
		}
		//剩余可开票金额
		$remain_money = $total_money - $opened_money;

		$return['total_money'] = formatMoney($total_money); //将金额分转元
		$return['opened_money'] = formatMoney($opened_money);
		$return['remain_money'] = formatMoney($remain_money);
		return $return;
	}








}
