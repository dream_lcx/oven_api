<?php


namespace app\Controllers\User;

use app\Services\Common\ReturnCodeService;
use app\Services\Common\UploadService;

class Seal extends Base
{
    protected $sign;
    protected $accountId;
    protected $user_seal_model;
    protected $user_model;
    protected $auth_model;


    //put your code here
    public function initialization($controller_name, $method_name)
    {
        parent::initialization($controller_name, $method_name);
        $this->auth_model = $this->loader->model('CustomerAuthModel', $this);
        $this->user_model = $this->loader->model('CustomerModel', $this);
        $this->user_seal_model = $this->loader->model('CustomerSealModel', $this);
        try {
            require_once APP_DIR . '/Library/eSign/API/eSignOpenAPI.php';
            $this->sign = get_instance()->eSign;
            $iRet = $this->sign->init();
            if ($iRet != 0) {
                echo "初始化失败";
            } else {
                echo "初始化成功";
            }
        } catch (\Exception $e) {
            return $this->jsonend(ReturnCodeService::FAIL, "服务异常" . $e->getMessage());
        }
    }

    /**
     * showdoc
     * @catalog API文档/用户端/企业印章
     * @title 生成印章-指定企业
     * @description
     * @method POST
     * @url /User/Seal/appointCompanySeal
     * @param user_id 可选 int 用户ID,默认当前用户
     * @param template_type 可选 string 公章类型,star:标准公章  oval:椭圆公章 rect:条形印章 dedicated:无五角星印章
     * @param color 可选 string 公章颜色,red:红色 blue:蓝色 black:黑色
     * @param h_text 可选 string 横向文内容
     * @param q_text 可选 string 下弦文内容
     * @return
     * @remark
     * @number 0
     * @author lcx
     * @date 2020-02-20
     */
    public function http_appointCompanySeal()
    {
        $user_id = $this->parm['user_id'] ?? $this->user_id;
        $auth_info = $this->auth_model->getOne(['user_id' => $user_id, 'status' => 2], '*');
        if (empty($auth_info)) {
            return $this->jsonend(ReturnCodeService::NO_REALNAME, "必须实名认证通过后才能生成印章,请先实名");
        }
        //注册账户,参数说明可见e签宝文档
        $name = $auth_info['company_name'];
        $organCode = $auth_info['business_license_number'];
        $mobile = '';
        $regType = 1;
        $email = '';
        $organType = 0;
        $legalArea = 0;
        $userType = 0;
        $agentName = '';
        $agentIdNo = '';
        $legalName = $auth_info['contact'];
        $legalIdNo = $auth_info['id_number'];
        $data = $this->sign->addOrganizeAccount($mobile, $name, $organCode, $regType, $email, $organType, $legalArea, $userType, $agentName, $agentIdNo, $legalName, $legalIdNo
        );
        if ($data['errCode'] != 0) {
            if ($data['errCode'] == 150016) {//如果账户已存在
                $redis_esign_data = $this->redis->hGet('cloud_esign_account', $organCode);
                $accountId = $this->parm['account_id'];
                if(!empty($redis_esign_data)){
                    $redis_esign_data = json_decode($redis_esign_data,true);
                    $accountId = $redis_esign_data['accountId'];
                }
                $this->sign->delUserAccount($accountId);//注销账户
                $data = $this->sign->addOrganizeAccount($mobile, $name, $organCode, $regType, $email, $organType, $legalArea, $userType, $agentName, $agentIdNo, $legalName, $legalIdNo
                );//重新生成
            }
            return $this->jsonend(ReturnCodeService::FAIL, "生成失败!" . $data['msg']);
        }
        //保存e签宝账户ID
        $param['name'] = $name;
        $param['organCode'] = $organCode;
        $param['mobile'] = $mobile;
        $param['regType'] = $regType;
        $param['organType'] = $organType;
        $param['legalArea'] = $legalArea;
        $param['userType'] = $userType;
        $param['agentName'] = $agentName;
        $param['agentIdNo'] = $agentIdNo;
        $param['legalName'] = $legalName;
        $param['legalIdNo'] = $legalIdNo;
        $param['accountId'] = $data['accountId'];
        $this->redis->hSet('cloud_esign_account', $organCode, json_encode($param));
        $accountId = $data['accountId'];
        $template_type = $this->parm['template_type'] ?? 'star';//star:标准公章  oval:椭圆公章 rect:条形印章 dedicated:无五角星印章
        $color = $this->parm['color'] ?? 'red';//公章颜色 red:红色 blue:蓝色 black:黑色
        $h_text = $this->parm['h_text'] ?? '';//横向文内容
        $q_text = $this->parm['q_text'] ?? '';//下弦文内容
        $Official_seal = $this->sign->addTemplateSeal($accountId, $template_type, $color, $h_text, $q_text);
        if ($Official_seal['errCode'] == 0) {
            $data = UploadService::upload($Official_seal['imageBase64'], '', 'pdf/seal/', 'qiniu', 'base64');
            $data['data']['accountId'] = $accountId;
            return $this->jsonend(ReturnCodeService::SUCCESS, "生成成功", $data['data']);
        }
        return $this->jsonend(ReturnCodeService::FAIL, "生成失败!" . $Official_seal['msg']);

    }

    /**
     * showdoc
     * @catalog API文档/用户端/企业印章
     * @title 保存印章
     * @description
     * @method POST
     * @url /User/Seal/add
     * @param user_id 可选 int 用户ID,默认当前用户
     * @param template_type 可选 string 公章类型,star:标准公章  oval:椭圆公章 rect:条形印章 dedicated:无五角星印章
     * @param color 可选 string 公章颜色,red:红色 blue:蓝色 black:黑色
     * @param h_text 可选 string 横向文内容
     * @param q_text 可选 string 下弦文内容
     * @param seal 必选 string 印章路径
     * @param name 必选 string 印章名称
     * @param account_id 必选 string e签宝唯一标识
     * @return
     * @remark
     * @number 0
     * @author lcx
     * @date 2020-02-20
     */
    public function http_add()
    {
        $user_id = $this->parm['user_id'] ?? $this->user_id;
        if (empty($this->parm['name'])) {
            return $this->jsonend(ReturnCodeService::ILLEGAL_PARAM, "请填写印章名称");
        }
        if (empty($this->parm['seal'])) {
            return $this->jsonend(ReturnCodeService::CREATE_SEAL, "印章生成失败,请重试");
        }
        if (empty($this->parm['account_id'])) {
            return $this->jsonend(ReturnCodeService::CREATE_SEAL, "缺少参数account_id");
        }
        $add_data['user_id'] = $user_id;
        $add_data['name'] = $this->parm['name'];
        $add_data['htext'] = $this->parm['h_text'] ?? '';
        $add_data['qtext'] = $this->parm['q_text'] ?? '';
        $add_data['color'] = $this->parm['color'] ?? 'red';
        $add_data['template_type'] = $this->parm['template_type'] ?? 'star';
        $add_data['seal'] = $this->parm['seal'] ?? '';
        $add_data['account_id'] = $this->parm['account_id'] ?? '';
        $add_data['create_time'] = time();
        $result = $this->user_seal_model->add($add_data);
        if ($result) {
            return $this->jsonend(ReturnCodeService::SUCCESS, "操作成功");
        }
        return $this->jsonend(ReturnCodeService::FAIL, "操作失败,请重试");

    }

    /**
     * showdoc
     * @catalog API文档/用户端/企业印章
     * @title 获取印章列表
     * @description
     * @method POST
     * @url /User/Seal/getLists
     * @param user_id 可选 int 用户ID,默认当前用户
     * @return
     * @return_param seal_id int 印章ID
     * @return_param user_id int 用户ID
     * @return_param name string 印章名称
     * @return_param seal string 印章路径
     * @return_param htext string 横向文内容
     * @return_param qtext string 下弦文内容
     * @return_param color string 公章颜色,red:红色 blue:蓝色 black:黑色
     * @return_param template_type string 公章类型,star:标准公章  oval:椭圆公章 rect:条形印章 dedicated:无五角星印章
     * @remark
     * @number 0
     * @author lcx
     * @date 2020-02-20
     */
    public function http_getLists()
    {
        $user_id = $this->parm['user_id'] ?? $this->user_id;
        $where['user_id'] = $user_id;
        $where['is_deleted'] = 0;
        if (!empty($this->parm['name'])) {
            $where['name'] = ['LIKE', '%' . $this->parm['name'] . '%'];
        }
        $data = $this->user_seal_model->getAll($where, '*', 1, -1);
        if (empty($data)) {
            return $this->jsonend(ReturnCodeService::NO_DATA, "暂无数据");
        }
        foreach ($data as $k => $v) {
            $data[$k]['seal_all_url'] = uploadImgPath($v['seal']);
            $data[$k]['create_time'] = date('Y-m-d H:i', $v['create_time']);
        }
        return $this->jsonend(ReturnCodeService::SUCCESS, "获取成功", $data);
    }

    /**
     * showdoc
     * @catalog API文档/用户端/企业印章
     * @title 删除印章
     * @description 印章和用户必须一致才可以删除
     * @method POST
     * @url /User/Seal/appointCompanySeal
     * @param user_id 可选 int 用户ID,默认当前用户
     * @param seal_id 必选 int 印章ID
     * @return
     * @remark
     * @number 0
     * @author lcx
     * @date 2020-02-20
     */
    public function http_del()
    {
        if (empty($this->parm['seal_id'])) {
            return $this->jsonend(ReturnCodeService::ILLEGAL_PARAM, "请选择印章");
        }
        //判断当前用户是否有该印章
        $user_id = $this->parm['user_id'] ?? $this->user_id;
        $where['user_id'] = $user_id;
        $where['seal_id'] = $this->parm['seal_id'];
        $where['is_deleted'] = 0;
        $info = $this->user_seal_model->getOne($where, 'seal_id');
        if (empty($info)) {
            return $this->jsonend(ReturnCodeService::ERROR_SEAL_DATA, "印章信息错误");
        }
        $result = $this->user_seal_model->save(['seal_id' => $info['seal_id']], ['is_deleted' => 1]);
        if ($result) {
            return $this->jsonend(ReturnCodeService::SUCCESS, "操作成功");
        }
        return $this->jsonend(ReturnCodeService::FAIL, "操作失败,请重试");
    }
}
