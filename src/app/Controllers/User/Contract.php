<?php

namespace app\Controllers\User;

use app\Services\Common\ConfigService;
use app\Services\Common\DataService;
use app\Services\Common\HttpService;
use app\Services\Common\ReturnCodeService;
use app\Services\Company\CompanyService;
use app\Services\Log\AsyncFile;
use app\Services\Stock\StockService;
use app\Services\User\UserService;
use Server\CoreBase\SwooleException;

class Contract extends Base
{

    protected $contract_model;
    protected $auth_model;
    protected $user_model;
    protected $contract_ep_model;
    protected $parts_model;
    protected $ep_part_model;
    protected $bind_model;
    protected $stoveManagementModel;
    protected $company_model;
    protected $contractAdditionalModel;
    protected $equipment_list_model;
    protected $ContractPackageRecordModel;
    protected $equipmentWaterRecordModel;
    protected $Achievement;
    protected $contract_additional_model;
    protected $userService;
    protected $stockModel;
    protected $equipmentModel;
    protected $work_eq_model;
    protected $customer_code_model;
    protected $customerCodeBillModel;
    protected $workOrderModel;
    protected $invite_model;
    protected $oa_workers_model;
    protected $operation_info_model;
    protected $energy_saving_data_test_model;

    // 前置方法，加载模型
    public function initialization($controller_name, $method_name)
    {
        parent::initialization($controller_name, $method_name);
        $this->contract_model = $this->loader->model('ContractModel', $this);
        $this->auth_model = $this->loader->model('CustomerAuthModel', $this);
        $this->user_model = $this->loader->model('CustomerModel', $this);
        $this->contract_ep_model = $this->loader->model('ContractEquipmentModel', $this);
        $this->parts_model = $this->loader->model('PartsModel', $this);
        $this->ep_part_model = $this->loader->model('EquipmentsPartsModel', $this);
        $this->bind_model = $this->loader->model('CustomerBindEquipmentModel', $this);
        $this->company_model = $this->loader->model('CompanyModel', $this);
        $this->contractAdditionalModel = $this->loader->model('ContractAdditionalModel', $this);
        $this->equipment_list_model = $this->loader->model('EquipmentListsModel', $this);
        $this->ContractPackageRecordModel = $this->loader->model('ContractPackageRecordModel', $this);
        $this->equipmentWaterRecordModel = $this->loader->model('EquipmentWaterRecordModel', $this);
        $this->Achievement = $this->loader->model('AchievementModel', $this);
        $this->contract_additional_model = $this->loader->model('ContractAdditionalModel', $this);
        $this->operation_info_model = $this->loader->model('OperationInfoModel', $this);
        $this->stockModel = $this->loader->model('StockModel', $this);
        $this->stoveManagementModel = $this->loader->model('StoveManagementModel', $this);
        $this->equipmentModel = $this->loader->model('EquipmentModel', $this);
        $this->work_eq_model = $this->loader->model('WorkEquipmentModel', $this);
        $this->customer_code_model = $this->loader->model('CustomerCodeModel', $this);
        $this->customerCodeBillModel = $this->loader->model('CustomerCodeBillModel', $this);
        $this->workOrderModel = $this->loader->model('WorkOrderModel', $this);
        $this->userService = new UserService();
        $this->invite_model = $this->loader->model('InvitationModel', $this);
        $this->oa_workers_model = $this->loader->model('OaWorkersModel', $this);
        $this->energy_saving_data_test_model = $this->loader->model('EnergySavingDataTestModel', $this);
    }

    /**
     * showdoc
     * @catalog API文档/用户端/合同相关
     * @title 合同签署
     * @description
     * @method POST
     * @url User/Contract/sign
     * @param contract_id 必选 int 合同ID
     * @param contact_type 可选 int 合同类型1电子合同-确认同意 2电子合同-电子签名 3纸质合同，默认1
     * @return {"code": 1000, "message": "签署成功", "data": ""}
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_sign()
    {
        if (empty($this->parm['contract_id'] ?? '')) {
            return $this->jsonend(-1001, '缺少参数');
        }
        $map['contract_id'] = $this->parm['contract_id'];
//        $contact_type = $this->parm['contact_type'] ?? 2;
        $data = $this->contract_model->getOne($map, '*');
        if (empty($data)) {
            return $this->jsonend(-1000, "合同不存在");
        }
        if ($data['status'] != 3) {
            return $this->jsonend(-1000, "抱歉,该合同无法进行签署!请确认合同是否有效或者已经签署过");
        }
        //判断用户是否通过实名认证
        $user_info = $this->user_model->getOne(array('user_id' => $data['user_id']), 'is_auth,realname,gender');
        if (empty($user_info) || $user_info['is_auth'] != 2) {
            return $this->jsonend(-1201, "该客户还未通过实名认证，请先进行实名");
        }
        //------------------------------------------移机新增begin------------------------------------------
        //根据合同获取工单信息
        $where = [
            'contract_id' => $map['contract_id'],
        ];
        $field = '
            rq_work_order.work_order_id,
            rq_work_order_business.business_id,
            rq_work_order.is_new_word_order,
            rq_work_equipment.equipment_id,
            rq_work_order.replacement_type,
            rq_work_order.move_change_equipments,
            rq_work_order.user_id,
            rq_work_order.equipments_id,
            rq_work_order.equipment_num
        ';
        $join = [
            ['work_order_business', 'rq_work_order.work_order_id = rq_work_order_business.work_order_id', 'LEFT'],
            ['work_equipment', 'rq_work_order.work_order_id = rq_work_equipment.work_order_id', 'LEFT'],
        ];
        $this->Achievement->table = 'work_order';
        $work_order = $this->Achievement->findJoinData($where, $field, $join);

        $is_replacement = false;
        if (!empty($work_order) && $work_order['business_id'] == 5) {
            if ($work_order['replacement_type'] == 2) {
                return $this->jsonend(-1000, "移机拆不需要签署合同");
            }
            $is_replacement = true;

        }

        //查询户号
        $customer_code_where = ['work_order_id' => $work_order['work_order_id']];
        $field = 'customer_code';
        $customer_code = $this->work_eq_model->getOne($customer_code_where, $field, []);

        //------------------------------------------移机新增end------------------------------------------
        /*
                //查询合同主板信息--处理主板配件信息
                $c_join = [
                    ['equipments e', 'e.equipments_id = rq_contract_equipment.equipments_id', 'left']
                ];
                $contract_info = $this->contract_ep_model->getAll(array('contract_id' => $this->parm['contract_id']), 'e.original_parts,e.filter_element,e.equipments_type,rq_contract_equipment.equipment_id', $c_join);
            */
        $eq_part_lists = array();
        $eq_filter_element = array();
        $eq_filter_element_num = array();
        $db_eq_filter_element_num = [];
        $edit_data['status'] = 4;
        $edit_data['effect_time'] = !empty($data['sign_date']) ? $data['sign_date'] : time();//合同生效时间默认是当前时间,如果后台设置了签约时间，就是签约时间
        $edit_data['installed_time'] = $edit_data['effect_time'];//装机时间
        $edit_data['now_date'] = time();//记录合同生效当时时间
        $edit_data['sign_client'] = empty($this->parm['sign_client'] ?? '') ? 1 : $this->parm['sign_client'];
        $edit_data['esign_filename'] = $this->parm['esign_filename'] ?? '';
        $edit_data['esign_user_sign'] = $this->parm['esign_user_sign'] ?? '';


        $add_status = false;

        $this->db->begin(function () use ($customer_code, $edit_data, &$add_status, $is_replacement, $work_order, $eq_part_lists, $eq_filter_element, $eq_filter_element_num, $data, $db_eq_filter_element_num) {
            //  try {
            //移机
            if ($is_replacement) {
                //合同生效时间，装机时间，到期时间应该与原合同一致
                $old_contract_id = $this->contract_model->getOne(['contract_id' => $this->parm['contract_id']], 'pid');
                $old_contract_info = [];
                if (!empty($old_contract_id) && !empty($old_contract_id['pid'])) {
                    $old_contract_info = $this->contract_model->getOne(['contract_id' => $old_contract_id['pid']], 'effect_time,installed_time,province,city,area,address');
                }
                $edit_data['effect_time'] = $old_contract_info['effect_time'] ?? '';
                $edit_data['installed_time'] = $old_contract_info['installed_time'] ?? '';
                //主板配件信息--如果是移机将原始合同的主板配件信息同步到移机合同
                $where = [
                    'contract_id' => $data['pid'],
                    'equipment_id' => $work_order['equipment_id'],
                    'is_delete' => 0,
                ];
                $this->Achievement->table = 'equipments_parts';
                $parts = $this->Achievement->selectData($where, '*', [], []);
                if (!empty($parts)) {
                    $this->ep_part_model->del(['contract_id' => $data['pid']]);//删除当前合同相关主板配件信息
                    foreach ($parts as $key => $value) {
                        unset($value['id']);
                        $value['contract_id'] = $this->parm['contract_id'];
                        $this->ep_part_model->add($value);
                    }
                }
                //用户绑定主板
                $bind_data['equipments_id'] = $work_order['equipments_id'];
                $bind_data['work_order_id'] = $work_order['work_order_id'];
                $bind_data['equipment_id'] = $work_order['equipment_id'];
                $bind_data['user_id'] = $work_order['user_id'];
                $bind_data['create_time'] = time();
                $bind_data['customer_code'] = $customer_code['customer_code'];
                $bind_data['is_owner'] = 2;
                $this->bind_model->addCustomerBindEquipment($bind_data);
                //判断该地区是否开通
                $address_info = $this->userService->getOwnArea($data['user_id'], '', $data['province_code'], $data['city_code'], $data['area_code'], $this->company);
                if (!$address_info) {
                    returnJsonAdmin(-1101, "该地址暂未开通服务");
                }
                //用户划拨到移机区域
                $this->Achievement->table = 'customer_administrative_center';
                $customer_administrative_center = $this->Achievement->findData(['user_id' => $work_order['user_id'], 'administrative_id' => $address_info['a_id']], 'id');
                if (empty($customer_administrative_center)) {
                    $this->Achievement->insertData([
                        'user_id' => $work_order['user_id'],
                        'operation_id' => $address_info['operation'],
                        'administrative_id' => $address_info['a_id'],
                        'update_time' => time(),
                    ]);
                }

                //更新主板地址
                $eq['province_code'] = $data['province_code'];
                $eq['city_code'] = $data['city_code'];
                $eq['area_code'] = $data['area_code'];
                $eq['province'] = $data['province'];
                $eq['city'] = $data['city'];
                $eq['area'] = $data['area'];
                $eq['address'] = $data['address'];
                $this->equipment_list_model->updateEquipmentLists($eq, array('equipment_id' => $work_order['equipment_id']));

                //更新合同主板关系表主板地址
                $eq_contr_map['contract_id'] = $this->parm['contract_id'];
                $eq_contr_map['equipment_id'] = $work_order['equipment_id'];
                $eq_contr_info = $this->contract_ep_model->getOne($eq_contr_map, 'moving_machine_number');
                if (empty($eq_contr_info)) {
                    throw new Exception("合同里无该主板信息");
                }
                $eq_contr_data['uptime'] = time();
                $eq_contr_data['moving_machine_number'] = $eq_contr_info['moving_machine_number'] + 1;
                $this->contract_ep_model->save($eq_contr_map, $eq_contr_data);

                //新增合同补充协议
                $add_data['contract_id'] = $this->parm['contract_id'];
                $add_data['equipment_id'] = $work_order['equipment_id'];
                $add_data['type'] = 1;
                $add_data['old_address'] = $old_contract_info['province'] . $old_contract_info['city'] . $old_contract_info['area'] . $old_contract_info['address'];
                $add_data['move_address'] = $data['province'] . $data['city'] . $data['area'] . $data['address'];
                $add_data['create_time'] = time();
                $this->contract_additional_model->add($add_data);

                $this->Achievement->table = 'customer';
                $user = $this->Achievement->findData(['user_id' => $work_order['user_id']], 'replacement_number');
                if (!empty($user) && $user['replacement_number'] > 0) {
                    $this->Achievement->table = 'customer';
                    $this->Achievement->setDec(['user_id' => $work_order['user_id']], 'replacement_number', 1);
                }
                //获取合同信息
                $this->Achievement->table = 'contract';
                $new_contract = $this->Achievement->findData(['contract_id' => $this->parm['contract_id']], 'contract_no,pid');
                if (!empty($new_contract)) {
                    //修改原始合同状态
                    $this->Achievement->updateData(['contract_id' => $new_contract['pid']], ['status' => 6]);
                    $this->Achievement->table = 'work_order';
                    $join = [
                        ['contract', 'rq_work_order.contract_id=rq_contract.contract_id', 'left']
                    ];
                    $old_contract = $this->Achievement->findJoinData(['rq_work_order.contract_id' => $new_contract['pid']], 'contract_no,work_order_id', $join);

                    if (!empty($old_contract)) {
                        //更新旧合同主板状态
                        $this->Achievement->table = 'contract_equipment';
                        $this->Achievement->updateData(['contract_id' => $new_contract['pid'], 'equipment_id' => $work_order['equipment_id']], ['state' => 0]);
                        //解除主板绑定关系
                        $this->Achievement->table = 'customer_bind_equipment';
                        $this->Achievement->updateData(['work_order_id' => $old_contract['work_order_id'], 'is_owner' => 2, 'user_id' => $work_order['user_id']], ['status' => 3, 'unbinding_time' => time()]);
                        //修改结算关系合同编号
                        $this->Achievement->table = 'contract_rank';
                        $this->Achievement->updateData(['contract_no' => $old_contract['contract_no']], ['contract_no' => $new_contract['contract_no']]);
                    }
                }


            } else {
                //主板配件设置
                $this->ep_part_model->del(['contract_id' => $this->parm['contract_id']]);//删除当前合同相关主板配件信息
                if (!empty($eq_part_lists)) {
                    foreach ($eq_part_lists as $k => $v) {
                        $this->ep_part_model->add($v);
                    }
                }
                if (!empty($eq_filter_element)) {
                    foreach ($eq_filter_element as $k => $v) {
                        $re = $this->ep_part_model->add($v);
                    }
                }

            }

            $this->contract_model->save(array('contract_id' => $this->parm['contract_id']), $edit_data);//修改合同信息
            $this->bind_model->save(array('customer_code' => $customer_code['customer_code'], 'is_owner' => 2, 'status' => 0), array('status' => 1));
            //出账日
            $out_bill_date = $this->customerCodeBillModel->outBillDate($edit_data['effect_time']);
            $this->customer_code_model->save(array('code' => $customer_code['customer_code'], 'state' => 0), ['state' => 1, 'out_bill_date' => $out_bill_date]);
            //查询合同主板信息
            $eq_join = [
                ['equipment_lists as el', 'rq_contract_equipment.equipment_id = el.equipment_id', 'inner'],
                ['equipments as e', 'e.equipments_id = el.equipments_id', 'inner']
            ];
            $contract_eq_info = $this->contract_ep_model->getAll(array('rq_contract_equipment.contract_id' => $this->parm['contract_id']), 'el.device_no,e.type,rq_contract_equipment.equipment_id', $eq_join);
            if (!empty($contract_eq_info)) {
                //下发指令-下发套餐
                $contract_eq = array_column($contract_eq_info, 'device_no');
                $this->contract_model->activateDevice($this->parm['contract_id'], $contract_eq);
            }

            //合同日志
            $this->addContractLog($this->parm['contract_id'], 4, "合同签署成功，正式生效。由用户端发起签署");

            //当合同签约成功且在华信模式下判断租赁下单根据推荐人减去市场推广或者城市合伙人的库存
            $joins = [
                ['work_order_business', 'rq_work_order.work_order_id=rq_work_order_business.work_order_id', 'left']
            ];
            $this->Achievement->table = 'work_order';
            $work_order_info = $this->Achievement->findJoinData(['rq_work_order.work_order_id' => $work_order['work_order_id']], 'rq_work_order_business.business_id,rq_work_order.user_id,rq_work_order.replacement_type,rq_work_order.move_change_equipments,rq_work_order.equipments_id,rq_work_order.equipment_num,rq_work_order.move_old_equipments', $joins);
            $equipment_info = $this->equipmentModel->getOne(['equipments_id' => $work_order_info['equipments_id']], 'equipments_id,type');

            if (in_array($this->company, $this->config->get('logic_mode.huaxin'))) {
                if ($equipment_info['type'] == 1 && ($work_order_info['business_id'] == 3 || ($work_order_info['business_id'] == 5 && in_array($work_order_info['replacement_type'], [1, 3]) && $work_order_info['move_change_equipments'] == 1))) {//租赁产品时订单的库存在签合同时扣除
                    //先判断用户是是否有市场推广，没得则减去所属合伙人的库存
                    $res = $this->userService->getUserBelongTo($work_order_info['user_id'], $this->company);
                    if (empty($res)) {
                        throw new SwooleException("用户推荐人信息错误");
                    }
                    //查询库存信息
                    $stock_where['role'] = $res['role'];
                    $stock_where['uid'] = $res['uid'];
                    $stock_where['equipments_id'] = $equipment_info['equipments_id'];//产品id
                    $stock_where['company_id'] = $this->company;
                    $stock_info = $this->stockModel->getOne($stock_where, 'stock_id,surplus');
                    $surplus = !empty($stock_info) ? $stock_info['surplus'] : 0;

                    //减去产品库存，并写入记录
                    StockService::changeStock($res['role'], $res['uid'], $work_order_info['equipments_id'], $work_order_info['equipment_num'], $this->company, 2);
                    StockService::addStockRecord($res['role'], $res['uid'], $work_order_info['equipments_id'], $surplus, $work_order_info['equipment_num'], 4, 2, $res['role'], $res['uid'], $this->company, '用户下单租赁产品，减少产品库存');

                    //移机拆/装和移机装工单且更换了新产品情况下增加库存
                    if ($work_order_info['business_id'] == 5 && in_array($work_order_info['replacement_type'], [1, 3]) && $work_order_info['move_change_equipments'] == 1) {
                        //查询旧产品剩余数量
                        $stock_where['equipments_id'] = $work_order_info['move_old_equipments'];
                        $old_stock_info = $this->stockModel->getOne($stock_where, 'surplus');
                        $surplus = !empty($old_stock_info) ? $old_stock_info['surplus'] : 0;
                        //增加旧产品库存
                        StockService::changeStock($res['role'], $res['uid'], $work_order_info['move_old_equipments'], $work_order_info['equipment_num'], $this->company, 1);
                        //写入库存记录
                        StockService::addStockRecord($res['role'], $res['uid'], $work_order_info['move_old_equipments'], $surplus, $work_order_info['equipment_num'], 1, 1, $res['role'], $res['uid'], $this->company, '用户移机下单更换租赁产品，增加旧产品库存');
                    }

                }

            }


            $add_status = true;
//            } catch (\Exception $exception) {
//                AsyncFile::write('contract_sign', date('Y-m-d H:i:s', time()) . '-' . $this->parm['contract_id'] . ':用户端签约失败,' . $exception->getMessage() . '///' . $exception->getLine());
//            }
        }, function ($e) {
            AsyncFile::write('contract_sign', date('Y-m-d H:i:s', time()) . '-' . $this->parm['contract_id'] . ':用户端签约失败,' . $e->error);
            return $this->jsonend(-1000, "签约失败,请重试!" . $e->error);
        });
        if ($add_status) {
            //向大数据发送通知
            //推送到大数据平台
            $order_notification = $this->config->get('order_notification');
            $gender = $user_info['gender'] == 1 ? '先生' : '女士';
            $big_data = [
                'title' => $order_notification['title'],
                'content' => mb_substr($user_info['realname'], 0, 1) . $gender . $order_notification['content']
            ];
            $url = $this->big_data_url . 'ShowData/A10';
            HttpService::post($url, json_encode(['data' => $big_data, 'flag' => 1]));
            return $this->jsonend(1000, "签约成功");
        }
        AsyncFile::write('contract_sign', date('Y-m-d H:i:s', time()) . '-' . $this->parm['contract_id'] . ':工程端签约失败');
        return $this->jsonend(-1000, "签约失败");
    }

    /**
     * showdoc
     * @catalog API文档/用户端/合同相关
     * @title 市场推广合同签署
     * @description
     * @method POST
     * @url User/Contract/workerSign
     * @param esign_filename 可选 string 电子签名成功后保存签署后的文件名称
     * @param esign_user_sign 可选 string 电子签名,用户签字图片
     * @return {"code": 1000, "message": "签署成功", "data": ""}
     * @remark
     * @number 0
     * @author tx
     * @date 2020-10-13
     */
    function http_workerSign()
    {
        //接收参数
        $esign_filename = $this->parm['esign_filename'] ?? '';
        $esign_user_sign = $this->parm['esign_user_sign'] ?? '';

        //查询申请人信息,否通过实名认证
        $where['user_id'] = $this->user_id;
        $user_info = $this->user_model->getOne($where, 'user_id,is_auth,is_dealer,username,account,source_is_default,openid');
        if (empty($user_info) || $user_info['is_auth'] != 2) {
            return $this->jsonend(-1000, '很抱歉您还未实名认证，请先进行实名');
        }
        if ($user_info['is_dealer'] == 1) {
            return $this->jsonend(-1000, '您已经是市场推广');
        }
        //查询申请状态
        $this->Achievement->table = 'oa_dealer_apply';
        $apply_info = $this->Achievement->findData($where);
        if (empty($apply_info)) {
            return $this->jsonend(-1000, '请先提交市场推广申请');
        }
        if ($apply_info['apply_status'] != 2) {
            return $this->jsonend(-1000, '很抱歉您未通过市场推广申请或您的申请正在处理中...');
        }
        //查询市场推广信息
        $this->Achievement->table = 'oa_workers';
        $workers = $this->Achievement->findData(['workers_number' => $user_info['account']], 'workers_id,sign_type,position,sign_status');
        if (!empty($workers) && $workers['position'] == 3) {
            return $this->jsonend(-1000, '您已经是市场推广了');
        }
        if (!empty($workers) && $workers['sign_status'] == 2) {
            return $this->jsonend(-1000, '您已经签过合同了');
        }
        if ($workers['sign_type'] == 3) {
            return $this->jsonend(-1000, '纸质合同不需要在线上签署合同');
        }
       // $setPassword = setPassword('123456');
//        $insert_data = [
//            'user_id' => $this->user_id,
//            'workers_name' => $apply_info['username'],
//            'workers_password' => $setPassword ? $setPassword['password'] : '',
//            'workers_phone' => $apply_info['phone'],
//            'workers_number' => $user_info['account'],
//            'position' => 3,
//            'workers_id_card' => $apply_info['id_card'],
//            'workers_entry_time' => time(),
//            'inspection_start_time' => time(),//当前考核开始时间
//            'inspection_end_time' => strtotime(date("Y-m-d 23:59:59", strtotime("+1 years", time()))),//当前考核截止时间
//            'workers_push_people' => $apply_info['referee'],//推荐人id
//            'strict' => $setPassword ? $setPassword['strict'] : '',
//            'add_time' => time(),
//            'esign_filename' => $esign_filename,
//            'esign_user_sign' => $esign_user_sign,
//            'sign_status' => 2,
//            'sign_time' => time(),
//        ];

        //查询市场推广推荐人信息
//        $inviter_info = $this->user_model->getOne(['user_id' => $apply_info['referee']], 'user_id,is_auth,is_dealer,username,account,telphone,id_card,source,add_time');
//        if (empty($inviter_info)) {
//            return $this->jsonend(-1000, '推荐人信息不存在');
//        }
//        $inviter = $this->Achievement->findData(['workers_number' => $inviter_info['account']], 'workers_id');
//        if (empty($inviter)) {
//            //不存在职工表中，则添加成为普通业务员
//            $setPassword = setPassword('123456');
//            $inviter_data = [
//                'user_id' => $inviter_info['user_id'],
//                'workers_name' => $inviter_info['username'],
//                'workers_password' => $setPassword ? $setPassword['password'] : '',
//                'workers_phone' => $inviter_info['telphone'],
//                'workers_number' => $inviter_info['account'],
//                'workers_id_card' => $inviter_info['id_card'],
//                'workers_entry_time' => $inviter_info['add_time'],
//                'workers_push_people' => $inviter_info['source'],//推荐人ID
//                'strict' => $setPassword ? $setPassword['strict'] : '',
//                'add_time' => time(),
//            ];
//        } else {
//            $inviter_data = [];
//        }

//        //若邀请人是默认推荐人且变更邀请人的增加邀请记录,并修改上级，只会走一次
//        if ($user_info['source_is_default'] == 2 && !empty($inviter_info['user_id']) && $apply_info['referee_is_default'] != 2) {
//            $update_user = [
//                'source' => $inviter_info['user_id'],
//                'source_is_valid' => 2,//是否生效
//                'source_is_default' => 1 //不是默认推荐人
//            ];
//        } else {
//            $update_user = [];
//        }

        $check = false;
        $this->db->begin(function () use (&$check, $where, $user_info, $workers) {
            //合同签署成功修改用户信息
            $this->user_model->save($where, ['is_dealer' => 1]);

//            //添加市场推广信息
//            $this->Achievement->table = 'oa_workers';
//            if (empty($workers)) {
//                $this->Achievement->insertData($insert_data);
//            } else {
//                unset($insert_data['user_id']);
//                unset($insert_data['workers_password']);
//                unset($insert_data['workers_number']);
//                unset($insert_data['workers_entry_time']);
//                unset($insert_data['workers_push_people']);
//                unset($insert_data['strict']);
//                unset($insert_data['add_time']);
//                $this->Achievement->updateData(['workers_id' => $workers['workers_id']], $insert_data);
//            }
//            //添加推荐人业务员信息
//            if ($inviter_data) {
//                $this->Achievement->insertData($inviter_data);
//            }
//            //添加邀请记录
//            if ($update_user) {
//                $this->user_model->save($where, $update_user);
//                $this->invite_model->addInviteRecord($this->user_id, $inviter_info['user_id'], $user_info['openid'], 1, 0);
//            }

            $check = true;
        }, function ($e) {
            return $this->jsonend(-1000, '签约失败,请重试!', $e->error);
        });

        if ($check) {
            return $this->jsonend(1000, '合同签署成功');
        }
        return $this->jsonend(-1000, '合同签署失败');

    }

    /**
     * showdoc
     * @catalog API文档/用户端/合同相关
     * @title 合同详情
     * @description
     * @method POST
     * @url User/Contract/getContractInfo
     * @param contract_id 必选 int 合同ID
     * @return {"code":1000,"message":"获取成功","data":{"contract_info":{"status":"4","contract_no":"CNA16575948802157","contract_money":"1000.00","contract_deposit":"400.00","is_effect":"2","effect_time":"2018-10-16","contract_cycle":"55","contract_class":"1","esign_path":"https:\\/\\/\\/jscloud.youheone.com\\/p\\/pdf\\/PDF_User\\/20181016_CNA16575948802157_57_1.pdf","c","contract_time_start_format":"2018-10-16","contract_time_end_format":"2018-12-10","contract_time_start":["2018","10","16"],"contract_time_end":["2018","12","10"],"province":"重庆市","city":"重庆市","area":"渝北区","address":"觅凰庭","renew_money":"600.00","esign_user_sign":"https:\\/\\/\\/jscloud.youheone.com\\/p\\/pdf\\/img_yz\\/20181016110359CNA165759488021571539659039_39_8207.png"},""},"rental_user_info":{"realname":"Liu","auth_type":"1","id_number":"500235199201303340","business_license_pic":null,"telphone":"18883880448"},"company_info":{"company_id":"1","company_name":"重庆优合一众网络科技有限公司","business_license_pic":"J9300300403043","company_addr":"重庆市渝北区山顶道国宾城12栋13-13","telphone":"023-86838055","contacts":"王**","contacts_tel":"18549302304","company_seal":"https:\\/\\/\\/jscloud.youheone.com\\/p\\/pdf\\/img_yz\\/\\/yinzhang.png","s","status":"1"},"equipments_info":{"list":[{"equipments_number":"00000001","equipments_name":"DR75-C5型直饮水机","model":"Product 7"}],"total_num":1},"original_parts":[{"parts_id":"6","parts_name":"减压阀","parts_pic":"https:\\/\\/\\/qn.youheone.com\\/f\\/\\/fad4520180911132244480.png","c","cycle":"720"}],"package_info":{"package_mode":"1","package_value":"55","package_money":"600.00","package_cycle":"55"},"contract_additional":[]}}
     * @return_param contract_info array 合同基本信息
     * @return_param rental_user_info array 客户信息
     * @return_param company_info array 公司信息
     * @return_param equipments_info array 主板信息
     * @return_param original_parts array 原始配件
     * @return_param package_info array 套餐信息
     * @return_param contract_additional array 补充协议
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_getContractInfo()
    {
        if (empty($this->parm['contract_id'] ?? '')) {
            return $this->jsonend(-1001, '缺少参数');
        }
        $map['contract_id'] = $this->parm['contract_id'];
        $data = $this->contract_model->getOne($map, '*');
        if (empty($data)) {
            return $this->jsonend(-1000, "合同不存在");
        }
        /*         * **********************合同信息********************* */
        $contract_info = array();
        $contract_info['contract_id'] = $data['contract_id'];//合同id
        $contract_info['contact_type'] = $data['contact_type'];//合同签署方式
        $contract_info['view_layout'] = $data['view_layout'];//合同界面布局样式1润泉版 2济南铭沁版
        $contract_info['status'] = $data['status'];
        $contract_info['operation_id'] = $data['operation_id'];
        $contract_info['company_id'] = $data['company_id']; // 入驻商ID
        $contract_info['contract_no'] = $data['contract_no']; //合同编号
        $contract_info['contract_money'] = $data['contract_money']; //合同金额
        $contract_info['contract_deposit'] = $data['contract_deposit']; //押金
        $contract_info['effect_time'] = empty($data['effect_time']) ? '' : date('Y-m-d', $data['effect_time']);


        if ($this->dev_mode == 1) {
            $contract_info['esign_path'] = empty($data['esign_filename']) ? '' : $this->config->get('debug_config.static_resource_host') . $data['esign_filename'];
        } else {

            $contract_info['esign_path'] = empty($data['esign_filename']) ? '' : $this->config->get('static_resource_host') . $data['esign_filename'];
        }
        $contract_info['contract_time_start_format'] = date('Y-m-d', time()); //合同开始时间
        $contract_info['contract_time_start_format'] = date('Y-m-d', $data['installed_time']); //合同开始时间
        $contract_info['contract_time_start'] = explode('-', $contract_info['contract_time_start_format']);
        $contract_info['contract_time_end'] = '--';
        $contract_info['province'] = $data['province'];
        $contract_info['city'] = $data['city'];
        $contract_info['area'] = $data['area'];
        $contract_info['address'] = $data['address'];
        $contract_info['installProperty'] = '家用机';
        $contract_info['waterPressure'] = ''; //水压
        $contract_info['capContractMoney'] = num_to_rmb($contract_info['contract_money']);
        $contract_info['cycle'] = ''; //换芯周期
        $contract_info['equipmentType'] = 'RO反渗透直饮机';//主板型号
        $contract_info['signRepresent'] = '';//签约代表
        if ($this->dev_mode == 1) {
            $static_resource_host = $this->config->get('debug_config.static_resource_host');
        } else {
            $static_resource_host = $this->config->get('static_resource_host');
        }

        /**         * *********************乙方信息---客户********************* */
        $rental_user_info = array();
        //认证信息
        $auth_info = $this->auth_model->getAll(array('user_id' => $data['user_id']), '*');
        if (empty($auth_info) || $auth_info['status'] != 2) {
            return $this->jsonend(-1101, "您还未通过认证,请先认证");
        }
        $rental_user_info['realname'] = $auth_info['realname'];
        $rental_user_info['auth_type'] = $auth_info['auth_type'];
        $rental_user_info['id_number'] = $auth_info['id_number'];
        $rental_user_info['user_id'] = $data['user_id'];
        if (in_array($rental_user_info['auth_type'], [2, 3])) {
            $rental_user_info['realname'] = $auth_info['company_name'];
            $rental_user_info['id_number'] = $auth_info['business_license_number'];
        }
        $rental_user_info['business_license_pic'] = $auth_info['business_license_pic'];
        //基本信息
        $user_info = $this->user_model->getOne(array('user_id' => $data['user_id']), 'user_class,telphone,emergency_contact');
        $user_info['emergency_contact'] = empty($user_info['emergency_contact']) ? '' : json_decode($user_info['emergency_contact'], true);
        $rental_user_info['telphone'] = $user_info['telphone'];
        $rental_user_info['nailEmail'] = '';//邮箱
        $rental_user_info['user_class'] = $user_info['user_class'];//邮箱
        $rental_user_info['nailUrgentTel'] = empty($user_info['emergency_contact']) ? '' : $user_info['emergency_contact']['tel'];//紧急联系电话
        $arr['rental_user_info'] = $rental_user_info;
        if ($rental_user_info['user_class'] == 2) {
            $static_resource_host = $this->config->get('qiniu.qiniu_url');
        }
        $contract_info['esign_user_sign'] = empty($data['esign_user_sign']) ? '' : $static_resource_host . $data['esign_user_sign'];

        $arr['contract_info'] = $contract_info;

        /**         * *********************甲方信息---公司********************* */
        //$arr['company_info'] = $this->company_model->getOne(array('company_id' => $data['company_id']), '*');
        $company_info = CompanyService::getCompanyInfo($data['company_id']);
        //获取运营中心的印章
        $operation_info = $this->operation_info_model->getOne(['o_id' => $contract_info['operation_id']], 'contract_company_seal,contract_company_name');
        if (!empty($operation_info) && !empty($operation_info['contract_company_seal'])) {
            $company_info['company_name'] = $operation_info['contract_company_name'];
            $company_info['company_seal'] = $this->config->get('qiniu.qiniu_url') . $operation_info['contract_company_seal'];
        }
        $arr['company_info'] = $company_info;

        /*         * **********************主板信息********************* */
        $equipment_info = array();
        $join = [
            ['equipment_lists as el', 'el.equipment_id = rq_contract_equipment.equipment_id', 'inner'],
            ['equipments as e', 'e.equipments_id = el.equipments_id', 'inner'],

        ];
        $contract_ep_info = $this->contract_ep_model->getAll(array('rq_contract_equipment.contract_id' => $this->parm['contract_id']), 'rq_contract_equipment.*,e.equipments_name,e.original_parts,el.device_no,equipments_orgin_money', $join);
        if (!empty($contract_ep_info)) {
            foreach ($contract_ep_info as $k => $v) {
                $equipment['equipments_number'] = $v['device_no'];
                $equipment['equipments_name'] = $v['equipments_name'];
                $equipment['equipments_orgin_money'] = $v['equipments_orgin_money'];
                $equipment['cap_equipments_orgin_money'] = num_to_rmb($v['equipments_orgin_money']);
                $equipment_info[] = $equipment;
            }
        }
        $arr['equipments_info']['list'] = $equipment_info;
        $arr['equipments_info']['total_num'] = count($equipment_info);

        /*         * **********************原始配件信息********************* */
        $parts = array();
        if (!empty($contract_ep_info)) {
            $original_parts = json_decode($contract_ep_info[0]['original_parts'], true);
            if (!empty($original_parts)) {
                foreach ($original_parts as $k => $v) {
                    $part_info = $this->parts_model->getOne(array('parts_id' => $v['parts_id']), 'parts_name,parts_pic');
                    $part['parts_id'] = $v['parts_id'];
                    $part['parts_name'] = $part_info['parts_name'];
                    $part['parts_pic'] = $this->config->get('qiniu.qiniu_url') . $part_info['parts_pic'];
                    $part['cycle'] = $v['cycle'];
                    $parts[] = $part;
                }
            }
        }

        $arr['original_parts'] = $parts;

        /*         * **********************合同套餐信息********************* */
        /* $package_info['package_mode'] = $data['package_mode'];
         $package_info['package_value'] = $data['package_value'];
         $package_info['package_money'] = $data['package_money'];
         $package_info['package_cycle'] = $data['package_cycle'];
         $arr['package_info'] = $package_info;*/

        // 获取合同补充协议信息
        $where['rq_contract_additional.contract_id'] = $this->parm['contract_id'];
        $join = [
            ['equipment_lists el', 'el.equipment_id = rq_contract_additional.equipment_id', 'left join']
        ];
        $field = 'rq_contract_additional.*,el.device_no';
        $order = ['create_time' => 'DESC'];
        $contract_additional = $this->contractAdditionalModel->getAll($where, $field, $join, $order);
        if ($contract_additional) {
            $type = ['1' => '移机协议', '2' => '拆机协议'];
            $status = ['1' => '待签署', '2' => '已签署', '3' => '作废'];
            foreach ($contract_additional as $k => $v) {
                $contract_additional[$k]['type'] = $type[$v['type']];
                $contract_additional[$k]['status'] = $status[$v['status']];
                if (strlen($v['create_time']) > 5) {
                    $contract_additional[$k]['create_time'] = date('Y-m-d H:i:s', $v['create_time']);
                } else {
                    $contract_additional[$k]['create_time'] = '';
                }
                if (strlen($v['sign_time']) > 5) {
                    $contract_additional[$k]['sign_time'] = date('Y-m-d H:i:s', $v['sign_time']);
                } else {
                    $contract_additional[$k]['sign_time'] = '';
                }
            }
        }
        $arr['contract_additional'] = $contract_additional;

        //获取续费优惠券金额
        $this->Achievement->table = 'coupon_cate';
        $coupon = $this->Achievement->findData(['put_type' => 5, 'status' => 1], 'money');
        $arr['coupon'] = ($coupon['money'] ?? 0) / 100;

        return $this->jsonend(1000, "获取成功", $arr);
    }

    /**
     * showdoc
     * @catalog API文档/用户端/合同相关
     * @title 获取申请市场推广合同详情
     * @description
     * @method POST
     * @url User/Contract/getApplyContract
     * @param
     * @return {"code":1000,"message":"获取成功","data":{}}
     * @return_param user_id int 用户id
     * @return_param workers_id int 市场推广id
     * @return_param workers_name string 姓名
     * @return_param workers_phone int 电话
     * @return_param workers_number int 工号
     * @return_param workers_id_card int 身份证号码
     * @return_param esign_filename string 电子签署合同文件
     * @return_param esign_user_sign string 用户签字图片
     * @return_param sign_type string 合同签署方式1电子合同2电子合同3纸质合同
     * @return_param sign_status string 合同签署状态1待签2已签
     * @return_param sign_time string 签署时间
     * @return_param company_info array 公司信息
     * @remark
     * @number 0
     * @author tx
     * @date 2020-10-14
     */
    public function http_getApplyContract()
    {
        /**         * *********************乙方信息---市场推广********************* */
        $where['user_id'] = $this->user_id;
        $this->Achievement->table = 'oa_dealer_apply';
        $info = $this->Achievement->findData($where, '*', []);
        if (empty($info)) {
            return $this->jsonend(-1000, '市场推广申请信息不存在');
        }
        if ($info['apply_status'] != 5) {
            return $this->jsonend(-1000, '你还未签合同');
        }

        $data = $info;

        /**         * *********************甲方信息---公司********************* */
        $user_info = $this->user_model->getOne(['user_id' => $this->user_id], 'user_id,user_class,user_belong_to_company');
        if (empty($user_info)) {
            return $this->jsonend(-1000, '用户信息不存在');
        }
        $company_info = CompanyService::getCompanyInfo($user_info['user_belong_to_company']);
        //获取运营中心的印章
        $center_info = $this->customer_administrative_center_model->getOne(['user_id' => $this->user_id]);
        if (empty($center_info)) {
            return $this->jsonend(-1000, '用户所属区域错误');
        }
        $operation_info = $this->operation_info_model->getOne(['o_id' => $center_info['operation_id']], 'contract_company_seal,contract_company_name');
        if (!empty($operation_info) && !empty($operation_info['contract_company_seal'])) {
            $company_info['company_name'] = $operation_info['contract_company_name'];
            $company_info['company_seal'] = $this->config->get('qiniu.qiniu_url') . $operation_info['contract_company_seal'];
        }
        $data['company_info'] = $company_info;
        //获取市场推广签署配置
        $config = ConfigService::getConfig('dealer_sign_way', true, $this->company);
        $work_info = [];
        //获取合同信息
        $worker_contract = $this->oa_workers_model->getOne($where, 'workers_id,esign_filename,esign_user_sign,sign_type,sign_status,sign_time,contract_enclosure');
        if (!empty($worker_contract)) {
            $worker_contract['esign_filename'] = UploadImgPath($worker_contract['esign_filename']);
            $contract_enclosure = !empty($worker_contract['contract_enclosure']) ? json_decode($worker_contract['contract_enclosure'], true)[0] : [];

            if (!empty($contract_enclosure['content'])) {
                foreach ($contract_enclosure['content'] as $k => $v) {
                    $contract_enclosure['content'][$k]['new_name'] = UploadImgPath($v['new_name']);
                }
            }
            if (!empty($contract_enclosure['time'])) {
                $contract_enclosure['time'] = date('Y-m-d H:i', $contract_enclosure['time']);
            }
            $worker_contract['contract_enclosure'] = $contract_enclosure;
            $work_info = $worker_contract;
            $work_info['contract_time_start_format'] = date('Y-m-d', $worker_contract['sign_time']); //合同开始时间
            $work_info['contract_time_start'] = explode('-', $work_info['contract_time_start_format']);

            $work_info['sign_time'] = !empty($work_info['sign_time']) ? date('Y-m-d H:i', $work_info['sign_time']) : '';
        } else {
            $work_info['sign_status'] = 1;//未签署状态
            $work_info['sign_type'] = $config;
        }
        $data['contract_info'] = $work_info;

        return $this->jsonend(1000, '获取成功', $data);

    }

    /**
     * showdoc
     * @catalog API文档/用户端/合同相关
     * @title 获取用户合同列表
     * @description
     * @method POST
     * @url User/Contract/getMyContractList
     * @param page 可选 int 页数，默认1
     * @param pagesize 可选 int 每页条数，默认10
     * @return { "code": 1000,"message": "获取列表成功","data": [{"contract_no": "CNA16700500823908","renew_date": "55","province": "重庆市","city": "重庆市", "area": "渝北区","address": "觅凰庭","contract_id": "3","status": "4","exire_date": "2018-12-10","contract_class": "1","status_name": "生效中" }]}
     * @return_param contract_no string 合同编号
     * @return_param renew_date string 合同续费金额，单位元
     * @return_param province string 合同省份
     * @return_param city string 合同城市
     * @return_param area string 合同区域
     * @return_param contract_id string 合同ID
     * @return_param status string 合同状态
     * @return_param exire_date string 到期时间
     * @return_param status_name string 状态名称
     * @return_param contract_class int 合同分类 1租赁合同 2购买合同
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_getMyContractList()
    {
        $page = $this->parm['page'] ?? 1;
        $pagesize = $this->parm['pagesize'] ?? 10;
        $where = ['rq_customer_bind_equipment.user_id' => $this->user_id,'rq_customer_bind_equipment.is_owner'=>['<>',1]];
        $join = [['customer_code e', 'e.code = rq_customer_bind_equipment.customer_code', 'left']];
        $field = "e.contract_id";
        $this->Achievement->table = "customer_bind_equipment";
        $contract_ = $this->Achievement->selectData($where, $field, $join);
        if (empty($contract_)) return $this->jsonend(ReturnCodeService::FAIL,'暂无相关数据');
        $contract_id = [];
        foreach ($contract_ as $key => $value) {
            if (!empty($value['contract_id'])) $contract_id[] = $value['contract_id'];
        }
        $map['contract_id'] = ['in',$contract_id];
        $field = 'contract_no,province,city,area,address,contract_id,status,contact_type';
        $data = $this->contract_model->getAll($map, $field, ['add_time' => 'DESC'], $page, $pagesize);
        if (empty($data)) return $this->jsonend(-1003, "暂无相关数据");
        foreach ($data as $k => $v) {
            $data[$k]['status_name'] = $this->config->get('contract_status')[$v['status']];
        }
        return $this->jsonend(1000, "获取列表成功", $data);
    }


    /**
     * showdoc
     * @catalog API文档/用户端/合同相关
     * @title 重新下发套餐
     * @description
     * @method POST
     * @url /User/Contract/againPackage
     * @param contract_id 必选 int 合同ID
     * @param equipment_id 必选 int 主板ID
     * @return
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-12-5
     */
    public function http_againPackage()
    {
        if (empty($this->parm['contract_id'] ?? '')) {
            return $this->jsonend(-1002, "缺少参数合同ID");
        }
        if (empty($this->parm['equipment_id'] ?? '')) {
            return $this->jsonend(-1002, "缺少参数主板ID");
        }
        $info = $this->ContractPackageRecordModel->getOne(['contract_id' => $this->parm['contract_id'], 'equipment_id' => $this->parm['equipment_id'], 'status' => 1]);
        if (empty($info)) {
            return $this->jsonend(-1000, "不存在未下发的套餐");
        }
        //查询主板编号
        $eq = $this->equipment_list_model->findEquipmentLists(['equipment_id' => $this->parm['equipment_id']], 'device_no,switch_machine,status');
        if (empty($eq)) {
            return $this->jsonend(-1000, "主板不存在");
        }
        if ($eq['status'] == 1) {
            return $this->jsonend(-1000, "主板已离线，请联网后再下发");
        }
        if ($eq['switch_machine'] == 0) {
            return $this->jsonend(-1000, "主板已关机，请开机后再下发");
        }
        //下发-绑定套餐
        $params = ['sn' => $eq['device_no'], 'working_mode' => $info['working_mode'], 'filter_element_max' => json_decode($info['filter_element_max'], true)];
        $path = '/House/Issue/bindingPackage';
        HttpService::Thrash($params, $path);

        sleepCoroutine(2000);

        //下发--数据同步
        $data_sync_params['sn'] = $eq['device_no'];
        $data_sync_params['used_days'] = $info['used_days'];
        $data_sync_params['used_traffic'] = $info['used_traffic'];
        $data_sync_params['remaining_days'] = $info['remaining_days'];
        $data_sync_params['remaining_traffic'] = $info['remaining_traffic'];
        $data_sync_path = '/House/Issue/data_sync';
        HttpService::Thrash($data_sync_params, $data_sync_path);
        $this->redis->del('device_heartbeat:' . $eq['device_no']);//删除redis心跳
        //请求心跳
        sleepCoroutine(2000);
        $state_params['sn'] = $eq['device_no'];
        $state_path = '/House/Issue/heartbeat';
        HttpService::Thrash($state_params, $state_path);
        //修改下发记录
        $res = $this->ContractPackageRecordModel->save(['id' => $info['id']], ['last_hand_time' => time()]);
        if ($res) {
            return $this->jsonend(1000, "请求发送成功,正在下发中");
        }
        return $this->jsonend(1000, "请求发送失败");
    }

    /**
     * showdoc
     * @catalog API文档/用户端/合同相关
     * @title 获取用户的pdf合同文件列表
     * @description
     * @method POST
     * @url /User/Contract/getContractPdf
     * @param contract_id 可选 int 合同ID（二选一）
     * @param work_order_id 可选 int 工单ID（二选一）
     * @return
     * @remark
     * @number 0
     * @author tx
     * @date 2020-10-23
     */
    public function http_getContractPdf()
    {
        //接收参数
        $contract_id = $this->parm['contract_id'] ?? '';
        $work_order_id = $this->parm['work_order_id'] ?? '';

        if (empty($contract_id) && empty($work_order_id)) {
            return $this->jsonend(-1001, '缺少参数');
        }
        $map = [];
        if (!empty($this->parm['contract_id'])) {
            $map['contract_id'] = $this->parm['contract_id'];
        }
        if (!empty($work_order_id)) {
            //根据勘测工单获取对应新装合同信息
            $where['work_order_id'] = $work_order_id;
            $work_info = $this->workOrderModel->getOne($where, 'contract_id,user_id');
            if (empty($work_info)) {
                return $this->jsonend(-1003, "工单信息错误");
            }
            if (!empty($work_info['contract_id'])) {
                $map['contract_id'] = $work_info['contract_id'];
            } else {
                $wheres['p_work_order_id'] = $work_order_id;
                $p_work_info = $this->workOrderModel->getOne($wheres, 'contract_id,user_id');
                $map['contract_id'] = $p_work_info['contract_id'] ?? 0;
            }

        }
        $list = $this->contract_model->getOne($map, 'gas_fee,sign_date,contract_id,user_id,audit_status,type,status,related_name,related_tel,related_id_card,quota_money,contract_enclosure,effect_time,contract_party');
        if (empty($list)) {
            return $this->jsonend(-1000, "合同不存在");
        }
        $data = $list;
        $gas_fee = formatMoney($data['gas_fee'], 2);
        //新增----获取用户是否采集数据
        $data['install_used'] = 0;
        $data['try_used'] = 0;
        if (!empty($data['contract_id'])) {
            $energy_data = $this->energy_saving_data_test_model->getAll(['contract_id' => $data['contract_id']], 'type,energy_saving_amount');
            if (!empty($energy_data)) {
                foreach ($energy_data as $k => $v) {
                    if ($v['type'] == 1) {
                        $data['install_used'] = !empty($v['energy_saving_amount']) ? 1 : 0;
                    } else {
                        $data['try_used'] = !empty($v['energy_saving_amount']) ? 1 : 0;
                    }
                }
            }
        }

        $contract_enclosure = !empty($data['contract_enclosure']) ? json_decode($data['contract_enclosure'], true) : [];
        if (!empty($contract_enclosure)) {
            foreach ($contract_enclosure as $k => $v) {
                $contract_enclosure[$k]['time'] = !empty($v['time']) ? date('Y-m-d H:i', $v['time']) : '';
                if (!empty($v['content'])) {
                    $content = $v['content'];
                    foreach ($content as $k1 => $v1) {
                        $content[$k1]['new_name'] = UploadImgPath($v1['new_name']);
                    }
                    $contract_enclosure[$k]['content'] = $content;
                }

            }
        }
        $data['quota_money'] = formatMoney($data['quota_money'], 2);
        $data['sign_date'] = date('Y-m-d', $data['sign_date']);
        $data['effect_time'] = !empty($data['effect_time']) ? ($data['effect_time'] ? date('Y-m-d', $data['effect_time']) : '') : $data['sign_date'];
        $data['contract_enclosure'] = $contract_enclosure;
        $data['gas_fee'] = $gas_fee;


        return $this->jsonend(1000, "获取成功", $data);

    }


    /**
     * showdoc
     * @catalog API文档/用户端/合同相关
     * @title 获取节能采集数据新装试用
     * @description
     * @method POST
     * @url /User/Contract/getEnergySaveData
     * @param contract_id 必选 int 合同ID
     * @return {"code":1000,"data":{"contract_id":1,"type":1},"msg":"获取成功"}
     * @return_param id int 保管清单序号
     * @return_param contract_id int 合同id
     * @return_param cooker_num int 炉具数量
     * @return_param cooker_data array 节能炉具数据
     * @return_param custody_list array 保管清单
     * @return_param energy_saving_amount array 节能优惠金额
     * @return_param esign_user_sign string 用户签字图片
     * @return_param esign_user_sign_time string 签字时间
     * @return_param type 必选 int 数据类型0测试1新装
     * @return_param cooker_name int 炉具名称
     * @return_param test_method int 炉具节能测试方法
     * @return_param before_testing_i string 改造前炉具燃烧时间分
     * @return_param before_testing_s string 改造前炉具燃烧时间秒
     * @return_param before_testing_gas_consumption int 改造前燃气消耗m³
     * @return_param after_testing_i int 改造后炉具燃烧时间分
     * @return_param after_testing_s int 改造后炉具燃烧时间秒
     * @return_param after_testing_gas_consumption int 改造后燃气消耗m³
     * @remark
     * @number 0
     * @author tx
     * @date 2020-11-12
     */
    public function http_getEnergySaveData()
    {
        //接收参数
        $contract_id = $this->parm['contract_id'] ?? '';
        $type = $this->parm['type'] ?? 0;

        if (empty($contract_id)) {
            return $this->jsonend(-1001, '缺少参数');
        }
        $where['contract_id'] = $contract_id;
        $where['type'] = $type;

        $data = $this->energy_saving_data_test_model->getOne($where);
        if (empty($data)) {
            return $this->jsonend(-1000, "暂无数据");
        }
        //查询炉具
        $stove_management = $this->stoveManagementModel->getAll(['is_delete'=>0]);
        if (empty($stove_management)) return $this->jsonend(ReturnCodeService::FAIL,'炉具信息不存在');
        //新增----获取用户是否采集数据
        $data['energy_saving_amount'] = $data['energy_saving_amount'] ? json_decode($data['energy_saving_amount'], true) : [];
        $cooker_data = $data['cooker_data'] ? json_decode($data['cooker_data'], true) : [];
        $data['custody_list'] = $data['custody_list'] ? json_decode($data['custody_list'], true) : [];
        $data['esign_user_sign'] = $data['esign_user_sign'] ? UploadImgPath($data['esign_user_sign']) : '';
        $data['esign_user_sign_time'] = $data['esign_user_sign_time'] ? dateFormat($data['esign_user_sign_time']) : 0;
        $data['create_time'] = $data['create_time'] ? dateFormat($data['create_time']) : 0;
        if (!empty($cooker_data)) {
            foreach ($cooker_data as $k => $v) {
                $cooker_data[$k]['cooker_name_desc'] = searchArray1($this->config->get('cooker_type'), 'id', $v['cooker_name'], 'name');
            }
        }
        $data['cooker_data'] = $cooker_data;
        return $this->jsonend(1000, "获取成功", $data);
    }

    /**
     * showdoc
     * @catalog API文档/用户端/合同相关
     * @title 获取合同附件
     * @description
     * @method POST
     * @url /User/Contract/contractEnclosure
     * @param contract_id|identification_id 必选 int 合同ID或者识别表ID
     * @return_param new_name string 图片路径
     * @return_param new_name_url string 完整图片路径
     * @return_param old_name string 名称
     * @return_param type string 类型
     * @return
     * @remark
     * @number 0
     * @author xln
     * @date 2020-9-18
     */
    public function http_contractEnclosure(){

        if (empty($this->parm['contract_id'])) {
            if (empty($this->parm['identification_id'])) return $this->jsonend(ReturnCodeService::FAIL, '参数缺失：合同ID或识别ID');
            $this->Achievement->table = 'contract';
            $where = ['identification_id' => $this->parm['identification_id']];
            $contract_data = $this->Achievement->findData($where, 'contract_id');
            if (empty($contract_data)) return $this->jsonend(ReturnCodeService::FAIL, '暂无数据');
            $contract_id = $contract_data['contract_id'];
        } else {
            $contract_id = $this->parm['contract_id'];
        }
        $contract_data = $this->contract_model->contractEnclosure($contract_id,$this->parm['type']??1);
        if (empty($contract_data)) return $this->jsonend(ReturnCodeService::FAIL,'暂无数据');
        return $this->jsonend(ReturnCodeService::SUCCESS,'获取成功',$contract_data);
    }


}
