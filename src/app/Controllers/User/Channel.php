<?php
/**
 * Created by PhpStorm.
 * User: asus
 * Date: 2020/2/25
 * Time: 16:35
 */

namespace app\Controllers\User;

use app\Services\Common\ConfigService;
use app\Services\Common\NotifiyService;
use app\Services\Common\CommonService;
use app\Services\Common\ReturnCodeService;
use app\Services\Common\VaildateService;
use app\Wechat\WxAuth;
use Server\Components\CatCache\CatCacheRpcProxy;


/**
 * 渠道模块API
 */
class Channel extends Base
{

    protected $channel_apply_model;//渠道申请模型
    protected $channel_model;//渠道模型
    protected $user_model;//用户模型
    protected $channel_promoters_model;//渠道推广模型
    protected $customer_account_bill_model;//用户账户账单模型
    protected $work_order_model;//工单模型
    protected $channel_commission_config_model;//渠道分成模型
    protected $oa_workers_model;//市场推广模型
    protected $channel_promoters_commission_config_model;//推广员分成模型
    protected $equipment_model;//产品模型
    protected $auth_model;//实名认证模型
    protected $bank_card_model;//银行卡模型
    protected $customer_account_model;//用户账户模型
    protected $oa_balance_config_model;//结算配置模型
    protected $funds_record_model;//资金变动记录模型
    protected $bankCardModel;
    protected $withdrawModel;


    public function initialization($controller_name, $method_name)
    {
        parent::initialization($controller_name, $method_name);
        $this->channel_apply_model = $this->loader->model('ChannelApplyModel', $this);
        $this->channel_model = $this->loader->model('ChannelModel', $this);
        $this->user_model = $this->loader->model('CustomerModel', $this);
        $this->channel_promoters_model = $this->loader->model('ChannelPromotersModel', $this);
        $this->customer_account_bill_model = $this->loader->model('CustomerAccountBillModel', $this);
        $this->work_order_model = $this->loader->model('WorkOrderModel', $this);
        $this->channel_commission_config_model = $this->loader->model('ChannelCommissionConfigModel', $this);
        $this->channel_promoters_commission_config_model = $this->loader->model('ChannelPromotersCommissionConfigModel', $this);
        $this->oa_workers_model = $this->loader->model('OaWorkersModel', $this);
        $this->equipment_model = $this->loader->model('EquipmentModel', $this);
        $this->auth_model = $this->loader->model('CustomerAuthModel', $this);
        $this->bank_card_model = $this->loader->model('BankCardModel', $this);
        $this->customer_account_model = $this->loader->model('CustomerAccountModel', $this);
        $this->oa_balance_config_model = $this->loader->model('OaBalanceConfigModel', $this);
        $this->funds_record_model = $this->loader->model('FundsRecordModel', $this);
        $this->bankCardModel = $this->loader->model('BankCardModel', $this);
        $this->withdrawModel = $this->loader->model('WithdrawModel', $this);

    }

    /**
     * showdoc
     * @catalog API文档/用户端/渠道相关
     * @title 获取用户身份
     * @description 获取用户身份信息
     * @method POST
     * @url User/Channel/getUserIdentityInfo
     * @param user_id 必选 int 用户user_id
     * @return {"code":"1000","message":"获取成功","data":{"identity":"0","identity_name":"普通用户","level":"0"}}
     * @return_param identity int 身份0普通用户1市场推广2渠道商3推广人员-1非公司用户
     * @return_param identity_name string 身份名称
     * @return_param level int 渠道推广员等级（仅当identity=3时）
     * @return_param status int 状态1正常2冻结
     * @remark
     * @number 0
     * @author tx
     * @date 2020-3-6
     */
    public function http_getUserIdentityInfo()
    {
        $user_id = $this->user_id;
        if (empty($user_id)) {
            return $this->jsonend(-1000, "缺少必要参数user_id");
        }
        $res = $this->getUserIdentity($user_id);

        return $this->jsonend(1000, "获取成功", $res);
    }

    /**
     * showdoc
     * @catalog API文档/用户端/渠道相关
     * @title 添加申请信息
     * @description 用户提交渠道申请
     * @method POST
     * @url User/Channel/addApply
     * @param user_id 必选 int 用户user_id
     * @param name 必选 string 单位名称
     * @param contact 必选 string 联系人
     * @param tellphone 必选 int 联系电话
     * @param brief 可选 string 简介
     * @param enclosure 可选 array 附件(多文件)传参格式{"1.jpg","2.jpg"}一维数组
     * @return_param 无
     * @remark 无
     * @number 0
     * @author tx
     * @date 2020-2-25
     */
    public function http_addApply()
    {
        //接收参数
        $user_id = $this->user_id;
        $name = $this->parm['name'] ?? '';
        $contact = $this->parm['contact'] ?? '';
        $tellphone = $this->parm['tellphone'] ?? '';
        $brief = $this->parm['brief'] ?? '';
        $enclosure = $this->parm['enclosure'] ?? '';

        if (empty($name)) {
            return $this->jsonend(-1000, "请输入单位名称！");
        }
        if (empty($contact)) {
            return $this->jsonend(-1000, "请输入联系人！");
        }
        if (empty($tellphone)) {
            return $this->jsonend(-1000, "请输入联系电话！");
        }
        if (!isMobile($tellphone)) {
            return $this->jsonend(-1000, "请输入正确的联系电话！");
        }
        //检测是否实名认证
        $check_auth = $this->auth_model->getOne(['user_id' => $user_id, 'status' => 2], 'status');
        if (empty($check_auth)) {
            return $this->jsonend(-1000, '您还未实名，请先实名认证！');
        }

        //判断用户身份
        $res = $this->getUserIdentity($user_id);
        if ($res['identity'] == 2) {
            return $this->jsonend(-1000, "您已经是渠道商了！");
        }
        //同一用户只能有一种身份
        if ($res['identity'] == 1 || $res['identity'] == 3) {//如果用户是市场推广或渠道或推广员
            return $this->jsonend(-1000, "很抱歉，您没有申请资格！");
        }

        //查询申请表信息
        $info = $this->channel_apply_model->getOne(['user_id' => $user_id], 'status,id,apply_time');

        $data['user_id'] = $user_id ?? '';
        $data['company_id'] = $this->company ?? '';
        $data['name'] = $name ?? '';
        $data['contact'] = $contact ?? '';
        $data['tellphone'] = $tellphone ?? '';
        $data['brief'] = $brief ?? '';
        $data['enclosure'] = $enclosure ? json_encode($enclosure) : '';//附件
        $data['apply_time'] = time();

        if (empty($info)) {
            $res = $this->channel_apply_model->add($data);
        }

        if ($res) {
            return $this->jsonend(1000, '申请成功，请耐心等待审核');
        }
        return $this->jsonend(-1000, '申请失败');
    }

    /**
     * showdoc
     * @catalog API文档/用户端/渠道相关
     * @title 编辑渠道申请信息
     * @description 编辑渠道申请信息
     * @method POST
     * @url User/Channel/editApply
     * @param apply_id 必选 int 申请id
     * @param name 必选 string 单位名称
     * @param contact 必选 string 联系人
     * @param tellphone 必选 int 联系电话
     * @param brief 可选 string 简介
     * @param enclosure 可选 array 附件（多文件）传参格式{"1.jpg","2.jpg"}一维数组
     * @return_param 无
     * @remark 无
     * @number 0
     * @author tx
     * @date 2020-2-25
     */
    public function http_editApply()
    {
        //接收参数
        $apply_id = $this->parm['apply_id'] ?? '';
        $name = $this->parm['name'] ?? '';
        $contact = $this->parm['contact'] ?? '';
        $tellphone = $this->parm['tellphone'] ?? '';
        $brief = $this->parm['brief'] ?? '';
        $enclosure = $this->parm['enclosure'] ?? '';

        if (empty($apply_id)) {
            return $this->jsonend(-1000, "缺少参数apply_id！");
        }
        if (empty($name)) {
            return $this->jsonend(-1000, "请输入单位名称！");
        }
        if (empty($contact)) {
            return $this->jsonend(-1000, "请输入联系人！");
        }
        if (empty($tellphone)) {
            return $this->jsonend(-1000, "请输入联系电话！");
        }
        if (!isMobile($tellphone)) {
            return $this->jsonend(-1000, "请输入正确的联系电话！");
        }

        $data['name'] = $name ?? '';
        $data['contact'] = $contact ?? '';
        $data['tellphone'] = $tellphone ?? '';
        $data['brief'] = $brief ?? '';
        $data['enclosure'] = $enclosure ? json_encode($enclosure) : '';//附件
        $data['status'] = 1;//附件

        $res = $this->channel_apply_model->save(['id' => $apply_id], $data);

        if ($res) {
            return $this->jsonend(1000, '申请成功，请耐心等待审核');
        }
        return $this->jsonend(-1000, '申请失败');

    }


    /**
     * showdoc
     * @catalog API文档/用户端/渠道相关
     * @title 查询渠道申请详情
     * @description 获取用户申请渠道商信息
     * @method POST
     * @url User/Channel/getApplyInfo
     * @param user_id 必选 int 用户user_id
     * @return {"code":"1000","message":"获取成功","data":{"name":"","pid":"1","contact":"","tellphone":null,"brief":"null","status":null,"apply_time":"null","check_time":"null","enclosure":["https:\/\/qn.youheone.com\/a4fd9201809171621285954.jpg"]}}
     * @return_param id int 渠道申请id
     * @return_param username string 姓名
     * @return_param telphone int 电话
     * @return_param name string 单位名称
     * @return_param pid int 推荐人id
     * @return_param contact string 联系人
     * @return_param tellphone int 联系电话
     * @return_param brief string 简介
     * @return_param enclosure string 附件
     * @return_param status int 审核状态1未审核2通过3未通过
     * @return_param apply_time string 申请时间
     * @return_param check_time string 审核时间
     * @remark
     * @number 0
     * @author tx
     * @date 2020-2-25
     */
    public function http_getApplyInfo()
    {
        $user_id = $this->user_id;
        if (empty($user_id)) {
            return $this->jsonend(-1000, '缺少参数user_id');
        }
        $join = [
            ['customer b', 'b.user_id = rq_channel_apply.user_id', 'left'], // 渠道申请表关联客户表
        ];
        $where['rq_channel_apply.user_id'] = $user_id;

        $data = $this->channel_apply_model->getOne($where, 'rq_channel_apply.*,b.realname,b.nickname,b.telphone', $join);

        if (empty($data)) {
            return $this->jsonend(-1000, '您还未申请渠道商!');
        }

        $data['username'] = empty($data['realname']) ? urldecode($data['nickname']) : $data['realname'];
        $data['apply_time'] = $data['apply_time'] ? date('Y/m/d H:i', $data['apply_time']) : '';
        $data['check_time'] = $data['check_time'] ? date('Y/m/d H:i', $data['check_time']) : '';
        $data['status_desc'] = $data['status'] == 1 ? '待审核' : ($data['status'] == 2 ? '通过' : '未通过');
        $enclosure = $data['enclosure'] ? json_decode($data['enclosure'], true) : '';
        $image = [];
        if (!empty($enclosure)) {
            foreach ($enclosure as $k => $v) {
                $image[$k]['all_url'] = $v ? UploadImgPath($v) : '';
                $image[$k]['url'] = $v ?? '';
            }
        }
        $data['enclosure'] = $image;
        return $this->jsonend(1000, '获取成功', $data);

    }

    /**
     * showdoc
     * @catalog API文档/用户端/渠道相关
     * @title 查询渠道商个人信息
     * @description 查询渠道商个人信息
     * @method POST
     * @url User/Channel/getChannelInfo
     * @param user_id 必选 int 用户user_id
     * @return {"code":"1000","message":"获取成功","data":{"username":"","channel_no":"1","tellphone":"","qrcode":"https:\/\/qn.youheone.com\/a4fd9201809171621285954.jpg","status":"null","status":null,"create_time":"null","check_time":"null"}}
     * @return_param username  string 姓名
     * @return_param name  string 单位名称
     * @return_param channel_no  string 渠道编号
     * @return_param telphone  int 联系电话
     * @return_param qrcode  string 渠道推广二维码
     * @return_param status  string 状态1正常2冻结
     * @return_param create_time  int 创建时间
     * @return_param total_money  int 总金额
     * @return_param available_money  int 可兑换金额
     * @return_param frozen_money  int 冻结金额
     * @return_param is_have_bank bool 是否有银行卡true有false没有
     * @remark
     * @number 0
     * @author tx
     * @date 2020-2-26
     */
    public function http_getChannelInfo()
    {
        $user_id = $this->parm['user_id'] ?? $this->user_id;
        if (empty($user_id)) {
            return $this->jsonend(-1000, '缺少参数user_id');
        }
        $join = [
            ['customer b', 'b.user_id = rq_channel.user_id', 'left'], // 渠道表关联客户表
            ['customer_account c', 'c.user_id = rq_channel.user_id', 'left'], // 渠道表关联用户账户表
        ];
        $where['rq_channel.user_id'] = $user_id;

        $data = $this->channel_model->getOne($where, 'rq_channel.*,b.realname,b.nickname,b.telphone,c.total_money,c.available_money,c.frozen_money', $join);

        if (empty($data)) {
            return $this->jsonend(-1000, '您还不是渠道商,请先成为渠道商');
        }
        $bank_info = $this->bank_card_model->getOne(['user_id' => $user_id,'role'=>4], 'bank_id');

        $data['username'] = empty($data['realname']) ? urldecode($data['nickname']) : $data['realname'];
        $data['create_time'] = $data['create_time'] ? date('Y/m/d H:i', $data['create_time']) : '';
        $data['status_desc'] = $data['status'] == 1 ? '正常' : '冻结';
        $data['qrcode'] = $data['qrcode'] ? UploadImgPath($data['qrcode']) : '';
        $data['total_money'] = $data['total_money'] ? formatMoney($data['total_money'], 2) : 0;
        $data['available_money'] = $data['available_money'] ? formatMoney($data['available_money'], 2) : 0;
        $data['frozen_money'] = $data['frozen_money'] ? formatMoney($data['frozen_money'], 2) : 0;
        $data['is_have_bank'] = !empty($bank_info['bank_id']) ? true : false;
        $data['recommender'] = $data['name'] ?? '';

        return $this->jsonend(1000, '获取成功', $data);

    }


    /**
     * showdoc
     * @catalog API文档/用户端/渠道相关
     * @title 查询推广员个人信息
     * @description 查询推广员个人信息
     * @method POST
     * @url User/Channel/getPromotersInfo
     * @param user_id 必选 int 用户user_id
     * @return {"code":"1000","message":"获取成功","data":{"username":"","name":"1","tellphone":"","level":"1","status":"null","status":null,"create_time":"null","total_money":"null"}}
     * @return_param name|username  string 姓名
     * @return_param recommender  string 推荐人
     * @return_param level int 推广员等级1团长2团员
     * @return_param telphone  int 联系电话
     * @return_param status  string 状态1正常2冻结
     * @return_param create_time  int 创建时间
     * @return_param total_money  int 总金额
     * @return_param available_money  int 可兑换金额
     * @return_param frozen_money  int 冻结金额
     * @remark
     * @number 0
     * @author tx
     * @date 2020-2-26
     */
    public function http_getPromotersInfo()
    {
        $user_id = $this->parm['user_id'] ?? $this->user_id;
        if (empty($user_id)) {
            return $this->jsonend(-1000, '缺少参数user_id');
        }

        $join = [
            ['customer b', 'b.user_id = rq_channel_promoters.user_id', 'left'], // 渠道表关联客户表
            ['customer_account c', 'c.user_id = rq_channel_promoters.user_id', 'left'], // 渠道表关联用户账户表
            ['customer b1', 'b1.user_id = rq_channel_promoters.pid', 'left'], // 关联客户表(推荐人)
        ];
        $where['rq_channel_promoters.user_id'] = $user_id;
        $data = $this->channel_promoters_model->getOne($where, 'rq_channel_promoters.*,b.realname,b.nickname,b1.realname as real_name,b1.nickname as nick_name,b.telphone,c.total_money,c.available_money,c.frozen_money', $join);

        if (empty($data)) {
            return $this->jsonend(-1000, '您还不是渠道推广，请先成为推广人员！');
        }

        $qrcode = '';
        //如果推广码不存在，则生成
        if (empty($data['qrcode'])) {
            $scene = 'user_id=' . $user_id;
            $result = $this->getPageQr($scene);
            if ($result['res'] == 'success') {
                $this->channel_promoters_model->save(['channel_promoter_id' => $data['channel_promoter_id']], ['qrcode' => $result['url']]);
                $qrcode = $result['all_url'];
            }
        }

        $data['username'] = empty($data['realname']) ? urldecode($data['nickname']) : $data['realname'];
        $data['recommender'] = empty($data['real_name']) ? urldecode($data['nick_name']) : $data['real_name'];
        $data['create_time'] = $data['create_time'] ? date('Y/m/d H:i', $data['create_time']) : '';
        $data['update_time'] = $data['update_time'] ? date('Y/m/d H:i', $data['update_time']) : '';
        $data['status_desc'] = $data['status'] == 1 ? '正常' : '冻结';
        $data['level_desc'] = $data['level'] == 1 ? '团长' : '团员';
        $data['total_money'] = $data['total_money'] ? formatMoney($data['total_money'], 2) : 0;
        $data['available_money'] = $data['available_money'] ? formatMoney($data['available_money'], 2) : 0;
        $data['frozen_money'] = $data['frozen_money'] ? formatMoney($data['frozen_money'], 2) : 0;
        $data['qrcode'] = $data['qrcode'] ? UploadImgPath($data['qrcode']) : $qrcode;

        if ($data['level'] == 1) {//如果是团长，推荐人显示所属渠道商
            $channel_info = $this->channel_model->getOne(['id' => $data['channel_id']], 'name');
            $data['recommender'] = $channel_info['name'];
        }


        return $this->jsonend(1000, '获取成功', $data);

    }

    //生成小程序二维码，并上传至七牛云
    public function getPageQr($scene)
    {
        $wx_config = $this->wx_config;
        $WxAuth = WxAuth::getInstance($wx_config['appId'], $wx_config['appSecret']);
        $WxAuth->setConfig($wx_config['appId'], $wx_config['appSecret']);

        //生成小程序二维码图片
        $width = '100px';
        $auto_color = true;
        $line_color = "{'r':'0','g':'0','b':'0'}";
        $page = 'pages/channel/home';
        $base = $WxAuth->getSamllPageQr($scene, $width, $auto_color, $line_color, $page);

        // 构建鉴权对象 #需要填写你的 Access Key 和 Secret Key
        $auth = get_instance()->QiniuAuth;
        $token = $auth->uploadToken($this->config->get('qiniu.bucket'));
        $uploadMgr = get_instance()->QiniuUploadManager;

        $key = date('YmdHis') . rand(0, 9999) . '_channel_' . rand(0, 9999) . '.jpg'; //上传后文件名称
        list($ret, $err) = $uploadMgr->put($token, $key, $base);
        if ($err !== null) {
            $data['res'] = 'fail';
            return $data;
        } else {
            $data['res'] = 'success';
            $data['url'] = $key;
            $data['all_url'] = $this->config->get('qiniu.qiniu_url') . $key;

            return $data;
        }
    }


    /**
     * showdoc
     * @catalog API文档/用户端/渠道相关
     * @title 添加渠道推广员
     * @description 渠道商添加渠道推广团长，团长添加团员
     * @method POST
     * @url User/Channel/addPromoter
     * @param telphone 必选 string 推广员电话
     * @param name 必选 string 推广员姓名
     * @param account 必选 string 推广员账号
     * @return_param
     * @remark
     * @number 0
     * @author tx
     * @date 2020-2-27
     */
    public function http_addPromoter()
    {
        //接收参数
        $user_id = $this->user_id;
        $name = $this->parm['name'] ?? '';
        $account = $this->parm['account'] ?? '';
        $telphone = $this->parm['telphone'] ?? '';

        if (empty($telphone)) {
            return $this->jsonend(-1001, "请输入推广员电话！");
        }
        if (!isMobile($telphone)) {
            return $this->jsonend(-1001, "请输入正确的电话！");
        }

        //先判断当前登录用户身份必须是渠道商或渠道推广团长
        $res = $this->getUserIdentity($user_id);
        if ($res['identity'] != 2 && $res['identity'] != 3) {
            return $this->jsonend(-1001, "很抱歉您不是渠道商或渠道推广团长，请先申请成为渠道商！");
        }
        if ($res['identity'] == 3 && $res['level'] != 1) {
            return $this->jsonend(-1001, "很抱歉您不是渠道推广团长！");
        }
        if ($res['status'] == 2) {
            return $this->jsonend(-1001, "您的账号已被冻结！");
        }

        //根据电话查询用户信息
        $where['telphone'] = $telphone;
        $user_info = $this->user_model->getOne($where, 'user_id,username,realname,nickname,account,telphone,source');
        if (empty($user_info)) {
            return $this->jsonend(-1001, "无用户信息！");
        }

        //先判断添加用户的身份
        $identity = $this->getUserIdentity($user_info['user_id']);
        if ($identity['identity'] == 2 || $identity['identity'] == 3) {
            return $this->jsonend(-1001, "该用户已经是渠道了！");
        }
        if ($identity['identity'] == 1) {
            return $this->jsonend(-1001, "该用户是市场推广，不能成为渠道！");
        }

        //查询渠道或推广信息
        if ($res['identity'] == 2) {//渠道商
            $channel_info = $this->channel_model->getOne(['user_id' => $user_id, 'is_deleted' => 2], 'id');
        }
        if ($res['identity'] == 3 && $res['level'] == 1) {//渠道推广团长
            $promoters_info = $this->channel_promoters_model->getOne(['user_id' => $user_id, 'is_deleted' => 2], 'channel_id');
        }

        //查询该推广员是否存在
        $info = $this->channel_promoters_model->getOne(['user_id' => $user_info['user_id'], 'is_deleted' => 2], 'channel_promoter_id,pid');

        if (!empty($info) || $info['pid']) {
            return $this->jsonend(-1000, '此用户已经是渠道推广员了');
        }

        $data['user_id'] = $user_info['user_id'] ?? '';
        $data['company_id'] = $this->company ?? '';
        $data['pid'] = $user_id;//推荐人id
        $data['channel_id'] = $res['identity'] == 2 ? $channel_info['id'] : $promoters_info['channel_id'];
        $data['name'] = $name ? $name : $user_info['realname'];
        $data['level'] = $res['identity'] == 2 ? 1 : 2;//渠道商添加团长，团长添加团员
        $data['create_time'] = time();

        if (empty($info)) {
            $result = $this->channel_promoters_model->add($data);
        }

        //修改用户所属渠道或推广信息
        if ($res['identity'] == 2) {
            $edit_data['user_belong_to_channel'] = $res['id'];
            $this->user_model->save(['user_id' => $user_info['user_id']], $edit_data);
        }
        if ($res['identity'] == 3 && $res['level'] == 1) {
            //查询团长所属渠道商
            $user_channel = $this->user_model->getOne(['user_id' => $user_id], 'user_belong_to_channel,user_belong_to_promoter');
            if (empty($user_channel)) {
                return $this->jsonend(1000, '团长所属渠道错误');
            }
            if ($user_channel['user_belong_to_promoter'] != $res['id']) {
                return $this->jsonend(1000, '团长信息错误');
            }
            $edit_data['user_belong_to_channel'] = $user_channel['user_belong_to_channel'];
            $edit_data['user_belong_to_promoter'] = $res['id'];
            $this->user_model->save(['user_id' => $user_info['user_id']], $edit_data);
        }

        if ($result) {
            return $this->jsonend(1000, '提交成功');
        }
        return $this->jsonend(-1000, '提交失败');
    }

    /**
     * showdoc
     * @catalog API文档/用户端/渠道相关
     * @title 编辑渠道推广员
     * @description 编辑渠道推广员
     * @method POST
     * @url User/Channel/editPromoter
     * @param channel_promoter_id 必选 int 渠道推广员id
     * @param name 必选 string 推广员姓名
     * @param telphone 必选 string 推广员电话
     * @param account 必选 string 推广员账号
     * @param status 必选 int 状态1正常2冻结
     * @return_param
     * @remark
     * @number 0
     * @author tx
     * @date 2020-2-27
     */
    public function http_editPromoter()
    {
        //接收参数
        $channel_promoter_id = $this->parm['channel_promoter_id'] ?? '';
        $name = $this->parm['name'] ?? '';
        $account = $this->parm['account'] ?? '';
        $telphone = $this->parm['telphone'] ?? '';
        $status = $this->parm['status'] ?? 1;

        if (empty($channel_promoter_id)) {
            return $this->jsonend(-1001, "缺少必要参数：channel_promoter_id！");
        }

        if (empty($telphone)) {
            return $this->jsonend(-1001, "请输入推广员电话！");
        }
        if (!isMobile($telphone)) {
            return $this->jsonend(-1001, "请输入正确的电话！");
        }

        //根据电话查询用户信息
        $where['telphone'] = $telphone;
        $user_info = $this->user_model->getOne($where, 'user_id,username,realname,nickname,account,telphone');
        if (empty($user_info)) {
            return $this->jsonend(-1001, "无用户信息！");
        }

        $data['user_id'] = $user_info['user_id'] ?? '';
        $data['name'] = $name ? $name : $user_info['realname'];
        $data['update_time'] = time();
        $data['status'] = $status;
        $result = $this->channel_promoters_model->save(['channel_promoter_id' => $channel_promoter_id], $data);


        if ($result) {
            return $this->jsonend(1000, '提交成功');
        }
        return $this->jsonend(-1000, '您未作任何修改');


    }

    /**
     * showdoc
     * @catalog API文档/用户端/渠道相关
     * @title 删除渠道推广员
     * @description 渠道商删除推广员
     * @method POST
     * @url User/Channel/delPromoter
     * @param channel_promoter_id 必选 int 推广员id
     * @return_param
     * @remark
     * @number 0
     * @author tx
     * @date 2020-2-28
     */
    public function http_delPromoter()
    {
        $channel_promoter_id = $this->parm['channel_promoter_id'] ?? '';
        if (empty($channel_promoter_id)) {
            return $this->jsonend(-1000, '缺少参数channel_promoter_id');
        }

        $promoters_info = $this->channel_promoters_model->getOne(['channel_promoter_id' => $channel_promoter_id], 'channel_promoter_id,is_deleted');
        if (empty($promoters_info) || $promoters_info['is_deleted'] == 1) {
            return $this->jsonend(-1000, '渠道推广员不存在或已经删除！');
        }

        $edit_data['is_deleted'] = 1;//逻辑删除
        $res = $this->channel_promoters_model->save(['channel_promoter_id' => $channel_promoter_id], $edit_data);

        if ($res) {
            return $this->jsonend(1000, '操作成功');
        }
        return $this->jsonend(-1000, '操作失败');

    }


    /**
     * showdoc
     * @catalog API文档/用户端/渠道相关
     * @title 获取推广人员列表
     * @description 获取推广人员列表
     * @method POST
     * @url User/Channel/getPromoterList
     * @param user_id 必选 int 用户id
     * @param page 可选 int 页数，默认1
     * @param pageSize 可选 int 每页条数，默认10
     * @return {"code":1000,"message":"获取列表成功","data":[{"channel_promoter_id":"1","channel_name":"","telphone":"","level":"1","status":"1","order_status":"3","create_time":"","update_time":""}]}
     * @return_param channel_promoter_id int 推广员ID
     * @return_param channel_name string 所属渠道商单位名称
     * @return_param name|username string 推广人员姓名
     * @return_param telphone int 推广人员电话
     * @return_param level int 推广员等级
     * @return_param status int 1正常2冻结
     * @return_param create_time string 创建时间
     * @return_param update_time string 更新时间
     * @return_param team_num int 团长下的团员数量（当user_id为渠道商时）
     * @remark
     * @number 0
     * @author tx
     * @date 2020-2-28
     */
    public function http_getPromoterList()
    {
        // 接收参数
        $user_id = $this->parm['user_id'] ?? $this->user_id;

        $page = $this->parm['page'] ?? 1;
        $pageSize = $this->parm['pageSize'] ?? 10;

        if (empty($user_id)) {
            return $this->jsonend(-1000, '缺少参数user_id');
        }
        //如果是渠道商获取团长
        $channel_info = $this->channel_model->getOne(['user_id' => $user_id], 'id');
        if (!empty($channel_info)) {
            $where['rq_channel_promoters.channel_id'] = $channel_info['id'];
            $where['rq_channel_promoters.level'] = 1;

        } else {//团长获取自己属下的团员
            $promoters_info = $this->channel_promoters_model->getOne(['user_id' => $user_id], 'channel_id');
            $where['rq_channel_promoters.channel_id'] = $promoters_info['channel_id'];
            $where['rq_channel_promoters.level'] = 2;
            $where['rq_channel_promoters.pid'] = $user_id;
        }

        $join = [
            ['customer b', 'b.user_id = rq_channel_promoters.user_id', 'left'], // 推广员关联客户表
            ['channel c', 'c.id = rq_channel_promoters.channel_id', 'left'],//推广员关联渠道表
        ];
        // 查询条件
        $where['rq_channel_promoters.is_deleted'] = 2;

        // 查询字段
        $field = 'rq_channel_promoters.*,b.username,b.nickname,b.realname,b.telphone,c.name as channel_name';

        // 调用模型获取列表
        $data = $this->channel_promoters_model->getAll($where, $page, $pageSize, $field, $join);

        if (empty($data)) {
            return $this->jsonend(-1000, '暂无数据');
        }

        foreach ($data as $k => $v) {
            $system_defalut_username = $this->config->get('system_defalut')['username'];
            $data[$k]['username'] = empty($v['realname']) ? (empty($v['nickname']) ? $system_defalut_username : urldecode($v['nickname'])) : $v['realname'];
            $data[$k]['create_time'] = $v['create_time'] ? date('Y/m/d H:i', $v['create_time']) : '';
            $data[$k]['update_time'] = $v['update_time'] ? date('Y/m/d H:i', $v['update_time']) : '';
            $data[$k]['status_desc'] = $v['status'] == 1 ? '正常' : '冻结';

            if (!empty($channel_info)) {
                //获取每个团长下的团员数量
                $team_num = $this->channel_promoters_model->count(['channel_id' => $channel_info['id'], 'pid' => $v['user_id']]);
            }
            $data[$k]['team_num'] = $team_num ?? '';

        }
        //获取总的数量
        $count = $this->channel_promoters_model->count($where, $join);

        $count = $count ?? 0;
        $data = [
            'data' => $data,
            'page' => [
                'current_page' => $page,
                'pageSize' => $pageSize,
                'total' => $count,
                'total_page' => ceil($count / $pageSize)
            ]
        ];

        return $this->jsonend(1000, '获取推广员列表成功', $data);


    }

    /**
     * showdoc
     * @catalog API文档/用户端/渠道相关
     * @title 获取渠道推广员详情(编辑回显)
     * @description 获取渠道推广员详情(编辑回显)
     * @method POST
     * @url User/Channel/getPromoterInfo
     * @param channel_promoter_id 必选 int 渠道推广员id
     * @return {"code":"1000","message":"获取成功","data":{"channel_promoter_id":"","channel_id":"1","channel_name":"","name":"","username":"null","telphone":null,"level":"null","account":"null","status":"null","create_time":"null","update_time":"null"}}
     * @return_param channel_promoter_id int 推广员id
     * @return_param channel_id int 所属渠道channel_id
     * @return_param channel_name string 所属渠道单位名称
     * @return_param name|username|realname string 推广员姓名
     * @return_param telphone int 联系电话
     * @return_param level int 渠道等级1团长2团员
     * @return_param account int 推广员账号
     * @return_param status int 状态1正常2冻结
     * @return_param create_time string 创建时间
     * @return_param update_time string 更新时间
     * @remark
     * @number 0
     * @author tx
     * @date 2020-2-28
     */
    public function http_getPromoterInfo()
    {
        //接收参数
        $channel_promoter_id = $this->parm['channel_promoter_id'] ?? '';

        if (empty($channel_promoter_id)) {
            return $this->jsonend(-1000, '缺少参数channel_promoter_id');
        }
        $join = [
            ['customer b', 'b.user_id = rq_channel_promoters.user_id', 'left'], // 推广员关联客户表
        ];

        $where['rq_channel_promoters.channel_promoter_id'] = $channel_promoter_id;

        $data = $this->channel_promoters_model->getOne($where, 'rq_channel_promoters.*,b.realname,b.nickname,b.telphone,b.account', $join);

        if (empty($data)) {
            return $this->jsonend(-1000, '暂无信息');
        }

        $data['username'] = empty($data['realname']) ? urldecode($data['nickname']) : $data['realname'];
        $data['create_time'] = $data['create_time'] ? date('Y/m/d H:i', $data['create_time']) : '';
        $data['update_time'] = $data['update_time'] ? date('Y/m/d H:i', $data['update_time']) : '';
        $data['status_desc'] = $data['status'] == 1 ? '正常' : '冻结';


        return $this->jsonend(1000, '获取成功', $data);

    }

    /*********************************************************/

    /**
     * showdoc
     * @catalog API文档/用户端/渠道相关
     * @title 获取渠道商/推广员账户明细列表
     * @description 获取渠道商/推广员账户明细列表
     * @method POST
     * @url User/Channel/getChannelAccountList
     * @param user_id 必选 int 用户id
     * @param page 可选 int 页数，默认1
     * @param pageSize 可选 int 每页条数，默认10
     * @return {"code":1000,"message":"获取推广人员列表成功","data":[{"channel_promoter_id":"1","channel_name":"","telphone":"","level":"1","status":"1","order_status":"3","create_time":"","update_time":""}]}
     * @return_param id int 账户表ID
     * @return_param username string 姓名
     * @return_param money string 变动金额
     * @return_param change_type string 变动类型1增加2减少
     * @return_param account_type string 变动账户类型1可用金额2冻结金额
     * @return_param source string 变动类型1渠道业绩结算2渠道推广员业绩结算3分成给渠道推广员4兑换
     * @return_param order_number int 工单号（业绩结算的工单source=1,2,3时 ）
     * @return_param remarks string 备注
     * @return_param telphone int 电话
     * @return_param create_time string 创建时间
     * @remark
     * @number 0
     * @author tx
     * @date 2020-3-2
     */
    public function http_getChannelAccountList()
    {
        // 接收参数
        $user_id = $this->user_id;
        $page = $this->parm['page'] ?? 1;
        $pageSize = $this->parm['pageSize'] ?? 10;

        if (empty($user_id)) {
            return $this->jsonend(-1000, '缺少参数user_id');
        }

        $join = [
            ['customer b', 'b.user_id = rq_customer_account_bill.user_id', 'left'], // 用户账户账单明细表关联客户表
        ];
        // 查询条件
        $where['rq_customer_account_bill.user_id'] = $user_id;
        $where['b.user_belong_to_company'] = $this->company;


        // 查询字段
        $field = 'rq_customer_account_bill.*,b.username,b.nickname,b.realname,b.telphone';

        // 调用模型获取订单列表
        $data = $this->customer_account_bill_model->getAll($where, $field, $page, $pageSize, $join);

        if (empty($data)) {
            return $this->jsonend(-1000, '暂无数据', $data);
        }

        foreach ($data as $k => $v) {
            $data[$k]['username'] = empty($v['realname']) ? urldecode($v['nickname']) : $v['realname'];
            $data[$k]['create_time'] = $v['create_time'] ? date('Y/m/d H:i', $v['create_time']) : '';
            $data[$k]['money'] = $v['money'] ? formatMoney($v['money'], 2) : 0;
            $data[$k]['change_type'] = $v['change_type'] == 1 ? '增加' : '减少';
            $data[$k]['account_type'] = $v['account_type'] == 1 ? '可用金额' : '冻结金额';
            if ($v['source'] != 4) {
                $work_order = $this->work_order_model->getOne(['work_order_id' => $v['work_order_id']], 'order_number');
                $data[$k]['order_number'] = $work_order['order_number'] ?? '';
            }
            $data[$k]['source'] = $v['source'] ? $this->config->get('user_account_bill_source')[$v['source']] : '';


        }

        $count = $this->customer_account_bill_model->count($where, $join);

        $count = $count ?? 0;
        $data = [
            'data' => $data,
            'page' => [
                'current_page' => $page,
                'pageSize' => $pageSize,
                'total' => $count,
                'total_page' => ceil($count / $pageSize)
            ]
        ];

        return $this->jsonend(1000, '获取列表成功', $data);


    }


    /**
     * showdoc
     * @catalog API文档/用户端/渠道相关
     * @title 获取渠道商/渠道推广员分成比例列表
     * @description 获取渠道商/渠道推广员分成比例列表
     * @method POST
     * @url User/Channel/getChannelCommissionList
     * @param user_id 必选 int 用户id
     * @param page 可选 int 页数，默认1
     * @param pageSize 可选 int 每页条数，默认10
     * @return {"code":1000,"message":"获取列表成功","data":[{"channel_promoter_id":"1","channel_name":"","telphone":"","level":"1","status":"1","order_status":"3","create_time":"","update_time":""}]}
     * @return_param id int 分成配置ID
     * @return_param equipments_name string 产品名称
     * @return_param equipments_type int 1租赁2购买
     * @return_param money string 佣金
     * @return_param username string 用户名称
     * @return_param proportion int 分成比例
     * @return_param remarks string 备注
     * @return_param telphone int 电话
     * @return_param create_time string 创建时间
     * @remark
     * @number 0
     * @author tx
     * @date 2020-3-2
     */
    public function http_getChannelCommissionList()
    {
        // 接收参数
        $user_id = $this->user_id;
        $page = $this->parm['page'] ?? 1;
        $pageSize = $this->parm['pageSize'] ?? 10;

        if (empty($user_id)) {
            return $this->jsonend(-1000, '缺少参数user_id');
        }
        //判断用户身份
        $res = $this->getUserIdentity($user_id);
        // 查询条件


        $data = [];
        if ($res['identity'] == 2) {//渠道商
            $where['rq_channel_commission_config.channel_id'] = $res['id'];
            $where['rq_channel_commission_config.is_deleted'] = 2;
            $join = [
                ['channel c', 'c.id = rq_channel_commission_config.channel_id', 'left'], // 渠道分成表关联渠道表
                ['customer b', 'b.user_id = c.user_id', 'left'], // 渠道表表关联客户表
                ['equipments e', 'e.equipments_id = rq_channel_commission_config.equipments_id', 'left'], // 渠道分成表关联产品表
            ];
            // 查询字段
            $field = 'rq_channel_commission_config.*,c.name,c.user_id,e.equipments_name,e.equipments_money,e.equipments_purchase_money,e.equipments_type,b.username,b.nickname,b.realname,b.telphone';
            $data = $this->channel_commission_config_model->getAll($where, $page, $pageSize, $field, $join);
            if (empty($data)) {
                $where['rq_channel_commission_config.channel_id'] = -1;
                $data = $this->channel_commission_config_model->getAll($where, $page, $pageSize, $field, $join);
            }
        } else if ($res['identity'] == 3) {//推广员
            $where['rq_channel_promoters_commission_config.is_deleted'] = 2;
            $where['rq_channel_promoters_commission_config.channel_promoter_id'] = $res['id'];
            $join = [
                ['channel_promoters c', 'c.channel_promoter_id = rq_channel_promoters_commission_config.channel_promoter_id', 'left'], // 渠道分成表关联渠道表
                ['customer b', 'b.user_id = c.user_id', 'left'], // 渠道表表关联客户表
                ['equipments e', 'e.equipments_id = rq_channel_promoters_commission_config.equipments_id', 'left'], // 渠道分成表关联产品表
            ];
            // 查询字段
            $field = 'rq_channel_promoters_commission_config.*,c.name,c.user_id,e.equipments_name,e.equipments_money,e.equipments_purchase_money,e.equipments_type,b.username,b.nickname,b.realname,b.telphone';


            $data = $this->channel_promoters_commission_config_model->getAll($where, $page, $pageSize, $field, $join, []);
            if (empty($data)) {
                $where['rq_channel_promoters_commission_config.channel_promoter_id'] = -1;
                $data = $this->channel_promoters_commission_config_model->getAll($where, $page, $pageSize, $field, $join, []);
            }
        }


        if (empty($data)) {
            return $this->jsonend(-1000, '暂无数据', $data);
        }

        foreach ($data as $k => $v) {
            $data[$k]['username'] = empty($v['realname']) ? urldecode($v['nickname']) : $v['realname'];
            $data[$k]['create_time'] = $v['create_time'] ? date('Y/m/d H:i', $v['create_time']) : '';
            $data[$k]['money'] = $v['money'] ? formatMoney($v['money'], 2) : 0;

        }
        if ($res['identity'] == 2) {
            $count = $this->channel_commission_config_model->count($where, $join);
        } else if ($res['identity'] == 3) {
            $count = $this->channel_promoters_commission_config_model->count($where, $join);
        }

        $count = $count ?? 0;
        $data = [
            'data' => $data,
            'page' => [
                'current_page' => $page,
                'pageSize' => $pageSize,
                'total' => $count,
                'total_page' => ceil($count / $pageSize)
            ]
        ];


        return $this->jsonend(1000, '获取成功', $data);


    }


    /**
     * showdoc
     * @catalog API文档/用户端/渠道相关
     * @title 渠道推广员分成
     * @description 设置渠道推广员分成(新增、编辑、删除)
     * @method POST
     * @url User/Channel/setPromoteCommission
     * @param user_id 必选 int 用户(渠道用户)id
     * @param channel_promoter_id 必选 int 渠道推广员id
     * @param equipments_id 必选 int 产品id
     * @param money 必选 int 设置分成金额
     * @param value 必选 二维数组传参格式[{"equipments_id":1,"money":2},{"equipments_id":1,"money":2}]
     * @remark
     * @number 0
     * @author tx
     * @date 2020-3-4
     */
    public function http_setPromoteCommission()
    {
        // 接收参数
        $user_id = $this->user_id;
        $channel_promoter_id = $this->parm['channel_promoter_id'] ?? '';
        $value = $this->parm['value'] ?? '';//二维数组

        //判断当前登录用户身份信息及状态
        $res = $this->getUserIdentity($user_id);

        if ($res['identity'] != 2 && $res['identity'] != 3) {//渠道商和团长都可以设置分成
            return $this->jsonend(-1000, '您还不是渠道商或推广员！');
        }
        if ($res['identity'] == 3 && $res['level'] != 1) {
            return $this->jsonend(-1000, '您还不是团长！');
        }
        if (empty($channel_promoter_id)) {
            return $this->jsonend(-1000, '缺少参数channel_promoter_id');
        }
        if (empty($value)) {
            return $this->jsonend(-1000, '产品或金额必须!');
        }
        if ($res['status'] == 2) {
            return $this->jsonend(-1000, '您的账号已被冻结！');
        }

        //判断该推广员的状态
        $status = $this->channel_promoters_model->getOne(['channel_promoter_id' => $channel_promoter_id], 'status');
        if ($status['status'] == 2) {
            return $this->jsonend(-1000, '该推广员的账号已经被冻结！');
        }

        //先判断是否有删除
        $data = $this->channel_promoters_commission_config_model->getAll(['channel_promoter_id' => $channel_promoter_id], 1, -1, 'equipments_id');
        foreach ($data as $k => $v) {
            $result = deep_in_array($v['equipments_id'], $value);
            if ($result == false) {
                $this->channel_promoters_commission_config_model->delete(['equipments_id' => $v['equipments_id'], 'channel_promoter_id' => $channel_promoter_id]);
            }
        }

        $add_status = false;
        $this->db->begin(function () use ($user_id, &$channel_promoter_id, &$value, &$add_status, &$res) {
            foreach ($value as $k => $v) {
                $equipments_id[] = $v['equipments_id'];
                $arr = array_count_values($equipments_id);

                if ($arr[$v['equipments_id']] >= 2) {
                    return $this->jsonend(-1000, '请勿重复分成同一产品！');
                }

                if (empty($v['equipments_id']) || empty($v['money'])) {
                    return $this->jsonend(-1000, '产品或金额不能为空!');
                }

                $where['equipments_id'] = $v['equipments_id'];
                $equipments_info = $this->equipment_model->getOne(['equipments_id' => $v['equipments_id']], 'equipments_name');

                //查询信息
                $info = $this->channel_promoters_commission_config_model->getOne(['equipments_id' => $v['equipments_id'], 'channel_promoter_id' => $channel_promoter_id]);

                if ($res['identity'] == 2) {//渠道商给团长分成
                    //查询渠道商的分成
                    $channel_info = $this->channel_model->getOne(['user_id' => $user_id], 'id');
                    $where['channel_id'] = $channel_info['id'];
                    //查询该渠道商所得分成如700
                    $commission = $this->channel_commission_config_model->getOne($where, 'id,channel_id,equipments_id,money');
                    if (empty($commission)) {
                        $where['channel_id'] = -1;
                        $commission = $this->channel_commission_config_model->getOne($where, 'id,channel_id,equipments_id,money');
                    }

                }

                if ($res['identity'] == 3 && $res['level'] == 1) {//团长给团员分成
                    $promoters_info = $this->channel_promoters_model->getOne(['user_id' => $user_id], 'channel_promoter_id,user_id');

                    $where['channel_promoter_id'] = $promoters_info['channel_promoter_id'];
                    //查询该团长分成
                    $commission = $this->channel_promoters_commission_config_model->getOne($where, 'id,channel_promoter_id,equipments_id,money');

                }

                $commission['money'] = $commission['money'] ? formatMoney($commission['money'], 2) : 0;

                if (empty($commission['money'])) {
                    return $this->jsonend(-1000, $equipments_info['equipments_name'] . '没有可分成金额！');

                }

                if ($v['money'] > $commission['money']) {
                    return $this->jsonend(-1000, $equipments_info['equipments_name'] . '的分成金额最多不能超过' . $commission['money'] . '元');
                }

                $wheres['equipments_id'] = $v['equipments_id'];
                $wheres['channel_promoter_id'] = $channel_promoter_id;

                //组装数据
                $add_data['channel_promoter_id'] = $channel_promoter_id;
                $add_data['equipments_id'] = $v['equipments_id'];
                $add_data['money'] = $v['money'] ? formatMoney($v['money'], 1) : 0;

                if (empty($info)) {
                    $add_data['create_time'] = time();
                    $this->channel_promoters_commission_config_model->add($add_data);
                } else {
                    $this->channel_promoters_commission_config_model->save($wheres, $add_data);
                }

                $add_status = true;
            }

        });

        if ($add_status) {
            return $this->jsonend(1000, '操作成功');
        }
        return $this->jsonend(-1000, '操作失败');


    }


    /**
     * showdoc
     * @catalog API文档/用户端/渠道相关
     * @title 获取渠道推广员分成详情列表（编辑回显）
     * @description 获取渠道推广员分成详情列表（编辑回显）
     * @method POST
     * @url User/Channel/getPromotersCommissionList
     * @param channel_promoter_id 必选 int 渠道推广id
     * @return {"code":1000,"message":"获取列表成功","data":[{"channel_promoter_id":"1","equipments_id":"","equipments_name":"","money":"1","create_time":"1"}]}
     * @return_param channel_promoter_id int 渠道推广id
     * @return_param equipments_id id 产品id
     * @return_param equipments_name string 产品名称
     * @return_param money int 分成金额单位元
     * @return_param create_time string 创建时间
     * @remark
     * @number 0
     * @author tx
     * @date 2020-3-2
     */
    public function http_getPromotersCommissionList()
    {
        $channel_promoter_id = $this->parm['channel_promoter_id'] ?? '';

        if (empty($channel_promoter_id)) {
            return $this->jsonend(-1000, '缺少必要参数channel_promoter_id');
        }

        $join = [
            ['equipments e', 'e.equipments_id = rq_channel_promoters_commission_config.equipments_id', 'left'], // 渠道推广分成表关联产品表
        ];

        $where['channel_promoter_id'] = $channel_promoter_id;

        $data = $this->channel_promoters_commission_config_model->getAll($where, 1, -1, 'rq_channel_promoters_commission_config.*,e.equipments_name', $join);

        if (empty($data)) {
            return $this->jsonend(-1000, '暂无分成信息');
        }

        foreach ($data as $k => $v) {
            $data[$k]['create_time'] = $v['create_time'] ? date('Y/m/d H:i', $v['create_time']) : '';
            $data[$k]['money'] = $v['money'] ? formatMoney($v['money'], 2) : 0;

        }

        return $this->jsonend(1000, '获取分成信息列表成功', $data);
    }


    /**
     * 获取用户身份信息(条件：一个用户只有一种身份的情况下)
     * @param 必选 $user_id int 用户ID
     * @return $identity int （0普通用户1市场推广2渠道3渠道推广员）
     */
    public function getUserIdentity($user_id)
    {
        if (empty($user_id)) {
            return false;
        }

        $where['user_id'] = $user_id;
        $where['is_deleted'] = 2;

        $res = [];
        //查询是否是渠道商
        $channel_info = $this->channel_model->getOne($where, 'id,status');

        if (!empty($channel_info)) {

            $res = [
                'id' => $channel_info['id'],
                'identity' => 2,
                'identity_name' => '渠道商',
                'status' => $channel_info['status']
            ];

            return $res;
        }

        //查询是否是推广
        $promote_info = $this->channel_promoters_model->getOne($where, 'channel_promoter_id,level,status');
        if (!empty($promote_info)) {
            $res = [
                'id' => $promote_info['channel_promoter_id'],
                'identity' => 3,//渠道推广员
                'level' => $promote_info['level'],//渠道推广员等级
                'identity_name' => $promote_info['level'] == 1 ? '团长' : '团员',
                'status' => $promote_info['status']
            ];

            return $res;
        }

        unset($where);
        $where['user_id'] = $user_id;
        $where['is_delete'] = 0;
        $where['position'] = ['NOT IN', [1, 2]];//业务和直推人

        //查询是否是市场推广
        $oa_workers_info = $this->oa_workers_model->getOne($where, 'workers_id,workers_status,position');
        if (!empty($oa_workers_info)) {

            $res = [
                'identity' => 1,
                'identity_name' => '市场推广',
                'status' => $oa_workers_info['workers_status']
            ];

            return $res;
        }

        unset($where);
        $where['user_id'] = $user_id;
        $where['is_delete'] = 0;
        //查询是否是普通用户
        $user_info = $this->user_model->getOne($where, 'user_id,account_status');
        if (empty($user_info)) {
            $res = [
                'identity' => -1,
                'identity_name' => '非公司用户',
            ];

            return $res;

        }

        $res = [
            'identity' => 0,
            'identity_name' => '普通用户',
            'status' => $user_info['account_status']
        ];

        return $res;

    }






}