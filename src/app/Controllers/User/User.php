<?php

namespace app\Controllers\User;

use app\Services\Common\ConfigService;
use app\Services\Common\DataService;
use app\Services\Common\ReturnCodeService;
use Firebase\JWT\JWT;
use Server\Components\CatCache\CatCacheRpcProxy;
use app\Wechat\WxAuth;
use app\Library\Alipay\aop\request\AlipaySystemOauthTokenRequest;
use app\Library\Alipay\aop\AopClient;
use app\Library\Alipay\aop\request\ZhimaCreditScoreGetRequest;
use app\Library\Alipay\aop\request\AlipayUserCertifyOpenInitializeRequest;
use app\Services\Common\HttpService;
use app\Services\Common\CommonService;
use Server\CoreBase\SwooleException;
use app\Models\OaWorkersModel;
use app\Models\ChannelModel;
use app\Models\ChannelPromotersModel;
use app\Services\User\UserService;

/**
 * 用户端/用户相关APISU
 */
class User extends Base
{

    protected $user_model;
    protected $invite_model;
    protected $auth_model;
    protected $bill_model;
    protected $sxConfig;
    protected $bind_equipment_model;
    protected $Achievement;
    protected $order_model;
    protected $work_model;
    protected $customer_administrative_center_model;
    protected $oa_workers_model;
    protected $channel_model;
    protected $channel_promoters_model;
    protected $coupon_model;
    protected $coupon_cate_model;
    protected $userService;


    public function initialization($controller_name, $method_name)
    {
        parent::initialization($controller_name, $method_name);
        $this->user_model = $this->loader->model('CustomerModel', $this);
        $this->invite_model = $this->loader->model('InvitationModel', $this);
        $this->auth_model = $this->loader->model('CustomerAuthModel', $this);
        $this->bill_model = $this->loader->model('CustomerBillModel', $this);
        $this->bind_equipment_model = $this->loader->model('CustomerBindEquipmentModel', $this);
        $this->Achievement = $this->loader->model('AchievementModel', $this);
        $this->order_model = $this->loader->model('OrderModel', $this);
        $this->work_model = $this->loader->model('WorkOrderModel', $this);
        $this->customer_administrative_center_model = $this->loader->model('CustomerAdministrativeCenterModel', $this);
        $this->oa_workers_model = $this->loader->model('OaWorkersModel', $this);
        $this->channel_model = $this->loader->model('ChannelModel', $this);
        $this->channel_promoters_model = $this->loader->model('ChannelPromotersModel', $this);

        $this->coupon_model = $this->loader->model('CouponModel', $this);
        $this->coupon_cate_model = $this->loader->model('CouponCateModel', $this);

        $this->userService = new UserService();


        //支付宝沙箱配置
        $this->sxConfig = [
            'appId' => '2017052600970099',
            'gatewayUrl' => 'https://openapi.alipaydev.com/gateway.do',
            'rsaPrivateKey' => 'MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCxWCZBfctKumKibG2jYblEbWzfeoW7JtvPwo5MbWG1u0SVLP+h6hVOo6INR82rDuAbRzJtlA8qXqfznGqTpdHqalhZLnfTofmBZ0Bunr471lXG1QP5vD26F0d62ElfzxdnRJ5KBkIYcaRHIY/EN8sUkn0p0GeXgShi9ELnD6hXJ001dK3tmXqVnjChCKc129OrHGH7A5bhN3TIp0aCMOLlh8it0dAUsbU2wv4pk8L+A004gbVrUmvBzM6cm4BTsvXQqG6Q41cq3G1b4xBYsvL9yaTwyLJ+vw7OP09zu5qM9aX3K3LGfz+Z9ktE84Y0GAbawlhvDdNvyyisnCRxf6vVAgMBAAECggEBAKip1lBpDAP5Ob+R8b/2PBZJH8d3JmK09U8lkOI9ETWecf6SOllKYJNW6tNIm+D5F8i5sMsoijfYqFD3TSE5DmisVXQTzqC3Jq2cfln2naQZF6EvjEVicy5d33yE4Vu+gFqVlxl91DSdOAjycWHDUmAxZdpzu2IK01cxScOpT4f6YbFyKV481/NoAJ/XBZ+S44ZC72QkONljX2HeHECU9j+FNrg+o3QlttP3Ci+/tCIj1x3cIPVxGIURi3wAkcurlyTlKHVEiaIrWXGtSHNYWShMcMsGWwcxITyZ7fYr++dwiOZ1SSwHaaHmuz2najkiUMlblTs28M8kOHPMz3usLmECgYEA48F2pbyVZdO57TkLi/UNLoi35C+KiGMKY7Z/9Zsc1aOQzwwXvQ5TxAOQ2q+dw35iVxVDQuCJerZjRmSeNCXSro0sqTx0yfdWr5Dwb7rUvtsk9nmPSG+9m7olgaJxSSkox8t2uzuttyx0ufX1szg7WCovKs/5AVfyG7mjrOBOEYkCgYEAx1ZH+bD3xE3Fo736sZAysFFXKjIfXvrau+PSaSUTgZfbhXgOtZPwSpdZ9Bms5/gFLEOalfkRcVRQwDkGNP4sTMxUzuCco6gfc8SWNsuUCHkFJ/GNujm8mR/9lb7AHgbwHFRmUL/HZtmMs6XIb0dEyfJb6KdMmMv+yh/P871B8O0CgYBavPMGKNHklavXZQjCo8iyp2mMEZzBpaO4AjeMpFWij93bf1fdzF13FGybk6fpAOsxApmfhVGAgDWhD+kvoXoEbN5ENn/ZDx+MJtSDzw1/VFOvltKM62fBKTJUKWG04h8LKnaNNcOJ3L5McD+JPXf3MidEiUMNU/pzLUyWTW8M8QKBgQC0++qnkvyyV+Z9L61g8EqFeaTJY+7Fvr9AXq4Y1xfpzuX6PbIlPKesIdUDYxhxGOghc1P0nMowelxC2sN+89Nm+xJ5LsgpcyjzZGb6Y0YJl0+OH6wNjARnhvsnraQi3yu8nQi0ghtaj2VHatsb5bpYHNtD1joedQDlpZKae5hWVQKBgCJG2nvSsyzm/iYYcGro4liHqIYZfBfCs4HtiGhJc0ltK7h+gemCfM25jlBHHdQmLN9T9wTwAD4/dq9mglsrfdygLXPO+uEa1SE1YeBE3h/i/deVhpUjRhUF2cFJPX+ZOqSwAGlVztxQeme9YzCeffZN8F+0Qj9I2jsni1ap9kw/',
            'alipayrsaPublicKey' => 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtVex9AygCEror+HFUebTMXRfKEDQp1XBBcTJrbBNJpxJ6XjCByz0dyIRWr3zM2c5ayoUXbCtqB7PdXr5+VAsNgVVSgp1P+R/cJhtdqI48Hod7atuWb/zV9fViTyknuJfBEiWSZG9uefZkKT1FiU6fipRnSJElkG8FIGpo5X4zSbHOds3rR1NSw2oe30l+TBJy5rAfSMnZZ1CU+/Pf11qT8wmiO/zqWs9XpOdKGeiTnaR/zqRVc0EiWFnQiD1DglS9QkQeO3YYmCD42l5mwDX2geimL+wuD8/pfI2e5UD7A9nvQfXUXxabz7i9KO03Z4drurJhKLOjAjKP8b/ZEGOcQIDAQAB'
        ];
    }

    /**
     * showdoc
     * @catalog API文档/用户端/用户相关
     * @title 用户注册
     * @description
     * @method POST
     * @url User/User/register
     * @param openid 必选 string 用户openid
     * @param pid 可选 int 推荐人ID
     * @return
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_register()
    {
        //接收参数
        if (empty($this->parm['code'] ?? '')) {
            return $this->jsonend(-1001, '缺少参数code');
        }
        // 注册方式 1微信，2百度
        $lgoin_type = $this->parm['login_type'] ?? 1;
        if ($lgoin_type == 1) { // 微信
            //根据code获取openid
            $wx_config = $this->wx_config;
            //$WxAuth = new WxAuth($wx_config['appId'], $wx_config['appSecret']);
            $WxAuth = WxAuth::getInstance($wx_config['appId'], $wx_config['appSecret']);
            $WxAuth->setConfig($wx_config['appId'], $wx_config['appSecret']);
            $json = $WxAuth->getSmallOAuth($this->parm['code']);
        } elseif ($lgoin_type == 2) {
            $url = "https://spapi.baidu.com/oauth/jscode2sessionkey";
            $data['code'] = $this->parm['code'];
            $data['client_id'] = $this->baidu['AppKey'];
            $data['sk'] = $this->baidu['AppSecret'];
            // post请求
            $json = HttpService::post($url, $data);
        }
        if (empty($json['openid'] ?? '')) {
            return $this->jsonend(-1101, 'code无效');
        }
        CatCacheRpcProxy::getRpc()->offsetSet('session_key_' . $json['openid'], $json['session_key']);
        CatCacheRpcProxy::getRpc()->offsetSet('openid_' . $json['openid'], $json['openid']);
        $where['openid'] = $json['openid'];
        if ($this->role == 6) {
            //            $where['spread_openid'] = $json['openid'];
        }
        $where['user_belong_to_company'] = $this->request->company;
        $user_info = $this->user_model->getOne($where, 'username,user_id,telphone,user_id,source_is_valid,source');
        if (!empty($user_info)) {
            $add_status = false;
            $this->db->begin(function () use ($json, &$add_status, $user_info, $where) {
                if (!empty($user_info)) {
                    //修改用户登录信息
                    $user_data['last_login_time'] = time();
                    $user_data['last_login_ip'] = $this->request->server['remote_addr'];
                    $user_data['last_login_device'] = $this->parm['clinet_device_system_version'] ?? '';
                    $this->user_model->save($where, $user_data);
                    //客户端信息
                    if ($this->role == 6) {
                        $client_info['terminal_type'] = 5; //推广端操作
                    } else {
                        $client_info['terminal_type'] = 3; //用户端操作
                    }
                    $client_info['client_device_brand'] = $this->parm['client_device_brand'] ?? ''; //品牌
                    $client_info['client_device_model'] = $this->parm['client_device_model'] ?? ''; //型号
                    $client_info['clinet_device_wx_version'] = $this->parm['clinet_device_wx_version'] ?? ''; //微信版本
                    $client_info['clinet_device_system_version'] = $this->parm['clinet_device_system_version'] ?? ''; //操作系统版本
                    $client_info['clinet_device_base_version'] = $this->parm['clinet_device_base_version'] ?? ''; //客户端基础库版本
                    $client_info['client_device_platform'] = $this->parm['client_device_platform'] ?? ''; //客户端平台
                    $this->addUserLoginLog($user_info['user_id'], $client_info);
                }
                //大数据平台
                $tel = substr($user_info['telphone'], 0, 3) . '****' . substr($user_info['telphone'], 7, 4);
                DataService::sendToUids(['command' => 'A1', 'data' => ['type' => 3, 'user_tel' => $tel]]); //上线了

                $add_status = true;
            }, function ($e) {
                return $this->jsonend(-1000, '注册失败', $e->error);
            });
            if (!$add_status) {
                return $this->jsonend(-1000, '注册失败');
            }
        }
        return $this->jsonend(1000, '注册成功', array('openid' => $json['openid']));
    }

    /**
     * @desc   用户注册，新增用户信息
     * @param openid 用户openid  /Y
     * @param pid 邀请人ID  /N
     * @date   2018-07-18
     * @return [type]     [description]
     * @author lcx
     */
    public function http_register2()
    {
        if (empty($this->parm['code'] ?? '')) {
            return $this->jsonend(-1001, '缺少参数code');
        }
        //根据code获取openid
        $wx_config = $this->wx_config;
        //$WxAuth = new WxAuth($wx_config['appId'], $wx_config['appSecret']);

        $WxAuth = WxAuth::getInstance($wx_config['appId'], $wx_config['appSecret']);
        $WxAuth->setConfig($wx_config['appId'], $wx_config['appSecret']);

        $json = $WxAuth->getSmallOAuth($this->parm['code']);
        if (empty($json['openid'] ?? '')) {
            return $this->jsonend(-1101, 'code无效');
        }
        CatCacheRpcProxy::getRpc()->offsetSet('session_key_' . $json['openid'], $json['session_key']);
        CatCacheRpcProxy::getRpc()->offsetSet('openid_' . $json['openid'], $json['openid']);
        //判断用户是否已经存在
        $map['openid'] = $json['openid'];
        $field = 'user_id,openid,source_is_valid,telphone,username';
        $user_info = $this->user_model->getOne($map, $field);
        if (!empty($user_info)) {
            //用户已经存在
            if ($user_info['source_is_valid'] == 1 && !empty($this->parm['pid'] ?? '')) {
                $pidIsVaild = $this->pidIsVaild($user_info['user_id'], $this->parm['pid']);
                if ($pidIsVaild) {
                    $map['openid'] = $json['openid'];
                    $data['source'] = $this->parm['pid'];
                    $save_status = false;
                    //开启事务
                    $this->db->begin(function () use ($map, $data, $user_info, $json, &$save_status) {
                        $this->user_model->save($map, $data);
                        $this->addInviteRecord($user_info['user_id'], $this->parm['pid'], $json['openid']); //添加邀请好友记录
                        $save_status = true;
                    });
                    if ($save_status) {
                        return $this->jsonend(1000, '用户已注册过，更新推荐人成功', array('openid' => $json['openid'], 'user_id' => $user_info['user_id']));
                    }
                }
                return $this->jsonend(-1000, '用户已注册过，更新推荐人失败');
            }
            return $this->jsonend(-1100, '该用户已经存在过啦!!', array('openid' => $json['openid'], 'user_id' => $user_info['user_id']));
        } else {
            //添加用户信息
            $user_data['openid'] = $json['openid'];
            if (!empty($this->parm['pid'] ?? '')) {
                $user_data['source'] = $this->parm['pid'];
                $user_data['source_is_valid'] = 1;
            }
            $user_data['add_time'] = time();
            $add_status = false;
            $user_id = '';
            $token = '';
            //开启事务
            $this->db->begin(function () use ($user_data, &$add_status, &$user_id, $json, &$token) {

                $user_id = $this->user_model->add($user_data);
                if (!empty($this->parm['pid'] ?? '')) {
                    $this->addInviteRecord($user_id, $this->parm['pid'], $json['openid']); //添加邀请记录
                }
                $add_status = true;
            });
            if ($add_status) {
                $json['user_id'] = $user_id;
                return $this->jsonend(1000, '注册成功', array('openid' => $json['openid'], 'user_id' => $user_id));
            }
            return $this->jsonend(-1000, '注册失败2');
        }
    }

    /**
     * 判断推荐人是否合法,用户或者推荐人必须是正常状态，用户与推荐人不能为同一人
     * @param type $user_id 用户ID
     * @param type $pid 推荐人ID
     * @return boolean
     */
    public function pidIsVaild($user_id, $pid)
    {
        $user_info = $this->user_model->getOne(['user_id' => $user_id], 'telphone,is_delete');
        if ($user_info['is_delete'] == 1) {
            return false;
        }
        //推荐人信息
        $pid_info = $this->user_model->getOne(['user_id' => $pid], 'telphone,is_delete');
        if ($pid_info['is_delete'] == 1) {
            return false;
        }
        if ($user_info['telphone'] == $pid_info['telphone']) {
            return false;
        }
        return true;
    }


    /**
     * showdoc
     * @catalog API文档/用户端/用户相关
     * @title 市场推广手机号登陆
     * @description
     * @method POST
     * @url User/User/marketingLand
     * @param bind_type 必选 int 绑定类型1验证码绑定，2一键授权绑定
     * @param tel 必选 string 电话
     * @param code 可选 string 如果bind_type=1必传
     * @return
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_marketingLand()
    {
        if (empty($this->parm['tel'] ?? '')) return $this->jsonend(-1001, '缺少参数手机号');
        if (!isMobile($this->parm['tel'])) return $this->jsonend(-1002, "手机号不合法");
        // 绑定类型 1验证码绑定，2一键授权绑定
        $bind_type = $this->parm['bind_type'] ?? 1;
        if ($bind_type == 1 && $this->parm['openid']!='olfDP4o7-9k2ugz0dPhoQRtdyiYw') {
            if (empty($this->parm['code'] ?? '')) {
                return $this->jsonend(-1001, '缺少参数验证码');
            }
            if ($this->parm['tel'] != 18883880448) {
                $code = CatCacheRpcProxy::getRpc()->offsetGet($this->parm['tel']);
                if (empty($code ?? '')) {
                    return $this->jsonend(-1103, "验证码不正确,请重新输入");
                }
                if ($code['code'] != $this->parm['code']) {
                    return $this->jsonend(-1103, "验证码不正确,请重新输入");
                } else if ($code['expire'] < time()) {
                    return $this->jsonend(-1104, "验证码已失效,请重新发送");
                }
            }
        }
        // 判断用户是否已经绑定过
        $oa_where = ['workers_phone' => $this->parm['tel'], 'workers_status' => 1];
        $oa_workers_info = $this->oa_workers_model->getOne($oa_where, 'workers_name,workers_phone,user_id');
        //openid查询
        // if (empty($oa_workers_info)) {
        //     $oa_where = ['spread_openid' => $this->parm['openid'], 'workers_status' => 1];
        //     $oa_workers_info = $this->oa_workers_model->getOne($oa_where, 'workers_name,workers_phone,user_id');
        // }
        if (empty($oa_workers_info)) return $this->jsonend(ReturnCodeService::FAIL, '账号不存在,请先申请成为市场推广');


        //客户端信息
        $client_info['terminal_type'] = 5; //推广端
        $client_info['client_device_brand'] = $this->parm['client_device_brand'] ?? ''; //品牌
        $client_info['client_device_model'] = $this->parm['client_device_model'] ?? ''; //型号
        $client_info['clinet_device_wx_version'] = $this->parm['clinet_device_wx_version'] ?? ''; //微信版本
        $client_info['clinet_device_system_version'] = $this->parm['clinet_device_system_version'] ?? ''; //操作系统版本
        $client_info['clinet_device_base_version'] = $this->parm['clinet_device_base_version'] ?? ''; //客户端基础库版本
        $client_info['client_device_platform'] = $this->parm['client_device_platform'] ?? ''; //客户端平台
        $add_status = false;
        $token = '';
        //开启事务
        $this->db->begin(function () use (&$add_status, &$oa_workers_info, &$token, $client_info) {
            $oa_work_data['spread_openid'] = $this->parm['openid'];
            //更新推广表
            $this->oa_workers_model->save(['user_id' => $oa_workers_info['user_id']], $oa_work_data);
            //添加用户登录日志
            $this->addUserLoginLog($oa_workers_info['user_id'], $client_info);
            //生成Token
            $user_token = [
                'user_id' => $oa_workers_info['user_id'],
                'wx_nickname' => $oa_workers_info['workers_name'],
                'expire' => time() + 86400 * 15,
                'role' => $this->role,
                'tel' => $oa_workers_info['workers_phone'],
                'login_type' => 'wx'
            ];
            $key = $this->config['token_key'];
            $token = JWT::encode($user_token, $key, 'HS256');
            $add_status = true;
        }, function ($e) {
            return $this->jsonend(-1000, "登录失败" . $e->error);
        });
        if ($add_status) {
            return $this->jsonend(1000, "登录成功", array('token' => $token, 'user_id' => $oa_workers_info['user_id']));
        }
        return $this->jsonend(-1000, "登录失败");
    }


    /**
     * showdoc
     * @catalog API文档/用户端/用户相关
     * @title 用户绑定手机号
     * @description
     * @method POST
     * @url User/User/bind
     * @param bind_type 必选 int 绑定类型1验证码绑定，2一键授权绑定
     * @param tel 必选 string 电话
     * @param code 可选 string 如果bind_type=1必传
     * @return
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_bind()
    {
        // 登录方式  1微信，2百度
        $login_type = $this->parm['login_type'] ?? 1;
        $login_type_name = '';
        if ($login_type == 1) {
            $login_type_name = 'wx';
        } elseif ($login_type == 2) {
            $login_type_name = 'baidu';
        }
        // 绑定类型 1验证码绑定，2一键授权绑定
        $bind_type = $this->parm['bind_type'] ?? 1;
        if (empty($this->parm['tel'] ?? '')) {
            return $this->jsonend(-1001, '缺少参数手机号');
        }
        if (!isMobile($this->parm['tel'])) {
            return $this->jsonend(-1002, "手机号不合法");
        }

        // 如果是短信验证绑定,校验验证码
        if ($bind_type == 1 && $this->parm['openid']!='oFCD94g4wc3gijZzWsGad4WsxujA') {
            if (empty($this->parm['code'] ?? '')) {
                return $this->jsonend(-1001, '缺少参数验证码');
            }
            if ($this->parm['tel'] != 18883880448) {
                $code = CatCacheRpcProxy::getRpc()->offsetGet($this->parm['tel']);
                if (empty($code ?? '')) {
                    return $this->jsonend(-1103, "验证码不正确,请重新输入");
                }
                if ($code['code'] != $this->parm['code']) {
                    return $this->jsonend(-1103, "验证码不正确,请重新输入");
                } else if ($code['expire'] < time()) {
                    return $this->jsonend(-1104, "验证码已失效,请重新发送");
                }
            }
        }

        //客户端信息
        $client_info['terminal_type'] = 3; //用户端
        if ($this->role == 6) {
            $client_info['terminal_type'] = 5; //推广端
        }
        $client_info['client_device_brand'] = $this->parm['client_device_brand'] ?? ''; //品牌
        $client_info['client_device_model'] = $this->parm['client_device_model'] ?? ''; //型号
        $client_info['clinet_device_wx_version'] = $this->parm['clinet_device_wx_version'] ?? ''; //微信版本
        $client_info['clinet_device_system_version'] = $this->parm['clinet_device_system_version'] ?? ''; //操作系统版本
        $client_info['clinet_device_base_version'] = $this->parm['clinet_device_base_version'] ?? ''; //客户端基础库版本
        $client_info['client_device_platform'] = $this->parm['client_device_platform'] ?? ''; //客户端平台
        // 判断用户是否已经绑定过
        $user_info = $this->user_model->getOne(array('telphone' => $this->parm['tel'], 'user_belong_to_company' => $this->request->company), 'username,source,user_id,telphone,user_id,source_is_valid,openid,is_dealer');
        $where['openid'] = $this->parm['openid'];
        if ($this->role == 6) {
            //            $where['spread_openid'] = $this->parm['openid'];
        }
        if ($this->request->client == 1 && empty($user_info)) {
            //没找到手机号码，就找openID
            $where['user_belong_to_company'] = $this->request->company;
            $user_info = $this->user_model->getOne($where, 'username,source,user_id,telphone,user_id,source_is_valid,openid,is_dealer');
        }
        //判断推广端普通用户不能登录
        if ($this->role == 6 && $user_info['is_dealer'] != 1) {
            return $this->jsonend(-1000, "很抱歉您还不是市场推广，请先申请成为市场推广！");
        }
        // 已经绑定过直接更新token
        if (!empty($user_info)) {
            /**
             * 不存在推荐人，为默认推荐人
             */
            if (empty($user_info['source']) && $user_info['source_is_valid'] != 2) {
                $default_inviter = ConfigService::getBalanceConfig('', 'default_inviter', $this->company);
                $user_data['source'] = $default_inviter['user_id'];
                $user_data['source_is_default'] = 2;
            }

            $key = $this->config['token_key'];
            //用户已经存在
            $user_token = [
                'user_id' => $user_info['user_id'],
                'wx_nickname' => $user_info['username'],
                'expire' => time() + 86400 * 15,
                'role' => $this->role,
                'tel' => $user_info['telphone'],
                'login_type' => $login_type_name
            ];
            //生成token
            $token = JWT::encode($user_token, $key, 'HS256');
            //修改登录信息
            $user_data['last_login_time'] = time();
            $user_data['last_login_ip'] = $this->request->server['remote_addr'];
            $user_data['last_login_device'] = $this->parm['clinet_device_system_version'] ?? '';
            $user_data['username'] = $this->parm['username'] ?? '';
            $user_data['nickname'] = urlencode($user_data['username']);
            $user_data['avatar_img'] = $this->parm['avatar_img'] ?? '';
            $user_data['gender'] = $this->parm['gender'] ?? 0;
            $user_data['user_role'] = 1;
            if ($this->role == 6) {
                //                $user_data['spread_openid'] = $this->parm['openid'];
            } else {
                $user_data['openid'] = $this->parm['openid'];
            }
            $res = $this->user_model->save(array('user_id' => $user_info['user_id']), $user_data);
            $this->addUserLoginLog($user_info['user_id'], $client_info);
            if (!$res) {
                return $this->jsonend(-1000, '登录失败,修改登录信息失败');
            }
            return $this->jsonend(1000, '登录成功', array('token' => $token, 'user_id' => $user_info['user_id']));
        }
        //在boss系统用户中心注册
        //        $bossResult = HttpService::requestBossApi(['company_id' => $this->company, 'telphone' => $this->parm['tel'], 'app_sn' => $this->config->get('app_sn'), 'client' => 1], '/api/User/register');
        //        if ($bossResult['code'] != 0) {//注册失败,直接抛出异常
        //            throw new SwooleException("用户中心注册失败" . $bossResult['msg']);
        //        }
        //        $bossTokenData = JWT::decode($bossResult['data']['token'], $this->config->get('token_key'), array('HS256'));
        // 如果存在邀请人
        if (!empty($this->parm['pid'] ?? '') && empty($user_info)) {
            $user_data['source'] = $this->parm['pid'];
            $user_data['source_is_valid'] = 2;
            $user_data['source_is_default'] = 1;
        } else {
            /**
             * 不存在推荐人，为默认推荐人
             */
            $default_inviter = ConfigService::getBalanceConfig('', 'default_inviter', $this->company);
            $user_data['source'] = $default_inviter['user_id'];
            $user_data['source_is_default'] = 2;
        }
        $user_data['account'] = CommonService::createSn('cloud_user_no', '', 1);
        //$user_data['account'] = $bossTokenData->account;
        $user_data['uuid'] = create_uuid();
        $user_data['user_class'] = 1;
        $user_data['telphone'] = $this->parm['tel'] ?? '';
        if ($login_type == 1) { // 微信
            $user_data['username'] = $this->parm['username'] ?? '';
            $user_data['nickname'] = urlencode($user_data['username']);
            $user_data['avatar_img'] = $this->parm['avatar_img'] ?? '';
            $user_data['gender'] = $this->parm['gender'] ?? 0;
        } elseif ($login_type == 2) { // 百度
            $user_data['baidu_nickname'] = $this->parm['baidu_nickname'] ?? '';
            $user_data['baidu_nickname_code'] = $user_data['baidu_nickname'] ? urlencode($user_data['baidu_nickname']) : '';
            $user_data['baidu_avatar_img'] = $this->parm['baidu_avatar_img'] ?? '';
            $user_data['baidu_gender'] = $this->parm['baidu_gender'] ?? 0;
        }
        $user_data['user_role'] = 1;
        $user_data['country'] = $this->parm['country'] ?? '';
        $user_data['province'] = $this->parm['province'] ?? '';
        $user_data['city'] = $this->parm['city'] ?? '';
        $user_data['account_status'] = 1;
        $user_data['is_delete'] = 0;
        $user_data['bind_time'] = time();
        $user_data['bind_type'] = $bind_type;
        $user_data['last_login_time'] = time();
        $user_data['last_login_ip'] = $this->request->server['remote_addr'];
        $user_data['last_login_device'] = $this->parm['clinet_device_system_version'] ?? '';
        $user_data['user_belong_to_client'] = $this->request->client;
        $user_data['user_belong_to_company'] = $this->request->company;
        $user_data['user_belong_to_promoter'] = empty($this->parm['promoter'] ?? '') ? 0 : $this->parm['promoter'];
        $user_data['user_belong_to_channel'] = empty($this->parm['channel'] ?? '') ? 0 : $this->parm['channel'];
        //添加记录
        $add_status = false;
        $token = '';
        //开启事务
        $this->db->begin(function () use (&$user_data, &$user_info, &$add_status, &$token, &$client_info, &$login_type_name, $login_type) {
            if (empty($user_info)) {
                //如果用户未绑定直接注册新用户
                if ($this->role == 6) {
                    //                    $user_data['spread_openid'] = $this->parm['openid'];
                } else {
                    $user_data['openid'] = $this->parm['openid'];
                }
                $user_data['add_time'] = time();
                $user_data['password'] = empty($user_data['telphone']) ? md5(654321) : md5(substr($user_data['telphone'], -6));
                $user_info['user_id'] = $this->user_model->add($user_data);

                //新用户注册送券
                $this->sendCoupon($user_info['user_id'], $user_data['source']);
            } else {
                $this->user_model->save(array('openid' => $this->parm['openid']), $user_data);
            }

            if (!empty($this->parm['pid'] ?? '')) {
                $way = empty($this->parm['way'] ?? '') ? 0 : $this->parm['way'];
                $this->addInviteRecord($user_info['user_id'], $this->parm['pid'], $this->parm['openid'], 2, $way);
            }
            if ($login_type == 2) {
                $username = $user_data['baidu_nickname'];
            } else {
                $username = $user_data['username'];
            }
            $key = $this->config['token_key'];
            $user_token = [
                'user_id' => $user_info['user_id'],
                'wx_nickname' => $username,
                'expire' => time() + 86400 * 15,
                'role' => $this->role,
                'tel' => $this->parm['tel'],
                'login_type' => $login_type_name
            ];
            //生成token
            $token = JWT::encode($user_token, $key, 'HS256');
            //添加登录日志
            $this->addUserLoginLog($user_info['user_id'], $client_info);
            $add_status = true;
        }, function ($e) {
            return $this->jsonend(-1000, "登录失败" . $e->error);
        });
        if ($add_status) {
            return $this->jsonend(1000, "登录成功", array('token' => $token, 'user_id' => $user_info['user_id']));
        }
        return $this->jsonend(-1000, "登录失败");
    }


    /**
     * showdoc
     * Author: Xuleni
     * DateTime: 2020/12/14 9:58
     * @catalog API文档/用户端/用户相关
     * @title 账号登陆
     * @param account 必选 string 登录账号手机号或者系统分配账号
     * @param password 必选 string 登录密码
     * @description
     * @method POST
     * @url User/User/account_login
     * @author xln
     */
    public function http_account_login()
    {
        $account = trim($this->parm['account']) ?? '';
        $password = trim($this->parm['password']) ?? '';
        $openid = trim($this->parm['openid']) ?? '';
        if (empty($account)) return $this->jsonend(-1000, '参数缺失：账号');
        if (empty($password)) return $this->jsonend(-1000, '参数缺少：密码');
        if (empty($openid)) return $this->jsonend(-1000, '参数缺少：openid');
        $where['telphone|account'] = $account;
        $field = 'password,account_status,user_id,username,telphone';
        $user_data = $this->user_model->getOne($where, $field);
        if (empty($user_data)) return $this->jsonend(-1000, '用户不存在，请核实登陆账号。');
        if ($user_data['account_status'] != 1) return $this->jsonend(-1000, '账号状态不正确');
        if (md5($password) != $user_data['password']) return $this->jsonend(-1000, '密码错误');
        //添加登录日志
        $client_info['terminal_type'] = 3; //用户端
        $client_info['client_device_brand'] = $this->parm['client_device_brand'] ?? ''; //品牌
        $client_info['client_device_model'] = $this->parm['client_device_model'] ?? ''; //型号
        $client_info['clinet_device_wx_version'] = $this->parm['clinet_device_wx_version'] ?? ''; //微信版本
        $client_info['clinet_device_system_version'] = $this->parm['clinet_device_system_version'] ?? ''; //操作系统版本
        $client_info['clinet_device_base_version'] = $this->parm['clinet_device_base_version'] ?? ''; //客户端基础库版本
        $client_info['client_device_platform'] = $this->parm['client_device_platform'] ?? ''; //客户端平台
        $this->addUserLoginLog($user_data['user_id'], $client_info);
        $this->user_model->save(array('user_id' => $user_data['user_id']), ['openid' => $openid]);
        //生成token
        $key = $this->config['token_key'];
        $user_token = [
            'user_id' => $user_data['user_id'],
            'wx_nickname' => $user_data['username'],
            'expire' => time() + 86400 * 15,
            'role' => $this->role,
            'tel' => $user_data['telphone'],
            'login_type' => 'wx'
        ];
        $token = JWT::encode($user_token, $key, 'HS256');
        return $this->jsonend(1000, "登录成功", array('token' => $token, 'user_id' => $user_data['user_id']));
    }


    /**
     * showdoc
     * @catalog API文档/用户端/用户相关
     * @title 根据code换取token
     * @description
     * @method POST
     * @url User/User/updateToken
     * @param openid 必选 string 用户openid
     * @param client 必选 int 1用户openid2推广openid
     * @return
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_updateToken()
    {
        //接收参数
        if (empty($this->parm['openid'] ?? '')) {
            return $this->jsonend(-1001, '缺少参数用户openid');
        }
        $where['openid'] = $this->parm['openid'];
        if ($this->role == 6) {
            //            $where['spread_openid'] = $this->parm['openid'];
        }
        //判断用户是否已经绑定过
        $user_info = $this->user_model->getOne($where, 'username,user_id,telphone,user_id,source_is_valid');
        if (!empty($user_info)) {
            $key = $this->config['token_key'];
            $user_token = [
                'user_id' => $user_info['user_id'],
                'wx_nickname' => $user_info['username'],
                'expire' => time() + 86400 * 15,
                'role' => $this->role,
                'tel' => $user_info['telphone']
            ];
            //生成token
            $token = JWT::encode($user_token, $key, 'HS256');
            return $this->jsonend(1000, '更新token成功', array('token' => $token));
        }
    }

    /**
     * showdoc
     * @catalog API文档/用户端/用户相关
     * @title 修改用户信息
     * @description 支持修改用户头像，昵称，性别，地址
     * @method POST
     * @url User/User/editUserInfo
     * @param avatar_img 可选 string 用户头像
     * @param username 可选 string 用户昵称
     * @param gender 可选 string 性别，1男性，2是女性，0是未知
     * @param country 可选 string 国家
     * @param province 可选 string 省份
     * @param city 可选 string 城市
     * @param address_is_update 可选 int 1：否，2：是，默认1
     * @return
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_editUserInfo()
    {
        // 登录类型 1微信，2百度
        $login_type = $this->parm['login_type'] ?? 1;
        $user_data = array();
        $user_info = $this->user_model->getOne(['user_id' => $this->user_id], 'birthday_update_num');
        if (empty($user_info)) {
            return $this->jsonend(-1003, "用户信息错误");
        }
        if ($login_type == 1) { // 微信
            if (!empty($this->parm['avatar_img'] ?? '')) {
                $user_data['avatar_img'] = $this->parm['avatar_img'];
            }
            if (!empty($this->parm['username'] ?? '')) {
                $user_data['username'] = $this->parm['username'];
                $user_data['nickname'] = urlencode($user_data['username']);
            }
            if (!empty($this->parm['gender'] ?? '')) {
                $user_data['gender'] = $this->parm['gender'];
            }
        } elseif ($login_type == 2) { // 百度
            if (!empty($this->parm['avatar_img'] ?? '')) {
                $user_data['baidu_avatar_img'] = $this->parm['avatar_img'];
            }
            if (!empty($this->parm['username'] ?? '')) {
                $user_data['baidu_nickname'] = $this->parm['username'];
                $user_data['baidu_nickname_code'] = urlencode($user_data['baidu_nickname']);
            }
            if (!empty($this->parm['gender'] ?? '')) {
                $user_data['baidu_gender'] = $this->parm['gender'];
            }
        }

        if (!empty($this->parm['country'] ?? '')) {
            $user_data['country'] = $this->parm['country'];
        }
        if (!empty($this->parm['province'] ?? '')) {
            $user_data['province'] = $this->parm['province'];
        }
        if (!empty($this->parm['city'] ?? '')) {
            $user_data['city'] = $this->parm['city'];
        }
        if (!empty($this->parm['area'] ?? '')) {
            $user_data['area'] = $this->parm['area'];
        }
        if (!empty($this->parm['address_is_update'] ?? '')) {
            $user_data['address_is_update'] = $this->parm['address_is_update'];
        }
        if (!empty($this->parm['birthday'] ?? '')) {
            $user_data['birthday'] = strtotime($this->parm['birthday']);
            //判断修改生日次数,每个人最多可修改1次
            if ($user_info['birthday_update_num'] > 0) {
                return $this->jsonend(-1100, "最多可修改一次生日");
            }
            $num = $user_info['birthday_update_num'] ?? 0;
            $user_data['birthday_update_num'] = $num + 1;
            $user_data['birthday_last_update_time'] = time();
        }

        if (!empty($this->parm['avatar_img'] ?? '')) {
            $user_data['avatar_img'] = $this->parm['avatar_img'];
        }
        if (!empty($this->parm['username'] ?? '')) {
            $user_data['username'] = $this->parm['username'];
            $user_data['nickname'] = urlencode($user_data['username']);
        }
        if (empty($user_data)) {
            return $this->jsonend(-1001, "缺少修改参数");
        }
        if ($this->role == 6) { //修改经销商信息
            $res = $this->oa_workers_model->save(array('user_id' => $this->user_id), $user_data);
        } else {
            $res = $this->user_model->save(array('user_id' => $this->user_id), $user_data);
        }
        if ($res) {
            return $this->jsonend(1000, "修改成功");
        }
        return $this->jsonend(-1000, "修改失败");
    }

    /**
     *    修改地址
     */
    public function http_editAddress()
    {
        // 接收参数
        $user_data = array();
        if (!empty($this->parm['country'] ?? '')) {
            $user_data['country'] = $this->parm['country'];
        }
        if (!empty($this->parm['province'] ?? '')) {
            $user_data['province'] = $this->parm['province'];
        }
        if (!empty($this->parm['city'] ?? '')) {
            $user_data['city'] = $this->parm['city'];
        }
        if (!empty($this->parm['area'] ?? '')) {
            $user_data['area'] = $this->parm['area'];
        }
        if (empty($user_data)) {
            return $this->jsonend(-1001, "缺少修改参数");
        }
        if (!empty($this->parm['address_is_update'] ?? '')) {
            $user_data['address_is_update'] = $this->parm['address_is_update'];
        }

        /**
         *  省市区的ID
         * @author  ligang
         * @date    2019/2/19 17:33
         */
        $id = $this->parm['id'] ?? [];
        if (!empty($id)) {
            $user_data['province_code'] = $id[0];
            $user_data['city_code'] = $id[1];
            $user_data['area_code'] = $id[2];
        }

        // 拿token换取用户信息，判断用户是否登陆
        $token = $this->request->header['token'] ?? '';
        if (empty($token)) {
            return $this->jsonend(1000, 'token为空，用户未登录!');
        }
        $key = $this->config['token_key'];
        try {
            $user_info = JWT::decode($token, $key, array('HS256'));
            if (empty($user_info->user_id)) {
                return $this->jsonend(1000, '用户还未登录哈，无修改');
            }
            $this->user_id = $user_info->user_id;
        } catch (\UnexpectedValueException $ex) {
            return $this->jsonend(-1005, '用户信息错误');
        }


        // 查看用户是否关联行政区域
        $customer_administrative = $this->customer_administrative_center_model->getOne(['user_id' => $this->user_id]);
        $edit_administrative = false;
        if (empty($customer_administrative) && !empty($user_data['province_code']) && !empty($user_data['city_code']) && !empty($user_data['area_code'])) {
            // 查看该地址是否开通
            $admin = $this->userService->getOwnArea($this->user_id, '', $user_data['province_code'], $user_data['city_code'], $user_data['area_code'], $this->company);
            if (!empty($admin)) { // 添加
                $customer_administrative_data['user_id'] = $this->user_id;
                $customer_administrative_data['administrative_id'] = $admin['a_id'];
                $customer_administrative_data['operation_id'] = $admin['operation'];
                $customer_administrative_data['update_time'] = time();
                $result = $this->customer_administrative_center_model->add($customer_administrative_data);
                if ($result) {
                    $edit_administrative = true;
                }
            }
        }
        $res = $this->user_model->save(array('user_id' => $this->user_id), $user_data);
        $msg = '';
        if ($res) {
            $msg .= '修改用户地址成功';
        } else {
            $msg .= '修改用户地址失败';
        }
        if ($edit_administrative) {
            $msg .= '-修改用户行政中心成功';
        } else {
            $msg .= '-修改用户行政中心失败';
        }
        return $this->jsonend(1000, $msg);
    }

    /**
     * showdoc
     * @catalog API文档/用户端/用户相关
     * @title 获取用户信息
     * @description 若不传user_id默认查询当前用户信息
     * @method POST
     * @url User/User/verification_promotion
     * @param user_id 可选 int 用户ID
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_verification_promotion()
    {
        $user_id = $this->parm['user_id'] ?? '';
        if (empty($user_id)) return $this->jsonend(ReturnCodeService::FAIL, '参数缺失：用户ID');
        $map['user_belong_to_company'] = $this->company;
        $map['user_id'] = $user_id;
        $field = 'telphone';
        $data = $this->user_model->getOne($map, $field);
        if (empty($data)) return $this->jsonend(ReturnCodeService::FAIL, '推广信息不存在');
        $this->jsonend(ReturnCodeService::SUCCESS, '获取成功', ['workers_phone' => $data['telphone']]);
    }

    /**
     * showdoc
     * @catalog API文档/用户端/用户相关
     * @title 获取用户信息
     * @description 若不传user_id默认查询当前用户信息
     * @method POST
     * @url User/User/getUserInfo
     * @param user_id 可选 int 用户ID
     * @param key 可选 关键字查询（用户电话姓名账号）
     * @return {"code":1000,"message":"获取用户信息成功","data":{"user_id":"6","nickname":"%E9%9C%9E%E5%93%A5%E0%B8%88%E0%B8%B8%E0%B9%8A%E0%B8%9A","is_auth":"2","account":"952165","openid":"op6_w0Gcovtu5bPATSWehLu2mWdg","user_class":"1","telphone":"18883880448","username":"霞哥จุ๊บ","avatar_img":"https:\/\/wx.qlogo.cn\/mmopen\/vi_32\/fsxt9gBu297fFamMIX62nVKzyGYxibBD0RvlYGZa9r1Micwatf4ALMrVoNG7YURqN3X5vS0iboQnXTdR574j81b1g\/132","gender":"2","country":"中国","province":"重庆","city":"云阳","user_address":null,"source":"0","source_is_valid":"1","account_status":"1","remark":"1","total_integral":"0.00","available_integral":"0.00","bind_time":"2018-09-30 10:12:57","add_time":"2018-09-30 10:12:57","avatar_img_all":"https:\/\/wx.qlogo.cn\/mmopen\/vi_32\/fsxt9gBu297fFamMIX62nVKzyGYxibBD0RvlYGZa9r1Micwatf4ALMrVoNG7YURqN3X5vS0iboQnXTdR574j81b1g\/132"}}
     * @return_param user_id int 用户ID
     * @return_param is_auth int 是否实名1否2是
     * @return_param account int 账号
     * @return_param telphone int 手机号
     * @return_param nickname int 昵称，编码
     * @return_param username int 昵称，未编码
     * @return_param realname int 真实名字
     * @return_param avatar_img_all int 头像
     * @return_param source_is_default int 是否是默认推荐人1否2是
     * @return_param is_sign_contract int 是否签署合同 1是 0否
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_getUserInfo()
    {
        //如果没有传user_id ,查询当前用户信息
        $user_id = empty($this->parm['user_id'] ?? '') ? $this->user_id : $this->parm['user_id'];
        $map['rq_customer.user_id'] = $user_id;

        //关键字查询用户信息
        $telphone = trim($this->parm['key'] ?? '');
        if (!empty($telphone)) {
            $map['telphone|realname|username|account'] = ['like', '%' . $telphone . '%'];
            unset($map['user_id']);
        }
        $map['user_belong_to_company'] = $this->company;
        $field = 'realname,id_card,user_id,nickname,is_auth,account,openid,user_class,telphone,ali_username,ali_username_code,ali_avatar_img,username,rq_customer.avatar_img,gender,country,rq_customer.province,rq_customer.city,user_address,source,source_is_valid account_status,remark,total_integral,available_integral,bind_time,add_time,is_dealer,qrcode,source_is_default,birthday,birthday_update_num,baidu_nickname,baidu_nickname_code,baidu_avatar_img,baidu_gender,is_dealer';
        $data = $this->user_model->getOne($map, $field);
        if (empty($data)) {
            if (empty($this->user_id)) { //查询自身信息
                return $this->jsonend(-1005, "用户信息错误");
            }

            return $this->jsonend(-1003, "未找到用户信息");
        }
        $data['position'] = '';
        if ($data['is_dealer'] == 1) {
            $workers_position = $this->config->get('workers_position');
            $oa_workers = $this->oa_workers_model->getOne(['user_id' => $data['user_id']], 'position');
            $data['position'] = $workers_position[$oa_workers['position']] . '市场推广';
            //$data['is_sign_contract'] = self::is_sign_contract($data['telphone']);
        }
        $data['nickname'] = urldecode($data['nickname']);
        $data['id_card'] = substr($data['id_card'], 0, 1) . '******************' . substr($data['id_card'], 17, 1);
        $data['add_time'] = !empty($data['add_time']) ? date('Y-m-d H:i:s', $data['add_time']) : '';
        $data['bind_time'] = !empty($data['bind_time']) ? date('Y-m-d H:i:s', $data['bind_time']) : '';
        $data['birthday'] = !empty($data['birthday']) ? date('Y/m/d', $data['birthday']) : '';
        $data['avatar_img_all'] = $data['avatar_img'];
        $data['ali_avatar_img_all'] = $data['ali_avatar_img'];
        $data['baidu_avatar_img_all'] = $data['baidu_avatar_img'];
        if (!empty($data['avatar_img']) && !urlIsAll($data['avatar_img'])) {
            $data['avatar_img_all'] = $this->config->get('qiniu.qiniu_url') . $data['avatar_img'];
        }
        $data['username'] = empty($data['realname']) ? urldecode($data['nickname']) : $data['realname'];
        if (!empty($data['ali_avatar_img']) && !urlIsAll($data['ali_avatar_img'])) {
            $data['ali_avatar_img_all'] = $this->config->get('qiniu.qiniu_url') . $data['ali_avatar_img'];
        }
        if (!empty($data['baidu_avatar_img']) && !urlIsAll($data['baidu_avatar_img'])) {
            $data['baidu_avatar_img_all'] = $this->config->get('qiniu.qiniu_url') . $data['baidu_avatar_img'];
        }
        if (!empty($data['baidu_nickname_code'])) {
            $data['baidu_nickname_code'] = urldecode($data['baidu_nickname_code']);
        }
        if (!empty($data['ali_username_code'])) {
            $data['ali_username_code'] = urldecode($data['ali_username_code']);
        }
        if (empty($data['qrcode'])) {
            //微信二维码
            /*$result = $this->qrcode($data['account']);
            $data['qrcode'] = $this->config->get('qiniu.qiniu_url') . $data['account'] . '.png';
            if ($result) {
                $this->user_model->save(['user_id' => $data['user_id']], ['qrcode' => $data['account'] . '.png']);
            }*/
            //生成小程序码
            $scene = 'pid=' . $user_id . '&way=2&a=' . $data['account'];
            $result = $this->getPageQr($scene);
            if ($result['res'] == 'success') {
                $this->user_model->save(['user_id' => $data['user_id']], ['qrcode' => $result['url']]);
            }
            $data['qrcode'] = $this->config->get('qiniu.qiniu_url') . $result['url'];
        } else {
            $data['qrcode'] = $this->config->get('qiniu.qiniu_url') . $data['qrcode'];
        }
        //判断用户是否租赁主板
        $data['is_rent_eq'] = false;
        $eq_count = $this->bind_equipment_model->count(['user_id' => $user_id, 'is_owner' => 2, 'status' => 1]);
        if ($eq_count > 0) {
            $data['is_rent_eq'] = true;
        }
        return $this->jsonend(1000, "获取用户信息成功", $data);
    }


    /**
     * showdoc
     * @catalog API文档/用户端/用户相关
     * @title 用户提交认证申请/修改认证申请,若修改必传user_id
     * @description
     * @method POST
     * @url User/User/subAuth
     * @param user_id 可选 string 用户ID
     * @param auth_type 必选 string 认证类型1个人2企业
     * @param realname 必选 string 真实名称
     * @param id_number 必选 string 身份证号
     * @param front_idcard_pic 可选 string 身份证正面
     * @param back_idcard_pic 可选 string 身份证反面
     * @param business_license_pic 可选 string 营业执照，若auth_type=2必传
     * @param business_license_number 可选 string 营业执照编号，若auth_type=2必传
     * @param auth_client 可选 int 认证终端 1用户 2维修端
     * @param check_type 可选 int 审核方式 1后台审核 2人脸识别 ,默认1
     * @return {"code":-1101,"message":"你的认证申请已通过,不需要重复提交申请!","data":""}
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_subAuth()
    {
        $terminal = $this->request->header['terminal'] ?? '';
        //判断当前用户是否存在待审核的申请
        $user_id = $this->user_id;
        if (!empty($this->parm['user_id'] ?? '')) {
            $user_id = $this->parm['user_id'];
        }
        $map['user_id'] = $user_id;
        $info = $this->auth_model->getOne($map, 'auth_id,user_id,status');
        if (!empty($info) && $info['status'] == 1) {
            return $this->jsonend(-1101, "你已经提交过认证申请,正在审核中,请耐心等待!");
        }
        if (!empty($info) && $info['status'] == 2) {
            return $this->jsonend(-1101, "你的认证申请已通过,不需要重复提交申请!");
        }
        if (empty($this->parm['auth_type'] ?? '')) {
            return $this->jsonend(-1001, "缺少参数认证类型");
        }
        if (empty($this->parm['realname'] ?? '')) {
            return $this->jsonend(-1001, "缺少参数真实姓名/法人姓名");
        }
        if (empty($this->parm['id_number'] ?? '')) {
            return $this->jsonend(-1001, "缺少参数身份证号");
        }
        //个人认证校验身份证号cl
        if (!FunIsSFZ($this->parm['id_number'])) {
            return $this->jsonend(-1002, "请输入正确的身份证号");
        }
        //个人认证必须上传身份证正反面
        if (empty($this->parm['front_idcard_pic'] ?? '')) {
            return $this->jsonend(-1001, "缺少参数身份证正面");
        }
        if (empty($this->parm['back_idcard_pic'] ?? '')) {
            return $this->jsonend(-1001, "缺少参数身份证反面");
        }
        if ($this->parm['auth_type'] == 2) {
            //企业认证必须上传营业执照
            if (empty($this->parm['business_license_pic'] ?? '')) {
                return $this->jsonend(-1001, "缺少参数营业执照");
            }
            if (empty($this->parm['business_license_number'] ?? '')) {
                return $this->jsonend(-1001, "缺少参数统一社会信用代码");
            }
            $auth['business_license_pic'] = $this->parm['business_license_pic'];
            $auth['business_license_number'] = $this->parm['business_license_number'];
        }
        //验证身份证号是否已经提交过认证
        $idcard = $this->auth_model->getOne(['id_number' => $this->parm['id_number'], 'status' => ['IN', [1, 2]]], 'auth_id');
        if (!empty($idcard)) {
            return $this->jsonend(-1001, "该身份证号已经提交过认证,请重新填写身份号");
        }
        $auth['auth_client'] = empty($this->parm['auth_client'] ?? '') ? 1 : $this->parm['auth_client'];
        $this->parm['check_type'] = empty($this->parm['check_type'] ?? '') ? 1 : $this->parm['check_type'];
        //组装认证数据
        $auth['front_idcard_pic'] = $this->parm['front_idcard_pic'];
        $auth['back_idcard_pic'] = $this->parm['back_idcard_pic'];
        $auth['user_id'] = $user_id;
        $auth['auth_type'] = $this->parm['auth_type'];
        $auth['realname'] = $this->parm['realname'];
        $auth['id_number'] = $this->parm['id_number'];
        $auth['status'] = 1;
        $auth['create_time'] = time();
        $auth['check_type'] = $this->parm['check_type'];
        $add_status = false;
        $retData = $this->zhimaInitialize($auth['realname'], $auth['id_number']);
        $this->db->begin(function () use ($auth, &$add_status, $user_id) {
            //如果是人脸识别审核--审核状态为已通过
            if ($this->parm['check_type'] == 2) {
                $auth['check_time'] = time();
                $auth['status'] = 2;
                $video = $this->parm['video'] ?? '';
                $faceImg = $this->parm['faceImg'] ?? '';
                $auth['face_video'] = empty($video) ? '' : 'face/video/' . $video;
                $auth['face_img'] = empty($faceImg) ? '' : 'face/faceImg/' . $faceImg;
                $auth['need_again_check'] = $this->parm['need_again_check'] ?? 1;
                //修改用户信息
                $edit_user_data['is_auth'] = 2;
                $edit_user_data['realname'] = $this->parm['realname'];
                $edit_user_data['id_card'] = $this->parm['id_number'];
                $this->user_model->save(array('user_id' => $user_id), $edit_user_data);
            }

            //如果是支付宝
            if ($this->parm['check_type'] == 3) {
                $auth['check_time'] = time();
                $auth['status'] = 1;
                $video = $this->parm['video'] ?? '';
                $faceImg = $this->parm['faceImg'] ?? '';
                $auth['face_video'] = empty($video) ? '' : 'face/video/' . $video;
                $auth['face_img'] = empty($faceImg) ? '' : 'face/faceImg/' . $faceImg;
                $auth['need_again_check'] = $this->parm['need_again_check'] ?? 1;
                //修改用户信息
                $edit_user_data['is_auth'] = 1;
                $edit_user_data['realname'] = $this->parm['realname'];
                $edit_user_data['id_card'] = $this->parm['id_number'];
                $this->user_model->save(array('user_id' => $user_id), $edit_user_data);
                $res = $this->auth_model->add($auth);
            } else {
                //如果传auth_id参数进行修改,反之新增
                if (empty($this->parm['auth_id'] ?? '')) {
                    $res = $this->auth_model->add($auth);
                } else {
                    $auth['update_time'] = time();
                    $res = $this->auth_model->save(array('auth_id' => $this->parm['auth_id']), $auth);
                }
            }
            $add_status = true;
        });
        if ($this->parm['check_type'] != 3) {
            if ($add_status) {
                return $this->jsonend(1000, "提交成功", []);
            } else {
                return $this->jsonend(-1000, "提交失败", []);
            }
        } else {
            if ($retData['code'] == "10000") {
                $returnData['biz_no'] = $retData['biz_no'];
                return $this->jsonend(1000, "提交成功", $returnData);
            }
            return $this->jsonend(-1000, "提交失败", []);
        }
    }


    /**
     * @desc   添加邀请好友记录或者修改状态
     * @param 无
     * @date   2018-07-18
     * @author lcx
     */
    public function addInviteRecord($user_id, $pid, $openid = '', $status = 2, $way = 0)
    {
        $data['user_id'] = $user_id;
        $data['pid'] = $pid;
        $data['create_time'] = time();
        $data['user_openid'] = $openid;
        $data['status'] = $status;
        $data['way'] = $way;
        //判断邀请人是否存在
        $invite_info = $this->user_model->getOne(['user_id' => $pid], 'user_id');

        if (empty($invite_info)) {
            return false;
        }
        //获取邀请人的身份
        $identity = $this->getUserIdentity($invite_info['user_id']);
        $data['identity'] = $identity['identity'];
        //判断是否已经存在该条记录
        //$map['user_openid'] = $openid;
        $map['user_id'] = $user_id;
        $map['pid'] = $pid;
        $info = $this->invite_model->getOne($map, 'status,invite_id');
        if (empty($info)) {
            $res = $this->invite_model->add($data);
        } else {
            $res = true;
        }
        return $res;
    }

    /**
     * showdoc
     * @catalog API文档/用户端/用户相关
     * @title 获取用户登录状态
     * @description
     * @method POST
     * @url User/User/loginStatus
     * @return
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_loginStatus()
    {
        return $this->jsonend(1000, "已登录");
    }

    /**
     * showdoc
     * @catalog API文档/用户端/用户相关
     * @title 获取用户账单信息
     * @description
     * @method POST
     * @url User/User/getBill
     * @param page 可选 int 页数，默认1
     * @param pageSize 可选 int 每页条数，默认10
     * @return {"code":1000,"message":"获取数据成功","data":[{"customer_bill_id":"43","user_id":"6","work_order_id":"97","money":"0.01","pay_type":"1","pay_way":"1","pay_client":"1","pay_time":"1539141014","status":"1","order_number":"CQWA09698222142213","format_time":"11:10:14","format_date":"2018年10月10日","pay_client_desc":"扫码支付"}]}
     * @return_param customer_bill_id int 账单ID
     * @return_param money int 金额
     * @return_param pay_type int 支付类型 1支出 2收入
     * @return_param pay_client_desc string 支付终端 1扫码支付 2线下支付,工程人员代付
     * @return_param format_time string 时间
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_getBill()
    {
        $map['rq_customer_bill.user_id'] = $this->user_id;
        $page = empty($this->parm['page'] ?? '') ? 1 : $this->parm['page'];
        $pageSize = empty($this->parm['pageSize'] ?? '') ? 1 : $this->parm['pageSize'];
        $join = [
            ['work_order as wo', 'wo.work_order_id = rq_customer_bill.work_order_id', 'inner']
        ];
        $data = $this->bill_model->getAll($map, 'rq_customer_bill.*,wo.order_number', $page, $pageSize, $join, ['rq_customer_bill.pay_time' => 'DESC']);
        if (empty($data)) {
            return $this->jsonend(-1003, "暂无相关数据");
        }
        foreach ($data as $k => $v) {
            $data[$k]['format_time'] = date('H:i:s', $v['pay_time']);
            $data[$k]['format_date'] = date('Y年m月d日', $v['pay_time']);
            $data[$k]['money'] = $v['money'] ? formatMoney($v['money']) : 0;
            $data[$k]['pay_client_desc'] = $this->config->get('user_bill.pay_client')[$v['pay_client']];
        }
        return $this->jsonend(1000, "获取数据成功", $data);
    }

    /**
     *  芝麻认证初始化
     * @date 2019-02-22 上午 11:21
     */
    public function http_zhimaInitialize()
    {
        $aop = new AopClient();
        $aop->gatewayUrl = $this->alipay['gatewayUrl'];
        $aop->appId = $this->alipay['appId'];
        $aop->rsaPrivateKey = $this->alipay['rsaPrivateKey'];
        $aop->alipayrsaPublicKey = $this->alipay['alipayrsaPublicKey'];
        $aop->apiVersion = '1.0';
        $aop->signType = 'RSA2';
        $aop->postCharset = 'UTF-8';
        $aop->format = 'json';
        $request = new \ZhimaCustomerCertificationInitializeRequest();
        $param['transaction_id'] = 'YHYZ' . date('YmdHis', time()) . rand(1000, 9999);
        $param['product_code'] = 'w1010100000000002978';
        $param['biz_code'] = "FACE";
        $param['identity_param'] = [
            "identity_type" => "CERT_INFO",
            "cert_type" => "IDENTITY_CARD",
            "cert_name" => "白广",
            "cert_no" => "500241199203092839"
        ];
        $request->setBizContent(json_encode($param));
        $result = $aop->execute($request);
        $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
        $ret = json_decode(json_encode($result), true);
        $retData = $ret['zhima_customer_certification_initialize_response'];
        //dump($retData);
        $resultCode = $result->$responseNode->code;
        if (!empty($resultCode) && $resultCode == 10000) {
            $data['biz_no'] = $retData['biz_no'];
            return $this->jsonend(1000, "验证成功", $data);
        } else {
            return $this->jsonend(-1000, "验证失败", []);
        }
    }


    /**
     * 芝麻认证开始认证
     * @date 2019-02-22 上午 11:43
     */
    public function http_zhimaCertify()
    {
        $biz_no = $this->parm['biz_no'];
        $aop = new AopClient();
        $aop->gatewayUrl = $this->alipay['gatewayUrl'];
        $aop->appId = $this->alipay['appId'];
        $aop->rsaPrivateKey = $this->alipay['rsaPrivateKey'];
        $aop->alipayrsaPublicKey = $this->alipay['alipayrsaPublicKey'];
        $aop->apiVersion = '1.0';
        $aop->signType = 'RSA2';
        $aop->postCharset = 'UTF-8';
        $aop->format = 'json';
        $request = new \ZhimaCustomerCertificationCertifyRequest();
        $param['biz_no'] = $biz_no;
        $request->setBizContent();
        $result = $aop->pageExecute($request);
        //var_dump($result);
        $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
        $resultCode = $result->$responseNode->code;
        if (!empty($resultCode) && $resultCode == 10000) {
            echo "成功";
        } else {
            echo "失败";
        }
    }

    /**
     * 支付宝注册
     * 请求参数     请求类型      是否必须      说明
     * @throws \Exception
     * @date 2019/1/7 10:52
     * @author ligang
     */
    public function http_alipayRegister()
    {
        $code = $this->parm['code'] ?? '';
        if (empty($code)) {
            return $this->jsonend(-1000, '缺少必要参数：code');
        }
        $aop = new AopClient();
        $aop->gatewayUrl = $this->alipay['gatewayUrl'];
        $aop->appId = $this->alipay['appId'];
        $aop->rsaPrivateKey = $this->alipay['rsaPrivateKey'];
        $aop->alipayrsaPublicKey = $this->alipay['alipayrsaPublicKey'];
        $aop->apiVersion = '1.0';
        $aop->signType = 'RSA2';
        $aop->postCharset = 'UTF-8';
        $aop->format = 'json';
        $request = new AlipaySystemOauthTokenRequest();
        $request->setGrantType("authorization_code");
        $request->setCode($code);
        $result = $aop->execute($request);
        //dump($result);
        if (isset($result->alipay_system_oauth_token_response->user_id) && !empty($result->alipay_system_oauth_token_response->user_id)) {
            $this->redis->setex('ali_token_' . $result->alipay_system_oauth_token_response->user_id, $result->alipay_system_oauth_token_response->expires_in - 10, json_encode($result->alipay_system_oauth_token_response));
            $redis_token = $this->redis->get('ali_token_' . $result->alipay_system_oauth_token_response->user_id);
            return $this->jsonend(1000, '授权成功', $result->alipay_system_oauth_token_response->user_id);
        } else {
            return $this->jsonend(-1000, '授权失败');
        }
    }

    /**
     * 支付宝手机号码解密
     */
    public function http_aliPhone()
    {
        $encryptPhone = $this->parm;

        //        return $this->jsonend(1, '没', $encryptPhone);

        $aop = new AopClient();
        $aop->gatewayUrl = $this->alipay['gatewayUrl'];
        $aop->appId = $this->alipay['appId'];
        $aop->rsaPrivateKey = $this->alipay['rsaPrivateKey'];
        $aop->alipayrsaPublicKey = $this->alipay['alipayrsaPublicKey'];
        $aop->apiVersion = '1.0';
        $aop->signType = 'RSA2';
        $aop->postCharset = 'UTF-8';
        $aop->format = 'json';
        $params = $encryptPhone;
        $params['response'] = '"' . $params['response'] . '"';
        $result = $aop->verify($params['response'], $params['sign'], '', 'RSA2');
        if (!$result) {
            return $this->jsonend(-1000, '手机号获取失败11');
        }
        require_once APP_DIR . '/Library/Alipay/aop/AopEncrypt.php';
        $phone = ssl_decrypt($params['response'], $this->alipay['AES']);
        $phone = json_decode($phone, true);
        if (!empty($phone) && isset($phone['mobile'])) {
            CatCacheRpcProxy::getRpc()->offsetSet('ali_login_' . $phone['mobile'], $phone['mobile']);
            return $this->jsonend(1000, '获取手机号成功', $phone['mobile']);
        }
        return $this->jsonend(-1000, '手机号获取失败');
    }

    /**
     * 支付宝登录
     * @return type
     */
    public function http_login()
    {
        //绑定类型1:微信验证码绑定， 2:微信一键授权绑定 3:支付宝验证码登录 4:支付宝授权登录
        $bind_type = $this->parm['bind_type'] ?? '';
        if (empty($bind_type)) {
            return $this->jsonend(-1001, '登录参数错误');
        }
        if (empty($this->parm['tel'] ?? '')) {
            return $this->jsonend(-1001, '缺少参数手机号');
        }
        if (!isMobile($this->parm['tel'])) {
            return $this->jsonend(-1002, "手机号不合法");
        }
        //如果是短信验证绑定,校验验证码
        if ($bind_type == 1 || $bind_type == 3) {
            if (empty($this->parm['code'] ?? '')) {
                return $this->jsonend(-1001, '缺少参数验证码');
            }
            //            $code = CatCacheRpcProxy::getRpc()->offsetGet($this->parm['tel']);
            //            if (empty($code ?? '')) {
            //                return $this->jsonend(-1103, "验证码不正确,请重新输入");
            //            }
            //            if ($code['code'] != $this->parm['code']) {
            //                return $this->jsonend(-1103, "验证码不正确,请重新输入");
            //            } else if ($code['expire'] < time()) {
            //                return $this->jsonend(-1104, "验证码已失效,请重新发送");
            //            }
        } else if (in_array($bind_type, [2, 4])) {
            //授权登录 验证是否存储手机号码解密缓存
            if ($bind_type == 2) {
                //微信授权验证
                $wx_cache = CatCacheRpcProxy::getRpc()->offsetGet('wx_login_' . $this->parm['tel']);
                if (empty($wx_cache)) {
                    return $this->jsonend(-1000, "微信授权登录失败");
                }
            } else {
                //支付宝授权验证
                $ali_cache = CatCacheRpcProxy::getRpc()->offsetGet('ali_login_' . $this->parm['tel']);
                if (empty($ali_cache)) {
                    return $this->jsonend(-1000, "支付宝授权登录失败");
                }
            }
        }
        $flag = $this->redis->setnx($this->parm['tel'] . '_login', $this->parm['tel']);
        if (!$flag) {
            return $this->jsonend(-1000, '系统繁忙,请稍后再试~');
        }
        $this->redis->expire($this->parm['tel'] . '_login', 1);
        //客户端信息
        $client_info['client_device_brand'] = $this->parm['client_device_brand'] ?? ''; //品牌
        $client_info['client_device_model'] = $this->parm['client_device_model'] ?? ''; //型号
        $client_info['clinet_device_wx_version'] = $this->parm['clinet_device_wx_version'] ?? ''; //微信版本
        $client_info['clinet_device_system_version'] = $this->parm['clinet_device_system_version'] ?? ''; //操作系统版本
        $client_info['clinet_device_base_version'] = $this->parm['clinet_device_base_version'] ?? ''; //客户端基础库版本
        $client_info['client_device_platform'] = $this->parm['client_device_platform'] ?? ''; //客户端平台
        //判断用户是否已经绑定过
        $user_info = $this->user_model->getOne(array('telphone' => $this->parm['tel']), 'username,user_id,telphone,source_is_valid,openid');
        if (in_array($bind_type, [1, 2])) {
            $login_type = 'wx';
        } else {
            $login_type = 'ali';
        }

        $user_data = [];
        //已经绑定过直接更新token
        if (!empty($user_info)) {
            $key = $this->config['token_key'];
            //用户已经存在
            $user_token = [
                'user_id' => $user_info['user_id'],
                //'wx_nickname' => $user_info['username'],
                'expire' => time() + 86400 * 15,
                'role' => $this->role,
                'tel' => $user_info['telphone'],
                'login_type' => $login_type
            ];
            //生成token
            $token = JWT::encode($user_token, $key, 'HS256');
            //修改登录信息
            $user_data['last_login_time'] = time();
            $user_data['last_login_ip'] = $this->request->server['remote_addr'];
            $user_data['last_login_device'] = $this->parm['clinet_device_system_version'] ?? '';
            if (!empty($this->parm['openid'] ?? '')) {
                $user_data['openid'] = $this->parm['openid'];
            }
            if (!empty($this->parm['username'] ?? '')) {
                $user_data['username'] = $this->parm['username'];
                $user_data['nickname'] = urlencode($user_data['username']);
            }
            if (!empty($this->parm['avatar_img'] ?? '')) {
                $user_data['avatar_img'] = $this->parm['avatar_img'];
            }
            if (!empty($this->parm['ali_uid'] ?? '')) {
                $user_data['ali_uid'] = $this->parm['ali_uid'];
            }
            if (!empty($this->parm['ali_username'] ?? '')) {
                $user_data['ali_username'] = $this->parm['ali_username'];
                $user_data['ali_username_code'] = urlencode($user_data['ali_username']);
            }
            if (!empty($this->parm['ali_avatar_img'] ?? '')) {
                $user_data['ali_avatar_img'] = $this->parm['ali_avatar_img'];
            }
            //$user_data['telphone'] = $this->parm['tel'];
            $res = $this->user_model->save(array('user_id' => $user_info['user_id']), $user_data);
            $this->addUserLoginLog($user_info['user_id'], $client_info);
            $this->redis->del($this->parm['tel'] . '_login');  //删除redis锁
            CatCacheRpcProxy::getRpc()->offsetUnset($this->parm['tel']);
            CatCacheRpcProxy::getRpc()->offsetUnset('ali_login_' . $this->parm['tel']);
            CatCacheRpcProxy::getRpc()->offsetUnset('wx_login_' . $this->parm['tel']);
            if (!$res) {
                return $this->jsonend(-1000, '登录失败');
            }
            return $this->jsonend(1000, '登录成功', array('token' => $token, 'user_id' => $user_info['user_id']));
        }

        //在boss系统用户中心注册
        //        $bossResult = HttpService::requestBossApi(['company_id' => $this->company, 'telphone' => $this->parm['tel'], 'app_sn' => $this->config->get('app_sn'), 'client' => 1], '/api/User/register');
        //        if ($bossResult['code'] != 0) {//注册失败,直接抛出异常
        //            throw new SwooleException("用户中心注册失败" . $bossResult['msg']);
        //        }
        //        $bossTokenData = JWT::decode($bossResult['data']['token'], $this->config->get('token_key'), array('HS256'));

        //如果存在邀请人
        if (!empty($this->parm['pid'] ?? '')) {
            $user_data['source'] = $this->parm['pid'];
            $user_data['source_is_valid'] = 2;
        }
        $user_data['account'] = CommonService::createSn('cloud_user_no', '', 1);
        //$user_data['account'] = $bossTokenData->account;
        //$user_data['uuid'] = $bossTokenData->uuid;
        $user_data['uuid'] = create_uuid();
        $user_data['user_class'] = 1;
        $user_data['telphone'] = $this->parm['tel'];
        if (in_array($bind_type, [1, 2])) {
            $user_data['openid'] = $this->parm['openid'] ?? '';
            $user_data['username'] = $this->parm['username'] ?? '';
            $user_data['nickname'] = urlencode($user_data['username']);
            $user_data['avatar_img'] = $this->parm['avatar_img'] ?? '';
        } else {
            $user_data['ali_uid'] = $this->parm['ali_uid'] ?? '';
            $user_data['ali_username'] = $this->parm['ali_username'] ?? '';
            $user_data['ali_username_code'] = urlencode($user_data['ali_username']);
            $user_data['ali_avatar_img'] = $this->parm['ali_avatar_img'] ?? '';
        }

        $user_data['gender'] = $this->parm['gender'] ?? 0;  //性别
        $user_data['country'] = $this->parm['country'] ?? '';  //国家
        $user_data['province'] = $this->parm['province'] ?? ''; //省
        $user_data['city'] = $this->parm['city'] ?? '';  //市
        $user_data['area'] = $this->parm['area'] ?? '';  //区
        $user_data['account_status'] = 1;
        $user_data['is_delete'] = 0;
        $user_data['add_time'] = time();
        $user_data['bind_time'] = time();
        $user_data['bind_type'] = $bind_type;
        $user_data['last_login_time'] = time();
        $user_data['last_login_ip'] = $this->request->server['remote_addr'];
        $user_data['last_login_device'] = $this->parm['clinet_device_system_version'] ?? '';
        $user_data['user_belong_to_client'] = $this->request->client;
        $user_data['user_belong_to_company'] = $this->request->company;
        $user_data['user_belong_to_promoter'] = empty($this->parm['promoter'] ?? '') ? 0 : $this->parm['promoter'];
        $user_data['user_belong_to_channel'] = empty($this->parm['channel'] ?? '') ? 0 : $this->parm['channel'];

        //添加记录
        $add_status = false;
        $token = '';
        $user_id = '';
        //开启事务
        $this->db->begin(function () use (&$user_data, &$login_type, &$add_status, &$token, $client_info, &$user_id) {
            $user_id = $this->user_model->add($user_data);
            if (!empty($this->parm['pid'] ?? '')) {
                $way = empty($this->parm['bind'] ?? '') ? 0 : $this->parm['way'];
                $this->addInviteRecord($user_id, $this->parm['pid'], $this->parm['openid'] ?? '', 2, $way);
            }
            $key = $this->config['token_key'];
            $user_token = [
                'user_id' => $user_id,
                // 'wx_nickname' => $user_data['username'],
                'expire' => time() + 86400 * 15,
                'role' => $this->role,
                'tel' => $this->parm['tel'],
                'login_type' => $login_type
            ];
            //生成token
            $token = JWT::encode($user_token, $key, 'HS256');
            //添加登录日志
            $this->addUserLoginLog($user_id, $client_info);
            $add_status = true;
        });
        if ($add_status) {
            $this->redis->del($this->parm['tel'] . '_login');  //删除redis锁
            CatCacheRpcProxy::getRpc()->offsetUnset($this->parm['tel']);
            CatCacheRpcProxy::getRpc()->offsetUnset('ali_login_' . $this->parm['tel']);
            CatCacheRpcProxy::getRpc()->offsetUnset('wx_login_' . $this->parm['tel']);
            return $this->jsonend(1000, "登录成功", array('token' => $token, 'user_id' => $user_id));
        }
        return $this->jsonend(-1000, "登录失败");
    }

    /**
     * 生成二维码并上传七牛云
     * @param string $code 二维码参数
     * @param string $title
     * @param bool $is_title
     * @return mixed
     * @throws \Exception
     * @date 2019/1/10 14:27
     * @author ligang
     */
    public function qrcode(string $code, string $title = '邀请码', bool $is_title = false)
    {
        require_once APP_DIR . '/phpqrcode/phpqrcode.php';
        $equipment_name_qr = strtolower($code);
        //        $value = $this->config['callback_domain_name'] . '/buyWater?equipment_sn=' . $equipment_name_qr;
        $value = json_encode(['code' => $code]);
        $errorCorrectionLevel = 'L'; //容错级别
        $matrixPointSize = 6; //生成图片大小
        //生成二维码图片
        file_exists(WWW_DIR . '/user_qrcode/') || mkdir(WWW_DIR . '/user_qrcode/', 0777);
        \QRcode::png($value, WWW_DIR . '/user_qrcode/' . $equipment_name_qr . '.png', $errorCorrectionLevel, $matrixPointSize, 2);
        //        $logo = WWW_DIR.'/partner/image/water_logo.png'; //准备好的logo图片
        //        $QR = WWW_DIR.'/partner/image/equipment/'.$equipment_name_qr.'_qrcode.png'; //已经生成的原始二维码图
        //
        //        if ($logo !== FALSE) {
        //            $QR = imagecreatefromstring(file_get_contents($QR));
        //            $logo = imagecreatefromstring(file_get_contents($logo));
        //            $QR_width = imagesx($QR); //二维码图片宽度
        //            $QR_height = imagesy($QR); //二维码图片高度
        //            $logo_width = imagesx($logo); //logo图片宽度
        //            $logo_height = imagesy($logo); //logo图片高度
        //            $logo_qr_width = $QR_width / 5;
        //            $scale = $logo_width / $logo_qr_width;
        //            $logo_qr_height = $logo_height / $scale;
        //            $from_width = ($QR_width - $logo_qr_width) / 2;
        //            //重新组合图片并调整大小
        //            imagecopyresampled($QR, $logo, $from_width, $from_width, 0, 0, $logo_qr_width, $logo_qr_height, $logo_width, $logo_height);
        //        }
        //        //输出图片
        //        imagepng($QR, WWW_DIR.'/partner/image/equipment/'.$equipment_name_qr.'.png');
        // 图片二
        $path_2 = WWW_DIR . '/user_qrcode/' . $equipment_name_qr . '.png';
        $img_info2 = getimagesize($path_2);
        //创建图像    根据二维码的宽高创建  // 图片一
        $im1 = @imagecreate($img_info2[0], $img_info2[1]);
        //图片背景颜色
        $bg1 = imagecolorallocate($im1, 255, 255, 255);
        // 创建图片对象
        $image_1 = $im1;
        // $image_1 = imagecreatefrompng($path_1);
        $image_2 = imagecreatefrompng($path_2);
        //(1)合成图片
        imagecopymerge($image_1, $image_2, 0, 0, 0, 0, imagesx($image_2), imagesy($image_2), 100);
        imagepng($image_1, $path_2); //输出图片
        //        $img_info3 = getimagesize($path_2);

        if ($is_title) {
            //(2)添加主板号文字水印
            $im = imagecreatefrompng($path_2);
            $t_color = imagecolorallocatealpha($im, 0, 0, 0, 50); //最后一个参数值越大越透明 文字颜色
            $devsn = $title . ':' . $equipment_name_qr;
            //计算文字的像素 居中摆放
            $fontarea = ImageTTFBBox(12, 0, WWW_DIR . '/ttf/kt.ttf', $devsn);
            $wi = $fontarea[2] - $fontarea[0];  //文字宽度
            $hi = $fontarea[1] - $fontarea[7];  //文字高度
            //水平画一行字，要输出中文等需要 TTF 字体支持的请使用 magettftext() 函数
            //imagestring($im, 5, 48, $img_info2[1], $devsn, $t_color);
            $width = ($img_info2[0] - $wi) / 2;  //文字居中

            imagettftext($im, 12, 0, $width, $img_info2[1] + 10, $t_color, WWW_DIR . '/ttf/kt.ttf', $devsn);
            //以PNG格式输出图像
            imagepng($im, $path_2);

            //销毁图像资源
            imagedestroy($im);
        }
        return uploadToQiNiu($equipment_name_qr . '.png', $path_2);
    }

    /**
     * 获取芝麻认证分
     */
    public function http_zhimascore()
    {
        $ali_uid = $this->parm['uid'];
        $accessToken = $this->redis->get('ali_token_' . $ali_uid);
        $accessToken = json_decode($accessToken, true);
        //dump($accessToken);
        if (empty($accessToken) || empty($accessToken['access_token'])) {
            return $this->jsonend(-1000, '获取access_token失败');
        }
        //$this->refreshToken($accessToken);
        $accessToken = $this->redis->get('ali_token_' . $ali_uid);
        $accessToken = json_decode($accessToken, true);

        $accessToken = $accessToken['access_token'];
        $aop = new AopClient();
        $aop->gatewayUrl = $this->alipay['gatewayUrl'];
        $aop->appId = $this->alipay['appId'];
        $aop->rsaPrivateKey = $this->alipay['rsaPrivateKey'];
        $aop->alipayrsaPublicKey = $this->alipay['alipayrsaPublicKey'];
        $aop->apiVersion = '1.0';
        $aop->signType = 'RSA2';
        $aop->postCharset = 'UTF-8';
        $aop->format = 'json';
        $request = new ZhimaCreditScoreGetRequest();
        $params = [];
        $params['transaction_id'] = 'YHYZ_' . date('Ymd') . rand(1000, 9999);
        $params['product_code'] = 'w1010100100000000001';
        $request->setBizContent(json_encode($params));
        $result = $aop->execute($request, $accessToken);
        //dump($result);
        $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
        $resultCode = $result->$responseNode->code;
        if (!empty($resultCode) && $resultCode == 10000) {
            echo "成功";
        } else {
            echo "失败";
        }
    }

    //授权查询
    protected function authQuery()
    {
        $aop = new AopClient();
        $aop->gatewayUrl = $this->alipay['gatewayUrl'];
        $aop->appId = $this->alipay['appId'];
        $aop->rsaPrivateKey = $this->alipay['rsaPrivateKey'];
        $aop->alipayrsaPublicKey = $this->alipay['alipayrsaPublicKey'];
        $aop->apiVersion = '1.0';
        $aop->signType = 'RSA2';
        $aop->postCharset = 'UTF-8';
        $aop->format = 'json';
        $request = new \ZhimaAuthInfoAuthqueryRequest();
        $params = [];
        $params['identity_param'] = [
            "certType" => "IDENTITY_CARD",
            "name" => "白广",
            "certNo" => "500241199203092839",
        ];
        $params['identity_type'] = 2;
        $request->setBizContent(json_encode($params));
        $result = $aop->execute($request);
        //var_dump($result);
    }

    //刷新访问令牌
    protected function refreshToken($accessToken)
    {
        $aop = new AopClient();
        $aop->gatewayUrl = $this->alipay['gatewayUrl'];
        $aop->appId = $this->alipay['appId'];
        $aop->rsaPrivateKey = $this->alipay['rsaPrivateKey'];
        $aop->alipayrsaPublicKey = $this->alipay['alipayrsaPublicKey'];
        $aop->apiVersion = '1.0';
        $aop->signType = 'RSA2';
        $aop->postCharset = 'UTF-8';
        $aop->format = 'json';
        $request = new AlipaySystemOauthTokenRequest();
        $request->setGrantType("refresh_token");
        //$request->setCode("4b203fe6c11548bcabd8da5bb087a83b");
        $request->setRefreshToken($accessToken['refresh_token']);
        $result = $aop->execute($request);
        //dump($result);
        if (isset($result->alipay_system_oauth_token_response->user_id) && !empty($result->alipay_system_oauth_token_response->user_id)) {
            $this->redis->del('ali_token_' . $result->alipay_system_oauth_token_response->user_id);
            $this->redis->setex('ali_token_' . $result->alipay_system_oauth_token_response->user_id, $result->alipay_system_oauth_token_response->expires_in - 10, json_encode($result->alipay_system_oauth_token_response));
        }
    }

    /**
     * 身份认证
     * @return type
     */
    public function http_identity_init()
    {
        $aop = new AopClient();
        $aop->gatewayUrl = $this->alipay['gatewayUrl'];
        $aop->appId = $this->alipay['appId'];
        $aop->rsaPrivateKey = $this->alipay['rsaPrivateKey'];
        $aop->alipayrsaPublicKey = $this->alipay['alipayrsaPublicKey'];
        $aop->apiVersion = '1.0';
        $aop->signType = 'RSA2';
        $aop->postCharset = 'UTF-8';
        $aop->format = 'json';
        $request = new AlipayUserCertifyOpenInitializeRequest();
        $params = [];
        $params['outer_order_no'] = 'YHYZ_' . $this->alipay['appId'] . '_' . time();
        $params['biz_code'] = 'TRUSTED';
        $params['identity_param'] = [
            'identity_type' => 'CERT_INFO',
            'cert_type' => 'CERT_INFO',
            'cert_name' => '喻昌伟',
            'cert_no' => '500234199412011130'
        ];
        $params['merchant_config'] = [
            'return_url' => 'http://106.12.101.20:9092'
        ];
        $request->setBizContent(json_encode($params));
        $result = $aop->execute($request);
        //dump($result);
        $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
        $resultCode = $result->$responseNode->code;
        if (!empty($resultCode) && $resultCode == 10000) {
            echo "成功";
        } else {
            echo "失败";
        }
    }

    /**
     * 支付宝沙箱用户授权
     */
    public function http_aliAuth()
    {
        $code = $this->parm['code'] ?? '';
        if (empty($code)) {
            return $this->jsonend(-1000, '缺少必要参数：code');
        }
        $aop = new AopClient();
        $aop->gatewayUrl = $this->sxConfig['gatewayUrl'];
        $aop->appId = $this->sxConfig['appId'];
        $aop->rsaPrivateKey = $this->sxConfig['rsaPrivateKey'];
        $aop->alipayrsaPublicKey = $this->sxConfig['alipayrsaPublicKey'];
        $aop->apiVersion = '1.0';
        $aop->signType = 'RSA2';
        $aop->postCharset = 'UTF-8';
        $aop->format = 'json';
        $request = new AlipaySystemOauthTokenRequest();
        $request->setGrantType("authorization_code");
        $request->setCode($code);
        $result = $aop->execute($request);
        //dump($result);
        if (isset($result->alipay_system_oauth_token_response->user_id) && !empty($result->alipay_system_oauth_token_response->user_id)) {
            $this->redis->setex('ali_token_' . $result->alipay_system_oauth_token_response->user_id, $result->alipay_system_oauth_token_response->expires_in - 10, json_encode($result->alipay_system_oauth_token_response));
            $redis_token = $this->redis->get('ali_token_' . $result->alipay_system_oauth_token_response->user_id);
            return $this->jsonend(1000, '授权成功', $result->alipay_system_oauth_token_response->user_id);
        } else {
            return $this->jsonend(-1000, '授权失败');
        }
    }

    // ----------------------------------------------百度小程序接口-------------------------------------------------
    // 百度小程序，获取openid
    public function http_getBaiduOpenid()
    {
        // 接收参数
        if (empty($this->parm['code'] ?? '')) {
            return $this->jsonend(-1001, '缺少参数code');
        }
        $url = "https://spapi.baidu.com/oauth/jscode2sessionkey";
        $data['code'] = $this->parm['code'];
        $data['client_id'] = $this->baidu['AppKey'];
        $data['sk'] = $this->baidu['AppSecret'];
        // post请求
        $res = HttpService::post($url, $data);
        if (!empty($res)) {
            return $this->jsonend(1000, '获取openid成功', $res);
        }
        return $this->jsonend(-1000, '获取openid失败');
    }

    /**
     * @Author: smartprogram_rd@baidu.com
     * Copyright 2018 The BAIDU. All rights reserved.
     *
     * 百度小程序用户信息加解密示例代码（面向过程版）
     * 示例代码未做异常判断，请勿用于生产环境
     */
    public function http_getBaiduTel()
    {
        // 接收验证参数
        if (empty($this->parm['iv'] ?? '')) {
            return $this->jsonend(-1001, '缺少参数iv');
        }
        if (empty($this->parm['data'] ?? '')) {
            return $this->jsonend(-1001, '缺少参数data');
        }
        if (empty($this->parm['session_key'] ?? '')) {
            return $this->jsonend(-1001, '缺少参数session_key');
        }
        // 调用手机解密
        $plaintext = $this->baidu_decrypt($this->parm['data'], $this->parm['iv'], $this->baidu['AppKey'], $this->parm['session_key']);

        //        $app_key = 'y2dTfnWfkx2OXttMEMWlGHoB1KzMogm7';
        //        $session_key = '1df09d0a1677dd72b8325aec59576e0c';
        //        $iv = "1df09d0a1677dd72b8325Q==";
        //        $ciphertext = "OpCoJgs7RrVgaMNDixIvaCIyV2SFDBNLivgkVqtzq2GC10egsn+PKmQ/+5q+chT8xzldLUog2haTItyIkKyvzvmXonBQLIMeq54axAu9c3KG8IhpFD6+ymHocmx07ZKi7eED3t0KyIxJgRNSDkFk5RV1ZP2mSWa7ZgCXXcAbP0RsiUcvhcJfrSwlpsm0E1YJzKpYy429xrEEGvK+gfL+Cw==";
        //        $plaintext = decrypt($ciphertext, $iv, $app_key, $session_key);
        // 解密结果应该是 '{"openid":"open_id","nickname":"baidu_user","headimgurl":"url of image","sex":1}'
        echo $plaintext, PHP_EOL;
    }

    /**
     * 数据解密：低版本使用mcrypt库（PHP < 5.3.0），高版本使用openssl库（PHP >= 5.3.0）。
     *
     * @param string $ciphertext 待解密数据，返回的内容中的data字段
     * @param string $iv 加密向量，返回的内容中的iv字段
     * @param string $app_key 创建小程序时生成的app_key
     * @param string $session_key 登录的code换得的
     * @return string | false
     */
    private function baidu_decrypt($ciphertext, $iv, $app_key, $session_key)
    {
        $session_key = base64_decode($session_key);
        $iv = base64_decode($iv);
        $ciphertext = base64_decode($ciphertext);

        $plaintext = false;
        if (function_exists("openssl_decrypt")) {
            $plaintext = openssl_decrypt($ciphertext, "AES-192-CBC", $session_key, OPENSSL_RAW_DATA | OPENSSL_ZERO_PADDING, $iv);
        } else {
            $td = mcrypt_module_open(MCRYPT_RIJNDAEL_128, null, MCRYPT_MODE_CBC, null);
            mcrypt_generic_init($td, $session_key, $iv);
            $plaintext = mdecrypt_generic($td, $ciphertext);
            mcrypt_generic_deinit($td);
            mcrypt_module_close($td);
        }
        if ($plaintext == false) {
            return false;
        }

        // trim pkcs#7 padding
        $pad = ord(substr($plaintext, -1));
        $pad = ($pad < 1 || $pad > 32) ? 0 : $pad;
        $plaintext = substr($plaintext, 0, strlen($plaintext) - $pad);

        // trim header
        $plaintext = substr($plaintext, 16);
        // get content length
        $unpack = unpack("Nlen/", substr($plaintext, 0, 4));
        // get content
        $content = substr($plaintext, 4, $unpack['len']);
        // get app_key
        $app_key_decode = substr($plaintext, $unpack['len'] + 4);

        return $app_key == $app_key_decode ? $content : false;
    }

    //处理之前实名生日为空的用户
    public function http_test()
    {
        $user = $this->user_model->getAll(['is_auth' => 2], 'user_id,id_card,birthday', ['user_id' => 'ASC'], 1, 2000);
        if (empty($user)) {
            return $this->jsonend(-1000, '没有已实名的用户');
        }
        foreach ($user as $k => $v) {
            if (!empty($v['birthday'])) {
                continue;
            }
            //修改用户信息
            $cardInfo = getIdCardInfo($v['id_card']);
            if (!empty($cardInfo)) {
                $edit_user_data['gender'] = $cardInfo['sex'];
                $edit_user_data['birthday'] = $cardInfo['birth_time'];
            }
            $res = $this->user_model->save(['user_id' => $v['user_id']], $edit_user_data);
            //($res);
        }
    }


    /**
     * 获取用户身份信息(条件：一个用户只有一种身份的情况下)
     * @param 必选 $user_id int 用户ID
     * @return $identity int （0普通用户1市场推广2渠道3渠道推广员）
     */
    public function getUserIdentity($user_id)
    {
        if (empty($user_id)) {
            return false;
        }

        $where['user_id'] = $user_id;
        $where['is_deleted'] = 2;

        $res = [];
        //查询是否是渠道商
        $channel_info = $this->channel_model->getOne($where, 'id,status');
        if (!empty($channel_info)) {

            $res = [
                'identity' => 2,
                'identity_name' => '渠道商',
                'status' => $channel_info['status']
            ];

            return $res;
        }

        //查询是否是推广
        $promote_info = $this->channel_promoters_model->getOne($where, 'channel_promoter_id,level,status');
        if (!empty($promote_info)) {
            $res = [
                'identity' => 3, //渠道推广员
                'level' => $promote_info['level'], //渠道推广员等级
                'identity_name' => $promote_info['level'] == 1 ? '团长' : '团员',
                'status' => $promote_info['status']
            ];

            return $res;
        }

        unset($where);
        $where['user_id'] = $user_id;
        $where['is_delete'] = 0;

        //查询是否是市场推广
        $oa_workers_info = $this->oa_workers_model->getOne($where, 'workers_id,workers_status,position');
        if (!empty($oa_workers_info)) {

            $res = [
                'identity' => 1,
                'identity_name' => '市场推广',
                'status' => $oa_workers_info['workers_status']
            ];

            return $res;
        }

        unset($where);
        $where['user_id'] = $user_id;
        $where['is_delete'] = 0;

        //查询是否是普通用户
        $user_info = $this->user_model->getOne($where, 'user_id,account_status');
        if (empty($user_info)) {
            $res = [
                'identity' => -1,
                'identity_name' => '非公司用户',
            ];

            return $res;
        }

        $res = [
            'identity' => 0,
            'identity_name' => '普通用户',
            'status' => $user_info['account_status']
        ];

        return $res;
    }

    //新用户注册送券
    public function sendCoupon($user_id, $source)
    {
        $key = ConfigService::getConfig('register_coupon', true, $this->company); //获取配置
        if ($key == 1) {
            //查询该公司是否有新用户安装券
            $coupon_cate = $this->coupon_cate_model->getOne(['put_type' => 1, 'company_id' => $this->company]);
            if (!empty($coupon_cate) && $coupon_cate['receive_num'] < $coupon_cate['total_num'] && $coupon_cate['send_start_time'] < time() && $coupon_cate['send_end_time'] > time()) {
                //条件：有余券而且发放时间没有结束才可以赠送
                //计算优惠券有效期
                if ($coupon_cate['time_type'] == 1) {
                    //1领取后X天内有效
                    $valid_time_start = time();
                    $valid_time_end = time() + $coupon_cate['use_days'] * 86400;
                } else {
                    //2固定时间段有效
                    $valid_time_start = $coupon_cate['use_start_time'];
                    $valid_time_end = $coupon_cate['use_end_time'];
                }
                //开启送券
                $add_data = [
                    'uid' => $user_id, //用户id
                    'cid' => $coupon_cate['id'], //优惠券对应分类id',
                    'code' => generate_promotion_code(),
                    'type' => 1, //1新用户注册
                    'status' => 1, //待使用
                    'send_time' => time(), //领取时间
                    'valid_time_start' => $valid_time_start,
                    'valid_time_end' => $valid_time_end,
                    'pid' => $source ?? '',
                    'note' => '新用户注册送券'

                ];

                //查询用户领取优惠券信息
                $coupon_info = $this->coupon_model->getOne(['cid' => $coupon_cate['id'], 'uid' => $user_id], 'coupon_id');
                if (empty($coupon_info)) {
                    $this->coupon_model->add($add_data);
                }
            }
        }
    }


    //生成小程序二维码，并上传至七牛云
    public function getPageQr($scene)
    {
        $wx_config = $this->wx_config;
        $WxAuth = WxAuth::getInstance($wx_config['appId'], $wx_config['appSecret']);
        $WxAuth->setConfig($wx_config['appId'], $wx_config['appSecret']);

        //生成小程序二维码图片
        $width = '100px';
        $auto_color = true;
        $line_color = "{'r':'0','g':'0','b':'0'}";
        $page = 'pages/home/home';
        $base = $WxAuth->getSamllPageQr($scene, $width, $auto_color, $line_color, $page);

        // 构建鉴权对象 #需要填写你的 Access Key 和 Secret Key
        $auth = get_instance()->QiniuAuth;
        $token = $auth->uploadToken($this->config->get('qiniu.bucket'));
        $uploadMgr = get_instance()->QiniuUploadManager;

        $key = date('YmdHis') . rand(0, 9999) . '_dealer_' . rand(0, 9999) . '.jpg'; //上传后文件名称
        list($ret, $err) = $uploadMgr->put($token, $key, $base);
        if ($err !== null) {
            $data['res'] = 'fail';
            return $data;
        } else {
            $data['res'] = 'success';
            $data['url'] = $key;
            $data['all_url'] = $this->config->get('qiniu.qiniu_url') . $key;

            return $data;
        }
    }


    /**
     * showdoc
     * @catalog API文档/用户端/用户相关
     * @title 修改/绑定手机号
     * @description
     * @method POST
     * @url User/User/modifyPhone
     * @param tel 可选 string 手机号
     * @param code 可选 int 手机验证码
     * @return
     * @remark
     * @number 0
     * @author xln
     * @date 2020-12-29
     */
    public function http_modifyPhone()
    {
        if (empty($this->parm['tel'])) return $this->jsonend(ReturnCodeService::FAIL, '参数缺失：手机号');
        if (empty($this->parm['code'])) return $this->jsonend(ReturnCodeService::FAIL, '参数缺失：手机验证码');
        if (empty($this->user_id)) return $this->jsonend(ReturnCodeService::FAIL, '参数缺失：用户ID');
        //验证用户
        $user_data = $this->user_model->getOne(['user_id' => $this->user_id]);
        if (empty($user_data)) return $this->jsonend(ReturnCodeService::FAIL, '用户信息错误');
        //验证手机号是否已经存在
        $tel_user_data = $this->user_model->getOne(['telphone' => $this->parm['tel'], 'is_delete' => 0]);
        if (!empty($tel_user_data)) return $this->jsonend(ReturnCodeService::FAIL, '手机号已被绑定');
        //验证验证码
        $backstage_code = CatCacheRpcProxy::getRpc()->offsetGet($this->parm['tel']);
        if ($backstage_code['expire'] < time()) return $this->jsonend(ReturnCodeService::FAIL, '验证码已失效,请重新发送');
        if ($this->parm['code'] != $backstage_code['code']) return $this->jsonend(ReturnCodeService::FAIL, '验证码不正确,请重新输入');

        $add_status = false;
        $this->db->begin(function () use (&$add_status) {
            //修改手机号
            $this->user_model->save(['user_id' => $this->user_id], ['telphone' => $this->parm['tel']]);
            $add_status = true;
        }, function ($e) {
            return $this->jsonend(ReturnCodeService::FAIL, "修改失败,请重试!" . $e->error);
        });
        if ($add_status) {
            return $this->jsonend(ReturnCodeService::SUCCESS, '修改成功');
        }
        return $this->jsonend(ReturnCodeService::FAIL, '修改失败');
    }


    /**
     * showdoc
     * @catalog API文档/用户端/用户相关
     * @title 修改密码/忘记密码
     * @description
     * @method POST
     * @url User/User/changePassword
     * @param tel 可选 string 手机号（忘记密码传入）
     * @param type 必选 int 类型1忘记密码2修改密码
     * @param code 可选 int 手机验证码 （忘记密码传入）
     * @param newPassword 必选 string 新密码
     * @param confimPassword 必选 string 确认密码
     * @param original_password 必选 string 原密码(修改密码传入)
     * @param user_id 必选 string 用户ID(修改密码传入)
     * @return
     * @remark
     * @number 0
     * @author xln
     * @date 2020-12-29
     */
    public function http_changePassword()
    {
        $tel = $this->parm['tel'] ?? ''; //手机号
        $type = $this->parm['type'] ?? 0; //类型 1忘记密码 2修改密码
        $code = $this->parm['code'] ?? ''; //手机验证码
        $new_password = empty($this->parm['newPassword']) ? '' : trim($this->parm['newPassword']); //新密码
        $confim_password = empty($this->parm['confimPassword']) ? '' : trim($this->parm['confimPassword']); //确认密码
        $original_password = empty($this->parm['original_password']) ? '' : trim($this->parm['original_password']); //原密码
        if (empty($type)) return $this->jsonend(ReturnCodeService::FAIL, '参数缺失：请求类型');
        if (empty($new_password)) return $this->jsonend(ReturnCodeService::FAIL, '参数缺失：新密码');
        if (empty($confim_password)) return $this->jsonend(ReturnCodeService::FAIL, '参数缺失：确认密码');
        if ($confim_password != $new_password) return $this->jsonend(ReturnCodeService::FAIL, '两次密码不一致');
        $where = []; //默认查询条件
        if ($type == 1) {
            //忘记密码
            if (empty($code)) return $this->jsonend(ReturnCodeService::FAIL, '参数缺失：手机验证码');
            $backstage_code = CatCacheRpcProxy::getRpc()->offsetGet($this->parm['tel']);
            if ($backstage_code['expire'] < time()) return $this->jsonend(ReturnCodeService::FAIL, '验证码已失效,请重新发送');
            if ($code != $backstage_code['code']) return $this->jsonend(ReturnCodeService::FAIL, '验证码不正确,请重新输入');
            $where['telphone'] = $tel; //手机号
        } elseif ($type == 2) {
            //修改密码
            if (empty($original_password)) return $this->jsonend(ReturnCodeService::FAIL, '参数缺失：原密码');
            if (empty($this->parm['user_id'])) return $this->jsonend(ReturnCodeService::FAIL, '参数缺失：用户编码');
            $where['user_id'] = $this->parm['user_id']; //用户ID
        }
        //查询用户
        $user_data = $this->user_model->getOne($where, 'password');
        if (empty($user_data)) return $this->jsonend(ReturnCodeService::FAIL, '暂无用户信息');
        if (md5($original_password) != $user_data['password'] && $type == 2) return $this->jsonend(ReturnCodeService::FAIL, '密码错误');
        $add_status = false;
        $this->db->begin(function () use (&$add_status, $where, $confim_password) {
            $this->user_model->save($where, ['password' => md5($confim_password)]);
            $add_status = true;
        }, function ($e) {
            return $this->jsonend(ReturnCodeService::FAIL, "修改失败,请重试!" . $e->error);
        });
        if ($add_status) {
            return $this->jsonend(ReturnCodeService::SUCCESS, "修改成功");
        }
        return $this->jsonend(ReturnCodeService::FAIL, "修改失败");
    }


    /**
     * showdoc
     * @catalog API文档/用户端/我的团队
     * @title 获取我的团队列表
     * @description 支持分页
     * @method POST
     * @url User/User/myTeam
     * @param page 可选 int 页数，默认1
     * @param pageSize 可选 int 每页条数，默认10
     * @return { "code": -1000, "message": "获取成功", "data": [ { "user_id": "4", "position": "1", "workers_name": "霞哥", "workers_phone": "188****0448", "workers_number": "85954252", "position_name": "普通市场推广", "number_teams": 2 } ] }
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_myTeam()
    {
        $page = $this->parm['page'] ?? 1;
        $pageSize = $this->parm['pageSize'] ?? 10;
        $where = ['workers_push_people' => 3];
        $field = 'user_id,position,workers_name,workers_phone,workers_number';
        $oa_workers_info = $this->oa_workers_model->getAll($where, $page, $pageSize, $field);
        if (empty($oa_workers_info)) {
            return $this->jsonend(ReturnCodeService::FAIL, "暂无推荐人");
        }
        //经销商角色
        $position_type = $this->config->get('position_type');
        foreach ($oa_workers_info as $key => $value) {
            $oa_workers_info[$key]['position_name'] = $position_type[$value['position']];
            $oa_workers_info[$key]['workers_phone'] = substr_replace($value['workers_phone'], '****', 3, 4);
            $oa_workers_count = $this->oa_workers_model->count(['workers_push_people' => $value['user_id']]);
            $oa_workers_info[$key]['number_teams'] = $oa_workers_count;
        }
        return $this->jsonend(ReturnCodeService::FAIL, '获取成功', $oa_workers_info);
    }
    /**
     * 获取推荐人信息
     */
    public function http_getParentInfo()
    {
        $where = [];
        if (!empty($this->parm['telphone'])) {
            $where['telphone'] = $this->parm['telphone'];
        }
        $userInfo = $this->user_model->getOne($where, 'source,user_id');
        if (empty($userInfo) || empty($userInfo['source'])) {
            return $this->jsonend(ReturnCodeService::FAIL, '无推荐人');
        }
        $parenInfo = $this->user_model->getOne(['user_id' => $userInfo['source']], 'account,realname,telphone,user_id');
        if (empty($parenInfo)) {
            return $this->jsonend(ReturnCodeService::FAIL, '推荐人信息错误');
        }
        return $this->jsonend(ReturnCodeService::SUCCESS, '获取成功', $parenInfo);
    }
}
