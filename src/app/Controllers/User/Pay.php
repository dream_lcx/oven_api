<?php

namespace app\Controllers\User;


use app\Services\Common\HttpService;
use app\Services\Common\ReturnCodeService;
use app\Services\Common\WxPayService;
use app\Services\Device\DeviceService;


/**
 * 用户端/缴费相关
 */
class Pay extends Base
{
    protected $user_model;
    protected $customer_code_model;
    protected $fee_order_model;
    protected $contract_model;
    protected $customer_code_bill_model;
    protected $coupon_model;
    protected $equipmentListsModel;


    public function initialization($controller_name, $method_name)
    {
        parent::initialization($controller_name, $method_name);
        $this->user_model = $this->loader->model('CustomerModel', $this);
        $this->customer_code_model = $this->loader->model('CustomerCodeModel', $this);
        $this->fee_order_model = $this->loader->model('FeeOrderModel', $this);
        $this->contract_model = $this->loader->model('ContractModel', $this);
        $this->customer_code_bill_model = $this->loader->model('CustomerCodeBillModel', $this);
        $this->coupon_model = $this->loader->model('CouponModel', $this);
        $this->equipmentListsModel = $this->loader->model('EquipmentListsModel',$this);

    }


    /**
     * showdoc
     * @catalog API文档/用户端/缴费相关
     * @title 我要缴费
     * @description 获取缴费详情
     * @method POST
     * @url User/Pay/getFeeInfo
     * @param code 必选 int 户号
     * @return {"code":1000,"message":"获取成功","data":{}}
     * @return_param code_id int 户号ID
     * @return_param code int 户号
     * @return_param province string 省
     * @return_param city string 市
     * @return_param area string 区
     * @return_param address string 详细地址
     * @return_param payment_amount int 缴费金额元
     * @return_param save_money float 节省金额元
     * @return_param create_time string 时间
     * @remark
     * @number 0
     * @author tx
     * @date 2020-9-15
     */
    public function http_getFeeInfo()
    {
        //接收参数
        $code = $this->parm['code'] ?? '';
        if (empty($code)) {
            return $this->jsonend(ReturnCodeService::NO_DATA, '缺少户号参数');
        }
        $where['code'] = substr($code, 0, 8);
        $where['c.company_id'] = $this->company;

        //查询户号信息
        $join = [
            ['contract c', 'c.contract_id = rq_customer_code.contract_id', 'left'], // 主板表对应户号表
        ];
        $code_info = $this->customer_code_model->getOne($where, 'code_id,code,code_name,c.province,c.city,c.area,c.address,payment_amount,create_time',$join);
        if (empty($code_info)) {
            return $this->jsonend(ReturnCodeService::FAIL, '户号信息错误');
        }
        $code_info['payment_amount'] = $code_info['payment_amount'] ? json_decode($code_info['payment_amount'], true) : [];
        //查询账单信息
        $code_bill = $this->customer_code_bill_model->getAll(['customer_code' => $code_info['code'], 'status' => 1], 'bill_id,cycle_start_time,money');
        $total_money = 0;
        if (!empty($code_bill)) {
            foreach ($code_bill as $k => $v) {
                $code_bill[$k]['cycle_start_time'] = !empty($v['cycle_start_time']) ? date('Y-m', $v['cycle_start_time']) : '--';
                $code_bill[$k]['money'] = formatMoney($v['money']);
                $total_money += $code_bill[$k]['money'];
            }

        }

        $data = [
            'info' => $code_info,
            'data' => $code_bill ?? [],
            'total_money' => $total_money,
        ];
        return $this->jsonend(1000, '获取列表成功', $data);

    }

    /**
     * showdoc
     * @catalog API文档/用户端/缴费相关
     * @title 缴费
     * @description 缴费
     * @method POST
     * @url User/Pay/feePay
     * @param code 必选 int 户号
     * @param coupon_id 可选 array 是否使用优惠券
     * @param pay_type 可选 int 支付方式1微信支付,2对公转账
     * @param payment_voucher 可选 string 付款凭证,仅对公转账必传
     * @return {"code":1000,"message":"支付成功","data":{}}
     * @return_param
     * @remark
     * @number 0
     * @author tx
     * @date 2020-9-15
     */
    public function http_feePay()
    {
        //接收参数
        $code = $this->parm['code'] ?? '';
        $code = substr($code, 0, 8);//目前硬件上的二维码始终多了一个汉字，特殊处理
        $coupon_id = $this->parm['coupon_id'] ?? '';//优惠券id
        $pay_type = $this->parm['pay_type']??1;//支付方式1微信支付 2对公转账

        if (empty($code)) {
            return $this->jsonend(ReturnCodeService::NO_DATA, '缺少户号参数');
        }
        if($pay_type==2 && empty($this->parm['payment_voucher'])){
            return $this->jsonend(ReturnCodeService::FAIL, '请上传凭证');
        }
        //查询优惠券信息
        $coupon_money = 0;
        if (!empty($coupon_id)) {
            $coupon_data = $this->coupon_model->getCouponList($coupon_id, 2);
            if (empty($coupon_data)) {
                return $this->jsonend(ReturnCodeService::NO_DATA, '优惠券不存在');
            }
            foreach ($coupon_data as $k => $v) {
                if ($v['status'] != 1) {
                    return $this->jsonend(ReturnCodeService::NO_DATA, '优惠券已使用或已过期');
                }
                if ($v['valid_time_start'] > time()) {
                    return $this->jsonend(ReturnCodeService::NO_DATA, '优惠券还未到使用日期');
                }
                if ($v['valid_time_end'] < time()) {
                    return $this->jsonend(ReturnCodeService::NO_DATA, '优惠券已经过期');
                }
                $coupon_money += $v['money'];
            }

        }
        //查询户号信息
        $code_info = $this->customer_code_model->getOne(['code' => $code, 'company_id' => $this->company], 'code_id,code,payment_amount,state');
        if (empty($code_info) || $code_info['state'] != 1) {
            return $this->jsonend(ReturnCodeService::FAIL, '户号信息错误或状态异常');
        }
        //查询用户信息
        $user_info = $this->user_model->getOne(['user_id' => $this->user_id], 'user_id,openid');
        if (empty($user_info)) {
            return $this->jsonend(ReturnCodeService::FAIL, '用户信息错误');
        }
        //查询账单信息
        $code_bill = $this->customer_code_bill_model->getAll(['customer_code' => $code_info['code'], 'status' => 1], 'bill_id,money');
        if (empty($code_bill)) {
            return $this->jsonend(ReturnCodeService::FAIL, '暂无待结账单信息');
        }

        $bill_id = [];
        $total_money = 0;
        foreach ($code_bill as $k => $v) {
            $code_bill[$k]['money'] = $v['money'];
            array_push($bill_id, $v['bill_id']);
            $total_money += $code_bill[$k]['money'];
        }
        //计算实际付款金额
        if ($total_money >= $coupon_money) {
            $payment_money = $total_money - $coupon_money;
        } else {
            $payment_money = 0;
        }

        $add_data = [
            'user_id' => $this->user_id,
            'bill_id' => ',' . implode(',', $bill_id) . ',',
            'company_id' => $this->company,
            'customer_code' => $code_info['code'],
            'order_sn' => createFeeSn('Y'),
            'money' => $total_money,//支付金额
            'discount_money' => $coupon_money,
            'coupon_id' => $coupon_id ? json_encode($coupon_id) : '',//优惠券id
            'create_time' => time(),
            'create_date' => date('Y-m', time()),
            'remark' => '用户户号账单缴费',
            'payment_voucher'=>$this->parm['payment_voucher']??''
        ];
        $check = false;
        $jsapi = [];
        $code = '';
        $this->db->begin(function () use (&$check,$pay_type, $add_data, $user_info, $total_money, &$jsapi, &$code, $bill_id, $payment_money, $coupon_money, $coupon_id) {
            //创建订单
            $order_id = $this->fee_order_model->add($add_data);
            //订单支付
            $pay_data['order_sn'] = $add_data['order_sn'];  // 订单编号
            $pay_data['money'] = formatMoney($payment_money, 2);  // 订单实际付款金额/元

            if ($pay_data['money'] != 0) {
                if($pay_type==2){
                    // 1-- 修改订单状态
                    $this->fee_order_model->save(['id' => $order_id], ['pay_status' => 3, 'pay_time' => time(), 'discount_money' => $coupon_money, 'payment_money' => $payment_money]);
                    //修改账单信息表
                    $this->customer_code_bill_model->save(['bill_id' => ['IN', $bill_id]], ['status' => 3]);
                    //修改优惠券信息
                    if (!empty($coupon_id)) {
                        $this->coupon_model->save(['coupon_id' => ['IN', $coupon_id]], ['status' => 2, 'use_time' => time()]);
                    }
                    $check = true;
                    $code = 1000;
                }else{
                    $notify_url = $this->config->get('callback_domain_name') . '/User/Callback/feeOrder';
                    if ($this->dev_mode == 1) {
                        $notify_url = $this->config->get('debug_config.callback_domain_name') . '/User/Callback/feeOrder';
                    }
                    if ($this->app_debug == 1) {
                        $pay_data['money'] = 1;
                    }
                    $jsapi = WxPayService::pay($this->wx_config, $user_info['openid'], $pay_data, $notify_url, $this->pay_config['sub_mch_id'] ?? '', $this->pay_config['mode'] ?? 1);
                    if ($jsapi) {
                        $check = true;
                        $code = 1000;
                    }
                }


            } else {
                /*//$url = '/User/Callback/feeCallback';
                $url = $this->config->get('callback_domain_name') . '/User/Callback/feeCallback';
                if ($this->dev_mode == 1) {
                    $pay_data['money'] = 0.01;
                    $url = $this->config->get('debug_config.callback_domain_name') . '/User/Callback/feeCallback';
                }

                $jsapi = HttpService::post(json_encode($pay_data), $url);
                if($jsapi){
                    $check=true;
                    $code=1001;
                }*/

                // 1-- 修改订单状态
                $this->fee_order_model->save(['id' => $order_id], ['pay_status' => 2, 'pay_time' => time(), 'discount_money' => $coupon_money, 'payment_money' => $payment_money]);
                //修改账单信息表
                $this->customer_code_bill_model->save(['bill_id' => ['IN', $bill_id]], ['status' => 2, 'settle_time' => time(), 'is_settlement' => 1]);

                //修改优惠券信息
                if (!empty($coupon_id)) {
                    $this->coupon_model->save(['coupon_id' => ['IN', $coupon_id]], ['status' => 2, 'use_time' => time()]);
                }
                $check = true;
                $code = 1001;
            }

        }, function ($e) {
            return $this->jsonend(ReturnCodeService::FAIL, '支付失败', $e->error);
        });
        if ($check) {
            return $this->jsonend($code, '支付成功', $jsapi);
        }
        return $this->jsonend(ReturnCodeService::FAIL, '支付失败');


    }



    /**
     * showdoc
     * @catalog API文档/用户端/缴费相关
     * @title 获取用户缴费记录列表
     * @description
     * @method POST
     * @url User/Pay/getFeeRecordLists
     * @param page 可选 int 页数，默认1
     * @param pageSize 可选 int 每页条数，默认10
     * @param startTime 可选 string 开始时间格式2020-09-24
     * @param code_no 必选 string 户号
     * @return {"code":"1000","message":"获取成功","data":{"data":[]}}
     * @return_param id int 自增id
     * @return_param customer_code int 户号
     * @return_param order_sn string 单号
     * @return_param pay_status int 支付状态1待支付2已支付
     * @return_param discount_money float 优惠金额/元
     * @return_param money float 支付金额
     * @return_param payment_money float 实际付款金额
     * @return_param wx_callback_num int 微信支付回调单号
     * @return_param gas_consumption int 用气量
     * @return_param pay_time string 支付时间
     * @return_param create_time string 下单时间
     * @return_param remark string 备注
     * @return_param coupon_id string 优惠券id
     * @return_param type int 发放类型1新用户注册2线下发放3免费领取4抽奖领取5邀请好友新装(缴费专用)6安装券
     * @return_param code string 优惠券兑换码
     * @return_param status int 0待领取1待使用2已使用3已过期
     * @return_param use_time string 使用时间
     * @return_param valid_time_start string 有效期开始时间
     * @return_param valid_time_end string 有效期结束时间
     * @return_param money int 优惠券金额/元
     * @remark
     * @number 0
     * @author tx
     * @date 2020/7/21
     */
    public function http_getFeeRecordLists()
    {
        // 接收参数
        $page = $this->parm['page'] ?? 1;
        $pageSize = $this->parm['pageSize'] ?? 10;
        $startTime = $this->parm['startTime'] ?? '';
        $endTime = $this->parm['endTime'] ?? '';
        $startDate = $this->parm['startDate'] ?? '';
        $code_no = $this->parm['code_no'] ?? '';
        if (empty($code_no)) {
            return $this->jsonend(-1000, '缺少参数');
        }

        //$where['company_id']=$this->company;
//        $where['user_id']=$this->user_id;
        $where['pay_status'] = 2;
        $where['customer_code'] = $code_no;

        // 判断用户有没有输开始日期 // 判断用户有没有输结束日期
        if (!empty($startTime) && !empty($endTime)) {
            $where['create_time'] = ['between', [strtotime($startTime), strtotime($endTime) + (60 * 60 * 24 - 1)]]; //终止到当天23:59:59
        } else if (!empty($startTime)) {
            $where['create_time'] = ['>=', strtotime($startTime)];
        } else if (!empty($endTime)) {
            $where['create_time'] = ['<=', strtotime($endTime) + (60 * 60 * 24 - 1)];
        }

        //用户输入年月日期
        if (!empty($startDate)) {
            $where['create_date'] = ['=', $startDate];
        }
        //查询缴费记录
        $data = $this->fee_order_model->getAll($where, "*", $page, $pageSize);
        if (empty($data)) {
            return $this->jsonend(-1000, '暂无数据');
        }
        foreach ($data as $k => $v) {
            $data[$k]['time'] = $v['create_time'] ?? 0;
            $data[$k]['create_time'] = !empty($v['create_time']) ? date('Y-m-d', $v['create_time']) : '--';
            $data[$k]['pay_time'] = !empty($v['pay_time']) ? date('H:i', $v['pay_time']) : '--';
            $data[$k]['money'] = formatMoney($v['money']);
            $data[$k]['discount_money'] = formatMoney($v['discount_money']);
            $data[$k]['payment_money'] = formatMoney($v['payment_money']);
            $coupon_info = [];
            if (!empty($v['coupon_id'])) {
                $coupon_id = json_decode($v['coupon_id'], true);
                $coupon_info = $this->coupon_model->getCouponList($coupon_id, 1);
            }
            $data[$k]['coupon_info'] = $coupon_info;

            //查询账单
            $bill_id = explode(',',$v['bill_id']);
            foreach ($bill_id as $key=>$value){
                if(empty($value)){
                    unset($bill_id[$key]);
                }
            }
            $customer_bill = $this->customer_code_bill_model->getAll(['bill_id' => ['IN', $bill_id]], 'bill_id,money,cycle_start_time');
            if (!empty($customer_bill)) {
                foreach ($customer_bill as $k1 => $v1) {
                    $customer_bill[$k1]['money'] = formatMoney($v1['money']);
                    $customer_bill[$k1]['cycle_start_time'] = !empty($v1['cycle_start_time']) ? date('m', $v1['cycle_start_time']) : '--';
                }
            }
            $data[$k]['list'] = $customer_bill;

        }

        $count = $count ?? 0;

        $data = groupData($data, 'time');
        $result = array_group_by($data, 'date');
        $data_tmp = [];
        foreach ($result as $k => $v) {
            $data_child_tmp['date'] = $k;
            $data_child_tmp['child'] = $v;
            $data_tmp[] = $data_child_tmp;
        }
        $data = [
            'data' => $data_tmp,
            'page' => [
                'current_page' => $page,
                'pageSize' => $pageSize,
                'total' => $count,
                'total_page' => ceil($count / $pageSize)
            ]
        ];

        return $this->jsonend(1000, '获取列表成功', $data);


    }


}