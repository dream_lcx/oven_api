<?php

namespace app\Controllers\User;

use app\Services\Common\DataService;
use app\Services\Common\EmailService;
use app\Services\Common\ConfigService;
use app\Services\Common\ReturnCodeService;
use app\Services\Common\SmsService;
use app\Services\Common\NotifiyService;
use app\Services\Common\CommonService;
use app\Services\Common\SecureService;
use app\Services\Company\CompanyService;
use app\Services\Contract\ContractService;
use app\Services\Device\DeviceService;
use app\Services\User\UserService;

/**
 * 工单模块API
 */
class Work extends Base
{

    protected $user_model;
    protected $survey_work_order_id;
    protected $order_model;
    protected $equip_model;
    protected $rent_package_model;
    protected $log_model;
    protected $work_model;
    protected $work_log_model;
    protected $auth_model;
    protected $addr_model;
    protected $contract_model;
    protected $contract_additional_model;
    protected $work_eq_model;
    protected $eq_list_model;
    protected $ep_part_model;
    protected $parts_model;
    protected $evaluate_model;
    protected $work_order_problem_model;
    protected $work_order_business_model;
    protected $business_type_model;
    protected $new_install_business_id;
    protected $repair_business_id;
    protected $move_business_id;
    protected $for_core_business_id;
    protected $engineersModel;
    protected $work_parts_model;
    protected $work_additional_service_model;
    protected $for_core_model;
    protected $bind_model;
    protected $contract_eq_model;
    protected $Achievement;
    protected $admin_user_model;
    protected $op_user_model;
    protected $administrative_user_model;
    protected $userService;
    protected $customer_code_model;
    protected $energySavingDataTestModel;
    protected $stoveManagementModel;
    protected $AchievementModel;

    public function initialization($controller_name, $method_name)
    {
        parent::initialization($controller_name, $method_name);
        $this->user_model = $this->loader->model('CustomerModel', $this);
        $this->order_model = $this->loader->model('OrderModel', $this);
        $this->equip_model = $this->loader->model('EquipmentModel', $this);
        $this->rent_package_model = $this->loader->model('RentalPackageModel', $this);
        $this->log_model = $this->loader->model('OrderLogModel', $this);
        $this->work_model = $this->loader->model('WorkOrderModel', $this);
        $this->work_log_model = $this->loader->model('WorkOrderLogModel', $this);
        $this->auth_model = $this->loader->model('CustomerAuthModel', $this);
        $this->addr_model = $this->loader->model('UseraddressModel', $this);
        $this->contract_model = $this->loader->model('ContractModel', $this);
        $this->work_eq_model = $this->loader->model('WorkEquipmentModel', $this);
        $this->eq_list_model = $this->loader->model('EquipmentListsModel', $this);
        $this->ep_part_model = $this->loader->model('EquipmentsPartsModel', $this);
        $this->parts_model = $this->loader->model('PartsModel', $this);
        $this->evaluate_model = $this->loader->model('EvaluateModel', $this);
        $this->work_order_problem_model = $this->loader->model('WorkOrderProblemModel', $this);
        $this->work_order_business_model = $this->loader->model('WorkOrderBusinessModel', $this);
        $this->business_type_model = $this->loader->model('BusinessTypeModel', $this);
        $this->engineersModel = $this->loader->model('EngineersModel', $this);
        $this->work_parts_model = $this->loader->model('WorkPartsModel', $this);
        $this->work_additional_service_model = $this->loader->model('WorkAdditionalServiceModel', $this);
        $this->for_core_model = $this->loader->model('ForCoreRecordModel', $this);
        $this->bind_model = $this->loader->model('CustomerBindEquipmentModel', $this);
        $this->contract_eq_model = $this->loader->model('ContractEquipmentModel', $this);
        $this->Achievement = $this->loader->model('AchievementModel', $this);
        $this->admin_user_model = $this->loader->model('AdminUserModel', $this);
        $this->op_user_model = $this->loader->model('OperationUserModel', $this);
        $this->administrative_user_model = $this->loader->model('AdministrativeUserModel', $this);
        $this->customer_administrative_center_model = $this->loader->model('CustomerAdministrativeCenterModel', $this);
        $this->customer_code_model = $this->loader->model('CustomerCodeModel', $this);
        $this->energySavingDataTestModel = $this->loader->model('EnergySavingDataTestModel', $this);
        $this->stoveManagementModel = $this->loader->model('StoveManagementModel', $this);
        $this->AchievementModel = $this->loader->model('AchievementModel', $this);
        $this->contract_additional_model = $this->loader->model('ContractAdditionalModel', $this);
        //业务类型ID
        $this->new_install_business_id = 3; //新装业务ID
        $this->repair_business_id = 6; //维修业务ID
        $this->move_business_id = 5; //移机业务ID
        $this->for_core_business_id = 2; //换芯
        $this->survey_work_order_id = 7; //勘测
        $this->userService = new UserService();
    }

    /**
     * showdoc
     * @catalog API文档/用户端/工单相关
     * @title 预约新装（华灶弃用Xln）
     * @description 新增新装工单，给用户发送消息通知，给后台发送工单提醒
     * @method POST
     * @url User/Work/newInstall
     * @header token 可选 string token
     * @param appointment_date 必选 string 预约日期
     * @param appointment_time_id 必选 int 预约时间段ID
     * @param contacts 必选 string 联系人
     * @param contact_number 必选 string 联系电话
     * @param order_id 必选 int 订单ID
     * @param remarks 可选 string 备注
     * @return
     * @return_param
     * @remark 这里是备注信息
     * @number 99
     * @author lcx
     * @date 2018-10-18
     */
    public function http_newInstall()
    {
        $business_id = $this->new_install_business_id; //新装
        if (empty($this->parm['appointment_date'] ?? '')) {
            return $this->jsonend(-1001, "请选择预约年月日");
        }
        if (empty($this->parm['appointment_time_id'] ?? '')) {
            return $this->jsonend(-1001, "请选择预约时间范围");
        }
        $time_info = $this->getRangeInfo($this->parm['appointment_time_id']);
        if (!$time_info) {
            $this->jsonend(-1001, "请选择有效的时间范围");
        }
        //处理预约时间
        $time = $this->parm['appointment_date'] . ' ' . $time_info['start'] . '-' . $time_info['end'];
        $time_start = strtotime($this->parm['appointment_date'] . ' ' . $time_info['start']);
        $time_end = strtotime($this->parm['appointment_date'] . ' ' . $time_info['end']);
        if ($time_start < time()) {
            return $this->jsonend(-1002, "预约时间不能小于当前时间");
        }
        if (empty($this->parm['contacts'] ?? '')) {
            return $this->jsonend(-1001, "请输入联系人");
        }
        if (empty($this->parm['contact_number'] ?? '')) {
            return $this->jsonend(-1001, "请输入联系电话");
        }
        if (!isMobile($this->parm['contact_number'])) {
            return $this->jsonend(-1002, "联系电话不合法");
        }
        //查询用户信息
        $user_info = $this->user_model->getOne(['user_id' => $this->user_id], 'user_id,source,username,openid,telphone,user_belong_to_promoter,user_belong_to_channel');
        if (empty($user_info)) {
            return $this->jsonend(-1001, "用户信息不存在");
        }
        //判断地址合法性
        //判断用户是否通过实名认证
//        $auth_info = $this->auth_model->getAll(array('user_id' => $this->user_id), 'status');
//        if (empty($auth_info) || $auth_info['status'] != 2) {
//            return $this->jsonend(-1105, "您还未通过实名认证", $auth_info);
//        }
        //判断是否已经发起过新装工单
        $work_info = $this->work_model->getWorkDetail(array('order_id' => $this->parm['order_id']), array(), 'work_order_status');
        if (!empty($work_info) && $work_info['work_order_status'] != 2) {
            return $this->jsonend(-1104, "该订单已经发起过预约安装,不需要重复发送");
        }
        //查询订单信息
        $order_info = $this->order_model->getOne(array('order_id' => $this->parm['order_id']), '*');
        if (empty($order_info)) {
            return $this->jsonend(-1101, "订单不存在");
        }
        if ($order_info['pay_status'] == 1) {
            return $this->jsonend(-1102, "该订单未支付,请先支付");
        }
        if ($order_info['order_status'] != 3) {
            return $this->jsonend(-1103, "该订单不合法");
        }
        //组装数据
        $data['appointment_time_id'] = $this->parm['appointment_time_id'];
        $data['appointment_time'] = $time;
        $data['appointment_start_time'] = $time_start;
        $data['appointment_end_time'] = $time_end;
        $data['order_number'] = CommonService::createSn('oven_work_order_no');
        $data['contacts'] = $this->parm['contacts'];
        $data['contact_number'] = $this->parm['contact_number'];
        $data['province'] = $order_info['province'];
        $data['city'] = $order_info['city'];
        $data['area'] = $order_info['area'];
        $data['province_code'] = $order_info['province_code'];
        $data['city_code'] = $order_info['city_code'];
        $data['area_code'] = $order_info['area_code'];
        $data['service_address'] = $order_info['address'];
        $data['lng'] = $order_info['lng'];
        $data['lat'] = $order_info['lat'];
        $data['work_order_status'] = 1;
        $data['reception_user'] = 1;
        $data['order_id'] = $this->parm['order_id'];
        $data['equipments_id'] = $order_info['equipments_id'];
        $data['user_id'] = $this->user_id;
        $data['equipment_num'] = $order_info['equipment_num'];
        $data['remarks'] = $this->parm['remarks'] ?? '';
        $data['company_id'] = $order_info['company_id'];
        $data['work_belong_to_client'] = $this->request->client ?? 1;
        $data['work_belong_to_promoter'] = $user_info['user_belong_to_promoter'];
        $data['work_belong_to_channel'] = $user_info['user_belong_to_channel'];
        //获取行政中心信息
        $admin_info = $this->userService->getOwnArea($user_info['user_id'], $user_info['source'], $order_info['province_code'], $order_info['city_code'], $order_info['area_code'], $this->company);
        if (!empty($admin_info)) {
            $data['administrative_id'] = $admin_info['a_id'];
            $data['operation_id'] = $admin_info['operation'];
        }
        $data['order_time'] = time();
        $add_status = false;
        $work_id = '';

        $this->db->begin(function () use ($data, &$add_status, $order_info, &$work_id, $business_id, $user_info) {
            //新增新装合同
            if ($order_info['order_type'] != 3) {
                $contract_id = $this->addContact($order_info, $business_id);
                $data['contract_id'] = $contract_id;
            }
            //添加工单
            $work_id = $this->work_model->add($data);
            //添加工单-业务类型关联表
            $b_data['work_order_id'] = $work_id;
            $b_data['business_id'] = $business_id;
            $this->work_order_business_model->add($b_data);
            //添加工单日志
            $log['work_order_id'] = $work_id;
            $log['create_work_time'] = $data['order_time'];
            $log['operating_time'] = time();
            $log['do_id'] = $this->user_id;
            $log['operating_type'] = 3;
            $log['operating_status'] = 1;
            $log['remarks'] = '【用户端】客户【' . $user_info['username'] . '】【客户ID为:' . $user_info['user_id'] . '】发起预约新装工单';
            $log['open_remarks'] = $this->config->get('work_order_status_desc')[1];
            $this->work_log_model->add($log);
            //非智能主板绑定
            /**
             * 非智能绑定主板更改到上门时绑定
             */
            //$this->bindNonEquipment($work_id);
            $add_status = true;
        });
        if ($add_status) {
            $business = $this->business_type_model->getOne(array('business_type_id' => $business_id, 'company_id' => $this->company), 'name');
            //通知到用户
            $user_notice_config = ConfigService::getTemplateConfig('user_appoint_success', 'user', $this->company);
            if (!empty($user_notice_config) && !empty($user_notice_config['template']) && $user_notice_config['template']['switch']) {
                $template_id = $user_notice_config['template']['wx_template_id'];
                $mp_template_id = $user_notice_config['template']['mp_template_id'];
                $template_data = [
                    'keyword1' => ['value' => $business['name']], // 预约类型
                    'keyword2' => ['value' => $data['contacts']], // 联系人
                    'keyword3' => ['value' => $data['contact_number']], // 联系方式
                    'keyword4' => ['value' => $data['appointment_time']], // 预约时间
                    'keyword5' => ['value' => '您已成功预约新装主板,我们会尽快为您处理!若有疑问可致电' . $this->user_wx_name . '官方客服,客服电话:' . ConfigService::getConfig('company_hotline', false, $this->company)], // 预约内容
                    'keyword6' => ['value' => $data['province'] . $data['city'] . $data['area'] . $data['service_address']] // 服务地址
                ];
                $weapp_template_keyword = '';
                $ap_time = date('Y年m月d日', $data['appointment_start_time']);
                $ap_start = date('H:i', $data['appointment_start_time']);
                $ap_end = date('H:i', $data['appointment_end_time']);
                $mp_template_data = [
                    'first' => ['value' => '恭喜您成功预约新装服务!', 'color' => '#4e4747'],
                    'keyword1' => ['value' => $data['contacts'], 'color' => '#4e4747'],
                    'keyword2' => ['value' => $ap_time, 'color' => '#4e4747'],
                    'keyword3' => ['value' => $ap_start . '-' . $ap_end, 'color' => '#4e4747'],
                    'keyword4' => ['value' => $data['contact_number'], 'color' => '#4e4747'],
                    'remark' => ['value' => '若有疑问可致电' . $this->user_wx_name . '官方客服,客服电话:' . ConfigService::getConfig('company_hotline', false, $this->company), 'color' => '#173177'],
                ];

                $template_tel = $user_info['telphone'];  // 电话
                $template_content = '恭喜!您已成功预约:' . $business['name'] . '服务,我们将尽快为您处理!若有疑问可致电' . $this->user_wx_name . '官方客服,客服电话:' . ConfigService::getConfig('company_hotline', false, $this->company);   // 短信内容
                $template_url = 'pages/user/afterSaleDetail/afterSaleDetail?detail=' . $work_id;
                NotifiyService::sendNotifiyAsync($user_info['openid'], $template_data, $template_id, $template_tel, $template_content, $template_url, $weapp_template_keyword, $mp_template_id, $mp_template_data, 'user', $this->company);//异步发送
            }
            //通知到后台
            $admin_notice_config = ConfigService::getTemplateConfig('user_appoint_success', 'admin', $this->company);
            if (!empty($admin_notice_config)) {
                //站内
                if (!empty($admin_notice_config['websocket']) && $admin_notice_config['websocket']['switch']) {
                    $msg['type'] = 2;
                    $msg['msg'] = '有新工单啦';
                    $this->sendToUid($this->uid, $msg);
                    //通知行政中心
                    $msg['type'] = 1;
                    $msg['msg'] = '有新订单拉';
                    $a_info = $this->userService->getOwnArea($user_info['user_id'], $user_info['source'], $data['province_code'], $data['city_code'], $data['area_code'], $this->company);
                    if (!empty($a_info)) {
                        $this->sendToUid($this->uid, $msg);
                    }
                }
                //发邮件通知
                if (!empty($admin_notice_config['email']) && $admin_notice_config['email']['switch']) {
                    $content = '<h2>' . $business['name'] . '工单通知</h2><br/><div>工单编号:' . $data['order_number'] . '</div><div>客户电话:' . $user_info['telphone'] . '</div><div>安装地址:' . $data['province'] . $data['city'] . $data['area'] . $data['service_address'] . '</div><div>预约安装时间:' . $ap_time . $ap_start . '-' . $ap_end . '</div><div style="color:#808080">请尽快进入管理后台审核工单!</div>';
                    //总后台
                    $admin_email = $this->admin_user_model->getOne(['is_admin' => 1, 'is_delete' => 0], 'notice_email');
                    if (!empty($admin_email) && !empty($admin_email['notice_email'])) {
                        EmailService::sendEmailAsync($admin_email['notice_email'], $content, "有新工单啦", '', '', '', $this->company);
                    }
                    if (!empty($a_info)) {
                        //对应运营中心
                        $op_email = $this->op_user_model->getOne(['o_id' => $a_info['operation'], 'is_delete' => 0, 'is_admin' => 1], 'notice_email');
                        if (!empty($op_email) && !empty($op_email['notice_email'])) {
                            EmailService::sendEmailAsync($op_email['notice_email'], $content, "有新工单啦", '', '', '', $this->company);
                        }
                        //对应合伙人
                        $administrative_email = $this->administrative_user_model->getOne(['a_id' => $a_info['a_id'], 'is_delete' => 0, 'is_admin' => 1], 'notice_email');
                        if (!empty($administrative_email) && !empty($administrative_email['notice_email'])) {
                            EmailService::sendEmailAsync($administrative_email['notice_email'], $content, "有新工单啦", '', '', '', $this->company);
                        }
                    }
                }
            }
            //通知到大数据平台
            $big_data_notice_config = ConfigService::getTemplateConfig('user_appoint_success', 'big_data', $this->company);
            if (!empty($big_data_notice_config) && !empty($big_data_notice_config['websocket']) && $big_data_notice_config['websocket']['switch']) {
                $tel = substr($user_info['telphone'], 0, 3) . '****' . substr($user_info['telphone'], 7, 4);
                DataService::sendToUids(['command' => 'A1', 'data' => ['type' => 2, 'product' => $business['name'], 'user_tel' => $tel, 'lng' => $data['lng'], 'lat' => $data['lat']]]);
            }
            return $this->jsonend(1000, "预约成功");
        }
        return $this->jsonend(-1000, "预约失败");
    }


    //非智能主板新装工单生成时绑定
    public function bindNonEquipment($work_order_id)
    {
        $this->parm['work_order_id'] = $work_order_id;
        if (empty($this->parm['work_order_id'] ?? '')) {
            return $this->jsonend(-1001, "缺少参数工单ID");
        }
        //查询工单信息
        $wo_map['work_order_id'] = $this->parm['work_order_id'];
        $work_order_info = $this->work_model->getWorkDetail($wo_map, array(), '*');
        if (empty($work_order_info)) {
            return $this->jsonend(-1102, "工单不存在");
        }
        $warranty_time['start'] = '';
        $warranty_time['end'] = '';
        //查询产品信息
        $eq_info = $this->equip_model->getDetail(array('equipments_id' => $work_order_info['equipments_id']), 'type,equipments_name,model,equipments_type,warranty_time,mainboard_id');
        //如果不是非智能主板,不走绑定流程
        if ($eq_info['type'] != 2) {
            return true;
        }
        $this->parm['device_no'] = DeviceService::createNonEqNo(); //生成主板编号
        /*		 * * *****主板保修时间，若从走下单流程，保修时间从确认收货时开始计算，若从不走下单流程，直接发起新装工单，保修时间从主板绑定时开始计算****** */
        if ($eq_info['equipments_type'] == 2) {
            $warranty_time['start'] = time();
            $warranty_time['end'] = $warranty_time['start'] + 86400 * $eq_info['warranty_time'];
        }
        //如果是后台添加的工单,没有订单数据
        if (!empty($work_order_info['order_id'])) {
            //判断该用户是否租赁该类型主板
            $or_map['equipments_id'] = $work_order_info['equipments_id'];
            $or_map['user_id'] = $work_order_info['user_id'];
            $or_map['pay_status'] = 2;
            $or_map['order_id'] = $work_order_info['order_id'];
            $order = $this->order_model->getOne($or_map, 'warranty_start_time,warranty_end_time');
            if (empty($order)) {
                return $this->jsonend(-1103, "该用户未租赁该产品，不能绑定");
            }
            if ($eq_info['equipments_type'] == 2) {
                $warranty_time['start'] = $order['warranty_start_time'];
                $warranty_time['end'] = $order['warranty_end_time'];
            }
        }

        //判断主板是否已经存在
        $equipment_info = $this->eq_list_model->findEquipmentLists(array('device_no' => $this->parm['device_no']), 'equipment_id');
        if (!empty($equipment_info)) {
            //判断用户是否重复绑定主板
            $bd_map['equipment_id'] = $equipment_info['equipment_id'];
            $bd_map['is_owner'] = 2;
            $bind_info = $this->bind_model->getOne($bd_map, '*');
            if (!empty($bind_info)) {
                return $this->jsonend(-1104, "该主板已被绑定过，不能重复绑定");
            }
        }
        //判断用户是否已经绑定完
        $bind_map['equipments_id'] = $work_order_info['equipments_id'];
        $bind_map['is_owner'] = 2;
        $bind_map['user_id'] = $work_order_info['user_id'];
        $bind_map['work_order_id'] = $this->parm['work_order_id'];
        $bind_list = $this->bind_model->getAll($bind_map, '*');
        if (!empty($bind_list) && count($bind_list) >= $work_order_info['equipment_num']) {
            return $this->jsonend(-1105, '该用户租赁该类型主板' . $work_order_info['equipment_num'] . '台，已绑定' . count($bind_list) . '台');
        }

        $eq_data['province'] = $work_order_info['province'];
        $eq_data['city'] = $work_order_info['city'];
        $eq_data['area'] = $work_order_info['area'];
        $eq_data['province_code'] = $work_order_info['province_code'];
        $eq_data['city_code'] = $work_order_info['city_code'];
        $eq_data['area_code'] = $work_order_info['area_code'];
        $eq_data['address'] = $work_order_info['service_address'];
        $eq_data['lng'] = $work_order_info['lng'];
        $eq_data['lat'] = $work_order_info['lat'];
        $eq_data['contact_number'] = $work_order_info['contact_number'];
        $eq_data['contact'] = $work_order_info['contacts'];
        $eq_data['alias_name'] = $eq_info['equipments_name'];
        $eq_data['company_id'] = $work_order_info['company_id'];
        if (empty($equipment_info)) {
            //新增主板记录
            $eq_data['equipments_id'] = $work_order_info['equipments_id'];
            $eq_data['device_no'] = $this->parm['device_no'];
            $eq_data['create_time'] = time();
            $eq_data['status'] = 1;
            $eq_data['contract_id'] = $work_order_info['contract_id'];

            $equipment_id = $this->eq_list_model->insertEquipmentLists($eq_data);
        } else {
            //修改主板信息
            $eq_data['contract_id'] = $work_order_info['contract_id'];
            $equipment_id = $equipment_info['equipment_id'];
            $eq_data['equipments_id'] = $work_order_info['equipments_id'];
            $this->eq_list_model->updateEquipmentLists($eq_data, array('equipment_id' => $equipment_id));
        }
        //添加绑定记录
        $bind_data['equipments_id'] = $work_order_info['equipments_id'];
        $bind_data['work_order_id'] = $this->parm['work_order_id'];
        $bind_data['equipment_id'] = $equipment_id;
        $bind_data['user_id'] = $work_order_info['user_id'];
        $bind_data['create_time'] = time();
        $bind_data['is_owner'] = 2;
        $this->bind_model->addCustomerBindEquipment($bind_data);

        //添加合同主板关联记录
        $contract_eq_data['contract_id'] = $work_order_info['contract_id'];
        $contract_eq_data['equipments_id'] = $work_order_info['equipments_id'];
        $contract_eq_data['equipment_id'] = $equipment_id;
        $contract_eq_data['equipments_address'] = $work_order_info['province'] . $work_order_info['city'] . $work_order_info['area'] . $work_order_info['service_address'];
        $contract_eq_data['addtime'] = time();
        $this->contract_eq_model->add($contract_eq_data);

        //添加工单主板关联表
        $work_eq_data['equipment_id'] = $equipment_id;
        $work_eq_data['work_order_id'] = $this->parm['work_order_id'];
        $work_eq_data['create_time'] = time();
        $this->work_eq_model->add($work_eq_data);

        //已判定完修改相应状态
        $bind_map['equipments_id'] = $work_order_info['equipments_id'];
        $bind_map['user_id'] = $work_order_info['user_id'];
        $bind_map['is_owner'] = 2;
        $bind_map['work_order_id'] = $this->parm['work_order_id'];
        $bind_lists = $this->bind_model->getAll($bind_map, '*');
        if (count($bind_lists) >= $work_order_info['equipment_num']) {
            $this->work_model->editWork(array('work_order_id' => $this->parm['work_order_id']), array('is_bind_equipment' => 1));
            //合同状态
            $this->contract_model->save(array('contract_id' => $work_order_info['contract_id']), array('status' => 3));
            //添加合同日志
            $this->addContractLog($work_order_info['contract_id'], 3, "工单已完成,合同状态修改为待客户签字");

            //新增----------2019年1月22日11:00:46----------------ligang
            //绑定主板完成后，更改合同关系表主板数
            $contract = $this->contract_model->getOne(['contract_id' => $work_order_info['contract_id']], 'contract_no');
            $this->Achievement->table = 'contract_rank';
            $contract_rank = $this->Achievement->findData(['contract_no' => $contract['contract_no']], 'id');
            $this->Achievement->updateData(['id' => $contract_rank['id']], ['device_number' => $work_order_info['equipment_num']]);
        }
    }

    //生成合同
    public function addContact($info, $business_id)
    {
        // 获取用户信息
        $user_info = $this->user_model->getOne(['user_id' => $this->user_id], 'user_id,source,username,openid,telphone');
        //获取配置
        $customer_administrative_center = $this->customer_administrative_center_model->getOne(['user_id' => $user_info['user_id']], 'operation_id');
        $operation_id = empty($customer_administrative_center) ? 0 : $customer_administrative_center['operation_id'];
        $data['contact_type'] = ConfigService::getConfig('sign_way', false, $this->company, $operation_id); //2电子合同-电子签名 1电子合同-确认同意 3纸质合同
        $data['user_id'] = $info['user_id'];
        $addr = $this->userService->getOwnArea($user_info['user_id'], $user_info['source'], $info['province_code'], $info['city_code'], $info['area_code'], $this->company);
        $data['administrative_id'] = $addr['a_id'];
        $data['operation_id'] = $addr['operation'];
        $data['province'] = $info['province'];
        $data['city'] = $info['city'];
        $data['area'] = $info['area'];
        $data['province_code'] = $info['province_code'];
        $data['city_code'] = $info['city_code'];
        $data['area_code'] = $info['area_code'];
        $data['address'] = $info['address'];
        $data['business_id'] = $business_id;
        $data['contract_no'] = CommonService::createSn('cloud_contract_no');
        $data['contact_person'] = $info['contact'];
        $data['phone'] = $info['contact_tel'];
        $data['contract_deposit'] = $info['total_deposit'];
        $data['contract_money'] = $info['order_actual_money'];
//        $data['renew_money'] = $info['order_actual_money'] - $info['total_deposit'];
        $data['add_time'] = time();
        $data['company_id'] = $info['company_id'];

        //获取合同模板
        $template_info = ContractService::getTemplate($info['company_id'], 3, $info['order_type'], $data['operation_id']);
        if (!empty($template_info)) {
            $data['view_layout'] = $template_info['view_layout'];
        }
        //		if ($info['order_type'] == 2){
//		    //购买直接为生效中
//            $data['status'] = 4;
//            $data['real_exire_date'] = time()+86400*365*100;
//            $data['exire_date'] = time()+86400*365*100;
//            $data['effect_time'] = time();
//        }
        $res = $this->contract_model->add($data);
        $this->addContractLog($res, 1, "用户【" . $user_info['username'] . "】【用户ID:" . $user_info['user_id'] . "】发起新装工单时新增新装合同");
        return $res;
    }

    /**
     * showdoc
     * @catalog API文档/用户端/工单相关
     * @title 提交售后（暂时弃用）
     * @description
     * @method POST
     * @url User/Work/subWorkOrder_zanqi
     * @param code_id 必选 int 户号ID
     * @param business_id 必选 int 业务类型ID
     * @param appointment_date 必选 string 预约日期
     * @param appointment_time_id 必选 int 预约时间段ID
     * @param contacts 必选 string 联系人
     * @param contact_number 必选 string 联系电话
     * @param order_id 必选 int 订单ID
     * @param equipment_id 可选 int 主板ID
     * @param remarks 可选 string 备注
     * @param problem_key 可选 int 维修主要问题 如果是维修必传
     * @param problem 可选 string 维修问题说明
     * @param picture 可选 array 图片说明
     * @param move_province_code 可选 string 移机省份code,移机必传
     * @param move_province_code 可选 string 移机城市code，移机必传
     * @param move_province_code 可选 string 移机区域code，移机必传
     * @param move_address 可选 string 移机地址，移机必传
     * @param move_lng 可选 string 移机经度 ，移机必传
     * @param move_lat 可选 string 移机维度 ，移机必传
     * @return
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */

    public function http_subWorkOrder_zanqi()
    {
        //处理业务类型
        $business_id = $this->parm['business_id'] ?? '';
        if (!is_array($business_id) || count($business_id) == 0) {
            return $this->jsonend(-1003, "请选择业务类型");
        }
        if (empty($this->parm['code_id'] ?? '')) {
            return $this->jsonend(-1003, "缺少户号参数");
        }
//        if (empty($this->parm['picture']) && in_array(6, $this->parm['business_id'])) {
//            return $this->jsonend(-1003, "至少上传一张照片");
//        }
        /*if (empty($this->parm['appointment_date'] ?? '')) {
            return $this->jsonend(-1003, "请选择预约年月日");
        }
        if (empty($this->parm['appointment_time_id'] ?? '')) {
            return $this->jsonend(-1003, "请选择预约时间范围");
        }
        $time_info = $this->getRangeInfo($this->parm['appointment_time_id']);
        if (!$time_info) {
            $this->jsonend(-1003, "请选择有效的时间范围");
        }

        // 处理预约时间
        $time = $this->parm['appointment_date'] . ' ' . $time_info['start'] . '-' . $time_info['end'];
        $time_start = strtotime($this->parm['appointment_date'] . ' ' . $time_info['start']);
        $time_end = strtotime($this->parm['appointment_date'] . ' ' . $time_info['end']);
        if ($time_start < time()) {
            return $this->jsonend(-1003, "预约时间不能小于当前时间");
        }*/

        if (empty($this->parm['contacts'] ?? '')) {
            return $this->jsonend(-1003, "请输入联系人");
        }
        if (empty($this->parm['contact_number'] ?? '')) {
            return $this->jsonend(-1003, "请输入联系电话");
        }
        if (!isMobile($this->parm['contact_number'])) {
            return $this->jsonend(-1003, "联系电话不合法");
        }
        /* // 判断用户是否通过实名认证
         $auth_info = $this->auth_model->getAll(array('user_id' => $this->user_id), '*');
         if (empty($auth_info) || $auth_info['status'] != 2) {
             return $this->jsonend(-1003, "您还未通过实名认证", $auth_info);
         }*/

        $res = $this->customer_code_model->getCodeContractInfo('', $this->parm['code_id']);
        if ($res['code'] < 0) {
            return $this->jsonend($res['code'], $res['msg']);
        }
        $eq_contract_info = $res['data'];
        // 查询主板信息
        if (!empty($this->parm['equipment_id'])) {
            $eq_info = $this->eq_list_model->findEquipmentLists(array('equipment_id' => $this->parm['equipment_id']));
            if (empty($eq_info)) {
                return $this->jsonend(-1003, "该主板未绑定");
            }
        }
        //不同业务类型处理start
        // 判断是否填写备注
        if (!empty($this->parm['remarks'])) {
            // 检测内容是否合法
            $res = SecureService::checkContent($this->parm['remarks'], 'user', $this->company);
            if ($res['errcode'] != 0 && $res['errmsg'] != 'ok') {
                return $this->jsonend(-1000, '内容不合法', $res);
            }
        }
        //维修
        if (in_array($this->repair_business_id, $business_id)) {
            if (empty($this->parm['problem_key'] ?? '')) {
                return $this->jsonend(-1001, "请选择维修主要问题");
            }
            $data['problem_key'] = json_encode($this->parm['problem_key']);
            if (empty($this->parm['problem'] ?? '')) {
                return $this->jsonend(-1001, "请具体描述存在问题");
            }
            $data['problem'] = $this->parm['problem'];
            if (!empty($this->parm['picture'] ?? '')) {
                $data['picture'] = json_encode($this->parm['picture']);
            }
            // 检测内容是否合法
            $res = SecureService::checkContent($this->parm['problem'], 'user', $this->company);
            if ($res['errcode'] != 0 && $res['errmsg'] != 'ok') {
                return $this->jsonend(-1000, '内容不合法', $res);
            }
        }
        // 获取行政中心信息
        $admin_info = $this->userService->getOwnArea($this->user_id, '', $eq_contract_info['province_code'], $eq_contract_info['city_code'], $eq_contract_info['area_code'], $this->company);
        if (!empty($admin_info)) {
            $data['administrative_id'] = $admin_info['a_id'];
            $data['operation_id'] = $admin_info['operation'];
        }
        // 获取用户信息
        $user_info = $this->user_model->getOne(['user_id' => $this->user_id], 'user_id,source,username,realname,openid,telphone,user_belong_to_promoter,user_belong_to_channel');
        if (empty($user_info)) {
            return $this->jsonend(-1001, "用户信息不存在");
        }
        //移机
        $is_replacement = false;
        $contact_log = [];
        $contact_data = [];
        $move_area_info = [];
        if (in_array($this->move_business_id, $business_id)) {
            $is_replacement = true;
            if (empty($this->parm['move_province_code'] ?? '') || empty($this->parm['move_city_code'] ?? '') || empty($this->parm['move_area_code'] ?? '')) {
                return $this->jsonend(-1001, "请选择移机区域");
            }
            if (empty($this->parm['move_address'] ?? '')) {
                return $this->jsonend(-1001, "请选择移机详细地址");
            }
//            if (empty($this->parm['move_lng'] ?? '') || empty($this->parm['move_lat'] ?? '')) {
//                return $this->jsonend(-1001, "缺少参数，移机地址经纬度");
//            }
            $province = $this->getAreaInfo($this->parm['move_province_code']);
            $city = $this->getAreaInfo($this->parm['move_city_code']);
            $area = $this->getAreaInfo($this->parm['move_area_code']);
            if (empty($province) || empty($city) || empty($area)) {
                return $this->jsonend(-1001, "选择移机区域信息错误");
            }

            //移机区域信息
            $move_area_info = $this->userService->getOwnArea($this->user_id, '', $this->parm['move_province_code'], $this->parm['move_city_code'], $this->parm['move_area_code'], $this->company);
            if (empty($move_area_info)) {
                return $this->jsonend(-1000, $province['area_name'] . $city['area_name'] . $area['area_name'] . ' 地区暂无开通业务');
            }

            $data['move_province_code'] = $this->parm['move_province_code'];
            $data['move_city_code'] = $this->parm['move_city_code'];
            $data['move_area_code'] = $this->parm['move_area_code'];
            $data['move_province'] = $province['area_name'];
            $data['move_city'] = $city['area_name'];
            $data['move_area'] = $area['area_name'];
            $data['move_address'] = $this->parm['move_address'];
            $data['move_lng'] = $this->parm['move_lng'];
            $data['move_lat'] = $this->parm['move_lat'];

            //-----------------------------移机begin-----------------------------
            $data['is_new_word_order'] = 1;//新建移机装工单
            $data['replacement_type'] = 2;//移机拆

            //获取原合同
            $this->Achievement->table = 'contract';
            $contract = $this->Achievement->findData(['contract_id' => $eq_contract_info['contract_id']]);
            //新建移机合同
            $contact_data['pid'] = $eq_contract_info['contract_id'];
            $contact_data['contact_type'] = $contract['contact_type'];
            $contact_data['receipt'] = $contract['receipt'];
            $contact_data['contract_deposit'] = $contract['contract_deposit'];
            $contact_data['installed_time'] = $contract['installed_time'];
            $contact_data['is_special'] = $contract['is_special'];
            $contact_data['remark'] = $contract['remark'];
            $contact_data['user_id'] = $this->user_id;
            $contact_data['administrative_id'] = $move_area_info['a_id'];
            $contact_data['operation_id'] = $move_area_info['operation'];
            $contact_data['province'] = $province['area_name'];
            $contact_data['city'] = $city['area_name'];
            $contact_data['area'] = $area['area_name'];
            $contact_data['province_code'] = $this->parm['move_province_code'];
            $contact_data['city_code'] = $this->parm['move_city_code'];
            $contact_data['area_code'] = $this->parm['move_area_code'];
            $contact_data['address'] = $this->parm['move_address'];
            $contact_data['contract_no'] = CommonService::createSn('cloud_contract_no');
            $contact_data['contact_person'] = $this->parm['contacts'];
            $contact_data['phone'] = $this->parm['contact_number'];
            $contact_data['business_id'] = 5;
            $contact_data['add_time'] = time();
            $contact_data['contract_money'] = $contract['contract_money'];
            $contact_data['company_id'] = $this->request->company;
            $contact_data['status'] = 3;
            $contact_data['related_name'] = $contract['related_name'];
            $contact_data['related_tel'] = $contract['related_tel'];
            $contact_data['related_id_card'] = $contract['related_id_card'];
            $contact_data['quota_money'] = $contract['quota_money'];
            $contact_data['gas_fee'] = $contract['gas_fee'];
            $contact_data['effect_time'] = $contract['effect_time'];
            $contact_data['contract_party'] = $contract['contract_party'];
            $contact_data['now_date'] = $contract['now_date'];
            //获取配置
            $customer_administrative_center = $this->customer_administrative_center_model->getOne(['user_id' => $contact_data['user_id']], 'operation_id');
            $operation_id = empty($customer_administrative_center) ? 0 : $customer_administrative_center['operation_id'];
            $contact_data['contact_type'] = ConfigService::getConfig('sign_way', false, $this->request->company, $operation_id); //2电子合同-电子签名 1电子合同-确认同意 3纸质合同
            //创建合同日志
            $contact_log['contract_id'] = $eq_contract_info['contract_id'];
            $contact_log['do_id'] = 0;
            $contact_log['do_type'] = 1;
            $contact_log['terminal_type'] = 4;
            $contact_log['do_time'] = time();
            $contact_log['remark'] = '【用户端】客户【' . $user_info['realname'] . '】【客户ID为:' . $this->user_id . '】新增移机工单同时生成合同';
            //-----------------------------移机end-------------------------------
        }

        //不同业务类型处理end
        // 组装数据
        /*$data['appointment_time'] = $time;
        $data['appointment_start_time'] = $time_start;
        $data['appointment_end_time'] = $time_end;
        $data['appointment_time_id'] = $this->parm['appointment_time_id'];*/
        $data['order_number'] = CommonService::createSn('oven_work_order_no');
        $data['contacts'] = $this->parm['contacts'];
        $data['contact_number'] = $this->parm['contact_number'];
        $data['province'] = $eq_contract_info['province'];
        $data['city'] = $eq_contract_info['city'];
        $data['area'] = $eq_contract_info['area'];
        $data['province_code'] = $eq_contract_info['province_code'];
        $data['city_code'] = $eq_contract_info['city_code'];
        $data['area_code'] = $eq_contract_info['area_code'];
        $data['service_address'] = $eq_contract_info['address'];
        $data['lng'] = $eq_contract_info['lng'];
        $data['lat'] = $eq_contract_info['lat'];
        $data['work_order_status'] = 1;
        $data['reception_user'] = 1;
        $data['equipments_id'] = $eq_contract_info['equipments_id'];
        $data['user_id'] = $this->user_id;
        $data['equipment_num'] = 1;
        $data['remarks'] = empty($this->parm['remarks'] ?? '') ? '' : $this->parm['remarks'];
        $data['company_id'] = $eq_contract_info['company_id'];
        $data['work_belong_to_client'] = $this->request->client;
        $data['work_belong_to_promoter'] = $user_info['user_belong_to_promoter'];
        $data['work_belong_to_channel'] = $user_info['user_belong_to_channel'];
        $data['order_time'] = time();


        $add_status = false;
        $work_id = '';
        $this->db->begin(function () use ($data, $user_info, $contact_data, $eq_contract_info, $contact_log, $is_replacement, &$add_status, &$work_id, $business_id, $move_area_info) {
            //移机操作
            if ($is_replacement) {

                //添加合同
                $this->Achievement->table = 'contract';
                $contact_id = $this->Achievement->insertData($contact_data);
                $data['contract_id'] = $contact_id;
                $new_work = $data;
                $new_work['is_new_word_order'] = 1;
                $new_work['is_bind_equipment'] = 1;
                $new_work['replacement_type'] = 3;//移机装
                $new_work['administrative_id'] = $move_area_info['a_id'];
                $new_work['operation_id'] = $move_area_info['operation'];
                $new_work['order_number'] = CommonService::createSn('oven_work_order_no');;
                //添加合同日志
                $this->Achievement->table = 'contract_log';
                $this->Achievement->insertData($contact_log);
                //移机装工单
                $new_work_id = $this->work_model->add($new_work);
                //移机装工单类型关系
                $this->work_order_business_model->add(['work_order_id' => $new_work_id, 'business_id' => 5]);
                //添加工单主板关联表
                $work_eq_data['equipment_id'] = $this->parm['equipment_id'] ?? 0;
                $work_eq_data['work_order_id'] = $new_work_id;
                $work_eq_data['customer_code'] = $eq_contract_info['code'];
                $work_eq_data['create_time'] = time();
                $this->work_eq_model->add($work_eq_data);
                //工单日志
                $log = [];
                $log['work_order_id'] = $new_work_id;
                $log['create_work_time'] = $data['order_time'];
                $log['operating_time'] = time();
                $log['do_id'] = $this->user_id;
                $log['operating_type'] = 3;
                $log['operating_status'] = 1;
                $log['remarks'] = '【用户端】客户【' . $user_info['username'] . '】【客户ID为:' . $user_info['user_id'] . '】发起新工单';
                $log['open_remarks'] = $this->config->get('work_order_status_desc')[1];
                $this->work_log_model->add($log);
                //新增合同主板关系
                $contract_eq_data['contract_id'] = $contact_id;
                $contract_eq_data['equipment_id'] = $this->parm['equipment_id'] ?? 0;
                $contract_eq_data['addtime'] = time();
                $this->contract_eq_model->add($contract_eq_data);
                //客户与区域的关系绑定
                $this->userService->addUserCenter($this->user_id, $this->parm['move_province_code'], $this->parm['move_city_code'], $this->parm['move_area_code'], $this->company);


            }
            // 1-- 添加工单
            $work_id = $this->work_model->add($data);
            //添加工单-业务类型关联表
            foreach ($business_id as $k => $v) {
                $b_data['work_order_id'] = $work_id;
                $b_data['business_id'] = $v;
                $this->work_order_business_model->add($b_data);
            }
            //添加工单主板关联表
            $work_eq_data['equipment_id'] = $this->parm['equipment_id'] ?? 0;
            $work_eq_data['work_order_id'] = $work_id;
            $work_eq_data['customer_code'] = $eq_contract_info['code'];
            $work_eq_data['create_time'] = time();
            $this->work_eq_model->add($work_eq_data);
            // 2-- 添加工单日志
            $log = [];
            $log['work_order_id'] = $work_id;
            $log['create_work_time'] = $data['order_time'];
            $log['operating_time'] = time();
            $log['do_id'] = $this->user_id;
            $log['operating_type'] = 3;
            $log['operating_status'] = 1;
            $log['remarks'] = '【用户端】客户【' . $user_info['username'] . '】【客户ID为:' . $user_info['user_id'] . '】发起新工单';
            $log['open_remarks'] = $this->config->get('work_order_status_desc')[1];
            $this->work_log_model->add($log);
            $add_status = true;
        });
        if ($add_status) {
            //获取业务类型名称
            $business_name_arr = [];
            $business = $this->business_type_model->getOne(array('business_type_id' => $v, 'company_id' => $this->company), 'name');
            if (!empty($business)) {

            }
            //通知后台
            $msg['type'] = 2;
            $msg['msg'] = '有新工单啦';
            $this->sendToUid($this->uid, $msg);
            //大数据平台
            $tel = substr($user_info['telphone'], 0, 3) . '****' . substr($user_info['telphone'], 7, 4);
            DataService::sendToUids(['command' => 'A1', 'data' => ['type' => 2, 'product' => $business['name'], 'user_tel' => $tel]]);
            //通知行政中心
            $msg['type'] = 1;
            $msg['msg'] = '有新订单拉';
            $a_info = $this->userService->getOwnArea($user_info['user_id'], $user_info['source'], $data['province_code'], $data['city_code'], $data['area_code'], $this->company);
            if (!empty($a_info)) {
                $this->sendToUid($this->uid, $msg);
            }

            //发邮件通知
            $content = '<h2>' . $business['name'] . '工单通知</h2><br/><div>工单编号:' . $data['order_number'] . '</div><div>客户电话:' . $user_info['telphone'] . '</div><div>安装地址:' . $data['province'] . $data['city'] . $data['area'] . $data['service_address'] . '</div><div style="color:#808080">请尽快进入管理后台审核工单!</div>';
            //总后台
            $admin_email = $this->admin_user_model->getOne(['is_admin' => 1, 'is_delete' => 0], 'notice_email');
            if (!empty($admin_email) && !empty($admin_email['notice_email'])) {
                EmailService::sendEmailAsync($admin_email['notice_email'], $content, "有新工单啦", '', '', '', $this->company);
            }
            if (!empty($a_info)) {
                //对应运营中心
                $op_email = $this->op_user_model->getOne(['o_id' => $a_info['operation'], 'is_delete' => 0, 'is_admin' => 1], 'notice_email');
                if (!empty($op_email) && !empty($op_email['notice_email'])) {
                    EmailService::sendEmailAsync($op_email['notice_email'], $content, "有新工单啦", '', '', '', $this->company);
                }
                //对应合伙人
                $administrative_email = $this->administrative_user_model->getOne(['a_id' => $a_info['a_id'], 'is_delete' => 0, 'is_admin' => 1], 'notice_email');
                if (!empty($administrative_email) && !empty($administrative_email['notice_email'])) {
                    EmailService::sendEmailAsync($administrative_email['notice_email'], $content, "有新工单啦", '', '', '', $this->company);
                }
            }


            return $this->jsonend(1000, "提交成功");
        }
        return $this->jsonend(-1000, "提交失败");
    }

    /**
     * showdoc
     * @catalog API文档/用户端/工单相关
     * @title 提交售后
     * @description
     * @method POST
     * @url User/Work/subWorkOrder
     * @param contract_id 必选 int 合同ID
     * @param business_id 必选 int 业务类型ID
     * @param remarks 可选 int 备注
     * @param stove_number 可选 int 灶眼数量（非移机传入）
     * @param problem_key 可选 array 故障部位（维修时传入）入参格式[1,2,3]
     * @param problem 可选 string 问题描述（非移机传入）
     * @param picture 可选 array 售后上传图片（非移机传入）入参格式[url,url,url]
     * @param move_province 可选 string 移机省（移机传入）
     * @param move_city 可选 string 移机市（移机传入）
     * @param move_area 可选 string 移机区（移机传入）
     * @param move_address 可选 string 移机详细地址（移机传入）
     * @param move_province_code 可选 string 移机省CODE（移机传入）
     * @param move_city_code 可选 string 移机市CODE（移机传入）
     * @param move_area_code 可选 string 移机区CODE（移机传入）
     * @return
     * @remark
     * @number 0
     * @author xln
     * @date 2018-10-18
     */
    public function http_subWorkOrder()
    {
        if (empty($this->parm['contract_id'])) return $this->jsonend(ReturnCodeService::FAIL, '参数缺失：合同ID');
        if (empty($this->parm['business_id'])) return $this->jsonend(ReturnCodeService::FAIL, '参数缺失：服务类型');
        $this->parm['business_id'] = $this->parm['business_id'][0];
        $this->parm['stove_number'] = $this->parm['stove_number'] ?? 0;
        //查询户号实际改造数量
        $customer_data = $this->customer_code_model->getOne(['contract_id' => $this->parm['contract_id']], 'actual_transformation_num');
        if (empty($customer_data)) return $this->jsonend(ReturnCodeService::FAIL, '暂无户号信息');
        if ($customer_data['actual_transformation_num'] <= 0) return $this->jsonend(ReturnCodeService::FAIL, '户号还未安装灶眼');
        $business_name = "拆机";
        $move_area_info = [];
        //查询合同状态
        $field = "user_id,contract_id,operation_id,administrative_id,status,first_party_assign_contact_name,first_party_assign_contact_phone,province,city,area,province_code,city_code,area_code,address";
        $contract_data = $this->contract_model->getOne(['contract_id' => $this->parm['contract_id']], $field);
        if (empty($contract_data)) return $this->jsonend(ReturnCodeService::FAIL, '暂无合同信息');
        if (!in_array($contract_data['status'], [11, 12])) return $this->jsonend(ReturnCodeService::FAIL, '合同状态不正确');

        //不是移机判断灶眼数量
        if (!in_array($this->parm['business_id'], [4, 5])) {
            if (empty($this->parm['stove_number'])) return $this->jsonend(ReturnCodeService::FAIL, '参数缺失：灶眼数量');
            if ($this->parm['stove_number'] > $customer_data['actual_transformation_num']) return $this->jsonend(ReturnCodeService::FAIL, '灶眼数量不能大于实际改造数量');
        }
        //维修
        $work_data = [];
        if ($this->parm['business_id'] == 6) {
            $business_name = "维修";
            if (empty($this->parm['problem_key'])) return $this->jsonend(ReturnCodeService::FAIL, '参数缺失：故障部位');
            if (empty($this->parm['problem'])) return $this->jsonend(ReturnCodeService::FAIL, '参数缺失：问题描述');
            $work_data['picture'] = empty($this->parm['picture']) ? "" : json_encode($this->parm['picture']); //售后上传的图片
            $work_data['problem_key'] = json_encode($this->parm['problem_key']); //主要问题ID
            $work_data['problem'] = $this->parm['problem']; //问题描述
        }

        //移机
        if ($this->parm['business_id'] == 5) {
            $business_name = "移机";
            if (empty($this->parm['move_province_code']) || empty($this->parm['move_city_code']) || empty($this->parm['move_area_code']) || empty($this->parm['move_address'])) {
                return $this->jsonend(ReturnCodeService::FAIL, '参数缺失：移机地址');
            }
//            //判断是否有移机补充合同
//            $contract_additional_data = $this->contract_additional_model->getOne(['contract_id'=>$this->parm['contract_id'],'status'=>1],'*');
//            if (empty($contract_additional_data)) return $this->jsonend(ReturnCodeService::FAIL,'请联系后台人员上传移交合同');
            //移机区域信息
            $move_area_info = $this->userService->getOwnArea($this->user_id, '', $this->parm['move_province_code'], $this->parm['move_city_code'], $this->parm['move_area_code'], $this->company);
            if (empty($move_area_info)) return $this->jsonend(-1000, '地区暂无开通业务');
            //获取地址名称
            $province = $this->getAreaInfo($this->parm['move_province_code']);
            $city = $this->getAreaInfo($this->parm['move_city_code']);
            $area = $this->getAreaInfo($this->parm['move_area_code']);
            //移机新装位置
            var_dump($customer_data);
            $work_data['move_province'] = $province['area_name'];
            $work_data['stove_number'] = $customer_data['actual_transformation_num'];
            $work_data['actual_stove_number'] = $customer_data['actual_transformation_num'];
            $work_data['move_city'] = $city['area_name'];
            $work_data['move_area'] = $area['area_name'];
            $work_data['move_address'] = $this->parm['move_address'];
            $work_data['move_province_code'] = $this->parm['move_province_code'];
            $work_data['move_city_code'] = $this->parm['move_city_code'];
            $work_data['move_area_code'] = $this->parm['move_area_code'];
            $work_data['replacement_type'] = 2; //移机拆
            $work_data['is_new_word_order'] = 1; //是否新建移机工单，1：是，2：否。移机专用
            $work_order_data['replacement_type'] = 2; //移机类型，1：移机拆/装，2：移机拆，3：移机装。移机专用
        }
        //拆机
        if ($this->parm['business_id'] == 4) {
            if (empty($this->parm['demolition_type'])) return $this->jsonend(ReturnCodeService::FAIL, '参数缺失：拆机类型');
            $work_data['demolition_type'] = $this->parm['demolition_type'] ?? 0;
            //拆部分。验证拆机眼数
            if ($customer_data['actual_transformation_num'] <= $this->parm['stove_number'] && $this->parm['demolition_type'] == 1) {
                return $this->jsonend(ReturnCodeService::FAIL, '安装眼数共:' . $customer_data['actual_transformation_num']) . '不能选择部分拆机';
            }
            //拆全部。取户号下安装的炉具数量
            if ($this->parm['demolition_type'] == 2) $this->parm['stove_number'] = $customer_data['actual_transformation_num'];
            $work_data['stove_number'] = $this->parm['stove_number'];
            $work_data['actual_stove_number'] = $this->parm['stove_number'];
        }

        // 获取用户信息
        $user_info = $this->user_model->getOne(['user_id' => $this->user_id], 'user_id,source,username,realname,openid,telphone,user_belong_to_promoter,user_belong_to_channel');
        if (empty($user_info)) return $this->jsonend(-1001, "用户信息不存在");

        //组装工单表数据
        $work_data['stove_number'] = empty($this->parm['stove_number']) ? 0 : $this->parm['stove_number']; //灶具数量
        $work_data['company_id'] = $this->request->company; //灶具数量
        $work_data['order_number'] = CommonService::createSn('oven_work_order_no'); //工单号
        $work_data['work_order_status'] = 1; //工单审核 待审核
        $work_data['user_id'] = $contract_data['user_id']; //用户编码
        $work_data['contract_id'] = $contract_data['contract_id']; //合同ID
        $work_data['operation_id'] = $contract_data['operation_id']; //运营合伙人ID
        $work_data['administrative_id'] = $contract_data['administrative_id']; //城市合伙人ID
        $work_data['order_time'] = time();
        $work_data['reception_uid'] = $this->user_id; //对应端操作者id
        $work_data['province'] = $contract_data['province'];
        $work_data['city'] = $contract_data['city'];
        $work_data['area'] = $contract_data['area'];
        $work_data['province_code'] = $contract_data['province_code'];
        $work_data['city_code'] = $contract_data['city_code'];
        $work_data['area_code'] = $contract_data['area_code'];
        $work_data['service_address'] = $contract_data['address'];
        $work_data['contact_number'] = $contract_data['first_party_assign_contact_phone'];
        $work_data['contacts'] = $contract_data['first_party_assign_contact_name'];
        $work_data['remarks'] = $this->parm['remarks'] ?? '';

        $add_status = false;
        $this->db->begin(function () use (&$add_status, &$work_data, $customer_data, $user_info, $business_name, $move_area_info) {
            //写入工单
            $work_order_id = $this->work_model->add($work_data);
            //移机装   写入工单
            if ($this->parm['business_id'] == 5) {
                $work_data['is_new_word_order'] = 1;
                $work_data['is_bind_equipment'] = 1;
                $work_data['replacement_type'] = 3; //移机装
                $work_data['demolition_work_order_id'] = $work_order_id; //移机拆工单ID
                $work_data['administrative_id'] = $move_area_info['a_id'];
                $work_data['operation_id'] = $move_area_info['operation'];
                $work_data['order_number'] = CommonService::createSn('oven_work_order_no');
                $work_data['stove_number'] = $customer_data['actual_transformation_num'];
                $work_data['actual_stove_number'] = $customer_data['actual_transformation_num'];
                $relocation_work_order_id = $this->work_model->add($work_data);
                //写入工单类型表
                $order_business_data = ['work_order_id' => $relocation_work_order_id, 'business_id' => $this->parm['business_id']];
                $this->work_order_business_model->add($order_business_data);
                //写入工单日志
                $log['work_order_id'] = $relocation_work_order_id;
                $log['create_work_time'] = $work_data['order_time'];
                $log['operating_time'] = time();
                $log['do_id'] = $this->user_id;
                $log['operating_type'] = 3;
                $log['operating_status'] = 1;
                $log['remarks'] = "【用户端】客户【" . $user_info['username'] . "】【客户ID为:" . $user_info['user_id'] . "】发起" . $business_name . "单";
                $log['open_remarks'] = $this->config->get('work_order_status_desc')[1];
                $this->work_log_model->add($log);
            }
            //写入工单类型表
            $order_business_data = ['work_order_id' => $work_order_id, 'business_id' => $this->parm['business_id']];
            $this->work_order_business_model->add($order_business_data);
            //写入工单日志
            $log['work_order_id'] = $work_order_id;
            $log['create_work_time'] = $work_data['order_time'];
            $log['operating_time'] = time();
            $log['do_id'] = $this->user_id;
            $log['operating_type'] = 3;
            $log['operating_status'] = 1;
            $log['remarks'] = "【用户端】客户【" . $user_info['username'] . "】【客户ID为:" . $user_info['user_id'] . "】发起" . $business_name . "单";
            $log['open_remarks'] = $this->config->get('work_order_status_desc')[1];
            $this->work_log_model->add($log);
            $add_status = true;
        }, function ($e, $msg) {
            return $this->jsonend(-1000, '出错啦~~，错误原因：' . $e->error ?? $msg);
        });
        if ($add_status) {
            //获取业务类型名称
            $business = $this->business_type_model->getOne(array('business_type_id' => $this->parm['business_id'], 'company_id' => $this->company), 'name');
            //通知后台
            $msg['type'] = 2;
            $msg['msg'] = '有新工单啦';
            $this->sendToUid($this->uid, $msg);
            //大数据平台
            $tel = substr($work_data['contact_number'], 0, 3) . '****' . substr($work_data['contact_number'], 7, 4);
            DataService::sendToUids(['command' => 'A1', 'data' => ['type' => 2, 'product' => $business_name, 'user_tel' => $tel]]);
            //通知行政中心
            $msg['type'] = 1;
            $msg['msg'] = '有新订单拉';
            $a_info = $this->userService->getOwnArea($user_info['user_id'], $user_info['source'], $work_data['province_code'], $work_data['city_code'], $work_data['area_code'], $this->company);
            if (!empty($a_info)) {
                $this->sendToUid($this->uid, $msg);
            }
            //发邮件通知
            $content = '<h2>' . $business['name'] . '工单通知</h2><br/><div>工单编号:' . $work_data['order_number'] . '</div><div>客户电话:' . $work_data['contact_number'] . '</div><div>安装地址:' . $work_data['province'] . $work_data['city'] . $work_data['area'] . $work_data['service_address'] . '</div><div style="color:#808080">请尽快进入管理后台审核工单!</div>';
            //总后台
            $admin_email = $this->admin_user_model->getOne(['is_admin' => 1, 'is_delete' => 0], 'notice_email');
            if (!empty($admin_email) && !empty($admin_email['notice_email'])) {
                EmailService::sendEmailAsync($admin_email['notice_email'], $content, "有新工单啦", '', '', '', $this->company);
            }
            if (!empty($a_info)) {
                //对应运营中心
                $op_email = $this->op_user_model->getOne(['o_id' => $a_info['operation'], 'is_delete' => 0, 'is_admin' => 1], 'notice_email');
                if (!empty($op_email) && !empty($op_email['notice_email'])) {
                    EmailService::sendEmailAsync($op_email['notice_email'], $content, "有新工单啦", '', '', '', $this->company);
                }
                //对应合伙人
                $administrative_email = $this->administrative_user_model->getOne(['a_id' => $a_info['a_id'], 'is_delete' => 0, 'is_admin' => 1], 'notice_email');
                if (!empty($administrative_email) && !empty($administrative_email['notice_email'])) {
                    EmailService::sendEmailAsync($administrative_email['notice_email'], $content, "有新工单啦", '', '', '', $this->company);
                }
            }
            //发送短信通知给审核人员
            $key = 'work_order';
            $content = '您有一份新的' . $business['name'] . '工单待审核，工单号：' . $work_data['order_number'] . '客户电话：' . $work_data['contact_number'];
            SmsService::smsNotification($key, $content, $this->company);
            $this->jsonend(ReturnCodeService::SUCCESS, '提交成功');
        } else {
            $this->jsonend(ReturnCodeService::FAIL, '提交失败');
        }
    }


    /**
     * showdoc
     * @catalog API文档/用户端/工单相关
     * @title 获取工单基本信息
     * @description 支持根据工单ID工单号查询如果是新装可以根据订单ID查询
     * @method POST
     * @url User/Work/getWorkInfo
     * @param work_order_id 可选 int 工单ID
     * @param order_number 可选 string 工单号
     * @param order_id 可选 int 订单ID
     * @return "data":{"order_id":"3","order_no":"CQR917731088221618"}
     * @return_param work_order_id int 工单ID
     * @return_param order_number string 工单号
     * @return_param work_order_status string 工单状态
     * @return_param combo_money string 支付金额
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */

    public function http_getWorkInfo()
    {
        $map = array();
        if (!empty($this->parm['work_order_id'] ?? '')) {
            $map['work_order_id'] = $this->parm['work_order_id'];
        }
        if (!empty($this->parm['order_number'] ?? '')) {
            $map['order_number'] = $this->parm['order_number'];
        }
        if (!empty($this->parm['order_id'] ?? '') && !empty($this->parm['business_id'] ?? '')) {
            $map['order_id'] = $this->parm['order_id'];
            $map['business_id'] = $this->parm['business_id'];
        }
        if (empty($map)) {
            return $this->jsonend(-1001, "缺少参数,工单ID,工单编号或者订单ID+业务类型");
        }
        $data = $this->work_model->getWorkDetail($map, array(), '*');
        if (!empty($data)) {
            return $this->jsonend(1000, "获取详情成功", $data);
        }
        return $this->jsonend(-1000, "获取详情失败");
    }

    /**
     * showdoc
     * @catalog API文档/用户端/工单相关
     * @title 工单列表
     * @description
     * @method POST
     * @url User/Work/getWorkList
     * @param page 可选 int 页数，默认1
     * @param pageSize 可选 int 每页条数，默认10
     * @param startTime 可选 string 开始时间
     * @param endTime 可选 string 结束时间
     * @param keyword 可选 string (工单号)
     * @return
     * @return_param work_order_id int 工单ID
     * @return_param order_number string 工单号
     * @return_param work_order_status string 工单状态
     * @return_param work_order_status_name string 状态名称
     * @return_param work_order_status_desc string 状态描述
     * @return_param contact_number int 联系电话
     * @return_param contacts string 联系人
     * @return_param appointment_time string 预约改造时间
     * @return_param order_time string 下单时间
     * @return_param equipments_name string 产品名称
     * @return_param main_pic string 产品图片
     * @return_param username string 用户名
     * @return_param engineers_name string 工程人员姓名
     * @return_param engineers_phone string 工程人员电话
     * @return_param equipment_id string 主板id
     * @return_param business_name string 业务类型名称
     * @return_param province string 省
     * @return_param city string 市
     * @return_param area string 区
     * @return_param service_address string 详细地址
     * @return_param reception_user int 0表示总后台下单1用户下单2表示系统下单3运营合伙人4城市合伙人5市场推广
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_getWorkList()
    {
        //接收参数
        $page = $this->parm['page'] ?? 1;
        $pageSize = $this->parm['pagesize'] ?? 10;
        $startTime = $this->parm['startTime'] ?? '';
        $endTime = $this->parm['endTime'] ?? '';
        $keyword = $this->parm['keyword'] ?? '';
        //查询用户名下所有合同
        $this->AchievementModel->table = 'customer_code';
        $code_where = ['t.user_id' => $this->user_id, 'is_owner' => ['<>', 1]];
        $code_field = 'contract_id';
        $code_join = [['customer_bind_equipment t', 't.customer_code = rq_customer_code.code', 'LEFT']];
        $customer_data = $this->AchievementModel->selectData($code_where, $code_field, $code_join);
        if (empty($customer_data)) return $this->jsonend(ReturnCodeService::FAIL, '暂无数据_1');
        $contract_id_data = [];
        foreach ($customer_data as $key => $value) {
            $contract_id_data[] = $value['contract_id'];
        }
        $map['rq_work_order.contract_id'] = ['in', $contract_id_data];
        $map['rq_work_order.company_id'] = $this->company;
        $map['rq_work_order.is_to_user'] = 1;
        $map['rq_work_order.work_order_status'] = ['in', [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 13, 14, 15]];
        if (!empty($keyword)) {
            $map['rq_work_order.order_number'] = ['like', '%' . $keyword . '%'];
        }
        // 判断用户有没有输开始日期 // 判断用户有没有输结束日期
        if (!empty($startTime) && !empty($endTime)) {
            $map['a.order_time'] = ['between', [strtotime($startTime), strtotime($endTime) + (60 * 60 * 24 - 1)]]; //终止到当天23:59:59
        } else if (!empty($startTime)) {
            $map['a.order_time'] = ['>=', strtotime($startTime)];
        } else if (!empty($endTime)) {
            $map['a.order_time'] = ['<=', strtotime($endTime) + (60 * 60 * 24 - 1)];
        }

        $join = [
            ['customer as c', 'c.user_id = rq_work_order.user_id', 'inner',], // 用户表关联工单表
//            ['work_equipment we', 'we.work_order_id = rq_work_order.work_order_id', 'left'],
            ['contract ct', 'ct.contract_id = rq_work_order.contract_id', 'left'],
        ];
        $field = 'rq_work_order.is_bind_equipment,rq_work_order.replacement_type,rq_work_order.is_new_word_order,rq_work_order.work_order_id,rq_work_order.contacts as contact_name,rq_work_order.contact_number as contact_phone,
                  rq_work_order.appointment_time,rq_work_order.province as transform_province_name,rq_work_order.city as transform_city_name,rq_work_order.area as transform_area_name,
                  rq_work_order.service_address as transform_detailed_address,rq_work_order.remarks,rq_work_order.order_time as create_time,
                  rq_work_order.order_number as sn,rq_work_order.work_order_status,rq_work_order.reception_user,c.username,
                  ct.shop_name,rq_work_order.move_province,rq_work_order.move_city,rq_work_order.move_area,rq_work_order.move_address,is_transfer,rq_work_order.contract_id';
        $order['rq_work_order.order_time'] = 'DESC';
        $data = $this->work_model->getAll($map, $join, $page, $pageSize, $field, $order);
        if (empty($data)) {
            return $this->jsonend(-1003, "没有数据");
        }
        foreach ($data as $k => $v) {

            $data[$k]['work_order_status_desc'] = $this->config->get('work_order_status_desc')[$v['work_order_status']];
            $data[$k]['create_time'] = $v['create_time'] ? date('Y-m-d H:i:s', $v['create_time']) : '';

            //处理业务类型
            $b_map['rq_work_order_business.work_order_id'] = $v['work_order_id'];
            $b_join = [
                ['business_type as b', 'b.business_type_id = rq_work_order_business.business_id', 'LEFT',]        // 关联业务类型表
            ];
            $b_map['b.company_id'] = $this->company;
            $business = $this->work_order_business_model->getAll($b_map, $b_join, 'rq_work_order_business.business_id,b.name');
            $b_id = [];
            $b_name = [];
            $installation_status_name = "";
            if (!empty($business)) {
                foreach ($business as $kk => $vv) {
                    array_push($b_id, $vv['business_id']);
                    if ($vv['business_id'] == 5) {
                        $replacement_type = $this->config->get('replacement_type')[$vv['replacement_type']];
                        $vv['name'] .= $replacement_type;
                    }
                    array_push($b_name, $vv['name']);
                    array_push($b_id, $vv['business_id']);
                    //如果是试用、改造查询安装中状态
                    if (in_array($vv['business_id'], [3, 9, 4]) && in_array($v['work_order_status'], [7, 14])) {
                        $installation_status_name = $this->energySavingDataTestModel->installation_status($v['work_order_id'], $v['is_transfer'], $v['is_bind_equipment'], $vv['business_id']);
                    }
                }
            }
            $data[$k]['business_name'] = !empty($b_name) ? implode('+', $b_name) : [];
            $data[$k]['business_id'] = $b_id;
            $data[$k]['status_name'] = $this->config->get('user_work_order_status')[$v['work_order_status']];
            if (in_array(6, $b_id) && $v['work_order_status'] == 7) {
                $data[$k]['status_name'] = '维修中';
            } else if (in_array(5, $b_id) && $v['work_order_status'] == 7) {
                $data[$k]['status_name'] = '移机中';
            } else if (in_array(4, $b_id) && $v['work_order_status'] == 7) {
                $data[$k]['status_name'] = '拆机中';
            }
            //如果是试用、改造、部分移机安装中拼接工单状态
            if (in_array($v['work_order_status'], [7, 14]) && (in_array(3, $b_id) || in_array(9, $b_id) || in_array(4, $b_id))) {
                $data[$k]['status_name'] = $data[$k]['status_name'] . $installation_status_name;
            }
        }
        return $this->jsonend(1000, "获取列表成功", $data);
    }


    /**
     * showdoc
     * @catalog API文档/用户端/工单相关
     * @title 工程进度
     * @description
     * @method POST
     * @url User/Work/projectProgress
     * @param work_order_id 可选 string 工单号
     * @param identification_id 可选 string 工单号
     * @return
     * @return_param content string 操作内容
     * @return_param open_remarks string 状态
     * @return_param operating_time string 操作时间
     * @remark
     * @number 0
     * @author xln
     * @date 2020-12-29
     */
    public function http_projectProgress()
    {
        if (empty($this->parm['work_order_id']) && empty($this->parm['identification_id'])) return $this->jsonend(ReturnCodeService::FAIL, '参数缺失： 工单ID或识别ID');
        $identification_id = $this->parm['identification_id'] ?? '';
        if (!empty($this->parm['work_order_id'])) {
            //查询工单信息
            $work_where = ['work_order_id' => $this->parm['work_order_id']];
            $work_join = [['contract t', 't.contract_id = rq_work_order.contract_id']];
            $work_field = 't.identification_id,work_order_id';
            $work_data = $this->work_model->getWorkDetail($work_where, $work_join, $work_field);
            if (empty($work_data)) return $this->jsonend(ReturnCodeService::FAIL, '暂无数据');
            //工单轨迹
            $log_field = 'work_order_log_id as id,operating_time,remarks as content,open_remarks';
            $log_where = ['work_order_id' => $this->parm['work_order_id']];
            $data = $this->work_log_model->getAll($log_where, 1, -1, [], $log_field, ['work_order_log_id' => 'desc'], 'work_order_log');
        } else {
            //识别表轨迹
            $this->AchievementModel->table = 'identification_log';
            $identification_where = ['identification_id' => $identification_id];
            $identification_field = 'id,create_time as operating_time,content,open_remarks';
            $data = $this->AchievementModel->selectData($identification_where, $identification_field, [], ['id' => 'desc']);
        }
        foreach ($data as $key => $value) {
            $data[$key]['operating_time'] = date('Y-m-d H:i:s', $value['operating_time']);
        }
        $w_data = [];
        foreach ($data as $k1 => $v1) {
            $w_data[] = $v1;
        }
        return $this->jsonend(ReturnCodeService::SUCCESS, '获取成功', $w_data);
    }


    /**
     * showdoc
     * @catalog API文档/用户端/工单相关
     * @title 工单详情
     * @description
     * @method POST
     * @url User/Work/workOrderDetail
     * @param work_order_id 必选 int 工单ID
     * @return {"code":1000,"message":"获取成功","data":{}}
     * @return_param work_order_id int 工单id
     * @return_param contract_id int 合同ID
     * @return_param equipments_id int 产品id
     * @return_param order_number string 工单号
     * @return_param scan_order_number string 用于扫码付调起支付
     * @return_param realname string 真名
     * @return_param telphone string 电话
     * @return_param account string 账号
     * @return_param reception_user int 0表示总后台下单1表示用户下单2表示系统下单3运营中心4行政中心
     * @return_param history array 订单追踪
     * @return_param complaint_data object 是否被投诉为空否不为空是
     * @return_param inviter_name string 推荐人
     * @return_param is_to_user int 用户是否可见1是2否
     * @return_param combo_money int 费用总价
     * @return_param pay_business_money int 基础费用
     * @return_param pay_additional_service_money int 额外服务
     * @return_param pay_filters_money int 换芯费用
     * @return_param pay_parts_money int 配件服务
     * @return_param pay_other_money int 其他费用
     * @return_param pay_way int 支付方式 1微信支付2支付宝支付3现金支付
     * @return_param wx_callback_num int 订单编号（支付单号）
     * @return_param order_pay_time string 支付时间
     * @return_param order_status string 订单状态1待付款2待发货3待安装4已安装5已取消6待收货7已作废8退款中9已退款
     * @return_param work_order_status int 1待审核2无效工单3问题已处理4前台已确认信息待指派工程部5已指派工程部待派单6工单已进入大厅7已指派工程人员待接单8工程人员已接单待预约
     * @return_param work_order_status int 9工程人员已预约待上门10工程人员已上门服务中11工程人员已完成待评价12已评价13工程人员已退单待审核14工程人员退单成功回到大厅15前台未回访
     * @return_param work_order_status int 16前台已回访未完成17前台已回访已完成18退款中19已退款20待回访
     * @return_param pay_status int 支付状态1未支付2已支付3退款中4已退款
     * @return_param create_time string 下单时间
     * @return_param pay_time string 支付时间
     * @return_param appointment_time string 预约时间
     * @return_param service_address string 服务地址
     * @return_param contacts string 联系人
     * @return_param contact_number string 联系电话
     * @return_param remarks string 备注
     * @return_param equipments_name string 产品名称
     * @return_param version string 固件版本
     * @return_param device_no string 主板编号
     * @return_param remarks string 备注
     * @return_param receipt_content string 回单内容
     * @return_param receipt_pic array 回单照片
     * @return_param return_water_pressure string 水压
     * @return_param return_tds string TDS值
     * @return_param operator_username string 回访姓名
     * @return_param visit_time string 回访时间
     * @return_param log array 工单轨迹
     * @return_param remarks string 备注
     * @return_param order_time string 下单时间
     * @return_param username string 用户名
     * @return_param source int 推荐人id
     * @return_param is_reminder int 是否催单1是2否默认2
     * @return_param reminder_time stirng 催单时间
     * @return_param work_order_visit_status int 回访状态1待回访2已回访
     * @return_param contact_number int 联系电话
     * @return_param contacts string 联系人
     * @return_param explainer string 讲解人E-2D-3E表示工程人员D表示市场推广
     * @return_param is_update_relation int 是否需要更新关系1否2是用户结算时关系更新
     * @return_param emergency_contact array 紧急联系人
     * @return_param is_bind_equipment int 新装工单是否绑定主板1是0否其他类型工单无该参数
     * @return_param repair_id string 维修人员ID
     * @return_param province string 省
     * @return_param city string 市
     * @return_param area string 区
     * @return_param service_address string 服务地址
     * @return_param who int 未指派已指派标识数3待指派4已指派6大厅1待审核20待回访默认-1
     * @return_param emergency_contact_name string 紧急联系人姓名
     * @return_param business_name string 工单类型
     * @return_param isShowChangeStatus bool 是否可更改状态审核之前或者新装工单绑定主板后不可以修改
     * @return_param equipments_name string 产品名称
     * @return_param inviter_name string 邀请人
     * @return_param engineers_name string 工程人员姓名
     * @return_param contract_no string 合同编号
     * @return_param combo_money string 支付金额
     * @return_param product_info string 产品信息
     * @return_param work_order_log array 日志信息
     * @return_param parts_data array 配件信息
     * @return_param evaluate_data array 评价信息
     * @return_param equipment_info array 主板信息
     * @return_param cooker_information array 炉具信息
     * @return_param img array 环境照片
     * @return_param gas_fee float 气费
     * @return_param cooker_num int 炉具数量
     * @return_param scene_type string 场景类型
     * @return_param cooker_type string 炉具类型
     * @return_param cooker_brand string 炉具品牌
     * @return_param cooker_model_number string 炉具型号
     * @return_param user_remarks string 用户备注
     * @return_param repair_remarks string 维修备注
     * @return_param cooker_model_number int 预计改造数量
     * @return_param expected_renovation_num int 实际改造数量
     * @return_param cooker_purchase_time string 炉具购买时间
     * @return_param expected_renovation_num array 实际改造数量
     * @return_param draught_fan string 风机信息
     * @return_param shop_name string 店铺名称
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_workOrderDetail()
    {
        // 接收参数
        $work_order_id = $this->parm['work_order_id'] ?? '';
        $client = $this->parm['client'] ?? 1;
        // 判断参数
        if (empty($work_order_id)) {
            return $this->jsonend(-1001, '工单ID不能为空');
        }
        // 工单ID字段替换
        $workOrderId = 'rq_work_order.work_order_id';
        // 查询条件
        $where = [$workOrderId => $work_order_id];

        // 连表操作
        $join = [
            ['customer as c', 'c.user_id = rq_work_order.user_id', 'left join',], // 用户表关联工单表
            ['contract as ct', 'ct.contract_id = rq_work_order.contract_id', 'left join',], // 合同表关联工单表
            ['work_parts as wp', 'wp.work_order_id = rq_work_order.work_order_id', 'left join',], // 工单配件表关联工单表
            ['parts as p', 'p.parts_id = wp.parts_id', 'left join',], // 配件表关联工单配件表
        ];
        // 查询字段
        $field = 'rq_work_order.*, p.parts_name,ct.contract_no,ct.type,ct.shop_name';
        // 调用模型查询数据
        $workDetail = $this->work_model->getWorkDetail($where, $join, $field);
        // 判断结果--返回
        if ($workDetail) {
            $workDetail['appointment_start_time'] = $workDetail['appointment_start_time'] ? date('Y-m-d H:i:s', $workDetail['appointment_start_time']) : '';
            $workDetail['appointment_end_time'] = $workDetail['appointment_end_time'] ? date('Y-m-d H:i:s', $workDetail['appointment_end_time']) : '';
            $appointment_time = $workDetail['appointment_time'] ? explode(' ', $workDetail['appointment_time']) : [];
            $workDetail['appointment_time'] = $appointment_time;
            $workDetail['order_time'] = $workDetail['order_time'] ? date('Y-m-d H:i:s', $workDetail['order_time']) : '';
            $workDetail['assigne_time'] = empty($workDetail['assigne_time']) ? '' : date('Y-m-d H:i:s', $workDetail['assigne_time']);
            $workDetail['assignd_time'] = empty($workDetail['assignd_time']) ? '' : date('Y-m-d H:i:s', $workDetail['assignd_time']);
            $workDetail['receive_time'] = empty($workDetail['receive_time']) ? '' : date('Y-m-d H:i:s', $workDetail['receive_time']);
            $workDetail['complete_time'] = empty($workDetail['complete_time']) ? '' : date('Y-m-d H:i:s', $workDetail['complete_time']);
            $workDetail['go_home_time'] = empty($workDetail['go_home_time']) ? '' : date('Y-m-d H:i:s', $workDetail['go_home_time']);
            $workDetail['engineers_avatar_img'] = empty($workDetail['engineers_avatar_img']) ? '' : $this->config->get('qiniu.qiniu_url') . $workDetail['engineers_avatar_img'];

            $workDetail['pay_business_money'] = formatMoney($workDetail['pay_business_money']);
            $workDetail['pay_additional_service_money'] = formatMoney($workDetail['pay_additional_service_money']);
            $workDetail['pay_parts_money'] = formatMoney($workDetail['pay_parts_money']);
            $workDetail['pay_other_money'] = formatMoney($workDetail['pay_other_money']);
            $workDetail['pay_origin_money'] = formatMoney($workDetail['pay_origin_money']);
            $workDetail['pay_client_desc'] = '--';
            $workDetail['pay_status_desc'] = '--';
            if ($workDetail['combo_money'] > 0) {
                $workDetail['pay_client_desc'] = empty($workDetail['pay_client']) ? '' : $this->config->get('user_bill.pay_client')[$workDetail['pay_client']];
                $workDetail['pay_status_desc'] = $workDetail['pay_status'] == 2 ? '已支付' : '待支付';
            }
            $workDetail['work_order_visit_status'] = '--';
            if ($workDetail['work_order_status'] == 10) {
                $workDetail['work_order_visit_status'] = ($workDetail['visit_status'] == 1 ? '待回访' : '已回访');
            }
            //查询工程人员信息
            $workDetail['engineering'] = [];
            $where = ['p.work_order_id' => $this->parm['work_order_id']];
            $join = [['work_engineering_relationship p', 'p.repair_id = rq_engineers.engineers_id']];
            $field = 'engineers_name,engineers_phone,engineers_number';
            $engineers = $this->engineersModel->getAll($where, $join, 1, -1, $field, []);
            if (!empty($engineers)) {
                $engineers_data = [];
                foreach ($engineers as $key => $value) {
                    $engineers_data[$key]['name'] = $value['engineers_name'] . '【' . $value['engineers_phone'] . '】';
                    $engineers_data[$key]['number'] = $value['engineers_number'];
                }
                $workDetail['engineering'] = $engineers_data;
            }

            $workDetail['combo_money'] = formatMoney($workDetail['combo_money']);
            $picture = '';
            if (!empty($workDetail['picture'])) {
                $picture = json_decode($workDetail['picture'], true);
                if (!empty($picture)) {
                    foreach ($picture as $k => $v) {
                        $picture[$k] = $this->config->get('qiniu.qiniu_url') . $v;
                    }
                }
            }
            $workDetail['picture'] = $picture;
            $problem_key = '';
            if (!empty($workDetail['problem_key'])) {
                $problem_key = json_decode($workDetail['problem_key'], true);
                if (!empty($problem_key)) {
                    foreach ($problem_key as $k => $v) {
                        $info = $this->work_order_problem_model->getOne(array('problem_id' => $v), 'problem_desc');
                        $problem_key[$k] = '';
                        if (!empty($info)) {
                            $problem_key[$k] = $info['problem_desc'];
                        }
                    }
                }
            }
            $workDetail['problem_key'] = $problem_key;
            //工单轨迹
            $work_log_map['work_order_id'] = $this->parm['work_order_id'];
            $work_log_map['is_to_user'] = 1;
            //$work_log_map['operating_status'] = ['IN', [1,2, 8, 9, 10, 11, 12]];
            $work_log_field = 'work_order_log_id,operating_time,operating_status,remarks,open_remarks';
            $work_log_order['work_order_log_id'] = 'DESC';
            $work_log = $this->work_log_model->getList($work_log_map, $work_log_field, $work_log_order);
            if (!empty($work_log)) {
                foreach ($work_log as $k => $v) {
                    $work_log[$k]['operating_time'] = date('Y-m-d H:i:s', $v['operating_time']);
                    $work_log[$k]['operating_status_name'] = !empty($v['operating_status']) ? $this->config->get('work_order_status_desc')[$v['operating_status']] : '';
                    $work_log[$k]['operating_status_desc'] = !empty($v['operating_status']) ? $this->config->get('work_order_status_desc')[$v['operating_status']] : '';
                }
            }
            $workDetail['work_order_log'] = $work_log;
            //处理业务类型
            $b_map['work_order_id'] = $workDetail['work_order_id'];
            $b_join = [
                ['business_type as b', 'b.business_type_id = rq_work_order_business.business_id', 'LEFT',]        // 关联业务类型表
            ];
            $b_map['b.company_id'] = $this->company;
            $business = $this->work_order_business_model->getAll($b_map, $b_join, 'rq_work_order_business.business_id,b.name');
            $b_id = [];
            $b_name = [];
            $installation_status_name = "";
            if (!empty($business)) {
                foreach ($business as $kk => $vv) {
                    array_push($b_id, $vv['business_id']);
                    if ($vv['business_id'] == 5) {
                        $replacement_type = $this->config->get('replacement_type')[$vv['replacement_type']];
                        $vv['name'] .= $replacement_type;
                    }
                    array_push($b_name, $vv['name']);
                    array_push($b_id, $vv['business_id']);
                    //如果是试用、改造查询安装中状态
                    if (in_array($vv['business_id'], [3, 9, 4]) && in_array($workDetail['work_order_status'], [7, 14])) {
                        $installation_status_name = $this->energySavingDataTestModel->installation_status($workDetail['work_order_id'], $workDetail['is_transfer'], $workDetail['is_bind_equipment'], $vv['business_id']);
                    }
                }
            }

            $workDetail['business_name'] = implode('+', $b_name);
            $workDetail['business_id'] = $b_id;
            $workDetail['work_order_status_name'] = $this->config->get('user_work_order_status')[$workDetail['work_order_status']];
            if (in_array(6, $b_id) && $workDetail['work_order_status'] == 7) {
                $workDetail['work_order_status_name'] = '维修中';
            } else if (in_array(5, $b_id) && $workDetail['work_order_status'] == 7) {
                $workDetail['work_order_status_name'] = '移机中';
            } else if (in_array(4, $b_id) && $workDetail['work_order_status'] == 7) {
                $workDetail['work_order_status_name'] = '拆机中';
            }
            //如果是试用、改造、部分移机安装中拼接工单状态
            if (in_array($workDetail['work_order_status'], [7, 14]) && (in_array(3, $b_id) || in_array(9, $b_id) || in_array(4, $b_id))) {
                $workDetail['work_order_status_name'] = $workDetail['work_order_status_name'] . $installation_status_name;
            }
            // 获取工单配件信息--------------
            $parts_data = [];
            // 1-- 获取原始配件
            // 1.1-- 获取主板ID
            $equipment_id = $this->work_eq_model->getOne(['work_order_id' => $work_order_id], 'equipment_id')['equipment_id'];
            // 1.2-- 根据主板ID获取原始配件
            $parts_data['original_parts'] = $this->ep_part_model->getJoinAll(['equipment_id' => $equipment_id, 'is_original' => 1]);

            // 3-- 获取配件
            $parts_join = [
                ['work_parts wp', 'wp.parts_id = rq_parts.parts_id', 'left join',], // 工单配件表关联配件表
                ['work_order wo', 'wo.work_order_id = wp.work_order_id', 'left join',], // 工单表关联工单配件表
            ];
            $parts_where['wp.work_order_id'] = $work_order_id;
            $parts_field = 'rq_parts.parts_name,rq_parts.parts_id,wp.parts_number';
            $parts_data['add_parts'] = $this->parts_model->getJoinAll($parts_where, $parts_field, $parts_join);
            $workDetail['parts_data'] = $parts_data;

            // 获取评价信息------------------
            $evaluate_where['work_order_id'] = $work_order_id;
            $evaluate_where['type'] = 2;
            $evaluate_where['status'] = 1;
            $evaluate_field = 'evaluate_id,evaluate_stars,evaluate_content,evaluate_image,evaluate_time';
            $evaluate_data = $this->evaluate_model->getOne($evaluate_where, $evaluate_field);

            // 获取评价标签
            $evaluate_label = [];
            if ($evaluate_data) {
                if ($evaluate_data['evaluate_image']) {
                    $evaluate_data['evaluate_image'] = json_decode($evaluate_data['evaluate_image'], true);
                    foreach ($evaluate_data['evaluate_image'] as $k => $v) {
                        $evaluate_data['evaluate_image'][$k] = $this->config['qiniu']['qiniu_url'] . $v;
                    }
                }
                $el_where['etl.evaluate_id'] = $evaluate_data['evaluate_id'];
                $el_where['a.status'] = 1;
                $el_where['a.label_type'] = 2;
                $el_join = [
                    ['evaluate_to_label etl', 'etl.label_id = a.label_id', 'left join',], // 评价标签关系表--关联--评价标签表
                ];
                $el_field = 'a.label_name,a.create_time';
                $evaluate_label = $this->evaluate_model->getJoinAll($el_where, $el_field, $el_join, 'evaluate_label a');
            }
            $evaluate_data['evaluate_label'] = $evaluate_label;
            $workDetail['evaluate_data'] = $evaluate_data;
            if ($workDetail['type'] == null) {
                $work_info = $this->work_model->getOne(['p_work_order_id' => $workDetail['work_order_id']], 'contract_id');
                if (!empty($work_info)) {
                    $contract = $this->contract_model->getOne(['contract_id' => $work_info['contract_id']], 'type');
                    if (!empty($contract)) {
                        $workDetail['type'] = $contract['type'];
                    }
                }
            }
            $workDetail['equipment_info'] = $this->work_eq_model->getOne(array('rq_work_equipment.work_order_id' => $work_order_id), 'equipment_id,cooker_num');
            return $this->jsonend(1000, '获取订单详情成功', $workDetail, true);
        } else {
            return $this->jsonend(-1000, '获取订单详情失败', $workDetail, true);
        }
    }

    /**
     * showdoc
     * @catalog API文档/用户端/工单相关
     * @title 催单
     * @description 催单
     * @method get
     * @url User/Work/workReminder
     * @header token 可选 string token
     * @param work_order_id 可选 int 工单ID
     * @return {"code":0,"data":{"uid":"1","username":"12154545","name":"吴系挂","groupid":2,"reg_time":"1436864169","last_login_time":"0"}}
     * @return_param
     * @return_param
     * @remark 这里是备注信息
     * @number 99
     */
    public function http_workReminder()
    {
        // 接收参数 -- 判断参数
        $work_order_id = $this->parm['work_order_id'] ?? '';
        $user_id = $this->user_id;
        if (empty($work_order_id)) {
            return $this->jsonend(-1001, '工单ID不能为空');
        }

        // 1-- 获取工单信息
        $where['work_order_id'] = $work_order_id;
        $where['user_id'] = $user_id;
        $field = 'repair_id,work_order_status,order_time,contact_number,contacts,order_number,province_code,city_code,area_code,
                  is_reminder,reminder_time';
        $work_data = $this->work_model->getOne($where, $field);
        if (!$work_data) {
            return $this->jsonend(-1000, '暂无工单信息', $work_data);
        }
        // 判断催单时间是否大于一个小时
        if ($work_data['is_reminder'] == 1) {
            // 上次催单时间加一个小时
            $time = date('Y-m-d H:i:s', strtotime('+1 hour', $work_data['reminder_time']));
            $current_time = date('Y-m-d H:i:s', time());
            if ($time > $current_time) {
                return $this->jsonend(-1000, '请于您上次催单时间后一小时在催');
            }
        }

        // 2-- 修改工单为已催单
        $data['is_reminder'] = 1;
        $data['reminder_time'] = time();
        $reminder_data = $this->work_model->editWork($where, $data);
        if (!$reminder_data) {
            return $this->jsonend(-1000, '催单失败，请稍后再试2', $reminder_data);
        }
        // 2-- 通知后台--发送短信
        if ($work_data['work_order_status'] != 2 && $work_data['work_order_status'] != 3 && $work_data['work_order_status'] < 7) {
            $content = '工单号为：' . $work_data['order_number'] . '的用户：' . $work_data['contacts'] . '催单了,请尽快处理';
            $company_config = CompanyService::getCompanyConfig($this->company);
            $sms_config = $company_config['sms_config'];
            SmsService::sendSmsAsync(ConfigService::getConfig('company_hotline', false, $this->company), $content, $sms_config);
        } // 3-- 通知工程人员--模板消息or发送短信
        else {
            // 3.1-- 获取工程人员信息
            $en_where['engineers_id'] = $work_data['repair_id'];
            $repair_data = $this->engineersModel->getOne($en_where);
            //通知到用户
            $engineer_notice_config = ConfigService::getTemplateConfig('reminder', 'engineer', $this->company);
            if (!empty($engineer_notice_config) && !empty($engineer_notice_config['template']) && $engineer_notice_config['template']['switch']) {
                $template_id = $engineer_notice_config['template']['wx_template_id'];
                $mp_template_id = $engineer_notice_config['template']['mp_template_id'];
                // 3.2-- 发送模板消息
                $work_data['order_time'] = date('Y-m-d H:i:s', $work_data['order_time']);
                $content = "催单提醒！！您的" . $work_data['order_number'] . "订单被用户催单了！请尽快处理!更多详情请进入'.$this->engineer_wx_name.'小程序查看！";
                // 模板内容
                $template_data = [
                    'keyword1' => ['value' => $work_data['order_number']], // 订单号
                    'keyword2' => ['value' => $work_data['order_time']], // 下单时间
                    'keyword3' => ['value' => $content], // 催单内容
                ];
                $mp_template_data = [
                    'keyword1' => ['value' => $work_data['order_number']], // 订单号
                    'keyword2' => ['value' => $work_data['order_time']], // 下单时间
                    'keyword3' => ['value' => $content], // 催单内容
                ];
                $template_tel = $repair_data['engineers_phone'];  // 工程人员电话
                $template_content = '催单提醒！！您的' . $work_data['order_number'] . '订单被用户催单了,请尽快处理'; // 短信内容
                $template_url = '/pages/user/afterSaleDetail/afterSaleDetail?detail';
                NotifiyService::sendNotifiyAsync($repair_data['openid'], $template_data, $template_id, $template_tel, $template_content, $template_url, '', $mp_template_id, $mp_template_data, 'engineer', $this->company);//异步发送
            }
        }
        return $this->jsonend(1000, '催单成功，我们会尽快为您处理');
    }

    /**
     * showdoc
     * @catalog API文档/用户端/工单相关
     * @title 工单费用详情
     * @description
     * @method POST
     * @url User/Work/getWorkCost
     * @param work_order_id 必选 int 工单ID
     * @return {"code":1000,"message":"获取成功","data":{"total":{"pay_origin_money":"0.00","pay_other_money":"0.00","combo_money":"0.00"},"base":{"total":{"money":"0.00"},"list":[{"single_money":"0.00","total_money":"0.00","num":"1","id":"3","name":"新装"}]},"add":[],"part":[],"filter":[]}}
     * @return_param total array 总金额明细（pay_origin_money 预计支付金额,单位元，pay_other_money 其他金额,比如催费时续费金额 combo_money 实际支付金额）
     * @return_param base array 基础服务金额（total服务总金额，list费用详情（single_money单价total_money总价 num数量 id服务ID name服务名称））
     * @return_param add array 额外服务金额（与基础服务返回一致）
     * @return_param part array 配件金额（与基础服务返回一致）
     * @return_param filter array 滤芯金额（与基础服务返回一致）
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_getWorkCost()
    {
        if (empty($this->parm['work_order_id'] ?? '')) {
            return $this->jsonend(-1001, "缺少参数工单ID");
        }
        $work_order_id = $this->parm['work_order_id'];
        $work_order_info = $this->work_model->getOne(array('work_order_id' => $work_order_id), 'pay_origin_money,pay_other_money,combo_money');
        if (empty($work_order_info)) {
            return $this->jsonend(-1001, "工单信息错误");
        }
        $data['total'] = array('pay_origin_money' => formatMoney($work_order_info['pay_origin_money']),
            'pay_other_money' => formatMoney($work_order_info['pay_other_money']),
            'combo_money' => formatMoney($work_order_info['combo_money']));
        //基础服务费用
        $base_join = [
            ['business_type b', 'b.business_type_id = rq_work_order_business.business_id', 'LEFT',]        // 关联业务类型表
        ];
        $base = $this->work_order_business_model->getAll(array('work_order_id' => $work_order_id, 'b.company_id' => $this->company), $base_join, 'rq_work_order_business.*,b.name');
        $base_final = [];
        if (!empty($base)) {
            $base_money = 0;
            $base_arr = [];
            foreach ($base as $k => $v) {
                $base_money += $v['money'];
                $a['single_money'] = formatMoney($v['single_money']);
                $a['total_money'] = formatMoney($v['money']);
                $a['num'] = $v['number'];
                $a['id'] = $v['business_id'];
                $a['name'] = $v['name'];
                $base_arr[] = $a;
            }
            $base_final['total']['money'] = formatMoney($base_money);
            $base_final['list'] = $base_arr;
        }
        //额外服务费用
        $add_join = [
            ['additional_service a', 'a.service_id = rq_work_addtional_service.service_id', 'LEFT',]        // 关联业务类型表
        ];
        $add = $this->work_additional_service_model->getAll('rq_work_addtional_service.*,a.service_name', 1, -1, ['work_service_id' => 'asc'], array('work_order_id' => $work_order_id), $add_join);
        $add_final = [];
        if (!empty($add)) {
            $add_arr = [];
            $add_money = 0;
            foreach ($add as $k => $v) {
                $add_money += $v['total_money'];
                $a['single_money'] = formatMoney($v['single_money']);
                $a['total_money'] = formatMoney($v['total_money']);
                $a['num'] = $v['service_num'];
                $a['id'] = $v['service_id'];
                $a['name'] = $v['service_name'];
                $add_arr[] = $a;
            }
            $add_final['total']['money'] = formatMoney($add_money);
            $add_final['list'] = $add_arr;
        }
        //配件费用
        $part_join = [
            ['parts p', 'p.parts_id = rq_work_parts.parts_id', 'LEFT',]        // 关联业务类型表
        ];
        $part = $this->work_parts_model->getAll(array('work_order_id' => $work_order_id), 1, -1, $part_join, 'rq_work_parts.*,p.parts_name');
        $part_final = [];
        if (!empty($part)) {
            $part_arr = [];
            $part_money = 0;
            foreach ($part as $k => $v) {
                $part_money += $v['total_money'];
                $a['single_money'] = formatMoney($v['single_money']);
                $a['total_money'] = formatMoney($v['total_money']);
                $a['num'] = $v['parts_number'];
                $a['id'] = $v['parts_id'];
                $a['name'] = $v['parts_name'];
                $part_arr[] = $a;
            }
            $part_final['total']['money'] = formatMoney($part_money);
            $part_final['list'] = $part_arr;
        }
        //滤芯费用
        $filter_join = [
            ['parts p', 'p.parts_id = rq_for_core_record.parts_id', 'LEFT',]        // 关联业务类型表
        ];
        $filter = $this->for_core_model->getAll(array('work_order_id' => $work_order_id), 1, -1, $filter_join, 'rq_for_core_record.*,p.parts_name');
        $filter_final = [];
        if (!empty($filter)) {
            $filter_momey = 0;
            $filter_arr = [];
            foreach ($filter as $k => $v) {
                $filter_momey += $v['money'];
                $a['single_money'] = formatMoney($v['money']);
                $a['total_money'] = formatMoney($v['money']);
                $a['num'] = $v['parts_number'];
                $a['id'] = $v['parts_id'];
                $a['name'] = $v['parts_name'];
                $filter_arr[] = $a;
            }
            $filter_final['total']['money'] = formatMoney($filter_momey);
            $filter_final['list'] = $filter_arr;
        }
        $data['base'] = $base_final;
        $data['add'] = $add_final;
        $data['part'] = $part_final;
        $data['filter'] = $filter_final;
        return $this->jsonend(1000, '获取成功', $data);
    }


    /**
     * showdoc
     * @catalog API文档/用户端/工单相关
     * @title 根据订单获取用户类型
     * @description
     * @method POST
     * @url User/Work/getUserClass
     * @param order_id 必选 int 订单ID
     * @param key 必选 string 对公转账配置值 enginer_transfer_pay
     * @return {"code": 1000,"message": "操作成功"}
     * @return_param
     * @remark
     * @number 0
     * @author tx
     * @date 2020-2-19
     */
    public function http_getUserClass()
    {
        if (empty($this->parm['order_id'] ?? '')) {
            return $this->jsonend(-1001, "缺少参数订单ID");
        }
        if (empty($this->parm['key'])) {
            return $this->jsonend(-1001, "缺少参数key");
        }
        $where['order_id'] = $this->parm['order_id'];
        $join = [
            ['customer c', 'c.user_id = rq_orders.user_id', 'left',]  // 关联用户表
        ];
        $info = $this->order_model->getOne($where, 'rq_orders.user_id,rq_orders.order_id,c.user_class', $join);
        $key = ConfigService::getConfig($this->parm['key'], true, $this->company);//获取配置

        if (empty($info || $key)) {
            return $this->jsonend(-1003, "暂无数据");
        }

        $data = [];
        $data['user_class'] = $info['user_class'] ?? 1;
        $data['key'] = $key ?? 0;
        return $this->jsonend(1000, "获取成功", $data);

    }

    /**
     * showdoc
     * @catalog API文档/用户端/工单相关
     * @title 取消工单
     * @description
     * @method POST
     * @url User/Work/cancelOrder
     * @param work_order_id 必选 int工单ID
     * @return {"code":1000,"message":"取消成功","data":true}
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_cancelOrder()
    {
        if (empty($this->parm['work_order_id'] ?? '')) {
            return $this->jsonend(-1001, "缺少参数工单参数id");
        }
        //查询订单信息
        $map['work_order_id'] = $this->parm['work_order_id'];
//        $map['user_id'] = $this->user_id;
        $field = 'pay_status,work_order_status';
        $order_info = $this->work_model->getOne($map, $field);
        if (empty($order_info)) {
            return $this->jsonend(-1100, "订单不存在");
        }
        if ($order_info['pay_status'] == 2) {
            return $this->jsonend(-1101, "该订单已支付,不能进行取消");
        }
        if (!in_array($order_info['work_order_status'], [1, 11, 13])) {
            return $this->jsonend(-1102, "该订单业务流程进行中，不能取消");
        }
        if ($order_info['work_order_status'] == 12) {
            return $this->jsonend(-1102, "该订单业务流程已经被关闭");
        }
        //修改订单状态
        $edit_data['work_order_status'] = 15;//已取消

        $add_status = false;
        $this->db->begin(function () use ($edit_data, &$add_status, $map) {
            $this->work_model->save($map, $edit_data);
            $this->addWorkOrderLog($this->parm['work_order_id'], time(), $edit_data['work_order_status'], "用户【用户ID:" . $this->user_id . "】已取消订单为无效工单");
            $add_status = true;
        });
        if ($add_status) {
            return $this->jsonend(1000, "取消成功");
        }
        return $this->jsonend(-1000, "取消失败");
    }


    /**
     * showdoc
     * @catalog API文档/用户端/工单相关
     * @title 获取工单进度
     * @description
     * @method POST
     * @url User/Work/getWorkRate
     * @param page 可选 int 页数，默认1
     * @param pageSize 可选 int 每页条数，默认10
     * @return
     * @return_param work_order_log_id int 工单日志ID
     * @return_param work_order_id int 工单ID
     * @return_param operating_time string 操作时间
     * @return_param create_work_time string 创建订单时间
     * @return_param operating_type string 操作类型0总后台操作1城市合伙人操作2工程人员操作3用户操作4系统操作5运营合伙人
     * @return_param operating_status string 操作状态见配置
     * @return_param is_to_user int 是否在用户端展示1是2否
     * @return_param remarks string 备注信息
     * @remark
     * @number 0
     * @author tx
     * @date 2020-9-18
     */
    public function http_getWorkRate()
    {
        //接收参数
        $page = $this->parm['page'] ?? 1;
        $pageSize = $this->parm['pagesize'] ?? 10;
        $work_order_id = $this->parm['work_order_id'] ?? '';
        if (empty($work_order_id)) {
            return $this->jsonend(ReturnCodeService::NO_DATA, '缺少工单参数');
        }
        //查询工单信息
        $where['work_order_id'] = $work_order_id;
        $work_info = $this->work_model->getOne($where, 'work_order_id');
        if (empty($work_info)) {
            return $this->jsonend(ReturnCodeService::NO_DATA, '工单参数错误');
        }
        $work_id = [];
        array_push($work_id, $work_info['work_order_id']);
        //查询工单进度信息
        unset($where);
        $where['work_order_id'] = ['IN', $work_id];
        $where['is_to_user'] = 1;
        $where['is_delete'] = 0;
        //$where['operating_status'] = ['IN', [1,2, 8, 9, 10, 11, 12]];
        $order = [
            'operating_time' => 'DESC',
            'work_order_log_id' => 'DESC'
        ];
        $data = $this->work_log_model->getAll($where, $page, $pageSize, [], '*', $order, 'work_order_log');
        if (empty($data)) {
            return $this->jsonend(ReturnCodeService::NO_DATA, '暂无工单轨迹');
        }
        foreach ($data as $k => $v) {
            $data[$k]['operating_time'] = $v['operating_time'] ? date('Y-m-d H:i', $v['operating_time']) : '';
            $data[$k]['create_work_time'] = $v['create_work_time'] ? date('Y-m-d H:i', $v['create_work_time']) : '';
            $data[$k]['operating_status_name'] = !empty($v['operating_status']) ? $this->config->get('user_work_order_status')[$v['operating_status']] : '';
            $data[$k]['operating_status_desc'] = !empty($v['operating_status']) ? $this->config->get('work_order_status_desc')[$v['operating_status']] : '';
        }
        return $this->jsonend(ReturnCodeService::SUCCESS, '获取成功', $data);
    }


    /**
     * showdoc
     * @catalog API文档/用户端/工单相关
     * @title 获取合同详情列表
     * @description
     * @method POST
     * @url User/Work/getEnergySavingDataList
     * @param contract_id|identification_id 必选 合同ID或者识别表ID
     * @return
     * @return_param business_data array 工单类型3改造9试用
     * @return_param contract_no string 合同编码
     * @remark
     * @number 0
     * @author xln
     * @date 2020-9-18
     */
    public function http_getEnergySavingDataList()
    {
        if (empty($this->parm['contract_id'])) {
            if (empty($this->parm['identification_id'])) return $this->jsonend(ReturnCodeService::FAIL, '参数缺失');
            $this->Achievement->table = 'contract';
            $where = ['identification_id' => $this->parm['identification_id']];
            $contract_data = $this->Achievement->findData($where, 'contract_id');
            if (empty($contract_data)) return $this->jsonend(ReturnCodeService::FAIL, '暂无数据');
            $contract_id = $contract_data['contract_id'];
        } else {
            $contract_id = $this->parm['contract_id'];
        }
        if (empty($contract_id)) return $this->jsonend(ReturnCodeService::FAIL, '参数缺失：合同ID');
        //查询工单类型
        $data = $this->contract_model->enclosure($contract_id);
        return $this->jsonend(ReturnCodeService::SUCCESS, '获取成功', $data);
    }

    /**
     * showdoc
     * @catalog API文档/用户端/工单相关
     * @title 获取采集信息、保管清单、分享确认单
     * @description
     * @method POST
     * @url User/Work/getEnergySavingData
     * @param contract_id 必选 int 合同ID
     * @param type 必选 int 类型：1、测试确认单、2保管清单、3分享确认单
     * @param work_order_type 必选 int 类型：1试用2改造
     * @return
     * @return_param company_sign string 公章图片
     * @return_param esign_user_sign string 签字图片
     * @return_param esign_user_sign_time string 签字时间
     * @return_param 测试确认单 id:id,confirm_time:确认时间,is_confirm_name:是否确认,stoves_code:炉具编码,cooker_name:炉具名称,test_method:炉具节能测试方法,before_testing_i:改造前炉具燃烧时间分before_testing_s:改造前炉具燃烧时间秒,before_testing_gas_consumption:改造前燃气消耗m³after_testing_i:改造后炉具燃烧时间分,after_testing_s:改造后炉具燃烧时间秒,after_testing_gas_consumption:改造后燃气消耗m³
     * @return_param 采集数据 data:[num:数量,money:总价,price:单价,name:名称],money:所有炉具总金额
     * @return_param 分享确认单 data:[A_first_year_proportion:甲方支付给乙方的节能服务费百分比,A_first_year_moeny:甲方支付给乙方的节能服务费,B_first_year_proportion:甲方享有百分比,B_first_year_moeny:甲方享有金额]
     * @remark
     * @number 0
     * @author xln
     * @date 2020-9-18
     */
    public function http_getEnergySavingData()
    {
        if (empty($this->parm['type'])) return $this->jsonend(ReturnCodeService::FAIL, '参数缺失：类型'); //1、测试确认单、2保管清单、3分享确认单
        if (empty($this->parm['work_order_type'])) return $this->jsonend(ReturnCodeService::FAIL, '参数缺失：类型'); //1试用2改造
        if (empty($this->parm['contract_id']) && empty($this->parm['identification_id'])) return $this->jsonend(ReturnCodeService::FAIL, '参数缺失：合同ID或识别ID');
        //查询合同信息
        $where = [];
        if (!empty($this->parm['identification_id'])) {
            $where['identification_id'] = $this->parm['identification_id'];
        }
        if (!empty($this->parm['contract_id'])) {
            $where['contract_id'] = $this->parm['contract_id'];
        }
        $this->Achievement->table = 'contract';
        $contract_data = $this->Achievement->findData($where, 'contract_id,type');
        if (empty($contract_data)) return $this->jsonend(ReturnCodeService::FAIL, '暂无数据');
        $contract_id = $contract_data['contract_id'];

        $where = ['is_merged' => 1, 'status' => 1, 'contract_id' => $contract_id, 'type' => $this->parm['type'], 'work_order_type' => $this->parm['work_order_type']];
        $field = 'work_order_id,id,cooker_data,work_order_type,esign_user_sign,esign_user_sign_time,create_time,engineering_esign_user_sign,engineering_user_sign_time';
        $energy_data = $this->energySavingDataTestModel->getOne($where, $field, []);
        if (empty($energy_data)) return $this->jsonend(ReturnCodeService::FAIL, '暂无数据');
        //查询工单
        $work_order_data = $this->work_model->getWorkDetail(['work_order_id' => $energy_data['work_order_id']], [], 'is_transfer,is_bind_equipment');
        $cooker_data = json_decode($energy_data['cooker_data'], true);
        $energy_data['is_transfer'] = 0; //默认不可以移交
        if (!empty($cooker_data) && in_array($this->parm['type'], [1, 2])) {
            $result = $this->stoveManagementModel->getAll(['company_id' => $this->company, 'is_delete' => 0], 'id,name,water_quantity');
            $money = 0;
            if ($this->parm['type'] == 2) {
                $money = $cooker_data['money'];
                $cooker_data = $cooker_data['data'];
            }
            if ($work_order_data['is_transfer'] == 2) {
                foreach ($cooker_data as $key => $v) {
                    if (!empty($v['is_confirm']) && $v['is_confirm'] == 1) {
                        $energy_data['is_transfer'] = 1;
                        break;
                    }
                    $energy_data['is_transfer'] = 2;
                }
            }
            foreach ($cooker_data as $k => $v) {
                //是否移交
                if ($energy_data['is_transfer'] == 1 && ($v['is_confirm'] == 1 || $work_order_data['is_bind_equipment'] == 0)) {
                    $energy_data['is_transfer'] = 1;
                }
                if ($this->parm['type'] == 1) { //测试确认单
                    $cooker_name_desc = searchArray($result, 'id', $v['cooker_name']);
                    $cooker_data[$k]['cooker_name_desc'] = empty($cooker_name_desc) ? '' : $cooker_name_desc[0]['name'];
                }
                $cooker_data[$k]['confirm_time'] = empty($v['confirm_time']) ? '' : date('Y-m-d H:i:s', $v['confirm_time']);
                $cooker_data[$k]['is_confirm_name'] = ($v['is_confirm'] == 1 ? '未确认' : '已确认');
            }
            if ($this->parm['type'] == 2) {
                $cooker_data = ['data' => $cooker_data, 'money' => $money, 'money_capital' => num_to_rmb($money)];
            }
        } else {
            //分享确认单
            $energy_data['is_transfer'] = 2;
        }
        $company_info = CompanyService::getCompanyInfo($this->company);
        $energy_data['company_sign'] = $company_info['company_seal'];
        $energy_data['cooker_data'] = $cooker_data;
        $energy_data['esign_user_sign_url'] = empty($energy_data['esign_user_sign']) ? '' : UploadImgPath($energy_data['esign_user_sign']);
        $energy_data['engineering_esign_user_sign_url'] = empty($energy_data['engineering_esign_user_sign']) ? '' : UploadImgPath($energy_data['engineering_esign_user_sign']);
        $energy_data['create_time'] = date('Y-m-d H:i:s', $energy_data['create_time']);
        $energy_data['esign_user_sign_time'] = empty($energy_data['esign_user_sign_time']) ? '' : date('Y-m-d H:i:s', $energy_data['esign_user_sign_time']);
        $energy_data['engineering_user_sign_time'] = empty($energy_data['engineering_user_sign_time']) ? '' : date('Y-m-d H:i:s', $energy_data['engineering_user_sign_time']);
        $energy_data['work_order_type'] = $this->contract_model->contractType($this->parm['contract_id'], $energy_data['work_order_id'], $this->parm['type']);
        return $this->jsonend(ReturnCodeService::SUCCESS, '获取成功', $energy_data);
    }


    /**
     * showdoc
     * @catalog API文档/用户端/工单相关
     * @title 用户确认采集信息
     * @description
     * @method POST
     * @url User/Work/CollectingInformationConfirm
     * @param id 必选 int id
     * @param array_id 必选 int 数组里ID
     * @return
     * @remark
     * @number 0
     * @author xln
     * @date 2020-9-18
     */
    public function http_CollectingInformationConfirm()
    {
        if (empty($this->parm['work_order_type'])) return $this->jsonend(ReturnCodeService::FAIL, '参数缺失：类型');
        if (empty($this->parm['type'])) return $this->jsonend(ReturnCodeService::FAIL, '参数缺失：类型');
        if (empty($this->parm['array_id'])) return $this->jsonend(ReturnCodeService::FAIL, '参数缺失：array_id');
        if (empty($this->parm['contract_id'])) return $this->jsonend(ReturnCodeService::FAIL, '参数缺失：合同ID');
        $energy_where = ['status' => 1, 'contract_id' => $this->parm['contract_id'], 'work_order_type' => $this->parm['work_order_type'], 'type' => $this->parm['type']];
        $energy_data = $this->energySavingDataTestModel->getOne($energy_where, 'cooker_data', ['id' => 'desc']);
        if (empty($energy_data)) return $this->jsonend(ReturnCodeService::FAIL, '暂无数据');
        $cooker_data = json_decode($energy_data['cooker_data'], true);
        $name = [1 => '测试确认单', 2 => '保管清单', 3 => '分享确认单'];
        if ($this->parm['type'] == 1) {
            foreach ($cooker_data as $key => $value) {
                if ($value['array_id'] == $this->parm['array_id']) {
                    $cooker_data[$key]['is_confirm'] = 2;
                    $cooker_data[$key]['confirm_time'] = time();
                }
            }
        }
        if ($this->parm['type'] == 2) {
            foreach ($cooker_data['data'] as $key => $value) {
                if ($value['array_id'] == $this->parm['array_id']) {
                    $cooker_data['data'][$key]['is_confirm'] = 2;
                    $cooker_data['data'][$key]['confirm_time'] = time();
                }
            }
        }
        $add_status = false;
        $this->db->begin(function () use (&$add_status, $cooker_data, $name, $energy_where) {
            $this->energySavingDataTestModel->save($energy_where, ['cooker_data' => json_encode($cooker_data)]);
            //写入合同日志
            $contact_log = ['contract_id' => $this->parm['contract_id'], 'do_id' => $this->user_id, 'do_type' => 3, 'terminal_type' => 5, 'do_time' => time(), 'remark' => '确认' . $name[$this->parm['type']]];
            $this->Achievement->table = 'contract_log';
            $this->Achievement->insertData($contact_log);
            $add_status = true;
        }, function ($e, $msg) {
            return $this->jsonend(-1000, '出错啦~~，错误原因：' . $e->error ?? $msg);
        });
        if ($add_status) {
            return $this->jsonend(ReturnCodeService::SUCCESS, '确认成功');
        }
    }


    /**
     * showdoc
     * @catalog API文档/用户端/工单相关
     * @title 用户签字
     * @description
     * @method POST
     * @url User/Work/addEsign_user_sign
     * @param contract_id 必选 int 合同ID
     * @param type 必选 int 类型：1、测试确认单、2保管清单、3分享确认单
     * @param work_order_type 必选 类型：1试用2改造
     * @param esign_user_sign 必选 签字图片
     * @return
     * @remark
     * @number 0
     * @author xln
     * @date 2020-9-18
     */
    public function http_addEsign_user_sign()
    {
        if (empty($this->parm['contract_id'])) return $this->jsonend(ReturnCodeService::FAIL, '参数缺失:合同ID');
        if (empty($this->parm['type'])) return $this->jsonend(ReturnCodeService::FAIL, '参数缺失：类型');
        if (empty($this->parm['work_order_type'])) return $this->jsonend(ReturnCodeService::FAIL, '参数缺失：类型');
        if (empty($this->parm['esign_user_sign'])) return $this->jsonend(ReturnCodeService::FAIL, '参数缺失：签字图片');
        $add_status = false;
        $this->db->begin(function () use (&$add_status) {
            $data = ['esign_user_sign' => $this->parm['esign_user_sign'], 'esign_user_sign_time' => time()];
            $where = ['status' => 1, 'type' => $this->parm['type'], 'contract_id' => $this->parm['contract_id'], 'work_order_type' => $this->parm['work_order_type']];
            $this->energySavingDataTestModel->save($where, $data);
            //写入合同日志
            $type_name = [1 => '测试确认单', 2 => '保管清单', 3 => '分享确认单'];
            $contact_log = ['contract_id' => $this->parm['contract_id'], 'do_id' => $this->user_id, 'do_type' => 3, 'terminal_type' => 5, 'do_time' => time(), 'remark' => $type_name[$this->parm['type']] . '确认签字'];
            $this->Achievement->table = 'contract_log';
            $this->Achievement->insertData($contact_log);
            $add_status = true;
        }, function ($e, $msg) {
            return $this->jsonend(-1000, '出错啦~~，错误原因：' . $e->error ?? $msg);
        });
        if ($add_status) {
            $this->jsonend(ReturnCodeService::SUCCESS, '操作成功');
        }
    }


}
