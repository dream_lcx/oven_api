<?php

namespace app\Controllers\User;

use app\Services\Device\DeviceService;
use app\Services\User\UserService;
use Server\CoreBase\Controller;
use app\Wechat\WxPay;
use app\Library\Alipay\aop\AopClient;
use app\Library\SLog;
use Server\CoreBase\SwooleException;
use app\Services\Common\EmailService;
use app\Services\Common\ConfigService;
use app\Services\Common\HttpService;
use app\Services\Common\NotifiyService;
use app\Services\Common\DataService;
use app\Services\Company\CompanyService;

/**
 * 支付回调
 * Class Callback
 * @package app\Controllers\User
 */
class Callback extends Controller
{

    /**
     * 初始化
     * Callback constructor.
     */
    private $order_model;
    private $user_model;
    private $log_model;
    private $admin_info_model;
    private $user_center_model;
    private $contract_model;
    private $renewOrderModel;
    protected $equipment_model;
    private $equipmentListsModel;
    private $eq_log_model;
    private $contract_log_model;
    protected $renew_eq_model;
    protected $wx_config;
    protected $order_logistics_model;
    protected $contract_ep_model;
    protected $admin_user_model;
    protected $op_user_model;
    protected $administrative_user_model;
    protected $finance_record_model;
    protected $renew_record_model;
    protected $Achievement;
    protected $GetIPAddressHttpClient;
    protected $BossApiClient;
    protected $userService;
    protected $service_payment_config;
    protected $fee_order_model;
    protected $customer_code_model;
    protected $customer_code_bill_model;
    protected $extensionModel;
    protected $coupon_model;
    protected $settlementModel;

    public function initialization($controller_name, $method_name)
    {
        parent::initialization($controller_name, $method_name);

        $this->user_model = $this->loader->model('CustomerModel', $this);
        $this->order_model = $this->loader->model('OrderModel', $this);
        $this->log_model = $this->loader->model('OrderLogModel', $this);
        $this->admin_info_model = $this->loader->model('AdministrativeInfoModel', $this);
        $this->user_center_model = $this->loader->model('CustomerAdministrativeCenterModel', $this);
        $this->contract_model = $this->loader->model('ContractModel', $this);
        $this->renewOrderModel = $this->loader->model('RenewOrderModel', $this);
        $this->equipment_model = $this->loader->model('EquipmentModel', $this);
        $this->equipmentListsModel = $this->loader->model('EquipmentListsModel', $this);
        $this->eq_log_model = $this->loader->model('EquipmentLogModel', $this);
        $this->contract_log_model = $this->loader->model('ContractLogModel', $this);
        $this->renew_eq_model = $this->loader->model('RenewOrderEquipmentModel', $this);
        $this->order_logistics_model = $this->loader->model('OrderLogisticsModel', $this);
        $this->bill_model = $this->loader->model('EngineerBillModel', $this);
        $this->contract_ep_model = $this->loader->model('ContractEquipmentModel', $this);
        $this->admin_user_model = $this->loader->model('AdminUserModel', $this);
        $this->op_user_model = $this->loader->model('OperationUserModel', $this);
        $this->administrative_user_model = $this->loader->model('AdministrativeUserModel', $this);
        $this->finance_record_model = $this->loader->model('FinanceRecordModel', $this);
        $this->renew_record_model = $this->loader->model('RenewRecordModel', $this);
        $this->Achievement = $this->loader->model('AchievementModel', $this);
        $this->userService = new UserService();
        $this->service_payment_config = $this->config->get('service_payment_config');
        $this->fee_order_model = $this->loader->model('FeeOrderModel', $this);
        $this->customer_code_model = $this->loader->model('CustomerCodeModel', $this);
        $this->customer_code_bill_model = $this->loader->model('CustomerCodeBillModel', $this);
        $this->coupon_model = $this->loader->model('CouponModel', $this);
        $this->settlementModel = $this->loader->model('SettlementModel', $this);


    }

    public function __construct()
    {
        parent::__construct();
        $this->GetIPAddressHttpClient = get_instance()->getAsynPool('BuyWater');
        $this->BossApiClient = get_instance()->getAsynPool('BossApi');

    }

    //租赁支付回调
    public function http_rentOrder()
    {
        //验证微信签名
        //$postStr = $this->http_input->getRawContent();
        //file_put_contents('buy_cloud.txt', $postStr);

        $postStr = file_get_contents('buy_cloud.txt');
        $postObj = simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA);

        if ($postObj->result_code == 'SUCCESS' && $postObj->return_code == 'SUCCESS') {
            $order_sn = $postObj->out_trade_no;
            $order = $this->order_model->getOne(['order_no' => $order_sn], '*');
            if (empty($order)) {
                $return['return_code'] = 'FAIL';
                $return['return_msg'] = 'error';
                $return = $this->arrayToXml($return);
                return $this->response->end($return);
            }
            if ($order['pay_status'] == 2) {
                $return['return_code'] = 'SUCCESS';
                $return['return_msg'] = 'OK';
                $return = $this->arrayToXml($return);
                return $this->response->end($return);
            }
            if ($order['pay_mode'] == 2) {
                $wx_config = $this->service_payment_config;
            } else {
                $company_config = CompanyService::getCompanyConfig($order['company_id']);
                $wx_config = $company_config['wx_config'];
            }
            $wx_config['payment_type'] = 'JSAPI';
            //$WxPay = new WxPay($wx_config);
            $WxPay = WxPay::getInstance($wx_config);
            if ($WxPay->isSetConfig) $WxPay->setConfig($wx_config);
            $signObj = [];
            foreach ($postObj as $key => $val) {
                if ($key != 'sign') {
                    $signObj[$key] = $val;
                }
            }

            //验证签名
            if ($WxPay->getSign($signObj) == $postObj->sign) {
                $order['wx_callback_num'] = $postObj->transaction_id;
                $total_fee = $postObj->total_fee;
                $status = false;
                $this->db->begin(function () use ($order, &$data, &$status, $total_fee) {
                    //修改订单状态,如果走发货流程 order_status=2 待发货 反之 order_status=3 待安装
                    $order_status = 3;
                    $deliver_way = 0;
                    if ($order['order_type'] == 2) {
                        $order_status = 2;
                        $deliver_way = 2;
                    }
                    //如果已经是已安装不修改订单状态
                    if ($order['order_status'] == 4) {
                        $order_status = 4;
                    }
                    $this->order_model->save(['order_id' => $order['order_id']], ['order_status' => $order_status, 'pay_status' => 2, 'pay_time' => time(), 'wx_callback_num' => $order['wx_callback_num'], 'deliver_way' => $deliver_way]);
                    //添加订单日志
                    $this->addOrderLog($order['order_id'], $order_status, "您的订单已支付成功,我们将尽快进行处理,订单编号:" . $order['order_no'], 1);
                    //添加用户与行政中心关系
                    $this->userService->addUserCenter($order['user_id'], $order['province_code'], $order['city_code'], $order['area_code'], $order['company_id']);
                    //添加物流信息
                    $this->addOrderLogistics($order['order_id'], 1, "您的订单开始处理", $order['user_id']);
                    //新增资金记录
                    $finance_record['type'] = 1;
                    $finance_record['order_id'] = $order['order_id'];
                    $finance_record['user_id'] = $order['user_id'];
                    $finance_record['money'] = $total_fee;
                    $finance_record['create_time'] = time();
                    $finance_record['is_online_pay'] = 1;
                    $finance_record['pay_way'] = 1;
                    $finance_record['callback_num'] = $order['wx_callback_num'];
                    $finance_record['payment_method'] = 1;
                    $finance_record['payment_uid'] = $order['user_id'];
                    $finance_record['o_id'] = $order['operation_id'];
                    $finance_record['a_id'] = $order['administrative_id'];
                    $finance_record['company_id'] = $order['company_id'];
                    $this->finance_record_model->add($finance_record);

                    $status = true;
                });
                if ($status) {
                    $user_info = $this->user_model->getOne(array('user_id' => $order['user_id']), 'openid,telphone,user_id,source');
                    $eq_info = $this->equipment_model->getDetail(array('equipments_id' => $order['equipments_id']), 'equipments_name');
                    //通知到用户
                    $user_notice_config = ConfigService::getTemplateConfig('user_new_order', 'user', $order['company_id']);
                    if (!empty($user_notice_config) && !empty($user_notice_config['template']) && $user_notice_config['template']['switch']) {
                        $template_id = $user_notice_config['template']['wx_template_id'];
                        $mp_template_id = $user_notice_config['template']['mp_template_id'];
                        //数据组装
                        $pay_way = '微信支付';
                        $order_time = date('Y-m-d H:i:s', $order['create_time']);

                        //通知数据组装
                        $template_data = [
                            'keyword1' => ['value' => $order['order_no']],
                            'keyword2' => ['value' => $eq_info['equipments_name']],
                            'keyword3' => ['value' => $order['equipments_num']],
                            'keyword4' => ['value' => 1],
                            'keyword5' => ['value' => $order['order_actual_money']],
                            'keyword6' => ['value' => $pay_way],
                            'keyword7' => ['value' => $order_time],
                            'keyword8' => ['value' => '您的订单已经支付成功，感谢您对我们的的支持。若有疑问可致电官方客服,客服电话:' . ConfigService::getConfig('company_hotline', false, $order['company_id'])]
                        ];

                        $template_tel = $user_info['telphone'];
                        $template_content = '您的订单(订单编号:' . $order['order_no'] . ',商品名称:' . $eq_info['equipments_name'] . ',数量:' . $order['equipments_num'] . ')已完成支付，金额为' . $order['order_actual_money'] . '元。打开微信进入' . $this->wx_config['app_wx_name'] . '可查看支付方式、下单时间等支付详情';
                        $template_url = 'pages/user/myOrderDetail/myOrderDetail?order_id=' . $order['order_id'];
                        $weapp_template_keyword = '';
                        $mp_template_data = [
                            'first' => ['value' => '您的订单已经支付成功，感谢您对我们的支持', 'color' => '#4e4747'],
                            'keyword1' => ['value' => $eq_info['equipments_name'], 'color' => '#4e4747'],
                            'keyword2' => ['value' => $order['order_no'], 'color' => '#4e4747'],
                            'keyword3' => ['value' => $order['order_actual_money'], 'color' => '#4e4747'],
                            'remark' => ['value' => '若有疑问可致电官方客服,客服电话:' . ConfigService::getConfig('company_hotline', false, $order['company_id']), 'color' => '#173177'],
                        ];
                        NotifiyService::sendNotifiyAsync($user_info['openid'], $template_data, $template_id, $template_tel, $template_content, $template_url, $weapp_template_keyword, $mp_template_id, $mp_template_data, 'user', $order['company_id']);
                    }

                    //通知到后台
                    $admin_notice_config = ConfigService::getTemplateConfig('user_new_order', 'admin', $order['company_id']);

                    if (!empty($admin_notice_config)) {
                        $a_info = $this->userService->getOwnArea($user_info['user_id'], $user_info['source'], $order['province_code'], $order['city_code'], $order['area_code'], $order['company_id']);
                        //站内
                        if (!empty($admin_notice_config['websocket']) && $admin_notice_config['websocket']['switch']) {
                            $msg['type'] = 1;
                            $msg['msg'] = '有新订单拉';
                            $this->sendToUid($this->uid, $msg);
                            //通知行政中心
                            $msg['type'] = 1;
                            $msg['msg'] = '有新订单拉';
                            if (!empty($a_info)) {
                                $this->sendToUid($this->uid, $msg);
                                $this->sendToUid($this->uid, $msg);
                            }
                        }
                        if (!empty($admin_notice_config['email']) && $admin_notice_config['email']['switch']) {
                            //发邮件通知
                            $opertaor = $this->config->get('order_opertaor')[$order['opertaor']];//订单来源
                            $content = '<h2>新订单通知</h2><br/><div>订单编号:' . $order['order_no'] . '</div><div>订单来源:' . $opertaor . '</div><div>客户电话:' . $user_info['telphone'] . '</div><div>商品名称:' . $eq_info['equipments_name'] . '(数量:' . $order['equipments_num'] . ')</div><div>订单金额:' . $order['order_actual_money'] . '元</div><div>下单时间:' . $order_time . '</div><div style="color:#808080">客户已完成支付,请进入管理后台查看详情!</div>';
                            //总后台
                            $admin_email = $this->admin_user_model->getOne(['is_admin' => 1, 'is_delete' => 0], 'notice_email');
                            if (!empty($admin_email) && !empty($admin_email['notice_email'])) {
                                EmailService::sendEmailAsync($admin_email['notice_email'], $content, "有新订单啦", '', '', '', $order['company_id']);
                            }
                            if (!empty($a_info)) {
                                //对应运营中心
                                $op_email = $this->op_user_model->getOne(['o_id' => $a_info['operation'], 'is_delete' => 0, 'is_admin' => 1], 'notice_email');
                                if (!empty($op_email) && !empty($op_email['notice_email'])) {
                                    EmailService::sendEmailAsync($op_email['notice_email'], $content, "有新订单啦", '', '', '', $order['company_id']);
                                }
                                //对应合伙人
                                $administrative_email = $this->administrative_user_model->getOne(['a_id' => $a_info['a_id'], 'is_delete' => 0, 'is_admin' => 1], 'notice_email');
                                if (!empty($administrative_email) && !empty($administrative_email['notice_email'])) {
                                    EmailService::sendEmailAsync($administrative_email['notice_email'], $content, "有新订单啦", '', '', '', $order['company_id']);
                                }
                            }
                        }

                    }
                    //通知到大数据平台
                    $big_data_notice_config = ConfigService::getTemplateConfig('user_new_order', 'big_data', $order['company_id']);
                    if (!empty($big_data_notice_config) && !empty($big_data_notice_config['websocket']) && $big_data_notice_config['websocket']['switch']) {
                        $tel = substr($user_info['telphone'], 0, 3) . '****' . substr($user_info['telphone'], 7, 4);
                        DataService::sendToUids(['command' => 'A1', 'data' => ['type' => 1, 'product' => $eq_info['equipments_name'], 'user_tel' => $tel]]);
                    }


                    $return['return_code'] = 'SUCCESS';
                    $return['return_msg'] = 'OK';
                    $return = $WxPay->arrayToXml($return);
                    return $this->response->end($return);
                }
            }

        }
        $return['return_code'] = 'FAIL';
        $return['return_msg'] = 'error';
        $return = $WxPay->arrayToXml($return);
        return $this->response->end($return);
    }

    //租赁支付宝支付回调
    public function http_ali_rentOrder()
    {
        //验证支付宝签名
        $postStr = $this->http_input->getRawContent();
        file_put_contents('ali_pay.txt', $postStr);
        //$postStr = file_get_contents('ali_pay.txt');
        $postStr = urldecode($postStr);
        $postStr = explode('&', $postStr);

        $post = [];
        foreach ($postStr as $k => $v) {
            $str = explode('=', $v);
            $post[$str[0]] = $str[1];
        }
        if (!empty($post)) {
            $alipay = $this->config->get('alipay');
            $aop = new AopClient();
            $aop->gatewayUrl = $alipay['gatewayUrl'];
            $aop->appId = $alipay['appId'];
            $aop->rsaPrivateKey = $alipay['rsaPrivateKey'];
            $aop->alipayrsaPublicKey = $alipay['alipayrsaPublicKey'];
            $aop->apiVersion = '1.0';
            $aop->signType = 'RSA2';
            $aop->postCharset = 'UTF-8';
            $aop->format = 'json';
            $result = $aop->rsaCheckV1($post, $aop->alipayrsaPublicKey, $aop->signType);
            //验证签名
            if ($result) {
                $order_sn = $post['out_trade_no'];
                $order = $this->order_model->getOne(['order_no' => $order_sn], '*');
                if (empty($order)) {
                    return $this->response->end('fail');
                }
                if ($order['pay_status'] == 2) {
                    return $this->response->end('SUCCESS');
                }
                $order['ali_callback_num'] = $post['notify_id'];
                $status = false;
                $this->db->begin(function () use ($order, &$data, &$status) {
                    //修改订单状态,如果走发货流程 order_status=2 待发货 反之 order_status=3 待安装
                    $order_status = 3;
                    $deliver_way = 0;
                    if ($order['order_type'] == 2) {
                        $order_status = 2;
                        $deliver_way = 2;
                    }
                    //如果已经是已安装不修改订单状态
                    if ($order['order_status'] == 4) {
                        $order_status = 4;
                    }
                    $this->order_model->save(['order_id' => $order['order_id']], ['order_status' => $order_status, 'pay_status' => 2, 'pay_time' => time(), 'ali_callback_num' => $order['ali_callback_num'], 'wx_callback_num' => $order['ali_callback_num'], 'deliver_way' => $deliver_way]);
                    //添加订单日志
                    $this->addOrderLog($order['order_id'], $order_status, "您的订单已支付成功,我们将尽快进行处理,订单编号:" . $order['order_no'], 1);
                    //添加用户与行政中心关系
                    $this->userService->addUserCenter($order['user_id'], $order['province_code'], $order['city_code'], $order['area_code'], $order['company_id']);
                    //添加物流信息
                    $this->addOrderLogistics($order['order_id'], 1, "您的订单开始处理", $order['user_id']);
                    //新增资金记录
                    $finance_record['type'] = 1;
                    $finance_record['order_id'] = $order['order_id'];
                    $finance_record['user_id'] = $order['user_id'];
                    $finance_record['money'] = $order['order_actual_money'];
                    $finance_record['create_time'] = time();
                    $finance_record['is_online_pay'] = 1;
                    $finance_record['pay_way'] = 2;
                    $finance_record['callback_num'] = $order['ali_callback_num'];
                    $finance_record['payment_method'] = 1;
                    $finance_record['payment_uid'] = $order['user_id'];
                    $finance_record['o_id'] = $order['operation_id'];
                    $finance_record['a_id'] = $order['administrative_id'];
                    $finance_record['company_id'] = $order['company_id'];
                    $this->finance_record_model->add($finance_record);


                    $status = true;
                });
                if ($status) {
                    $user_info = $this->user_model->getOne(array('user_id' => $order['user_id']), 'openid,telphone,user_id,source');
                    $eq_info = $this->equipment_model->getDetail(array('equipments_id' => $order['equipments_id']), 'equipments_name,model');
                    if (empty($user_info) || empty($eq_info)) {
                        return $this->response->end('fail');
                    }
                    $a_info = $this->userService->getOwnArea($user_info['user_id'], $user_info['source'], $order['province_code'], $order['city_code'], $order['area_code'], $order['company_id']);

                    //发邮件通知
                    if (1 == 0) {//暂时关闭
                        $order_time = date('Y-m-d H:i:s', $order['create_time']);
                        $opertaor = $this->config->get('order_opertaor')[$order['opertaor']];//订单来源
                        $content = '<h2>新订单通知</h2><br/><div>订单编号:' . $order['order_no'] . '</div><div>订单来源:' . $opertaor . '</div><div>客户电话:' . $user_info['telphone'] . '</div><div>商品名称:' . $eq_info['equipments_name'] . '(数量:' . $order['equipments_num'] . ')</div><div>订单金额:' . $order['order_actual_money'] . '元</div><div>下单时间:' . $order_time . '</div><div style="color:#808080">客户已完成支付,请进入管理后台查看详情!</div>';
                        //总后台
                        $admin_email = $this->admin_user_model->getOne(['is_admin' => 1, 'is_delete' => 0], 'notice_email');
                        if (!empty($admin_email) && !empty($admin_email['notice_email'])) {
                            EmailService::sendEmailAsync($admin_email['notice_email'], $content, "有新订单啦", '', '', '', $order['company_id']);
                        }
                        if (!empty($a_info)) {
                            //对应运营中心
                            $op_email = $this->op_user_model->getOne(['o_id' => $a_info['operation'], 'is_delete' => 0, 'is_admin' => 1], 'notice_email');
                            if (!empty($op_email) && !empty($op_email['notice_email'])) {
                                EmailService::sendEmailAsync($op_email['notice_email'], $content, "有新订单啦", '', '', '', $order['company_id']);
                            }
                            //对应合伙人
                            $administrative_email = $this->administrative_user_model->getOne(['a_id' => $a_info['a_id'], 'is_delete' => 0, 'is_admin' => 1], 'notice_email');
                            if (!empty($administrative_email) && !empty($administrative_email['notice_email'])) {
                                EmailService::sendEmailAsync($administrative_email['notice_email'], $content, "有新订单啦", '', '', '', $order['company_id']);
                            }
                        }
                    }

                    return $this->response->end('SUCCESS');
                }
            }
        }
        return $this->response->end('fail');
    }


    //缴费支付回调
    public function http_feeOrder()
    {
        //验证微信签名
        $postStr = $this->http_input->getRawContent();
        file_put_contents('fee_order.txt', $postStr);
        //$postStr = file_get_contents('fee_order.txt');
        $postObj = simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA);
        if ($postObj->result_code == 'SUCCESS' && $postObj->return_code == 'SUCCESS') {
            //判断锁
            $feeOrderLock = $this->redis->hGet('feeOrderLock', $postObj->out_trade_no);
            if (!empty($feeOrderLock)) {
                $return['return_code'] = 'FAIL';
                $return['return_msg'] = 'error';
                $return = $this->arrayToXml($return);
                return $this->response->end($return);
            }
            $order_sn = $postObj->out_trade_no;

            $order = $this->fee_order_model->getOne(['order_sn' => $order_sn], '*');
            if (empty($order)) {
                $return['return_code'] = 'FAIL';
                $return['return_msg'] = 'error';
                $return = $this->arrayToXml($return);
                return $this->response->end($return);
            }
            if ($order['pay_status'] == 2) {
                $return['return_code'] = 'SUCCESS';
                $return['return_msg'] = 'OK';
                $return = $this->arrayToXml($return);
                return $this->response->end($return);
            }
            /* if($order['pay_mode']==2){
                 $wx_config = $this->service_payment_config;
             }else{
                 $company_config = CompanyService::getCompanyConfig($order['company_id']);
                 $wx_config = $company_config['wx_config'];
             }*/
            $company_config = CompanyService::getCompanyConfig($order['company_id']);
            $wx_config = $company_config['wx_config'];
            $wx_config['payment_type'] = 'JSAPI';
            //$WxPay = new WxPay($wx_config);
            $WxPay = WxPay::getInstance($wx_config);
            if ($WxPay->isSetConfig) $WxPay->setConfig($wx_config);
            $signObj = [];
            foreach ($postObj as $key => $val) {
                if ($key != 'sign') {
                    $signObj[$key] = $val;
                }
            }
            //验证签名
            if ($WxPay->getSign($signObj) == $postObj->sign) {
                $order['wx_callback_num'] = $postObj->transaction_id;
                $total_fee = $postObj->total_fee;

                // 获取用户信息
                $user_info = $this->user_model->getOne(array('user_id' => $order['user_id']), 'username,openid,telphone');
                $status = false;
                $this->redis->hSet('feeOrderLock', $postObj->out_trade_no, json_encode($postStr));//加锁
                $this->db->begin(function () use ($order, &$status, $postObj, $postStr, $user_info, $total_fee) {
                    //缴费
                    $this->fee($order, $total_fee);
                    $status = true;
                });
                $bill_ids = explode(',',$order['bill_id']);
                foreach ($bill_ids as $k => $v) {
                    if(empty($v)){
                        continue;
                    }
                    $result = $this->settlementModel->settlement($v);//结算
                    if ($result['code'] != 1000) {
                        var_dump($v . ':结算异常>>' . $result['msg']);
                    }
                }
                //下发套餐
                //查询设备号
                $device_no = $this->equipmentListsModel->getOne(['customer_code'=>$order['customer_code']],'device_no');
                //账单到期时间
                $customer_code_bull_where = ['customer_code' => $order['customer_code'], 'status' => 2, 'is_delete' => 0];
                $customer_code_bull_field = 'cycle_end_time';
                $customer_code_bull_order = ['cycle_end_time' => 'DESC'];
                $bill_info = $this->customer_code_bill_model->getAll($customer_code_bull_where,$customer_code_bull_field, $customer_code_bull_order);
                if (!empty($device_no) && !empty($bill_info)){
                    //处理下发时间
                    $month_date = getthemonth(date('Y-m-d', $bill_info[0]['cycle_end_time']));
                    $expire_date = $month_date[1];//下个月1号到期
                    $shutdown_date = date('Y-m-15', strtotime('next month', $bill_info[0]['cycle_end_time']));//下个月15号停机
                    $params = ['sn' => $device_no['device_no'],'expire_date'=>$expire_date,'shutdown_date'=>$shutdown_date];
                    $result = DeviceService::Thrash($params, 'binding_package');
                }

                $this->redis->hDel('feeOrderLock', $postObj->out_trade_no);//解锁
                if ($status) {
                    //缴费成功通知到用户
                    $user_notice_config = ConfigService::getTemplateConfig('user_new_order', 'user', $order['company_id']);
                    if (!empty($user_notice_config) && !empty($user_notice_config['template']) && $user_notice_config['template']['switch']) {
                        $template_id = $user_notice_config['template']['wx_template_id'];
                        $mp_template_id = $user_notice_config['template']['mp_template_id'];
                        //数据组装
                        $pay_way = '微信支付';
                        $order_time = date('Y-m-d H:i:s', $order['create_time']);

                        //通知数据组装
                        $template_data = [
                            'keyword1' => ['value' => $order['order_sn']],
                            'keyword2' => ['value' => $order['customer_code']],
                            'keyword3' => ['value' => 1],
                            'keyword4' => ['value' => 1],
                            'keyword5' => ['value' => formatMoney($total_fee, 2)],
                            'keyword6' => ['value' => $pay_way],
                            'keyword7' => ['value' => $order_time],
                            'keyword8' => ['value' => '您的订单已经支付成功，感谢您对我们的的支持。若有疑问可致电官方客服,客服电话:' . ConfigService::getConfig('company_hotline', false, $order['company_id'])]
                        ];
                        $template_tel = $user_info['telphone'];
                        $template_content = '您的订单(订单编号:' . $order['order_sn'] . ',户号:' . $order['customer_code'] . ')已完成支付，金额为' . formatMoney($order['payment_money'], 2) . '元。打开微信进入' . $this->wx_config['app_wx_name'] . '可查看支付方式、下单时间等支付详情';
                        $template_url = 'pages/guidance/guidance?code=' . $order['customer_code'];
                        $weapp_template_keyword = '';
                        $mp_template_data = [
                            'first' => ['value' => '您的订单已经支付成功，感谢您对我们的支持', 'color' => '#4e4747'],
                            'keyword1' => ['value' => $order['customer_code'], 'color' => '#4e4747'],
                            'keyword2' => ['value' => $order['order_sn'], 'color' => '#4e4747'],
                            'keyword3' => ['value' => formatMoney($total_fee, 2) . '元', 'color' => '#4e4747'],
                            'remark' => ['value' => '若有疑问可致电官方客服,客服电话:' . ConfigService::getConfig('company_hotline', false, $order['company_id']), 'color' => '#173177'],
                        ];
                        NotifiyService::sendNotifiyAsync($user_info['openid'], $template_data, $template_id, $template_tel, $template_content, $template_url, $weapp_template_keyword, $mp_template_id, $mp_template_data, 'user', $order['company_id']);
                    }
                    $return['return_code'] = 'SUCCESS';
                    $return['return_msg'] = 'OK';
                    $return = $WxPay->arrayToXml($return);
                    return $this->response->end($return);
                }
            }
        }
        $return['return_code'] = 'FAIL';
        $return['return_msg'] = 'error';
        $return = $WxPay->arrayToXml($return);
        return $this->response->end($return);
    }

    /**
     * @desc  缴费金额为0时回调
     * @param order_sn string 必选 订单编号
     * @param money string 必选 金额
     * @date   2020-6-19
     * @return [type]     [description]
     * @author lcx
     */
    public function http_feeCallback()
    {
        //接收参数
        $param = json_decode($this->http_input->getRawContent(), true);

        $order_sn = $param['order_sn'];
        $total_fee = $param['money'];

        //判断锁
        $feeCallbackLock = $this->redis->hGet('feeCallbackLock', $order_sn);
        if (!empty($feeCallbackLock)) {
            return false;
        }
        //获取订单信息
        $order = $this->fee_order_model->getOne(['order_sn' => $order_sn], '*');
        $this->db->begin(function () use ($order,$order_sn, &$status, $total_fee) {
            $this->redis->hSet('feeCallbackLock', $order_sn, 1);//加锁
            $this->fee($order, $total_fee);
            $status = true;
        });
        $bill_ids = explode(',',$order['bill_id']);
        foreach ($bill_ids as $k => $v) {
            if(empty($v)){
                continue;
            }
            $result = $this->settlementModel->settlement($v);//结算
            if ($result['code'] != 1000) {
                var_dump($v . ':结算异常>>' . $result['msg']);
            }
        }

        $this->redis->hDel('feeCallbackLock', $order_sn);//解锁
        if ($status) {
            return true;
        }
        return false;

    }

    //缴费公共处理方法
    public function fee($order, $total_fee)
    {
        //查询优惠券信息
        if (!empty($order['coupon_id'])) {
            $coupon_id = json_decode($order['coupon_id'], true);
            //修改优惠券信息
            $this->coupon_model->save(['coupon_id' => ['IN', $coupon_id]], ['status' => 2, 'use_time' => time()]);
        }
        // 1-- 修改订单状态
        $this->fee_order_model->save(['id' => $order['id']], ['pay_status' => 2, 'pay_time' => time(), 'wx_callback_num' => $order['wx_callback_num'], 'payment_money' => $total_fee]);
        //修改账单信息表
        $order['bill_id'] = explode(',',$order['bill_id']);
        $this->customer_code_bill_model->save(['bill_id' => ['IN', $order['bill_id']]], ['status' => 2]);
        //新增资金记录
        //查询户号所属合同的区域信息
        $join = [
            ['contract c', 'c.contract_id=rq_customer_code.contract_id', 'left']
        ];
        $contract_info = $this->customer_code_model->getOne(['rq_customer_code.code' => $order['customer_code'], 'rq_customer_code.is_delete' => 0], 'administrative_id,operation_id', $join);
        $finance_record['type'] = 4;
        $finance_record['fee_order_id'] = $order['id'];
        $finance_record['user_id'] = $order['user_id'];
        $finance_record['money'] = $total_fee;
        $finance_record['create_time'] = time();
        $finance_record['is_online_pay'] = 1;
        $finance_record['pay_way'] = 1;
        $finance_record['callback_num'] = $order['wx_callback_num'];
        $finance_record['payment_method'] = 1;
        $finance_record['payment_uid'] = $order['user_id'];
        $finance_record['o_id'] = $contract_info['operation_id'] ?? 0;
        $finance_record['a_id'] = $contract_info['administrative_id'] ?? 0;
        $finance_record['company_id'] = $order['company_id'];
        $this->finance_record_model->add($finance_record);

    }


    /**
     * @desc   添加订单操作日志
     * @param
     * @date   2018-07-19
     * @return [type]     [description]
     * @author lcx
     */
    public function addOrderLog($order_id, $status, $remark, $is_to_user = 0)
    {
        $data['operater_role'] = 1;
        $data['order_id'] = $order_id;
        $data['status'] = $status;
        $data['create_time'] = time();
        $data['remark'] = $remark;
        $data['is_to_user'] = $is_to_user;
        $res = $this->log_model->add($data);
        return $res;
    }

    /**
     * @desc   添加主板日志
     * @param
     * @date   2018-07-19
     * @return [type]     [description]
     * @author lcx
     */
    public function addEquipmentLog($user_id, $equipment_id, $type, $remark)
    {
        $data['user_id'] = $user_id;
        $data['equipment_id'] = $equipment_id;
        $data['type'] = $type;
        $data['user_role'] = 1;
        $data['create_time'] = time();
        $data['remark'] = $remark;
        $res = $this->eq_log_model->add($data);
        return $res;
    }

    /**
     * @desc   添加合同操作日志
     * @param $status 1增2删3改4解绑'
     * @date   2018-07-19
     * @author lcx
     * @return [type]     [description]
     */
    public function addContractLog($contract_id, $status = 1, $remark, $user_id)
    {
        $data['contract_id'] = $contract_id;
        $data['do_id'] = $user_id;
        $data['do_type'] = $status;
        $data['terminal_type'] = 3;
        $data['do_time'] = time();
        $data['remark'] = $remark;
        $res = $this->contract_log_model->add($data);
        return $res;
    }

    /**
     * @desc   添加订单物流信息
     * @param $status
     * @date   2018-07-19
     * @return [type]     [description]
     * @author lcx
     */
    public function addOrderLogistics($order_id, $status_code, $remark, $operator_uid)
    {
        $data['order_id'] = $order_id;
        $data['status_code'] = $status_code;
        $data['status_name'] = $this->config->get('order_logistics_code')[$status_code];
        $data['remark'] = $remark;
        $data['create_time'] = time();
        $data['operator_role'] = 3;
        $data['operator_uid'] = $operator_uid;
        $res = $this->order_logistics_model->add($data);
        //修改订单物流状态
        $this->order_model->save(array('order_id' => $order_id), array('logistics_status' => $status_code));
        return $res;
    }

    /**
     * 对象 转 数组
     * @param object $obj 对象
     * @return array
     */
    protected function object_to_array($obj)
    {
        $obj = (array)$obj;
        foreach ($obj as $k => $v) {
            if (gettype($v) == 'resource') {
                return;
            }
            if (gettype($v) == 'object' || gettype($v) == 'array') {
                $obj[$k] = (array)object_to_array($v);
            }
        }

        return $obj;
    }

    function arrayToXml($arr)
    {
        $xml = "<xml>";
        foreach ($arr as $key => $val) {
            if (is_numeric($val)) {
                $xml .= "<" . $key . ">" . $val . "</" . $key . ">";
            } else {
                $xml .= "<" . $key . "><![CDATA[" . $val . "]]></" . $key . ">";
            }
        }
        $xml .= "</xml>";

        return $xml;
    }


}
