<?php

namespace app\Controllers\User;

/**
 * 投诉
 */
class Complaint extends Base {

    protected $complaint_model;
    protected $complaint_type_model;

    public function initialization($controller_name, $method_name) {
        parent::initialization($controller_name, $method_name);
        $this->complaint_model = $this->loader->model('ComplaintModel', $this);
        $this->complaint_type_model = $this->loader->model('ComplaintTypeModel', $this);
    }

    /**
     * showdoc
     * @catalog API文档/用户端/投诉相关
     * @title 新增投诉记录
     * @description 
     * @method POST
     * @url User/Complaint/add
     * @param complaint_type_id 必选 int 投诉类型ID
     * @param complaint_object 必选 int 投诉对象  1 对订单投诉 2对工单投诉 3 对产品投诉 4对工程人员投诉
     * @param content 必选 string 投诉内容
     * @param pic 可选 array 图片
     * @return {"code": 1000, "message": "提交成功，我们将尽快为您处理","data": ""}
     * @remark {"complaint_type_id":1,"complaint_object":4,"order_id":86,"content":"22222222"}
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_add() {
        if (empty($this->parm['complaint_type_id'] ?? '')) {
            return $this->jsonend(-1001, "缺少投诉类型ID");
        }
        if (empty($this->parm['complaint_object'] ?? '')) {
            return $this->jsonend(-1001, "缺少投诉对象");
        }
        switch ($this->parm['complaint_object']) {
            case 1:
                if (empty($this->parm['order_id'] ?? '')) {
                    return $this->jsonend(-1001, "缺少参数订单ID");
                }
                $data['order_id'] = $this->parm['order_id'];
                break;
            case 2:
                if (empty($this->parm['work_order_id'] ?? '')) {
                    return $this->jsonend(-1001, "缺少参数工单ID");
                }
                $data['work_order_id'] = $this->parm['work_order_id'];
                break;
            case 3:
                if (empty($this->parm['equipments_id'] ?? '')) {
                    return $this->jsonend(-1001, "缺少参数产品ID");
                }
                $data['equipments_id'] = $this->parm['equipments_id'];
                break;
            case 2:
                if (empty($this->parm['engineers_id'] ?? '')) {
                    return $this->jsonend(-1001, "缺少参数工程人员ID");
                }
                $data['engineers_id'] = $this->parm['engineers_id'];
                break;
        }
        if (empty($this->parm['content'] ?? '')) {
            return $this->jsonend(-1001, "请填写投诉内容");
        }
        $data['user_id'] = $this->user_id;
        $data['complaint_type_id'] = $this->parm['complaint_type_id'];
        $data['complaint_object'] = $this->parm['complaint_object'];
        $data['content'] = $this->parm['content'];
        if (!empty($this->parm['pic'] ?? '')) {
            $data['pic'] = json_encode($this->parm['pic']);
        }
//        $data['status'] = 1;
        $data['create_time'] = time();
        $res = $this->complaint_model->add($data);
        if ($res) {
            return $this->jsonend(1000, "提交成功，我们将尽快为您处理");
        }
        return $this->jsonend(-1000, "提交失败");
    }

    /**
     * showdoc
     * @catalog API文档/用户端/投诉相关
     * @title 获取投诉类型列表
     * @description 
     * @method POST
     * @url User/Complaint/getTypeList
     * @param object 可选 投诉对象  1 对订单投诉 2对工单投诉 3 对产品投诉 4对工程人员投诉
     * @return {"code": 1000,"message": "获取数据成功","data": [{"complaint_type_id": "1","object": "1", "name": "产品有问题","status": "1", "sort": "0","create_time": "1536646518","is_del": "0"} ]}
     * @return_param complaint_type_id int 类型ID
     * @return_param object int 对象 1 对订单投诉 2对工单投诉 3 对产品投诉 4对工程人员投诉
     * @return_param name string 类型名称
     * @remark 
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_getTypeList() {
        if (!empty($this->parm['object'] ?? '')) {
            $map['object'] = ['IN', [$this->parm['object'], 5]];
        }
        $map['status'] = 1;
        $map['is_del'] = 0;
        $map['company_id'] = $this->company;
        $data = $this->complaint_type_model->getAll($map);
        if ($data) {
            return $this->jsonend(1000, "获取数据成功", $data);
        }
        return $this->jsonend(-1003, "暂无相关数据");
    }

}
