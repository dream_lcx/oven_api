<?php
/**
 * Created by PhpStorm.
 * User: LG
 * Date: 2019/2/13
 * Time: 15:47
 */

namespace app\Controllers\Engineer;

use app\Library\SLog;
use app\Services\Common\ConfigService;
use app\Services\Common\ReturnCodeService;
use app\Services\Log\FileLogService;
use app\Services\User\UserService;
use Server\Components\CatCache\CatCacheRpcProxy;

class Account extends Base
{
    protected $Achievement;
    protected $UserService;
    protected $bankCardModel;
    protected $withdrawModel;
    protected $extensionModel;
    protected $customerCodeBillModel;
    public function initialization($controller_name, $method_name)
    {
        parent::initialization($controller_name, $method_name); // TODO: Change the autogenerated stub
        $this->Achievement = $this->loader->model('AchievementModel', $this);
        $this->bankCardModel = $this->loader->model('BankCardModel', $this);
        $this->withdrawModel = $this->loader->model('WithdrawModel', $this);
        $this->extensionModel = $this->loader->model('ExtensionModel',$this);
        $this->customerCodeBillModel = $this->loader->model('CustomerCodeBillModel',$this);
        $this->Achievement->table = '';
        $this->UserService = new UserService();
    }


    /**
     * 获取工程人员信息
     * 请求参数     请求类型      是否必须      说明
     * @author ligang
     * @date 2019/2/13 15:49
     */
    public function http_engineers()
    {
        $where = [
            'engineers_id' => $this->user_id
        ];
        $field = '*';
        $this->Achievement->table = 'engineers';
        $result = $this->Achievement->findData($where, $field);
        $is_have_bank = false;
        if (!empty($result)) {
            $count = $this->bankCardModel->count(['uid' => $this->user_id, 'role' => 5, 'is_delete' => 0]);
            if ($count > 0) {
                $is_have_bank = true;
            }
        }
        $result['is_have_bank'] = $is_have_bank;
        $result['money'] = formatMoney($result['money']);
        $result['temp_money'] = formatMoney($result['temp_money']);
        $result['total_money'] = formatMoney($result['total_money']);

        return $this->jsonend(1000, '获取成功', $result);
    }

    /**
     * 获取银行卡列表（选择所属银行）
     * @author ligang
     * @date 2019/1/11 10:19
     */
    public function http_banks()
    {
        $bank = $this->config->get('banks');
        return $this->jsonend(1000, '获取成功', $bank);
    }

    /**
     * 添加兑换账户
     * 请求参数         请求类型      是否必须          说明
     * bank_type        int             是           类型，1：银行卡，2：微信，3：支付宝
     * bank_number      string          是           银行卡号
     * bank_name        string          是           开户行，bank_type为1时必须
     * bank_code        string          是           银行代码，bank_type为1时必须
     * name             string          是           持卡人姓名
     * id_card          string          是           身份证号，bank_type为1时必须
     * bank_phone       string          是           手机号码
     * address          string          是           开户地，bank_type为1时必须
     * @author ligang
     * @date 2019/1/11 9:59
     */
    public function http_addBank()
    {
        $result = $this->bankCardModel->addBankCard($this->parm, $this->user_id, 5);
        return $this->jsonend($result['code'], $result['msg'], $result['data']);
    }

    /**
     * 市场推广银行卡列表
     * 请求参数     请求类型      是否必须      说明
     * row          int             否           每页显示条数
     * page         int             否           当前页
     * @author ligang
     * @date 2019/1/11 10:52
     */
    public function http_bankList()
    {
        $row = $this->parm['row'] ?? 10;
        $page = $this->parm['page'] ?? 1;
        $where = [
            'uid' => $this->user_id,
            'role' => 5,
            'is_delete' => 0
        ];
        $result = $this->bankCardModel->getBankCardList($where, '', [], $page, $row);
        if ($result['code'] != ReturnCodeService::SUCCESS) {
            return $this->jsonend($result['code'], $result['msg']);
        }
        $data = [
            'data' => $result['data'],
            'page' => [
                'current_page' => $result['page'],
                'row' => $result['pageSize'],
                'total' => $result['count'],
                'total_page' => $result['allPage']
            ]
        ];
        return $this->jsonend(1000, '获取成功', $data);
    }

    /**
     * 获取银行卡详情
     * 请求参数     请求类型      是否必须      说明
     * bank_id      int             是           银行卡ID
     * @author ligang
     * @date 2019/1/11 11:02
     */
    public function http_getBankInfo()
    {
        $bank_id = $this->parm['bank_id'] ?? '';
        $where = [
            'bank_id' => $bank_id,
            'is_delete' => 0
        ];
        $result = $this->bankCardModel->getBankCardDetail($where);
        if ($result['code'] != ReturnCodeService::SUCCESS) {
            return $this->jsonend($result['code'], $result['msg']);
        }
        return $this->jsonend(ReturnCodeService::SUCCESS, '获取成功', $result['data']);
    }

    /**
     * 删除银行卡
     * 请求参数     请求类型      是否必须      说明
     * bank_id      int             是           银行卡ID
     * @author ligang
     * @date 2019/1/11 11:06
     */
    public function http_deleteBank()
    {
        $bank_id = $this->parm['bank_id'] ?? '';
        $where = [
            'bank_id' => $bank_id,
            'is_delete' => 0
        ];
        $result = $this->bankCardModel->delBankCard($where);
        if ($result['code'] != ReturnCodeService::SUCCESS) {
            return $this->jsonend($result['code'], $result['msg']);
        }
        return $this->jsonend(ReturnCodeService::SUCCESS, $result['msg']);
    }

    /**
     * 编辑银行卡
     * 请求参数         请求类型      是否必须      说明
     * bank_id          int             是           银行卡ID
     * bank_type        int             是           类型，1：银行卡，2：微信，3：支付宝
     * bank_number      string          是           银行卡号
     * bank_name        string          是           开户行，bank_type为1时必须
     * bank_code        string          是           银行代码，bank_type为1时必须
     * name             string          是           持卡人姓名
     * id_card        string          是           身份证号，bank_type为1时必须
     * bank_phone       string          是           手机号码
     * address          string          是           开户地，bank_type为1时必须
     * @author ligang
     * @date time
     */
    public function http_editBank()
    {
        $result = $this->bankCardModel->editBankCard($this->parm);
        if ($result['code'] != ReturnCodeService::SUCCESS) {
            return $this->jsonend($result['code'], $result['msg']);
        }
        return $this->jsonend(ReturnCodeService::SUCCESS, $result['msg']);
    }

    /**
     * 兑换信息
     * 请求参数     请求类型      是否必须      说明
     * money        float           否           兑换金额
     * @author ligang
     * @date 2019/1/11 11:26
     */
    public function http_withdrawDepositInfo()
    {
        $this->Achievement->table = 'engineers';
        $engineers_info = $this->Achievement->findData(['engineers_id' => $this->user_id], 'engineers_id,money');
        if (empty($engineers_info)) {
            return $this->jsonend(-1000, '工程人员不存在');
        }
        $engineers_info['money'] = formatMoney($engineers_info['money'],2);
        $field = 'bank_id,bank_type,bank_name,bank_number,bank_phone';
        $order = [
            '`default`' => 'ASC',
            'add_time' => 'DESC',
            'update_time' => 'DESC'
        ];
        $banks = $this->bankCardModel->getAll(['uid' => $this->user_id, 'is_delete' => 0, 'role' => 5], [], 1, -1, $field, $order);
        if (!empty($banks)) {
            foreach ($banks as $k => $v) {
                $banks[$k]['bank_last_four_number'] = substr($v['bank_number'], strlen($v['bank_number']) - 4, 4);
            }
        }
        //获取兑换账户类型配置
        $config = ConfigService::getBalanceConfig('', trim('cash_account_type'), $this->company, 1);
        $support_bank = !empty($config[0]) ? true : false;
        $support_weixin = !empty($config[1]) ? true : false;
        $support_alipay = !empty($config[2]) ? true : false;
        $data = [
            'workers_info' => $engineers_info,
            'banks' => $banks,
            'withdraw_config' => ['support_weixin' => $support_weixin, 'support_bank' => $support_bank, 'support_alipay' => $support_alipay]
        ];
        return $this->jsonend(1000, '获取成功', $data);
    }

    /**
     * 发起兑换
     * 请求参数     请求类型      是否必须      说明
     * account      int             是          账户,若account_type=1传银行卡ID,account_type=2时传微信openid
     * money        float           是           原始兑换金额（用户输入的金额，不是计算后的）
     * @author ligang
     * @date 2019/1/11 11:35
     */
    public function http_withdrawDepositApply()
    {
        $account = $this->parm['account'] ?? '';
        $money = $this->parm['money'] ?? '';
        $account_type = $this->parm['account_type'] ?? 1;
        $sms_code = $this->parm['sms_code'] ?? '';
        if (empty($account)) {
            return $this->jsonend(-1000, '请选择兑换账户');
        }
        if (empty($money)) {
            return $this->jsonend(-1000, '请请输入兑换金额');
        }
        if (empty($sms_code)) {
            return $this->jsonend(-1000, '请请输入验证码');
        }
        if (empty($this->parm['phone'])) {
            return $this->jsonend(-1000, '缺少参数手机号');
        }
        $cache_code = CatCacheRpcProxy::getRpc()->offsetGet($this->parm['phone']);
        if ($cache_code['code'] != $sms_code) {
            return $this->jsonend(-1000, '验证码错误');
        }
        $this->Achievement->table = 'engineers';
        $engineers_info = $this->Achievement->findData(['engineers_id' => $this->user_id], 'engineers_id,money');
        if (empty($engineers_info)) {
            return $this->jsonend(-1000, '工程人员不存在');
        }
        if ($account_type == 1) {
            $bank_info = $this->bankCardModel->getOne(['bank_id' => $account, 'is_delete' => 0], 'bank_id,is_delete');
            if (empty($bank_info)) {
                return $this->jsonend(-1000, '银行卡不存在');
            }
        }

        //获取兑换配置信息
        $this->Achievement->table = 'oa_balance_config';
        $payment_fee = $this->Achievement->findData(['`key`' => 'payment_fee', '`state`' => 1, 'is_delete' => 0]);
        if (empty($payment_fee)) {
            return $this->jsonend(-1000, '兑换功能已被关闭');
        }
        if ($payment_fee['type'] == 2 && (time() < $payment_fee['start_time'] || time() > $payment_fee['end_time'])) {
            return $this->jsonend(-1000, '请在有效的时间范围内兑换');
        }
        $payment_fee['value'] = json_decode($payment_fee['value'], 1);
        $service_param['balance'] = formatMoney($engineers_info['money'],2);
        $service_param['money'] = $money;
        $service_param['min_money'] = $payment_fee['value']['min_money'];
        $service_param['max_money'] = $payment_fee['value']['max_money'];
        $service_param['min_fee'] = $payment_fee['value']['min'];
        $service_param['max_fee'] = $payment_fee['value']['max'];
        $service_param['fee_rate'] = $account_type==2?$payment_fee['value']['wx_percentage']:$payment_fee['value']['percentage'];
        $service_param['account_type'] = $account_type;
        $serviceResult = serviceFee($service_param);
        if ($serviceResult['code'] != ReturnCodeService::SUCCESS) {
            return $this->jsonend($serviceResult['code'], $serviceResult['msg']);
        }
        $serviceResult = $serviceResult['data'];
        $update_data = [
            'money' => floatval($engineers_info['money']) - $serviceResult['deduct_money'],
        ];
        $data = [
            'cash_order_number' => pay_sn('CE'),
            'uid' => $this->user_id,
            'role' => 5,
            'cash_order_money' => formatMoney($money,1),
            'phone'=>$this->parm['phone'],
            'balance' => $update_data['money'],
            'account_type' => $account_type,
            'payment_fee' => $serviceResult['fee'],
            'remit_money' => $serviceResult['remit_money'],
            'add_time' => time(),
        ];
        if ($account_type == 1) {
            $data['bank_id'] = $bank_info['bank_id'];
        } else if ($account_type == 2) {
            $data['wx_openid'] = $account;
        }
        $check = false;
        $redis_lock = $this->redis->get('cloud_engineer_withdraw_' . $this->user_id);
        if ($redis_lock) {
            return $this->jsonend(-1000, '请勿频繁操作,60s后重试');
        }
        $this->redis->set('cloud_engineer_withdraw_' . $this->user_id, json_encode($data), 60);
        $this->db->begin(function () use (&$check, $update_data, $data, $money, $serviceResult) {
            $this->Achievement->table = 'engineers';
            $this->Achievement->updateData(['engineers_id' => $this->user_id], $update_data);
            $insert_id = $this->withdrawModel->add($data);
            //写入资金变动记录
            $funds_record = [
                'user_id' => $this->user_id,
                'user_type' => 2,
                'source' => 1,
                'account_type' => 1,
                'order_id' => $insert_id,
                'money' => formatMoney($money,1),
                'note' => '兑换包含手续费[' . number_format($serviceResult['fee'], 2) . ']',
                'add_time' => time(),
            ];
            $this->Achievement->table = 'funds_record';
            $this->Achievement->insertData($funds_record);
            $check = true;
        }, function ($e) {
            FileLogService::WriteLog(SLog::ERROR_LOG, 'withdraw', '兑换失败' . $e->error);
            return $this->jsonend('-1000', '兑换失败', $e->error);
        });

        if ($check) {
            return $this->jsonend('1000', '兑换成功，请等待审核');
        }
        return $this->jsonend('-1000', '兑换失败');
    }

    /**
     * 兑换记录
     * 请求参数     请求类型      是否必须      说明
     * row          int             否           每页显示数目
     * page         int             否           当前页
     * @author ligang
     * @date 2019/1/11 14:17
     */
    public function http_withdrawDepositRecord()
    {
        $row = $this->parm['row'] ?? 10;
        $page = $this->parm['page'] ?? 1;


        $where = [
            'rq_withdraw.uid' => $this->user_id,
            'rq_withdraw.role'=>5
        ];
        $join = [
            ['bank_card', 'rq_withdraw.bank_id = rq_bank_card.bank_id', 'LEFT']
        ];
        $field = '
            rq_withdraw.cash_order_money,
            rq_withdraw.cash_order_state,
            rq_withdraw.remarks,
            rq_withdraw.bank_note,
            rq_withdraw.complete_time,
            rq_withdraw.play_time,
            rq_withdraw.examine_pass,
            rq_withdraw.balance,
            rq_withdraw.payment_fee,
            rq_withdraw.add_time,
            rq_withdraw.play_money_state,
            rq_withdraw.wx_openid,
            rq_withdraw.remit_money,
            rq_withdraw.account_type,
            rq_bank_card.bank_number
        ';
        $order = [
            'add_time' => 'DESC',
            'cash_order_state' => 'DESC',
            'play_money_state' => 'ASC',
        ];
        $result = $this->withdrawModel->record($where, $field, $join, $page, $row, $order);
        if ($result['code'] != ReturnCodeService::SUCCESS) {
            return $this->jsonend($result['code'], $result['msg']);
        }
        $data = [
            'data' => $result['data'],
            'page' => [
                'current_page' => $result['page'],
                'row' => $result['pageSize'],
                'total' => $result['count'],
                'total_page' => $result['allPage']
            ]
        ];
        return $this->jsonend(1000, '获取成功', $data);

    }

    /**
     * 获取工程人员业绩明细
     * 请求参数     请求类型      是否必须      说明
     * row          int             否           每页显示数目
     * page         int             否           当前页
     * @author tx
     * @date 2020/4/23
     */
    function http_achievementRecord()
    {
        $row = $this->parm['row'] ?? 10;
        $page = $this->parm['page'] ?? 1;
        $keywords = $this->parm['keywords'] ?? '';

        if (!empty($keywords)) {
            $where['e.customer_code|b.bill_number|c.username|c.realname|c.telphone'] = ['like', '%' . $keywords . '%'];
        }

        $where['rq_oa_achievement_workers.workers_id'] = $this->user_id;
        $where['rq_oa_achievement_workers.role'] = 2;//工程端

        $join = [
            ['oa_achievement e', 'rq_oa_achievement_workers.achievement_id = e.achievement_id', 'LEFT'],
            ['customer_code_bill b', 'b.bill_id = e.form_bill_id', 'LEFT'],
            ['customer_code d', 'd.code = b.customer_code', 'LEFT'],
            ['customer c', 'c.user_id = d.user_id', 'LEFT'],
        ];
        $field = '
            rq_oa_achievement_workers.source,
            rq_oa_achievement_workers.role,
            rq_oa_achievement_workers.money,
            rq_oa_achievement_workers.is_available,
            rq_oa_achievement_workers.transfer_time,
            e.achievement_type,
            e.customer_code,
            e.form_bill_id,
            e.add_time,
            b.bill_number,
            c.username,
            c.realname,
            c.telphone
        ';
        $order = [
            'e.add_time' => 'DESC',
        ];

        $this->Achievement->table = 'oa_achievement_workers';

        $result = $this->Achievement->selectPageData($where, $field, $join, $row, $page, $order);

        if(empty($result)){
            return $this->jsonend(-1000, '暂无数据');
        }
        foreach ($result as $key => $value) {
            $result[$key]['add_time'] = $value['add_time'] ? date('Y/m/d H:i', $value['add_time']) : '--';
            $result[$key]['transfer_time'] = $value['transfer_time'] ? date('Y/m/d H:i', $value['transfer_time']) : '--';
            $result[$key]['money'] = formatMoney($value['money'],2);
            $result[$key]['achievement_type_desc'] = $value['achievement_type'] == 1 ? '新装' : '缴费';
            $result[$key]['source'] = $value['source']?$this->config->get('achievement_source')[$value['source']]:'';
            $result[$key]['username'] = empty($value['realname'])?$value['username']:$value['realname'];
        }
        $count = $this->Achievement->findJoinData($where, 'COUNT(*) AS number', $join);
        $count = $count['number'] ?? 0;
        $data = [
            'data' => $result,
            'page' => [
                'current_page' => $page,
                'row' => $row,
                'total' => $count,
                'total_page' => ceil($count / $row)
            ]
        ];
        return $this->jsonend(1000, '获取成功', $data);

    }










}