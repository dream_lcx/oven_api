<?php

namespace app\Controllers\Engineer;

use app\Library\SLog;
use app\Services\Log\AsyncFile;
use app\Services\Log\FileLogService;
use Server\CoreBase\Controller;
use Server\CoreBase\SelectCoroutine;
use Monolog\Logger;
use Server\SwooleMarco;
use app\Services\Common\ConfigService;
use Server\CoreBase\SwooleRedirectException;
use Server\CoreBase\SwooleException;
use app\Wechat\WxPay;
use app\Services\Company\CompanyService;
/*
 * 基础类
 */

class Base extends Controller
{

    protected $parm;
    protected $user_id;
    protected $work_log_model;
    protected $time_model;
    protected $log_model;
    protected $user_info;
    protected $contract_log_model;
    protected $dev_mode;
    protected $app_debug;
    protected $wx_config;
    protected $work_eq_model;
    protected $engineer_wx_name;
    protected $user_wx_name;
    protected $company;
    protected $contract_model;
    protected $machine_model;
    protected $pay_config;
    protected $work_equipment;
    protected $big_data_url;
    protected $eq_qrcode_model;

    public function initialization($controller_name, $method_name)
    {
        parent::initialization($controller_name, $method_name);
        $this->user_id = $this->request->user_id;
        $this->company = $this->request->company;
        $this->parm = json_decode($this->http_input->getRawContent(), true);
        $this->work_log_model = $this->loader->model('WorkOrderLogModel', $this);
        $this->time_model = $this->loader->model('WorkOrderTimeModel', $this);
        $this->log_model = $this->loader->model('OrderLogModel', $this);
        $this->engineers_model = $this->loader->model('EngineersModel', $this);
        $this->contract_log_model = $this->loader->model('ContractLogModel', $this);
        $this->work_eq_model = $this->loader->model('WorkEquipmentModel', $this);
        $this->contract_model = $this->loader->model('ContractModel', $this);
        $this->machine_model = $this->loader->model('MachineModel', $this);
        $this->eq_qrcode_model = $this->loader->model('EquipmentQrcodeModel',$this);
        //判断用户状态是否正常
        if (!empty($this->user_id)) {
            $userinfo = $this->engineers_model->getOne(array('engineers_id' => $this->user_id), 'openid,engineers_status,is_delete');
            if (!empty($userinfo) && ($userinfo['engineers_status'] == 1 || $userinfo['is_delete'] == 1)) {
                return $this->jsonend(-1006, "该账号已被冻结,更多信息请联系后台管理员");
            }
            $openid = $this->request->header['openid'] ?? '';
            if (empty($openid)) {
                return $this->jsonend(-1004, '未登录');
            }
            if ($openid != $userinfo['openid']) {
                return $this->jsonend(-1006, "该账号已在其他端登录,若不是本人操作修改登录密码");
            }
        }

        //开发者模式
        $this->dev_mode = ConfigService::getConfig('develop_mode', false, $this->company);
        //调试模式
        $this->app_debug = ConfigService::getConfig('app_debug', false, $this->company);
        $company_config = CompanyService::getCompanyConfig($this->company);
        $this->wx_config = $company_config['engineer_wx_config'];
        $this->pay_config = $company_config['pay_config'];//支付配置
        $this->engineer_wx_name = $company_config['engineer_wx_config']['app_wx_name'] ?? '优净云';
        $this->user_wx_name = $company_config['wx_config']['app_wx_name'] ?? '优净云';
        $this->big_data_url = $this->config->get('big_data_url');
        if ($this->dev_mode == 1) {
            $this->big_data_url = $this->config->get('debug_config.big_data_url');
        }
    }

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 异常的回调(如果需要继承$autoSendAndDestroy传flase)
     * @param \Throwable $e
     * @param callable $handle
     */
    public function onExceptionHandle(\Throwable $e, $handle = null)
    {
        $log = '--------------------------[报错信息]----------------------------' . PHP_EOL;
        $log .= 'client：Engineer' . PHP_EOL;
        $log .= 'DATE：' . date("Y-m-d H:i:s") . PHP_EOL;
        $log .= 'getMessage：' . $e->getMessage() . PHP_EOL;
        $log .= 'getCode：' . $e->getCode() . PHP_EOL;
        $log .= 'getFile：' . $e->getFile() . PHP_EOL;
        $log .= 'getLine：' . $e->getLine();
        FileLogService::WriteLog(SLog::ERROR_LOG,'error',$log);

        //必须的代码
        if ($e instanceof SwooleRedirectException) {
            $this->http_output->setStatusHeader($e->getCode());
            $this->http_output->setHeader('Location', $e->getMessage());
            $this->http_output->end('end');
            return;
        }
        if ($e instanceof SwooleException) {
          FileLogService::WriteLog(SLog::ERROR_LOG,'error',$e->getMessage() . "\n" . $e->getTraceAsString());
        }
        //可以重写的代码
        if ($handle == null) {
            switch ($this->request_type) {
                case SwooleMarco::HTTP_REQUEST:
                    $e->request = $this->request;
                    $data = get_instance()->getWhoops()->handleException($e);
                    $this->http_output->setStatusHeader(500);
                    $this->http_output->end($data);
                    break;
                case SwooleMarco::TCP_REQUEST:
                    $this->send($e->getMessage());
                    break;
            }
        } else {
            \co::call_user_func($handle, $e);
        }
    }

    /**
     * 返回json格式
     */
    public function jsonend($code = '', $message = '', $data = '', $gzip = true)
    {
        $output = array('code' => $code, 'message' => $message, 'data' => $data);

        if (!$this->canEnd()) {
            return;
        }
        if (!get_instance()->config->get('http.gzip_off', false)) {
            //低版本swoole的gzip方法存在效率问题
//            if ($gzip) {
//                $this->response->gzip(1);
//            }
            //压缩备用方案
            /* if ($gzip) {
              $this->response->header('Content-Encoding', 'gzip');
              $this->response->header('Vary', 'Accept-Encoding');
              $output = gzencode($output . " \n", 9);
              } */
        }
        if (is_array($output) || is_object($output)) {
            $this->response->header('Content-Type', 'text/html; charset=UTF-8');
            $output = json_encode($output, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            //$output = "<pre>$output</pre>";
        }
        $this->response->end($output);
        $this->endOver();
    }

    //根据工单ID获取工单类型以及保修时间
    function getWorkorderRange($work_order_id)
    {
        $work_eq_join = [
            ['equipment_lists el', 'el.equipment_id = rq_work_equipment.equipment_id', 'left join'],
        ];
        $equipment_info = $this->work_eq_model->getAll(array('rq_work_equipment.work_order_id' => $work_order_id), 'rq_work_equipment.equipment_id,el.device_no,start_time,end_time', ['rq_work_equipment.create_time' => 'DESC'], $work_eq_join);
        if (empty($equipment_info)) {
            return false;
        }
        return $equipment_info[0];
    }

    /**
     * @desc   添加工单操作日志
     * @param
     * @date   2018-07-19
     * @return [type]     [description]
     * @author lcx
     */
    public function addWorkOrderLog($work_order_id, $time, $status, $remark,$open_remarks='')
    {
        $log['work_order_id'] = $work_order_id;
        $log['create_work_time'] = $time;
        $log['operating_time'] = time();
        $log['do_id'] = $this->user_id;
        $log['operating_type'] = 2;
        $log['operating_status'] = $status;
        $log['remarks'] = $remark;
        $lang = $this->config->get('work_order_status_desc')[$status] ?? '';
        $log['open_remarks'] = empty($open_remarks) ? $lang : $lang . '!' . $open_remarks;
        $res = $this->work_log_model->add($log);
        return $res;
    }

    /**
     * @desc   添加订单操作日志
     * @param
     * @date   2018-07-19
     * @return [type]     [description]
     * @author lcx
     */
    public function addOrderLog($order_id, $status, $remark, $is_to_user = 0)
    {
        $data['operater_role'] = 2;
        $data['order_id'] = $order_id;
        $data['status'] = $status;
        $data['create_time'] = time();
        $data['remark'] = $remark;
        $data['is_to_user'] = $is_to_user;
        $res = $this->log_model->add($data);
        return $res;
    }

    /**
     * @desc   获取时间范围详情
     * @param
     * @date   2018-07-24
     * @return [type]     [description]
     * @author lcx
     */
    public function getRangeInfo($time_id)
    {
        $info = $this->time_model->getOne(array('time_id' => $time_id));
        if (!empty($info)) {
            return $info;
        }
        return false;
    }

    /**
     * @desc   添加合同操作日志
     * @param $status 1增2删3改4解绑'
     * @date   2018-07-19
     * @author lcx
     * @return [type]     [description]
     */
    public function addContractLog($contract_id, $status = 1, $remark)
    {
        $data['contract_id'] = $contract_id;
        $data['do_id'] = $this->user_id;
        $data['do_type'] = $status;
        $data['terminal_type'] = 3;
        $data['do_time'] = time();
        $data['remark'] = $remark;
        $res = $this->contract_log_model->add($data);
        return $res;
    }

    /**
     * 处理不同基础服务费用
     * @param array $ser_info 服务信息 => $rental_cost 租赁时费用 $buy_cost 购买时费用 $usiness_type_id 服务ID
     * @param array $eq_info 主板信息  =>$equipment_type 类型  $start_time保修开始时间 $end_time 保修结束时间
     */
    public function baseServiceCost($ser_info, $eq_info)
    {
        $cost = $ser_info['rental_cost'];
        return $cost;
    }

    /**
     * 处理不同滤芯费用
     * @param array $part_info 配件信息 => $parts_money 租赁时费用 $parts_buy_money 购买时费用 $usiness_type_id 服务ID
     * @param array $eq_info 主板信息  =>$equipment_type 类型  $start_time保修开始时间 $end_time 保修结束时间
     */
    public function partCost($part_info, $eq_info)
    {
        $cost = $part_info['parts_money'];
        return $cost;
    }

    /**
     * 处理不同额外服务费用
     * @param array $ser_info 服务信息 => $parts_money 租赁时费用 $parts_buy_money 购买时费用 $usiness_type_id 服务ID
     * @param array $eq_info 主板信息  =>$equipment_type 类型  $start_time保修开始时间 $end_time 保修结束时间
     */
    public function addtionalServiceCost($ser_info, $eq_info)
    {
        $cost = $ser_info['service_rental_cost'];
        return $cost;
    }

    //根据工单查询合同时间
    public function getContractTime($work_order_id, $contract_id = '')
    {
        $return = [];
        //合同是否签约
        if (empty($contract_id)) {
            //根据工单找主板，主板找合同
            $join = [
                ['contract_equipment', 'rq_contract_equipment.equipment_id=rq_work_equipment.equipment_id']
            ];
            $info = $this->work_eq_model->getOne(['work_order_id' => $work_order_id], 'contract_id', $join);
            $contract_id = $info['contract_id'];
        }
        if (!empty($contract_id)) {
            $contract_info = $this->contract_model->getOne(['contract_id' => $contract_id], 'effect_time');
            $return['effect_time'] = empty($contract_info['effect_time']) ? '' : date('Y年m月d日', $contract_info['effect_time']);
        }
        return $return;

    }

    /**
     * showdoc
     * Author: Xuleni
     * DateTime: 2020/11/27 11:17
     * @catalog
     * @title 查询二维码或者缴费器是否存在
     */
    public function selectMainboardNumber($device_no){
        //查询二维码表
        $code_device_no = $this->eq_qrcode_model->getOne(['qrcode_no | device_no'=>$device_no,'company_id'=>$this->company],'status,device_no');
        $device_no_a = '';
        if (!empty($code_device_no)){
            if ($code_device_no['status'] != 1)return $this->jsonend(-1001,'缴费器二维码状态不正确');
            $device_no_a = $code_device_no['device_no'];
        }else{
            $replaced_device_lib = $this->eq_library_model->getOne(['device_no' => $device_no], 'id,device_status,device_no');
            if (!empty($replaced_device_lib['device_no'])) $device_no_a = $replaced_device_lib['device_no'];
        }
        return $device_no_a;
    }

}
