<?php

namespace app\Controllers\Engineer;

use app\Services\Common\ConfigService;
use app\Services\Common\DataService;
use app\Services\Common\ReturnCodeService;
use Firebase\JWT\JWT;
use Server\Components\CatCache\CatCacheRpcProxy;
use app\Wechat\WxAuth;
use app\Library\Alipay\aop\request\AlipaySystemOauthTokenRequest;
use app\Library\Alipay\aop\AopClient;
use app\Library\Alipay\aop\request\ZhimaCreditScoreGetRequest;
use app\Library\Alipay\aop\request\AlipayUserCertifyOpenInitializeRequest;
use app\Services\Common\HttpService;
use app\Services\Common\CommonService;
use Server\CoreBase\SwooleException;
use app\Models\OaWorkersModel;
use app\Models\ChannelModel;
use app\Models\ChannelPromotersModel;
use app\Services\User\UserService;

/**
 * 用户端/用户相关APISU
 */
class Projectperformance extends Base
{

    protected $user_model;
    protected $AchievementModel;
    protected $oaAchievementWorkersModel;
    protected $contractModel;
    protected $workOrderModel;
    protected $engineersModel;

    public function initialization($controller_name, $method_name)
    {
        parent::initialization($controller_name, $method_name);
        $this->AchievementModel = $this->loader->model('AchievementModel', $this);
        $this->oaAchievementWorkersModel = $this->loader->model('OaAchievementWorkersModel', $this);
        $this->contractModel = $this->loader->model('ContractModel', $this);
        $this->workOrderModel = $this->loader->model('WorkOrderModel', $this);
        $this->engineersModel = $this->loader->model('EngineersModel', $this);
    }


    /**
     * showdoc
     * @catalog API文档/工程端/区域总管
     * @title 获取区域总管业绩明细
     * @description
     * @method POST
     * @url Engineer/Projectperformance/achievementDetailed
     * @param page 可选 int 页数，默认1
     * @param pagesize 可选 int 每页条数，默认10
     * @param keywords 可选 int 关键字查询
     * @param start_time 可选 int 时间查询开始时间
     * @param end_time 可选 int 时间查询结束时间
     * @return
     * @return_param settlement_amount array 金额：total_amount：总金额，amount_details：金额明细数组，money：金额，source：金额来源
     * @return_param shop_name string 店铺名称
     * @return_param customer_code string 户号
     * @return_param is_available int 是否划拨：1是2否
     * @return_param available_name string 是否划拨
     * @return_param transfer_time string 划拨时间
     * @return_param code_address string 户号地址
     * @return_param engineers array 工程人员：engineers_data：工程人员数组，engineers_name：工程名称，engineers_phone：工程电话，count：工程人数
     * @remark
     * @number 0
     * @author xln
     * @date 2021-1-7
     */
    public function http_achievementDetailed()
    {
        if (empty($this->user_id)) return $this->jsonend(ReturnCodeService::FAIL, '未登录');
        $page = $this->parm['page'] ?? 1;
        $pageSize = $this->parm['pageSize'] ?? 10;
        $keywords = $this->parm['keywords'] ?? '';
        $start_time = $this->parm['start_time'] ?? '';
        $end_time = $this->parm['end_time'] ?? '';
        if (!empty($keywords)) {
            //时间搜索
            if ((!empty($start_time) && !empty($end_time)) ){
                $keyword_where['add_time'] = ['BETWEEN', [strtotime($start_time), strtotime($end_time)]];
            }
            $keyword_where = ['code|shop_name|address'=>['LIKE', '%' . trim($keywords) . '%']];
            $keyword_field = "achievement_id";
            $keyword_join = [
                ['customer_code e','e.code = rq_oa_achievement.customer_code','left'],
                ['contract t','t.contract_id = e.contract_id','left'],
            ];
            $this->AchievementModel->table = 'oa_achievement';
            $keyword_data = $this->AchievementModel->selectPageData($keyword_where, $keyword_field, $keyword_join,$pageSize,$page);
            if (empty($keyword_data)) return $this->jsonend(ReturnCodeService::FAIL,'暂无数据');
            $keyword_achievement_id = [];
            foreach ($keyword_data as $key=>$value){
                $keyword_achievement_id[] = $value['achievement_id'];
            }
            $achievement_where['achievement_id'] = ['in',$keyword_achievement_id];
        }
        if ((!empty($start_time) && !empty($end_time)) && empty($keywords)){
            $achievement_where['add_time'] = ['BETWEEN', [strtotime($start_time), strtotime($end_time)]];
        }
        //分页分组查询业绩ID
        $achievement_where['role'] = 2;
        $achievement_where['workers_id'] = $this->user_id;
        $achievement_group = 'achievement_id';
        $achievement_order = ['id' => 'desc'];
        $achievement_data = $this->oaAchievementWorkersModel->getAll($achievement_where, $page, $pageSize, 'achievement_id', [], $achievement_order, $achievement_group);
        if (empty($achievement_data)) return $this->jsonend(ReturnCodeService::FAIL, '暂无数据');
        $achievement_id_array = []; //业绩ID
        foreach ($achievement_data as $key => $value) {
            $achievement_id_array[] = $value['achievement_id'];
        }
        //查询业绩信息
        $oa_workers_where = ['achievement_id' => ['in', $achievement_id_array], 'role' => 2];
        $oa_workers_field = "achievement_id,source,workers_id,money,is_available,transfer_time,add_time";
        $oa_workers_data = $this->oaAchievementWorkersModel->getAll($oa_workers_where, 1, -1, $oa_workers_field, [], ['id' => 'DESC']);
        if (empty($oa_workers_data)) return $this->jsonend(ReturnCodeService::FAIL, '暂无数据');
        $data = []; //默认返回数据
        $group_oa_workers = []; //默认分组数组
        $achievement_source = $this->config->get('achievement_source');
        foreach ($oa_workers_data as $key => $value) $group_oa_workers[$value['achievement_id']][] = $value;
        //处理数组
        foreach ($group_oa_workers as $key => $value) {
            $data[$key]['settlement_amount']['total_amount'] = 0;
            foreach ($value as $k1 => $v1) {
                //查询工程人员、户号
                $this->AchievementModel->table = 'oa_achievement';
                $achievement_where = ['achievement_id' => $v1['achievement_id'], 'state' => 1];
                $achievement_field = 'customer_code,engineers';
                $achievement_data = $this->AchievementModel->findData($achievement_where, $achievement_field);

                $contract = $this->get_contract($achievement_data['customer_code']); //查询店铺名称合同ID
                $data[$key]['shop_name'] = $contract['shop_name'] ?? ''; //店铺名称
                $data[$key]['customer_code'] = $achievement_data['customer_code']; //户号
                $data[$key]['is_available'] = $v1['is_available']; //是否划拨
                $data[$key]['available_name'] = ($v1['is_available'] == 1 ? '是' : '否'); //是否划拨
                $data[$key]['transfer_time'] = date('Y-m-d H:i:s', $v1['transfer_time']); //划拨时间
                $data[$key]['add_time'] = date('Y-m-d H:i', $v1['add_time']);
                $data[$key]['code_address'] = $contract['province'].$contract['city'].$contract['area'].$contract['address']; //户号地址
                if (empty($data[$key]['engineers'])){
                    $data[$key]['engineers'] = $this->get_engineers(json_decode($achievement_data['engineers'], true)); //工程人员信息
                }
                $data[$key]['settlement_amount']['total_amount'] += formatMoney($v1['money']); //总金额
                $data[$key]['settlement_amount']['amount_details'][$k1]['money'] = formatMoney($v1['money']); //单笔金额
                $data[$key]['settlement_amount']['amount_details'][$k1]['source'] = $achievement_source[$v1['source']]; //金额来源
            }
        }
        $data_ = [];
        foreach ($data as $key=>$value){
            $data_[] = $value;
        }
        return $this->jsonend(ReturnCodeService::SUCCESS, '获取成功', $data_);
    }


    /**
     * 查询工程人员信息
     * @param array $engineers
     */
    public function get_engineers(array $engineers)
    {
        $where = ['engineers_id' => ['in', $engineers]];
        $field = 'engineers_name,engineers_phone';
        $engineers_data = $this->engineersModel->getAll($where, [], 1, -1, $field, []);
        return ['engineers_data' => $engineers_data, 'count' => count($engineers_data)];
    }

    /**
     * 根据户号查询、店铺名称合同ID
     * @param string $code 户号
     * @return mixed
     */
    public function get_contract(string $code)
    {
        $where = ['e.code' => $code];
        $field = "shop_name,e.contract_id,province,city,area,address";
        $join = [['customer_code e', 'e.contract_id = rq_contract.contract_id']];
        $data = $this->contractModel->getOne($where, $field, $join);
        return $data;
    }


}