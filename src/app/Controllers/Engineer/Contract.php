<?php

namespace app\Controllers\Engineer;


use app\Services\Common\ConfigService;
use app\Services\Common\DataService;
use app\Services\Common\HttpService;
use app\Services\Common\ReturnCodeService;
use app\Services\Company\CompanyService;
use app\Services\Log\AsyncFile;
use app\Services\User\UserService;
use app\Services\Stock\StockService;
use Server\CoreBase\SwooleException;

class Contract extends Base
{

    protected $contract_model;
    protected $auth_model;
    protected $user_model;
    protected $contract_ep_model;
    protected $parts_model;
    protected $ep_part_model;
    protected $bind_model;
    protected $company_model;
    protected $contractAdditionalModel;
    protected $equipment_list_model;
    protected $ContractPackageRecordModel;
    protected $equipmentWaterRecordModel;
    protected $Achievement;
    protected $contract_additional_model;
    protected $userService;
    protected $stockModel;
    protected $equipmentModel;
    protected $work_eq_model;
    protected $customer_code_model;
    protected $customerCodeBillModel;
    protected $work_order_model;
    // 前置方法，加载模型
    public function initialization($controller_name, $method_name)
    {
        parent::initialization($controller_name, $method_name);
        $this->contract_model = $this->loader->model('ContractModel', $this);
        $this->auth_model = $this->loader->model('CustomerAuthModel', $this);
        $this->user_model = $this->loader->model('CustomerModel', $this);
        $this->contract_ep_model = $this->loader->model('ContractEquipmentModel', $this);
        $this->parts_model = $this->loader->model('PartsModel', $this);
        $this->ep_part_model = $this->loader->model('EquipmentsPartsModel', $this);
        $this->bind_model = $this->loader->model('CustomerBindEquipmentModel', $this);
        $this->company_model = $this->loader->model('CompanyModel', $this);
        $this->contractAdditionalModel = $this->loader->model('ContractAdditionalModel', $this);
        $this->equipment_list_model = $this->loader->model('EquipmentListsModel', $this);
        $this->ContractPackageRecordModel = $this->loader->model('ContractPackageRecordModel', $this);
        $this->equipmentWaterRecordModel = $this->loader->model('EquipmentWaterRecordModel', $this);
        $this->Achievement = $this->loader->model('AchievementModel', $this);
        $this->contract_additional_model = $this->loader->model('ContractAdditionalModel', $this);
        $this->userService = new UserService();
        $this->operation_info_model = $this->loader->model('OperationInfoModel', $this);
        $this->stockModel = $this->loader->model('StockModel', $this);
        $this->equipmentModel = $this->loader->model('EquipmentModel', $this);
        $this->work_eq_model = $this->loader->model('WorkEquipmentModel', $this);
        $this->customer_code_model = $this->loader->model('CustomerCodeModel', $this);
        $this->customerCodeBillModel = $this->loader->model('CustomerCodeBillModel', $this);
        $this->work_order_model = $this->loader->model('WorkOrderModel',$this);
    }

    public function http_test()
    {
        $params = ['sn' => "866050034443331", 'working_mode' => 1, 'filter_element_max' => [180, 180, 36500, 36500, 36500]];
        $path = '/House/Issue/bindingPackage';
        HttpService::Thrash($params, $path);
        //下发--数据同步
        $data_sync_params['sn'] = "866050034443331";
        $data_sync_params['used_days'] = 0;
        $data_sync_params['used_traffic'] = 0;
        $data_sync_params['remaining_days'] = 55;
        $data_sync_params['remaining_traffic'] = 0;
        $data_sync_path = '/House/Issue/data_sync';
        HttpService::Thrash($data_sync_params, $data_sync_path);
    }

    /**
     * showdoc
     * @catalog API文档/工程端/合同相关
     * @title 签合同
     * @description 根据合同ID跟用户ID
     * @method POST
     * @url Engineer/Contract/sign
     * @param contract_id 必选 int 合同ID
     * @param contact_type 可选 int 合同签署方式。3纸质合同（选择纸质合同需传3）
     * @return {"code": 1000,"message": "签约成功","data": ""}
     * @return_param
     * @remark {"contract_id":"21"}
     * @number 0
     * @author lcy
     * @date 2018-10-18
     */
    public function http_sign()
    {
        if (empty($this->parm['contract_id'] ?? '')) {
            return $this->jsonend(-1001, '缺少参数');
        }
        $map['contract_id'] = $this->parm['contract_id'];
        $work_order_id = $this->parm['work_order_id'] ?? '';
        $data = $this->contract_model->getOne($map, '*');
        if (empty($data)) {
            return $this->jsonend(-1000, "合同不存在");
        }
        if ($data['status'] != 3) {
            return $this->jsonend(-1000, "抱歉,该合同无法进行签署!请确认合同是否有效或者已经签署过");
        }
        //判断用户是否通过实名认证
        $user_info = $this->user_model->getOne(array('user_id' => $data['user_id']), 'is_auth,realname,gender');
        if (empty($user_info) || $user_info['is_auth'] != 2) {
            return $this->jsonend(-1201, "该客户还未通过实名认证，请先进行实名");
        }
        //查询户号
        $customer_code_where = ['work_order_id' => $work_order_id];
        $field = 'customer_code';
        $customer_code = $this->work_eq_model->getOne($customer_code_where, $field, []);
        //------------------------------------------移机新增begin------------------------------------------
        //根据合同获取工单信息
        $where = [
            'contract_id' => $map['contract_id'],
            'rq_work_order_business.business_id' => 5
        ];
        if (!empty($work_order_id)) {
            $where['rq_work_order.work_order_id'] = $work_order_id;
        }
        $field = '
            rq_work_order.work_order_id,
            rq_work_order_business.business_id,
            rq_work_order.is_new_word_order,
            rq_work_order.replacement_type,
            rq_work_order.move_change_equipments,
            rq_work_equipment.equipment_id,
            rq_work_order.user_id,
            rq_work_order.equipments_id,
            rq_work_order.equipment_num
        ';
        $join = [
            ['work_order_business', 'rq_work_order.work_order_id = rq_work_order_business.work_order_id', 'LEFT'],
            ['work_equipment', 'rq_work_order.work_order_id = rq_work_equipment.work_order_id', 'LEFT'],
        ];
        $this->Achievement->table = 'work_order';
        $work_order = $this->Achievement->findJoinData($where, $field, $join);
        $is_replacement = false;
        if (!empty($work_order)) {
            if ($work_order['replacement_type'] == 2) {
                return $this->jsonend(-1000, "移机拆不需要签署合同");
            }
            $is_replacement = true;
        }
        //------------------------------------------移机新增end------------------------------------------
        //是否是纸质合同
        $edit_data['contact_type'] = $this->parm['contact_type'] ?? $data['contact_type']; //合同签署方式 如果传入3则更改签署方式，否则不修改
        $edit_data['status'] = 4;
        $edit_data['effect_time'] = !empty($data['sign_date']) ? $data['sign_date'] : time();//合同生效时间默认是当前时间,如果后台设置了签约时间，就是签约时间
        $edit_data['installed_time'] = $edit_data['effect_time'];//装机时间
        $edit_data['now_date'] = time();//记录合同生效当时时间
        $edit_data['sign_client'] = empty($this->parm['sign_client'] ?? '') ? 1 : $this->parm['sign_client'];
        $edit_data['esign_filename'] = $this->parm['esign_filename'] ?? '';
        $edit_data['esign_user_sign'] = $this->parm['esign_user_sign'] ?? '';
        $eq_filter_element_num = 0;
        $db_eq_filter_element_num = [];
        $add_status = false;
        $this->db->begin(function () use ($customer_code, $edit_data, &$add_status, $is_replacement, $work_order, $eq_filter_element_num, $data, $db_eq_filter_element_num) {
            try {
                //移机
                if ($is_replacement) {
                    //合同生效时间，装机时间，到期时间应该与原合同一致
                    $old_contract_id = $this->contract_model->getOne(['contract_id' => $this->parm['contract_id']], 'pid');
                    $old_contract_info = [];
                    if (!empty($old_contract_id) && !empty($old_contract_id['pid'])) {
                        $old_contract_info = $this->contract_model->getOne(['contract_id' => $old_contract_id['pid']], 'effect_time,installed_time,province,city,area,address');
                    }
                    $edit_data['effect_time'] = $old_contract_info['effect_time'] ?? '';
                    $edit_data['installed_time'] = $old_contract_info['installed_time'] ?? '';
                    //主板配件信息--如果是移机将原始合同的主板配件信息同步到移机合同
                    $where = [
                        'contract_id' => $data['pid'],
                        'equipment_id' => $work_order['equipment_id'],
                        'is_delete' => 0,
                    ];
                    $this->Achievement->table = 'equipments_parts';
                    $parts = $this->Achievement->selectData($where, '*', [], []);
                    if (!empty($parts)) {
                        $this->ep_part_model->del(['contract_id' => $data['pid']]);//删除当前合同相关主板配件信息
                        foreach ($parts as $key => $value) {
                            unset($value['id']);
                            $value['contract_id'] = $this->parm['contract_id'];
                            $this->ep_part_model->add($value);
                        }
                    }
                    //用户绑定主板
                    $bind_data['equipments_id'] = $work_order['equipments_id'];
                    $bind_data['work_order_id'] = $work_order['work_order_id'];
                    $bind_data['equipment_id'] = $work_order['equipment_id'];
                    $bind_data['user_id'] = $work_order['user_id'];
                    $bind_data['create_time'] = time();
                    $bind_data['is_owner'] = 2;
                    $bind_data['customer_code'] = $customer_code['customer_code'];
                    $this->bind_model->addCustomerBindEquipment($bind_data);
                    //判断该地区是否开通
                    $address_info = $this->userService->getOwnArea($work_order['user_id'], '', $data['province_code'], $data['city_code'], $data['area_code'], $this->company);
                    if (!$address_info) {
                        returnJsonAdmin(-1101, "该地址暂未开通服务");
                    }
                    //用户划拨到移机区域
                    $this->Achievement->table = 'customer_administrative_center';
                    $customer_administrative_center = $this->Achievement->findData(['user_id' => $work_order['user_id'], 'administrative_id' => $address_info['a_id']], 'id');
                    if (empty($customer_administrative_center)) {
                        $this->Achievement->insertData([
                            'user_id' => $work_order['user_id'],
                            'operation_id' => $address_info['operation'],
                            'administrative_id' => $address_info['a_id'],
                            'update_time' => time(),
                        ]);
                    }
                    //更新主板地址
                    $eq['province_code'] = $data['province_code'];
                    $eq['city_code'] = $data['city_code'];
                    $eq['area_code'] = $data['area_code'];
                    $eq['province'] = $data['province'];
                    $eq['city'] = $data['city'];
                    $eq['area'] = $data['area'];
                    $eq['address'] = $data['address'];
                    $this->equipment_list_model->updateEquipmentLists($eq, array('equipment_id' => $work_order['equipment_id']));
                    //更新合同主板关系表主板地址
                    $eq_contr_map['contract_id'] = $this->parm['contract_id'];
                    $eq_contr_map['equipment_id'] = $work_order['equipment_id'];
                    $eq_contr_info = $this->contract_ep_model->getOne($eq_contr_map, 'moving_machine_number');
                    if (empty($eq_contr_info)) {
                        throw new Exception("合同里无该主板信息");
                    }
                    $eq_contr_data['uptime'] = time();
                    $eq_contr_data['moving_machine_number'] = $eq_contr_info['moving_machine_number'] + 1;
                    $this->contract_ep_model->save($eq_contr_map, $eq_contr_data);
                    //新增合同补充协议
                    $add_data['contract_id'] = $this->parm['contract_id'];
                    $add_data['equipment_id'] = $work_order['equipment_id'];
                    $add_data['type'] = 1;
                    $add_data['old_address'] = $old_contract_info['province'] . $old_contract_info['city'] . $old_contract_info['area'] . $old_contract_info['address'];
                    $add_data['move_address'] = $data['province'] . $data['city'] . $data['area'] . $data['address'];
                    $add_data['create_time'] = time();
                    $this->contract_additional_model->add($add_data);
                    $this->Achievement->table = 'customer';
                    $user = $this->Achievement->findData(['user_id' => $work_order['user_id']], 'replacement_number');
                    if (!empty($user) && $user['replacement_number'] > 0) {
                        $this->Achievement->table = 'customer';
                        $this->Achievement->setDec(['user_id' => $work_order['user_id']], 'replacement_number', 1);
                    }
                    //获取合同信息
                    $this->Achievement->table = 'contract';
                    $new_contract = $this->Achievement->findData(['contract_id' => $this->parm['contract_id']], 'contract_no,pid,contact_type');
                    if (!empty($new_contract)) {
                        //修改原始合同状态
                        $this->Achievement->updateData(['contract_id' => $new_contract['pid']], ['status' => 6]);
                        $this->Achievement->table = 'work_order';
                        $join = [
                            ['contract', 'rq_work_order.contract_id=rq_contract.contract_id', 'left']
                        ];
                        $old_contract = $this->Achievement->findJoinData(['rq_work_order.contract_id' => $new_contract['pid']], 'contract_no,work_order_id', $join);
                        if (!empty($old_contract)) {
                            //更新旧合同主板状态
                            $this->Achievement->table = 'contract_equipment';
                            $this->Achievement->updateData(['contract_id' => $new_contract['pid'], 'equipment_id' => $work_order['equipment_id']], ['state' => 0]);
                            //解除主板绑定关系
                            $this->Achievement->table = 'customer_bind_equipment';
                            $this->Achievement->updateData(['work_order_id' => $old_contract['work_order_id'], 'is_owner' => 2, 'user_id' => $work_order['user_id']], ['status' => 3, 'unbinding_time' => time()]);
                            //修改结算关系合同编号
                            $this->Achievement->table = 'contract_rank';
                            $this->Achievement->updateData(['contract_no' => $old_contract['contract_no']], ['contract_no' => $new_contract['contract_no']]);
                        }
                    }
                }
                $this->contract_model->save(array('contract_id' => $this->parm['contract_id']), $edit_data);//修改合同信息
                $this->bind_model->save(array('customer_code' => $customer_code['customer_code'], 'is_owner' => 2, 'status' => 0), array('status' => 1));
                //出账日
                $out_bill_date = $this->customerCodeBillModel->outBillDate($edit_data['effect_time']);
                $this->customer_code_model->save(array('code' => $customer_code['customer_code'], 'state' => 0), ['state' => 1, 'out_bill_date' => $out_bill_date]);
                $this->customer_code_model->save(array('code' => $customer_code['customer_code'], 'state' => 0), array('state' => 1));

                //查询合同主板信息
                $eq_join = [
                    ['equipment_lists as el', 'el.equipment_id = rq_contract_equipment.equipment_id', 'inner'],
                    ['equipments as e', 'e.equipments_id = el.equipments_id', 'inner']
                ];
                $contract_eq_info = $this->contract_ep_model->getAll(array('rq_contract_equipment.contract_id' => $this->parm['contract_id']), 'el.device_no,e.type,rq_contract_equipment.equipment_id', $eq_join);
                if (!empty($contract_eq_info)) {
                    //下发指令-下发套餐
                    $contract_eq = array_column($contract_eq_info,'device_no');
                    $this->contract_model->activateDevice($this->parm['contract_id'], $contract_eq);
                }
                //合同日志
                $this->addContractLog($this->parm['contract_id'], 4, "合同签署成功，正式生效。由维修端发起签署");

                //当合同签约成功且在华信模式下判断租赁下单根据推荐人减去市场推广或者城市合伙人的库存
                $joins = [
                    ['work_order_business', 'rq_work_order.work_order_id=rq_work_order_business.work_order_id', 'left']
                ];
                $this->Achievement->table = 'work_order';
                $work_order_info = $this->Achievement->findJoinData(['rq_work_order.work_order_id' => $this->parm['work_order_id']], 'rq_work_order_business.business_id,rq_work_order.user_id,rq_work_order.replacement_type,rq_work_order.move_change_equipments,rq_work_order.equipments_id,rq_work_order.equipment_num,rq_work_order.move_old_equipments', $joins);
                $equipment_info = $this->equipmentModel->getOne(['equipments_id' => $work_order_info['equipments_id']], 'equipments_id');

                if (in_array($this->company, $this->config->get('logic_mode.huaxin'))) {
                    if ($equipment_info['equipments_type'] == 1 && ($work_order_info['business_id'] == 3 || ($work_order_info['business_id'] == 5 && in_array($work_order_info['replacement_type'], [1, 3]) && $work_order_info['move_change_equipments'] == 1))) {//租赁产品时订单的库存在签合同时扣除
                        //先判断用户是是否有市场推广，没得则减去所属合伙人的库存
                        $res = $this->userService->getUserBelongTo($work_order_info['user_id'], $this->company);
                        if (empty($res)) {
                            throw new SwooleException("用户推荐人信息错误");
                        }
                        //查询库存信息
                        $stock_where['role'] = $res['role'];
                        $stock_where['uid'] = $res['uid'];
                        $stock_where['equipments_id'] = $equipment_info['equipments_id'];//产品id
                        $stock_where['company_id'] = $this->company;
                        $stock_info = $this->stockModel->getOne($stock_where, 'stock_id,surplus');
                        $surplus = !empty($stock_info) ? $stock_info['surplus'] : 0;
                        //减去产品库存，并写入记录
                        StockService::changeStock($res['role'], $res['uid'], $work_order_info['equipments_id'], $work_order_info['equipment_num'], $this->company, 2);
                        StockService::addStockRecord($res['role'], $res['uid'], $work_order_info['equipments_id'], $surplus, $work_order_info['equipment_num'], 4, 2, $res['role'], $res['uid'], $this->company, '用户下单租赁产品，减少产品库存');
                        //移机拆/装和移机装工单且更换了新产品情况下增加原库存
                        if ($work_order_info['business_id'] == 5 && in_array($work_order_info['replacement_type'], [1, 3]) && $work_order_info['move_change_equipments'] == 1) {
                            //查询旧产品剩余数量
                            $stock_where['equipments_id'] = $work_order_info['move_old_equipments'];
                            $old_stock_info = $this->stockModel->getOne($stock_where, 'surplus');
                            $surplus = !empty($old_stock_info) ? $old_stock_info['surplus'] : 0;
                            //增加旧产品库存
                            StockService::changeStock($res['role'], $res['uid'], $work_order_info['move_old_equipments'], $work_order_info['equipment_num'], $this->company, 1);
                            //写入库存记录
                            StockService::addStockRecord($res['role'], $res['uid'], $work_order_info['move_old_equipments'], $surplus, $work_order_info['equipment_num'], 1, 1, $res['role'], $res['uid'], $this->company, '用户移机下单更换租赁产品，增加旧产品库存');
                        }
                    }
                }
                $add_status = true;
            } catch (\Exception $exception) {
                return $this->jsonend(-1000, "签约失败!" . $exception->getMessage());
            }
        }, function ($e) {
            return $this->jsonend(-1000, "签约失败,请重试!" . $e->error);
        });
        if ($add_status) {
            //向大数据发送通知
            $big_data_notice_config = ConfigService::getTemplateConfig('contract_sign_success', 'big_data', $this->company);
            if (!empty($big_data_notice_config) && !empty($big_data_notice_config['websocket']) && $big_data_notice_config['websocket']['switch']) {
                mb_internal_encoding('UTF-8');
                $gender = $user_info['gender'] == 1 ? '先生' : '女士';
                $notice_of_contract = $this->config->get('notice_of_contract');
                $big_data = [
                    'title' => $notice_of_contract['title'],
                    'content' => mb_substr($user_info['realname'], 0, 1) . $gender . $notice_of_contract['content']
                ];
                $url = $this->big_data_url . 'ShowData/notificationMessage';
                HttpService::post($url, json_encode(['data' => $big_data, 'flag' => 0]));
            }
            $msg = '';
            if ($data['contact_type'] == 3) {
                $msg = '请尽快递交纸质合同原件到公司';
            }
            return $this->jsonend(1000, "签约成功" . $msg);
        }
        AsyncFile::write('contract_sign', date('Y-m-d H:i:s', time()) . '-' . $this->parm['contract_id'] . ':工程端签约失败');
        return $this->jsonend(-1000, "签约失败");
    }

    /**
     * showdoc
     * @catalog API文档/工程端/合同相关
     * @title 获取合同详情
     * @description 根据合同ID跟订单ID合同编号
     * @method POST
     * @url Engineer/Contract/getContractInfo
     * @param contract_id 必选 int 合同ID
     * @return
     * @return_param status sting 状态
     * @return_param contract_no sting 合同编号
     * @return_param contract_money sting 合同金额
     * @return_param contract_deposit sting 押金总和
     * @return_param is_effect sting 合同是否生效
     * @return_param effect_time sting 生效时间
     * @return_param contract_cycle sting 周期
     * @return_param contract_class sting 合同分类
     * @return_param esign_path sting 图片
     * @return_param contract_time_start_format sting 合同开始时间
     * @return_param contract_time_end_format sting 合同结束时间
     * @return_param contract_time_start sting 开始时间
     * @return_param contract_time_end sting 结束时间
     * @return_param province sting 省
     * @return_param city sting 市
     * @return_param area sting 区
     * @return_param address sting 详细地址
     * @return_param renew_money sting 续费金额
     * @return_param esign_user_sign sting 图片
     * @return_param area sting 区
     * @return_param area sting 区
     * @return_param rental_user_info array 用户信息(realname 姓名,auth_type 认证类型,id_number 身份证号,business_license_pic 营业执照,telphone 电话)
     * @return_param company_info array 公司信息(company_id ID,company_name 公司名称,business_license_pic 营业执照,company_addr 公司地址,telphone 座机,contacts 法人,contacts_tel 电话,company_seal 公司印章,status 状态)
     * @return_param equipments_info array 产品信息(list array(equipments_number 主板编号,equipments_name 主板名称,model 主板型号),total_num 主板数量)
     * @return_param original_parts array 原始配件(array(parts_id ID,parts_name 配件名称,parts_pic 配件图片,cycle 默认使用周期))
     * @return_param package_info array 套餐信息(package_mode 租赁模式,package_value 值,package_money 套餐金额,package_cycle 周期)
     * @return_param contract_additional array 合同补充协议()
     * @remark {"contract_id":"21"}
     * @number 0
     * @author lcy
     * @date 2018-10-18
     */
    public function http_getContractInfo()
    {
        if (empty($this->parm['contract_id'] ?? '')) {
            return $this->jsonend(-1001, '缺少参数');
        }
        $map['contract_id'] = $this->parm['contract_id'];
        $data = $this->contract_model->getOne($map, '*');
        if (empty($data)) {
            return $this->jsonend(-1000, "合同不存在");
        }
        /*         * **********************合同信息********************* */
        $contract_info = array();
        $contract_info['contact_type'] = $data['contact_type'];
        $contract_info['view_layout'] = $data['view_layout'];//合同界面布局样式1润泉版 2济南铭沁版
        $contract_info['status'] = $data['status'];
        $contract_info['operation_id'] = $data['operation_id'];
        $contract_info['company_id'] = $data['company_id']; // 入驻商ID
        $contract_info['contract_no'] = $data['contract_no']; //合同编号
        $contract_info['contract_money'] = $data['contract_money']; //合同金额
        $contract_info['contract_deposit'] = $data['contract_deposit']; //押金
        $contract_info['effect_time'] = empty($data['effect_time']) ? '' : date('Y-m-d', $data['effect_time']);
        if ($this->dev_mode == 1) {
            $contract_info['esign_path'] = empty($data['esign_filename']) ? '' : $this->config->get('debug_config.static_resource_host') . $data['esign_filename'];
        } else {
            $contract_info['esign_path'] = empty($data['esign_filename']) ? '' : $this->config->get('static_resource_host') . $data['esign_filename'];
        }
        if (empty($data['installed_time'])) $data['installed_time'] = time();
        $contract_info['contract_time_start_format'] = date('Y-m-d', $data['installed_time']); //合同开始时间
        $contract_info['contract_time_start'] = explode('-', $contract_info['contract_time_start_format']);
        $contract_info['province'] = $data['province'];
        $contract_info['city'] = $data['city'];
        $contract_info['area'] = $data['area'];
        $contract_info['address'] = $data['address'];
        $contract_info['capContractMoney'] = num_to_rmb($contract_info['contract_money']);
        $contract_info['signRepresent'] = '';//签约代表
        if ($this->dev_mode == 1) {
            $static_resource_host = $this->config->get('debug_config.static_resource_host');
        } else {
            $static_resource_host = $this->config->get('static_resource_host');
        }
        $contract_info['esign_user_sign'] = empty($data['esign_user_sign']) ? '' : $static_resource_host . $data['esign_user_sign'];
        $arr['contract_info'] = $contract_info;
        /**         * *********************乙方信息---客户********************* */
        $rental_user_info = array();
        //认证信息
        $auth_info = $this->auth_model->getAll(array('user_id' => $data['user_id']), '*');
        if (empty($auth_info) || $auth_info['status'] != 2) {
            return $this->jsonend(-1101, "该客户还未通过认证,请先认证");
        }
        $rental_user_info['realname'] = $auth_info['realname'];
        $rental_user_info['auth_type'] = $auth_info['auth_type'];
        $rental_user_info['id_number'] = $auth_info['id_number'];
        if (in_array($rental_user_info['auth_type'], [2, 3])) {
            $rental_user_info['realname'] = $auth_info['company_name'];
            $rental_user_info['id_number'] = $auth_info['business_license_number'];
        }
        $rental_user_info['business_license_pic'] = $auth_info['business_license_pic'];

        //基本信息
        $user_info = $this->user_model->getOne(array('user_id' => $data['user_id']), 'telphone,emergency_contact');
        $user_info['emergency_contact'] = empty($user_info['emergency_contact']) ? '' : json_decode($user_info['emergency_contact'], true);
        $rental_user_info['telphone'] = $user_info['telphone'];
        $rental_user_info['nailEmail'] = '';//邮箱
        $rental_user_info['user_id'] = $data['user_id'];
        $rental_user_info['nailUrgentTel'] = empty($user_info['emergency_contact']) ? '' : $user_info['emergency_contact']['tel'];//紧急联系电话
        $arr['rental_user_info'] = $rental_user_info;
        /**         * *********************甲方信息---公司********************* */
        $company_info = CompanyService::getCompanyInfo($data['company_id']);
        //获取运营中心的印章
        $operation_info = $this->operation_info_model->getOne(['o_id' => $contract_info['operation_id']], 'contract_company_seal,contract_company_name');
        if (!empty($operation_info) && !empty($operation_info['contract_company_seal'])) {
            $company_info['company_name'] = $operation_info['contract_company_name'];
            $company_info['company_seal'] = $this->config->get('qiniu.qiniu_url') . $operation_info['contract_company_seal'];
        }
        $arr['company_info'] = $company_info;


        /*         * **********************主板信息********************* */
        $equipment_info = array();
        $join = [
            ['equipment_lists as el', 'el.equipment_id = rq_contract_equipment.equipment_id', 'inner'],
            ['equipments as e', 'e.equipments_id = el.equipments_id', 'inner']
        ];
        $contract_ep_info = $this->contract_ep_model->getAll(array('rq_contract_equipment.contract_id' => $this->parm['contract_id']), '*', $join);
        if (!empty($contract_ep_info)) {
            foreach ($contract_ep_info as $k => $v) {
                $equipment['equipments_name'] = $v['equipments_name'];
                $equipment_info[] = $equipment;
            }
        }
        $arr['equipments_info']['list'] = $equipment_info;
        $arr['equipments_info']['total_num'] = count($equipment_info);

        /*         * **********************原始配件信息********************* */
        $parts = array();
        if (!empty($contract_ep_info)) {
            $original_parts = json_decode($contract_ep_info[0]['original_parts'], true);
            if (!empty($original_parts)) {
                foreach ($original_parts as $k => $v) {
                    $part_info = $this->parts_model->getOne(array('parts_id' => $v['parts_id']), 'parts_name,parts_pic');
                    $part['parts_id'] = $v['parts_id'];
                    $part['parts_name'] = $part_info['parts_name'];
                    $part['parts_pic'] = $this->config->get('qiniu.qiniu_url') . $part_info['parts_pic'];
                    $part['cycle'] = $v['cycle'];
                    $parts[] = $part;
                }
            }
        }

        $arr['original_parts'] = $parts;

        /*         * **********************合同套餐信息********************* */
        // 获取合同补充协议信息
        $where['rq_contract_additional.contract_id'] = $this->parm['contract_id'];
        $join = [
            ['equipment_lists el', 'el.equipment_id = rq_contract_additional.equipment_id', 'left join']
        ];
        $field = 'rq_contract_additional.*,el.device_no';
        $order = ['create_time' => 'DESC'];
        $contract_additional = $this->contractAdditionalModel->getAll($where, $field, $join, $order);
        if ($contract_additional) {
            $type = ['1' => '移机协议', '2' => '拆机协议'];
            $status = ['1' => '待签署', '2' => '已签署', '3' => '作废'];
            foreach ($contract_additional as $k => $v) {
                $contract_additional[$k]['type'] = $type[$v['type']];
                $contract_additional[$k]['status'] = $status[$v['status']];
                if (strlen($v['create_time']) > 5) {
                    $contract_additional[$k]['create_time'] = date('Y-m-d H:i:s', $v['create_time']);
                } else {
                    $contract_additional[$k]['create_time'] = '';
                }
                if (strlen($v['sign_time']) > 5) {
                    $contract_additional[$k]['sign_time'] = date('Y-m-d H:i:s', $v['sign_time']);
                } else {
                    $contract_additional[$k]['sign_time'] = '';
                }
            }
        }
        $arr['contract_additional'] = $contract_additional;
        //获取续费优惠券金额
        $this->Achievement->table = 'coupon_cate';
        $coupon = $this->Achievement->findData(['put_type' => 5, 'status' => 1], 'money');
        $arr['coupon'] = ($coupon['money'] ?? 0) / 100;

        return $this->jsonend(1000, "获取成功", $arr);
    }

    /**
     * showdoc
     * @catalog API文档/工程端/合同相关
     * @title 获取我的合同
     * @description 根据token
     * @method POST
     * @url Engineer/Contract/getMyContractList
     * @param
     * @return
     * @return_param contract_no string 合同编号
     * @return_param renew_date string 续费周期
     * @return_param province string 省
     * @return_param city string 市
     * @return_param area string 区
     * @return_param address string 详细地址
     * @return_param contract_id string 合同ID
     * @return_param status string 状态
     * @return_param contract_class string 合同类型
     * @return_param status_name string 状态翻译
     * @remark
     * @number 0
     * @author lcy
     * @date 2018-10-18
     */
    public function http_getMyContractList()
    {
        $page = empty($this->parm['page'] ?? '') ? 1 : $this->parm['page'];
        $pagesize = empty($this->parm['pagesize'] ?? '') ? 1 : $this->parm['pagesize'];
        $map['user_id'] = $this->user_id;
        $map['status'] = ['IN', [3, 4, 5]];
        $field = 'contract_no,renew_date,province,city,area,address,contract_id,status';
        $data = $this->contract_model->getAll($map, $field, ['add_time' => 'DESC'], $page, $pagesize);
        if (empty($data)) {
            return $this->jsonend(-1003, "暂无相关数据");
        }
        foreach ($data as $k => $v) {
            $data[$k]['status_name'] = $this->config->get('contract_status')[$v['status']];
        }
        return $this->jsonend(1000, "获取列表成功", $data);
    }

    /**
     * showdoc
     * @catalog API文档/工程端/合同相关
     * @title 重新下发套餐
     * @description
     * @method POST
     * @url /Engineer/Contract/againPackage
     * @param contract_id 必选 int 合同ID
     * @param equipment_id 必选 int 主板ID
     * @return
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-12-5
     */
    public function http_againPackage()
    {
        if (empty($this->parm['contract_id'] ?? '')) {
            return $this->jsonend(-1002, "缺少参数合同ID");
        }
        if (empty($this->parm['equipment_id'] ?? '')) {
            return $this->jsonend(-1002, "缺少参数主板ID");
        }
        $info = $this->ContractPackageRecordModel->getOne(['contract_id' => $this->parm['contract_id'], 'equipment_id' => $this->parm['equipment_id'], 'status' => 1]);
        if (empty($info)) {
            return $this->jsonend(-1000, "不存在未下发的套餐");
        }
        //查询主板编号
        $eq = $this->equipment_list_model->findEquipmentLists(['equipment_id' => $this->parm['equipment_id']], 'device_no,switch_machine,status');
        if (empty($eq)) {
            return $this->jsonend(-1000, "主板不存在");
        }
//        if ($eq['status'] == 1) {
//            return $this->jsonend(-1000, "主板已离线，请联网后再下发");
//        }
        if ($eq['switch_machine'] == 0) {
            return $this->jsonend(-1000, "主板已关机，请开机后再下发");
        }
        //下发-绑定套餐
        $params = ['sn' => $eq['device_no'], 'working_mode' => $info['working_mode'], 'filter_element_max' => json_decode($info['filter_element_max'], true)];
        $path = '/House/Issue/bindingPackage';
        HttpService::Thrash($params, $path);
        sleepCoroutine(2000);
        //下发--数据同步
        $data_sync_params['sn'] = $eq['device_no'];
        $data_sync_params['used_days'] = $info['used_days'];
        $data_sync_params['used_traffic'] = $info['used_traffic'];
        $data_sync_params['remaining_days'] = $info['remaining_days'];
        $data_sync_params['remaining_traffic'] = $info['remaining_traffic'];
        $data_sync_path = '/House/Issue/data_sync';
        HttpService::Thrash($data_sync_params, $data_sync_path);
        $this->redis->del('device_heartbeat:' . $eq['device_no']);//删除redis心跳
        //请求心跳
        sleepCoroutine(2000);
        $state_params['sn'] = $eq['device_no'];
        $state_path = '/House/Issue/heartbeat';
        HttpService::Thrash($state_params, $state_path);
        //修改下发记录
        $res = $this->ContractPackageRecordModel->save(['id' => $info['id']], ['last_hand_time' => time()]);
        if ($res) {
            return $this->jsonend(1000, "请求发送成功,正在下发中");
        }
        return $this->jsonend(1000, "请求发送失败");
    }


    /**
     * showdoc
     * @catalog API文档/工程端/合同相关
     * @title 获取合同详情列表
     * @description
     * @method POST
     * @url Engineer/Contract/getEnergySavingDataList
     * @param work_order_id 必选 工单ID
     * @return
     * @return_param business_data array 工单类型3改造9试用
     * @return_param contract_no string 合同编码
     * @remark
     * @number 0
     * @author xln
     * @date 2020-9-18
     */
    public function http_getEnergySavingDataList()
    {
        if (empty($this->parm['work_order_id'])) return $this->jsonend(ReturnCodeService::FAIL, '参数缺失');
        $work_work_data = $this->work_order_model->getOne(['work_order_id' => $this->parm['work_order_id']], 'contract_id');
        if (empty($work_work_data['contract_id'])) return $this->jsonend(ReturnCodeService::FAIL, '暂无数据');
        $contract_id = $work_work_data['contract_id'];
        //查询工单类型
        $data = $this->contract_model->enclosure($contract_id);
        return $this->jsonend(ReturnCodeService::SUCCESS, '获取成功', $data);
    }




    /**
     * showdoc
     * @catalog API文档/工程端/合同相关
     * @title 获取合同附件
     * @description
     * @method POST
     * @url /Engineer/Contract/contractEnclosure
     * @param work_order_id 必选 int 合同ID或者识别表ID
     * @return_param new_name string 图片路径
     * @return_param new_name_url string 完整图片路径
     * @return_param old_name string 名称
     * @return_param type string 类型
     * @return
     * @remark
     * @number 0
     * @author xln
     * @date 2020-9-18
     */
    public function http_contractEnclosure(){
        if (empty($this->parm['contract_id'])) {
            if (empty($this->parm['identification_id'])) return $this->jsonend(ReturnCodeService::FAIL, '参数缺失：合同ID或识别ID');
            $this->Achievement->table = 'contract';
            $where = ['identification_id' => $this->parm['identification_id']];
            $contract_data = $this->Achievement->findData($where, 'contract_id');
            if (empty($contract_data)) return $this->jsonend(ReturnCodeService::FAIL, '暂无数据');
            $contract_id = $contract_data['contract_id'];
        } else {
            $contract_id = $this->parm['contract_id'];
        }

        $contract_data = $this->contract_model->contractEnclosure($contract_id,$this->parm['type']??1);
        if (empty($contract_data)) return $this->jsonend(ReturnCodeService::FAIL,'暂无数据');
        return $this->jsonend(ReturnCodeService::SUCCESS,'获取成功',$contract_data);
    }


}
