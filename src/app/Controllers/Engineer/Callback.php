<?php

namespace app\Controllers\Engineer;

use app\Library\SLog;
use app\Services\Common\NotifiyService;
use app\Services\Company\CompanyService;
use app\Services\Log\FileLogService;
use Server\CoreBase\Controller;
use app\Wechat\WxPay;
use Server\CoreBase\SwooleException;
use app\Services\Common\ConfigService;
use app\Services\Common\HttpService;

/**
 * 支付回调
 * Class Callback
 * @package app\Controllers\User
 */
class Callback extends Controller
{
    /**
     * 初始化
     * Callback constructor.
     */

    private $user_model;
    protected $renew_eq_model;
    protected $GetIPAddressHttpClient;
    protected $dev_mode;
    protected $app_debug;
    protected $wx_config;
    protected $engineers_model;
    protected $work_order_business_model;
    protected $work_order_model;
    protected $partsModel;
    protected $orderModel;
    protected $return_model;
    protected $equipment_lists_model;
    protected $bind_model;
    protected $contract_eq_model;
    protected $work_eq_model;
    protected $ep_part_model;
    protected $parts_model;
    protected $equip_model;
    protected $contract_model;
    protected $businessTypeModel;
    protected $work_order_problem_model;
    protected $for_core_model;
    protected $shift_record_model;
    protected $disassemble_record_model;
    protected $repair_record_model;
    protected $new_install_business_id;
    protected $repair_business_id;
    protected $move_business_id;
    protected $for_core_business_id;
    protected $disassemble_business_id;
    protected $contract_additional_model;
    protected $additionalServiceModel;
    protected $work_additional_service_model;
    protected $engineer_bill_model;
    protected $user_bill_model;
    protected $work_log_model;
    protected $log_model;
    protected $ContractPackageRecordModel;
    protected $EquipmentLogModel;
    protected $BossApiClient;
    protected $finance_record_model;
    protected $renewOrderModel;
    protected $Achievement;
    protected $contract_log_model;
    protected $contract_ep_model;
    protected $renew_record_model;
    protected $service_payment_config;


    public function initialization($controller_name, $method_name)
    {
        parent::initialization($controller_name, $method_name);
        $this->company = $this->request->company;
        $this->work_order_model = $this->loader->model('WorkOrderModel', $this);
        $this->partsModel = $this->loader->model('PartsModel', $this);
        $this->orderModel = $this->loader->model('OrderModel', $this);
        $this->return_model = $this->loader->model('WorkOrderReturnModel', $this);
        $this->equipment_lists_model = $this->loader->model('EquipmentListsModel', $this);
        $this->bind_model = $this->loader->model('CustomerBindEquipmentModel', $this);
        $this->contract_eq_model = $this->loader->model('ContractEquipmentModel', $this);
        $this->work_eq_model = $this->loader->model('WorkEquipmentModel', $this);
        $this->ep_part_model = $this->loader->model('EquipmentsPartsModel', $this);
        $this->parts_model = $this->loader->model('PartsModel', $this);
        $this->equip_model = $this->loader->model('EquipmentModel', $this);
        $this->contract_model = $this->loader->model('ContractModel', $this);
        $this->user_model = $this->loader->model('CustomerModel', $this);
        $this->businessTypeModel = $this->loader->model('BusinessTypeModel', $this);
        $this->work_order_problem_model = $this->loader->model('WorkOrderProblemModel', $this);
        $this->engineers_model = $this->loader->model('EngineersModel', $this);
        $this->work_order_business_model = $this->loader->model('WorkOrderBusinessModel', $this);
        $this->for_core_model = $this->loader->model('ForCoreRecordModel', $this);
        $this->shift_record_model = $this->loader->model('ShiftRecordModel', $this);
        $this->disassemble_record_model = $this->loader->model('DisassembleRecordModel', $this);
        $this->repair_record_model = $this->loader->model('RepairRecordModel', $this);
        $this->contract_additional_model = $this->loader->model('ContractAdditionalModel', $this);
        $this->additionalServiceModel = $this->loader->model('AdditionalServiceModel', $this);
        $this->work_additional_service_model = $this->loader->model('WorkAdditionalServiceModel', $this);
        $this->engineer_bill_model = $this->loader->model('EngineerBillModel', $this);
        $this->user_bill_model = $this->loader->model('CustomerBillModel', $this);
        $this->work_log_model = $this->loader->model('WorkOrderLogModel', $this);
        $this->log_model = $this->loader->model('OrderLogModel', $this);
        $this->ContractPackageRecordModel = $this->loader->model('ContractPackageRecordModel', $this);
        $this->EquipmentLogModel = $this->loader->model('EquipmentLogModel', $this);
        $this->finance_record_model = $this->loader->model('FinanceRecordModel', $this);
        $this->renewOrderModel = $this->loader->model('RenewOrderModel', $this);
        $this->Achievement = $this->loader->model('AchievementModel', $this);
        $this->contract_log_model = $this->loader->model('ContractLogModel', $this);
        $this->contract_ep_model = $this->loader->model('ContractEquipmentModel', $this);
        $this->renew_eq_model = $this->loader->model('RenewOrderEquipmentModel', $this);
        $this->renew_record_model = $this->loader->model('RenewRecordModel', $this);
        //业务类型ID
        $this->new_install_business_id = 3; //新装业务ID
        $this->repair_business_id = 6; //维修业务ID
        $this->move_business_id = 5; //移机业务ID
        $this->for_core_business_id = 2; //换芯
        $this->disassemble_business_id = 4; //拆机
        $company_config = CompanyService::getCompanyConfig($this->company);
        $this->wx_config = $company_config['engineer_wx_config'];
        $this->service_payment_config = $this->config->get('service_payment_config');


    }

    public function __construct()
    {
        parent::__construct();
        $this->GetIPAddressHttpClient = get_instance()->getAsynPool('BuyWater');
        $this->BossApiClient = get_instance()->getAsynPool('BossApi');
    }

    /**
     * 回单扫码付
     * @desc 描述
     * 参数名  类型  是否必须    说明
     * @return mixed
     * @throws SwooleException
     * @date 2019/8/12 16:39
     * @author ligang
     * @RequestType GET|POST
     */
    public function http_scanPayCallback()
    {

        $postStr = $this->http_input->getRawContent();
        file_put_contents('scan_pay_cloud.txt', $postStr);
        //  $postStr = file_get_contents('scan_pay_cloud.txt');

        $postObj = simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA);
        if ($postObj->result_code == 'SUCCESS' && $postObj->return_code == 'SUCCESS') {
            $order_sn = $postObj->out_trade_no;
            $order = $this->work_order_model->getWorkDetail(['scan_order_number' => $order_sn], array(), '*');
            if (empty($order)) {
                $return['return_code'] = 'FAIL';
                $return['return_msg'] = 'error';
                $return = $this->arrayToXml($return);
                return $this->response->end($return);
            }
            if ($order['pay_status'] == 2) {
                $return['return_code'] = 'SUCCESS';
                $return['return_msg'] = 'OK';
                $return = $this->arrayToXml($return);
                return $this->response->end($return);
            }
            if ($order['pay_mode'] == 2) {
                $wx_config = $this->service_payment_config;
            } else {
                $company_config = CompanyService::getCompanyConfig($order['company_id']);
                $wx_config = $company_config['mp_config'];
            }
            $wx_config['payment_type'] = 'NATIVE';
            //$WxPay = new WxPay($wx_config);
            $WxPay = WxPay::getInstance($wx_config);
            if ($WxPay->isSetConfig) $WxPay->setConfig($wx_config);
            $signObj = [];
            foreach ($postObj as $key => $val) {
                if ($key != 'sign') {
                    $signObj[$key] = $val;
                }
            }

            //验证签名
            if ($WxPay->getSign($signObj) == $postObj->sign) {
                $order['wx_callback_num'] = $postObj->transaction_id;
                $total_fee = $postObj->total_fee;
                $status = false;
                $this->db->begin(function () use ($order, &$data, &$status, $total_fee) {
                    $this->successBack($order['work_order_id'], $order['wx_callback_num'], $total_fee);
                    $this->getWorkorderRange($order['work_order_id']);
                    //新增资金记录
                    $finance_record['company_id'] = $order['company_id'];
                    $finance_record['type'] = 2;
                    $finance_record['work_order_id'] = $order['work_order_id'];
                    $finance_record['user_id'] = $order['user_id'];
                    $finance_record['money'] = $total_fee;
                    $finance_record['create_time'] = time();
                    $finance_record['is_online_pay'] = 1;
                    $finance_record['pay_way'] = 1;
                    $finance_record['callback_num'] = $order['wx_callback_num'];
                    $finance_record['payment_method'] = 1;
                    $finance_record['payment_uid'] = $order['user_id'];
                    $finance_record['o_id'] = $order['operation_id'];
                    $finance_record['a_id'] = $order['administrative_id'];
                    $finance_record['company_id'] = $order['company_id'];
                    $this->finance_record_model->add($finance_record);
                    $status = true;
                });
                if ($status) {
                    $return['return_code'] = 'SUCCESS';
                    $return['return_msg'] = 'OK';
                    $return = $WxPay->arrayToXml($return);
                    return $this->response->end($return);
                }
            }
        }
        $return['return_code'] = 'FAIL';
        $return['return_msg'] = 'error';
        $return = $WxPay->arrayToXml($return);
        return $this->response->end($return);
    }


    /**
     * 回单工程代付
     * @throws SwooleException
     * @date 2019/8/12 16:40
     */
    public function http_payCallback()
    {

        $postStr = $this->http_input->getRawContent();
        file_put_contents('work_cloud.txt', $postStr);
       //   $postStr = file_get_contents('work_cloud.txt');

        $postObj = simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA);
        if ($postObj->result_code == 'SUCCESS' && $postObj->return_code == 'SUCCESS') {
            $order_sn = $postObj->out_trade_no;
            $order = $this->work_order_model->getWorkDetail(['order_number' => $order_sn], array(), '*');
            if (empty($order)) {
                $return['return_code'] = 'FAIL';
                $return['return_msg'] = 'error';
                $return = $this->arrayToXml($return);
                return $this->response->end($return);
            }
            if ($order['pay_status'] == 2) {
                $return['return_code'] = 'SUCCESS';
                $return['return_msg'] = 'OK';
                $return = $this->arrayToXml($return);
                return $this->response->end($return);
            }
            if ($order['pay_mode'] == 2) {
                $wx_config = $this->service_payment_config;
            } else {
                $company_config = CompanyService::getCompanyConfig($order['company_id']);
                $wx_config = $company_config['engineer_wx_config'];
            }

            $wx_config['payment_type'] = 'JSAPI';
            //$WxPay = new WxPay($wx_config);
            $WxPay = WxPay::getInstance($wx_config);
            if ($WxPay->isSetConfig) $WxPay->setConfig($wx_config);
            $signObj = [];
            foreach ($postObj as $key => $val) {
                if ($key != 'sign') {
                    $signObj[$key] = $val;
                }
            }
            //验证签名
            if ($WxPay->getSign($signObj) == $postObj->sign) {
                $order['wx_callback_num'] = $postObj->transaction_id;
                $total_fee = $postObj->total_fee;
                $status = false;
                $this->db->begin(function () use ($order, &$data, &$status, $total_fee) {
                    $this->successBack($order['work_order_id'], $order['wx_callback_num'], $total_fee);
                    $this->getWorkorderRange($order['work_order_id']);
                    //新增资金记录
                    $finance_record['company_id'] = $order['company_id'];
                    $finance_record['type'] = 2;
                    $finance_record['work_order_id'] = $order['work_order_id'];
                    $finance_record['user_id'] = $order['user_id'];
                    $finance_record['money'] = $total_fee;
                    $finance_record['create_time'] = time();
                    $finance_record['is_online_pay'] = 1;
                    $finance_record['pay_way'] = 1;
                    $finance_record['callback_num'] = $order['wx_callback_num'];
                    $finance_record['payment_method'] = 2;
                    $finance_record['payment_uid'] = $order['repair_id'];
                    $finance_record['o_id'] = $order['operation_id'];
                    $finance_record['a_id'] = $order['administrative_id'];
                    $this->finance_record_model->add($finance_record);
                    $status = true;
                });
                if ($status) {
                    $return['return_code'] = 'SUCCESS';
                    $return['return_msg'] = 'OK';
                    $return = $WxPay->arrayToXml($return);
                    return $this->response->end($return);
                }
            }
        }
        $return['return_code'] = 'FAIL';
        $return['return_msg'] = 'error';
        $return = $WxPay->arrayToXml($return);
        return $this->response->end($return);
    }

    //工单支付成功回调
    public function successBack($work_order_id, $wx_callback_num, $total_fee)
    {
        try {
            //工单信息
            $map['work_order_id'] = $work_order_id;
            $work_order_info = $this->work_order_model->getWorkDetail($map);
            if (empty($work_order_info)) {
                return $this->jsonend(-1101, "工单不存在");
            }
            //工单主板
            $work_eq_info = $this->work_eq_model->getAll(array('work_order_id' => $work_order_id), 'equipment_id,new_device_no,is_change_device');
            if ($work_order_info['is_circumscribed'] != 1) {
                if (empty($work_eq_info)) {
                    return $this->jsonend(-1101, "工单信息错误");
                }
            }
            //工单业务类型
            $work_business = $this->work_order_business_model->getAll(array('work_order_id' => $work_order_id), [], 'business_id');
            $business = [];
            if (empty($work_business)) {
                return $this->jsonend(-1101, "工单信息错误");
            }
            foreach ($work_business as $k => $v) {
                array_push($business, $v['business_id']);
            }

            // 工单修改条件
            $where = ['work_order_id' => $work_order_id];
            // 工单修改数据
            $data['work_order_status'] = 11;
            $data['complete_time'] = time();
            $data['order_pay_time'] = time();
            $data['pay_status'] = 2;
            $data['wx_callback_num'] = $wx_callback_num;
            $this->work_order_model->editWork($where, $data); //修改工单信息
            $engineers_info = $this->engineers_model->getOne(array('engineers_id' => $work_order_info['repair_id']), 'engineers_name,engineers_id,engineers_phone,openid');
            $this->addWorkOrderLog($work_order_id, time(), 11, "【维修端】工程人员【" . $engineers_info['engineers_name'] . "】【工程人员ID:" . $engineers_info['engineers_id'] . "】已完成工单", $engineers_info['engineers_id'],'工程人员:'.$engineers_info['engineers_name'].',电话:'.$engineers_info['engineers_phone']);
            $record['work_order_id'] = $work_order_id;
            $record['user_id'] = $work_order_info['user_id'];
            $record['repair_id'] = $work_order_info['repair_id'];
            if ($work_order_info['is_circumscribed'] != 1) {
                $record['equipment_id'] = $work_eq_info[0]['equipment_id'];
                $record['contract_id'] = $work_order_info['contract_id'];
            }
            //维修
            if (in_array($this->repair_business_id, $business)) {
                $repair_record = $record;
                $repair_record['repair_record_time'] = time();
                $this->repair_record_model->add($repair_record); //添加记录
            }
            //拆机
            if (in_array($this->disassemble_business_id, $business)) {
                $disassemble_record = $record;
                $disassemble_record['disassemble_record_time'] = time();
                $this->disassemble_record_model->add($disassemble_record); //添加记录
                if ($work_order_info['is_circumscribed'] != 1) {
                    //新增合同补充协议
                    $add_data['contract_id'] = $work_order_info['contract_id'];
                    $add_data['equipment_id'] = $work_eq_info[0]['equipment_id'];
                    $add_data['type'] = 2;
                    $add_data['create_time'] = time();
                    $this->contract_additional_model->add($add_data);
                    //修改合同主板状态
                    $this->contract_eq_model->save(array('contract_id' => $work_order_info['contract_id'], 'equipment_id' => $work_eq_info[0]['equipment_id']), array('disassemble_time' => time(), 'state' => 0));
                    //如果合同的主板全部拆机，将合同冻结
                    $contr_eq_info = $this->contract_eq_model->getAll(array('contract_id' => $work_order_info['contract_id'], 'state' => 1), 'id');
                    if (count($contr_eq_info) == 0) {
                        $this->contract_model->save(array('contract_id' => $work_order_info['contract_id']), array('status' => 6));
                    }
                    //修改用户绑定主板状态
                    $this->bind_model->save(array('equipment_id' => $work_eq_info[0]['equipment_id']), array('status' => 2));
                }
            }
            //移机
            if (in_array($this->move_business_id, $business)) {
//            //主板信息
//            $eq_info = $this->equipment_lists_model->findEquipmentLists(array('equipment_id' => $work_eq_info[0]['equipment_id']), 'equipment_id,contract_id');
//            if (empty($eq_info)) {
//                throw new SwooleException("主板信息错误");
//            }
                $shift_record = $record;
                $shift_record['old_province_code'] = $work_order_info['province_code'];
                $shift_record['old_city_code'] = $work_order_info['city_code'];
                $shift_record['old_area_code'] = $work_order_info['area_code'];
                $shift_record['old_address'] = $work_order_info['province'] . $work_order_info['city'] . $work_order_info['area'] . '-' . $work_order_info['service_address'];
                $shift_record['move_province_code'] = $work_order_info['move_province_code'];
                $shift_record['move_city_code'] = $work_order_info['move_city_code'];
                $shift_record['move_area_code'] = $work_order_info['move_area_code'];
                $shift_record['move_address'] = $work_order_info['move_province'] . $work_order_info['move_city'] . $work_order_info['move_area'] . '-' . $work_order_info['move_address'];
                $shift_record['shift_recordd_time'] = time();
                $this->shift_record_model->add($shift_record); //添加记录
//                //更新主板地址
//                $eq['province_code'] = $work_order_info['move_province_code'];
//                $eq['city_code'] = $work_order_info['move_city_code'];
//                $eq['area_code'] = $work_order_info['move_area_code'];
//                $eq['province'] = $work_order_info['move_province'];
//                $eq['city'] = $work_order_info['move_city'];
//                $eq['area'] = $work_order_info['move_area'];
//                $eq['address'] = $work_order_info['move_address'];
//                if ($work_order_info['is_circumscribed'] != 1) {
//                    $this->equipment_lists_model->updateEquipmentLists($eq, array('equipment_id' => $work_eq_info[0]['equipment_id']));
//                    //修改合同主板地址
//                    $eq_contr_map['contract_id'] = $eq_info['contract_id'];
//                    $eq_contr_map['equipment_id'] = $eq_info['equipment_id'];
//                    $eq_contr_info = $this->contract_eq_model->getOne($eq_contr_map, 'moving_machine_number');
//                    if (empty($eq_contr_info)) {
//                        file_put_contents('receipt_work_order_record.txt', date('Y-m-d H:i:s') . ':' . $work_order_id . ',错误原因:合同里无该主板信息' . PHP_EOL, FILE_APPEND);
//                        throw new SwooleException("合同里无该主板信息");
//                    }
//                    $eq_contr_data['equipments_address'] = $work_order_info['move_province'] . $work_order_info['move_city'] . $work_order_info['move_area'] . $work_order_info['move_address'];
//                    $eq_contr_data['uptime'] = time();
//                    $eq_contr_data['moving_machine_number'] = $eq_contr_info['moving_machine_number'] + 1;
//                    $this->contract_eq_model->save($eq_contr_map, $eq_contr_data);
//                    //新增合同补充协议
//                    $add_data['contract_id'] = $eq_info['contract_id'];
//                    $add_data['equipment_id'] = $eq_info['equipment_id'];
//                    $add_data['type'] = 1;
//                    $add_data['old_address'] = $work_order_info['province'] . $work_order_info['city'] . $work_order_info['area'] . $work_order_info['service_address'];
//                    $add_data['move_address'] = $work_order_info['move_province'] . $work_order_info['move_city'] . $work_order_info['move_area'] . $work_order_info['move_address'];
//                    $add_data['create_time'] = time();
//                    $this->contract_additional_model->add($add_data);
//                }
            }
            //新装
            if (in_array($this->new_install_business_id, $business)) {
                //后台新增的工单无订单信息,不对订单状态进行处理
                if (!empty($work_order_info['order_id'])) {
                    //修改订单状态--已安装
                    $this->orderModel->save(array('order_id' => $work_order_info['order_id']), array('order_status' => 4, 'pay_status' => 2, 'pay_time' => time()));
                    //添加订单日志
                    $order_info = $this->orderModel->getOne(array('order_id' => $work_order_info['order_id']), 'order_no');
                    $this->addOrderLog($work_order_info['order_id'], 4, '订单已完成安装,通过工单支付完成,订单编号:' . $order_info['order_no'], 1);
                }
            }
//            if ($work_order_info['is_circumscribed'] != 1) {
//                //如果更换了主板
//                if ($work_eq_info[0]['is_change_device'] == 1) {
//                    $this->editEquip($work_eq_info[0]['equipment_id'], $eq_info['device_no'], $work_eq_info[0]['new_device_no'], $eq_info['contract_id']);
//                }
//            }

            //新增用户付款记录
            $user_bill['user_id'] = $work_order_info['user_id'];
            $user_bill['work_order_id'] = $work_order_id;
            $user_bill['money'] = $total_fee;
            $user_bill['pay_type'] = 1;
            $user_bill['pay_way'] = $work_order_info['pay_way'];
            $user_bill['pay_time'] = time();
            $user_bill['pay_client'] = $work_order_info['pay_client'];
            $user_bill['status'] = 1;
            $this->user_bill_model->add($user_bill);
            //新增工程人员收款记录
            $engineer_bill['engineers_id'] = $work_order_info['repair_id'];
            $engineer_bill['work_order_id'] = $work_order_id;
            $engineer_bill['money'] = $total_fee;
            $engineer_bill['pay_type'] = 3;
            $engineer_bill['pay_way'] = $work_order_info['pay_way'];
            $engineer_bill['pay_time'] = time();
            $engineer_bill['status'] = 1;
            $engineer_bill['pay_client'] = $work_order_info['pay_client'];
            $this->engineer_bill_model->add($engineer_bill);
        } catch (\Throwable $e) {
            $return['return_code'] = 'FAIL';
            $return['return_msg'] = $e->getMessage();
            $this->response->header('Content-Type', 'text/html; charset=UTF-8');
            $output = json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return $this->response->end($output);
        }

        //支付成功，异步发送消息给师傅
        $user_info = $this->user_model->getOne(['user_id' => $work_order_info['user_id']], 'telphone,realname');

        //支付方式
        if ($work_order_info['pay_way'] == 1 && $work_order_info['pay_client'] == 1) {
            $pay_way = '用户微信扫码支付';
        } elseif ($work_order_info['pay_way'] == 1 && $work_order_info['pay_client'] == 2) {
            $pay_way = '工程人员微信代支付';
        } else {
            $pay_way = '未知';
        }
        $engineer_notice_config = ConfigService::getTemplateConfig('TransactionReminders', 'engineer', $this->company);
        if (!empty($engineer_notice_config) && !empty($engineer_notice_config['template']) && $engineer_notice_config['template']['switch']) {
            $template_id = $engineer_notice_config['template']['wx_template_id'];
            $mp_template_id = $engineer_notice_config['template']['mp_template_id'];
            //通知数据组装
            $template_data = [
                'keyword1' => ['value' => $work_order_info['order_number']],
                'keyword2' => ['value' => date('Y-m-d H:i:s', time())],
                'keyword3' => ['value' => $pay_way],
                'keyword4' => ['value' => number_format($work_order_info['combo_money'] / 100, 2) . ' 元'],
                'keyword5' => ['value' => $work_order_info['contacts']],
                'keyword6' => ['value' => $work_order_info['contact_number']],
                'keyword7' => ['value' => '交易通知'],
            ];

            $template_tel = $engineers_info['engineers_phone'];
            $template_content = '工单编号：' . $work_order_info['order_number'] . ' 客户姓名：' . $user_info['realname'] ?? '' . ' ';
            $template_content .= '客户电话：' . $user_info['telphone'] . ' 的用户支付成功 ';
            $template_content .= '支付金额：' . number_format($work_order_info['combo_money'] / 100, 2) . ' 元';
            $template_content .= '支付方式：' . $pay_way;
            $template_url = 'pages/order/orderDetail/orderDetail?id=' . $work_order_id;
            $weapp_template_keyword = '';
            $mp_template_data = [
                'first' => ['value' => '您的订单已经支付成功，感谢您对的支持', 'color' => '#4e4747'],
                'keyword1' => ['value' => '工程师傅回单支付', 'color' => '#4e4747'],
                'keyword2' => ['value' => $pay_way, 'color' => '#4e4747'],
                'keyword3' => ['value' => number_format($work_order_info['combo_money'] / 100, 2) . ' 元', 'color' => '#4e4747'],
                'keyword4' => ['value' => date('Y-m-d H:i:s', time()), 'color' => '#4e4747'],
                'remark' => ['value' => '若有疑问可致电客服，查询工单信息', 'color' => '#173177'],
            ];
            NotifiyService::sendNotifiyAsync($engineers_info['openid'], $template_data, $template_id, $template_tel, $template_content, $template_url, $weapp_template_keyword, $mp_template_id, $mp_template_data, 'engineer', $this->company);
        }
    }

    /**
     * @desc   添加订单操作日志
     * @param
     * @date   2018-07-19
     * @return [type]     [description]
     * @author lcx
     */
    public function addOrderLog($order_id, $status, $remark, $is_to_user = 0)
    {
        $data['operater_role'] = 2;
        $data['order_id'] = $order_id;
        $data['status'] = $status;
        $data['create_time'] = time();
        $data['remark'] = $remark;
        $data['is_to_user'] = $is_to_user;
        $res = $this->log_model->add($data);
        return $res;
    }

    /**
     * @desc   添加工单操作日志
     * @param
     * @date   2018-07-19
     * @return [type]     [description]
     * @author lcx
     */
    public function addWorkOrderLog($work_order_id, $time, $status, $remark, $user_id,$open_remarks='')
    {
        $log['work_order_id'] = $work_order_id;
        $log['create_work_time'] = $time;
        $log['operating_time'] = time();
        $log['do_id'] = $user_id;
        $log['operating_type'] = 2;
        $log['operating_status'] = $status;
        $log['remarks'] = $remark;
        $lang = $this->config->get('work_order_status_desc')[$status] ?? '';
        $log['open_remarks'] = empty($open_remarks) ? $lang : $lang . '!' . $open_remarks;
        $res = $this->work_log_model->add($log);
        return $res;
    }

    /**
     * 对象 转 数组
     * @param object $obj 对象
     * @return array
     */
    protected function object_to_array($obj)
    {
        $obj = (array)$obj;
        foreach ($obj as $k => $v) {
            if (gettype($v) == 'resource') {
                return;
            }
            if (gettype($v) == 'object' || gettype($v) == 'array') {
                $obj[$k] = (array)object_to_array($v);
            }
        }

        return $obj;
    }

    //如果更换了主板，支付回调修改主板信息
    public function editEquip($eq_id, $old_device_no, $new_device_no, $contract_id)
    {
        $old_device_info = $this->equipment_lists_model->findEquipmentLists(['equipment_id' => $eq_id]);
        $bind_info = $this->bind_model->getOne(['equipment_id' => $eq_id, 'user_id']);
        if (empty($old_device_info) || empty($bind_info)) {
            return false;
        }
        $r = $this->equipment_lists_model->updateEquipmentLists(['device_no' => $new_device_no], ['equipment_id' => $eq_id]); //修改主板编号
        $filter_element = json_decode($old_device_info['filter_element'], true);
        $eq_filter_element_num = [0, 0, 0, 0, 0];
        if (!empty($filter_element)) {
            foreach ($filter_element as $k => $v) {
                array_push($eq_filter_element_num, $v['max']);
            }
        }
        //重新下发套餐
        //下发-绑定套餐
        $params = ['sn' => $new_device_no, 'working_mode' => $old_device_info['working_mode'], 'filter_element_max' => $eq_filter_element_num];
        $path = '/House/Issue/bindingPackage';
        HttpService::Thrash($params, $path);
        sleepCoroutine(2000);
        //下发--数据同步
        $data_sync_params['sn'] = $new_device_no;
        $data_sync_params['used_days'] = $old_device_info['used_days'];
        $data_sync_params['used_traffic'] = $old_device_info['used_traffic'];
        $data_sync_params['remaining_days'] = $old_device_info['remaining_days'];
        $data_sync_params['remaining_traffic'] = $old_device_info['remaining_traffic'];

        $data_sync_path = '/House/Issue/data_sync';
        HttpService::Thrash($data_sync_params, $data_sync_path);
        //请求心跳
        sleepCoroutine(2000);
        $state_params['sn'] = $new_device_no;
        $state_path = '/House/Issue/heartbeat';
        HttpService::Thrash($state_params, $state_path);
        //保存套餐信息
        $package_record['contract_id'] = $contract_id;
        $package_record['equipment_id'] = $eq_id;
        $package_record['used_days'] = $data_sync_params['used_days'];
        $package_record['remaining_days'] = $data_sync_params['remaining_days'];
        $package_record['used_traffic'] = $data_sync_params['used_traffic'];
        $package_record['remaining_traffic'] = $data_sync_params['remaining_traffic'];
        $package_record['working_mode'] = $old_device_info['working_mode'];
        $package_record['filter_element_max'] = json_encode($eq_filter_element_num);
        $package_record['hand_time'] = time();
        $package_record['last_hand_time'] = time();
        $package_record['status'] = 1;
        $this->ContractPackageRecordModel->add($package_record);
        //新增主板日志
        $log['user_id'] = $bind_info['user_id'];
        $log['equipment_id'] = $eq_id;
        $log['type'] = 9;
        $log['user_role'] = 2;
        $log['remark'] = '更换主板，旧主板编号:' . $old_device_no . ',新主板编号:' . $new_device_no;
        $log['create_time'] = time();
        $this->EquipmentLogModel->add($log);
    }

    //根据工单ID获取工单类型以及保修时间
    function getWorkorderRange($work_order_id)
    {
        $work_eq_join = [
            ['equipment_lists el', 'el.equipment_id = rq_work_equipment.equipment_id', 'left join'],
        ];
        $equipment_info = $this->work_eq_model->getAll(array('rq_work_equipment.work_order_id' => $work_order_id), 'rq_work_equipment.equipment_id,el.device_no,start_time,end_time', ['rq_work_equipment.create_time' => 'DESC'], $work_eq_join);
        if (empty($equipment_info)) {
            return false;
        }
        return $equipment_info[0];
    }

    //续费支付回调
    public function http_rentRenewOrder()
    {

        //验证微信签名
        $postStr = $this->http_input->getRawContent();
        file_put_contents('./renew_order.txt', $postStr);
        // $postStr = file_get_contents('renew_order.txt');

        $postObj = simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA);
        if ($postObj->result_code == 'SUCCESS' && $postObj->return_code == 'SUCCESS') {
            //判断锁
            $rentRenewOrderLock = $this->redis->hGet('rentRenewOrderLock', $postObj->out_trade_no);
            if (!empty($rentRenewOrderLock)) {
                $return['return_code'] = 'FAIL';
                $return['return_msg'] = 'error';
                $return = $this->arrayToXml($return);
                return $this->response->end($return);
            }
            $order_sn = $postObj->out_trade_no;
            $join = [
                ['contract c', 'c.contract_id=rq_renew_order.contract_id', 'left']
            ];
            $order = $this->renewOrderModel->getOne(['renew_order_no' => $order_sn], 'rq_renew_order.*,c.company_id', $join);
            if (empty($order)) {
                $return['return_code'] = 'FAIL';
                $return['return_msg'] = 'error';
                $return = $this->arrayToXml($return);
                return $this->response->end($return);
            }
            if ($order['pay_status'] == 2) {
                $return['return_code'] = 'SUCCESS';
                $return['return_msg'] = 'OK';
                $return = $this->arrayToXml($return);
                return $this->response->end($return);
            }
            if ($order['pay_mode'] == 2) {
                $wx_config = $this->service_payment_config;
            } else {
                $company_config = CompanyService::getCompanyConfig($order['company_id']);
                $wx_config = $company_config['engineer_wx_config'];
            }
            $wx_config['payment_type'] = 'JSAPI';
            //$WxPay = new WxPay($wx_config);
            $WxPay = WxPay::getInstance($wx_config);
            if ($WxPay->isSetConfig) $WxPay->setConfig($wx_config);
            $signObj = [];
            foreach ($postObj as $key => $val) {
                if ($key != 'sign') {
                    $signObj[$key] = $val;
                }
            }

            //验证签名
            if ($WxPay->getSign($signObj) == $postObj->sign) {
                $order['wx_callback_num'] = $postObj->transaction_id;
                $total_fee = $postObj->total_fee;
                // 获取用户信息
                $user_info = $this->user_model->getOne(array('user_id' => $order['user_id']), 'username,openid,telphone');
                // 获取合同信息
                $contract_info = $this->contract_model->getOne(['contract_id' => $order['contract_id']]);
                $status = false;
                $exire_date = $contract_info['exire_date'];
                $this->db->begin(function () use ($order, &$status, $postObj, $postStr, $contract_info, &$exire_date, $user_info, $total_fee) {
                    $this->redis->hSet('rentRenewOrderLock', $postObj->out_trade_no, json_encode($postStr));//加锁
                    $this->renew($order, $contract_info, $user_info, $total_fee);
                    $status = true;
                });
                $this->redis->hDel('rentRenewOrderLock', $postObj->out_trade_no);//解锁
                if ($status) {
                    //开始续费结算和发送模板消息
                    $this->sendRenewMsg($order, $contract_info, $user_info, $exire_date);
                    $return['return_code'] = 'SUCCESS';
                    $return['return_msg'] = 'OK';
                    $return = $WxPay->arrayToXml($return);
                    return $this->response->end($return);
                }
            }
        }
        $return['return_code'] = 'FAIL';
        $return['return_msg'] = 'error';
        $return = $WxPay->arrayToXml($return);
        return $this->response->end($return);
    }


    /**
     * @desc   续费金额为0时回调
     * @param order_sn string 必选 订单编号
     * @param order_money string 必选 金额
     * @param openid string 必选 工程openid
     * @date   2020-6-19
     * @return [type]     [description]
     * @author lcx
     */
    public function http_renewCallback()
    {
        //接收参数
        $param = json_decode($this->http_input->getRawContent(), true);
        $order_sn = $param['order_sn'];
        $total_fee = $param['order_money'];
        $openid = $param['openid'];
        $join = [
            ['contract c', 'c.contract_id=rq_renew_order.contract_id', 'left']
        ];
        //判断锁
        $renewCallbackLock = $this->redis->hGet('renewCallbackLock', $order_sn);
        if (!empty($renewCallbackLock)) {
            return false;
        }
        //获取续费订单信息
        $order = $this->renewOrderModel->getOne(['renew_order_no' => $order_sn], 'rq_renew_order.*,c.company_id', $join);

        //获取用户信息
        $user_info = $this->user_model->getOne(array('user_id' => $order['user_id']), 'username,openid,telphone');
        // 获取合同信息
        $contract_info = $this->contract_model->getOne(['contract_id' => $order['contract_id']]);
        $status = false;
        $exire_date = $contract_info['exire_date'];

        $this->db->begin(function () use ($order, &$status, $contract_info, &$exire_date, $user_info, $total_fee, $order_sn) {
            $this->redis->hSet('renewCallbackLock', $order_sn, json_encode($order));//加锁
            $this->renew($order, $contract_info, $user_info, $total_fee);
            $status = true;
        });
        $this->redis->hDel('renewCallbackLock', $order_sn);//解锁
        if ($status) {
            //开始续费结算和发送模板消息
            $this->sendRenewMsg($order, $contract_info, $user_info, $exire_date);
            return true;
        }
        return false;
    }

    //续费结算
    public function sendRenewMsg(array $order, array $contract_info, array $user_info, $exire_date)
    {
        $this->RenewalSettlement($contract_info['contract_no'], $order['renew_order_id']);
        $user_notice_config = ConfigService::getTemplateConfig('renew_success', 'user', $order['company_id']);
        if (!empty($user_notice_config) && !empty($user_notice_config['template']) && $user_notice_config['template']['switch']) {
            $template_id = $user_notice_config['template']['wx_template_id'];
            $mp_template_id = $user_notice_config['template']['mp_template_id'];
            // 模板信息
            $exire_date = date('Y-m-d H:i:s', $exire_date);
            $now = date('Y-m-d H:i:s', $order['create_time']);
            $package_mode = '时长模式';
            $package = $package_mode . '(' . $order['package_value'] . '天)';
            if ($order['package_mode'] == 2) {
                $package_mode = '流量模式';
                $package = $package_mode . '(' . $order['package_value'] . 'ML)';
            }
            // 模板内容
            $template_data = [
                'keyword1' => ['value' => $order['renew_order_no']], //订单号
                'keyword2' => ['value' => $order['package_cycle']], // 续费时长
                'keyword3' => ['value' => $package], // 续费类型
                'keyword4' => ['value' => $exire_date], //到期时间
                'keyword5' => ['value' => $now], //续费时间
                'keyword6' => ['value' => $order['order_actual_money']], // 金额
                'keyword7' => ['value' => '您的续费已完成。感谢您一直以来对我们的支持,我们永远都是维护您健康的坚实后盾,合同编号:' . $contract_info['contract_no']], // 备注
            ];
            $weapp_template_keyword = '';
            $mp_template_data = [
                'first' => ['value' => '恭喜!您已成功续费', 'color' => '#4e4747'],
                'keyword1' => ['value' => "合同续费(合同编号:" . $contract_info['contract_no'] . ")", 'color' => '#4e4747'],
                'keyword2' => ['value' => $exire_date, 'color' => '#4e4747'],
                'remark' => ['value' => '感谢您一直以来对我们的支持,我们永远都是维护您健康的坚实后盾!若有疑问可致电官方客服,客服电话:' . ConfigService::getConfig('company_hotline', false, $order['company_id']), 'color' => '#173177'],
            ];
            $template_tel = $user_info['telphone'];  // 电话
            $template_content = '您的续费的' . $package . '已成功，续费时长为' . $order['package_cycle'] . '，续费金额为' . $order['order_actual_money'] . '。打开微信进入' . '优净云' . '可查看详情,如有疑问请联系客服,客服电话:' . ConfigService::getConfig('company_hotline', false, $order['company_id']);
            NotifiyService::sendNotifiyAsync($user_info['openid'], $template_data, $template_id, $template_tel, $template_content, '', $weapp_template_keyword, $mp_template_id, $mp_template_data, 'user', $order['company_id']);

        }

    }

    //合同续费公共方法
    public function renew(array $order, array $contract_info, array $user_info, $total_fee)
    {
        // 1-- 修改订单状态
        $this->renewOrderModel->edit(['renew_order_id' => $order['renew_order_id']], ['status' => 3, 'pay_status' => 2, 'pay_time' => time(), 'renew_time' => time(), 'wx_callback_num' => $order['wx_callback_num']]);
        // 2-- 修改合同信息
        $exire_date = $contract_info['exire_date'] + ($order['package_cycle'] * 60 * 60 * 24);  // 到期时间延后
        $contract['exire_date'] = $exire_date;
        $contract['real_exire_date'] = $exire_date;
        if ($exire_date > time()) {
            $contract['status'] = 4;
        }
        $this->contract_model->save(['contract_id' => $order['contract_id']], $contract);
        //添加合同日志
        $this->addContractLog($order['contract_id'], 3, "【用户端】用户【" . $user_info['username'] . "】【用户ID:" . $order['user_id'] . "】工程端发起续费成功,修改合同到期时间为" . date('Y-m-d H:i:s', $exire_date), $order['user_id']);

        //查询合同主板信息
        $contract_ep_info = $this->contract_ep_model->getAll(array('contract_id' => $order['contract_id']), 'equipment_id');
        if (empty($contract_ep_info)) {
            //throw new SwooleException("该合同下无正常运行的主板,无法续费");
            return false;
        }
        //添加续费-主板关联表
        foreach ($contract_ep_info as $key => $value) {
            $eq_data = [];
            $eq_data['renew_order_id'] = $order['renew_order_id'];
            $eq_data['equipment_id'] = $value['equipment_id'];
            $eq_data['create_time'] = time();
            $this->renew_eq_model->add($eq_data);
        }

        //添加续费记录
        $record = [];
        $record['renew_order_id'] = $order['renew_order_id'];
        $record['user_id'] = $order['user_id'];
        $record['contract_id'] = $order['contract_id'];
        $record['remarks'] = '工程师傅发起-合同续费，微信支付';
        $record['renew_record_time'] = time();
        $this->renew_record_model->add($record);

        //优惠券
        if (!empty($order['coupon_id'])) {
            $coupon_id = explode(',', $order['coupon_id']);
            $update_coupon = [
                'use_time' => time(),
                'status' => 2,
                'order_total_sn' => $order['renew_order_no'],
                'use_type' => 2,
                'order_id' => $order['renew_order_id'],
            ];
            $this->Achievement->table = 'coupon';
            $this->Achievement->updateData(['coupon_id' => ['IN', $coupon_id]], $update_coupon);

            $tmp = [
                'coupon_id' => '',
                'use_type' => 1,
                'note' => '工程师傅发起用户合同续费[编号：' . $contract_info['contract_no'] . ']',
                'add_time' => time(),
            ];
            $this->Achievement->table = 'coupon_log';
            foreach ($coupon_id as $key => $value) {
                $tmp['coupon_id'] = $value;
                $this->Achievement->insertData($tmp);
            }
        }
//                    $payServiceFeeResult = HttpService::requestBossApi(['company_id' => $contract_info['company_id'], 'equipment_id' => array_column($contract_ep_info, 'equipment_id'), 'type' => 2, 'user_id' => $order['user_id'], 'contract_id' => $order['contract_id']], '/api/Equipment/multPayServiceFee');
//                    if ($payServiceFeeResult['code'] != 1000) {//扣费失败,直接抛出异常
//                        throw new SwooleException("扣费失败" . $payServiceFeeResult['msg']);
//                    }
        //续费主板--下发套餐--数据同步指令
        $renew_eq_map['renew_order_id'] = $order['renew_order_id'];
        $renew_eq_join = [
            ['equipment_lists as el', 'rq_renew_order_equipment.equipment_id = el.equipment_id', 'LEFT'],
            ['equipments as e', 'e.equipments_id = el.equipments_id', 'LEFT'],
        ];
        $renew_eq_data = $this->renew_eq_model->getAll($renew_eq_map, 'el.equipment_id,el.device_no,e.type,el.used_days,el.used_traffic,el.remaining_days,el.remaining_traffic', $renew_eq_join);
        if (!empty($renew_eq_data)) {
            foreach ($renew_eq_data as $k => $v) {
                //修改主板表租赁时间
                $this->equipment_lists_model->updateEquipmentLists(array('end_time' => $contract['exire_date']), array('equipment_id' => $v['equipment_id']));
                if ($v['type'] == 1) {//如果是智能主板
                    $exire_date = strtotime(getMonthLastDay($contract['exire_date']));   //获取签约月份最后一天
                    $data_sync_params['sn'] = $v['device_no'];
                    $data_sync_params['used_days'] = $v['used_days'];
                    $data_sync_params['used_traffic'] = $v['used_traffic'];
                    $data_sync_params['remaining_days'] = $v['remaining_days'];
                    $data_sync_params['remaining_traffic'] = $v['remaining_traffic'];
                    if ($order['package_mode'] == 2) {
                        $data_sync_params['remaining_traffic'] = $order['package_value'] + $v['remaining_traffic'];
                    } else if ($order['package_mode'] == 1) {
                        $data_sync_params['remaining_days'] = ceil(($exire_date - time()) / 86400);
                    }
                    $data_sync_path = '/House/Issue/data_sync';
                    HttpService::Thrash($data_sync_params, $data_sync_path);
                    sleepCoroutine(2000);
                    //请求心跳
                    $state_params['sn'] = $v['device_no'];
                    $state_path = '/House/Issue/heartbeat';
                    HttpService::Thrash($state_params, $state_path);
                } else {
                    continue;
                }
            }
        }
        //新增资金记录
        $finance_record['type'] = 3;
        $finance_record['renew_order_id'] = $order['renew_order_id'];
        $finance_record['user_id'] = $order['user_id'];
        $finance_record['money'] = $total_fee;
        $finance_record['create_time'] = time();
        $finance_record['is_online_pay'] = 1;
        $finance_record['pay_way'] = 1;
        $finance_record['callback_num'] = $order['wx_callback_num'];
        $finance_record['payment_method'] = 2;
        $finance_record['payment_uid'] = $order['user_id'];
        $finance_record['o_id'] = $contract_info['operation_id'];
        $finance_record['a_id'] = $contract_info['administrative_id'];
        $finance_record['company_id'] = $contract_info['company_id'];
        $this->finance_record_model->add($finance_record);

    }


    /**
     * @desc   添加合同操作日志
     * @param $status 1增2删3改4解绑'
     * @date   2018-07-19
     * @author lcx
     * @return [type]     [description]
     */
    public function addContractLog($contract_id, $status = 1, $remark, $user_id)
    {
        $data['contract_id'] = $contract_id;
        $data['do_id'] = $user_id;
        $data['do_type'] = $status;
        $data['terminal_type'] = 3;
        $data['do_time'] = time();
        $data['remark'] = $remark;
        $res = $this->contract_log_model->add($data);
        return $res;
    }


    /**
     * 续费结算
     * @param string $contract_no
     * @date 2019/1/25 10:41
     * @author ligang
     */
    public function RenewalSettlement(string $contract_no, $renew_order_id)
    {
        $data = [
            'contract_no' => $contract_no,
            'type' => 2,
            'from_renew_order_id' => $renew_order_id
        ];
        $json = json_encode($data);
        $response = $this->GetIPAddressHttpClient->httpClient
            ->setData($json)
            ->setMethod('post')
            ->coroutineExecute('/Achievement/Settlement/balance');
        $is_success = true;
        if ($response['statusCode'] == 200) {
            $body = json_decode($response['body'], 1);
            if ($body['code'] != 1000) {
                //结算失败
                $is_success = false;
            }
        } else {
            $is_success = false;
        }
        if (!$is_success) {
            //失败写入日志
            SLog::SL(SLog::CONTRACT_LOG, $contract_no)->info(__FUNCTION__, [$contract_no, $response]);
        }
    }

    function arrayToXml($arr)
    {
        $xml = "<xml>";
        foreach ($arr as $key => $val) {
            if (is_numeric($val)) {
                $xml .= "<" . $key . ">" . $val . "</" . $key . ">";
            } else {
                $xml .= "<" . $key . "><![CDATA[" . $val . "]]></" . $key . ">";
            }
        }
        $xml .= "</xml>";

        return $xml;
    }

    //扫码付补单
    public function http_scanPayAddOrder()
    {
        $this->parm = json_decode($this->http_input->getRawContent(), true);
        $order_sn = $this->parm['out_trade_no'];
        $order = $this->work_order_model->getWorkDetail(['scan_order_number' => $order_sn], array(), '*');
        if (empty($order)) {
            $return['return_code'] = 'FAIL';
            $return['return_msg'] = 'error';
            $return = $this->arrayToXml($return);
            return $this->response->end($return);
        }
        if ($order['pay_status'] == 2) {
            $return['return_code'] = 'SUCCESS';
            $return['return_msg'] = 'OK';
            $return = $this->arrayToXml($return);
            return $this->response->end($return);
        }
        $company_config = CompanyService::getCompanyConfig($order['company_id']);
        $wx_config = $company_config['mp_config'];
        $wx_config['payment_type'] = 'NATIVE';
        //$WxPay = new WxPay($wx_config);
        $WxPay = WxPay::getInstance($wx_config);
        if ($WxPay->isSetConfig) $WxPay->setConfig($wx_config);

        //验证签名
        $order['wx_callback_num'] = $this->parm['transaction_id'];
        $total_fee = $this->parm['total_fee'];
        $status = false;
        $this->db->begin(function () use ($order, &$data, &$status, $total_fee) {
            $this->successBack($order['work_order_id'], $order['wx_callback_num'], $total_fee);
            //   $this->getWorkorderRange($order['work_order_id']);
            //新增资金记录
            $finance_record['company_id'] = $order['company_id'];
            $finance_record['type'] = 2;
            $finance_record['work_order_id'] = $order['work_order_id'];
            $finance_record['user_id'] = $order['user_id'];
            $finance_record['money'] = $total_fee;
            $finance_record['create_time'] = time();
            $finance_record['is_online_pay'] = 1;
            $finance_record['pay_way'] = 1;
            $finance_record['callback_num'] = $order['wx_callback_num'];
            $finance_record['payment_method'] = 1;
            $finance_record['payment_uid'] = $order['user_id'];
            $finance_record['o_id'] = $order['operation_id'];
            $finance_record['a_id'] = $order['administrative_id'];
            $this->finance_record_model->add($finance_record);
            $status = true;
        }, function ($e) {
            $return['return_code'] = 'SUCCESS';
            $return['return_msg'] = $e;
            $this->response->header('Content-Type', 'text/html; charset=UTF-8');
            $output = json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            $this->response->end($output);
        });
        if ($status) {
            $return['return_code'] = 'SUCCESS';
            $return['return_msg'] = 'OK';
            $return = $WxPay->arrayToXml($return);
            return $this->response->end($return);
        }
        $return['return_code'] = 'FAIL';
        $return['return_msg'] = 'error';
        $return = $WxPay->arrayToXml($return);
        return $this->response->end($return);
    }

    //工程代付补单
    public function http_payCallbackAdd()
    {
        $this->parm = json_decode($this->http_input->getRawContent(), true);
        $order_sn = $this->parm['out_trade_no'];
        $order = $this->work_order_model->getWorkDetail(['order_number' => $order_sn], array(), '*');
        if (empty($order)) {
            $return['return_code'] = 'FAIL';
            $return['return_msg'] = 'error';
            $return = $this->arrayToXml($return);
            return $this->response->end($return);
        }
        if ($order['pay_status'] == 2) {
            $return['return_code'] = 'SUCCESS';
            $return['return_msg'] = 'OK';
            $return = $this->arrayToXml($return);
            return $this->response->end($return);
        }
        $company_config = ConfigService::getCompanyConfig($order['company_id']);
        $wx_config = $company_config['engineer_wx_config'];
        $wx_config['payment_type'] = 'JSAPI';
        //$WxPay = new WxPay($wx_config);
        $WxPay = WxPay::getInstance($wx_config);
        if ($WxPay->isSetConfig) $WxPay->setConfig($wx_config);
        //验证签名

        $order['wx_callback_num'] = $this->parm['transaction_id'];
        $total_fee = $this->parm['total_fee'];
        $status = false;
        $this->db->begin(function () use ($order, &$data, &$status, $total_fee) {
            $this->successBack($order['work_order_id'], $order['wx_callback_num'], $total_fee);
            $this->getWorkorderRange($order['work_order_id']);
            //新增资金记录
            $finance_record['company_id'] = $order['company_id'];
            $finance_record['type'] = 2;
            $finance_record['work_order_id'] = $order['work_order_id'];
            $finance_record['user_id'] = $order['user_id'];
            $finance_record['money'] = $total_fee;
            $finance_record['create_time'] = time();
            $finance_record['is_online_pay'] = 1;
            $finance_record['pay_way'] = 1;
            $finance_record['callback_num'] = $order['wx_callback_num'];
            $finance_record['payment_method'] = 2;
            $finance_record['payment_uid'] = $order['repair_id'];
            $finance_record['o_id'] = $order['operation_id'];
            $finance_record['a_id'] = $order['administrative_id'];
            $this->finance_record_model->add($finance_record);
            $status = true;
        });
        if ($status) {
            $return['return_code'] = 'SUCCESS';
            $return['return_msg'] = 'OK';
            $return = $WxPay->arrayToXml($return);
            return $this->response->end($return);
        }


        $return['return_code'] = 'FAIL';
        $return['return_msg'] = 'error';
        $return = $WxPay->arrayToXml($return);
        return $this->response->end($return);
    }

    //续费支付补单
    public function http_rentRenewOrderAdd()
    {

        //验证微信签名
        $this->parm = json_decode($this->http_input->getRawContent(), true);
        $order_sn = $this->parm['out_trade_no'];
        $join = [
            ['contract c', 'c.contract_id=rq_renew_order.contract_id', 'left']
        ];
        $order = $this->renewOrderModel->getOne(['renew_order_no' => $order_sn], 'rq_renew_order.*,c.company_id', $join);
        if (empty($order)) {
            $return['return_code'] = 'FAIL';
            $return['return_msg'] = 'error';
            $return = $this->arrayToXml($return);
            return $this->response->end($return);
        }
        if ($order['pay_status'] == 2) {
            $return['return_code'] = 'SUCCESS';
            $return['return_msg'] = 'OK';
            $return = $this->arrayToXml($return);
            return $this->response->end($return);
        }
        $company_config = CompanyService::getCompanyConfig($order['company_id']);
        $wx_config = $company_config['engineer_wx_config'];
        $wx_config['payment_type'] = 'JSAPI';
        //$WxPay = new WxPay($wx_config);
        $WxPay = WxPay::getInstance($wx_config);
        if ($WxPay->isSetConfig) $WxPay->setConfig($wx_config);
        $order['wx_callback_num'] = $this->parm['transaction_id'];
        $total_fee = $this->parm['total_fee'];
        // 获取用户信息
        $user_info = $this->user_model->getOne(array('user_id' => $order['user_id']), 'username,openid,telphone');
        // 获取合同信息
        $contract_info = $this->contract_model->getOne(['contract_id' => $order['contract_id']]);
        $status = false;
        $exire_date = $contract_info['exire_date'];
        $this->db->begin(function () use ($order, &$status, $contract_info, &$exire_date, $user_info, $total_fee) {
            // 1-- 修改订单状态
            $this->renewOrderModel->edit(['renew_order_id' => $order['renew_order_id']], ['status' => 3, 'pay_status' => 2, 'pay_time' => time(), 'renew_time' => time(), 'wx_callback_num' => $order['wx_callback_num']]);
            // 2-- 修改合同信息
            $exire_date = $contract_info['exire_date'] + ($order['package_cycle'] * 60 * 60 * 24);  // 到期时间延后
            $contract['exire_date'] = $exire_date;
            $contract['real_exire_date'] = $exire_date;
            if ($exire_date > time()) {
                $contract['status'] = 4;
            }
            $this->contract_model->save(['contract_id' => $order['contract_id']], $contract);
            //添加合同日志
            $this->addContractLog($order['contract_id'], 3, "【用户端】用户【" . $user_info['username'] . "】【用户ID:" . $order['user_id'] . "】工程端发起续费成功,修改合同到期时间为" . date('Y-m-d H:i:s', $exire_date), $order['user_id']);


            //查询合同主板信息
            $contract_ep_info = $this->contract_ep_model->getAll(array('contract_id' => $order['contract_id']), 'equipment_id');
            if (empty($contract_ep_info)) {
                throw new SwooleException("该合同下无正常运行的主板,无法续费");
            }
            //添加续费-主板关联表
            foreach ($contract_ep_info as $key => $value) {
                $eq_data = [];
                $eq_data['renew_order_id'] = $order['renew_order_id'];
                $eq_data['equipment_id'] = $value['equipment_id'];
                $eq_data['create_time'] = time();
                $this->renew_eq_model->add($eq_data);
            }

            //添加续费记录
            $record = [];
            $record['renew_order_id'] = $order['renew_order_id'];
            $record['user_id'] = $order['user_id'];
            $record['contract_id'] = $order['contract_id'];
            $record['remarks'] = '工程师傅发起-合同续费，微信支付';
            $record['renew_record_time'] = time();
            $this->renew_record_model->add($record);

            //优惠券
            if (!empty($order['coupon_id'])) {
                $coupon_id = explode(',', $order['coupon_id']);
                $update_coupon = [
                    'use_time' => time(),
                    'status' => 2,
                    'order_total_sn' => $order['renew_order_no'],
                    'use_type' => 2,
                    'order_id' => $order['renew_order_id'],
                ];
                $this->Achievement->table = 'coupon';
                $this->Achievement->updateData(['coupon_id' => ['IN', $coupon_id]], $update_coupon);

                $tmp = [
                    'coupon_id' => '',
                    'use_type' => 1,
                    'note' => '工程师傅发起用户合同续费[编号：' . $contract_info['contract_no'] . ']',
                    'add_time' => time(),
                ];
                $this->Achievement->table = 'coupon_log';
                foreach ($coupon_id as $key => $value) {
                    $tmp['coupon_id'] = $value;
                    $this->Achievement->insertData($tmp);
                }
            }
//                $payServiceFeeResult = HttpService::requestBossApi(['company_id' => $contract_info['company_id'], 'equipment_id' => array_column($contract_ep_info, 'equipment_id'), 'type' => 2, 'user_id' => $order['user_id'], 'contract_id' => $order['contract_id']], '/api/Equipment/multPayServiceFee');
//                if ($payServiceFeeResult['code'] != 1000) {//扣费失败,直接抛出异常
//                    throw new SwooleException("扣费失败" . $payServiceFeeResult['msg']);
//                }
            //续费主板--下发套餐--数据同步指令
            $renew_eq_map['renew_order_id'] = $order['renew_order_id'];
            $renew_eq_join = [
                ['equipment_lists as el', 'rq_renew_order_equipment.equipment_id = el.equipment_id', 'LEFT'],
                ['equipments as e', 'e.equipments_id = el.equipments_id', 'LEFT'],
            ];
            $renew_eq_data = $this->renew_eq_model->getAll($renew_eq_map, 'el.equipment_id,el.device_no,e.type,el.used_days,el.used_traffic,el.remaining_days,el.remaining_traffic', $renew_eq_join);
            if (!empty($renew_eq_data)) {
                foreach ($renew_eq_data as $k => $v) {
                    //修改主板表租赁时间
                    $this->equipment_lists_model->updateEquipmentLists(array('end_time' => $contract['exire_date']), array('equipment_id' => $v['equipment_id']));
                    if ($v['type'] == 1) {//如果是智能主板
                        $data_sync_params['sn'] = $v['device_no'];
                        $data_sync_params['used_days'] = $v['used_days'];
                        $data_sync_params['used_traffic'] = $v['used_traffic'];
                        $data_sync_params['remaining_days'] = $v['remaining_days'];
                        $data_sync_params['remaining_traffic'] = $v['remaining_traffic'];
                        if ($order['package_mode'] == 2) {
                            $data_sync_params['remaining_traffic'] = $order['package_value'] + $v['remaining_traffic'];
                        } else if ($order['package_mode'] == 1) {
                            $data_sync_params['remaining_days'] = $order['package_value'] + $v['remaining_days'];
                        }
                        $data_sync_path = '/House/Issue/data_sync';
                        HttpService::Thrash($data_sync_params, $data_sync_path);
                        sleepCoroutine(2000);
                        //请求心跳
                        $state_params['sn'] = $v['device_no'];
                        $state_path = '/House/Issue/heartbeat';
                        HttpService::Thrash($state_params, $state_path);
                    } else {
                        continue;
                    }
                }
            }
            //新增资金记录
            $finance_record['type'] = 3;
            $finance_record['renew_order_id'] = $order['renew_order_id'];
            $finance_record['user_id'] = $order['user_id'];
            $finance_record['money'] = $total_fee;
            $finance_record['create_time'] = time();
            $finance_record['is_online_pay'] = 1;
            $finance_record['pay_way'] = 1;
            $finance_record['callback_num'] = $order['wx_callback_num'];
            $finance_record['payment_method'] = 2;
            $finance_record['payment_uid'] = $order['user_id'];
            $finance_record['o_id'] = $contract_info['operation_id'];
            $finance_record['a_id'] = $contract_info['administrative_id'];
            $finance_record['company_id'] = $contract_info['company_id'];
            $this->finance_record_model->add($finance_record);


            $status = true;
        }, function ($e) {


        });
        if ($status) {
            // 模板信息
            $exire_date = date('Y-m-d H:i:s', $exire_date);
            $now = date('Y-m-d H:i:s', $order['create_time']);
            $package_mode = '时长模式';
            $package = $package_mode . '(' . $order['package_value'] . '天)';
            if ($order['package_mode'] == 2) {
                $package_mode = '流量模式';
                $package = $package_mode . '(' . $order['package_value'] . 'ML)';
            }

            // 模板内容
            $template_data = [
                'keyword1' => ['value' => $order['renew_order_no']], //订单号
                'keyword2' => ['value' => $order['package_cycle']], // 续费时长
                'keyword3' => ['value' => $package], // 续费类型
                'keyword4' => ['value' => $exire_date], //到期时间
                'keyword5' => ['value' => $now], //续费时间
                'keyword6' => ['value' => $order['order_actual_money']], // 金额
                'keyword7' => ['value' => '您的续费已完成。感谢您一直以来对我们的支持,我们永远都是维护您健康的坚实后盾,合同编号:' . $contract_info['contract_no']], // 备注
            ];
            //开始续费结算
            $this->RenewalSettlement($contract_info['contract_no'], $order['renew_order_id']);
            $return['return_code'] = 'SUCCESS';
            $return['return_msg'] = 'OK';
            $return = $WxPay->arrayToXml($return);
            return $this->response->end($return);
        }


        $return['return_code'] = 'FAIL';
        $return['return_msg'] = 'error';
        $return = $WxPay->arrayToXml($return);
        return $this->response->end($return);
    }


}
