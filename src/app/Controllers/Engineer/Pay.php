<?php

namespace app\Controllers\Engineer;

use app\Library\SLog;
use app\Services\Common\ConfigService;
use app\Services\Common\WxPayService;
use app\Services\Company\CompanyService;
use app\Services\Log\FileLogService;
use app\Wechat\WxPay;

/**
 * 工程人员支付
 */
class Pay extends Base
{

    protected $work_order_model;
    protected $engineers_model;
    protected $workEngineeringRelationshipModel;
    public function initialization($controller_name, $method_name)
    {
        parent::initialization($controller_name, $method_name);
        $this->work_order_model = $this->loader->model('WorkOrderModel', $this);
        $this->engineers_model = $this->loader->model('EngineersModel', $this);
        $this->workEngineeringRelationshipModel = $this->loader->model('WorkEngineeringRelationshipModel',$this);
    }

    /**
     * showdoc
     * @catalog API文档/工程端/支付相关
     * @title 扫码付
     * @description
     * @method POST
     * @url Engineer/Pay/scanPay
     * @param
     * @return {"code": 1000,"message": "支付成功","data":{}}
     * @return_param
     * @remark {"work_order_id":"1"}
     * @number 0
     * @author lcy
     * @date 2018-10-18
     */
    public function http_scanPay()
    {
        if (empty($this->parm['work_order_id'] ?? '')) {
            return $this->jsonend(-1001, "缺少参数ID");
        }
        $work_order_info = $this->work_order_model->getWorkDetail(array('work_order_id' => $this->parm['work_order_id']), array(), 'combo_money,work_order_id,order_number,pay_status,pay_client');
        if (empty($work_order_info)) {
            return $this->jsonend(-1101, "工单不存在");
        }
        if ($work_order_info['pay_status'] == 2) {
            return $this->jsonend(-1102, "请勿重复支付");
        }
        if ($work_order_info['pay_client'] != 1) {
            return $this->jsonend(-1103, "请选择正确的支付方式");
        }
        $company_config = CompanyService::getCompanyConfig($this->company);
        $wx_config = $company_config['mp_config'];
        $wx_config['ip'] = getServerIp();
        $add_status = false;
        $data = [];
        $err_code_des = '';
        $this->db->begin(function () use (&$add_status, &$wxorder, &$data, $wx_config, $work_order_info, &$err_code_des) {
            //修改订单编号--两种支付方式切换支付订单号会重复
            $order_no = $work_order_info['order_number'] . 'S';
            $edit_order_data['pay_mode'] = $this->pay_config['mode'] ?? 1;
            $edit_order_data['pay_mchid'] = $this->pay_config['mode'] == 2 ? $this->pay_config['sub_mch_id'] : $this->wx_config['mchid'];
            $edit_order_data['scan_order_number'] = $order_no;
            $this->work_order_model->editWork(['work_order_id' => $this->parm['work_order_id']], $edit_order_data);
            $pay_data['order_sn'] = $order_no;
            $pay_data['money'] = formatMoney($work_order_info['combo_money'],2);
            $pay_data['order_id'] = $work_order_info['work_order_id'];
            $notify_url = $this->config->get('callback_domain_name') . '/Engineer/Callback/scanPayCallback';
            if ($this->dev_mode == 1) {
                $pay_data['money'] = 0.01;
                $notify_url = $this->config->get('debug_config.callback_domain_name') . '/Engineer/Callback/scanPayCallback';
            }
            $code = WxPayService::nativePay($this->wx_config, $pay_data, $notify_url, $this->pay_config['sub_mch_id'] ?? '', $this->pay_config['mode'] ?? 1);

            if ($code['return_code'] == 'SUCCESS' && $code['return_msg'] == 'OK' && $code['result_code'] == 'SUCCESS') {
                $qr = $this->wx_qrcode($code['code_url'], $order_no, '');
                if ($qr) {
                    if ($this->dev_mode == 1) {
                        $data['url'] = $this->config->get('debug_config.static_resource_host') . 'pay_qrcode/' . '/' . $order_no . ".png";
                    } else {
                        $data['url'] = $this->config->get('static_resource_host') . 'pay_qrcode/' . '/' . $order_no . ".png";
                    }
                    $data['order_sn'] = $order_no;
                    $data['money'] = $pay_data['money'];
                    $add_status = true;
                }
            }

        }, function ($e) {
            FileLogService::WriteLog(SLog::ERROR_LOG, 'work_order', '扫码付生成二维码>>工单ID:' . $this->parm['work_order_id'] . ',错误原因:' . $e->error);
            return $this->jsonend(-1000, "支付失败" . $e->error);
        });
        if ($add_status) {
            return $this->jsonend(1000, "支付成功", $data);
        }
        FileLogService::WriteLog(SLog::ERROR_LOG, 'work_order', '扫码付生成二维码>>工单ID:' . $this->parm['work_order_id'] . ',错误原因:' . $err_code_des);
        return $this->jsonend(-1000, "支付失败" . $err_code_des);
    }

    /**
     * showdoc
     * @catalog API文档/工程端/支付相关
     * @title 工程人员代付
     * @description
     * @method POST
     * @url Engineer/Pay/pay
     * @param
     * @return {"code": 1000,"message": "操作成功","data":{}}
     * @return_param
     * @remark {"work_order_id":"1"}
     * @number 0
     * @author lcy
     * @date 2018-10-18
     */
    public function http_pay()
    {
        if (empty($this->parm['work_order_id'] ?? '')) {
            return $this->jsonend(-1001, "缺少参数ID");
        }
        $work_order_info = $this->work_order_model->getWorkDetail(array('work_order_id' => $this->parm['work_order_id']), array(), 'repair_id,combo_money,work_order_id,order_number,pay_status,pay_client');
        if (empty($work_order_info)) {
            return $this->jsonend(-1101, "工单不存在");
        }
        if ($work_order_info['pay_status'] == 2) {
            return $this->jsonend(-1102, "请勿重复支付");
        }
        if ($work_order_info['pay_client'] != 2) {
            return $this->jsonend(-1103, "请选择正确的支付方式");
        }
        //工程人员信息
        $engineers_data = $this->workEngineeringRelationshipModel->getAll(['work_order_id'=>$this->parm['work_order_id'],'state'=>1],'repair_id');
        $user_info = [];
        if (!empty($engineers_data)){
            foreach ($engineers_data as $key=>$value){
                if ($this->user_id == $value['repair_id']){
                    $user_info = $this->engineers_model->getOne(array('engineers_id' => $value['repair_id']), 'openid');
                }
            }
        }
        if (empty($user_info) || empty($user_info['openid'])) {
            return $this->jsonend(-1103, "用户信息错误");
        }
        $add_status = false;
        $jsapi = '';
        $this->db->begin(function () use (&$add_status, &$jsapi, $work_order_info, $user_info) {
            //调起微信支付
            //修改订单编号--两种支付方式切换支付订单号会重复
            // $order_no = CommonService::createSn('cloud_work_order_no');
            $edit_order_data['pay_mode'] = $this->pay_config['mode'] ?? 1;
            $edit_order_data['pay_mchid'] = $this->pay_config['mode'] == 2 ? $this->pay_config['sub_mch_id'] : $this->wx_config['mchid'];
            $this->work_order_model->editWork(['work_order_id' => $this->parm['work_order_id']], $edit_order_data);
            //调起支付
            $pay_data['order_sn'] = $work_order_info['order_number'];
            $pay_data['money'] = formatMoney($work_order_info['combo_money'],2);
            $notify_url = $this->config->get('callback_domain_name') . '/Engineer/Callback/payCallback';
            if ($this->dev_mode == 1) {
                $pay_data['money'] = 0.01;
                $notify_url = $this->config->get('debug_config.callback_domain_name') . '/Engineer/Callback/payCallback';
            }
            $jsapi = WxPayService::pay($this->wx_config, $user_info['openid'], $pay_data, $notify_url, $this->pay_config['sub_mch_id'] ?? '', $this->pay_config['mode'] ?? 1);
            $add_status = true;
        });
        if ($jsapi) {
            $order_data['work_order_id'] = $this->parm['work_order_id'];
            $order_data['jsapi'] = $jsapi;
            return $this->jsonend(1000, '操作成功', $order_data);
        }
        return $this->jsonend(-1000, '操作失败');
    }


    /**
     * showdoc
     * @catalog API文档/工程端/支付相关
     * @title 查询微信支付状态queryPayStatus
     * @description
     * @method POST
     * @url Engineer/Pay/queryPayStatus
     * @param
     * @return {"code": 1000,"message": "支付成功"}
     * @return_param
     * @remark {"work_order_number":"1246547"}
     * @number 0
     * @author lcy
     * @date 2018-10-18
     */
    public function http_queryPayStatus()
    {
        if (empty($this->parm['work_order_number'] ?? '')) {
            return $this->jsonend(-1001, "缺少参数工单号");
        }
        $order_sn = $this->parm['work_order_number'];
        //微信支付
        $this->wx_config['ip'] = getServerIp();
        $result = WxPayService::queryPayStatus($this->wx_config, $order_sn, $this->pay_config['sub_mch_id'] ?? '',$this->pay_config['mode'] ?? 1);
        return $this->jsonend($result['code'], $result['msg']);
//        //$wxpay = new WxPay($wx_config);
//        $wxpay = WxPay::getInstance($wx_config);
//        if ($wxpay->isSetConfig) $wxpay->setConfig($wx_config);
//        $wxorder['appid'] = $wx_config['appId'];
//        $wxorder['mch_id'] = $wx_config['mchid'];
//        $wxorder['out_trade_no'] = $order_sn;
//        $wxorder['nonce_str'] = $wxpay->createNoncestr();
//        $wxorder['sign_type'] = 'MD5';
//        $wxorder['sign'] = $wxpay->getSign($wxorder);
//        $xml = $wxpay->arrayToXml($wxorder);
//        $code = $wxpay->postXmlCurl($xml, 'https://api.mch.weixin.qq.com/pay/orderquery');
//        $code = $wxpay->xmlToArray($code);
//        if ($code['return_code'] == 'SUCCESS' && $code['return_msg'] == 'OK' && isset($code['transaction_id']) && $code['trade_state_desc'] == '支付成功') {
//            return $this->jsonend(1000, "支付成功");
//        }
//        return $this->jsonend(-1000, "未支付");
    }

    /**
     * 扫码支付 生成二维码
     * @param type $wx_code
     * @param type $order_sn
     * @return boolean
     * @author ycw
     */
    public function wx_qrcode($wx_code, $order_sn, $dir)
    {
        //$qrCode = new QrCode($wx_code);
        $qrCode = get_instance()->QrCode;
        $qrCode->setText($wx_code);
        // $this->response->header('Content-Type: ',$qrCode->getContentType());
        $qrCode->writeString();
        $url = WWW_DIR . '/pay_qrcode/'.$dir . '/' . $order_sn . '.png';
        if (!file_exists(WWW_DIR . '/pay_qrcode/' . '/')){
            mkdir(WWW_DIR . '/pay_qrcode/', 0777);
        }
        $qrCode->writeFile($url);
        // $this->http_output->endFile(WWW_DIR.'/pay_qrcode/'.$dir,$order_sn.'.png'); //直接返回图片
        if (file_exists($url)) {
            return true;
        }
        return false;
    }

}
