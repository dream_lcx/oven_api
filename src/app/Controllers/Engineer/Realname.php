<?php

namespace app\Controllers\Engineer;

use app\Library\SLog;
use app\Services\Log\FileLogService;
use app\Tasks\AppTask;
use app\Services\Common\ConfigService;

/**
 * 用户实名认证
 * 2019-03-04
 */
class Realname extends Base {

    protected $user_model;
    protected $auth_model;
    protected $face_client;
    protected $aip_face;
    protected $auth_api = 2; //调用接口 1腾讯 2百度
    protected $aip_ocr;
    protected $idcard_model;
    protected $order_model;
    protected $work_model;
    protected $system_config;
    protected $customer_administrative_center_model;

    public function __construct() {
        parent::__construct();

        //腾讯
        if ($this->auth_api == 1) {
            require_once APP_DIR . '/Library/face/index.php';
            $face_config = $this->config['face'];
            $this->face_client = new CIClient($face_config['appid'], $face_config['secretId'], $face_config['secretKey'], $face_config['bucket']);
            $this->face_client->setTimeout(120);
        } else {
            //百度
            $baidu_face_config = $this->config->get('baidu_face');
            $appId = $baidu_face_config['appId'];
            $apiKey = $baidu_face_config['apiKey'];
            $secretKey = $baidu_face_config['secretKey'];
            require_once APP_DIR . '/Library/BaiduApi/AipFace.php';
            require_once APP_DIR . '/Library/BaiduApi/AipOcr.php';
            $this->aip_face = new \AipFace($appId, $apiKey, $secretKey);
            $this->aip_ocr = new \AipOcr($appId, $apiKey, $secretKey);
        }

    }

    public function initialization($controller_name, $method_name) {
        parent::initialization($controller_name, $method_name);
        $this->user_model = $this->loader->model('CustomerModel', $this);
        $this->auth_model = $this->loader->model('CustomerAuthModel', $this);
        $this->idcard_model = $this->loader->model('ContractIdcardModel', $this);
        $this->order_model = $this->loader->model('OrderModel', $this);
        $this->work_model = $this->loader->model('WorkOrderModel', $this);
        $this->customer_administrative_center_model = $this->loader->model('CustomerAdministrativeCenterModel', $this);
        $this->system_config = ConfigService::getConfig('user_auth_check',false,$this->company);
        $this->accessKey = $this->config->get('qiniu.accessKey');
        $this->secrectKey = $this->config->get('qiniu.secrectKey');
        $this->bucket = $this->config->get('qiniu.bucket');
    }

    /**
     * showdoc
     * @catalog API文档/工程端/实名认证相关
     * @title 实名认证-第一步保存身份证号姓名
     * @description
     * @method POST
     * @url Engineer/Realname/step1
     * @param user_id 可选 string 用户ID
     * @param auth_type 必选 string 认证类型1个人2企业3政事
     * @param realname 必选 string 真实名称
     * @param id_number 必选 string 身份证号
     * @param legal_person 必选 string 法人姓名
     * @param license_number 必选 string 营业执照号
     * @param contact 可选 string 联系人
     * @param tellphone 可选 string 联系电话
     * @param company_name 必选 string 单位名称
     * @param company_address 必选 string 单位地址
     * @param auth_client 可选 int 认证终端 1用户 2维修端
     * @return {"code":-1101,"message":"你的认证申请已通过,不需要重复提交申请!","data":""}
     * @return
     * @remark
     * @number 0
     * @author lcx
     * @date 2019-03-04
     */
    public function http_step1() {
        $user_id = $this->parm['user_id'] ?? $this->user_id;
        $auth_type = trim($this->parm['auth_type']?? 1);
        $id_number = trim($this->parm['id_number'] ?? '');
        $name = trim($this->parm['realname'] ?? '');
        $license_number = trim($this->parm['license_number'] ?? '');
        $legal_person = trim($this->parm['legal_person'] ?? '');
        $contact=trim($this->parm['contact'] ?? '');
        $tellphone=trim($this->parm['tellphone'] ?? '');
        $company_name=trim($this->parm['company_name'] ?? '');
        $company_address = trim($this->parm['company_address'] ?? '');
        $comidcard=trim($this->parm['comidcard'] ?? '');
        $comname=trim($this->parm['comname'] ?? '');
        $comaddress=trim($this->parm['comaddress'] ?? '');

        //用户认证
        if($auth_type==1){
            if (empty($id_number) || empty($name)) {
                return $this->jsonend(-1001, "请输入姓名和身份证号");
            }
            if(!isRealName($name)){
                return $this->jsonend(-1001, "请输入正确的用户姓名");
            }
            if(!checkIdCard($id_number)){
                return $this->jsonend(-1001, "请输入正确的身份证号码");
            }
            $where['rq_customer_auth.id_number']=$id_number;
        }
        //企业认证
        if($auth_type==2){
            if (empty($license_number) || empty($legal_person)) {
                return $this->jsonend(-1001, "请输入法人代表和统一社会信用代码");
            }
            if(!isRealName($legal_person)){
                return $this->jsonend(-1001, "请输入正确的法人代表");
            }
//            if(!preg_match('/\d{15}/',$license_number)){
//                return $this->jsonend(-1001, "请输入正确的统一社会信用代码");
//            }
            if(empty($comidcard)){
                return $this->jsonend(-1001, "请输入法人身份证号码");
            }
            if(!checkIdCard($comidcard)){
                return $this->jsonend(-1001, "请输入正确的身份证号码");
            }
            if (empty($comname) || empty($comaddress)) {
                return $this->jsonend(-1001, "请输入单位名称和单位地址");
            }
            if(!isMobile($tellphone)){
                return $this->jsonend(-1001, "请输入正确的联系电话");
            }
            $where['business_license_number']=$license_number;
        }

        //政事认证
        if($auth_type==3){

            if (empty($company_name) || empty($company_address)) {
                return $this->jsonend(-1001, "请输入单位名称和单位地址");
            }
            if(!isRealName($company_name)){
                return $this->jsonend(-1001, "请输入正确的单位名称");
            }
            if(!isMobile($tellphone)){
                return $this->jsonend(-1001, "请输入正确的联系电话");
            }
            $where['company_name']=$company_name;
        }
        $where['user_belong_to_company']=$this->company;
        //查询认证状态
        $info = $this->auth_model->getOne(['user_id' => $user_id], 'status,auth_id,step1_time');
        if (!empty($info) && $info['status'] == 2) {
            return $this->jsonend(-1001, "您已经实名认证过");
        }
        if (!empty($info) && $info['status'] == 1) {
            return $this->jsonend(-1001, "您已经提交实名认证,请等待审核");
        }
        if (!empty($info) && !empty($info['step1_time']) && $info['status'] !=3) {
            return $this->jsonend(1001, "证件和姓名对比已通过,请继续下一步认证");
        }
        //判断身份证唯一性
        $join = [
            ['customer c','c.user_id=rq_customer_auth.user_id','left']
        ];
        $only = $this->auth_model->getOne($where,'auth_id,rq_customer_auth.user_id',$join);

        if (!empty($only) && $only['user_id'] != $user_id) {
            return $this->jsonend(1001, "该证件或单位已认证过,不能重复认证");
        }
        $customer_administrative_center = $this->customer_administrative_center_model->getOne(['user_id'=>$user_id],'operation_id');
        $operation_id = empty($customer_administrative_center)?0:$customer_administrative_center['operation_id'];
        $this->system_config = ConfigService::getConfig('user_auth_check',false,$this->company,$operation_id);
        if ($this->auth_api == 1) {//腾讯接口-暂未开通
            return $this->jsonend(-1000, "接口调用失败");
        } else {//百度接口

            //如果用户认证实名认证方式非后台审核，调用第三方接口
            if ($this->system_config != 1 && $auth_type==1) {
                $result = $this->baidu_idmatch($id_number, $name); //调百度身份证与名字对比接口
                if($result['error_code']==222022){
                    return $this->jsonend(-1000, '姓名和身份证不匹配');
                }
                if ($result['error_code'] != 0) {
                    return $this->jsonend(-1000, $result['msg']);
                }
            }
            if($auth_type==1){
                $auth['status'] = -2;//待上传身份证
            }else{
                $auth['status'] = -4;//待上传营业执照/附件
            }

            //写入数据库
            $auth['user_id'] = $user_id;
            $auth['auth_type'] = $this->parm['auth_type'] ?? 1;
            $auth['auth_client'] = $this->parm['auth_client'] ?? 2;
            $auth['create_time'] = time();
            $auth['step1_time'] = time();
            $auth['check_type'] = $this->system_config;
            $auth['realname'] = $name?$name:($legal_person??'');
            $auth['id_number'] = $id_number?$id_number:($comidcard??'');
            $auth['business_license_number']=$license_number??'';
            $auth['contact'] = $contact??'';
            $auth['tellphone'] = $tellphone??'';
            $auth['company_name']=$company_name?$company_name:($comname??'');
            $auth['company_address']=$company_address?$company_address:($comaddress??'');

            if (empty($info)) {
                $res = $this->auth_model->add($auth);
            } else {
                $res = $this->auth_model->save(array('auth_id' => $info['auth_id']), $auth);
            }
            if ($res) {
                return $this->jsonend(1000, "提交成功", []);
            } else {
                return $this->jsonend(-1000, "提交失败", []);
            }
        }
    }

    //百度API-营业执照识别
    public function baidu_license($license)
    {
        $arr['license'] = $this->aip_ocr->businessLicense($license,[]);
        return $arr;
    }

    /**
     * showdoc
     * @catalog API文档/工程端/实名认证相关
     * @title 实名认证-第二步
     * @description
     * @method POST
     * @url Engineer/Realname/step2
     * @param auth_type 必选 string 认证类型1个人2企业3政事
     * @param front_idcard_pic 必选 string 身份证正面
     * @param back_idcard_pic 必选 string 身份证反面
     * @param businesspic 必选 string 营业执照/单位附件
     * @return
     * @remark
     * @number 0
     * @author lcx
     * @date 2019-03-04
     */
    public function http_step2() {
        $user_id = $this->parm['user_id'] ?? $this->user_id;
        $auth_type=trim($this->parm['auth_type']?? 1);
        //获取配置
        $customer_administrative_center = $this->customer_administrative_center_model->getOne(['user_id'=>$user_id],'operation_id');
        $operation_id = empty($customer_administrative_center)?0:$customer_administrative_center['operation_id'];
        $this->system_config = ConfigService::getConfig('user_auth_check',false,$this->company,$operation_id);
        //用户认证
        if($auth_type==1){
            $front = $this->parm['front_idcard_pic'] ?? '';
            $back = $this->parm['back_idcard_pic'] ?? '';
            //全路径
            $front_all = $this->config->get('qiniu.qiniu_url') . $front;
            $back_all = $this->config->get('qiniu.qiniu_url') . $back;
            // $back_all = 'https://qn.youheone.com/201903051031511840_wx2845e1b4ffe09c17.o6zAJsy8NsukYGXcCKMpaisJ81Nc.PmIRkj7uWjSC67d9ad2796035962e66519be6c34acdb.jpg';
            // $front_all = 'https://qn.youheone.com/201903051015451382_wx2845e1b4ffe09c17.o6zAJsy8NsukYGXcCKMpaisJ81Nc.VYmSkW8VhG1ob16c1563ded39163968640f3ecf813d4.jpg';
            if (empty($front)) {
                return $this->jsonend(-1002, "请上传身份证正面");
            }
            if (empty($back)) {
                return $this->jsonend(-1002, "请上传身份证反面");
            }
        }else{
            //企业认证/政事认证
            $license_pic=$this->parm['businesspic']??'';
            $license_all = $this->config->get('qiniu.qiniu_url') . $license_pic;
            if(empty($license_all)){
                return $this->jsonend(-1002, "请上传营业执照/单位附件");
            }

        }

        //查询用户认证信息
        $info = $this->auth_model->getOne(['user_id' => $user_id], 'realname,id_number,business_license_number,company_name,auth_id,status,step2_time');
        if (empty($info)) {
            return $this->jsonend(-1101, "请先提交个人信息资料");
        }
        if (!empty($info) && $this->system_config == 2 && $info['status'] == -3) {//用户认证
            return $this->jsonend(1001, "身份证信息已通过审核,请继续下一步认证");
        }
        if (!empty($info) && $info['status'] == 2) {
            return $this->jsonend(-1102, "审核已通过");
        }
        if (!empty($info) && $info['status'] != -2 && $info['status'] != -4) {
            return $this->jsonend(-1001, "您已提交过身份信息,请返回上一页重新进入");
        }
        $record = [];
        //如果是人脸识别和证件验证，走第三方接口
        if ($this->system_config != 1) {
            if($auth_type==1){
                //用户认证
                $front_base = file_get_contents($front_all);
                $back_base = (file_get_contents($back_all));
                $result = $this->baidu_idcard($front_base, $back_base);
                if (!empty($result['front']['err_code'] ?? '')) {
                    return $this->jsonend(-1101, $result['front']['err_msg']);
                }
                if (!empty($result['back']['err_code'] ?? '')) {
                    return $this->jsonend(-1102, $result['back']['err_msg']);
                }
                //判断姓名身份证号与身份证是否一致
                if ($result['front']['words_result']['姓名']['words'] != $info['realname']) {
                    return $this->jsonend(-1103, "您提交的姓名与身份证信息不一致");
                }
                if ($result['front']['words_result']['公民身份号码']['words'] != $info['id_number']) {
                    return $this->jsonend(-1103, "您提交的身份证号与身份证信息不一致");
                }
                //保存身份证信息
                $record['user_id'] = $user_id;
                $record['name'] = $result['front']['words_result']['姓名']['words'];
                $record['idcard_number'] = $result['front']['words_result']['公民身份号码']['words'];
                $record['address'] = $result['front']['words_result']['住址']['words'];
                $record['birthday'] = $result['front']['words_result']['出生']['words'];
                $record['sex'] = $result['front']['words_result']['性别']['words'];
                $record['nation'] = $result['front']['words_result']['民族']['words'];
                $record['sign_date'] = $result['back']['words_result']['签发日期']['words'];
                $record['sign_office'] = $result['back']['words_result']['签发机关']['words'];
                $record['invalid_date'] = $result['back']['words_result']['失效日期']['words'];
                $record['create_time'] = time();
            }else if($auth_type==2){
                //企业认证
                $license_all = file_get_contents($license_all);//baidu_license
                $result = $this->baidu_license($license_all);

                if (!empty($result['license']['err_code'] ?? '')) {
                    return $this->jsonend(-1101, $result['license']['err_msg']);
                }
                //判断法人姓名与营业执照是否一致
                if ($result['license']['words_result']['法人']['words'] != $info['realname']) {
                    return $this->jsonend(-1103, "您提交的法人与营业执照信息不一致");
                }

                //保存身份证信息
                $record['user_id'] = $user_id;
                $record['company'] = $result['license']['words_result']['注册资本']['words'];
                $record['credit_code'] = $result['license']['words_result']['社会信用代码']['words'];
                $record['address'] = $result['license']['words_result']['单位名称']['words'];
                $record['birthday'] = $result['license']['words_result']['法人']['words'];
                $record['sex'] = $result['license']['words_result']['证件编号']['words'];
                $record['nation'] = $result['license']['words_result']['组成形式']['words'];
                $record['sign_date'] = $result['license']['words_result']['成立日期']['words'];
                $record['sign_office'] = $result['license']['words_result']['地址']['words'];
                $record['invalid_date'] = $result['license']['words_result']['经营范围']['words'];
                $record['invalid_date'] = $result['license']['words_result']['类型']['words'];
                $record['invalid_date'] = $result['license']['words_result']['有效期']['words'];
                $record['create_time'] = time();

            }else{
                //政事审核（其他审核方式只走后台审核）
                $edit['status'] = 1;
            }

        }
        //修改认证信息
        if($auth_type==1){
            $edit['front_idcard_pic'] = $front;
            $edit['back_idcard_pic'] = $back;
        }else{
            $edit['business_license_pic'] = $license_pic;
        }
        $edit['check_type'] = $this->system_config;
        $edit['step2_time'] = time();
        $edit_user_data = [];
        //如果是后台审核，将审核状态直接修改为待审核
        if ($this->system_config == 1) {
            $edit['status'] = 1;
        } else if ($this->system_config == 4) {//如果是身份证验证,将审核状态直接修改为审核通过
            if($auth_type ==3){
                $edit['status'] = 1;//政审为待审核
            }else{
                $edit['status'] = 2;//用户和企业为审核通过
                $edit_user_data['user_class'] = $auth_type ==1?1:2;
            }
            $edit['check_time'] = time();
            if($auth_type==1){
                //修改用户信息
                $cardInfo = getIdCardInfo($info['id_number']);
                if (!empty($cardInfo)) {
                    $edit_user_data['gender'] = $cardInfo['sex'];
                    $edit_user_data['birthday'] = $cardInfo['birth_time'];
                }
                $edit_user_data['id_card'] = $info['id_number'];
            }
            $edit_user_data['is_auth'] = 2;
            $edit_user_data['realname'] = $info['realname'];
        } else if ($this->system_config == 2) {//如果是人脸识别,将审核状态直接修改为待活体检测
            if($auth_type==1){
                $edit['status'] = -3;
            }else if($auth_type==2){//企业为审核通过
                $edit['status'] = 2;
                $edit_user_data['user_class'] =2;//企业用户
            }
            $edit['step2_time'] = time();
        }
        $add_status = false;
        $this->db->begin(function () use ( &$add_status, $info, $edit, $record, $edit_user_data, $user_id,$auth_type) {
            $this->auth_model->save(['auth_id' => $info['auth_id']], $edit);
            if ($this->system_config != 1 && $auth_type==1) {
                $this->idcard_model->add($record);
            }
            //如果是身份证修改用户信息
            if ($this->system_config == 4) {
                $this->user_model->save(array('user_id' => $user_id), $edit_user_data);
            }
            $add_status = true;
        });
        //如果是后台审核、政事认证任何验证都为后台审核,提示审核中
        if ($add_status && $this->system_config == 1 ||($add_status && $this->system_config == 2 && $auth_type==3) ||($add_status && $this->system_config == 4 && $auth_type==3)) {
            return $this->jsonend(1100, '申请已提交,请耐心等待审核');
        }
        //如果是用户、企业证件验证,提示审核通过
        if ($add_status && $this->system_config == 4 && $auth_type !=3) {
            return $this->jsonend(1200, '证件审核通过');
        }
        //如果是企业人脸验证认证为审核通过
        if ($add_status && $this->system_config == 2 && $auth_type ==2) {
            return $this->jsonend(1100, '审核通过');
        }
        //如果是用户人脸识别,进入下一步
        if ($add_status && $this->system_config == 2 && $auth_type==1) {
            return $this->jsonend(1000, '验证成功!正在跳转至下一步~~');
        }
        return $this->jsonend(-1000, '身份证审核失败');
    }

    /**
     * showdoc
     * @catalog API文档/工程端/实名认证相关
     * @title 实名认证第三步-人脸核身
     * @description
     * @method POST
     * @url Engineer/Realname/step3
     * @param code 必选 string 验证码
     * @param file 必选 string 视频文件
     * @param id_card 可选 string 身份证号
     * @param name 可选 string 用户真实姓名
     * @return
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_step3() {
        $this->parm = $this->http_input->getAllPostGet();
        $read_num = false;
        if ($read_num) {
            if (empty($this->parm['code'] ?? '')) {
                return $this->jsonend(-1000, 'code 参数错误');
            }
        }
        if (empty($this->request->files['file'] ?? '')) {
            return $this->jsonend(-1000, '请上传视频');
        }
        $file = $this->request->files['file'];
        $user_id = $this->parm['user_id'] ?? $this->user_id;
        $info = $this->auth_model->getOne(['user_id' => $user_id], 'realname,id_number,auth_id,status,step3_time,step3_num');
        //获取配置
        $customer_administrative_center = $this->customer_administrative_center_model->getOne(['user_id'=>$user_id],'operation_id');
        $operation_id = empty($customer_administrative_center)?0:$customer_administrative_center['operation_id'];
        $this->system_config = ConfigService::getConfig('user_auth_check',false,$this->company,$operation_id);
        if ($this->auth_api == 1) {
            return false;
        } else {
            $session_id = $this->parm['session_id'] ?? '';
            $result = $this->baidu_face($info['realname'], $info['id_number'], '', $file, $session_id);
            if ($result['code'] != 1000) {
                $edit_data['step3_time'] = time();
                $edit_data['step3_num'] = $info['step3_num'] + 1; //增加活体检测次数
                //如果第3次活体检测未通过，直接转为后台审核
                if ($info['step3_num'] >= 2) {
                    $edit_data['status'] = 1;
                    $edit_data['check_type'] = 1;
                }
                $res = $this->auth_model->save(['auth_id' => $info['auth_id']], $edit_data);
                if ($res && $info['step3_num'] >= 2) {
                    return $this->jsonend(-1110, $result['msg'] . ',由于您多次活体检测未通过,系统已自动转为后台审核,请耐心等待审核结果');
                }
                return $this->jsonend($result['code'], $result['msg']);
            }
            //修改认证信息
            $edit['status'] = 2;
            $edit['step3_time'] = time();
            $edit['step3_num'] = $info['step3_num'] + 1; //增加活体检测次数
            $edit['face_video'] = $result['data']['face_video'];
            $edit['face_img'] = $result['data']['face_img'];
            $edit['check_time'] = time();
            //修改用户信息
            $cardInfo = getIdCardInfo($info['id_number']);
            if(!empty($cardInfo)){
                $edit_user_data['gender'] = $cardInfo['sex'];
                $edit_user_data['birthday'] = $cardInfo['birth_time'];
            }
            $edit_user_data['is_auth'] = 2;
            $edit_user_data['realname'] = $info['realname'];
            $edit_user_data['id_card'] = $info['id_number'];
            $edit_user_data['user_class'] = 1;//个人用户
            $add_status = false;
            $this->db->begin(function () use ( &$add_status, $user_id, $info, $edit, $edit_user_data) {
                $this->auth_model->save(['auth_id' => $info['auth_id']], $edit);
                $this->user_model->save(array('user_id' => $user_id), $edit_user_data);
                $add_status = true;
            });
            if ($add_status) {
                return $this->jsonend(1000, '活体检测通过,认证成功');
            }
            return $this->jsonend(-1000, '活体检测未通过');
        }
    }

    /**
     * showdoc
     * @catalog API文档/公共API/实名认证相关
     * @title 获取唇语验证字符串
     * @description
     * @method POST
     * @url Common/Face/livegetfour
     * @return
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_livegetfour() {
        if ($this->auth_api == 1) {
            return $this->tencent_live_get_four();
        } else {
            return $this->baidu_live_get_four();
        }
    }

    /*	 * ****************百度人脸识别开始*********************** */

    //获取唇语验证字符串
    public function baidu_live_get_four() {
        $is_check = false; //是否进行语音验证
        if (!$is_check) {
            $code = (string) rand(2222, 9999);
            return $this->jsonend(1000, 'ok', ['code' => $code, 'session_id' => '']);
        } else {
            //获取语音验证码
            $code_res = $this->aip_face->videoSessioncode();
            $code = [];
            if ($code_res['err_no'] == 0) {
                $code = $code_res['result'];
                return $this->jsonend(1000, 'ok', $code);
            } else {
                return $this->jsonend(-1000, '获取唇语字符串失败');
            }
        }
    }

    //人脸核身
    public function baidu_face($name, $id_card, $code, $file, $session_id) {

//        //上传到七牛云
//        // 构建鉴权对象 #需要填写你的 Access Key 和 Secret Key
//        $auth = new Auth($this->accessKey, $this->secrectKey);
//        // 生成上传 Token
//        $token = $auth->uploadToken($this->bucket);
//
//        $uploadMgr = new UploadManager();
//        if (empty($file)) {
//            $result['code'] = -1001;
//            $result['msg'] = '请选择上传视频';
//            return $result;
//        }
//        $imgname = date('YmdHis') . rand(0, 9999) . '_' . $file['name']; //上传后文件名称
//        list($ret, $err) = $uploadMgr->putFile($token, $imgname, $file['tmp_name']);
//        if ($err !== null) {
//            $result['code'] = -1000;
//            $result['msg'] = '视频上传失败';
//            return $result;
//        }
        //上传到本地服务器
        $imgname = date('YmdHis') . $id_card . time() . '_' . rand(0, 9999) . substr($file['name'], strrpos($file['name'], '.')); //上传后文件名称
        $tmp = $file['tmp_name'];
        $filepath = WWW_DIR . '/face/video/';
        file_exists(WWW_DIR . '/face/') || mkdir(WWW_DIR . '/face/', 0777);
        file_exists($filepath) || mkdir($filepath, 0777);
        file_exists(WWW_DIR . '/face/faceImg/') || mkdir(WWW_DIR . '/face/faceImg/', 0777);
        if (!move_uploaded_file($tmp, $filepath . $imgname)) {
            return $this->jsonend(-1000, '上传视频失败');
        }
        //$url = $this->config->get('qiniu.qiniu_url') . $key;
        $url = $filepath . $imgname;
        $videoBase64 = base64_encode(file_get_contents($url));
        $res = $this->aip_face->videoFaceliveness($session_id, $videoBase64);
        $msg = $this->config->get('baidu_face_error_code')[$res['err_no']] ?? '验证失败,请重试';
        //unset($res['result']['pic_list']);
        if ($res['err_no'] == 0) {
            if ($res['result']['score'] < $res['result']['thresholds']['frr_1e-3']) {
                $result['code'] = -1000;
                $result['msg'] = '活体检测失败';
                return $result;
            }
            $score = $res['result']['score'] ?? '';
            $result = $this->aip_face->personVerify($res['result']['pic_list'][0]['pic'], 'BASE64', $id_card, $name);
            if ($result['error_code'] != 0) {
                $msg2 = $this->config->get('baidu_gongan')[$result['error_code']] ?? '身份验证失败,请重试';
                $result['code'] = -1000;
                $result['msg'] = $msg2;
                return $result;
            }
            if ($result['result']['score'] < 80) {
                $result['code'] = -1000;
                $result['msg'] = '公安身份校验不通过';
                return $result;
            }
            //异步上传视频资源到七牛云
            $AppTask = $this->loader->task(AppTask::class, $this);
            $AppTask->startTask('sysnUploadToQiniu', [file_get_contents($url), 'face/video/' . $imgname], -1, function ($serv, $task_id, $data) {

            });
            $img_key = 'face/faceImg/' . substr($imgname, 0, strpos($imgname, '.')) . '.jpg'; //上传后文件名称
            $base = base64_decode($res['result']['pic_list'][0]['pic']);
            //异步上传图片资源到七牛云
            $AppTask = $this->loader->task(AppTask::class, $this);
            $AppTask->startTask('sysnUploadToQiniu', [$base, $img_key], -1, function ($serv, $task_id, $data) {

            });
            $urls = [
                'face_video' => 'face/video/' . $imgname,
                'face_img' => $img_key,
            ];
            $result['code'] = 1000;
            $result['msg'] = '认证成功';
            $result['data'] = $urls;
            return $result;
        }
        $result['code'] = -1000;
        $result['msg'] = $msg;
        return $result;
    }

    //百度API-身份证识别
    public function baidu_idcard($front, $back) {
        $arr['front'] = $this->aip_ocr->idcard($front, 'front', []);
        $arr['back'] = $this->aip_ocr->idcard($back, 'back', []);
        return $arr;
    }

    //百度API- 身份证与名字比对
    public function baidu_idmatch($id_card_number, $name) {
        if (empty($id_card_number)) {
            $result['error_code'] = -1002;
            $result['msg'] = '请输入身份证号码';
            return $result;
        }
        if (empty($name)) {
            $result['error_code'] = -1002;
            $result['msg'] = '请输入姓名';
            return $result;
        }
        //验证身份证是否合法
        if (!FunIsSFZ($id_card_number)) {
            $result['error_code'] = -1101;
            $result['msg'] = '请输入正确的身份证号码';
            return $result;
        }
        //验证姓名是否合法,长度不能少于2
        if (mb_strlen($name) < 2) {
            $result['error_code'] = -1102;
            $result['msg'] = '请输入正确的姓名';
            return $result;
        }
        $result = $this->aip_face->personIdmatch($id_card_number, $name);
        $error_msg = $this->config->get('baidu_idmatch')[$result['error_code']] ?? $result['error_msg'];
        $result['msg'] = $error_msg;
        return $result;
    }

    /*	 * ****************百度人脸识别结束*********************** */


    /*	 * **************腾讯人脸识别开始********************* */

    //获取唇语验证字符串
    public function tencent_live_get_four() {
        $is_check = true; //是否进行语音验证
        if (!$is_check) {
            $code = (string) rand(1000, 9999);
            return $this->jsonend(1000, 'ok', ['code' => $code, 'session_id' => '']);
        } else {
            $v_data = $this->face_client->faceLiveGetFour();
            $faceObj = json_decode($v_data, true);
            if ($faceObj['code'] == 0 && isset($faceObj['data']['validate_data'])) {
                return $this->jsonend(1000, 'ok', ['code' => $faceObj['data']['validate_data'], 'session_id' => '']);
            } else {
                return $this->jsonend(-1000, '获取唇语字符串失败');
            }
        }
    }

    //人脸核身
    public function tencent_face($name, $id_card, $code, $file) {
        $imgname = date('YmdHis') . $id_card . time() . '_' . rand(0, 9999) . substr($file['name'], strrpos($file['name'], '.')); //上传后文件名称
        $tmp = $file['tmp_name'];
        $filepath = WWW_DIR . '/face/video/';
        file_exists(WWW_DIR . '/face/') || mkdir(WWW_DIR . '/face/', 0777);
        file_exists($filepath) || mkdir($filepath, 0777);
        file_exists(WWW_DIR . '/face/faceImg/') || mkdir(WWW_DIR . '/face/faceImg/', 0777);
        if (!move_uploaded_file($tmp, $filepath . $imgname)) {
            return $this->jsonend(-1000, '上传视频失败');
        }
        $data = $this->face_client->faceIdCardLiveDetectFour($code, array('file' => $filepath . $imgname), $id_card, $name);
        $data = json_decode($data, true);
        if ($data['code'] == 0 && $data['data']['compare_status'] == 0 && $data['data']['live_status'] == 0 && $data['data']['sim'] >= 75) {
            file_put_contents(WWW_DIR . '/face/faceImg/' . substr($imgname, 0, strpos($imgname, '.')) . '.jpg', base64_decode($data['data']['video_photo']));
            $url = [
                'video' => $imgname,
                'faceImg' => substr($imgname, 0, strpos($imgname, '.')) . '.jpg',
            ];
            if (isset($data['data']['video_photo'])) {
                unset($data['data']['video_photo']);
            }
            return $this->jsonend(1000, '匹配成功', $url);
        } else {
            if (isset($data['data']['video_photo'])) {
                unset($data['data']['video_photo']);
            }
            //写入数据日志
            if (isset($data['data']['live_status']) && $data['data']['live_status'] != 0) {
                $msg = $this->errorMsg($data['data']['live_status']);
            } else if (isset($data['data']['compare_status']) && $data['data']['compare_status'] != 0) {
                $msg = $this->errorMsg($data['data']['compare_status']);
            } else {
                $msg = '匹配失败';
            }
            return $this->jsonend(-1000, $msg);
        }
    }

    /*	 * **************腾讯人脸识别结束********************* */

    /**
     * showdoc
     * @catalog API文档/工程端/实名认证相关
     * @title 获取用户认证信息
     * @description 如果传user_id查询该user_id认证信息,默认查询当前用户认证信息
     * @method POST
     * @url Engineer/Realname/getAuthInfo
     * @param user_id 可选 int 用户ID
     * @return {"code":1000,"message":"获取成功","data":{"auth_id":"6","user_id":"6","auth_type":"1","realname":"刘春霞","id_number":"500023519990160664","front_idcard_pic":"201810091100334249_tmp_7a6a217455fd3b8d304de93e832024860b798bdcbda9ffa7.jpg","back_idcard_pic":"201810091100366989_tmp_074465ed1c35477030234ab15641f4a824ee39b2d0109c2d.jpg","business_license_pic":null,"business_license_number":null,"status":"2","create_time":"2018-10-09 11:00:39","update_time":"","check_type":"1","check_time":"2018-10-09 11:00:45","remarks":"","is_delete":"0","auth_client":"1","face_video":null,"face_img":null,"front_idcard_pic_all":"https:\/\/qn.youheone.com\/201810091100334249_tmp_7a6a217455fd3b8d304de93e832024860b798bdcbda9ffa7.jpg","back_idcard_pic_all":"https:\/\/qn.youheone.com\/201810091100366989_tmp_074465ed1c35477030234ab15641f4a824ee39b2d0109c2d.jpg","business_license_pic_all":"","status_name":"认证成功"}}
     * @return_param auth_id int 认证ID
     * @return_param user_id int 用户ID
     * @return_param auth_type int 认证类型1个人认证 2企业认证3政事
     * @return_param realname int 真实姓名
     * @return_param id_number int 身份证号
     * @return_param front_idcard_pic int 身份证正面
     * @return_param back_idcard_pic int 身份证反面
     * @return_param business_license_pic int 营业执照
     * @return_param business_license_number int 识别号
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_getAuthInfo() {
        $map['user_id'] = $this->user_id;
        if (!empty($this->parm['user_id'] ?? '')) {
            $map['user_id'] = $this->parm['user_id'];
        }
        $data = $this->auth_model->getOne($map, '*');
        if (empty($data)) {
            return $this->jsonend(-1003, "还未提交过认证申请");
        }
        $data['create_time'] = !empty($data['create_time']) ? date('Y-m-d H:i:s', $data['create_time']) : '';
        $data['update_time'] = !empty($data['update_time']) ? date('Y-m-d H:i:s', $data['update_time']) : '';
        $data['check_time'] = !empty($data['check_time']) ? date('Y-m-d H:i:s', $data['check_time']) : '';
        $data['front_idcard_pic_all'] = UploadImgPath($data['front_idcard_pic']);
        $data['back_idcard_pic_all'] = UploadImgPath($data['back_idcard_pic']);
        $data['business_license_pic_all'] = UploadImgPath($data['business_license_pic']);
        $data['id_number_formate'] = substr($data['id_number'], 0, 1) . '******************' . substr($data['id_number'], 16, 2);
        $data['business_license_number_formate'] = substr($data['business_license_number'], 0, 3) . '******************' . substr($data['business_license_number'], 16, 2);
        $status = $this->config->get('realname_auth_status');
        $data['status_name'] = $status[$data['status']];
        $data['cur_system_check_type'] = $this->system_config; //当前认证方式
        return $this->jsonend(1000, "获取成功", $data);
    }

    /**
     * showdoc
     * @catalog API文档/工程端/实名认证相关
     * @title 获取用户认证状态
     * @description 如果传user_id查询该user_id认证状态,默认查询当前用户认证状态
     * @method POST
     * @url Engineer/Realname/getAuthStatus
     * @param user_id 可选 int 用户ID
     * @return {"code":1000,"message":"需要提醒客户认证","data":{"auth_status":0,"show_modal":true,"order_count":17,"work_count":57}}
     * @return_param auth_status int 认证状态 0未提交认证或者审核未通过 1已提交认证信息,等待审核中 2 审核通过
     * @return_param order_count int 订单数量
     * @return_param work_count int 工单数量
     * @return_param is_auth int 是否提交过认证
     * @return_param show_modal boolean 是否显示提示弹窗
     * @remark
     * @number 0
     * @author lcx
     * @date 2019-03-01
     */
    public function http_getAuthStatus() {
        //查询认证状态
        $user_id = $this->user_id;
        if (!empty($this->parm['user_id'] ?? '')) {
            $user_id = $this->parm['user_id'];
        }
        $map['user_id'] = $user_id;
        $data = $this->auth_model->getOne($map, '*');
        $data['is_auth'] = false;
        if (!empty($data)) {
            $data['is_auth'] = true;
        }
        //判断是否有新订单-不包括待支付，已取消订单
        $order_map['order_status'] = ['IN', [2, 3, 4, 6]];
        $order_map['user_id'] = $user_id;
        $data['order_count'] = $this->order_model->count($order_map, []);
        //判断是否有新工单
        $work_map['user_id'] = $user_id;
        $data['work_count'] = $this->work_model->getCount($work_map);
        return $this->jsonend(1000, "获取成功", $data);
    }
    /**
     * showdoc
     * @catalog API文档/工程端/实名认证相关
     * @title 修改认证状态
     * @description 目前仅支付调用
     * @method POST
     * @url Engineer/Realname/subAuthResult
     * @param data 必选 bool 认证结果
     * @return
     * @remark
     * @number 0
     * @author lcx
     * @date 2019-03-01
     */
    public function http_subAuthResult() {
        try {
            $flag = $this->parm['data'];
            $uid = $this->user_id;
            if ($flag) {
                $auth['status'] = 2;
                $auth['update_time'] = time();
                $res = $this->auth_model->save(array('user_id' => $uid), $auth);
                $edit_user_data['is_auth'] = 2;
                $this->user_model->save(array('user_id' => $uid), $edit_user_data);
                return $this->jsonend(1000, "认证成功", []);
            } else {
                return $this->jsonend(-1000, "认证失败", []);
            }
        } catch (\Exception $e) {
            return $this->jsonend(-1000, "非法操作", []);
        }
    }


}
