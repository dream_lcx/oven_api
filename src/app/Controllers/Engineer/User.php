<?php

namespace app\Controllers\Engineer;


/**
 * 用户模块API
 */
class User extends Base {

    protected $user_model;
    protected $invite_model;
    protected $auth_model;

    protected $engineers_model;

    public function initialization($controller_name, $method_name) {
        parent::initialization($controller_name, $method_name);
        $this->user_model = $this->loader->model('CustomerModel', $this);
        $this->invite_model = $this->loader->model('InvitationModel', $this);
        $this->auth_model = $this->loader->model('CustomerAuthModel', $this);
        $this->engineers_model = $this->loader->model('EngineersModel', $this);
    }

    /**
     * showdoc
     * @catalog API文档/工程端/用户相关
     * @title 添加编辑用户紧急联系人
     * @description
     * @method POST
     * @url Engineer/User/editContacts
     * @param user_id 必选 sting 用户ID
     * @param work_order_id 必选 sting 工单ID
     * @param emergency_contact 必选 array 紧急联系人信息,数组可以多个
     * @return {"code": 1000,"message": "操作成功","data":""}
     * @return_param
     * @remark {"work_order_id":"12"}
     * @number 0
     * @author ysf
     * @date 2018-10-19
     */
    public function http_editContacts()
    {
        // 接收验证参数
        $user_id = $this->parm['user_id'] ?? '';
        $emergency_contact = $this->parm['emergency_contact'] ?? '';
        if (empty($user_id)) {
            return $this->jsonend(-1001, '用户ID不能为空');
        }
        if (empty($emergency_contact)) {
            return $this->jsonend(-1001, '请填写紧急联系人信息');
        }
        if (empty($this->parm['work_order_id'])) {
            return $this->jsonend(-1001, '工单ID不能为空');
        }

        if(!isChsDash($emergency_contact['name'])){
            return $this->jsonend(-1001, '请填写正确的联系人');
        }

        if(!isMobile($emergency_contact['tel'])){
            return $this->jsonend(-1001, '请填写正确的联系电话');
        }

        $where = ['user_id' => $user_id];
        $data['emergency_contact'] = json_encode($emergency_contact);
        $data['operation_time'] = time();

        // 操作紧急联系人
        $res = $this->user_model->save($where, $data);
        $engineers_info = $this->engineers_model->getOne(array('engineers_id' => $this->user_id), 'engineers_name,engineers_id');
        $remark = "【维修端】工程人员【" . $engineers_info['engineers_name'] . "】【工程人员ID:" . $engineers_info['engineers_id'] . "】操作工单紧急联系人";
        $this->addWorkOrderLog($this->parm['work_order_id'], time(), 0, $remark);
        if ($res) {
            return $this->jsonend(1000, '操作成功');
        }
            return $this->jsonend(-1000, '操作失败');
    }


    /**
     * showdoc
     * @catalog API文档/工程端/用户相关
     * @title 获取用户信息
     * @description
     * @method POST
     * @url Engineer/User/getUserInfo
     * @param user_id 必选 sting 用户ID
     * @return
     * @return_param user_id 用户ID
     * @return_param nickname 用户昵称
     * @return_param is_auth 是否实名认证1否2是
     * @return_param account 账号
     * @return_param openid 用户openid
     * @return_param user_class 用户类型1个人用户 2企业用户
     * @return_param telphone 用户电话
     * @return_param username 用户昵称(解码的)
     * @return_param avatar_img 头像
     * @return_param gender 性别
     * @return_param country 国家
     * @return_param province 省
     * @return_param city 市
     * @return_param area 区
     * @return_param user_address 详细地址
     * @return_param source 推荐人ID
     * @return_param source_is_valid 是否生效 1否2 是
     * @return_param account_status 帐号状态(1.正常,2.冻结)
     * @return_param remark 备注
     * @return_param total_integral 累计积分
     * @return_param available_integral 可用积分
     * @return_param bind_time 绑定手机时间
     * @return_param add_time 注册时间
     * @remark {"user_id":"1"}
     * @number 0
     * @author lcy
     * @date 2018-10-18
     */
    public function http_getUserInfo() {
        if (empty($this->parm['user_id'] ?? '')) {
            return $this->jsonend(-1001, "缺少参数用户ID");
        }
        $map['user_id'] = $this->parm['user_id'];
        $field = 'user_id,nickname,is_auth,account,openid,user_class,telphone,username,avatar_img,gender,country,province,city,user_address,source,source_is_valid,account_status,remark,total_integral,available_integral,bind_time,add_time,birthday';
        $data = $this->user_model->getOne($map, $field);
        if (empty($data)) {
            return $this->jsonend(-1005, "未找到用户信息");
        }
        $data['add_time'] = !empty($data['add_time']) ? date('Y-m-d H:i:s', $data['add_time']) : '';
        $data['bind_time'] = !empty($data['bind_time']) ? date('Y-m-d H:i:s', $data['bind_time']) : '';
		$data['birthday'] = !empty($data['birthday']) ? date('Y/m/d', $data['birthday']) : '';
        $data['avatar_img_all'] = $data['avatar_img'];
        if (!empty($data['avatar_img']) && !urlIsAll($data['avatar_img'])) {
            $data['avatar_img_all'] = $this->config->get('qiniu.qiniu_url') . $data['avatar_img'];
        }
        if (!empty($data['nickname'])) {
            $data['username'] = urldecode($data['nickname']);
        }
        return $this->jsonend(1000, "获取用户信息成功", $data);
    }

}
