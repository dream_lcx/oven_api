<?php

namespace app\Controllers\Engineer;

use app\Wechat\WxAuth;
use Server\Components\CatCache\CatCacheRpcProxy;

/**
 * 小程序公共API
 */
class Wx extends Base {

    protected $accessKey;
    protected $secrectKey;
    protected $bucket;   // 要上传的空间
    protected $user_model;

    public function initialization($controller_name, $method_name) {
        parent::initialization($controller_name, $method_name);
        $this->user_model = $this->loader->model('CustomerModel', $this);
    }

    public function __construct() {
        parent::__construct();
        $this->accessKey = $this->config->get('qiniu.accessKey');
        $this->secrectKey = $this->config->get('qiniu.secrectKey');
        $this->bucket = $this->config->get('qiniu.bucket');
    }

    /**
     * showdoc
     * @catalog API文档/公共API/微信相关
     * @title 获取用户openid
     * @description 根据code获取openid
     * @method POST
     * @url Common/Wx/register
     * @param code 必选 string 小程序登录后获取code
     * @return
     * @return_param openid string 用户openid
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_register()
    {
        if (empty($this->parm['code'] ?? '')) {
            return $this->jsonend(-1001, "缺少参数code");
        }
        $wx_config = $this->wx_config;
        //$WxAuth = new WxAuth($wx_config['appId'], $wx_config['appSecret']);

        $WxAuth = WxAuth::getInstance($wx_config['appId'], $wx_config['appSecret']);
        $WxAuth->setConfig($wx_config['appId'], $wx_config['appSecret']);

        $json = $WxAuth->getSmallOAuth($this->parm['code']);
        if (!empty($json['openid'])) {
            CatCacheRpcProxy::getRpc()->offsetSet('session_key_' . $json['openid'], $json['session_key']);
            CatCacheRpcProxy::getRpc()->offsetSet('openid_' . $json['openid'], $json['openid']);
            return $this->jsonend(1000, '获取openid成功', $json);
        } else {
            return $this->jsonend(-1000, '获取openid失败');
        }
    }

}
