<?php

namespace app\Controllers\Common;

use app\QcloudImage\CIClient;
use app\Tasks\AppTask;

/**
 * 人脸识别--腾讯
 *
 * @author lcx
 */
class Face extends Base {

    protected $face_client;
    protected $aip_face;
    protected $auth_api = 2; //调用接口 1腾讯 2百度
    protected $body_analysis;

    public function __construct() {
        parent::__construct();

        //腾讯
        if ($this->auth_api == 1) {
            require_once APP_DIR . '/Library/face/index.php';
            $face_config = $this->config['face'];
            $this->face_client = new CIClient($face_config['appid'], $face_config['secretId'], $face_config['secretKey'], $face_config['bucket']);
            $this->face_client->setTimeout(120);
        } else {
            //百度
            $baidu_face_config = $this->config->get('baidu_face');
            $appId = $baidu_face_config['appId'];
            $apiKey = $baidu_face_config['apiKey'];
            $secretKey = $baidu_face_config['secretKey'];
            require_once APP_DIR . '/Library/BaiduApi/AipFace.php';
            require_once APP_DIR . '/Library/BaiduApi/AipBodyAnalysis.php';
            $this->aip_face = new \AipFace($appId, $apiKey, $secretKey);
            $this->body_analysis = new \AipBodyAnalysis($appId, $apiKey,$secretKey);
        }

    }

    public function initialization($controller_name, $method_name) {
        parent::initialization($controller_name, $method_name);
        $this->parm = $this->http_input->getAllPostGet();
    }

    /**
     * showdoc
     * @catalog API文档/公共API/实名认证相关
     * @title 获取唇语验证字符串
     * @description 
     * @method POST
     * @url Common/Face/livegetfour
     * @return 
     * @remark 
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_livegetfour() {
        if ($this->auth_api == 1) {
            return $this->tencent_live_get_four();
        } else {
            return $this->baidu_live_get_four();
        }
    }

    /**
     * showdoc
     * @catalog API文档/公共API/实名认证相关
     * @title 人脸核身
     * @description 
     * @method POST
     * @url Common/Face/人脸核身
     * @param code 必选 string 验证码  
     * @param file 必选 string 视频文件
     * @param id_card 可选 string 身份证号  
     * @param name 可选 string 用户真实姓名
     * @return 
     * @remark 
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_face() {
        $debug = false; //系统自动通过，如果需要真实审核改为false即可
        $read_num = true;
        if (!$debug && $read_num) {
            if (empty($this->parm['code'] ?? '')) {
                return $this->jsonend(-1000, 'code 参数错误');
            }
        }
        if (empty($this->request->files['file'] ?? '')) {
            return $this->jsonend(-1000, '请上传视频');
        }
        if (empty($this->parm['id_card'] ?? '')) {
            return $this->jsonend(-1000, '请填写身份证号');
        }
        if (empty($this->parm['name'] ?? '')) {
            return $this->jsonend(-1000, '请填写真实姓名');
        }

        $file = $this->request->files['file'];
        //模拟通过
        if ($debug) {
            $url = [
                'video' => '',
                'faceImg' => '',
                'need_again_check' => 2
            ];
            return $this->jsonend(1000, '匹配成功', $url);
        }
        if ($this->auth_api == 1) {
            return $this->tencent_face($this->parm['name'], $this->parm['id_card'], $this->parm['code'], $file);
        } else {
            $session_id = $this->parm['session_id'] ?? '';
            return $this->baidu_face($this->parm['name'], $this->parm['id_card'], $this->parm['code'], $file, $session_id);
        }
    }

    public function http_idcard() {
        if (empty($this->parm['id_card'] ?? '')) {
            return $this->jsonend(-1000, '请填写身份证号');
        }
        if (empty($this->parm['name'] ?? '')) {
            return $this->jsonend(-1000, '请填写真实姓名');
        }
        if (empty($this->parm['img_url'] ?? '')) {
            return $this->jsonend(-1000, '请上传身份证照片');
        }
        $img['url'] = $this->parm['img_url'];
        //$img['file'] = $this->request->files['file'];
        $data = $this->face_client->faceIdCardCompare($this->parm['id_card'], $this->parm['name'], $img);
        $data = json_decode($data, true);
        if ($data['code'] == 0 && $data['data']['similarity'] >= 75) {
            return $this->jsonend(1000, '匹配成功', $data['data']['similarity']);
        } else {
            $msg = $this->errorMsg($data['code']);
            return $this->jsonend(-1000, $msg);
        }
    }

    public function errorMsg($code = '') {
        $msg = '匹配失败';
        echo '------' . $code . '---------';
        switch ($code) {
            case -4006:
                $msg = '视频中自拍照特征提取失败';
                break;
            case -4007:
                $msg = '视频中自拍照之间对比失败';
                break;
            case -4009:
                $msg = 'Card 照片提取特征失败';
                break;
            case -4010:
                $msg = '自拍照与 Card 照片相似度计算失败';
                break;
            case -4011:
                $msg = '照片解码失败';
                break;
            case -4012:
                $msg = '照片人脸检测失败';
                break;
            case -4015:
                $msg = '自拍照人脸检测失败';
                break;
            case -4016:
                $msg = '自拍照解码失败';
                break;
            case -4017:
                $msg = 'Card 照片人脸检测失败';
                break;
            case -4018:
                $msg = 'Card 照片解码失败';
                break;
            case -5001:
                $msg = '视频无效，上传文件不符合视频要求';
                break;
            case -5002:
                $msg = '唇语失败';
                break;
            case -5005:
                $msg = '自拍照解析照片不足，视频里检测到的人脸较少';
                break;
            case -5007:
                $msg = '视频没有声音';
                break;
            case -5008:
                $msg = '语音识别失败，视频里的人读错数字';
                break;
            case -5009:
                $msg = '视频人脸检测失败，没有嘴或者脸';
                break;
            case -5010:
                $msg = '唇动检测失败，视频里的人嘴巴未张开或者张开幅度小';
                break;
            case -5011:
                $msg = '活体检测失败';
                break;
            case -5012:
                $msg = '视频中噪声太大';
                break;
            case -5013:
                $msg = '视频里的声音太小';
                break;
            case -5014:
                $msg = '活体检测 level 参数无效';
                break;
            case -5015:
                $msg = '视频像素太低，最小 270*480';
                break;
            case -5016:
                $msg = '视频里的人不是活体';
                break;
            case -5801:
                $msg = '缺少身份证号码或身份证姓名';
                break;
            case -5802:
                $msg = '服务器内部错误，服务暂时不可用';
                break;
            case -5803:
                $msg = '身份证姓名与身份证号码不一致';
                break;
            case -5804:
                $msg = '身份证号码无效';
                break;
            case -5805:
                $msg = '用户未输入图像或者 url 下载失败';
                break;
            case -5806:
                $msg = '身份证姓名或身份证号码无效';
                break;
            case -5809:
                $msg = '用户身份证登记照无效';
                break;
            default :
                echo $code . '------default';
                $msg = '匹配失败';
                break;
        }
        return $msg;
    }

    /*     * **************腾讯人脸识别开始********************* */

    //获取唇语验证字符串
    public function tencent_live_get_four() {
        $is_check = true; //是否进行语音验证
        if (!$is_check) {
            $code = (string) rand(1000, 9999);
            return $this->jsonend(1000, 'ok', ['code' => $code, 'session_id' => '']);
        } else {
            $v_data = $this->face_client->faceLiveGetFour();
            $faceObj = json_decode($v_data, true);
            if ($faceObj['code'] == 0 && isset($faceObj['data']['validate_data'])) {
                return $this->jsonend(1000, 'ok', ['code' => $faceObj['data']['validate_data'], 'session_id' => '']);
            } else {
                return $this->jsonend(-1000, '获取唇语字符串失败');
            }
        }
    }

    //人脸核身
    public function tencent_face($name, $id_card, $code, $file) {
        $imgname = date('YmdHis') . $id_card . time() . '_' . rand(0, 9999) . substr($file['name'], strrpos($file['name'], '.')); //上传后文件名称
        $tmp = $file['tmp_name'];
        $filepath = WWW_DIR . '/face/video/';
        file_exists(WWW_DIR . '/face/') || mkdir(WWW_DIR . '/face/', 0777);
        file_exists($filepath) || mkdir($filepath, 0777);
        file_exists(WWW_DIR . '/face/faceImg/') || mkdir(WWW_DIR . '/face/faceImg/', 0777);
        if (!move_uploaded_file($tmp, $filepath . $imgname)) {
            return $this->jsonend(-1000, '上传视频失败');
        }
        $data = $this->face_client->faceIdCardLiveDetectFour($code, array('file' => $filepath . $imgname), $id_card, $name);
        $data = json_decode($data, true);
        if ($data['code'] == 0 && $data['data']['compare_status'] == 0 && $data['data']['live_status'] == 0 && $data['data']['sim'] >= 75) {
            file_put_contents(WWW_DIR . '/face/faceImg/' . substr($imgname, 0, strpos($imgname, '.')) . '.jpg', base64_decode($data['data']['video_photo']));
            $url = [
                'video' => $imgname,
                'faceImg' => substr($imgname, 0, strpos($imgname, '.')) . '.jpg',
            ];
            if (isset($data['data']['video_photo'])) {
                unset($data['data']['video_photo']);
            }
            return $this->jsonend(1000, '匹配成功', $url);
        } else {
            if (isset($data['data']['video_photo'])) {
                unset($data['data']['video_photo']);
            }
            if (isset($data['data']['live_status']) && $data['data']['live_status'] != 0) {
                $msg = $this->errorMsg($data['data']['live_status']);
            } else if (isset($data['data']['compare_status']) && $data['data']['compare_status'] != 0) {
                $msg = $this->errorMsg($data['data']['compare_status']);
            } else {
                $msg = '匹配失败';
            }
            return $this->jsonend(-1000, $msg);
        }
    }

    /*     * **************腾讯人脸识别结束********************* */

    /*     * ****************百度人脸识别开始*********************** */

    //获取唇语验证字符串
    public function baidu_live_get_four() {
        $is_check = false; //是否进行语音验证
        if (!$is_check) {
            $code = (string) rand(2222, 9999);
            return $this->jsonend(1000, 'ok', ['code' => $code, 'session_id' => '']);
        } else {
            //获取语音验证码
            $code_res = $this->aip_face->videoSessioncode();
            $code = [];
            if ($code_res['err_no'] == 0) {
                $code = $code_res['result'];
                return $this->jsonend(1000, 'ok', $code);
            } else {
                return $this->jsonend(-1000, '获取唇语字符串失败');
            }
        }
    }

    //人脸核身
    public function baidu_face($name, $id_card, $code, $file, $session_id) {
        $imgname = date('YmdHis') . $id_card . time() . '_' . rand(0, 9999) . substr($file['name'], strrpos($file['name'], '.')); //上传后文件名称
        $tmp = $file['tmp_name'];
        $filepath = WWW_DIR . '/face/video/';
        file_exists(WWW_DIR . '/face/') || mkdir(WWW_DIR . '/face/', 0777);
        file_exists($filepath) || mkdir($filepath, 0777);
        file_exists(WWW_DIR . '/face/faceImg/') || mkdir(WWW_DIR . '/face/faceImg/', 0777);
        if (!move_uploaded_file($tmp, $filepath . $imgname)) {
            return $this->jsonend(-1000, '上传视频失败');
        }
        //  $imgname = "201901091754495002351992013033401547027689_4554.mp4";
        $videoBase64 = base64_encode(file_get_contents($filepath . $imgname));
        $res = $this->aip_face->videoFaceliveness($session_id, $videoBase64);
        $msg = $this->config->get('baidu_face_error_code')[$res['err_no']] ?? '验证失败,请重试';
        //unset($res['result']['pic_list']);
        //var_dump($res);
        if ($res['err_no'] == 0 ) {
            if($res['result']['score']<$res['result']['thresholds']['frr_1e-3']){
                return $this->jsonend(-1000, '活体检测失败');
            }
            $score = $res['result']['score'] ?? '';
            $result = $this->aip_face->personVerify($res['result']['pic_list'][0]['pic'],'BASE64',$id_card,$name);
            //var_dump($result);
            if($result['error_code']!=0){
                $msg2 = $this->config->get('baidu_gongan')[$result['error_code']] ?? '身份验证失败,请重试';
                return $this->jsonend(-1000, $msg2);
            }
            if($result['result']['score']<80){
                return $this->jsonend(-1000, '公安身份校验不通过');
            }
             file_put_contents(WWW_DIR . '/face/faceImg/' . substr($imgname, 0, strpos($imgname, '.')) . '.jpg', base64_decode($res['result']['pic_list'][0]['pic']));
            $url = [
                'video' => $imgname,
                'faceImg' => substr($imgname, 0, strpos($imgname, '.')) . '.jpg',
            ];
            return $this->jsonend(1000, "验证通过",$url);
        }
        return $this->jsonend(-1000, $msg);
    }

    /*     * ****************百度人脸识别结束*********************** */
    //百度人像分割
    public function http_bodySeg(){
        $image = file_get_contents(WWW_DIR.'/5.jpg');
        $time = time();
       // 调用人像分割
        $res = $this->body_analysis->bodySeg($image);
        $foreground = base64_decode($res['foreground']);
        $filepath = WWW_DIR.'/';
        $imgname = 'bodyseg-fg-'.$time.'.png';
        $url = $filepath . $imgname;
        file_put_contents($url,$foreground);
        //异步上传视频资源到七牛云
        $AppTask = $this->loader->task(AppTask::class, $this);
        $AppTask->startTask('sysnUploadToQiniu', [file_get_contents($url), 'baidu/bodyseg/' . $imgname], -1, function ($serv, $task_id, $data) {

        });
        //scoremap
        $scoremap = base64_decode($res['scoremap']);
        $imgname_scoremap = 'bodyseg-sp-'.$time.'.png';
        $url_scoremap = $filepath . $imgname_scoremap;
        file_put_contents($url_scoremap,$scoremap);
        //异步上传视频资源到七牛云
        $AppTask = $this->loader->task(AppTask::class, $this);
        $AppTask->startTask('sysnUploadToQiniu', [file_get_contents($url_scoremap), 'baidu/bodyseg/' . $imgname_scoremap], -1, function ($serv, $task_id, $data) {

        });

        dump($res['log_id']);

    }
}
