<?php

namespace app\Controllers\Common;

use app\Wechat\WxAuth;
use Server\Components\CatCache\CatCacheRpcProxy;

/**
 * 小程序公共API
 */
class Wx extends Base {

    protected $accessKey;
    protected $secrectKey;
    protected $bucket;   // 要上传的空间
    protected $user_model;
    protected $customer_code_model;

    public function initialization($controller_name, $method_name) {
        parent::initialization($controller_name, $method_name);
        $this->user_model = $this->loader->model('CustomerModel', $this);
        $this->customer_code_model = $this->loader->model('CustomerCodeModel', $this);
    }

    public function __construct() {
        parent::__construct();
        $this->accessKey = $this->config->get('qiniu.accessKey');
        $this->secrectKey = $this->config->get('qiniu.secrectKey');
        $this->bucket = $this->config->get('qiniu.bucket');
    }

    /**
     * showdoc
     * @catalog API文档/公共API/微信相关
     * @title 保存formID
     * @description 在开发者工具上无法获取到有效地formId
     * @method POST
     * @url Common/Wx/saveFormId
     * @param openid 必选 string 用户名  
     * @param formId 必选 string 表单formId  
     * @return {code: -1001, message: "formId不合法", data: ""}
     * @remark {"formId":"the formId is a mock one","openid":"o-abM4ve_N2NzGzadJtGB1gVbp4A"}
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_saveFormId() {
        if (empty($this->parm['openid'] ?? '')) {
            return $this->jsonend(-1001, "缺少参数openid");
        }
        if (empty($this->parm['formId'] ?? '')) {
            return $this->jsonend(-1001, "缺少参数formId");
        }
        if ($this->parm['formId'] == 'the formId is a mock one') {
            return $this->jsonend(-1001, "formId不合法");
        }
        $key = 'cloud_water:' . $this->parm['openid'];
        $arr['formId'] = $this->parm['formId'];
        $arr['expire_time'] = time() + 3600;
        $res = $this->redis->lPush($key, json_encode($arr));
        if ($res) {
            return $this->jsonend(1000, "保存成功");
        }
        return $this->jsonend(-1000, "保存失败");
    }

    /**
     * showdoc
     * @catalog API文档/公共API/微信相关
     * @title 生成页面二维码
     * @description 
     * @method POST
     * @url Common/Wx/getPageQr
     * @param query 必选 string 参数id=1
     * @param page 必选 string 页面路径pages/home/home,该路径必须在发布版存在  
     * @return {"code": 1000,"message": "success","data": {"url": "201810181611554495_goods_5887.jpg","all_url": "https:\/\/qn.youheone.com\/201810181611554495_goods_5887.jpg" }}
     * @return_param url string 二维码图片名称
     * @return_param all_url string 二维码图片路径
     * @remark {"query":"id=1","page":"pages/home/home"}
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_getPageQr() {
        $wx_config = $this->wx_config;
        //$WxAuth = new WxAuth($wx_config["appId"], $wx_config["appSecret"]);
        $WxAuth = WxAuth::getInstance($wx_config['appId'], $wx_config['appSecret']);
        $WxAuth->setConfig($wx_config['appId'], $wx_config['appSecret']);

        $scene = $this->parm['query'];//scence参数太长
        $page = $this->parm['page'];
        $width = '400px';
        $auto_color = true;
        $line_color = "{'r':'0','g':'0','b':'0'}";
        $base = $WxAuth->getSamllPageQr($scene, $width, $auto_color, $line_color, $page, $this->role_key);

        // 构建鉴权对象 #需要填写你的 Access Key 和 Secret Key
        //$auth = new Auth($this->accessKey, $this->secrectKey);
        $auth = get_instance()->QiniuAuth;
        $token = $auth->uploadToken($this->bucket);
        //$uploadMgr = new UploadManager();
        $uploadMgr = get_instance()->QiniuUploadManager;
        $key = date('YmdHis') . rand(0, 9999) . '_pic_' . rand(0, 9999) . '.jpg'; //上传后文件名称
        list($ret, $err) = $uploadMgr->put($token, $key, $base);
        if ($err !== null) {
            return $this->jsonend(-1000, 'fail');
        } else {
            $data['url'] = $key;
            $data['all_url'] = $this->config->get('qiniu.qiniu_url') . $key;
            return $this->jsonend(1000, 'success', $data);
        }
    }



    //生成用户推广二维码
    public function http_getUserPageQr() {
        $wx_config = $this->wx_config;
        //$WxAuth = new WxAuth($wx_config["appId"], $wx_config["appSecret"]);
        $WxAuth = WxAuth::getInstance($wx_config['appId'], $wx_config['appSecret']);
        $WxAuth->setConfig($wx_config['appId'], $wx_config['appSecret']);
        $width = '100px';
        $auto_color = true;
        $line_color = "{'r':'0','g':'0','b':'0'}";
        $users = $this->user_model->getAll([], 'user_id,telphone,account');
        $page = "pages/home/home";
        foreach ($users as $k => $v) {
            if (!empty($v['telphone'])) {
                $scene = "pid=" . $v['user_id'];
                $base = $WxAuth->getSamllPageQr($scene, $width, $auto_color, $line_color, $page);
                $base = base64_encode($base); //将二进制转base64
                $filename = $v['telphone'] . '_' . $v['account'] . '.png';
                $url = convertBaseimg($base, $filename);
                echo $url;
            }
        }
    }
    /**
     * showdoc
     * @catalog API文档/公共API/微信相关
     * @title 手机号解密
     * @description 获取小程序用户手机信息后进行解密
     * @method POST
     * @url Common/Wx/aesPhoneDecrypt
     * @param session_key 必选 string 用户名
     * @param data 必选 string 密码
     * @param iv 可选 string 用户昵称
     * @return {"code": 1000, "message": "用户信息解析成功", "data": { "phoneNumber": "18883880448", "purePhoneNumber": "18883880448","countryCode": "86", "watermark": {"timestamp": 1539831979, "appid": "wx76e19f9eb5bcb658" } }
     * @return_param phoneNumber int 手机号
     * @return_param countryCode string 国家编号
     * @remark {data:"Q1ocwpmYHhzy+oe01hHpDQxv8wv1jckHMpVfLT2FLn8ipRLx4vrtZJ/SRb0ZAaqkvWRDUjeDj7HOkL8L9cw2fOcpfdABSvvJ6Gm7PXtZdgwEoALjvlm71O+QGAcSY+gDUXNOvQCB0v+vfvmsplJA2gIret3pL50rBzHLyVpF5LMEBKgiAXTUrWZ4YetWE/wQkQghrQRky2wI4yqCicDQig=="iv:"M1dbvvl8mZWTbtptBxAOiA=="openid:"o-abM4ve_N2NzGzadJtGB1gVbp4A"}
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_aesPhoneDecrypt()
    {
        $wx_config = $this->wx_config;
        $session_key = CatCacheRpcProxy::getRpc()->offsetGet('session_key_' . $this->parm['openid']);
        if (empty($session_key)) {
            return $this->jsonend(-1001, "缺少参数session_key");
        }
        if (empty($this->parm['data'] ?? '')) {
            return $this->jsonend(-1001, "缺少参数加密串");
        }
        if (empty($this->parm['iv'] ?? '')) {
            return $this->jsonend(-1001, "缺少参数iv");
        }
        $encryptedData = $this->parm['data'];
        $iv = $this->parm['iv'];


        //$pc = new WXBizDataCrypt($wx_config['appId'], $sessionKey);
        $pc = get_instance()->WXBizDataCrypt;
        $pc->setSessionKey($session_key);
        $pc->setAppid($wx_config['appId']);

        $errCode = $pc->decryptData($encryptedData, $iv, $data);
        if ($errCode == 0) {
            $arr = json_decode($data, true);
            return $this->jsonend(1000, '用户信息解析成功', $arr);
        }
        return $this->jsonend(-1000, '用户信息解析失败');
    }

    /**
     * showdoc
     * @catalog API文档/公共API/微信相关
     * @title 获取用户openid
     * @description 根据code获取openid
     * @method POST
     * @url Common/Wx/getUserOpenid
     * @param code 必选 string 小程序登录后获取code
     * @return
     * @return_param openid string 用户openid
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_getUserOpenid()
    {
        if (empty($this->parm['code'] ?? '')) {
            return $this->jsonend(-1001, "缺少参数code");
        }
        $wx_config = $this->wx_config;
        //$WxAuth = new WxAuth($wx_config['appId'], $wx_config['appSecret']);
        $WxAuth = WxAuth::getInstance($wx_config['appId'], $wx_config['appSecret']);
        $WxAuth->setConfig($wx_config['appId'], $wx_config['appSecret']);
        $json = $WxAuth->getSmallOAuth($this->parm['code']);
        if (!empty($json['openid'])) {
            CatCacheRpcProxy::getRpc()->offsetSet('session_key_' . $json['openid'], $json['session_key']);
            CatCacheRpcProxy::getRpc()->offsetSet('openid_' . $json['openid'], $json['openid']);
            return $this->jsonend(1000, '获取openid成功', $json);
        } else {
            return $this->jsonend(-1000, '获取openid失败');
        }
    }
    //生成用户付款码
    public function http_createPayCodeQr() {
        $code = $this->customer_code_model->getOne(['code'=>$this->parm['code'],'is_delete'=>0],'code_id,pay_qrcode');
        if(!empty($code) && empty($code['pay_qrcode'])){
            $data['url'] = $code['pay_qrcode'];
            $data['all_url'] = $this->config->get('qiniu.qiniu_url') . $code['pay_qrcode'];
            return $this->jsonend(1000, 'success', $data);
        }
        $wx_config = $this->company_config['wx_config'];
        //$WxAuth = new WxAuth($wx_config["appId"], $wx_config["appSecret"]);
        $WxAuth = WxAuth::getInstance($wx_config['appId'], $wx_config['appSecret']);
        $WxAuth->setConfig($wx_config['appId'], $wx_config['appSecret']);

        $scene = 'code='.$this->parm['code'];//scence参数太长
        $page = 'pages/user/renew/renew';
        $width = '400px';
        $auto_color = true;
        $line_color = "{'r':'0','g':'0','b':'0'}";
        $base = $WxAuth->getSamllPageQr($scene, $width, $auto_color, $line_color, $page, $this->role_key);

        // 构建鉴权对象 #需要填写你的 Access Key 和 Secret Key
        //$auth = new Auth($this->accessKey, $this->secrectKey);
        $auth = get_instance()->QiniuAuth;
        $token = $auth->uploadToken($this->bucket);
        //$uploadMgr = new UploadManager();
        $uploadMgr = get_instance()->QiniuUploadManager;
        $key = date('YmdHis') . rand(0, 9999) . '_pic_' . rand(0, 9999) . '.jpg'; //上传后文件名称
        list($ret, $err) = $uploadMgr->put($token, $key, $base);
        if ($err !== null) {
            return $this->jsonend(-1000, 'fail');
        } else {
            $data['url'] = $key;
            $data['all_url'] = $this->config->get('qiniu.qiniu_url') . $key;
            //如果付款码为空,写入
            $this->customer_code_model->save(['code_id'=>$code['code_id']],['pay_qrcode'=>$key]);
            return $this->jsonend(1000, 'success', $data);
        }
    }

}
