<?php


namespace app\Controllers\Common;

use app\Services\Common\HttpService;

/**
 * 树米SIM卡查询
 * @desc API
 * @author xg
 * @date 2019/11/27
 */
class Sim extends Base
{
    protected $pageSize;
    protected $count;
    protected $equipmentLibraryModel;

    public function initialization($controller_name, $method_name)
    {
        parent::initialization($controller_name, $method_name);
        $this->equipmentLibraryModel = $this->loader->model('EquipmentLibraryModel', $this);
        $this->count = 0;
        $this->pageSize = 100;
        $this->conf = $this->config->get('shumi');
    }

    /**
     * 查询ism流量卡状态
     */
    public function http_getIms()
    {
        ini_set('max_execution_time', 0);
        $count = $this->equipmentLibraryModel->count(['device_status' => ['<>', 3], 'sim_sn' => ['<>', 'null']]);
        $this->count = $count;
        if ($count <= 0) {
            return true;
        }
        $status_success = $this->setImsStatus(1);
        if ($status_success) {
            $this->setImsInfo(1);
        }
        return true;
    }

    /**
     * 查询流量卡状态 并修改数据库状态
     * @param type $page
     * @return boolean
     */
    public function setImsStatus($page)
    {
        $cards = $this->equipmentLibraryModel->getAll(['device_status' => ['<>', 3], 'sim_sn' => ['<>', 'null']], '*', $page, $this->pageSize);
        if (empty($cards)) {
            return true;
        }
        $card_arr = [];
        foreach ($cards as $k => &$v) {
            if (empty($v['sim_sn'])) {
                unset($cards[$k]);
                continue;
            }
            array_push($card_arr, trim($v['sim_sn']));
        }
        $card_str = implode(',', $card_arr);
        //$this->getStatus(['iccid'=>'89860429161891321670','date'=>'20181228','duration'=>'2'],'/card/daily/usage/');
        $data = $this->getStatus(['iccidList' => $card_str], '/card/batch/status/');
        if (!empty($data)) {
            foreach ($data['data'] as $kk => $vv) {

                if (isset($vv['errorMsg'])) {
                    continue;
                }
                if (!isset($vv['accountStatus']) || empty($vv['accountStatus'])) {
                    continue;
                }
                $status = 0;
                switch ($vv['accountStatus']) {
                    case 'UNKNOWN'://未知状态
                        $status = -1;
                        break;
                    case 'READY'://待激活
                        $status = 0;
                        break;
                    case 'ACTIVATED'://已激活
                        $status = 1;
                        break;
                    case 'SUSPENDED'://已停机
                        $status = 2;
                        break;
                    case 'TERMINATED'://拆机
                        $status = 3;
                        break;
                    default :
                        $status = 0;
                        break;
                }
                try {
                    $this->equipmentLibraryModel->save(['sim_sn' => $vv['iccid']], ['sim_status' => $status]);
                } catch (\Throwable $e) {
                    echo '------流量卡状态修改失败--------' . $e->getMessage();
                    continue;
                }
            }
        }
        $total_page = ceil($this->count / $this->pageSize);
        $page++;
        if ($page <= $total_page) {
             $this->setImsStatus($page);
        }else{
            return $this->bossJsonend(1000, '查询完毕');
        }
        return true;
    }

    /**
     * 查询流量卡详情 并修改数据库状态
     * @param type $page
     * @return boolean
     */
    public function setImsInfo($page)
    {
        $cards = $this->equipmentLibraryModel->getAll(['device_status' => ['<>', 3], 'sim_sn' => ['<>', 'null']], '*', $page, $this->pageSize);
        if (empty($cards)) {
            return true;
        }
        $card_arr = [];
        foreach ($cards as $k => &$v) {
            if (empty($v['sim_sn'])) {
                unset($cards[$k]);
                continue;
            }
            array_push($card_arr, trim($v['sim_sn']));
        }
        $card_str = implode(',', $card_arr);
        $data = $this->getStatus(['iccidList' => $card_str], '/device/batch/info/');
        foreach ($data['data'] as $kk => $vv) {
            if (isset($vv['errorMsg'])) {
                continue;
            }
            try {
                $this->equipmentLibraryModel->save(['sim_sn' => $vv['iccid']], ['activa_time' => $vv['activeTime'], 'expire_time' => $vv['agreementEndTime'], 'seet_meal' => $vv['rateplan'],'last_query_time'=>time()]);
            } catch (\Throwable $e) {
                echo '------流量卡状态修改失败--------' . $e->getMessage();
                continue;
            }
        }
        $total_page = ceil($this->count / $this->pageSize);
        $page++;
        if ($page <= $total_page) {
                $this->setImsInfo($page);
        }else{
            return $this->bossJsonend(1000, '详情查询完毕');
        }
        return true;
    }


    /**
     * 调用API 查询流量卡状态
     * @param type $params 参数 ['sn'=>$equipment_number,'osn'=>$order_id,'money'=>$money]
     * @param type $path 路径 '/Water/Water/recharge_payment'
     */
    public function getStatus($params = [], $path = '/device/info/')
    {
        if (count($params) <= 1) {
            $keys = $this->is_assoc($params);
            if ($keys === true) {
                $path = '/v1/openapi' . $path . $params[0];
                $path2 = $path;
            } else {
                $path = rtrim($path, '/');
                $path2 = '/v1/openapi' . $path . $keys[0] . $params[$keys[0]];
                $path = $path . '?';
                $path = '/v1/openapi' . $path . $keys[0] . '=' . $params[$keys[0]];
            }
        } else {
            $path = '/v1/openapi' . $path;
            $path = rtrim($path, '/');
            $url = $this->getSign($params);
            $path2 = $path . $url;
            $path = $path . '?' . $url;
        }

        $header = [];
        $header['timestamp'] = time();
        $header['key'] = $this->conf['apiKey'];
        $str = 'GET' . $path2 . $header['timestamp'] . $header['key'] . $this->conf['apiSecret'];
        $sha = sha1($str, true);
        $sign = base64_encode($sha);
        $header['signature'] = $sign;
        $url = rtrim($this->conf['testUrl'],'/').$path;
        $result = HttpService::get($url, $header);
        if ($result) {
            return json_decode($result, true);
        } else {
            echo '========请求失败!==========';
        }
    }

    /**
     * 判断是否是索引数组
     * @param type $array
     * @return boolean
     */
    function is_assoc($array)
    {
        if (is_array($array)) {
            $keys = array_keys($array);
            if ($keys !== array_keys($keys)) {
                return $keys;
            }
            return true;
        }
        return true;
    }


    //组装签名数据
    public function getSign($Obj)
    {
        foreach ($Obj as $k => $v) {
            $Parameters[$k] = $v;
        }
        ksort($Parameters);
        $String = $this->formatBizQueryParaMap($Parameters, false);

        // $String = md5($String);
        $result_ = $String;
        //$result_ = strtoupper($String);

        return $result_;
    }

    //组装签名数据
    function formatBizQueryParaMap($paraMap, $urlencode)
    {
        $buff = "";
        ksort($paraMap);
        foreach ($paraMap as $k => $v) {
            if ($urlencode) {
                $v = urlencode($v);
            }

            $buff .= $k . $v;
        }
        $reqPar = '';
        if (strlen($buff) > 0) {
            $reqPar = substr($buff, 0, strlen($buff) - 1);
        }
        return $reqPar;
    }
}