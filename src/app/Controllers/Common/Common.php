<?php

namespace app\Controllers\Common;


use app\Services\Common\EmailService;
use app\Services\Company\CompanyService;
use app\Tasks\AppTask;
use Server\Components\CatCache\CatCacheRpcProxy;
use app\Wechat\WxAuth;
use app\Services\Common\SmsService;
use app\Services\Common\ConfigService;
use app\Custom\File;

/**
 * 公共API
 */
class Common extends Base
{

    protected $accessKey;
    protected $secrectKey;
    protected $bucket;   // 要上传的空间
    protected $bus_type_model;
    protected $time_model;
    protected $WxAuth;
    protected $work_eq_model;
    protected $ep_part_model;
    protected $additionalServiceModel;
    protected $work_order_model;
    protected $contract_model;
    protected $Achievement;
    protected $work_order_problem_model;
    protected $administrative_user_model;
    protected $customer_administrative_center_model;
    protected $stoveManagementModel;
    protected $customerCodeBillModel;
    protected $customerModel;
    protected $engineersModel;

    public function initialization($controller_name, $method_name)
    {
        parent::initialization($controller_name, $method_name);
        $this->customerModel = $this->loader->model('CustomerModel', $this);
        $this->engineersModel = $this->loader->model('EngineersModel', $this);
        $this->bus_type_model = $this->loader->model('BusinessTypeModel', $this);
        $this->time_model = $this->loader->model('WorkOrderTimeModel', $this);
        $this->work_order_problem_model = $this->loader->model('WorkOrderProblemModel', $this);
        $this->work_eq_model = $this->loader->model('WorkEquipmentModel', $this);
        $this->ep_part_model = $this->loader->model('EquipmentsPartsModel', $this);
        $this->additionalServiceModel = $this->loader->model('AdditionalServiceModel', $this);
        $this->work_order_model = $this->loader->model('WorkOrderModel', $this);
        $this->contract_model = $this->loader->model('ContractModel', $this);
        $this->administrative_user_model = $this->loader->model('AdministrativeUserModel', $this);
        $this->customer_administrative_center_model = $this->loader->model('CustomerAdministrativeCenterModel', $this);
        $this->stoveManagementModel = $this->loader->model('StoveManagementModel', $this);
        $this->customerCodeBillModel = $this->loader->model('CustomerCodeBillModel', $this);
        //$this->WxAuth = new WxAuth($this->wx_config["appId"], $this->wx_config["appSecret"]);
        $this->WxAuth = WxAuth::getInstance($this->wx_config["appId"], $this->wx_config["appSecret"]);
        $this->WxAuth->setConfig($this->wx_config["appId"], $this->wx_config["appSecret"]);

        $this->Achievement = $this->loader->model('AchievementModel', $this);
        $this->Achievement->table = '';

        $this->accessKey = $this->config->get('qiniu.accessKey');
        $this->secrectKey = $this->config->get('qiniu.secrectKey');
        $this->bucket = $this->config->get('qiniu.bucket');
    }


    /**
     * showdoc
     * @catalog API文档/公共API/文件相关
     * @title 上传单张图片到七牛云
     * @description 上传单张图片到七牛云，上传前进行图片安全检测（小程序安全接口）
     * @method POST
     * @url Common/Common/upload
     * @param file 必选 string 二进制文件
     * @return {"code": 1000,"message": "success","data": {"url": "201810181014167784_TB2yJEHDgaTBuNjSszfXXXgfpXa_!!666940545.jpg_60x60q90.jpg","all_url": "https:\/\/qn.youheone.com\/201810181014167784_TB2yJEHDgaTBuNjSszfXXXgfpXa_!!666940545.jpg_60x60q90.jpg"}}
     * @return_param url string 上传成功后图片部分路径
     * @return_param all_url string 上传成功后图片完整路径
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_upload()
    {
        // 构建鉴权对象 #需要填写你的 Access Key 和 Secret Key
        //$auth = new Auth($this->accessKey, $this->secrectKey);
        $auth = get_instance()->QiniuAuth;
        // 生成上传 Token
        $token = $auth->uploadToken($this->bucket);
//        $uploadMgr = new UploadManager();
        $uploadMgr = get_instance()->QiniuUploadManager;
        $filePath = $this->request->files; //接收file文件

        if (empty($filePath) || empty($filePath['file'] ?? '')) {
            return $this->jsonend(-1001, '请选择上传图片');
        }
        $key = date('YmdHis') . rand(0, 9999) . '_' . $filePath['file']['name']; //上传后文件名称
        try {
            list($ret, $err) = $uploadMgr->putFile($token, $key, $filePath['file']['tmp_name']);
        } catch (\Exception $e) {
            return $this->jsonend(-1000, "上传失败" . $e->getMessage(), []);
        }
        if ($err !== null) {
            return $this->jsonend(-1000, 'fail');
        } else {
            //图片检测
            $client = 'user';
            if ($this->role == 5) {
                $client = 'engineers';
            }
//            $imgSec = $this->WxAuth->imgSecCheck($this->config->get('qiniu.qiniu_url') . $key, $client);
//            if ($imgSec['errcode'] != 0) {
//                return $this->jsonend(-1000, "图片里有违规内容");
//            }
            $data['url'] = $key;
            $data['all_url'] = $this->config->get('qiniu.qiniu_url') . $key;

            return $this->jsonend(1000, 'success', $data);
        }
    }

    /**
     * showdoc
     * @catalog API文档/公共API/短信相关
     * @title 发送短信
     * @description 发送短信验证码
     * @method POST
     * @url Common/Common/sendSms
     * @param tel 必选 string 手机号码
     * @return { "code": 1000, "message": "短信已发送到手机尾号：0418,验证码:875898，验证码24小时内有效-有效时间2018-10-19 10:19:41", "data": ""}
     * @remark 请输入合法手机号
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_sendSms()
    {
        if (empty($this->parm['tel'] ?? '')) {
            return $this->jsonend(-1001, "缺少参数手机号");
        }
        if (!isMobile($this->parm['tel'])) {
            return $this->jsonend(-1002, "手机号不合法");
        }
        $code = rand(100000, 999999);
        $arr['code'] = $code;
        $arr['expire'] = time() + 300;
        if ($this->app_debug == 1) {
            $arr['expire'] = time() + 3600 * 24;
            $message = '短信已发送到手机尾号：' . substr($this->parm['tel'], -4) . ',验证码:' . $code;
            //设置缓存
            CatCacheRpcProxy::getRpc()->offsetSet($this->parm['tel'], $arr);
        } else {
            $content = "验证码" . $code;
            $company_config = CompanyService::getCompanyConfig($this->company);
            $sms_config = $company_config['sms_config'];
            $send = SmsService::sendBaMiSms($this->parm['tel'], $content, $sms_config);
            if ($send['code'] != 1000) {
                return $this->jsonend(-1000, $send['msg']);
            }
            CatCacheRpcProxy::getRpc()->offsetSet($this->parm['tel'], $arr);
            $message = '短信已发送到手机尾号：' . substr($this->parm['tel'], -4) . ',5分钟内有效';
        }
        return $this->jsonend(1000, $message, ($this->app_debug == 1) ? $code : '');
    }





    /**
     * showdoc
     * @catalog API文档/公共API/工单相关
     * @title  获取业务类型
     * @description  获取业务类型信息，如果传工单ID，可根据工单计算费用信息
     * @method POST
     * @url Common/Common/getBussinessType
     * @param type 可选 int 类型 1售后 2 增值任务(催费,签约等) 3新装
     * @param work_order_id 可选 int 工单
     * @return {"code": 1000,"message": "获取成功", "data": [ {"business_type_id": "3","name": "新装","icon": "2677120180911131537287.png","rental_cost": "200","buy_cost": "600","status": "1","sort": "2","type": "3","is_sign": "1","create_time": null,"update_time": null }}]
     * @return_param business_type_id int 业务类型ID
     * @return_param name string 业务类型名称
     * @return_param icon string 图片
     * @return_param rental_cost int 租赁类型支付金额，单位分
     * @return_param buy_cost int 购买类型支付金额，单位分
     * @return_param cost int 根据工单计算支付金额，单位分
     * @return_param cost_unit string 根据工单计算支付金额，单位元
     * @return_param other_cost int 根据工单计算其他费用，单位分
     * @return_param other_cost_unit string 根据工单计算其他费用，单位元
     * @return_param other_cost_desc string 其他费用说明（当前主要是合同续费金额）
     * @remark {"work_order_id":8}
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_getBussinessType()
    {
        if (!empty($this->parm['type'] ?? '')) {
            $map['type'] = $this->parm['type'];
        }
        $map['status'] = 1;
        $map['business_type_id'] = ['NOT IN', [8, 9]];
        $map['company_id'] = $this->company;
        $data = $this->bus_type_model->getAll($map, '*');
        if (!empty($data)) {
            if (!empty($this->parm['work_order_id'] ?? '')) {
                $eq_info = $this->getWorkorderRange($this->parm['work_order_id']);
                //获取工单信息
                $work_info = $this->work_order_model->getWorkDetail(array('work_order_id' => $this->parm['work_order_id']), array(), 'work_order_status,user_id,repair_id,contract_id,replacement_type');
                foreach ($data as $k => $v) {
                    $data[$k]['icon'] = empty($v['icon']) ? '' : $this->config->get('qiniu.qiniu_url') . $v['icon'];
                    $data[$k]['cost'] = $this->baseServiceCost($v, $eq_info);
                    $data[$k]['cost_unit'] = formatMoney($this->baseServiceCost($v, $eq_info));
                    $data[$k]['other_cost'] = 0;
                    $data[$k]['other_cost_unit'] = formatMoney(0, 1, 2);
                    $data[$k]['other_cost_desc'] = '';
                    //如果是催费,查询合同费用
                    if ($v['business_type_id'] == 1 && !empty($work_info['contract_id'])) {
                        $contract_info = $this->contract_model->getOne(array('contract_id' => $work_info['contract_id']), 'renew_money');
                        if (empty($contract_info)) {
                            return $this->jsonend(-1101, "数据错误");
                        }
                        $renew_money = formatMoney($contract_info['renew_money'], 1, 2) * 100;
                        $data[$k]['other_cost'] = $renew_money * 100;
                        $data[$k]['other_cost_unit'] = formatMoney($renew_money * 100, 1, 2);
                        $data[$k]['other_cost_desc'] = '合同续费金额';
                    }
                    //若是移机
                    if ($v['business_type_id'] == 5) {
                        //获取用户免费移机次数
                        $this->Achievement->table = 'customer';
                        $user = $this->Achievement->findData(['user_id' => $work_info['user_id']], 'replacement_number');
                        if (!empty($user) && $user['replacement_number'] <= 0) {
                            $data[$k]['cost_unit'] = 0;
                            $data[$k]['cost'] = 0;
                        } elseif ($work_info['replacement_type'] == 3) {
                            $data[$k]['cost_unit'] = 0;
                            $data[$k]['cost'] = 0;
                        }
                    }
                }
            }
            return $this->jsonend(1000, "获取成功", $data);
        }
        return $this->jsonend(-1003, "暂无相关数据");
    }

    /**
     * showdoc
     * @catalog API文档/公共API/工单相关
     * @title  获取额外服务列表
     * @description  获取额外服务列表,支持分页
     * @method POST
     * @url Common/Common/getServiceList
     * @param page 可选 int 页数，默认1
     * @param pagesize 可选 int 每页条数，默认10
     * @param work_order_id 可选 int 工单ID
     * @return {"code": 1000, "message": "获取额外服务成功", "data": [{ "service_id": "6","service_name": "cehsi", "service_pic": "https:\/\/qn.youheone.com\/c0607201809261740588779.jpg","service_rental_cost": "20000","service_buy_cost": "28000", "create_time": "1537954862", "update_time": "1538122557","cost": 20000,"cost_unit": "200.00"}]}
     * @return_param service_id int 服务ID
     * @return_param service_name string 服务名称
     * @return_param service_pic string 服务图片
     * @return_param service_rental_cost int 租赁类型费用，单位分
     * @return_param service_buy_cost int 购买类型费用，单位分
     * @return_param cost int 若传工单ID，根据工单计算出的费用，单位分
     * @return_param cost_unit string 若传工单ID，根据工单计算出的费用，单位元
     * @remark {"work_order_id":8}
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_getServiceList()
    {
        $page = empty($this->parm['page'] ?? '') ? 1 : $this->parm['page'];
        $pagesize = empty($this->parm['pagesize'] ?? '') ? 10 : $this->parm['pagesize'];
        $field = '*';
        $order = ['create_time' => 'DESC'];
        $result = $this->additionalServiceModel->getAll($field, $page, $pagesize, $order, ['company_id' => $this->company]);
        if ($result) {
            if (!empty($this->parm['work_order_id'] ?? '')) {
                $eq_info = $this->getWorkorderRange($this->parm['work_order_id']);
            }
            foreach ($result as $k => $v) {
                $result[$k]['service_pic'] = empty($v['service_pic']) ? '' : $this->config->get('qiniu.qiniu_url') . $v['service_pic'];
                $cost = $v['service_rental_cost'];
                $result[$k]['cost'] = $cost;
                $result[$k]['cost_unit'] = formatMoney($cost);
            }
            return $this->jsonend(1000, '获取额外服务成功', $result);
        }
        return $this->jsonend(-1000, '暂无额外服务', $result);
    }

    //根据工单ID获取工单类型以及保修时间
    function getWorkorderRange($work_order_id)
    {
        $work_eq_join = [
            ['equipment_lists el', 'el.equipment_id = rq_work_equipment.equipment_id', 'left join'],
        ];
        $equipment_info = $this->work_eq_model->getAll(array('rq_work_equipment.work_order_id' => $work_order_id), 'start_time,end_time', ['rq_work_equipment.create_time' => 'DESC'], $work_eq_join);
        if (empty($equipment_info)) {
            return false;
        }
        return $equipment_info[0];
    }

    /**
     * showdoc
     * @catalog API文档/公共API/工单相关
     * @title 获取预约时间范围
     * @description 获取预约时间范围--后台配置
     * @method POST
     * @url Common/Common/getRangeTime
     * @return {"code": 1000, "message": "获取成功", "data": { "time": [ {"time_id": "1","start": "9:00", "end": "10:00", "status": "2", "sort": "1","format": "9:00-10:00"} ],"date": { "start": "2018-10-19","end": "2018-10-25"} }}
     * @return_param time array 时间段列表（time_id时间ID,start开始时间点，end结束时间点，format格式化后时间段）
     * @return_param date array 日期段（start开始日期,end结束日期）
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_getRangeTime()
    {
        $map['status'] = 2;
        $data = $this->time_model->getAll($map);
        if (!empty($data)) {
            foreach ($data as $k => $v) {
                $data[$k]['format'] = $v['start'] . '-' . $v['end'];
            }
        }
        $list['time'] = $data;
        $start = date('Y-m-d');
        //预约可选择时间7天内
        $end = date('Y-m-d', strtotime('+7 days'));
        $list['date'] = array('start' => $start, 'end' => $end);
        //如果是下午18点后预约只能预约第二天下午的,18点前可预约第二天早上的
        $hour = date('H', time());
        $limit['is_later'] = false;
        $limit['desc'] = '下午18点后预约最早可预约次日下午,18点前可预约次日上午';
        if ($hour >= 18) {
            $limit['is_later'] = true;
            $limit['tomorrow_date'] = date('Y-m-d', strtotime('+1 days')); //第二天日期
            $limit['time_id'] = [3, 4]; //时间点开始ID
        }
        $list['limit'] = $limit;
        return $this->jsonend(1000, "获取成功", $list);
    }

    /**
     * showdoc
     * @catalog API文档/公共API/工单相关
     * @title 获取退单原因列表
     * @description
     * @method POST
     * @url Common/Common/getReturnReason
     * @return {"code": 1000,"message": "获取成功", "data": {"1": "时间问题", "2": "无法完成", "3": "距离太远", "4": "其他" }}
     * @return_param date array (键值对)
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_getReturnReason()
    {
        return $this->jsonend(1000, "获取成功", $this->config->get('return_reason'));
    }

    /**
     * showdoc
     * @catalog API文档/公共API/工单相关
     * @title 获取售后主要问题列表
     * @description 可以根据业务类型查询
     * @method POST
     * @url Common/Common/getProblemList
     * @param business_id 可选 int 业务类型ID
     * @return {"code": 1000,"message": "获取列表成功","data": [ {"problem_id": "1", "business_id": "6","problem_desc": "漏水","sort": "0","create_time": "1536646637","is_del": "2" }]}
     * @return_param problem_id int 问题ID
     * @return_param business_id int 业务类型ID
     * @return_param problem_desc string 问题描述
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_getProblemList()
    {
        $map = array();
        if (!empty($this->parm['business_id'] ?? '')) {
            $map['business_id'] = ['IN', [0, $this->parm['business_id']]];
        }
        $data = $this->work_order_problem_model->getList($map, '*', array('sort' => 'ASC'));
        if (!empty($data)) {
            return $this->jsonend(1000, '获取列表成功', $data);
        }
        return $this->jsonend(-1003, '暂无相关数据');
    }

    /**
     * showdoc
     * @catalog API文档/公共API/配置相关
     * @title 获取系统配置
     * @description 根据Key获取系统配置
     * @method POST
     * @url Common/Common/getSystemConfig
     * @param key 必选 string 配置key
     * @return {"code": 1000, "message": "获取成功", "data": "2"}
     * @remark {"key":"sign_way"}
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_getSystemConfig()
    {
        if (empty($this->parm)) {
            return $this->jsonend(-1001, "缺少参数");
        }
        //如果是合同签署方式或者实名实名审核方式,根据用户所属运营中心查找
        $operation_id = 0;
        if (!empty($this->parm['user_id'])) {
            //获取配置
            $customer_administrative_center = $this->customer_administrative_center_model->getOne(['user_id' => $this->parm['user_id']], 'operation_id');
            $operation_id = empty($customer_administrative_center) ? 0 : $customer_administrative_center['operation_id'];
        }
        $data = ConfigService::getConfig($this->parm['key'], true, $this->company, $operation_id);
        if (empty($data)) {
            return $this->jsonend(-1003, "没有相关配置");
        }
        return $this->jsonend(1000, "获取成功", $data);
    }

    /**
     * 获取结算配置参数
     * 请求参数     请求类型      是否必须      说明
     * key          string          是           配置key
     * @param string $key
     * @date 2019/2/14 10:18
     * @author ligang
     */
    public function http_getBalanceConfig()
    {
        $key = $this->parm['key'] ?? '';
        if (empty($key)) {
            $this->jsonend(-1000, '缺少参数');
        }
        $result = ConfigService::getBalanceConfig('', trim($key), $this->company, 2);
        return $this->jsonend(1000, '获取成功', $result);
    }


    /**
     * showdoc
     * @catalog API文档/公共API/配置相关
     * @title 获取服务端配置
     * @description 获取服务端配置
     * @method POST
     * @param key string key  scene_type场景类型；cooker_type炉具类型；cooker_core_type 灶芯类型switch_machine开关机；status物联网状态；
     * @url Common/Common/getServiceConfig
     * @return {"code": 1000,"message": "获取数据成功","data": {"scene_type": {"1": "场景一","2": "场景二"}}}
     * Author: Xuleni
     * DateTime: 2020/9/8 14:23
     */
    public function http_getServiceConfig()
    {
        if (empty($this->parm['key'])) return $this->jsonend(-1000, '参数缺少');
        $result = $this->config->get($this->parm['key']);
        if (empty($result)) {
            return $this->jsonend(-1000, '暂无相关数据');
        }
        return $this->jsonend(1000, '获取成功', $result);
    }


    /**
     * showdoc
     * Author: Xuleni
     * DateTime: 2020/11/18 16:13
     * @catalog
     * @title 灶具信息
     * @description
     * @method POST
     * @url
     */
    public function http_getStoveConfig()
    {
        $result = $this->stoveManagementModel->getAll(['company_id' => $this->company, 'is_delete' => 0], 'id,name,water_quantity,price');
        return $this->jsonend(1000, '获取成功', $result);
    }


    /**
     * 获取认证类型
     * @desc 描述
     * @author tx
     * @RequestType POST
     * @date 2019/4/12 10:59
     */
    public function http_getAuthType()
    {
        $auth_method = $this->config->get('auth_type');
        if (empty($auth_method)) {
            return $this->jsonend(-1000, '暂无相关数据');
        }
        return $this->jsonend(1000, '获取成功', $auth_method);
    }


    /**
     * showdoc
     * @catalog API文档/公共API/配置相关
     * @title 获取搜索选项配置
     * @description
     * @method POST
     * @url Common/Common/getSearchConfig
     * @param config_name 可选 string 客户管理:auth_type认证类型realname_auth_status实名认证状态,财务管理:finance_type支付类型finance_pay_way支付方式finance_check_status对账状态,工单管理:work_order_visit_status回访状态work_order_status工单状态,主板管理:device_status主板上报状态equipmentlist_status主板在线离线状态device_type主板类型screen_mode屏幕显示模式
     * @return {"code":1000,"message":"获取客户成功","data":[{"user_id":"15","account":""}]}
     * @return_param user_id int 用户id
     * @remark
     * @number 0
     * @author tx
     * @date 2020/4/28
     */
    public function http_getSearchConfig()
    {
        $value = $this->parm['config_name'] ?? '';
        if (empty($value)) {
            return $this->jsonend(-1000, '缺少配置参数名称');
        }
        $config = $this->config->get($value);
        if (empty($config)) {
            return $this->jsonend(-1000, '暂无数据');
        }
        $i = 0;
        foreach ($config as $k => $v) {
            $data[$i]['id'] = $k;
            $data[$i]['name'] = $v;
            $i++;
        }
        return $this->jsonend(1000, '获取成功', $data);
    }

    /**
     * showdoc
     * @catalog API文档/公共API/配置相关
     * @title 获取服务场景
     * @description 系统配置下拉选项
     * @method POST
     * @param
     * @url Common/Common/appointmentConfiguration
     * @return {"code": 1000,"message": "获取数据成功","data": {"scene_type": {"1": "场景一","2": "场景二"},"cooker_type": {"1": "炉具一","2": "炉具二"}}}
     * Author: Xuleni
     * DateTime: 2020/9/8 14:23
     */
    public function http_appointmentConfiguration()
    {
        //服务场景
        $config['scene_type'] = $this->config->get('scene_type');
        return $this->jsonend(1000, '获取数据成功', $config);
    }

    /**
     * showdoc
     * @catalog API文档/公共API/配置相关
     * @title 获取对公账户信息
     * @description
     * @method POST
     * @param
     * @url Common/Common/companyAccountConfig
     * @return
     * @return_param public_account_company_name string 公司名称
     * @return_param public_account_number string 公司对公账户
     * @return_param public_account_open_bank string 公司开户行
     * Author: lcx
     * DateTime: 2021/03/24
     */
    public function http_companyAccountConfig()
    {
        $data['public_account_company_name'] = ConfigService::getConfig('public_account_company_name');
        $data['public_account_number'] = ConfigService::getConfig('public_account_number');
        $data['public_account_open_bank'] = ConfigService::getConfig('public_account_open_bank');
        return $this->jsonend(1000, '获取数据成功', $data);
    }

    /**
     * showdoc
     * @catalog API文档/公共API/配置相关
     * @title 手动出账
     * @description
     * @method POST
     * @param
     * @url Common/Common/handOutBill
     * @return
     * @return_param customer_code string 户号
     * Author: lcx
     * DateTime: 2021/03/24
     */
    public function http_handOutBill()
    {
        if (empty($this->parm['customer_code'])) {
            return $this->jsonend(-1000, '缺少参数customer_code');
        }
        $result = $this->customerCodeBillModel->outBill($this->parm['customer_code'], 2);
        return $this->jsonend($result['code'], $result['msg']);
    }
    /**
     * showdoc
     * @catalog API文档/公共API/报单相关
     * @title 获取讲解人
     * @description
     * @method POST
     * @param
     * @url Common/Common/getExplainers
     * @return
     * @return_param 
     * Author: lcx
     * DateTime: 2022/03/24
     */
    public function http_getExplainers(){
          //市场推广
          $dealers = $this->customerModel->getAll(['is_dealer' => 1, 'is_delete' => 0, 'user_belong_to_company' => $this->company], 'user_id,realname,username,nickname,telphone');
          $dealer_arr = [];
          foreach ($dealers as $key => $value) {
              $dealer['id'] = 'D-' . $value['user_id'];
              $dealer['name'] = empty($value['realname']) ? urldecode($value['nickname']) : $value['realname'];
              $dealer['phone'] = $value['telphone'];
              $dealer['role'] = '市场推广';
              $dealer_arr[] = $dealer;
          }
          //工程人员
          $join = [
              ['administrative_info ai', 'ai.a_id=a.administrative_id', 'left']
          ];
          $engineers = $this->engineersModel->getAll([ 'ai.company_id' => $this->company], $join,  1, -1,'engineers_id,engineers_name,engineers_phone',['engineers_id'=>'ASC']);
          $engineer_arr = [];
          foreach ($engineers as $key => $value) {
              $engineer['id'] = 'E-' . $value['engineers_id'];
              $engineer['name'] = $value['engineers_name'];
              $engineer['phone'] = $value['engineers_phone'];
              $engineer['role'] = '工程';
              $engineer_arr[] = $engineer;
          }
          $explainer = array_merge($dealer_arr, $engineer_arr);//将市场推广和工程人员合并
          return $this->jsonend(1000, "获取成功",$explainer);
    }


}
