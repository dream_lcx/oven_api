<?php


namespace app\Controllers\Common;


use app\Services\Company\CompanyService;

class Company extends Base
{
    // 前置方法，加载模型
    public function initialization($controller_name, $method_name)
    {
        if ($this->response !== null && $this->request_type == 'http_request') {
            $this->http_output->setHeader('Access-Control-Allow-Origin', '*');
            $this->http_output->setHeader("Access-Control-Allow-Headers", " Origin, X-Requested-With, Content-Type, Accept,token");
            $this->http_output->setHeader("Access-Control-Allow-Methods", " POST");
        }
        parent::initialization($controller_name, $method_name);
    }

    /**
     * showdoc
     * @catalog API文档/公共API/公司相关
     * @title 获取公司信息
     * @description
     * @method POST
     * @url /Common/Company/getCompanyInfo
     * @param company_id 可选 int 公司ID
     * @return
     * @remark
     * @number 0
     * @author lcx
     * @date 2021-2-23
     */
    public function http_getCompanyInfo()
    {
        if (empty($this->parm['company_id'])) {
            return $this->jsonend(-1000, "缺少参数company_id");
        }
        $data = CompanyService::getCompanyInfo($this->parm['company_id']);
        if (!$data) {
            return $this->jsonend(-1000, "公司信息不存在");
        }
        return $this->jsonend(1000, "获取成功", $data);

    }
    /**
     * showdoc
     * @catalog API文档/公共API/公司相关
     * @title 获取公司配置信息
     * @description
     * @method POST
     * @url /Common/Company/getCompanyConfig
     * @param company_id 可选 int 公司ID
     * @return
     * @remark
     * @number 0
     * @author lcx
     * @date 2021-2-23
     */
    public function http_getCompanyConfig()
    {
        if (empty($this->parm['company_id'])) {
            return $this->jsonend(-1000, "缺少参数company_id");
        }
        $data = CompanyService::getCompanyConfig($this->parm['company_id']);
        if (!$data) {
            return $this->jsonend(-1000, "公司信息不存在");
        }
        return $this->jsonend(1000, "获取成功", $data);

    }
}