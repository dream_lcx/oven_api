<?php

namespace app\Controllers\Common;


/**
 * 系统配置信息
 *
 * @author lcx
 */
class Conf extends Base {

    protected $timed_model;

    // 前置方法,加载模型
    public function initialization($controller_name, $method_name) {
        parent::initialization($controller_name, $method_name);
        $this->timed_model = $this->loader->model('EquipmentTimedFlushConfigModel', $this);
    }

    /**
    * showdoc
    * @catalog API文档/公共API/主板相关
    * @title 定时冲洗时间配置
    * @description 通过后台配置
    * @method POST
    * @url Common/Conf/timedFlush
    * @return {"code": 1000,"message": "获取数据成功","data": [{"value": "2", "description": "2小时冲洗一次" }]}
    * @return_param value int 时长
    * @return_param description string 描述
    * @remark 
    * @number 0
    * @author lcx
    * @date 2018-10-18
    */
    public function http_timedFlush() {
        $map['status'] = 1;
        $field = 'value,description';
        $data = $this->timed_model->getAll($map,$field);
        if(empty($data)){
            return $this->jsonend(-1003,"暂无相关数据");
        }
        foreach ($data as $key=>$value){
            $data[$key]['value'] = intval($value['value']);
        }
        return $this->jsonend(1000,"获取数据成功",$data);
    }
}
