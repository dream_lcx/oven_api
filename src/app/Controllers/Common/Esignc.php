<?php

namespace app\Controllers\Common;

use app\Library\SLog;
use app\Services\Common\UploadService;
use app\Services\Company\CompanyService;
use app\Services\Contract\ContractService;
use app\Services\Log\FileLogService;
use app\Tasks\AppTask;
use app\Services\Common\HttpService;

/**
 * e签宝--签名
 *
 * @author lcx
 */
class Esignc extends Base
{

    protected $sign;
    protected $accountId;
    protected $contract_model;
    protected $auth_model;
    protected $user_model;
    protected $contract_ep_model;
    protected $parts_model;
    protected $ep_part_model;
    protected $bind_model;
    protected $company_model;
    protected $contract_temp_model;
    protected $operation_info_model;
    protected $customer_administrative_center_model;
    protected $oaWorkersContractModel;

    //put your code here
    public function initialization($controller_name, $method_name)
    {
        parent::initialization($controller_name, $method_name);
        $this->oaWorkersContractModel = $this->loader->model('OaWorkersContractModel', $this);
        $this->contract_model = $this->loader->model('ContractModel', $this);
        $this->auth_model = $this->loader->model('CustomerAuthModel', $this);
        $this->user_model = $this->loader->model('CustomerModel', $this);
        $this->contract_ep_model = $this->loader->model('ContractEquipmentModel', $this);
        $this->parts_model = $this->loader->model('PartsModel', $this);
        $this->ep_part_model = $this->loader->model('EquipmentsPartsModel', $this);
        $this->bind_model = $this->loader->model('CustomerBindEquipmentModel', $this);
        $this->company_model = $this->loader->model('CompanyModel', $this);
        $this->contract_temp_model = $this->loader->model('ContractTemplateModel', $this);
        $this->operation_info_model = $this->loader->model('OperationInfoModel', $this);
        $this->customer_administrative_center_model = $this->loader->model('CustomerAdministrativeCenterModel', $this);
        $this->accessKey = $this->config->get('qiniu.accessKey');
        $this->secrectKey = $this->config->get('qiniu.secrectKey');
        $this->bucket = $this->config->get('qiniu.bucket');
        try {
            require_once APP_DIR . '/Library/eSign/API/eSignOpenAPI.php';

            //$this->sign = new eSign();
            $this->sign = get_instance()->eSign;
            $iRet = $this->sign->init();
            if ($iRet != 0) {
                echo "初始化失败";
            } else {
                echo "初始化成功";
                $this->reg();
            }
        } catch (\Exception $e) {
            return $this->jsonend(-1000, "服务异常" . $e->getMessage());
        }


    }


//      public function __construct() {
//      parent::__construct();
//      require_once APP_DIR . '/eSign/API/eSignOpenAPI.php';
//
//      }

    //注册账户
    public function reg()
    {
        //获取e签宝配置
        // $e_config = $this->config['esign'];
        $e_config = $this->esign_config;
        $data = $this->sign->addOrganizeAccount($e_config['mobile'], $e_config['name'], $e_config['organCode'], $e_config['regType'], $e_config['email'], $e_config['organType'], $e_config['legalArea'], $e_config['userType'], $e_config['agentName'], $e_config['agentIdNo'], $e_config['legalName'], $e_config['legalIdNo']
        );
        if ($data['errCode'] == 0) {
            $this->accountId = $data['accountId'];
        } else {
            var_dump($data);
            return $this->http_output->end('注册失败');
        }
    }
    //注册个人账户
    public function regPersonAccount($mobile,$name,$idNo){
        $data = $this->sign->addPersonAccount($mobile, $name, $idNo);
        if ($data['errCode'] == 0) {
            return $data['accountId'];
        } else {
            var_dump($data);
            return false;
        }
    }

    /**
     * showdoc
     * @catalog API文档/公共API/合同相关
     * @title 生成印章-当前企业
     * @description
     * @method POST
     * @url /Common/Esignc/yz
     * @param seal_code 可选 string 防伪码
     * @return
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-12-5
     */
    public function http_yz()
    {
        $seal_code = $this->parm['seal_code'] ?? '5001127106712';//防伪码
        $Official_seal = $this->sign->addTemplateSeal($this->accountId, 'star', 'red', '', $seal_code);
        if ($Official_seal['errCode'] == 0) {
            $data = UploadService::upload($Official_seal['imageBase64'], '', 'pdf/seal/', 'qiniu', 'base64');
            return $this->jsonend(1000, "生成成功", $data['data']);
        } else {
            return $this->jsonend(-1000, "生成失败");
        }

    }

    //生成企业印章
    public function entSeal($seal_code = '5001035044983')
    {
        $Official_seal = $this->sign->addTemplateSeal($this->accountId, 'star', 'red', '', $seal_code);
        return $Official_seal;
    }

    //新增生成企业账户并生成印章
    public function http_regSeal()
    {
        //接收参数
        $name = $this->parm['name'] ?? '';
        $organCode = $this->parm['organCode'] ?? '';
        $regType = $this->parm['regType'] ?? '';
        $mobile = $this->parm['mobile'] ?? '';
        $email = $this->parm['email'] ?? '';
        $organType = $this->parm['organType'] ?? '';
        $userType = $this->parm['userType'] ?? '';
        $seal_code = $this->parm['seal_code'] ?? '5001127106712';
        $agentName = $this->parm['agentName'] ?? '';
        $agentIdNo = $this->parm['agentIdNo'] ?? '';
        $legalName = $this->parm['legalName'] ?? '';
        $legalArea = $this->parm['legalArea'] ?? '';
        $legalIdNo = $this->parm['legalIdNo'] ?? '';
        $data = $this->sign->addOrganizeAccount($mobile, $name, $organCode, $regType, $email, $organType, $legalArea, $userType, $agentName, $agentIdNo, $legalName, $legalIdNo);
        if ($data['errCode'] == 0) {
            $this->accountId = $data['accountId'];
        } else {
            var_dump($data);
            return $this->jsonend(-1000, $data['msg']);
        }
        //生成印章
        $Official_seal = $this->sign->addTemplateSeal($this->accountId, 'star', 'red', '', $seal_code);
        if ($Official_seal['errCode'] == 0) {
            $data = UploadService::upload($Official_seal['imageBase64'], '', 'pdf/seal/', 'qiniu', 'base64');
            return $this->jsonend(1000, "生成成功", $data['data']);
        } else {
            var_dump($Official_seal);
            return $this->jsonend(-1000, $Official_seal['msg']);
        }
    }

    /**
     * showdoc
     * @catalog API文档/公共API/合同相关
     * @title 电子签名
     * @description
     * @method POST
     * @url Common/Esignc/pdf
     * @param contract_id 必选 int 合同ID
     * @return { "code": 1000,"message": "签署成功", "data": ""}
     * @remark {"contract_id":16}
     * @number 0
     * @author lcx
     * @date 2018-10-18
     */
    public function http_pdf()
    {
        file_exists(WWW_DIR . '/pdf/PDF_Generate/') || mkdir(WWW_DIR . '/pdf/PDF_Generate/', 0777);
        file_exists(WWW_DIR . '/pdf/img_yz/') || mkdir(WWW_DIR . '/pdf/img_yz/', 0777);
        file_exists(WWW_DIR . '/pdf/PDF_Company/') || mkdir(WWW_DIR . '/pdf/PDF_Company/', 0777);
        file_exists(WWW_DIR . '/pdf/PDF_User/') || mkdir(WWW_DIR . '/pdf/PDF_User/', 0777);
        $this->parm = $this->http_input->getAllPostGet();
        //读取合同信息
        $contract_id = $this->parm['contract_id'];

        $contract_info = $this->getContractInfo($contract_id);
        if (empty($contract_info)) {
            echo "合同ID:" . $contract_id . '未找到合同信息';
            return false;
        }
        //处理原始配件
        $original_parts = '';
        if (!empty($contract_info['original_parts'])) {
            $original_parts_arr = [];
            foreach ($contract_info['original_parts'] as $k => $v) {
                array_push($original_parts_arr, $v['parts_name']);
            }
            if (!empty($original_parts_arr)) {
                $original_parts = implode(',', $original_parts_arr);
            }
        }
        //处理主板信息
        $eq_info = '';
        if (!empty($contract_info['equipments_info']['list'])) {
            $eq_arr = [];
            foreach ($contract_info['equipments_info']['list'] as $k => $v) {
                //   $str = $v['equipments_name'] . '(编号:' . $v['equipments_number'] . ')';
                $str = $v['equipments_name'];
                array_push($eq_arr, $str);
            }
            if (!empty($eq_arr)) {
                $eq_info = implode(',', $eq_arr);
            }
        }
        $date = date('Ymd');
        $pdf_path = WWW_DIR . '/pdf/PDF_Generate/';
        $pdf_file = $date . '_' . $contract_info['contract_info']['contract_no'] . '_' . date('s') . '.pdf';
        //查询合同模板
        $temp_pdf = 'rental_contract_temp.pdf';
        $temp_info = ContractService::getTemplate($contract_info['contract_info']['company_id'], $contract_info['contract_info']['business_id'], 1, $contract_info['contract_info']['operation_id']);
        if ($temp_info) {
            $temp_pdf = $temp_info['local_path'];
        }
        FileLogService::WriteLog(SLog::DEBUG_LOG, 'esign', "模板" . json_encode($temp_info));

        //合同模板存在七牛云，由于e签宝服务暂只支持本地文档生成pdf,若合同模板在本地服务器不存在，从七牛云下载
        if (!file_exists(WWW_DIR . '/pdf/PDF_Template/' . $temp_pdf)) {
            $downResult = downloadOriginFile($this->config->get('qiniu.qiniu_url') . $temp_pdf, WWW_DIR . '/pdf/PDF_Template', $temp_pdf);
            if (!$downResult) {
                return $this->jsonend(-1000, "从远程服务器下载合同模板失败,请重试");
            }
            chmod($downResult['save_path'], 0777);
        }
        $tmpFile = array(
            'srcFileUrl' => WWW_DIR . '/pdf/PDF_Template/' . $temp_pdf,
            'dstFileUrl' => $pdf_path . $pdf_file
        );
        //房屋性质
        $txtFields = array(
            'nailUserName' => $contract_info['rental_user_info']['realname'],
            'nailIdentificationNumber' => $contract_info['rental_user_info']['id_number'],
            'nailAddress' => $contract_info['contract_info']['province'] . $contract_info['contract_info']['city'] . $contract_info['contract_info']['area'],
            'nailAddressDetail' => $contract_info['contract_info']['address'],
            'nailUrgentTel' => empty($contract_info['rental_user_info']['emergency_contact']) ? '' : $contract_info['rental_user_info']['emergency_contact']['tel'],//紧急联系电话
            'nailEmail' => '',//邮箱
            'nailTel' => $contract_info['rental_user_info']['telphone'],
            'BUserName' => $contract_info['company_info']['company_name'],
            'BIdentificationNumber' => $contract_info['company_info']['business_license_pic'],
            'BAddress' => $contract_info['company_info']['company_addr'],
            'BTel' => $contract_info['company_info']['telphone'],
            'BEmail' => $contract_info['company_info']['email'] ?? '',
            'equipment' => $eq_info,
            'equipmentType' => 'RO反渗透直饮机',//主板型号
            'installProvince' => $contract_info['contract_info']['province'],//安装地址省份
            'installCity' => $contract_info['contract_info']['city'],//安装地址城市
            'installArea' => $contract_info['contract_info']['area'],//安装地址区域
            'installAddress' => $contract_info['contract_info']['address'],//安装地址详细地址
            'contractNumber' => $contract_info['contract_info']['contract_no'],
            'contractMoney' => $contract_info['contract_info']['contract_money'],
            'parts' => $original_parts,
            'eqNum' => $contract_info['equipments_info']['total_num'],
            'AY' => date('Y'),
            'AM' => date('m'),
            'AD' => date('d'),
            'BY' => date('Y'),
            'BM' => date('m'),
            'BD' => date('d'),
            'originMoney' => $contract_info['equipments_info']['list'][0]['equipments_orgin_money'],
            'capOriginMoney' => num_to_rmb($contract_info['equipments_info']['list'][0]['equipments_orgin_money']),
            'installProperty' => '家用机',
            'cycle' => '',
            'eqRemarks' => '',
            'waterPressure' => '',
            'twoContractMoney' => '',
            'renewMoney' => 0,
            'own_house' => false,
            'rental_house' => false,
            'startY' => date('Y', strtotime($contract_info['contract_info']['contract_time_start_format'])),//合同开始
            'startM' => date('m', strtotime($contract_info['contract_info']['contract_time_start_format'])),//合同开始
            'startD' => date('d', strtotime($contract_info['contract_info']['contract_time_start_format'])),//合同开始
            'endY' => date('Y', strtotime($contract_info['contract_info']['contract_time_end_format'])),//合同到期
            'endM' => date('m', strtotime($contract_info['contract_info']['contract_time_end_format'])),//合同到期
            'endD' => date('d', strtotime($contract_info['contract_info']['contract_time_end_format'])),//合同到期
            'signTime' => date('Y-m-d'),//签约时间
            'signRepresent' => ''//签约代表
        );

        //租赁合同 有效期
        // if ($contract_info['contract_info']['contract_class'] == 1) {
        $txtFields['leaseYear'] = 1;
        $txtFields['startY'] = $contract_info['contract_info']['contract_time_start'][0];
        $txtFields['startM'] = $contract_info['contract_info']['contract_time_start'][1];
        $txtFields['startD'] = $contract_info['contract_info']['contract_time_start'][2];
        $txtFields['endY'] = $contract_info['contract_info']['contract_time_end'][0];
        $txtFields['endM'] = $contract_info['contract_info']['contract_time_end'][1];
        $txtFields['endD'] = $contract_info['contract_info']['contract_time_end'][2];
        // }

        $data = $this->sign->createFromTemplate($tmpFile, false, $txtFields, false);

        if ($data['errCode'] == 0) {
            $company_sign = true;
            if ($contract_info['company_info']['need_company_sign']) {
                $company_sign = $this->ptqs($pdf_file, $contract_info['company_info']['seal_code'], $temp_info['company_sign_postion_config']);
            }
            if ($company_sign) {
                $file = $this->request->files['file'];
                $imgname = date('YmdHis') . $contract_info['contract_info']['contract_no'] . time() . '_' . rand(0, 9999) . substr($file['name'], strrpos($file['name'], '.')); //上传后文件名称
                $tmp = $file['tmp_name'];
                $filepath = WWW_DIR . '/pdf/img_yz/';
                if (!move_uploaded_file($tmp, $filepath . $imgname)) {
                    return $this->jsonend(-1001, '图片上传失败');
                }

                $sign_file = $filepath . $imgname;
                //如果是个人用户签合同的签名需要调整图片放向，企业用户和政事用户签合同的签章不需要
                $imgturn = true;
                if ($contract_info['rental_user_info']['user_class'] != 1) {
                    $imgturn = false;
                }
                $accountId = $this->regPersonAccount($contract_info['rental_user_info']['telphone'],$contract_info['rental_user_info']['realname'],$contract_info['rental_user_info']['id_number']);
                $user_sign_status = $this->userSign($pdf_file, $sign_file, $imgturn, $temp_info['user_sign_postion_config'], $contract_info['company_info']['need_company_sign'],$accountId);
                if ($user_sign_status) {
                    $verify_status = $this->fileVerify($pdf_file);
                    // if ($verify_status) {
                    $user_pdf_path = WWW_DIR . '/pdf/PDF_User/';
//                        $user_img_path = WWW_DIR . '/pdf/PDF_TO_IMG/';
                    // $img = pdf2png($user_pdf_path.$pdf_file,$user_img_path
                    //上传资源到七牛云
                    $AppTask = $this->loader->task(AppTask::class, $this);
                    $file_path = WWW_DIR . '/pdf/PDF_User/' . $pdf_file;
                    $AppTask->startTask('sysnUploadToQiniu', [file_get_contents($file_path), 'pdf/PDF_User/' . $pdf_file], -1, function ($serv, $task_id, $data) {
                    });
                    $file_path2 = WWW_DIR . '/pdf/img_yz/' . $imgname;
                    $AppTask->startTask('sysnUploadToQiniu', [file_get_contents($file_path2), 'pdf/img_yz/' . $imgname], -1, function ($serv, $task_id, $data) {
                    });
                    return $this->jsonend(1000, "签约成功", array('filename' => 'pdf/PDF_User/' . $pdf_file, 'esign_user_sign' => 'pdf/img_yz/' . $imgname));
                    //}
                    // return $this->jsonend(-1000, "验签失败");
                }
                return $this->jsonend(-1000, "用户签署失败");
            }
            return $this->jsonend(-1000, "公司印章失败");
        }
        FileLogService::WriteLog(SLog::DEBUG_LOG, 'esign', json_encode($data));
        return $this->jsonend(-1000, "签约失败");
    }

    /**
     * showdoc
     * @catalog API文档/公共API/合同相关
     * @title 市场推广电子签约合同
     * @description
     * @method POST
     * @url Common/Esignc/workerPdf
     * @param user_id int 用户id
     * @return { "code": 1000,"message": "签署成功", "data": ""}
     * @remark
     * @number 0
     * @author tx
     * @date 2020-10-13
     */

    public function http_workerPdf()
    {
        $local = WWW_DIR;
        //申请市场推广合同模板
        file_exists($local . '/pdf/PDF_Worker/') || mkdir($local . '/pdf/PDF_Worker/', 0777);
        file_exists($local . '/pdf/PDF_Generate/') || mkdir($local . '/pdf/PDF_Generate/', 0777);
        file_exists($local . '/pdf/img_yz/') || mkdir($local . '/pdf/img_yz/', 0777);
        file_exists($local . '/pdf/PDF_Company/') || mkdir($local . '/pdf/PDF_Company/', 0777);
        file_exists($local . '/pdf/PDF_User/') || mkdir($local . '/pdf/PDF_User/', 0777);

        $this->parm = $this->http_input->getAllPostGet();

        //查询用户信息
        $user_id = $this->parm['user_id'];
        $user_info = $this->user_model->getOne(['user_id' => $user_id]);
        if (empty($user_info)) {
            return $this->jsonend(-1000, "用户信息错误");
        }
        $join = [
            ['oa_workers', 'rq_oa_workers.workers_id=rq_oa_workers_contract.workers_id', 'left']
        ];
        $contract_info = $this->oaWorkersContractModel->getOne(['user_id' => $user_id], 'rq_oa_workers_contract.*,rq_oa_workers.position', $join);
        if (empty($contract_info)) {
            return $this->jsonend(-1000, "合同信息错误");
        }
        //查询公司信息
        $company_info = CompanyService::getCompanyInfo($user_info['user_belong_to_company']);//请求boss后台接口获取入驻商信息

        $date = date('Ymd');
        $pdf_path = $local . '/pdf/PDF_Generate/';
        $pdf_file = $date . '_' . $user_info['account'] . '_' . date('s') . '.pdf';
        //查询合同模板
        $temp_pdf = $contract_info['position'].'_hztuiguangtemplate_20210325.pdf';

        //合同模板存在七牛云，由于e签宝服务暂只支持本地文档生成pdf,若合同模板在本地服务器不存在，从七牛云下载
        if (!file_exists($local . '/pdf/PDF_Worker/' . $temp_pdf)) {
            $downResult = downloadOriginFile($this->config->get('qiniu.qiniu_url') . $temp_pdf, $local . '/pdf/PDF_Worker/', $temp_pdf);
            if (!$downResult) {
                return $this->jsonend(-1000, "从远程服务器下载合同模板失败,请重试");
            }
            chmod($downResult['save_path'], 0777);
        }
        $tmpFile = array(
            'srcFileUrl' => $local . '/pdf/PDF_Worker/' . $temp_pdf,
            'dstFileUrl' => $pdf_path . $pdf_file
        );

        //合同内容
        $txtFields = array(
            'contract_no' => $contract_info['contract_no'],
            'second_party_name' => $contract_info['second_party_name'] ?? '',
            'second_party_idcard' => $contract_info['second_party_idcard'] ?? '',
            'second_party_address' => $contract_info['second_party_address'] ?? '',
            'second_party_telphone' => $contract_info['second_party_telphone'] ?? '',
            'first_party_date_y' => date('Y'),
            'first_party_date_m' => date('m'),
            'first_party_date_d' => date('d'),
            'second_party_date_y' => date('Y'),
            'second_party_date_m' => date('m'),
            'second_party_date_d' => date('d'),
        );
        $data = $this->sign->createFromTemplate($tmpFile, false, $txtFields, false);
        if ($data['errCode'] == 0) {
            $company_sign = true;
            $company_info['need_company_sign'] = true;
            if ($company_info['need_company_sign']) {
                $company_sign = $this->ptqs($pdf_file, $company_info['seal_code'], '');
            }
            if ($company_sign) {
                $file = $this->request->files['file'];
                $imgname = date('YmdHis') . $contract_info['contract_no'] . '_' . rand(0, 9999) . substr($file['name'], strrpos($file['name'], '.')); //上传后文件名称
                $tmp = $file['tmp_name'];
                $filepath = $local . '/pdf/img_yz/';
                if (!move_uploaded_file($tmp, $filepath . $imgname)) {
                    return $this->jsonend(-1001, '图片上传失败');
                }

                $sign_file = $filepath . $imgname;
                //如果是个人用户签合同的签名需要调整图片放向，企业用户和政事用户签合同的签章不需要
                $imgturn = true;
                $accountId = $this->regPersonAccount($contract_info['second_party_telphone'],$contract_info['second_party_name'],$contract_info['second_party_idcard']);
                $user_sign_status = $this->userSign($pdf_file, $sign_file, $imgturn, '', $company_info['need_company_sign'],$accountId);
                if ($user_sign_status) {
                    $verify_status = $this->fileVerify($pdf_file);
                    $user_pdf_path = $local . '/pdf/PDF_User/';
                    //上传资源到七牛云
                    $AppTask = $this->loader->task(AppTask::class, $this);
                    $file_path = $local . '/pdf/PDF_User/' . $pdf_file;
                    $AppTask->startTask('sysnUploadToQiniu', [file_get_contents($file_path), 'pdf/PDF_User/' . $pdf_file], -1, function ($serv, $task_id, $data) {
                    });
                    $file_path2 = $local . '/pdf/img_yz/' . $imgname;
                    $AppTask->startTask('sysnUploadToQiniu', [file_get_contents($file_path2), 'pdf/img_yz/' . $imgname], -1, function ($serv, $task_id, $data) {
                    });
                    //修改合同状态
                    $edit_contract_data['sign_time'] = time();
                    $edit_contract_data['esign_user_sign'] = 'pdf/img_yz/' . $imgname;
                    $edit_contract_data['content'] = 'pdf/PDF_User/' . $pdf_file;
                    $edit_contract_data['sign_type'] = 2;
                    $edit_contract_data['status'] = 2;
                    $this->oaWorkersContractModel->save(['id' => $contract_info['id']], $edit_contract_data);
                    return $this->jsonend(1000, "签约成功", array('filename' => 'pdf/PDF_User/' . $pdf_file, 'esign_user_sign' => 'pdf/img_yz/' . $imgname));
                    //}
                    // return $this->jsonend(-1000, "验签失败");
                }
                return $this->jsonend(-1000, "用户签署失败");
            }
            return $this->jsonend(-1000, "公司印章失败");
        }
        FileLogService::WriteLog(SLog::DEBUG_LOG, 'esign', json_encode($data));
        return $this->jsonend(-1000, "签约失败");


    }


    //企业签章位置
    public function ptqs($pdf_file, $seal_code, $sign_postion_config = '')
    {
        $pdf_path = WWW_DIR . '/pdf/PDF_Generate/';
        $pdf_path2 = WWW_DIR . '/pdf/PDF_Company/';
        file_exists($pdf_path2) || mkdir($pdf_path2, 0777);
        //待签署文档信息
        $signFile = array(
            'srcPdfFile' => $pdf_path . $pdf_file,
            'dstPdfFile' => $pdf_path2 . $pdf_file,
            'fileName' => '',
            'ownerPassword' => ''
        );
        //签章位置
        $signPos = array(
            'posPage' => '3',
            'posX' => '200',
            'posY' => '200',
            'key' => '',
            'width' => '159',
            'isQrcodeSign' => false
        );
        if (!empty($sign_postion_config)) {
            $signPos = json_decode($sign_postion_config, true);
        }
        try {
            $data = $this->entSeal($seal_code);
            if ($data['errCode'] == 0) {
                $dataPDF = $this->sign->userSignPDF($this->accountId, $signFile, $signPos, 'Single', $data['imageBase64'], true);
                if ($dataPDF['errCode'] == 0) {
                    return true;
                } else {
                    return false;
                }
            } else {
                var_dump($data);
                return false;
            }
        } catch (\Exception $e) {
        }
        //$data = $this->sign->selfSignPDF($signFile, $signPos, '0', 'Single', true);
    }

    //平台用户签字
    public function userSign($pdf_file, $sign_file, $imgturn = true, $sign_postion_config = '', $need_company_sign = true,$accountId='')
    {
        $pdf_path = WWW_DIR . '/pdf/PDF_Company/';
        if (!$need_company_sign) {//如果不需要企业签章,直接在模板上签署
            $pdf_path = WWW_DIR . '/pdf/PDF_Generate/';
        }
        $pdf_path2 = WWW_DIR . '/pdf/PDF_User/';
        file_exists($pdf_path2) || mkdir($pdf_path2, 0777);
        // $pdf_file = '20180906_3333333333333333333.pdf';
        $signFile = [
            'srcPdfFile' => $pdf_path . $pdf_file,
            'dstPdfFile' => $pdf_path2 . $pdf_file,
            'fileName' => '',
            'ownerPassword' => '',
        ];

        $signPos = array(
            'posPage' => 3,
            'posX' => '500',
            'posY' => '200',
            'key' => ' ',
            'width' => '200',
        );
        if (!empty($sign_postion_config)) {
            $signPos = json_decode($sign_postion_config, true);
        }
        // $sign_file = WWW_DIR . '/pdf/img_yz/yinzhang.png';
        //旋转图片90度
        // chmod($sign_file, 0777);
        if ($imgturn) {
            imgturn($sign_file);
        }
        $sealData = base64_encode(file_get_contents($sign_file));
        $data = $this->sign->userSignPDF($accountId, $signFile, $signPos, 'Single', $sealData, true);
        if ($data['errCode'] == 0) {
            return true;
        }
        return false;
    }

    //PDF文档验签
    public function fileVerify($pdf_file)
    {
        $pdf_path2 = WWW_DIR . '/pdf/PDF_User/';
        //$pdf_file = '20180906_3333333333333333333.pdf';
        $data = $this->sign->fileVerify($pdf_path2 . $pdf_file, true);
        if ($data['errCode'] == 0) {
            return true;
        }
        return false;
    }


    //获取合同详情
    public function getContractInfo($contract_id)
    {
        $map['contract_id'] = $contract_id;
        $data = $this->contract_model->getOne($map, '*');
        if (empty($data)) {
            return $this->jsonend(-1000, "合同不存在");
        }
        /*         * **********************合同信息********************* */
        $contract_info = array();
        $contract_info['status'] = $data['status'];
        $contract_info['operation_id'] = $data['operation_id'];
        $contract_info['contract_no'] = $data['contract_no']; //合同编号
        $contract_info['contract_money'] = $data['contract_money']; //合同金额
        $contract_info['contract_deposit'] = $data['contract_deposit']; //押金
        $contract_info['effect_time'] = empty($data['effect_time']) ? '' : date('Y-m-d', $data['effect_time']);
        $contract_info['company_id'] = $data['company_id'];
        $contract_info['business_id'] = $data['business_id'];
        $contract_info['province'] = $data['province'];
        $contract_info['city'] = $data['city'];
        $contract_info['area'] = $data['area'];
        $contract_info['address'] = $data['address'];
        $contract_info['esign_user_sign'] = $data['esign_user_sign'];
        if ($data['status'] > 3) {
            $contract_info['contract_time_start_format'] = date('Y-m-d', time()); //合同装机时间
        } else {
            $contract_info['contract_time_start_format'] = date('Y-m-d', $data['installed_time']); //合同开始时间
        }
        $contract_info['contract_time_end_format'] = date('Y-m-d', strtotime($contract_info['contract_time_start_format']) + 20 * 365 * 86400);
        $contract_info['contract_time_start'] = explode('-', $contract_info['contract_time_start_format']);
        $contract_info['contract_time_end'] = explode('-', $contract_info['contract_time_end_format']);
        $arr['contract_info'] = $contract_info;


        /**         * *********************乙方信息---客户********************* */
        $rental_user_info = array();
        //认证信息
        $auth_info = $this->auth_model->getAll(array('user_id' => $data['user_id']), '*');
        if (empty($auth_info) || $auth_info['status'] != 2) {
            return $this->jsonend(-1101, "该客户还未通过认证,请先认证");
        }
        $rental_user_info['realname'] = $auth_info['realname'];
        $rental_user_info['auth_type'] = $auth_info['auth_type'];
        $rental_user_info['id_number'] = $auth_info['id_number'];
        if (in_array($rental_user_info['auth_type'], [2, 3])) {
            $rental_user_info['realname'] = $auth_info['company_name'];
            $rental_user_info['id_number'] = $auth_info['business_license_number'];
        }
        $rental_user_info['business_license_pic'] = $auth_info['business_license_pic'];
        //基本信息
        $user_info = $this->user_model->getOne(array('user_id' => $data['user_id']), 'telphone,emergency_contact,user_class');
        $rental_user_info['telphone'] = $user_info['telphone'];
        $rental_user_info['user_class'] = $user_info['user_class'];
        $rental_user_info['emergency_contact'] = empty($user_info['emergency_contact']) ? '' : json_decode($user_info['emergency_contact'], true);
        $arr['rental_user_info'] = $rental_user_info;

        /**         * *********************甲方信息---公司********************* */
        //$arr['company_info'] = $this->company_model->getOne(array('company_id' => $data['company_id']), '*');
        $company_info = CompanyService::getCompanyInfo($data['company_id']);//请求boss后台接口获取入驻商信息
        //获取运营中心的印章
        $company_info['need_company_sign'] = true;//是否需要企业签章,若运营中心有自己的印章则不需要,直接在合同模板附上
        $operation_info = $this->operation_info_model->getOne(['o_id' => $contract_info['operation_id']], 'contract_company_seal,contract_company_name');
        if (!empty($operation_info) && !empty($operation_info['contract_company_seal'])) {
            $company_info['need_company_sign'] = false;
            $company_info['company_name'] = $operation_info['contract_company_name'];
            $company_info['company_seal'] = $this->config->get('qiniu.qiniu_url') . $operation_info['contract_company_seal'];
        }
        $arr['company_info'] = $company_info;

        /*         * **********************主板信息********************* */
        $equipment_info = array();
        $join = [
            ['equipment_lists as el', 'el.equipment_id = rq_contract_equipment.equipment_id', 'inner'],
            ['equipments as e', 'e.equipments_id = el.equipments_id', 'inner']
        ];
        $contract_ep_info = $this->contract_ep_model->getAll(array('rq_contract_equipment.contract_id' => $data['contract_id']), 'rq_contract_equipment.*,e.equipments_name,e.original_parts,el.device_no,e.equipments_orgin_money', $join);
        if (!empty($contract_ep_info)) {
            foreach ($contract_ep_info as $k => $v) {
                $equipment['equipments_number'] = $v['device_no'];
                $equipment['equipments_name'] = $v['equipments_name'];
                $equipment['equipments_orgin_money'] = $v['equipments_orgin_money'];
                $equipment_info[] = $equipment;
            }
        }
        $arr['equipments_info']['list'] = $equipment_info;
        $arr['equipments_info']['total_num'] = count($equipment_info);

        /**         * *********************原始配件信息********************* */
        $parts = array();
        if (!empty($contract_ep_info)) {
            $original_parts = json_decode($contract_ep_info[0]['original_parts'], true);
            if (!empty($original_parts)) {
                foreach ($original_parts as $k => $v) {
                    $part_info = $this->parts_model->getOne(array('parts_id' => $v['parts_id']), 'parts_name,parts_pic');
                    $part['parts_id'] = $v['parts_id'];
                    $part['parts_name'] = $part_info['parts_name'];
                    $part['parts_pic'] = $this->config->get('qiniu.qiniu_url') . $part_info['parts_pic'];
                    $part['cycle'] = $v['cycle'];
                    $parts[] = $part;
                }
            }
        }

        $arr['original_parts'] = $parts;
        return $arr;
    }


    //重新签署合同，解决数据错误情况
    public function http_againPdf()
    {
        file_exists(WWW_DIR . '/pdf/PDF_Generate/') || mkdir(WWW_DIR . '/pdf/PDF_Generate/', 0777);
        file_exists(WWW_DIR . '/pdf/img_yz/') || mkdir(WWW_DIR . '/pdf/img_yz/', 0777);
        file_exists(WWW_DIR . '/pdf/PDF_Company/') || mkdir(WWW_DIR . '/pdf/PDF_Company/', 0777);
        file_exists(WWW_DIR . '/pdf/PDF_User/') || mkdir(WWW_DIR . '/pdf/PDF_User/', 0777);
        $contract_id = $this->parm['contract_id'] ?? '';
        //读取合同信息
        if (empty($contract_id)) {
            $this->parm = $this->http_input->getAllPostGet();
        }
        if (empty($this->parm['contract_id'])) {
            return $this->jsonend(-1000, "缺少合同参数");
        }
        $contract_id = $this->parm['contract_id'] ?? '';
        $contract_info = $this->getContractInfo($contract_id);
        if (empty($contract_info)) {
            return $this->jsonend(-1000, "未找到合同信息");
        }
        if (empty($contract_info['contract_info']['esign_user_sign'])) {
            return $this->jsonend(-1000, "用户未签署");
        }
        //处理原始配件
        $original_parts = '';
        if (!empty($contract_info['original_parts'])) {
            $original_parts_arr = [];
            foreach ($contract_info['original_parts'] as $k => $v) {
                array_push($original_parts_arr, $v['parts_name']);
            }
            if (!empty($original_parts_arr)) {
                $original_parts = implode(',', $original_parts_arr);
            }
        }
        //处理主板信息
        $eq_info = '';
        if (!empty($contract_info['equipments_info']['list'])) {
            $eq_arr = [];
            foreach ($contract_info['equipments_info']['list'] as $k => $v) {
                //   $str = $v['equipments_name'] . '(编号:' . $v['equipments_number'] . ')';
                $str = $v['equipments_name'];
                array_push($eq_arr, $str);
            }
            if (!empty($eq_arr)) {
                $eq_info = implode(',', $eq_arr);
            }
        }
        $date = date('Ymd');
        $pdf_path = WWW_DIR . '/pdf/PDF_Generate/';
        $pdf_file = $date . '_' . $contract_info['contract_info']['contract_no'] . '_' . rand(1, 100) . '.pdf';
        //查询合同模板
        $temp_pdf = 'rental_contract_temp.pdf';
        $temp_info = ContractService::getTemplate($contract_info['contract_info']['company_id'], $contract_info['contract_info']['business_id'], 1, $contract_info['contract_info']['operation_id']);
        if ($temp_info) {
            $temp_pdf = $temp_info['local_path'];
        }
        $tmpFile = array(
            'srcFileUrl' => WWW_DIR . '/pdf/PDF_Template/' . $temp_pdf,
            'dstFileUrl' => $pdf_path . $pdf_file
        );
        $end_Y = date('Y', strtotime($contract_info['contract_info']['contract_time_start_format'])) + 1;
        $txtFields = array(
            'nailUserName' => $contract_info['rental_user_info']['realname'],
            'nailIdentificationNumber' => $contract_info['rental_user_info']['id_number'],
            'nailAddress' => $contract_info['contract_info']['province'] . $contract_info['contract_info']['city'] . $contract_info['contract_info']['area'],
            'nailAddressDetail' => $contract_info['contract_info']['address'],
            'nailUrgentTel' => empty($contract_info['rental_user_info']['emergency_contact']) ? '' : $contract_info['rental_user_info']['emergency_contact']['tel'],//紧急联系电话
            'nailEmail' => '',//邮箱
            'nailTel' => $contract_info['rental_user_info']['telphone'],
            'BUserName' => $contract_info['company_info']['company_name'],
            'BIdentificationNumber' => $contract_info['company_info']['business_license_pic'],
            'BAddress' => $contract_info['company_info']['company_addr'],
            'BTel' => $contract_info['company_info']['telphone'],
            'BEmail' => $contract_info['company_info']['email'] ?? '',
            'equipment' => $eq_info,
            'equipmentType' => 'RO反渗透直饮机',//主板型号
            'installProvince' => $contract_info['contract_info']['province'],//安装地址省份
            'installCity' => $contract_info['contract_info']['city'],//安装地址城市
            'installArea' => $contract_info['contract_info']['area'],//安装地址区域
            'installAddress' => $contract_info['contract_info']['address'],//安装地址详细地址
            'contractNumber' => $contract_info['contract_info']['contract_no'],
            'contractMoney' => $contract_info['contract_info']['contract_money'],
            'parts' => $original_parts,
            'eqNum' => $contract_info['equipments_info']['total_num'],
            'AY' => date('Y', strtotime($contract_info['contract_info']['contract_time_start_format'])),
            'AM' => date('m', strtotime($contract_info['contract_info']['contract_time_start_format'])),
            'AD' => date('d', strtotime($contract_info['contract_info']['contract_time_start_format'])),
            'BY' => $end_Y,
            'BM' => date('m', strtotime($contract_info['contract_info']['contract_time_start_format'])),
            'BD' => date('d', strtotime($contract_info['contract_info']['contract_time_start_format'])),
            'originMoney' => $contract_info['equipments_info']['list'][0]['equipments_orgin_money'],
            'capOriginMoney' => num_to_rmb($contract_info['equipments_info']['list'][0]['equipments_orgin_money']),
            'installProperty' => '家用机',
            'cycle' => '',
            'eqRemarks' => '',
            'waterPressure' => '',
            'twoContractMoney' => '',
            'renewMoney' => 0,
            'own_house' => false,
            'rental_house' => false,
            'startY' => date('Y', strtotime($contract_info['contract_info']['contract_time_start_format'])),//合同开始
            'startM' => date('m', strtotime($contract_info['contract_info']['contract_time_start_format'])),//合同开始
            'startD' => date('d', strtotime($contract_info['contract_info']['contract_time_start_format'])),//合同开始
            'endY' => $end_Y,
            'endM' => date('m', strtotime($contract_info['contract_info']['contract_time_end_format'])),//合同到期
            'endD' => date('d', strtotime($contract_info['contract_info']['contract_time_end_format'])),//合同到期
            'signTime' => $contract_info['contract_info']['contract_now_date'],//签约时间
            'signRepresent' => ''//签约代表
        );

        //租赁合同 有效期
        // if ($contract_info['contract_info']['contract_class'] == 1) {
        $txtFields['leaseYear'] = 1;
        $txtFields['startY'] = $contract_info['contract_info']['contract_time_start'][0];
        $txtFields['startM'] = $contract_info['contract_info']['contract_time_start'][1];
        $txtFields['startD'] = $contract_info['contract_info']['contract_time_start'][2];
        $txtFields['endY'] = $contract_info['contract_info']['contract_time_start'][0] + 1;
        $txtFields['endM'] = $contract_info['contract_info']['contract_time_end'][1];
        $txtFields['endD'] = $contract_info['contract_info']['contract_time_end'][2];
        // }
        $data = $this->sign->createFromTemplate($tmpFile, false, $txtFields, false);
        if ($data['errCode'] == 0) {
            $company_sign = $this->ptqs($pdf_file, $contract_info['company_info']['seal_code']);
            if ($company_sign) {
                $imgname = $contract_info['contract_info']['esign_user_sign'];//用户签署图
                $filepath = WWW_DIR . '/';
                //$filepath = 'https://qn.youheone.com/';
                $sign_file = $filepath . $imgname;
                if (!file_exists($sign_file)) {
                    $temp_arr = explode('/', $imgname);
                    $temp_pdf = $temp_arr[count($temp_arr) - 1];
                    $downResult = downloadOriginFile($this->config->get('qiniu.qiniu_url') . $imgname, WWW_DIR . '/pdf/img_yz', $temp_pdf);
                    if (!$downResult) {
                        return $this->jsonend(-1000, "从远程服务器下载合同模板失败,请重试");
                    }
                    chmod($downResult['save_path'], 0777);
                    $sign_file = $downResult['save_path'];
                }
                $user_sign_status = $this->userSign($pdf_file, $sign_file, false);
                if ($user_sign_status) {
                    //上传资源到七牛云
                    $AppTask = $this->loader->task(AppTask::class, $this);
                    $file_path = WWW_DIR . '/pdf/PDF_User/' . $pdf_file;
                    $AppTask->startTask('sysnUploadToQiniu', [file_get_contents($file_path), 'pdf/PDF_User/' . $pdf_file], -1, function ($serv, $task_id, $data) {
                    });
                    $this->contract_model->save(['contract_id' => $this->parm['contract_id']], ['esign_filename' => 'pdf/PDF_User/' . $pdf_file]);//修改合同信息
                    return $this->jsonend(1000, "验签通过,签署成功", array('filename' => 'pdf/PDF_User/' . $pdf_file, 'esign_user_sign' => $imgname));
                    //}
                    // return $this->jsonend(-1000, "验签失败");
                }
                return $this->jsonend(-1000, "用户签署失败");
            }
            return $this->jsonend(-1000, "公司印章失败");
        }
        return $this->jsonend(-1000, "PDF填充失败");

    }

    //用户签字并上传到服务器
    public function http_userSignName()
    {
        $this->parm = $this->http_input->getAllPostGet();
        $file = $this->request->files['file'];
        $contract_id = $this->parm['contract_id'];
        $map['contract_id'] = $contract_id;
        $contract_info = $this->contract_model->getOne($map, '*');
        if (empty($contract_info)) {
            return $this->jsonend(-1000, "合同不存在");
        }
        $imgname = date('YmdHis') . $contract_info['contract_no'] . time() . '_' . rand(0, 9999) . substr($file['name'], strrpos($file['name'], '.')); //上传后文件名称
        $tmp = $file['tmp_name'];
        $filepath = WWW_DIR . '/pdf/img_yz/';
        if (!move_uploaded_file($tmp, $filepath . $imgname)) {
            return $this->jsonend(-1001, '图片上传失败');
        }
        $AppTask = $this->loader->task(AppTask::class, $this);
        $file_path2 = WWW_DIR . '/pdf/img_yz/' . $imgname;
        $AppTask->startTask('sysnUploadToQiniu', [file_get_contents($file_path2), 'pdf/img_yz/' . $imgname], -1, function ($serv, $task_id, $data) {
        });
        $edit_data['esign_user_sign'] = 'pdf/img_yz/' . $imgname;
        $this->contract_model->save(array('contract_id' => $this->parm['contract_id']), $edit_data);
        imgturn($file_path2);
        return $this->jsonend(1000, "签名成功", 'pdf/img_yz/' . $imgname);
    }

    //CRM打开合同原件
    public function http_openContract()
    {
        $auth = get_instance()->QiniuAuth;
        $token = $auth->uploadToken($this->bucket); // 生成上传 Token
        $uploadMgr = get_instance()->QiniuUploadManager;
        if (empty($this->parm['contract_id'])) {
            return $this->jsonend(-1000, "缺少参数");
        }
        $map['contract_id'] = $this->parm['contract_id'];
        $contract_info = $this->contract_model->getOne($map, 'esign_filename,esign_user_sign,status');
        if (empty($contract_info) || in_array($contract_info['status'], [1, 2, 3]) || empty($contract_info['esign_user_sign']) || empty($contract_info['esign_filename'])) {
            return $this->jsonend(-1000, "合同不存在或者合同未签署");
        }
        $array = get_headers($this->config->get('qiniu.qiniu_url') . $contract_info['esign_filename'], 1);//判断是否可访问
        //判断文件是否存在于七牛
        if (preg_match('/200/', $array[0])) {
            return $this->jsonend(1000, '合同原件已上传至七牛云', $this->config->get('qiniu.qiniu_url') . $contract_info['esign_filename']);
        } else {
            //尝试重新上传
            $file_path = WWW_DIR . '/' . $contract_info['esign_filename'];
            if (file_exists($file_path)) {
                try {
                    list($img_ret, $img_err) = $uploadMgr->put($token, file_get_contents($file_path), $contract_info['esign_filename']);
                    if ($img_err == null) {
                        return $this->jsonend(1000, '上传七牛云成功', $this->config->get('qiniu.qiniu_url') . $contract_info['esign_filename']);
                    }
                } catch (\Exception $e) {
                }
            } else {
                //合同原件不存在,需要重新生成合同
                return $this->jsonend(1000, '合同信息错误');
            }

        }


    }
}
