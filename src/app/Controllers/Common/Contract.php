<?php


namespace app\Controllers\Common;

use app\Library\SLog;
use app\Services\Common\HttpService;
use app\Services\Log\AsyncFile;
use app\Services\Log\FileLogService;
use Server\CoreBase\SwooleException;

/**
 * 合同公共接口
 * Class Contract
 * @package app\Controllers\Common
 */
class Contract extends Base
{

    protected $contract_model;
    protected $contract_ep_model;
    protected $bind_model;
    protected $equipment_list_model;
    protected $contractPackageRecordModel;
    protected $equipmentsPartsModel;
    protected $Achievement;
    protected $work_eq_model;
    protected $settlementModel;

    // 前置方法，加载模型
    public function initialization($controller_name, $method_name)
    {
        if ($this->response !== null && $this->request_type == 'http_request') {
            $this->http_output->setHeader('Access-Control-Allow-Origin', '*');
            $this->http_output->setHeader("Access-Control-Allow-Headers", " Origin, X-Requested-With, Content-Type, Accept,token");
            $this->http_output->setHeader("Access-Control-Allow-Methods", " POST");
        }
        parent::initialization($controller_name, $method_name);
        $this->contract_model = $this->loader->model('ContractModel', $this);
        $this->contract_ep_model = $this->loader->model('ContractEquipmentModel', $this);
        $this->bind_model = $this->loader->model('CustomerBindEquipmentModel', $this);
        $this->equipment_list_model = $this->loader->model('EquipmentListsModel', $this);
        $this->contractPackageRecordModel = $this->loader->model('ContractPackageRecordModel', $this);
        $this->equipmentWaterRecordModel = $this->loader->model('EquipmentWaterRecordModel', $this);
        $this->equipmentsPartsModel = $this->loader->model('EquipmentsPartsModel', $this);
        $this->Achievement = $this->loader->model('AchievementModel', $this);
        $this->work_eq_model = $this->loader->model('WorkEquipmentModel', $this);
        $this->settlementModel = $this->loader->model('SettlementModel', $this);

    }

    /**
     * showdoc
     * @catalog API文档/公共API/合同相关
     * @title 重新下发套餐
     * @description
     * @method POST
     * @url /Common/Contract/issuePackage
     * @param contract_id 必选 int 合同ID
     * @param equipment_id 必选 int 主板ID
     * @return
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-12-5
     */
    public function http_issuePackage()
    {

        AsyncFile::write('issue_package', $this->parm['device_no'] . '开始下发套餐');
        if (empty($this->parm['device_no'] ?? '')) {
            return $this->jsonend(-1002, "缺少参数");
        }
        //查询主板编号
        $eq_info = $this->equipment_list_model->findEquipmentLists(['device_no' => $this->parm['device_no']], 'equipment_id,device_no,switch_machine,status');
        if (empty($eq_info)) {
            AsyncFile::write('issue_package', date('Y-m-d H:i:s', time()) . '-' . $eq_info['device_no'] . ':主板不存在');
            return $this->jsonend(-1000, "主板不存在");
        }
        if ($eq_info['switch_machine'] == 0) {
            AsyncFile::write('issue_package', date('Y-m-d H:i:s', time()) . '-' . $eq_info['device_no'] . ':主板已关机，请开机后再下发');
            return $this->jsonend(-1000, "主板已关机，请开机后再下发");
        }
        //查找该主板关联的合同
        $join = [
            ['contract c', 'c.contract_id = rq_contract_equipment.contract_id', 'left']
        ];
        $map['c.status'] = ['IN', [4, 5]];//生效中或者已过期的合同
        $map['rq_contract_equipment.is_delete'] = 0;
        $map['rq_contract_equipment.state'] = 1;
        $map['equipment_id'] = $eq_info['equipment_id'];
        $contract_info = $this->contract_ep_model->getAll($map, 'c.contract_id,c.package_mode,c.package_value,c.package_cycle,effect_time,exire_date', $join);
        if (empty($contract_info)) {
            AsyncFile::write('issue_package', date('Y-m-d H:i:s', time()) . '-' . $eq_info['device_no'] . ':没有相关合同,无法进行套餐下发');
            return $this->jsonend(-1000, "没有相关合同,无法进行套餐下发");
        }
        if (count($contract_info) > 1) {
            AsyncFile::write('issue_package', date('Y-m-d H:i:s', time()) . '-' . $eq_info['device_no'] . ':该主板存在多个合同中,无法进行套餐下发');
            return $this->jsonend(-1000, "该主板存在多个合同中,无法进行套餐下发");
        }
        //工作模式 0流量模式 1时长模式
        $working_mode = 1;
        if ($contract_info[0]['package_mode'] == 2) {
            $working_mode = 0;
        }
        //滤芯最大值
        //$part_map['contract_id'] = $contract_info[0]['contract_id'];
        $part_map['equipment_id'] = $eq_info['equipment_id'];
        $part_map['is_filter'] = 1;
        $part_map['is_delete'] = 0;
        $eq_parts = $this->equipmentsPartsModel->getAll($part_map, 'last_replace_time,expire_time', [], ['filter_level' => 'ASC']);
        if (empty($eq_parts)) {
            AsyncFile::write('issue_package', date('Y-m-d H:i:s', time()) . '-' . $eq_info['device_no'] . ':该主板无滤芯记录,无法进行套餐下发');
            return $this->jsonend(-1000, "该主板无滤芯记录,无法进行套餐下发");
        }
        //计算最大值
        $filter_element_max = [];
        foreach ($eq_parts as $k => $v) {
            $max = $v['expire_time'] - $v['last_replace_time'] < 0 ? 0 : $v['expire_time'] - $v['last_replace_time'];//过期时间与替换时间的差值
            $max = ceil($max / 86400);
            if (count($filter_element_max) < 5) {//取5级
                array_push($filter_element_max, $max);
            }
        }
        //下发-绑定套餐
        $params = ['sn' => $eq_info['device_no'], 'working_mode' => $working_mode, 'filter_element_max' => $filter_element_max];
        $path = '/House/Issue/bindingPackage';
        HttpService::Thrash($params, $path);
        sleepCoroutine(2000);
        //计算数据
        $used_days = ceil((time() - $contract_info[0]['effect_time']) / 86400);//已用天数，当前时间-签约时间
        //主板在到期时间当月最后一天前都可以使用--2020-04-13修改
        $shutdown = $this->parm['shutdown']??0;
        $exire_date = strtotime(getMonthLastDay($contract_info[0]['exire_date']));   //获取签约月份最后一天
        if($shutdown==1){
            $exire_date = $contract_info[0]['exire_date'];
        }
        $remaining_days = floor(($exire_date - time()) / 86400);//剩余天数,过期时间-当前时间
        //统计主板已用流量
        $equipment_water_record_sum = $this->equipmentWaterRecordModel->getOne(['equipment_id' => $eq_info['equipment_id']], 'sum(water) total_water');  // 总用水量
        $used_traffic = $equipment_water_record_sum['total_water'] ?? 0;//已用流量
        $remaining_traffic = 999999999;
        if ($working_mode == 0) {
            $remaining_traffic = $contract_info[0]['package_value'] - $used_traffic;//如果是流量模式,剩余流量=套餐值-已用流量
        }
        //下发--数据同步
        $data_sync_params['sn'] = $eq_info['device_no'];
        $data_sync_params['used_days'] = $used_days;
        $data_sync_params['used_traffic'] = $used_traffic;
        $data_sync_params['remaining_days'] = $remaining_days;
        $data_sync_params['remaining_traffic'] = $remaining_traffic;
        $data_sync_path = '/House/Issue/data_sync';
        HttpService::Thrash($data_sync_params, $data_sync_path);
        //保存主板数据
        $this->equipment_list_model->updateEquipmentLists(array('working_mode' => $working_mode, 'remaining_traffic' => $data_sync_params['remaining_traffic'], 'remaining_days' => $data_sync_params['remaining_days'], 'used_traffic' => $data_sync_params['used_traffic'], 'used_days' => $data_sync_params['used_days']), array('equipment_id' => $eq_info['equipment_id']));
        $this->redis->del('device_heartbeat:' . $eq_info['device_no']);//删除redis心跳
        //删除redis数据，用于同步
        $this->redis->del('device_heartbeat:' . $eq_info['device_no']);
        $this->redis->del('work_state:' . $eq_info['device_no']);
        //请求心跳
        sleepCoroutine(2000);
        $state_params['sn'] = $eq_info['device_no'];
        $state_path = '/House/Issue/heartbeat';
        HttpService::Thrash($state_params, $state_path);
        AsyncFile::write('issue_package', date('Y-m-d H:i:s', time()) . '-' . $eq_info['device_no'] . ':' . json_encode(['package' => $params, 'data_sync_params' => $data_sync_params]));
        return $this->jsonend(1000, "请求发送成功,正在下发中");

    }
    //结算
    public function http_settlement(){
        $bill_ids = explode(',',$this->parm['id']);
        foreach ($bill_ids as $k => $v) {
            if(empty($v)){
                continue;
            }
            $result = $this->settlementModel->settlement($v);//结算
            if ($result['code'] != 1000) {
                var_dump($v . ':结算异常>>' . $result['msg']);
            }
        }
    }



}