<?php

namespace app\Controllers\Common;

use app\Services\Company\CompanyService;
use app\Services\Log\AsyncFile;
use Server\CoreBase\Controller;
use app\Services\Common\ConfigService;
use Server\CoreBase\SwooleRedirectException;
use Server\CoreBase\SwooleException;
use Monolog\Logger;
use Server\SwooleMarco;

/*
 * 基础类
 */

class Base extends Controller
{

    protected $parm;
    protected $GetIPAddressHttpClient;
    protected $dev_mode;
    protected $app_debug;
    protected $wx_config;
    protected $client;
    protected $esign_config;
    protected $role_key;
    protected $company_config;
    public function initialization($controller_name, $method_name)
    {
        if ($this->response !== null && $this->request_type == 'http_request') {
            $this->http_output->setHeader('Access-Control-Allow-Origin', '*');
            $this->http_output->setHeader("Access-Control-Allow-Headers", " Origin, X-Requested-With, Content-Type, Accept,token,company,openid,role");
            $this->http_output->setHeader("Access-Control-Allow-Methods", " POST");
        }
        parent::initialization($controller_name, $method_name);
        $this->parm = json_decode($this->http_input->getRawContent(), true);
        $this->user_id = $this->request->user_id;
        $this->company = $this->request->company;
        if (empty($this->company)) {
            $this->company = 1;
        }
        //开发者模式
        $this->dev_mode = ConfigService::getConfig('develop_mode', false, $this->company);
        //调试模式
        $this->app_debug = ConfigService::getConfig('app_debug', false, $this->company);
        $this->role = $this->request->header['role'] ?? 4; //角色 端1总后台 2运营 3行政 4用户 5工程
        //获取微信配置
        $this->company_config=CompanyService::getCompanyConfig($this->company);
        $this->wx_config = $this->company_config['wx_config'];
        $this->role_key = 'user';
        if ($this->role == 5) {
            $this->role_key = 'engineer';
            $this->wx_config = $this->company_config['engineer_wx_config'];
        }else if($this->role==6){
            $this->role_key = 'spread';
            $this->wx_config = $this->company_config['spread_wx_config'];
        }
        $this->esign_config = $this->company_config['esign_config'];
    }

    public function __construct()
    {
        parent::__construct();
        $this->GetIPAddressHttpClient = get_instance()->getAsynPool('BuyWater');
    }

    /**
     * 异常的回调(如果需要继承$autoSendAndDestroy传flase)
     * @param \Throwable $e
     * @param callable $handle
     */
    public function onExceptionHandle(\Throwable $e, $handle = null)
    {
        $log = '--------------------------[报错信息]----------------------------' . PHP_EOL;
        $log .= 'client：Common' . PHP_EOL;
        $log .= 'DATE：' . date("Y-m-d H:i:s") . PHP_EOL;
        $log .= 'getMessage：' . $e->getMessage() . PHP_EOL;
        $log .= 'getCode：' . $e->getCode() . PHP_EOL;
        $log .= 'getFile：' . $e->getFile() . PHP_EOL;
        $log .= 'getLine：' . $e->getLine();
        AsyncFile::write('error', $log);
        //必须的代码
        if ($e instanceof SwooleRedirectException) {
            $this->http_output->setStatusHeader($e->getCode());
            $this->http_output->setHeader('Location', $e->getMessage());
            $this->http_output->end('end');
            return;
        }
        if ($e instanceof SwooleException) {
            secho("EX", "--------------------------[报错指南]----------------------------" . date("Y-m-d h:i:s"));
            secho("EX", "异常消息：" . $e->getMessage());
            print_context($this->getContext());
            secho("EX", "--------------------------------------------------------------");
            $this->log($e->getMessage() . "\n" . $e->getTraceAsString(), Logger::ERROR);
        }
        //可以重写的代码
        if ($handle == null) {
            switch ($this->request_type) {
                case SwooleMarco::HTTP_REQUEST:
                    $e->request = $this->request;
                    $data = get_instance()->getWhoops()->handleException($e);
                    $this->http_output->setStatusHeader(500);
                    $this->http_output->end($data);
                    break;
                case SwooleMarco::TCP_REQUEST:
                    $this->send($e->getMessage());
                    break;
            }
        } else {
            \co::call_user_func($handle, $e);
        }
    }

    /**
     * 返回json格式
     */
    public function jsonend($code = '', $message = '', $data = '', $gzip = true)
    {
        $output = array('code' => $code, 'message' => $message, 'data' => $data);
        if (!$this->canEnd()) {
            return;
        }
        if (!get_instance()->config->get('http.gzip_off', false)) {
            //低版本swoole的gzip方法存在效率问题
//            if ($gzip) {
//                $this->response->gzip(1);
//            }
            //压缩备用方案
            /* if ($gzip) {
              $this->response->header('Content-Encoding', 'gzip');
              $this->response->header('Vary', 'Accept-Encoding');
              $output = gzencode($output . " \n", 9);
              } */
        }
        if (is_array($output) || is_object($output)) {
            $this->response->header('Content-Type', 'application/json; charset=UTF-8');
            $output = json_encode($output, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            //$output = "<pre>$output</pre>";
        }

        $this->response->end($output);
        $this->endOver();
    }

    public function bossJsonend($code = '', $message = '', $data = '', $conut = '', $info = '')
    {
        $output = array('code' => $code, 'msg' => $message, 'data' => $data, 'count' => $conut, 'info' => $info);
        if (!$this->canEnd()) {
            return;
        }
        if (is_array($output) || is_object($output)) {
            $this->response->header('Content-Type', 'text/html; charset=UTF-8');
            $output = json_encode($output, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            //$output = "<pre>$output</pre>";
        }

        $this->response->end($output);
        $this->endOver();
    }


    /**
     * 处理不同基础服务费用
     * @param array $ser_info 服务信息 => $rental_cost 租赁时费用 $buy_cost 购买时费用 $usiness_type_id 服务ID
     * @param array $eq_info 主板信息  =>$equipment_type 类型  $start_time保修开始时间 $end_time 保修结束时间
     */
    public function baseServiceCost($ser_info, $eq_info)
    {
        $cost = $ser_info['rental_cost'];
        return $cost;
    }

    /**
     * 处理不同滤芯费用
     * @param array $part_info 配件信息 => $parts_money 租赁时费用 $parts_buy_money 购买时费用 $usiness_type_id 服务ID
     * @param array $eq_info 主板信息  =>$equipment_type 类型  $start_time保修开始时间 $end_time 保修结束时间
     */
    public function partCost($part_info, $eq_info)
    {
        $cost = $part_info['parts_money'];

        return $cost;
    }

    /**
     * 处理不同额外服务费用
     * @param array $ser_info 服务信息 => $parts_money 租赁时费用 $parts_buy_money 购买时费用 $usiness_type_id 服务ID
     * @param array $eq_info 主板信息  =>$equipment_type 类型  $start_time保修开始时间 $end_time 保修结束时间
     */
    public function addtionalServiceCost($ser_info, $eq_info)
    {
        $cost = $ser_info['service_rental_cost'];
        return $cost;
    }



}
