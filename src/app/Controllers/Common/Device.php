<?php


namespace app\Controllers\Common;

use app\Library\SLog;
use app\Services\Common\HttpService;
use app\Services\Device\DeviceService;
use app\Services\Log\AsyncFile;
use app\Services\Log\FileLogService;
use Server\CoreBase\SwooleException;

/**
 * 主板公共接口
 * Class Contract
 * @package app\Controllers\Common
 */
class Device extends Base
{

    protected $equipment_list_model;
    protected $equipmentsPartsModel;
    protected $partsModel;
    protected $equipmentListsModel;
    protected $Achievement;
    protected $contractModel;

    // 前置方法，加载模型
    public function initialization($controller_name, $method_name)
    {
        if ($this->response !== null && $this->request_type == 'http_request') {
            $this->http_output->setHeader('Access-Control-Allow-Origin', '*');
            $this->http_output->setHeader("Access-Control-Allow-Headers", " Origin, X-Requested-With, Content-Type, Accept,token");
            $this->http_output->setHeader("Access-Control-Allow-Methods", " POST");
        }
        parent::initialization($controller_name, $method_name);

        $this->equipment_list_model = $this->loader->model('EquipmentListsModel', $this);
        $this->equipmentsPartsModel = $this->loader->model('EquipmentsPartsModel', $this);
        $this->partsModel = $this->loader->model('PartsModel', $this);
        $this->equipmentListsModel = $this->loader->model('EquipmentListsModel', $this);
        $this->Achievement = $this->loader->model('AchievementModel', $this);
        $this->contractModel = $this->loader->model('ContractModel', $this);

    }

    /**
     * showdoc
     * @catalog API文档/公共API/主板相关
     * @title 同步滤芯数据
     * @description
     * @method POST
     * @url /Common/Device/syncFilterData
     * @param equipment_id 可选 int 主板ID
     * @return
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-12-5
     */
    public function http_syncFilterData()
    {

        if (!empty($this->parm['equipment_id'])) {
            $map['equipment_id'] = $this->parm['equipment_id'];
        } else {
            $map['device_status'] = 4;
        }
        $lists = $this->equipment_list_model->selectEquipmentLists($map, 'equipment_id,device_no,device_status,remaining_days');
        if (empty($lists)) {
            return $this->jsonend(-1002, "暂无处于滤芯待复位状态的主板");
        }
        $join = [
            ['equipments_parts ep', 'ep.parts_id = rq_parts.parts_id', 'left'],  // 主板配件表 -- 配件表
        ];
        $field = 'ep.id,rq_parts.parts_id,rq_parts.parts_name,ep.cycle,ep.expire_time';
        foreach ($lists as $k => $v) {
            // 获取主板滤芯
            $where['ep.equipment_id'] = $v['equipment_id'];
            $where['ep.is_filter'] = 1;  // 1表示滤芯
            $where['ep.is_delete'] = 0;  // 0表示未删除
            $equipments_parts = $this->partsModel->getJoinAll($where, $field, $join);
            unset($where);
            if ($equipments_parts) {
                foreach ($equipments_parts as $kk => &$vv) {
                    $residual_value = floor(($vv['expire_time'] - time()) / 86400); // 剩余天数，当前仅根据时间计算，算法待定
                    if ($v['remaining_days'] < 1) {
                        $residual_value = 30;//如果欠费和滤芯待复位同时存在，下发固定的滤芯值
                    }
                    $params = ['sn' => $v['device_no'], 'type' => 2, 'key' => $kk + 1, 'value' => $residual_value + 1];
                    $path = '/House/Issue/filter_element_reset_modification';
                    sleepCoroutine(2000);
                    $result = HttpService::Thrash($params, $path);
                    if (!$result) {
                        return $this->jsonend(-1002, $v['device_no'] . '发送失败');
                    }
                }
                //删除redis数据，用于同步
                $this->redis->del('device_heartbeat:' . $v['device_no']);
                $this->redis->del('work_state:' . $v['device_no']);

                //请求心跳
                sleepCoroutine(2000);
                $state_params['sn'] = $v['device_no'];
                $state_path = '/House/Issue/heartbeat';
                HttpService::Thrash($state_params, $state_path);
            }

        }
        return $this->jsonend(1000, "处理完成,请在主板信息里确认是否同步成功");


    }

    /**
     * 获取所有主板版本
     */
    public function http_getVersion()
    {
        $where['company_id'] = $this->parm['company_id'];

        $data = $this->equipmentListsModel->selectEquipmentLists($where, 'equipment_id,device_no');
        if (empty($data)) {
            return $this->jsonend(-1000, "暂无数据");
        }

        foreach ($data as $k => $v) {
            if ($v['device_no'] == 0) {
                continue;
            }
            $params = ['sn' => $v['device_no']];
            $path = '/House/Issue/version';
            $result = HttpService::Thrash($params, $path);
            if (!$result) {
                return $this->jsonend(-1002, $v['device_no'] . '发送失败');
            }
        }
        return $this->jsonend(1000, "获取版本成功,请在主板信息里确认");

    }

    /**
     * showdoc
     * @catalog API文档/公共API/主板相关
     * @title 开关机
     * @description
     * @method POST
     * @url /Common/Device/switch_control
     * @param sn 必选 int 主板编号
     * @param device_status 必选 int 开关机,1:关机,0:开机
     * @return
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-12-5
     */
    public function http_switch_control()
    {
        if (empty($this->parm['sn'])) {
            return $this->jsonend(-1000, "缺少参数:设备编号");
        }
        if (!isset($this->parm['device_status']) || !in_array($this->parm['device_status'], [0, 1])) {
            return $this->jsonend(-1000, "缺少参数:开关机状态");
        }
        $eq_info = $this->equipmentListsModel->getOne(['device_no' => $this->parm['sn']], 'status,device_status');
        if (empty($eq_info)) {
            return $this->jsonend(-1000, "设备不存在");
        }
        if ($eq_info['status'] != 2){
            return $this->jsonend(-1000,'设备已离线');
        }
        if ($eq_info['device_status'] == 2){
            return $this->jsonend(-1000,'设备已欠费，请尽快缴纳费用');
        }
        $device_status = $this->parm['device_status'];
        $params = ['sn' => $this->parm['sn'], 'status' => $device_status];
        $result = DeviceService::Thrash($params, 'switch_control');
        if ($result['code'] == 1) {
            $this->equipmentListsModel->save(['device_no' => $this->parm['sn']], ['switch_machine' => $device_status]);
            return $this->jsonend(1000, '操作成功');
        }
        return $this->jsonend(-1000, '操作失败');
    }

    /**
     * showdoc
     * @catalog API文档/公共API/主板相关
     * @title 下发套餐
     * @description
     * @method POST
     * @url /Common/Device/binding_package
     * @param sn 必选 int 主板编号
     * @param duration 必选 int 时长,单位小时
     * @return
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-12-5
     */
    public function http_binding_package()
    {
        if (empty($this->parm['sn'])) {
            return $this->jsonend(-1000, "缺少参数:设备编号");
        }
        $join = [
            ['customer_code', 'rq_customer_code.code=rq_equipment_lists.customer_code']
        ];
        $eq_info = $this->equipmentListsModel->getOne(['device_no' => $this->parm['sn'], 'rq_customer_code.is_delete' => 0], 'contract_id', $join);
        if (empty($eq_info) || empty($eq_info['contract_id'])) {
            return $this->jsonend(-1000, "合同信息错误");
        }
        $result = $this->contractModel->activateDevice($eq_info['contract_id'], [$this->parm['sn']]);
        return $this->jsonend($result['code'], $result['msg']);
    }


    /**
     * showdoc
     * @catalog API文档/公共API/主板相关
     * @title 下发自定义套餐
     * @description
     * @method POST
     * @url /Common/Device/custom_binding_package
     * @param sn 必选 int 主板编号
     * @param expire_date 必选 string 到期时间
     * @param shutdown_date 必选 string 停机时间
     * @return
     * @remark
     * @number 0
     * @author xln
     * @date 2018-12-5
     */
    public function http_custom_binding_package(){
        if (empty($this->parm['sn']))  return $this->jsonend(-1000, "缺少参数:设备编号");
        if (empty($this->parm['expire_date'])) return $this->jsonend(-1000,'缺少参数：到期时间');
        if (empty($this->parm['shutdown_date'])) return $this->jsonend(-1000,'缺少参数：停机时间');
        if (strtotime($this->parm['expire_date']) > strtotime($this->parm['shutdown_date'])) return $this->jsonend(-1000,'到期时间不能大于停机时间');
        $join = [
            ['customer_code', 'rq_customer_code.code=rq_equipment_lists.customer_code']
        ];
        $eq_info = $this->equipmentListsModel->getOne(['device_no' => $this->parm['sn'], 'rq_customer_code.is_delete' => 0], 'contract_id', $join);
        if (empty($eq_info) || empty($eq_info['contract_id'])) {
            return $this->jsonend(-1000, "合同信息错误");
        }
        $result = $this->contractModel->customActivateDevice($eq_info['contract_id'], [$this->parm['sn']],$this->parm['expire_date'],$this->parm['shutdown_date']);
        return $this->jsonend($result['code'], $result['msg']);
    }


    /**
     * showdoc
     * @catalog API文档/公共API/主板相关
     * @title 获取ICCID
     * @description
     * @method POST
     * @url /Common/Device/get_iccid
     * @param sn 必选 int 主板编号
     * @return
     * @remark
     * @number 0
     * @author xln
     * @date 2018-12-5
     */
    public function http_get_iccid(){
        if (empty($this->parm['sn']))  return $this->jsonend(-1000, "缺少参数:设备编号");
        $join = [['customer_code', 'rq_customer_code.code=rq_equipment_lists.customer_code']];
        $eq_info = $this->equipmentListsModel->getOne(['device_no' => $this->parm['sn'], 'rq_customer_code.is_delete' => 0], 'contract_id', $join);
        if (empty($eq_info) || empty($eq_info['contract_id'])) {
            return $this->jsonend(-1000, "合同信息错误");
        }
        $result = $this->contractModel->getIccId($this->parm['sn']);
        return $this->jsonend($result['code'], $result['msg']);
    }


    /**
     * showdoc
     * @catalog API文档/公共API/主板相关
     * @title 获取运行总时长
     * @description
     * @method POST
     * @url /Common/Device/read_running_time
     * @param sn 必选 int 主板编号
     * @return
     * @remark
     * @number 0
     * @author xln
     * @date 2018-12-5
     */
    public function http_read_running_time(){
        if (empty($this->parm['sn']))  return $this->jsonend(-1000, "缺少参数:设备编号");
        $result = $this->contractModel->getReadRunningTime($this->parm['sn']);
        return $this->jsonend($result['code'], $result['msg']);
    }


    /**
     * showdoc
     * @catalog API文档/公共API/主板相关
     * @title 清空运行时长
     * @description
     * @method POST
     * @url /Common/Device/clear_running_time
     * @param sn 必选 int 主板编号
     * @return
     * @remark
     * @number 0
     * @author xln
     * @date 2018-12-5
     */
    public function http_clear_running_time(){
        if (empty($this->parm['sn']))  return $this->jsonend(-1000, "缺少参数:设备编号");
        $result = $this->contractModel->clearRunningTime($this->parm['sn']);
        return $this->jsonend($result['code'], $result['msg']);
    }


    /**
     * showdoc
     * @catalog API文档/公共API/主板相关
     * @title 下发二维码
     * @description
     * @method POST
     * @url /Common/Device/set_qrcode
     * @param sn 必选 int 主板编号
     * @param content 必选 int 二维码内容
     * @return
     * @remark
     * @number 0
     * @author lcx
     * @date 2018-12-5
     */
    public function http_set_qrcode()
    {
        if (empty($this->parm['sn'])) {
            return $this->jsonend(-1000, "缺少参数:设备编号");
        }
        $customer_code = $this->equipmentListsModel->getOne(['device_no' => $this->parm['sn']], 'customer_code,status');
        $qrcode_url = $this->config->get('equipment_qrcode');
        if (empty($customer_code['customer_code']) || empty($qrcode_url)) {
            return $this->jsonend(-1000, "缺少参数:二维码内容");
        }
        if ($customer_code['status'] != 2){
            return $this->jsonend(-1000, "设备不在线");
        }
        $content = $qrcode_url . $customer_code['customer_code']; //二维码地址
        $params = ['sn' => $this->parm['sn'], 'content' => $content];
        $result = DeviceService::Thrash($params, 'set_qrcode');
        if ($result['code'] == 1) {
            return $this->jsonend(1000, '操作成功');
        }
        return $this->jsonend(-1000, '操作失败');
    }

    /**
     * showdoc
     * @catalog API文档/公共API/主板相关
     * @title 请求ICCID
     * @description
     * @method POST
     * @url /Equipment/Send/iccid
     * @param sn 必选 int 主板编号
     * @return
     * @remark
     * @number 0
     * @author xln
     * @date 2018-12-5
     */
    public function http_iccid(){
        if (empty($this->parm['sn'])) {
            return $this->jsonend(-1000, "缺少参数:设备编号");
        }
        $customer_code = $this->equipmentListsModel->getOne(['device_no' => $this->parm['sn']], 'customer_code,status');
        if ($customer_code['status'] != 2){
            return $this->jsonend(-1000, "设备不在线");
        }
        $params = ['sn' => $this->parm['sn']];
        $result = DeviceService::Thrash($params, 'iccid');
        if ($result['code'] == 1) {
            return $this->jsonend(1000, '操作成功');
        }
        return $this->jsonend(-1000, '操作失败');
    }







}