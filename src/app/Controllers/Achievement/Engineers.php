<?php
/**
 * Created by PhpStorm.
 * User: LG
 * Date: 2019/4/12
 * Time: 14:58
 */

namespace app\Controllers\Achievement;


class Engineers extends Base
{
    protected $Achievement;

    public function initialization($controller_name, $method_name) {
        parent::initialization($controller_name, $method_name); // TODO: Change the autogenerated stub

        $this->Achievement = $this->loader->model('AchievementModel', $this);
        $this->Achievement->table = '';
    }
    /**
     * 工程师傅列表
     * @desc 工程师傅列表
     * 参数名      类型      是否必须            说明
     * page         int         否           当前页码，默认1
     * row          int         否           每页显示数目
     * keyword      string      否           搜索值
     * @author ligang
     * @RequestType POST
     * @date 2019/4/12 14:59
     */
    public function http_engineersList()
    {
        $page = $this->parm['page'] ?? 1;
        $row = $this->parm['row'] ?? 10;
        $keyword = $this->parm['keyword'] ?? '';

        $where = [
            'rq_engineers.is_delete'=>0
        ];
        $where['rq_administrative_info.company_id'] = $this->company;
        if (!empty($keyword)){
            $where['engineers_name|engineers_number|engineers_phone'] = ['LIKE','%'.trim($keyword).'%'];
        }

        $field = '
            rq_engineers.engineers_id,
            rq_engineers.engineers_name,
            rq_engineers.money,
            rq_engineers.temp_money,
            rq_engineers.engineers_number,
            rq_engineers.engineers_phone,
            rq_engineers.reg_time,
            rq_administrative_info.company_name AS a_name,
            rq_operation_info.company_name AS o_name,
            rq_engineers.engineers_status
        ';
        $join = [
            ['administrative_info','rq_engineers.administrative_id = rq_administrative_info.a_id','LEFT'],
            ['operation_info','rq_engineers.operation_id = rq_operation_info.o_id','LEFT'],
        ];
        $order = [
            'reg_time'=>'DESC'
        ];

        $this->Achievement->table = 'engineers';
        $result = $this->Achievement->selectPageData($where,$field,$join,$row,$page,$order);

        foreach ($result as $key=>$value){
            $result[$key]['money'] = formatMoney($value['money'],2);
            $result[$key]['temp_money'] = formatMoney($value['temp_money'],2);
            $result[$key]['reg_time'] = empty($value['reg_time']) ? '' : date('Y-m-d H:i:s',$value['reg_time']);
            $result[$key]['engineers_status'] = ($value['engineers_status'] == 1) ? '冻结' : '正常';
        }

        $count = $this->Achievement->findJoinData($where,'COUNT(*) AS number',$join);
        $total_money = $this->Achievement->findJoinData($where,'SUM(money) AS total_money',$join);//累计账户余额
        $total_temp_money = $this->Achievement->findJoinData($where,'SUM(temp_money) AS total_temp_money',$join);//累计冻结金额

        $count = $count['number'] ?? 0;

        $page = [
            'current_page'=>$page,
            'row'=>$row,
            'total_number'=>$count,
            'total_money'=>formatMoney($total_money['total_money'])??0,
            'total_temp_money'=>formatMoney($total_temp_money['total_temp_money'])??0,
            'total_page'=>ceil($count / $row)
        ];

        $data = [
            'data'=>$result,
            'page'=>$page
        ];

        return $this->jsonend(1000,'获取成功',$data);

    }

    /**
     * 获取工程师傅详情
     * @desc 描述
     * 参数名      类型      是否必须    说明
     * id           int         是       工程师傅ID
     * @author ligang
     * @RequestType GET|POST
     * @date 2019/4/12 16:03
     */
    public function http_engineersInfo()
    {
        $id = $this->parm['id'] ?? '';
        if (empty($id)){
            return $this->jsonend(-1000,'请选择工程师傅');
        }
        $where = [
            'engineers_id'=>$id,
            'rq_engineers.is_delete'=>0
        ];
        $field = '
            rq_engineers.engineers_id,
            rq_engineers.engineers_name,
            rq_engineers.engineers_number,
            rq_engineers.engineers_phone,
            rq_engineers.reg_time,
            rq_engineers.money,
            rq_engineers.gender,
            rq_engineers.department,
            rq_engineers.login_time,
            rq_engineers.engineers_idcard,
            rq_engineers.birthday,
            rq_engineers.engineers_address,
            rq_engineers.note,
            rq_engineers.avatar_img,
            rq_administrative_info.company_name AS a_name,
            rq_operation_info.company_name AS o_name,
            rq_engineers.engineers_status
        ';
        $join = [
            ['administrative_info','rq_engineers.administrative_id = rq_administrative_info.a_id','LEFT'],
            ['operation_info','rq_engineers.operation_id = rq_operation_info.o_id','LEFT'],
        ];
        $this->Achievement->table = 'engineers';
        $result = $this->Achievement->findJoinData($where,$field,$join);
        if (empty($result)){
            return $this->jsonend(-1000,'工程师傅不存在或已被删除');
        }
        $result['reg_time'] = empty($result['reg_time']) ? '' : date('Y-m-d H:i:s',$result['reg_time']);
        $result['login_time'] = empty($result['login_time']) ? '' : date('Y-m-d H:i:s',$result['login_time']);
        $result['birthday'] = empty($result['birthday']) ? '' : date('Y-m-d H:i:s',$result['birthday']);
        $result['avatar_img'] = UploadImgPath($result['avatar_img']);
        return $this->jsonend(1000,'获取成功',$result);
    }

    /**
     * 编辑工程师傅信息（金额以及特殊属性除外）
     * @desc 描述
     * 参数名              类型      是否必须        说明
     * engineers_id         int         是           工程师傅ID
     * engineers_name       string      是           姓名
     * engineers_phone      string      是           电话
     * gender               int         是           性别，0:未知，1：男性，2:女性
     * engineers_status     int         是           状态，1：冻结，2：正常
     * department           string      是           工程部 部门
     * engineers_idcard     string      是           身份证号码
     * birthday             string      是           生日
     * engineers_address    string      是           家庭地址
     * note                 string      是           备注
     * @author ligang
     * @RequestType POST
     * @date 2019/4/12 16:16
     */
    public function http_editEngineers()
    {
        //去除空格
        foreach ($this->parm as $key=>$value){
            $this->parm[$key] = trim($value);
        }

        $engineers_id = $this->parm['engineers_id'] ?? '';
        $engineers_name = $this->parm['engineers_name'] ?? '';
        $engineers_phone = $this->parm['engineers_phone'] ?? '';
        $gender = $this->parm['gender'] ?? '';
        $engineers_status = $this->parm['engineers_status'] ?? '';
        $department = $this->parm['department'] ?? '';
        $engineers_idcard = $this->parm['engineers_idcard'] ?? '';
        $birthday = $this->parm['birthday'] ?? '';
        $engineers_address = $this->parm['engineers_address'] ?? '';
        $note = $this->parm['note'] ?? '';

        if (empty($engineers_id)){
            return $this->jsonend(-1000,'请选择工程师傅');
        }
        if (empty($engineers_name)){
            return $this->jsonend(-1000,'请输入姓名');
        }
        if (strlen($engineers_name) > 10){
            return $this->jsonend(-1000,'姓名最多输入10个字符');
        }
        if (empty($engineers_phone)){
            return $this->jsonend(-1000,'请输入电话');
        }
        if (!isMobile($engineers_phone)){
            return $this->jsonend(-1000,'请输入正确的电话');
        }
        if (empty($gender)){
            return $this->jsonend(-1000,'请选择性别');
        }
        if (!in_array($gender,[0,1,2])){
            return $this->jsonend(-1000,'请选择正确的性别');
        }
        if (empty($engineers_status)){
            return $this->jsonend(-1000,'请选择状态');
        }
        if (!in_array($engineers_status,[1,2])){
            return $this->jsonend(-1000,'请选择正确的状态');
        }
        if (empty($engineers_idcard)){
            return $this->jsonend(-1000,'请输入身份证号码');
        }
        if (!FunIsSFZ($engineers_idcard)){
            return $this->jsonend(-1000,'请输入正确的身份证号码');
        }
        if (empty($birthday)){
            return $this->jsonend(-1000,'请输入或选择出生日期');
        }
        if (empty($engineers_address)){
            return $this->jsonend(-1000,'请输入地址');
        }

        $this->Achievement->table = 'engineers';
        $result = $this->Achievement->findData(['engineers_id'=>$engineers_id]);
        if (empty($result)){
            return $this->jsonend(-1000,'工程师傅不存在');
        }

        $data = [
            'engineers_name'=>$engineers_name,
            'engineers_phone'=>$engineers_phone,
            'gender'=>$gender,
            'engineers_status'=>$engineers_status,
            'department'=>$department ?? $result['department'],
            'engineers_idcard'=>$engineers_idcard,
            'birthday'=>strtotime($birthday),
            'engineers_address'=>$engineers_address,
            'note'=>$note ?? $result['note'],
        ];

        $this->Achievement->updateData(['engineers_id'=>$engineers_id],$data);

        return $this->jsonend(1000,'编辑成功');
    }

    /**
     * 获取工程师傅账户变动记录
     * @desc 获取工程师傅账户变动记录
     * 参数名      类型  是否必须    说明
     * id           int     否       工程师傅ID，当id不存在时获取所有的工程师傅
     * page         int     否       当前页码，默认1
     * row          int     否       每页显示数目，默认10
     * time_frame   string  否       时间范围，开始时间和结束时间中间用~分割，如：2019-4-12 ~ 2019-4-12
     * source       int     否       来源，1：兑换，2：结算
     * account_type int     否       账户类型，1：支出，2：收入
     * @author ligang
     * @RequestType POST
     * @date 2019/4/12 16:53
     */
    public function http_fundsRecord()
    {
        $id = $this->parm['id'] ?? '';
//        if (empty($id)){
//            return $this->jsonend(-1000,'请选择工程人员');
//        }
        $page = $this->parm['page'] ?? 1;
        $row = $this->parm['row'] ?? 10;
        $time_frame = $this->parm['time_frame'] ?? '';
        $source = $this->parm['source'] ?? '';
        $account_type = $this->parm['account_type'] ?? '';

        $where = [
            'user_type'=>2
        ];
        if (!empty($id)){
           $where['rq_funds_record.user_id'] = $id;
        }
        if (!empty($account_type)){
            $where['account_type'] = $account_type;
        }
        if (!empty($source)){
            $where['source'] = $account_type;
        }

        //处理时间
        if (!empty($time_frame) && stripos($time_frame, '~') === false) {
            return $this->jsonend(-1000, '时间格式错误');
        }
        if (!empty($time_frame)) {
            $time_frame = explode('~', $time_frame);
            $star_time = trim($time_frame[0]) . ' 00:00:00';
            $end_time = trim($time_frame[1]) . ' 23:59:59';
            $where['add_time'] = ['BETWEEN', [strtotime($star_time), strtotime($end_time)]];
        }

        $field = '
            rq_funds_record.user_type,
            rq_funds_record.source,
            rq_funds_record.account_type,
            rq_funds_record.money,
            rq_funds_record.note,
            rq_funds_record.add_time,
            rq_engineers.engineers_name,
            rq_engineers.engineers_number,
            rq_engineers.engineers_phone
        ';
        $join = [
            ['engineers','rq_funds_record.user_id = rq_engineers.engineers_id','LEFT']
        ];
        $order = [
            'add_time'=>'DESC'
        ];
        $this->Achievement->table = 'funds_record';
        $result = $this->Achievement->selectPageData($where,$field,$join,$row,$page,$order);

        foreach ($result as $key=>$value){
            $result[$key]['add_time'] = empty($value['add_time']) ? '' : date('Y-m-d H:i:s',$value['add_time']);
            $result[$key]['source'] = ($value['source'] == 1) ? '兑换' : '结算';
            $result[$key]['account_type'] = ($value['account_type'] == 1) ? '支出' : '收入';
        }

        $count = $this->Achievement->findJoinData($where,'COUNT(*) AS number',$join);
        $count = $count['number'] ?? 0;

        //统计
        if (empty($account_type) || $account_type == 2){
            //总支出
            $where['account_type'] = 1;
            $total_pay = $this->Achievement->findJoinData($where,'SUM(rq_funds_record.money) AS number',$join);
        }
        if (empty($account_type) || $account_type == 1) {
            //总收入
            $where['account_type'] = 2;
            $total_income = $this->Achievement->findJoinData($where, 'COUNT(rq_funds_record.money) AS number', $join);
        }
        $page = [
            'current_page'=>$page,
            'row'=>$row,
            'total_number'=>$count,
            'total_page'=>ceil($count / $row)
        ];

        $data = [
            'data'=>$result,
            'page'=>$page,
            'count'=>[
                'total_pay'=>$total_pay['number'] ?? 0,
                'total_income'=>$total_income['number'] ?? 0,
            ]
        ];
        return $this->jsonend(1000,'获取成功',$data);
    }

    /**
     * 修改金额
     * @desc 修改金额
     * 参数名  类型      是否必须    说明
     * id       int      是       工程师傅ID
     * money    string   是       金额，正数增加，负数减少，特定符号：+或-
     * note     string   否       备注
     * @author ligang
     * @RequestType GET|POST
     * @throws \Server\CoreBase\SwooleException
     * @date 2019/4/15 18:47
     */
    public function http_editMoney()
    {
        $id = $this->parm['id'] ?? '';
        if (empty($id)){
            return $this->jsonend(-1000,'请选择工程师傅');
        }
        $money = $this->parm['money'] ?? '';
        if (empty($money)){
            return $this->jsonend(-1000,'请输入金额');
        }
        //判断金额
        $reg = '/^(\-|\+?|[1-9])\d+(\.\d+)?$/';
        if (!preg_match($reg,$money)){
            return $this->jsonend(-1000,'请输入正确的金额');
        }
        $note = $this->parm['note'] ?? '管理员修改';

        //验证工程人员
        $this->Achievement->table = 'engineers';
        $info = $this->Achievement->findData(['engineers_id'=>$id],'money,engineers_status');
        if (empty($info)){
            return $this->jsonend(-1000,'工程师傅不存在');
        }
        if ($info['engineers_status'] != 2){
            return $this->jsonend(-1000,'工程师傅状态异常');
        }

        //判断金额加减
        $is_add = false;
        if (stripos($money,'-') !== false){
            //减
            $money = substr($money,1);
        }elseif (stripos($money,'+') !== false){
            //加
            $money = substr($money,1);
            $is_add = true;
        }else{
            $is_add = true;
        }

        $account_log = [
            'user_id' => $id,
            'user_type' => 2,
            'source' => 3,
            'account_type' => $is_add ? 2 : 1,
            'money' => $money,
            'note' => $note,
            'add_time' => time(),
        ];

        $update_data = [
            'money'=>$info['money']
        ];
        $update_data['money']  = $is_add ? $update_data['money'] + $money : $update_data['money'] - $money;
        $check = false;
        $this->db->begin(function ()use (&$check,$account_log,$update_data,$id){
            $this->Achievement->table = 'funds_record';
            $this->Achievement->insertData($account_log);
            $this->Achievement->table = 'engineers';
            $this->Achievement->updateData(['engineers_id'=>$id],$update_data);
            $check = true;
        },function ($e){
            return $this->jsonend(-1000,'操作失败',$e->error);
        });
        if ($check){
            return $this->jsonend(1000,'操作成功');
        }
        return $this->jsonend(-1000,'操作失败');
    }

    /**
     * 工程师傅业绩列表
     * @desc 工程师傅业绩列表
     * 请求参数         请求类型      是否必须      说明
     * row              int             否           每页显示数目
     * page             int             否           当前页码
     * search           string          否           关键字
     * c_time           string          否           结算时间（起始时间之间用~分割）
     * @author ligang
     * @RequestType POST
     * @date 2019/5/10 15:24
     */
    public function http_bill()
    {
        $row = $this->parm['row'] ?? 10;
        $page = $this->parm['page'] ?? 1;
        $search = $this->parm['search'] ?? '';
        $c_time = $this->parm['c_time'] ?? '';

        $where = [];
        $where['rq_oa_achievement_workers.role'] = 2;
        if (!empty($search)){
            $where['rq_customer.telphone|rq_customer.realname|rq_customer_code.code|rq_customer_code_bill.bill_number'] = ['LIKE','%'.trim($search).'%'];
        }
        if (!empty($c_time)) {
            $c_time = explode('~', $c_time);
            $star_time = trim($c_time[0]) . ' 00:00:00';
            $end_time = trim($c_time[1]) . ' 23:59:59';
            $where['rq_oa_engineers_performance.add_time'] = ['BETWEEN', [strtotime($star_time), strtotime($end_time)]];
        }

        $field = "
                rq_customer.telphone,
                rq_customer.realname,
                rq_customer_code.code,
                rq_oa_achievement_workers.role,
                rq_oa_achievement_workers.workers_position,
                rq_oa_achievement_workers.source,
                rq_oa_achievement_workers.money,
                rq_customer_code_bill.bill_number,
                rq_engineers.engineers_name,
                rq_engineers.engineers_phone,
                rq_oa_achievement_workers.add_time
            ";
        $join = [
            ['oa_achievement', 'rq_oa_achievement_workers.achievement_id = rq_oa_achievement.achievement_id', 'LEFT'],
            ['customer_code_bill', 'rq_customer_code_bill.bill_id = rq_oa_achievement.form_bill_id', 'LEFT'],
            ['customer_code', 'rq_customer_code. CODE = rq_customer_code_bill.customer_code', 'LEFT'],
            ['engineers', 'rq_engineers.engineers_id = rq_oa_achievement_workers.workers_id', 'LEFT'],
            ['customer', 'rq_customer.user_id = rq_customer_code.user_id', 'LEFT'],
        ];
        $order = [
            'rq_oa_achievement_workers.add_time' => 'DESC',
            'rq_oa_achievement_workers.achievement_id' => 'DESC',
            'rq_oa_achievement_workers.sort' => 'ASC',
        ];

        $this->Achievement->table = 'oa_achievement_workers';
        $result = $this->Achievement->selectPageData($where,$field,$join,$row,$page,$order);
        $count = $this->Achievement->findJoinData($where, 'COUNT(*) AS number', $join);
        $achievement_source = $this->config->get('achievement_source');
        $achievement_role = $this->config->get('achievement_role');
        $workers_position = $this->config->get('workers_position');
        $result_data = [];
        foreach ($result as $key=>$value){
            $name = '';
            if ($value['role'] == 1) {
                $name = '市场推广';
            } elseif ($value['role'] == 3) {
                $name = "承包商";
            }
            $result_data[$key]['name_phone'] = $value['realname'] . "(" . $value['telphone'] . ")";
            $result_data[$key]['code'] = $value['code'];
            $result_data[$key]['bill_number'] = $value['bill_number'];
            $result_data[$key]['role'] = $achievement_source[$value['source']];
            $result_data[$key]['workers_position'] = $workers_position[$value['workers_position']] . $name;
            $result_data[$key]['money'] = formatMoney($value['money']);
            $result_data[$key]['engineers_name'] = $value['engineers_name']."(".$value['engineers_phone'].")";
            $result_data[$key]['add_time'] = !empty($value['add_time']) ? date('Y-m-d H:i:s',$value['add_time']) : '';
        }
        $count = $count['number'] ?? 0;
        $money = $this->Achievement->findJoinData($where, 'SUM(rq_oa_achievement_workers.money) AS total', $join);
        $data = [
            'data' => $result_data,
            'total_money' => formatMoney($money['total']) ?? '0.00',
            'page' => [
                'current_page' => $page,
                'row' => $row,
                'total' => $count,
                'total_page' => ceil($count / $row)
            ]
        ];
        return $this->jsonend(1000, '获取成功', $data);
    }

    /**
     * 删除文件
     * @author ligang
     * @param string $path      文件或目录，目录以/结尾
     * @param string $suffix    文件后缀，空为所有文件，区分大小写，目录有效
     * @param bool $today       是否检查当天文件，true当天文件不会被删除，目录有效
     * @return bool
     * @date 2019/1/29 9:57
     */
    public function delFile(string $path, string $suffix = '', bool $today = true) {
        if (is_dir($path)) {
            $date = date('Ymd', time());
            //是个目录
            $file_array = scandir($path);
            foreach ($file_array as $key => $value) {
                if ($value == '.' || $value == '..') {
                    continue;
                }
                $file_path = $path . $value;
                if ($today) {
                    $filectime = filectime($file_path);
                    if ($filectime !== false && date('Ymd', $filectime) == $date) {
                        continue;
                    }
                }

                if (!empty($suffix)) {
                    if (stripos($value, $suffix) === false) {
                        continue;
                    }
                    unlink($file_path);
                } else {
                    unlink($file_path);
                }
            }
            return true;
        }
        unlink($path);
        return true;
    }

    /**
     * 导出工程师傅业绩明细
     * @author ligang
     * @RequestType POST
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @date 2019/6/14 16:12
     */
    public function http_exportEngineerSettlement()
    {

        $path = WWW_DIR . '/work_order/';
        //删除以前的文件
        $this->delFile($path, 'xlsx');

        $search = $this->parm['search'] ?? '';
        $c_time = $this->parm['c_time'] ?? '';

        $where = [];
        if (!empty($search)){
            $where['rq_engineers.engineers_name|rq_engineers.engineers_phone|rq_oa_achievement.contract_number'] = ['LIKE','%'.trim($search).'%'];
        }
        if (!empty($c_time)) {
            $time = explode('~', $c_time);
            $star_time = trim($time[0]) . ' 00:00:00';
            $end_time = trim($time[1]) . ' 23:59:59';
            $where['rq_oa_engineers_performance.add_time'] = ['BETWEEN', [strtotime($star_time), strtotime($end_time)]];
        }

        $field = '
            rq_oa_engineers_performance.money,
            rq_oa_engineers_performance.add_time,
            rq_engineers.engineers_name,
            rq_engineers.engineers_number,
            rq_engineers.engineers_phone,
            rq_oa_achievement.achievement_work_order_type,
            rq_oa_achievement.contract_number
        ';
        $join = [
            ['engineers','rq_oa_engineers_performance.engineers_id = rq_engineers.engineers_id','LEFT'],
            ['oa_achievement','rq_oa_engineers_performance.achievement_id = rq_oa_achievement.achievement_id','LEFT'],
        ];
        $order =[
            'rq_oa_engineers_performance.add_time'=>'DESC'
        ];

        $this->Achievement->table = 'oa_engineers_performance';
        $result = $this->Achievement->selectData($where,$field,$join,$order);
        foreach ($result as $key=>$value){
            //去除null
            $key_array = array_keys($value);
            foreach ($key_array as $k => $v) {
                if (empty($value[$v])) {
                    $result[$key][$v] = '';
                }
            }
            $result[$key]['add_time'] = !empty($value['add_time']) ? date('Y-m-d H:i:s',$value['add_time']) : '';
            $result[$key]['achievement_work_order_type'] = $value['achievement_work_order_type'] == 3 ? '新装':'续费';
            $result[$key]['contract_number'] = ' '.$value['contract_number'];
        }


        $tableTile = ['业务类型','合同编号', '工程姓名', '工程编号','工程电话', '结算金额', '结算时间'];
        $field = ['achievement_work_order_type','contract_number', 'engineers_name', 'engineers_number','engineers_phone', 'money', 'add_time'];
        $sheet_title = '工程结算明细'.$c_time;
        $title = $c_time . '工程结算明细';
        $file_name = date('YmdHis') . '工程结算明细';

        file_exists($path) || mkdir($path, 0777);
        $file_path = $path;
        $suffix = 'xlsx';
        $output = false;

        $xlx = $this->export($tableTile, $field, $sheet_title, $title, $result, $file_name, $file_path, $suffix, $output);
        $data = [
            'url' => 'work_order/' . $xlx
        ];
        return $this->jsonend(1000, 'ok', $data);
    }

}