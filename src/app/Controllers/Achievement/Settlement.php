<?php
/**
 * Created by PhpStorm.
 * User: LG
 * Date: 2019/1/11
 * Time: 15:26
 */

namespace app\Controllers\Achievement;

use app\Library\SLog;
use app\Services\Common\ConfigService;
use app\Services\Common\HttpService;
use app\Services\Common\ReturnCodeService;
use app\Services\Log\AsyncFile;
use app\Services\Log\FileLogService;
use app\Services\User\UserService;
use Endroid\QrCode\ErrorCorrectionLevel;
use think\image\Exception;

/**
 * 结算算法
 * Class Settlement
 * @package app\Controllers\Achievement
 */
class Settlement extends Base
{

    protected $extensionModel;
    protected $Achievement;

    public function initialization($controller_name, $method_name)
    {
        parent::initialization($controller_name, $method_name); // TODO: Change the autogenerated stub
        $this->extensionModel = $this->loader->model('ExtensionModel', $this);
        $this->Achievement = $this->loader->model('AchievementModel', $this);
    }

    /**
     * 结算
     */
    public function http_teamPerformance()
    {
        if (empty($this->parm['bill_id'])) {
            return $this->jsonend(-1000, '缺少参数bill_id');
        }
        $data = $this->extensionModel->settlement($this->parm['bill_id']);
        return $this->jsonend(1000, 'OK', $data);
    }

    /**
     * 更新关系
     */
    public function http_contractualRelationship()
    {
        if (empty($this->parm['contract_no'])) {
            return $this->jsonend(-1000, '缺少参数contract_no');
        }
        $data = $this->extensionModel->contractualRelationship($this->parm['contract_no']);
        return $this->jsonend(1000, 'OK', $data);
    }

    /**
     * 工程新装结算
     */
    public function http_newClothesPrice(){
        if (empty($this->parm['contract_id'])){
            return $this->jsonend(-1000, '缺少参数contract_id');
        }
        $data = $this->extensionModel->newClothesPrice($this->parm['contract_id']);
        return $this->jsonend(1000, 'OK', $data);
    }

    /**
     * 导出业绩
     */
    public function http_exportAchievement()
    {
        $type = $this->parm['type'] ?? 1;//1导出统计 2导出明细
        $start_time = empty($this->parm['start_time'] ?? '') ? 0 : strtotime($this->parm['start_time'] . '00:00:00');
        $end_time = empty($this->parm['end_time'] ?? '') ? strtotime(date('Y-m-d 23:59:59', time())) : strtotime($this->parm['end_time'] . '23:59:59');
        //获取2019装机的合同
        $this->Achievement->table = 'contract';
        $lists = $this->Achievement->selectData(['status' => ['IN', [4, 5, 6]], 'installed_time' => ['between', [$start_time, $end_time], 'company_id' => $this->company]], 'contract_no,installed_time,status,contract_id');
        $contrac_no_arr = [];
        $contract_data = [];
        $i = 0;
        foreach ($lists as $k => $v) {
            $achievement_time = $v['installed_time'];
            //如果是2019.1.1前装机的根据派单时间
            if ($v['installed_time'] < strtotime('2019-01-01')) {
                $achievement_time = '';
                $this->Achievement->table = 'work_order';
                $join = [
                    ['work_order_business wob', 'wob.work_order_id=rq_work_order.work_order_id', 'left']
                ];
                $work_order_info = $this->Achievement->findJoinData(['contract_id' => $v['contract_id'], 'wob.business_id' => 3], 'order_time', $join);
                if (!empty($work_order_info)) {
                    $achievement_time = $work_order_info['order_time'];
                }
            }
            if (empty($achievement_time)) {
                // FileLogService::WriteLog(SLog::DEBUG_LOG, 'exportAchievement', $v['contract_no'] . '-装机时间小于2019.1.1并且派单时间为空' . PHP_EOL);
                continue;
            }
            if ($achievement_time < $end_time) {
                //判断是否结算
                $this->Achievement->table = 'oa_achievement';
                $achievement = $this->Achievement->findJoinData(['contract_number' => $v['contract_no'], 'achievement_work_order_type' => 3, 'state' => 1], 'achievement_id', []);
                if (empty($achievement)) {
                    continue;
                }
                array_push($contrac_no_arr, $v['contract_no']);
                $i++;
                // FileLogService::WriteLog(SLog::DEBUG_LOG, 'exportAchievement', $v['contract_no'] . '-' . date('Y-m-d H:i:s', $achievement_time) . PHP_EOL);
            }

        }
        if ($type == 2) {
            //组装导出明细数据
            foreach ($contrac_no_arr as $k => $v) {
                $this->Achievement->table = 'contract_rank';
                $contract_rank = $this->Achievement->findData(['contract_no' => $v], 'manage_id');
                $this->Achievement->table = 'oa_workers';
                $workers = $this->Achievement->findJoinData(['rq_oa_workers.user_id' => $contract_rank['manage_id']], 'workers_name,workers_phone,workers_entry_time,inspection_standard');
                if (empty($workers['workers_phone'])) {
                    continue;
                }
                $item['workers_entry_time'] = date('Y年m月d日', $workers['workers_entry_time']);
                $item['name'] = $workers['workers_name'] . '(联系电话:' . $workers['workers_phone'] . ')';
                $item['contract_no'] = $v['contract_no'] . ' ';
                $item['installed_time'] = date('Y年m月d日', $v['installed_time']);
                $item['num'] = '1台';
                $contract_data[] = $item;
            }

            $sheet_title = '物联网业绩明细表';
            $title = '物联网业绩明细表';
            $file_name = date('YmdHis') . '-物联网业绩明细表';
            $path = WWW_DIR . '/export_achievement/';
            file_exists($path) || mkdir($path, 0777);
            $file_path = $path;
            $suffix = 'xlsx';
            $output = false;
            $tableTile = ['合同编号', '装机时间', '市场推广姓名', '升级（入职）时间', '业绩'];
            $field = ['contract_no', 'installed_time', 'name', 'workers_entry_time', 'num'];
            $xlx = $this->export($tableTile, $field, $sheet_title, $title, $contract_data, $file_name, $file_path, $suffix, $output);
            $url = $this->config->get('callback_domain_name');
            if ($this->dev_mode == 1) {
                $url = $this->config->get('debug_config.callback_domain_name');
            }
            $data = [
                'url' => $url . '/export_achievement/' . $xlx
            ];
            return $this->jsonend(1000, 'ok', $data);
        } else {
            // FileLogService::WriteLog(SLog::DEBUG_LOG, 'exportAchievement', "共" . $i . '个合同' . PHP_EOL);
            $this->Achievement->table = 'contract_rank';
            $where['contract_no'] = ['IN', $contrac_no_arr];
            $contract_rank = $this->Achievement->selectData($where, 'manage_id,count(*) count', [], [], 'manage_id');
            $data = [];
            foreach ($contract_rank as $k => $v) {
                $this->Achievement->table = 'oa_workers';
                $join = [
                    ['customer c', 'c.user_id=rq_oa_workers.user_id', 'left']
                ];
                $workers = $this->Achievement->findJoinData(['rq_oa_workers.user_id' => $v['manage_id']], 'workers_name,workers_phone,workers_entry_time,inspection_standard,c.city,c.area', $join);
                if (empty($workers['workers_phone'])) {
                    continue;
                }
                $item['area'] = $workers['city'] . $workers['area'];
                $item['workers_entry_time'] = date('Y年m月d日', $workers['workers_entry_time']);
                $item['role'] = '市场推广';
                $item['name'] = $workers['workers_name'] . '(联系电话:' . $workers['workers_phone'] . ')';
                $item['num'] = $v['count'] . '台';
                $data[] = $item;
                //FileLogService::WriteLog(SLog::DEBUG_LOG, 'exportAchievement', $workers['workers_name'] . '(联系电话:' . $workers['workers_phone'] . ')共' . $v['count'] . '台' . PHP_EOL);
            }
            $sheet_title = '物联网业绩表';
            $title = '物联网业绩表';
            $file_name = date('YmdHis') . '-年物联网业绩表';
            $path = WWW_DIR . '/export_achievement/';
            file_exists($path) || mkdir($path, 0777);
            $file_path = $path;
            $suffix = 'xlsx';
            $output = false;
            $tableTile = ['地区', '升级（入职）时间', '角色', '姓名', '累计业绩'];
            $field = ['area', 'workers_entry_time', 'role', 'name', 'num'];
            $xlx = $this->export($tableTile, $field, $sheet_title, $title, $data, $file_name, $file_path, $suffix, $output);
            $url = $this->config->get('callback_domain_name');
            if ($this->dev_mode == 1) {
                $url = $this->config->get('debug_config.callback_domain_name');
            }
            $data = [
                'url' => $url . '/export_achievement/' . $xlx
            ];
        }
        return $this->jsonend(1000, 'ok', $data);


    }


    public function http_examineAchievementDetail()
    {
        //接收参数
        $type = $this->parm['type'] ?? -1;//1导出统计 2导出明细
        $keywords = $this->parm['keywords'] ?? '';//关键字搜索
        $page = $this->parm['page'] ?? 1;
        $pageSize = $this->parm['pageSize'] ?? 10;
        $c_time = $this->parm['c_time'] ?? '';
        $start_time = empty($this->parm['start_time'] ?? '') ? 0 : strtotime($this->parm['start_time'] . '00:00:00');
        $end_time = empty($this->parm['end_time'] ?? '') ? strtotime(date('Y-m-d 23:59:59', time())) : strtotime($this->parm['end_time'] . '23:59:59');
        //如果查询某个人的业绩,仅查询考核周期内的
        if (!empty($this->user_id) || !empty($this->parm['user_id'])) {
            $user_id = empty($this->parm['user_id']) ? $this->user_id : $this->parm['user_id'];
            $this->Achievement->table = 'oa_workers';
            $workers_info = $this->Achievement->findJoinData(['user_id' => $user_id, 'is_delete' => 0], 'inspection_start_time,inspection_end_time', []);
            if (!empty($workers_info)) {
                $start_time = $workers_info['inspection_start_time'];
                $end_time = $workers_info['inspection_end_time'];
            }
        }
        //业绩系统传的时间格式
        if (!empty($c_time) && stripos($c_time, '~') === false) {
            return $this->jsonend(-1000, '时间格式错误');
        }
        if (!empty($c_time)) {
            $c_time = explode('~', $c_time);
            $start_time = strtotime(trim($c_time[0]) . ' 00:00:00');
            $end_time = strtotime(trim($c_time[1]) . ' 23:59:59');
        }
        //获取2019装机的合同
        $this->Achievement->table = 'oa_achievement';
        $join = [
            ['contract', 'rq_contract.contract_no=rq_oa_achievement.contract_number', 'left'],
        ];
        $lists = $this->Achievement->selectData(['rq_contract.status' => ['IN', [4, 5, 6]], 'rq_contract.installed_time' => ['between', [$start_time, $end_time], 'rq_contract.company_id' => $this->company, 'achievement_work_order_type' => 3]], 'rq_contract.contract_no,rq_contract.installed_time,rq_contract.status,rq_contract.contract_id', $join);
        if (empty($lists)) {
            return $this->jsonend(-1000, '暂无数据');
        }
        $contrac_no = [];
        $i = 0;
        foreach ($lists as $k => $v) {
            $achievement_time = $v['installed_time'];
            //如果是2019.1.1前装机的根据派单时间
            if ($v['installed_time'] < strtotime('2019-01-01')) {
                $achievement_time = '';
                $this->Achievement->table = 'work_order';
                $join = [
                    ['work_order_business wob', 'wob.work_order_id=rq_work_order.work_order_id', 'left']
                ];
                $work_order_info = $this->Achievement->findJoinData(['contract_id' => $v['contract_id'], 'wob.business_id' => 3], 'order_time', $join);
                if (!empty($work_order_info)) {
                    $achievement_time = $work_order_info['order_time'];
                }
            }
            if (!empty($achievement_time) && $achievement_time < $end_time) {
                //判断是否结算
                $this->Achievement->table = 'oa_achievement';
                $achievement = $this->Achievement->findJoinData(['contract_number' => $v['contract_no'], 'achievement_work_order_type' => 5, 'state' => 1], 'achievement_id', []);
                if (empty($achievement)) {
                    continue;
                }
                array_push($contrac_no, $v['contract_no']);
                $i++;
                // FileLogService::WriteLog(SLog::DEBUG_LOG, 'exportAchievement', $v['contract_no'] . '-' . date('Y-m-d H:i:s', $achievement_time) . PHP_EOL);
            }
        }
        //查询数据
        $this->Achievement->table = 'oa_achievement_workers';
        $joins = [
            ['oa_achievement h', 'h.achievement_id=rq_oa_achievement_workers.achievement_id', 'left'],
            ['oa_workers w', 'w.workers_id=rq_oa_achievement_workers.workers_id', 'left'],
            ['contract c', 'c.contract_no=h.contract_number', 'left'],
        ];

        $field = 'rq_oa_achievement_workers.id,
                rq_oa_achievement_workers.money,
                rq_oa_achievement_workers.is_available,
                rq_oa_achievement_workers.source,
                rq_oa_achievement_workers.workers_position,
                rq_oa_achievement_workers.add_time,
                h.contract_number,
                h.contract_customer,
                h.equipments_number,          
                h.achievement_work_order_type,          
                w.workers_id,
                w.workers_name,
                w.workers_phone,
                w.workers_number,
                w.workers_entry_time,
                w.city,
                w.area,
                w.check_out,
                w.inspection_total,
                w.inspection_results,
                w.inspection_standard,
                w.inspection_start_time,
                w.inspection_end_time,
                c.installed_time';
        $where = [
            'rq_oa_achievement_workers.workers_position' => 3,
            'h.contract_number' => ['IN', $contrac_no],
            'c.company_id' => $this->company,
        ];
        if (!empty($this->parm['user_id'])) {
            $where['w.user_id'] = $this->parm['user_id'];
        }
        if (!empty($keywords)) {
            $where['h.contract_number|h.contract_customer'] = ['LIKE', '%' . trim($keywords) . '%'];
        }
        $order = [
            'h.add_time' => 'desc'
        ];
        $inspection_info = [];
        $workers_id = [];

        $result = $this->Achievement->selectPageData($where, $field, $joins, $pageSize, $page, $order, 'h.contract_number,w.workers_id');
        if (empty($result)) {
            return $this->jsonend(-1000, '暂无数据');
        }
        foreach ($result as $k => $v) {
            $result[$k]['inspection_start_time'] = $v['inspection_start_time'] ? date('Y-m-d H:i', $v['inspection_start_time']) : '--';
            $result[$k]['inspection_end_time'] = $v['inspection_end_time'] ? date('Y-m-d H:i', $v['inspection_end_time']) : '--';
            $result[$k]['add_time'] = $v['add_time'] ? date('Y-m-d H:i', $v['add_time']) : '--';
            $result[$k]['installed_time'] = $v['installed_time'] ? date('Y-m-d H:i', $v['installed_time']) : '--';
            $result[$k]['inspection_date'] = dateFormats($v['inspection_start_time']) . ' 至 ' . dateFormats($v['inspection_end_time']);

            //市场推广详情
            $inspection_info['inspection_results'] = $v['inspection_results'] . ' ' ?? 0;
            $inspection_info['inspection_standard'] = $v['inspection_standard'] . ' ' ?? 0;
            $inspection_info['inspection_date'] = dateFormats($v['inspection_start_time']) . ' 至 ' . dateFormats($v['inspection_end_time']);

            //组装导出明细数据
            $item['name'] = $v['workers_name'] . '(tel:' . $v['workers_phone'] . ')';
            $item['contract_number'] = $v['contract_number'] . ' ';
            $item['installed_time'] = $v['installed_time'] ? date('Y年m月d日', $v['installed_time']) : '--';
            $item['workers_entry_time'] = $v['workers_entry_time'] ? date('Y年m月d日', $v['workers_entry_time']) : '--';
            $item['inspection_date'] = $inspection_info['inspection_date'];
            $item['equipments_number'] = $v['equipments_number'] . ' ' ?? '';
            $item['inspection_total'] = $v['inspection_total'] . ' ' ?? 0;
            $item['inspection_results'] = $inspection_info['inspection_results'];
            $item['inspection_standard'] = $inspection_info['inspection_standard'];
            $contract_data[] = $item;
            array_push($workers_id, $v['workers_id']);

        }
        //$count = $this->Achievement->findJoinData($where, 'COUNT(*) AS number', $joins);
        $count = count($result) ?? 0;
        $data = [
            'inspection_info' => $inspection_info,
            'data' => $result,
            'page' => [
                'current_page' => $page,
                'row' => $pageSize,
                'total' => $count,
                'total_page' => ceil($count / $pageSize)
            ]
        ];


        if ($type == 1) {
            $this->Achievement->table = 'oa_workers';
            $wheres['workers_id'] = ['IN', $workers_id];
            $work_list = $this->Achievement->selectPageData($wheres, '*', [], 9999999, 1, ['inspection_total' => 'desc'], 'workers_id');
            foreach ($work_list as $k => $v) {
                $work_list[$k]['workers_entry_time'] = $v['workers_entry_time'] ? date('Y年m月d日', $v['workers_entry_time']) : '--';
                $work_list[$k]['workers_sex'] = $v['workers_sex'] == 1 ? '男' : '女';
                $work_list[$k]['role'] = '市场推广';
                $work_list[$k]['inspection_date'] = dateFormats($v['inspection_start_time']) . ' 至 ' . dateFormats($v['inspection_end_time']);
                $work_list[$k]['inspection_total'] = $v['inspection_total'] . ' ' ?? 0;
                $work_list[$k]['inspection_results'] = $v['inspection_results'] . ' ' ?? 0;
                $work_list[$k]['inspection_standard'] = $v['inspection_standard'] . ' ' ?? 0;
                $work_list[$k]['workers_name'] = $v['workers_name'] . '(工号:' . $v['workers_number'] . ')';
            }
            //导出统计
            $sheet_title = '市场推广业绩考核统计表';
            $title = '市场推广业绩考核统计表';
            $file_name = date('YmdHis') . '-市场推广业绩考核统计表';
            $path = WWW_DIR . '/export_achievement/';
            file_exists($path) || mkdir($path, 0777);
            $file_path = $path;
            $suffix = 'xlsx';
            $output = false;
            //$tableTile = ['地区', '升级（入职）时间', '角色', '姓名', '主板台数','累计完成','实际完成','考核标准','考核周期'];
            //$field = ['area', 'workers_entry_time', 'role', 'name', 'equipments_number','inspection_total','inspection_results','inspection_standard','inspection_date'];

            $tableTile = ['角色', '员工', '区域', '升级（入职）时间', '电话', '性别', '累计完成/台', '实际完成/台', '考核标准/台', '考核周期'];
            $field = ['role', 'workers_name', 'area', 'workers_entry_time', 'workers_phone', 'workers_sex', 'inspection_total', 'inspection_results', 'inspection_standard', 'inspection_date'];
            $xlx = $this->export($tableTile, $field, $sheet_title, $title, $work_list, $file_name, $file_path, $suffix, $output);
            $url = $this->config->get('callback_domain_name');
            if ($this->dev_mode == 1) {
                $url = $this->config->get('debug_config.callback_domain_name');
            }
            $export_data = [
                //'url' => $url . '/export_achievement/' . $xlx
                'url' => 'export_achievement/' . $xlx
            ];
            return $this->jsonend(1000, 'ok', $export_data);

        } else if ($type == 2) {
            //组装导出明细数据
            $sheet_title = '市场推广业绩考核明细表';
            $title = '市场推广业绩考核明细表';
            $file_name = date('YmdHis') . '-市场推广业绩考核明细表';
            $path = WWW_DIR . '/export_achievement/';
            file_exists($path) || mkdir($path, 0777);
            $file_path = $path;
            $suffix = 'xlsx';
            $output = false;
            $tableTile = ['合同编号', '装机时间', '市场推广姓名', '升级（入职）时间', '主板台数/台', '实际完成/台', '考核标准/台', '考核周期'];
            $field = ['contract_number', 'installed_time', 'name', 'workers_entry_time', 'equipments_number', 'inspection_results', 'inspection_standard', 'inspection_date'];
            $xlx = $this->export($tableTile, $field, $sheet_title, $title, $contract_data, $file_name, $file_path, $suffix, $output);
            $url = $this->config->get('callback_domain_name');
            if ($this->dev_mode == 1) {
                $url = $this->config->get('debug_config.callback_domain_name');
            }
            $export_data = [
                //'url' => $url . '/export_achievement/' . $xlx
                'url' => 'export_achievement/' . $xlx
            ];
            return $this->jsonend(1000, 'ok', $export_data);

        } else {
            return $this->jsonend(1000, '获取成功', $data);
        }

    }

}