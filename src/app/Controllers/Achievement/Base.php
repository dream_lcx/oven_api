<?php

namespace app\Controllers\Achievement;

use app\Library\SLog;
use app\Services\Log\AsyncFile;
use app\Services\Log\FileLogService;
use Server\CoreBase\Controller;
use Monolog\Logger;
use Server\SwooleMarco;
use app\Wechat\WxPay;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\IOFactory;
use app\Services\Common\ConfigService;
use Server\CoreBase\SwooleRedirectException;
use Server\CoreBase\SwooleException;
use app\Services\Company\CompanyService;

/*
 * 基础类
 */

class Base extends Controller
{

    protected $parm;
    protected $user_id;
    protected $user_info;
    protected $log_model;
    protected $addr_model;
    protected $equip_area_model;
    protected $area_model;
    protected $admin_info_model;
    protected $time_model;
    protected $contract_log_model;
    protected $work_log_model;
    protected $user_model;
    protected $user_log_model;
    protected $dev_mode;
    protected $app_debug;
    protected $wx_config;
    protected $alipay;
    protected $max_bind_number = 5;
    protected $engineer_wx_name;
    protected $user_wx_name;
    protected $username;
    protected $engineer_wx_config;
    protected $spread_wx_config;

    public function initialization($controller_name, $method_name)
    {
        $this->http_output->setHeader('Access-Control-Allow-Origin', '*');
        $this->http_output->setHeader("Access-Control-Allow-Headers", " Origin, X-Requested-With, Content-Type, Accept,token,company,openid,role");
        $this->http_output->setHeader("Access-Control-Allow-Methods", " POST");
        parent::initialization($controller_name, $method_name);
        $this->user_id = $this->request->user_id;
        $this->company = $this->request->company;
        $this->username = $this->request->username ?? '';
        $this->parm = json_decode($this->http_input->getRawContent(), true);
        $this->log_model = $this->loader->model('OrderLogModel', $this);
        $this->addr_model = $this->loader->model('UseraddressModel', $this);
        $this->equip_area_model = $this->loader->model('EquipmentsAdministrativeModel', $this);
        $this->admin_info_model = $this->loader->model('AdministrativeInfoModel', $this);
        $this->area_model = $this->loader->model('AreaModel', $this);
        $this->time_model = $this->loader->model('WorkOrderTimeModel', $this);
        $this->contract_log_model = $this->loader->model('ContractLogModel', $this);
        $this->work_log_model = $this->loader->model('WorkOrderLogModel', $this);
        $this->user_model = $this->loader->model('CustomerModel', $this);
        $this->user_log_model = $this->loader->model('CustomerLogModel', $this);
        //判断用户状态是否正常
        if (!empty($this->user_id)) {
            $userinfo = $this->user_model->getOne(array('user_id' => $this->user_id), 'is_delete,account_status');
            if (!empty($userinfo) && ($userinfo['is_delete'] == 1 || $userinfo['account_status'] == 2)) {
                return $this->jsonend(-1006, "该账号已被冻结,更多信息请联系客服");
            }
        }
        //开发者模式
        $this->dev_mode = ConfigService::getConfig('develop_mode', false, $this->company);
        //调试模式
        $this->app_debug = ConfigService::getConfig('app_debug', false, $this->company);
        $company_config = CompanyService::getCompanyConfig($this->company);
        $this->wx_config = $company_config['wx_config'];
        $this->engineer_wx_config = $company_config['engineer_wx_config'];
        $this->spread_wx_config = $company_config['spread_wx_config'];
        $this->engineer_wx_name = $company_config['engineer_wx_config']['app_wx_name'] ?? '华灶';
        $this->user_wx_name = $company_config['wx_config']['app_wx_name'] ?? '华灶';
        $this->alipay = $this->config->get('alipay');
    }

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 异常的回调(如果需要继承$autoSendAndDestroy传flase)
     * @param \Throwable $e
     * @param callable $handle
     */
    public function onExceptionHandle(\Throwable $e, $handle = null)
    {
        $log = '--------------------------[报错信息]----------------------------' . PHP_EOL;
        $log .= 'client：Achievement' . PHP_EOL;
        $log .= 'DATE：' . date("Y-m-d H:i:s") . PHP_EOL;
        $log .= 'getMessage：' . $e->getMessage() . PHP_EOL;
        $log .= 'getCode：' . $e->getCode() . PHP_EOL;
        $log .= 'getFile：' . $e->getFile() . PHP_EOL;
        $log .= 'getLine：' . $e->getLine();
       FileLogService::WriteLog(SLog::ERROR_LOG,'error',$log);
        //必须的代码
        if ($e instanceof SwooleRedirectException) {
            $this->http_output->setStatusHeader($e->getCode());
            $this->http_output->setHeader('Location', $e->getMessage());
            $this->http_output->end('end');
            return;
        }
        if ($e instanceof SwooleException) {
         FileLogService::WriteLog(SLog::ERROR_LOG,'error',$e->getMessage() . "\n" . $e->getTraceAsString());
        }
        //可以重写的代码
        if ($handle == null) {
            switch ($this->request_type) {
                case SwooleMarco::HTTP_REQUEST:
                    $e->request = $this->request;
                    $data = get_instance()->getWhoops()->handleException($e);
                    $this->http_output->setStatusHeader(500);
                    $this->http_output->end($data);
                    break;
                case SwooleMarco::TCP_REQUEST:
                    $this->send($e->getMessage());
                    break;
            }
        } else {
            \co::call_user_func($handle, $e);
        }
    }

    /**
     * 返回json格式
     */
    public function jsonend($code = '', $message = '', $data = '', $gzip = true)
    {
        $output = array('code' => $code, 'message' => $message, 'data' => $data);

        if (!$this->canEnd()) {
            return;
        }
        if (!get_instance()->config->get('http.gzip_off', false)) {
            //低版本swoole的gzip方法存在效率问题
//            if ($gzip) {
//                $this->response->gzip(1);
//            }
            //压缩备用方案
            /* if ($gzip) {
              $this->response->header('Content-Encoding', 'gzip');
              $this->response->header('Vary', 'Accept-Encoding');
              $output = gzencode($output . " \n", 9);
              } */
        }
        if (is_array($output) || is_object($output)) {
            $this->response->header('Content-Type', 'application/json; charset=UTF-8');
            $output = json_encode($output, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            //$output = "<pre>$output</pre>";
        }
        $this->response->end($output);
        $this->endOver();
    }

    /**
     * @desc   调起支付
     * @param  $order_info [openid,order_money,order_sn]
     * @date   2018-07-18
     * @return [type]     [description]
     * @author lcx
     */
    public function payOrder($order_info, $callback)
    {
        $wx_config = $this->wx_config;
        $wx_config['ip'] = $this->request->header['x-real-ip'] ?? $this->request->server['remote_addr'];
        //$wxpay = new WxPay($wx_config);
        $wxpay = WxPay::getInstance($wx_config);
        if ($wxpay->isSetConfig) $wxpay->setConfig($wx_config);
        $order['openid'] = $order_info['openid'];
        $order['body'] = $wx_config['app_wx_name'];
        $order['out_trade_no'] = $order_info['order_sn'];
        $order_money = $order_info['order_money'];
        if ($this->app_debug == 1) {
            $order_money = 0.01;
        }
        $order['total_fee'] = $order_money * 100;
        if ($this->dev_mode == 1) {
            $url = $this->config->get('debug_config.callback_domain_name') . '/User/Callback/' . $callback;
        } else {
            $url = $this->config->get('callback_domain_name') . '/User/Callback/' . $callback;
        }

        $order['notify_url'] = $url; //微信回调地址
        $order['trade_type'] = 'JSAPI';
        $order['attach'] = '1';
        $wxpay->setParameter($order);
        $timeStamp = time();
        $jsApiObj['appId'] = $wx_config['appId'];
        $jsApiObj["timeStamp"] = "$timeStamp";
        $jsApiObj["nonceStr"] = $wxpay->createNoncestr();
        $prepay_id = $wxpay->getPrepayId();
        $jsApiObj["package"] = "prepay_id=" . $prepay_id;
        $jsApiObj["signType"] = "MD5";
        $jsApiObj["paySign"] = $wxpay->getSign($jsApiObj);
        return $jsApiObj;
    }

    /**
     * @desc   判断选择地址是否在产品服务区内
     * @param
     * @date   2018-07-19
     * @return [type]     [description]
     * @author lcx
     */
    public function isInService($address_id, $equipments_id)
    {
        $address_info = $this->addr_model->getOne(array('address_id' => $address_id), '*');
        if (empty($address_info)) {
            return false;
        }
        //是否所属全省/市
        $map['provice_code'] = ['IN', [-1, $address_info['provice_code']]];
        $map['city_code'] = ['IN', [-1, $address_info['city_code']]];
        $map['area_code'] = ['IN', [-1, $address_info['area_code']]];
        $map['equipments_id'] = $equipments_id;
        $info = $this->equip_area_model->getOne($map, 'city_code,area_code');
        if (!empty($info)) {
            return true;
        }
        return false;
    }



    /**
     * @desc   根据地址code获取详细信息
     * @param
     * @date   2018-07-19
     * @return [type]     [description]
     * @author lcx
     */
    public function getAreaInfo($code)
    {
        $data = $this->area_model->getOne(array('id' => $code), '*');
        if (!empty($data)) {
            return $data;
        }
        return '';
    }

    /**
     * @desc   添加订单操作日志
     * @param
     * @date   2018-07-19
     * @return [type]     [description]
     * @author lcx
     */
    public function addOrderLog($order_id, $status, $remark, $is_to_user = 0)
    {
        $data['operater_role'] = 1;
        $data['order_id'] = $order_id;
        $data['status'] = $status;
        $data['create_time'] = time();
        $data['remark'] = $remark;
        $data['is_to_user'] = $is_to_user;
        $res = $this->log_model->add($data);
        return $res;
    }

    /**
     * @desc   添加工单操作日志
     * @param
     * @date   2018-07-19
     * @return [type]     [description]
     * @author lcx
     */

    public function addWorkOrderLog($work_order_id, $time, $status, $remark)
    {
        $log['work_order_id'] = $work_order_id;
        $log['create_work_time'] = $time;
        $log['operating_time'] = time();
        $log['do_id'] = $this->user_id;
        $log['operating_type'] = 2;
        $log['operating_status'] = $status;
        $log['remarks'] = $remark;
        $res = $this->work_log_model->add($log);
        return $res;
    }

    /**
     * @desc   添加合同操作日志
     * @param $status 1增2删3改4解绑'
     * @date   2018-07-19
     * @author lcx
     * @return [type]     [description]
     */
    public function addContractLog($contract_id, $status = 1, $remark)
    {
        $data['contract_id'] = $contract_id;
        $data['do_id'] = $this->user_id;
        $data['do_type'] = $status;
        $data['terminal_type'] = 3;
        $data['do_time'] = time();
        $data['remark'] = $remark;
        $res = $this->contract_log_model->add($data);
        return $res;
    }

    /**
     * @desc   添加用户登录日志
     * @param $user_id 用户信息
     * @param $client_info 客户端信息
     * @date   2018-07-19
     * @return [type]     [description]
     * @author lcx
     */
    public function addUserLoginLog($user_id, $client_info)
    {
        $data['operator_type'] = 2;
        $data['operator_id'] = $user_id;
        $user_info = $this->user_model->getOne(array('user_id' => $user_id), 'realname,username,account');
        if (!empty($user_info)) {
            $data['operator_name'] = $user_info['username'];
            $data['operator_account'] = $user_info['account'];
            $data['customer_id'] = $user_id;
            $data['add_time'] = time();
            $data['terminal_type'] = 3;
            $data['client_ip'] = $this->request->server['remote_addr'];
            $data['client_device_brand'] = $client_info['client_device_brand']; //品牌
            $data['client_device_model'] = $client_info['client_device_model']; //型号
            $data['client_device_version'] = $client_info['clinet_device_wx_version'] . '-' . $client_info['clinet_device_system_version'] . '-' . $client_info['clinet_device_base_version'];
            $data['client_device_platform'] = $client_info['client_device_platform'];
            return $this->user_log_model->add($data);
        }
        return false;
    }

    /**
     * @desc   获取时间范围详情
     * @param
     * @date   2018-07-24
     * @return [type]     [description]
     * @author lcx
     */
    public function getRangeInfo($time_id)
    {
        $info = $this->time_model->getOne(array('time_id' => $time_id));
        if (!empty($info)) {
            return $info;
        }
        return false;
    }

    /**
     * 公共导出方法
     * @param array $tableTile 表头
     * @param array $field 字段名称，与表头对应
     * @param string $sheet_title sheet名称
     * @param string $title 内容标题
     * @param array $data 导出数据
     * @param string $file_name 文件名
     * @param string $file_path 文件储存路径
     * @param string $suffix 文件后缀
     * @param bool $output 是否输出
     * @return string
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @date 2019/1/28 16:36
     * @author ligang
     */
    function export(array $tableTile, array $field, string $sheet_title, string $title, array $data, string $file_name, string $file_path, string $suffix = 'xlsx', bool $output = false)
    {
        $filename = $file_name . '.' . strtolower($suffix);
        switch (strtolower($suffix)) {
            case 'xlsx':
                $writerType = 'Xlsx';
                break;
            case 'xls':
                $writerType = 'Xls';
                break;
            case 'ods':
                $writerType = 'Ods';
                break;
            case 'csv':
                $writerType = 'Csv';
                break;
            case 'html':
                $writerType = 'Html';
                break;
            case 'tcpdf':
                $writerType = 'Tcpdf';
                break;
            case 'dompdf':
                $writerType = 'Dompdf';
                break;
            case 'mpdf':
                $writerType = 'Mpdf';
                break;
            default:
                throw new \Exception('文件格式不存在：' . $suffix, 500);
        }
        if (stripos($file_path, $filename) === false) {
            if (!file_exists($file_path)) {
                throw new \Exception('目录不存在：' . $file_path, 500);
            }
            $save_path = $file_path . $filename;
        } else {
            $save_path = $file_path;
        }

        $spreadsheet = get_instance()->Spreadsheet;
        //$Spreadsheet = new Spreadsheet();

        $spreadsheet->getProperties()
            ->setCreator("Maarten Balliauw")
            ->setLastModifiedBy("Maarten Balliauw")
            ->setTitle("Office 2007 XLSX Test Document")
            ->setSubject("Office 2007 XLSX Test Document")
            ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
            ->setKeywords("office 2007 openxml php")
            ->setCategory("Test result file");

        //设置sheet名
        $spreadsheet->getActiveSheet()->setTitle($sheet_title);

        //设置表头
        $spreadsheet->getActiveSheet()->fromArray($tableTile, null, 'A2');

        $letter = 'A';
        for ($i = 0; $i < count($tableTile) - 1; $i++) {
            if ($letter == 'A') {
                $spreadsheet->getActiveSheet()->getColumnDimension($letter)->setWidth(22);
            }
            //操过Z后是从AA-AZ，BA-BZ,CA-CZ....
            $letter++;
            if ($letter != 'A') {
                $spreadsheet->getActiveSheet()->getColumnDimension($letter)->setWidth(22);
            }
        }

        $titleStyleArray = [
            'font' => [
                'bold' => true,
                'size' => 14
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
            ],
        ];
        //合并单元格，并设置标题和样式
        $spreadsheet->getActiveSheet()->mergeCells('A1:' . $letter . '1');
        $spreadsheet->getActiveSheet()->setCellValue('A1', $title);
        $spreadsheet->getActiveSheet()->getStyle('A1')->applyFromArray($titleStyleArray);

        //设置数据部分样式
        $dataStyleArray = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
            ],
        ];
        $spreadsheet->getActiveSheet()->getStyle('A2:' . $letter . (count($data) + 3))->applyFromArray($dataStyleArray);

        //设置行高(参数：pRow是表的行数，从一开始)
        $spreadsheet->getActiveSheet()->getRowDimension('1')->setRowHeight(50);

        //设置列宽
        //$spreadsheet->getActiveSheet()->getColumnDimension($letter)->setWidth(22);
        //设置默认列宽
        //$spreadsheet->getActiveSheet()->getColumnDimension($letter)->setAutoSize(true);

        $k = 3;
        foreach ($data as $key => $value) {
            $curr_letter = 'A';
            for ($j = 0; $j < count($tableTile); $j++) {
                $spreadsheet->getActiveSheet()->setCellValue($curr_letter . $k, $value[$field[$j]]);
                if ((is_numeric($value[$field[$j]]) || is_float($value[$field[$j]])) && $value[$field[$j]] < 9999) {
                    //设置单元格格式
                    $spreadsheet->getActiveSheet()->getStyle($curr_letter . $k)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
                }
//            elseif (strtotime($value[$field[$j]])){
//                $spreadsheet->getActiveSheet()->getStyle($curr_letter . $k)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_DATE_YYYYMMDDSLASH);
//            }
                $curr_letter++;
            }
            $k++;
        }

        if ($output) {
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="' . $filename . '"');
            header('Cache-Control: max-age=0');
            $writer = IOFactory::createWriter($spreadsheet, $writerType);
            $writer->save('php://output');
            return false;
        } else {
            $writer = IOFactory::createWriter($spreadsheet, $writerType);
            $writer->save($save_path);
            return $filename;
        }
    }



}
