<?php

namespace app\Tasks;

use Server\CoreBase\Task;
use Qiniu\Auth;
use Qiniu\Storage\UploadManager;
use app\Services\Common\EmailService;
use app\Services\Common\NotifiyService;
use app\Services\Common\ConfigService;
use app\Services\Common\SmsService;

/**
 * 异步任务
 */
class AppTask extends Task
{

    protected $accessKey;
    protected $secrectKey;
    protected $bucket;

    public function initialization($task_id, $from_id, $worker_pid, $task_name, $method_name, $context)
    {
        parent::initialization($task_id, $from_id, $worker_pid, $task_name, $method_name, $context);
        $this->accessKey = $this->config->get('qiniu.accessKey');
        $this->secrectKey = $this->config->get('qiniu.secrectKey');
        $this->bucket = $this->config->get('qiniu.bucket');

        //启用备用redis连接池
        $this->redis = $this->loader->redis("redisPoolSpare", $this);
    }



    /**
     * 异步上传本地服务器图片到七牛云
     */
    public function sysnUploadToQiniu()
    {
        $param = func_get_args(); //接收参数
        //$auth = new Auth($this->accessKey, $this->secrectKey); // 构建鉴权对象 #需要填写你的 Access Key 和 Secret Key
        $auth = get_instance()->QiniuAuth;
        $token = $auth->uploadToken($this->bucket); // 生成上传 Token
        //$uploadMgr = new UploadManager();
        $uploadMgr = get_instance()->QiniuUploadManager;
        try {
            list($img_ret, $img_err) = $uploadMgr->put($token, $param[1], $param[0]);
            if ($img_err == null) {
                echo $param[1] . '上传七牛云成功';
            }
        } catch (\Exception $e) {
        }
    }

    /**
     * 异步发送短信
     * @desc
     * @param [$phone,$con] $phone 手机号 $con 内容
     * @return bool
     * @date 2019-04-26
     * @author lcx
     */
    public function sendSmsTask()
    {
        $param = func_get_args(); //接收参数
        $phone = $param[0] ?? ''; //手机号
        $con = $param[1] ?? ''; //短信内容
        $sms_config = $param[2] ?? ''; //短信内容
        SmsService::sendBaMiSms($phone, $con,$sms_config); //调用短信服务
    }

    /**
     * 异步发送邮件
     * @desc
     * @param [$phone,$con] $phone 手机号 $con 内容
     * @return bool
     * @date 2019-04-26
     * @author lcx
     */
    public function sendEmailTask()
    {
        $param = func_get_args(); //接收参数
        $email = $param[0] ?? '';
        $content = $param[1] ?? '';
        $subject = $param[2] ?? '';
        $attach_path = $param[3] ?? '';
        $attach_name = $param[4] ?? '';
        $helo = $param[5] ?? '';
        $company_id = $param[6] ?? '';
        EmailService::sendEmail($email, $content, $subject, $attach_path, $attach_name, $helo,$company_id); //调用短信服务
    }

    /**
     * 消息通知
     * @param string $openid 小程序openid或者公众号openid
     * @param string $weapp_template_id 小程序模板ID
     * @param string $template_url 跳转小程序页面路径 pages/home/home?id=1
     * @param array $weapp_template_data 小程序模板数据
     * @param string $weapp_template_keyword 小程序放大关键字
     * @param string $mp_template_id 公众号模板消息ID
     * @param string $mp_template_data 公众号模板消息数据
     * @param string $sms_tel 发送短信电话
     * @param string $sms_content 发送短信内容
     * @return type
     */
    public function sendNotifiyTask()
    {
        $param = func_get_args(); //接收参数
        $openid = $param[0];
        $weapp_template_data = $param[1];
        $weapp_template_id = $param[2];
        $sms_tel = $param[3];
        $sms_content = $param[4];
        $template_url = $param[5] ?? '';
        $weapp_template_keyword = $param[6] ?? '';
        $mp_template_id = $param[7] ?? '';
        $mp_template_data = $param[8] ?? '';
        $name = $param[9] ?? '';
        $company_config = $param[10] ?? '';
        NotifiyService::sendNotifiy($openid, $weapp_template_data, $weapp_template_id, $sms_tel, $sms_content, $template_url, $weapp_template_keyword, $mp_template_id, $mp_template_data, $name,$company_config);
    }

}
