<?php

namespace app\Tasks;

use app\Services\Company\CompanyService;
use app\Services\Log\AsyncFile;
use app\Services\Log\FileLogService;
use app\Wechat\WxPay;
use Server\CoreBase\Task;
use app\Services\Common\ConfigService;
use app\Services\Common\NotifiyService;
use app\Library\SLog;

/**
 * 定时任务
 */
class TimeTask extends Task
{

    protected $GetIPAddressHttpClient;
    protected $coupon_model;
    protected $coupon_cate_model;
    protected $customerCodeBillModel;
    protected $customerCodeModel;
    protected $withdrawModel;

    public function initialization($task_id, $from_id, $worker_pid, $task_name, $method_name, $context)
    {
        parent::initialization($task_id, $from_id, $worker_pid, $task_name, $method_name, $context);
        $this->GetIPAddressHttpClient = get_instance()->getAsynPool('BuyWater');
        $this->coupon_model = $this->loader->model('CouponModel', $this);
        $this->coupon_cate_model = $this->loader->model('CouponCateModel', $this);
        $this->customerCodeBillModel = $this->loader->model('CustomerCodeBillModel', $this);
        $this->customerCodeModel = $this->loader->model('CustomerCodeModel', $this);
        $this->withdrawModel = $this->loader->model('WithdrawModel', $this);

        //启用备用redis连接池
        $this->redis = $this->loader->redis("redisPoolSpare", $this);
    }


    // 优惠券定时任务
    public function couponTask()
    {
        $mysql = get_instance()->config['mysql'];
        $this->prefix = $mysql[$mysql['active']]['prefix'] ?? '';

        // 1-- 获取所有今天已失效的优惠券
        $where['uid'] = ['>', 0]; // 表示领取了的
        $where['status'] = 1;  // 待使用的
        $where['valid_time_end'] = strtotime(date("Y-m-d", strtotime("-1 day")));
        $coupon_data = $this->db->select('*')
            ->from($this->prefix . 'coupon')
            ->TPWhere($where)
            ->query()
            ->result_array();
        unset($where);

        // 2-- 修改已失效的优惠券状态
        if ($coupon_data) {
            foreach ($coupon_data as $k => $v) {
                $where['coupon_id'] = $v['coupon_id'];
                $data['status'] = 3;
                $this->db->update($this->prefix . 'coupon')
                    ->set($data)
                    ->TPWhere($where)
                    ->query()
                    ->affected_rows();
                unset($where);
            }
        }
    }

    //户号账单出账
    public function outBillData()
    {
//        if (!in_array(date('H'), [13,14,15, 16, 17,20, 22, 23])) {
//            return false;
//        }
        //是否是本月最后一天
        $next_month = strtotime(date('m', strtotime("+1 day")));
        $last_day = false;
        if (date('m') != $next_month) {
            $last_day = true;
        }
        $where['out_bill_date'] = date('d');
        if ($last_day) {
            $where['out_bill_date'] = ['>=', date('d')];
        }
        $where['state'] = 1;
        $where['rq_customer_code.is_delete'] = 0;
        $where['c.status'] = 11;
        $json = [
            ['contract c', 'c.contract_id = rq_customer_code.contract_id'],
            ['customer u', 'u.user_id = rq_customer_code.user_id'],
        ];
        $data = $this->db->select('code,c.company_id,payment_amount,u.telphone,u.openid')
            ->from('rq_customer_code')
            ->TPJoin($json)
            ->TPWhere($where)
            ->query()
            ->result_array();
        if (empty($data)) {
            return false;
        }
        foreach ($data as $k => $v) {
            $result = $this->customerCodeBillModel->outBill($v['code']);
        }

    }


    //查询银行打款信息
    public function inquiryBank()
    {
        //查询审核通过兑换单子
        $where['cash_order_state'] = 2;//审核通过
        $where['play_money_type'] = 1;//线上打款
        $where['play_money_state'] = ['NOT IN', [3]];//打款成功

        $mysql = get_instance()->config['mysql'];
        $this->prefix = $mysql[$mysql['active']]['prefix'] ?? '';
        $data = $this->db->select('cash_order_id,cash_order_number,cash_order_state,play_money_type,play_money_state')
            ->from($this->prefix . 'withdraw')
            ->TPWhere($where)
            ->query()
            ->result_array();
        if (empty($data)) {
            var_dump(date('Y-m-d') . '暂无待查询的线上兑换订单');
        }
        //查询打款信息
        $presentationLog = '[' . date('Y-m-d H:i:s') . '] ';
        foreach ($data as $k => $v) {
            $result = $this->queryBank($v['cash_order_number']);
            /*foreach ($result as $key => $value) {
                if (is_array($value)) {
                    $presentationLog .= $key . ':' . json_encode($value) . ';';
                } else {
                    $presentationLog .= $key . ':' . $value . ';';
                }
            }
            AsyncFile::write('presentationLog', $presentationLog);*/

            if ($result == false) {
                var_dump('订单' . $v['cash_order_number'] . '查询失败');
            }
            if ($result['return_code'] != 'SUCCESS') {
                var_dump('订单' . $v['cash_order_number'] . '查询失败,失败原因：' . $result['return_msg']);
            }
            if ($result['result_code'] != 'SUCCESS') {
                var_dump('订单' . $v['cash_order_number'] . '查询失败,错误代码：' . $result['err_code'] . ';错误代码描述：' . $result['err_code_des']);
            }

            $update = [];
            if (isset($result['status']) && $result['status'] == 'PROCESSING') {
                //（处理中，如有明确失败，则返回额外失败原因；否则没有错误原因）
                $update['error_msg'] = '打款中...';
                $update['play_money_state'] = 2;
            } elseif (isset($result['status']) && $result['status'] == 'SUCCESS') {
                //（付款成功）
                $update['error_msg'] = '打款付款成功';
                $update['play_money_state'] = 3;
            } elseif (isset($result['status']) && $result['status'] == 'FAILED') {
                //（付款失败,需要替换付款单号重新发起付款）
                $update['error_msg'] = '付款失败，请重新审核';
                $update['cash_order_number'] = pay_sn('DWD');
                $update['cash_order_state'] = 1;
                $update['play_money_state'] = 1;
            } elseif (isset($result['status']) && $result['status'] == 'BANK_FAIL') {
                //（银行退票，订单状态由付款成功流转至退票,退票时付款金额和手续费会自动退还）
                $update['error_msg'] = '银行退票';
                $update['cash_order_state'] = 1;
                $update['play_money_state'] = 1;
            } else {
                $update['error_msg'] = $result['return_msg'];
                var_dump('订单' . $v['cash_order_number'] . '查询成功,状态异常：' . $result['return_msg']);
            }
            //修改查询信息
            if (!empty($update)) {
                $res = $this->db->update($this->prefix . 'withdraw')
                    ->set($update)
                    ->TPwhere(['cash_order_id' => $v['cash_order_id']])
                    ->query()
                    ->affected_rows();
            }

        }

    }


    /**
     * 查询打款银行卡信息
     * @param string $order_number
     * @return mixed
     * @throws \Exception
     * @date 2019/7/2 11:45
     * @author ligang
     */
    protected function queryBank(string $order_number)
    {
        $config = $this->config->get('wx_pay');
        //$wx = new WxPay($config);
        $wx = WxPay::getInstance($config);
        if ($wx->isSetConfig) $wx->setConfig($config);
        $param = [
            'mch_id' => $config['mchid'],
            'partner_trade_no' => $order_number,
            'nonce_str' => $wx->createNoncestr(),
        ];
        $param['sign'] = $wx->getSign($param);
        $xml = $wx->arrayToXml($param);
        $wx->url = $wx::GET_APY_BANK;
        $result = $wx->postXmlSSLCurl($xml, $wx->url);
        if ($result === false) {
            return false;
        }
        return $wx->xmlToArray($result);
    }

    /**
     * 缴费通知
     */
    public function billPayWarn()
    {
        //每天15点整通知
        if (!in_array(date('d'), [1, 15]) || date('H') != 14 || date('i') != 0 || date('s') != 0) {
            return false;
        }
        $mysql = get_instance()->config['mysql'];
        $this->prefix = $mysql[$mysql['active']]['prefix'] ?? '';

        $where['status'] = 1;
        $data = $this->db->select('customer_code,money,cycle_end_time,cycle_start_time,bill_number')
            ->from($this->prefix . 'customer_code_bill')
            ->TPWhere($where)
            ->order(['cycle_start_time' => 'ASC'])
            ->query()
            ->result_array();
        if (!empty($data)) {
            //发送模板消息--续费提醒
            //通知到用户
            foreach ($data as $k => $v) {
                $user_notice_config = ConfigService::getTemplateConfig('bill_pay_notice', 'user', $v['company_id']);
                if (!empty($user_notice_config) && !empty($user_notice_config['template']) && $user_notice_config['template']['switch']) {
                    $template_id = $user_notice_config['template']['wx_template_id'];
                    $mp_template_id = $user_notice_config['template']['mp_template_id'];
                    $str = '尊敬的用户,您好!。为了保证您的正常使用，';

                    $mp_template_data = [
                        'first' => ['value' => '尊敬的用户,您好!您有一份账单需要及时缴费,账单编号:' . $v['bill_number'], 'color' => '#4e4747'],
                        'keyword1' => ['value' => formatMoney($v['money'], 2)],
                        'keyword2' => ['value' => date('Y-m-15', strtotime('+1 month', $v['cycle_start_time'])) . '之前'],
                        'remark' => ['value' => $str . '请及时进入微信小程序进行续费。感谢您一直以来对我们的支持!点击卡片即可进入续费'],
                    ];
                    $content = $str . '请及时进行续费。感谢您一直以来对我们的支持!';
                    $url = '';
                    $company_config = CompanyService::getCompanyConfig(1);
                    //查询用户
                    $join = [
                        ['customer as c', $this->prefix . 'customer_bind_equipment.user_id = c.user_id', 'inner']
                    ];
                    $users = $this->db->select('telphone,openid')
                        ->from($this->prefix . 'customer_bind_equipment')
                        ->TPWhere(['customer_code'=>$v['customer_code'],'is_owner'=>['IN',[2,3],'status'=>1]])
                        ->TPJoin($join)
                        ->order(['create_time' => 'ASC'])
                        ->query()
                        ->result_array();
                    if(!empty($users)){
                        foreach ($users as $kk=>$vv){
                            NotifiyService::sendNotifiy($vv['openid'], [], $template_id, $vv['telphone'], $content, $url, '', $mp_template_id, $mp_template_data, 'user', $company_config);
                        }
                    }

                }
            }

        }
    }


}
