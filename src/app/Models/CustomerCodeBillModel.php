<?php

namespace app\Models;

use app\Services\Common\ConfigService;
use app\Services\Common\NotifiyService;
use app\Services\Common\ReturnCodeService;
use app\Services\Company\CompanyService;
use Server\CoreBase\Model;


class CustomerCodeBillModel extends Model
{
    // 表名
    protected $dbName = 'customer_code_bill';
    protected $customerCodeModel;
    protected $customerBindEquipmentModel;

    public function initialization(&$context)
    {
        parent::initialization($context);
        $this->customerCodeModel = $this->loader->model('CustomerCodeModel', $this);
        $this->customerBindEquipmentModel = $this->loader->model('CustomerBindEquipmentModel', $this);
    }

    //列表
    public function getAll(array $where, string $field = '*', $order = [], $table = '')
    {

        if (empty($table)) {
            $table = $this->dbName;
        }
        $result = $this->db->select($field)
            ->from($table)
            ->TPWhere($where)
            ->order($order)
            ->query()
            ->result_array();
        return $result;
    }

    //详情
    public function getOne(array $where, string $field = "*")
    {
        $result = $this->db
            ->select($field)
            ->from($this->dbName)
            ->TPWhere($where)
            ->query()
            ->row();
        return $result;
    }

    //新增一条数据
    public function add(array $data, $table = '')
    {
        if (empty($table)) {
            $table = $this->dbName;
        }
        $id = $this->db->insert($table)
            ->set($data)
            ->query()
            ->insert_id();
        return $id;
    }

    /**
     * @desc  更新信息
     * @param 无
     * @date   2018-07-18
     * @param array $where [description]
     * @param array $data [description]
     * @return [type]            [description]
     * @author lcx
     */
    public function save(array $where, array $data)
    {
        $result = $this->db->update($this->dbName)
            ->set($data)
            ->TPwhere($where)
            ->query()
            ->affected_rows();
        return $result;
    }

    public function count(array $map, array $join = array())
    {
        $result = $this->db->select('*')
            ->from($this->dbName)
            ->TPWhere($map)
            ->TPJoin($join)
            ->query()
            ->num_rows();
        return $result;
    }

    /**
     * 出账日==签约次日
     * @param $time 时间戳,暂定根据签约时间
     */
    public function outBillDate($time)
    {
        //默认1号
        if (empty($time)) {
            return 1;
        }
        $day = 1;//第几天
        $tomorrow_time = $time + 86400 * $day;
        $day = date('d', $tomorrow_time);
        return $day;
    }

    /**
     * 出账
     * @param $customer_code
     * @param $way 出账方式 1:自动出账,每个月1号出账 2:自动出账
     * @return array
     */
    public function outBill($customer_code, $way = 1)
    {
        $prefix = '';
        if ($way == 1) {
            $prefix = 'rq_';
        }
        if (empty($customer_code)) {
            return returnResult(ReturnCodeService::FAIL, "缺少参数:户号");
        }

        $join = [
            ['contract', 'rq_contract.contract_id=rq_customer_code.contract_id', 'left'],
        ];
        $customer_code_info = $this->customerCodeModel->getOne(['rq_customer_code.code' => $customer_code, 'rq_contract.status' => ['IN', [11, 12]]], 'code_id,code,rq_customer_code.company_id,payment_amount,out_bill_date,effect_time,payment_start_date', $join, $prefix . 'customer_code');
        var_dump(date('Y-m-d H:i:s') . $customer_code . '开始出账,生效时间:' . date('Y-m-d 0:0:0', $customer_code_info['effect_time']));
        if (empty($customer_code_info)) {
            return returnResult(ReturnCodeService::FAIL, $customer_code . '出账设置金额有误');
        }
        //账单周期
        $cycle_start_date = $customer_code_info['payment_start_date'];//账单周期开始时间
        $bill_data = $this->getAll(['customer_code' => $customer_code, 'is_delete' => 0, 'bill_type' => 1], 'cycle_end_time,create_time', ['cycle_end_time' => 'DESC'], $prefix . 'customer_code_bill');
        if (!empty($bill_data)) {
            $cycle_start_date = date('Y-m-d 0:0:0', strtotime("+1 day", $bill_data[0]['cycle_end_time']));//账单周期开始时间
        }
        $cycle_date = getTheMonth($cycle_start_date);//账单周期
        $cycle_start_time = strtotime($cycle_start_date);
        $cycle_end_time = strtotime($cycle_date[1] . ' 23:59:59');

        if ($way == 1 && $cycle_end_time > time()) {
            return returnResult(ReturnCodeService::FAIL, $customer_code . "该户号未到出账日");
        }
        $payment_amount_data = json_decode($customer_code_info['payment_amount'], true);
        if (empty($payment_amount_data)) return returnResult(ReturnCodeService::FAIL, $customer_code . "户号信息错误");
        $payment_amount = []; //缴费金额数组
        foreach ($payment_amount_data['data'] as $key => $value) {
            $payment_amount[$key + 1] = $value['A_first_year_moeny'];
        }
        if (date('Y-m', $customer_code_info['effect_time']) == date('Y-m', $cycle_start_time)) {
            $money = $payment_amount[1] / 2;
            if (date('d',$customer_code_info['effect_time']) > 15) $money = 0;
        } else {
            $current_time = time();//当前时间
            $money = $payment_amount[1];
            $data = [4, 3, 2, 1];
            foreach ($data as $key => $value) {
                $effect_time = strtotime(date("Y-m-d 23:59:59", strtotime("+" . $value . "years", $customer_code_info['effect_time']))); //4、3、2年缴费时间
                if ($effect_time < $current_time) {
                    $money = $payment_amount[$value];
                    break;
                }
            }
        }
        $add_data['customer_code'] = $customer_code;
        $add_data['bill_type'] = 1;
        $add_data['money'] = formatMoney($money, 1);
        $add_data['cycle_start_time'] = $cycle_start_time;
        $add_data['cycle_end_time'] = $cycle_end_time;
        $add_data['status'] = 1;
        $add_data['create_time'] = time();
        $add_data['bill_number'] = date('Ymd') . $customer_code . rand(100, 999);
        $result = $this->add($add_data, $prefix . 'customer_code_bill');
        if ($result) {
            $user_notice_config = ConfigService::getTemplateConfig('out_bill', 'user', 1);
            if (!empty($user_notice_config) && !empty($user_notice_config['template']) && $user_notice_config['template']['switch']) {
                $template_id = $user_notice_config['template']['wx_template_id'];
                $mp_template_id = $user_notice_config['template']['mp_template_id'];
                $month = date('m', $add_data['cycle_start_time']);
                $template_data = [
                    'keyword1' => ['value' => '户号:' . $add_data['customer_code']],
                    'keyword2' => ['value' => '缴费'],
                    'keyword3' => ['value' => '请及时进入微信小程序进行缴费。感谢您一直以来对我们的支持!点击卡片即可进入缴费'],
                ];
                $content = '尊敬的用户您好!您的' . $month . '月账单已经出账!户号:' . $add_data['customer_code'] . ',请及时进入微信小程序进行缴费。感谢您一直以来对我们的支持!';
                $url = 'pages/user/renew/renew?code=' . $add_data['customer_code'];
                $weapp_template_keyword = '';

                $mp_template_data = [
                    'first' => ['value' => '尊敬的用户您好!您的' . $month . '月账单已出,账单金额:' . formatMoney($add_data['money'], 2) . '元'],
                    'keyword1' => ['value' => $add_data['customer_code'], 'color' => '#4e4747'],
                    'keyword2' => ['value' => $month . '月', 'color' => '#4e4747'],
                    'keyword3' => ['value' => '点击查看账单详细内容,为确保您能正常使用，请及时进入微信小程序进行缴费!感谢您一直以来对我们的支持!客服电话:' . ConfigService::getConfig('company_hotline', false, 1), 'color' => '#4e4747'],
                    'remark' => ['value' => '', 'color' => '#173177'],
                ];
                //   FileLogService::WriteLog(SLog::DEBUG_LOG, 'task', $v['phone'] . '-' . $content);
                $company_config = CompanyService::getCompanyConfig(1);

            }
            //用户账单出账通知
            $joins = [
                ['customer u', 'u.user_id = rq_customer_bind_equipment.user_id']
            ];
            $where = [];
            $where['customer_code'] = $customer_code;
            $where['is_owner'] = ['IN',[2,3]];
            $users = $this->customerBindEquipmentModel->getAll($where,'openid,telphone',$joins);
            if (!empty($users) && !empty($user_notice_config) && !empty($user_notice_config['template']) && $user_notice_config['template']['switch']) {
               foreach ($users as $key=>$val){
                   NotifiyService::sendNotifiy($val['openid'], $template_data, $template_id, $val['telphone'], $content, $url, $weapp_template_keyword, $mp_template_id, $mp_template_data, 'user', $company_config);
               }
           }


            return returnResult(ReturnCodeService::SUCCESS, $customer_code . '出账成功', ['bill_id' => $result]);
        }
        return returnResult(ReturnCodeService::FAIL, $customer_code . '出账失败');

    }

}