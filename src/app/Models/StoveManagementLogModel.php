<?php

namespace app\Models;

use Server\CoreBase\Model;

class StoveManagementLogModel extends Model{

    //炉具管理
    // 表名
    protected $table = 'stove_management_log';


    /**
     * @desc  查询一条数据
     * @param 无
     * @date   2018-07-18
     * @param array $where [description]
     * @param string $field [description]
     * @return [type]            [description]
     * @author lcx
     */
    public function getOne(array $where, string $field = "*", array $join = array())
    {
        $result = $this->db
            ->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->TPJoin($join)
            ->query()
            ->row();
        return $result;
    }




}