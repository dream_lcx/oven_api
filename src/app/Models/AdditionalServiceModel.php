<?php

namespace app\Models;

use Server\CoreBase\Model;

/**   YSF
 *    额外服务模型
 *    Date: 2018/9/20
 * Class AdditionalServiceModel
 * @package app\Models
 */
class AdditionalServiceModel extends Model {

    // 表名
    protected $name = 'additional_service';

    // 列表获取
    public function getAll(string $field = '*', int $page = 1, int $pageSize = -1, array $order, array $where = array(),array $join = array()) {
        if ($pageSize < 0) {
            $result = $this->db->select($field)
                    ->from($this->name)
                    ->order($order)
                    ->TPJoin($join)
                    ->TPWhere($where)
                    ->query()
                    ->result_array();
        } else {
            $result = $this->db->select($field)
                    ->from($this->name)
                    ->page($pageSize, $page)
                    ->order($order)
                    ->TPJoin($join)
                    ->TPWhere($where)
                    ->query()
                    ->result_array();
        }

        return $result;
    }

    /**
     * @desc  查询一条数据
     * @param  无
     * @date   2018-07-24
     * @author lcx
     * @param  array      $where [description]
     * @param  string     $field [description]
     * @return [type]            [description]
     */
    public function getOne(array $where, string $field = "*") {
        $result = $this->db
                ->select($field)
                ->from($this->name)
                ->TPWhere($where)
                ->query()
                ->row();
        return $result;
    }

    /** lcx 
     * 新增
     * @param array $data 数组,添加的数据
     * @return type
     * @date 20180823
     */
    public function add(array $data) {
        $id = $this->db->insert($this->name)
                ->set($data)
                ->query()
                ->insert_id();
        return $id;
    }

}
