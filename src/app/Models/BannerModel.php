<?php
namespace app\Models;

use Server\CoreBase\Model;

/**   YSF
 *    广告banner Model
 *    Date: 2018/8/1
 * Class BannerModel
 * @package app\Models
 */
class BannerModel extends Model
{
    // 表名
    protected $dbName = 'banner';

    /**   YSF
     *    列表查询
     * @param array $where   查询条件
     * @param int $page      当前页
     * @param int $pageSize  每页条数
     * @param string $field  查询字段
     * @param array $order   排序方式
     * @return mixed
     */
    public function getAll(array $where, int $page, int $pageSize, string $field, array $order)
    {
        $result = $this->db->select($field)
                    ->from($this->dbName)
                    ->TPWhere($where)
                    ->page($pageSize, $page)
                    ->order($order)
                    ->query()
                    ->result_array();
        return $result;
    }

}