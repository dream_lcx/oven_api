<?php

namespace app\Models;

use Server\CoreBase\Model;

/**
 * 工单配件
 */
class WorkPartsModel extends Model {

    protected $table = 'work_parts';

    /**
     * @desc  查询一条数据
     * @param  无
     * @date   2018-07-18
     * @author lcx
     * @param  array      $where [description]
     * @param  string     $field [description]
     * @return [type]            [description]
     */
    public function getOne(array $where, string $field = "*") {
        $result = $this->db
                ->select($field)
                ->from($this->table)
                ->TPWhere($where)
                ->query()
                ->row();
        return $result;
    }

    /**    lcx
     *     链表查询
     * @param array $where
     * @param string $field
     * @param $join
     * @param array $order
     * @return mixed
     */
    public function getAlls(array $where, string $field = '*', array $join,array $order = ['id' => 'DESC'])
    {
        $result = $this->db->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->TPJoin($join)
            ->order($order)
            ->query()
            ->result_array();
        return $result;
    }

    /**
     * @desc   添加信息
     * @param  无
     * @date   2018-07-20
     * @author lcx
     * @param  array      $data [description]
     */
    public function add(array $data) {
        $id = $this->db->insert($this->table)
                ->set($data)
                ->query()
                ->insert_id();
        return $id;
    }

    /**   YSF
     *    查询列表
     * @param array $where    查询条件
     * @param int $page       当前页
     * @param int $pageSize   每页条数
     * @param array $join     连表
     * @param string $field   查询字段
     * @param array $order    排序方式
     * @return mixed
     */
    public function getAll(array $where, int $page, int $pageSize, array $join, string $field, array $order=['id'=>'asc']) {
        if ($pageSize < 0) {
            $result = $this->db->select($field)
                    ->from($this->table)
                    ->TPWhere($where)
                    ->TPJoin($join)
                    ->order($order)
                    ->query()
                    ->result_array();
        } else {
            $result = $this->db->select($field)
                    ->from($this->table)
                    ->TPWhere($where)
                    ->TPJoin($join)
                    ->page($pageSize, $page)
                    ->order($order)
                    ->query()
                    ->result_array();
        }
        

        return $result;
    }
     /**
     * 删除
     * @param array $where 条件
     * @return type
     */
    public function del(array $where){
        $result = $this->db->delete()
            ->from($this->table)
            ->TPwhere($where)
            ->query()
            ->affected_rows();               
        return $result;
    }

}
