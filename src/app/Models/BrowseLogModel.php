<?php
/**
 * Created by PhpStorm.
 * User: bg
 * Date: 2019-04-19
 * Time: 上午 10:49
 */

namespace app\Models;

use Server\CoreBase\Model;

class BrowseLogModel extends Model{
    // 表名
    protected $browseLogTableName = 'browse_log';

    /**
     *  插入浏览数据
     * @author bg
     * @param array $data
     * @return mixed
     * @date 2019-04-19 上午 10:51
     */
    public function insertBrowseLog(array $data){
        $result = $this->db->insert($this->browseLogTableName)
            ->set($data)
            ->query()
            ->insert_id();
        return $result;
    }

}