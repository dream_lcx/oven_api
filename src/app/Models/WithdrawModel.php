<?php

namespace app\Models;

use app\Services\Common\ReturnCodeService;
use app\Wechat\WxPay;
use Server\CoreBase\Model;


class WithdrawModel extends Model
{
    // 表名
    protected $dbName = 'withdraw';

    /**
     *    列表
     * @param array $where 查询条件
     * @param array $join 连表
     * @param int $page 当前页
     * @param int $pageSize 每页条数
     * @param string $field 查询字段
     * @param $order          排序方式
     * @return mixed
     */
    public function getAll(array $where, array $join, int $page, int $pageSize, string $field, $order)
    {
        if ($pageSize < 0) {
            $result = $this->db->select($field)
                ->from($this->dbName)
                ->TPJoin($join)
                ->TPWhere($where)
                ->order($order)
                ->query()
                ->result_array();
        } else {
            $result = $this->db->select($field)
                ->from($this->dbName)
                ->TPJoin($join)
                ->TPWhere($where)
                ->page($pageSize, $page)
                ->order($order)
                ->query()
                ->result_array();
        }


        return $result;
    }

    /**
     *    查询单条数据
     * @param array $where 查询条件
     * @param string $field 查询字段
     * @return null
     */
    public function getOne(array $where, string $field = '*', array $join = [])
    {
        $result = $this->db->select($field)
            ->from($this->dbName)
            ->TPWhere($where)
            ->TPJoin($join)
            ->query()
            ->row();
        return $result;
    }


    /**
     * @desc   添加信息
     * @param 无
     * @date   2018-07-24
     * @param array $data [description]
     * @author lcx
     */
    public function add(array $data)
    {
        $id = $this->db->insert($this->dbName)
            ->set($data)
            ->query()
            ->insert_id();
        return $id;
    }

    /**
     * @desc  更新信息
     * @param 无
     * @date   2018-07-24
     * @param array $where [description]
     * @param array $data [description]
     * @return [type]            [description]
     * @author lcx
     */
    public function save(array $where, array $data)
    {
        $result = $this->db->update($this->dbName)
            ->set($data)
            ->TPwhere($where)
            ->query()
            ->affected_rows();
        return $result;
    }
    /**
     * @desc   统计数量
     * @param 无
     * @date   2018-12-6
     * @param array $data [description]
     * @author lcx
     */
    public function count($map, $join = [])
    {
        $result = $this->db->select('*')
            ->from($this->dbName)
            ->TPWhere($map)
            ->TPJoin($join)
            ->query()
            ->num_rows();
        return $result;
    }

    //获取记录
    public function record($where, $field = '', $join = [], $page = 1, $pageSize = 10, $order = [])
    {
        $result = $this->getAll($where, $join, $page, $pageSize, $field, $order);
        if (empty($result)) {
            return returnResult(ReturnCodeService::NO_DATA, '暂无数据');
        }
        foreach ($result as $k => $v) {
            $result[$k]['remarks'] = empty($v['remarks']) ? '--' : $v['remarks'];
            $result[$k]['cash_order_money'] = formatMoney($v['cash_order_money'],2);
            $result[$k]['remit_money'] = formatMoney($v['remit_money'],2);
            $result[$k]['balance'] = formatMoney($v['balance'],2);
            $result[$k]['payment_fee'] = formatMoney($v['payment_fee'],2);
            if ($v['cash_order_state'] == 1) {
                $time = $v['add_time'];
                $state = '待审核';
            } elseif ($v['cash_order_state'] == 3) {
                $time = $v['examine_pass'];
                $state = '申请驳回';
            } else {
                $result[$k]['remarks'] = empty($result[$k]['bank_note']) ? '--' : $v['bank_note'];
                if ($v['play_money_state'] == 1) {
                    $time = $v['examine_pass'];
                    $state = '待打款';
                } elseif ($v['play_money_state'] == 2) {
                    $time = $v['play_time'];
                    $state = '打款中';
                } else {
                    $time = $v['complete_time'];
                    $state = '已完成';
                }
            }
            $result[$k]['bank_number'] = $v['account_type'] == 1 ? $v['bank_number'] : '微信零钱';
            $result[$k]['time'] = date('Y-m-d H:i:s', $time);
            $result[$k]['state'] = $state;
            unset($result[$k]['complete_time']);
            unset($result[$k]['play_time']);
            unset($result[$k]['examine_pass']);
            unset($result[$k]['add_time']);
            unset($result[$k]['cash_order_state']);
            unset($result[$k]['play_money_state']);
        }
        $count = $this->count($where, $join);
        $allPage = ceil($count / $pageSize);
        return returnResult(ReturnCodeService::SUCCESS, '获取成功', $result, $count, $allPage, $page, $pageSize);
    }



}