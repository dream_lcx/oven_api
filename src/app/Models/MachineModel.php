<?php

namespace app\Models;

use Server\CoreBase\Model;

/**
 * 机型模型
 */
class MachineModel extends Model
{
    protected $table = 'machine';

    /**    lcx
     *     分页查询
     * @param array $where
     * @param int $page
     * @param int $pageSize
     * @param string $field
     * @param $join
     * @param array $order
     * @return mixed
     */

    public function getAll(array $where, int $page = 1, int $pageSize = 10, string $field = '*', array $join = [], array $order = ['rq_machine.create_time' => 'DESC'])
    {
        if ($pageSize < 0) {
            $result = $this->db->select($field)
                ->from($this->table)
                ->TPWhere($where)
                ->TPJoin($join)
                ->order($order)
                ->query()
                ->result_array();
        } else {
            $result = $this->db->select($field)
                ->from($this->table)
                ->TPWhere($where)
                ->TPJoin($join)
                ->page($pageSize, $page)
                ->order($order)
                ->query()
                ->result_array();
        }

        return $result;
    }

    /**
     * @desc  查询一条数据
     * @param  无
     * @date   2020-2-25
     * @author tx
     * @param  array      $where [description]
     * @param  string     $field [description]
     * @return [type]            [description]
     */
    public function getOne(array $where, string $field="*",array $join=array()){
        $result = $this->db
            ->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->TPJoin($join)
            ->query()
            ->row();
        return $result;
    }


    //获取机型名称
    public function getMachineName($company_id = 1, $machine_id = -1)
    {

        //启用备用redis连接池
        //$this->redis = $this->loader->redis("redisPoolSpare", $this);

        //先读取redis，如果又数据返回redis里面的数据
        //$result = json_decode($this->redis->get('machine_name'),true);

        /*if (!empty($result)) {
            if(empty($machine_id)){
                return $result;
            }else{
                return $result[$machine_id];
            }

        }*/

        //如果redis里面没有数据，再查询数据库
        $where['company_id'] = $company_id;
        $where['is_deleted'] = 2;//

        $machineList = $this->getAll($where, 1,-1,'machine_id,name',[],['sort' => 'ASC']);
        if ($machine_id<0) {//查询所有
/*            $machineList = $this->getAll($where, 1,-1,'*',[],['sort' => 'ASC']);
            if(!empty($machineList)){
                foreach ($machineList as $k=>$v){
                    $machineList[$k]['machine_id'] = $v['machine_id'];
                }
            }*/
            return $machineList;
        }else{
           /* $machine_id = $machine_id;
            $machine = $this->getOne(['machine_id'=>$machine_id],'name');
            if(empty($machine)){
                return "未知";
            }
            return $machine['name'];*/

            $data = [];
            foreach ($machineList as $k => $v) {

                if($machine_id==0){
                    return '其他';
                }
                $data[$v['machine_id']] = $v['name'];

            }
            return $data[$machine_id]??'其他';

        }

        //将最新查询结果存储在缓存中
        //$this->redis->set('machine_name', $data);

    }






}