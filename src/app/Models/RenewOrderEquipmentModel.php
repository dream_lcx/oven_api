<?php
namespace app\Models;
use Server\CoreBase\Model;
/**
 * 续费主板记录
 *
 * @author lcx
 */
class RenewOrderEquipmentModel extends Model{
    protected $table = 'renew_order_equipment';
    /**
     * 获取所有数据
     * @param array $where
     * @param string $field
     * @return type
     */
    public function getAll(array $where, string $field='*',array $join=array())
    {
        $result = $this->db->select($field)
                    ->from($this->table)
                    ->TPWhere($where)
                    ->TPJoin($join)
                    ->query()
                    ->result_array();
        return $result;
    }
    /**
     * @desc  查询一条数据
     * @param  无
     * @date   2018-08-24
     * @author lcx
     * @param  array      $where [description]
     * @param  string     $field [description]
     * @return [type]            [description]
     */
    public function getOne(array $where, string $field="*"){
        $result = $this->db
            ->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->query()
            ->row();
        return $result;       
    }
    /**
     * @desc   添加信息
     * @param  无
     * @date    2018-08-24
     * @author lcx
     * @param  array      $data [description]
     */
    public function add(array $data){             
        $id = $this->db->insert($this->table)
            ->set($data)
            ->query()
            ->insert_id();   
        return $id;
    }
    /**
     * @desc  更新信息
     * @param  无
     * @date    2018-08-24
     * @author lcx
     * @param  array      $where [description]
     * @param  array      $data  [description]
     * @return [type]            [description]
     */
    public function save(array $where,array $data){             
        $result = $this->db->update($this->table)
            ->set($data)
            ->TPwhere($where)
            ->query()
            ->affected_rows();               
        return $result;
    }
 
}
