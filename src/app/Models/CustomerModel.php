<?php

namespace app\Models;

use app\Services\Common\CommonService;
use app\Services\Common\ConfigService;
use app\Services\Common\HttpService;
use Firebase\JWT\JWT;
use Server\CoreBase\Model;
use Server\CoreBase\SwooleException;


/**
 * 用户模型
 */
class CustomerModel extends Model
{
    protected $table = 'customer';


    protected $OaWorkersModel;
    protected $invitationModel;
    protected $CustomerCodeModel;
    protected $customerBindEquipmentModel;

    public function initialization(&$context)
    {
        parent::initialization($context);
        $this->OaWorkersModel = $this->loader->model('OaWorkersModel', $this);
        // 邀请好友模型
        $this->invitationModel = $this->loader->model('InvitationModel', $this);
        //用户绑定设备记录表
        $this->customerBindEquipmentModel = $this->loader->model('CustomerBindEquipmentModel',$this);
        //户号模型
        $this->CustomerCodeModel = $this->loader->model('CustomerCodeModel',$this);
    }
    /**
     * @desc  查询一条用户数据
     * @param 无
     * @date   2018-07-18
     * @param array $where [description]
     * @param string $field [description]
     * @return [type]            [description]
     * @author lcx
     */
    public function getOne(array $where, string $field = "*", array $join = [])
    {
        $result = $this->db
            ->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->TPJoin($join)
            ->query()
            ->row();
        return $result;
    }

    /**
     * @desc   添加用户信息
     * @param 无
     * @date   2018-07-18
     * @param array $data [description]
     * @author lcx
     */
    public function add(array $data)
    {
        $id = $this->db->insert($this->table)
            ->set($data)
            ->query()
            ->insert_id();
        return $id;
    }

    /**
     * @desc  更新用户信息
     * @param 无
     * @date   2018-07-18
     * @param array $where [description]
     * @param array $data [description]
     * @return [type]            [description]
     * @author lcx
     */
    public function save(array $where, array $data)
    {
        $result = $this->db->update($this->table)
            ->set($data)
            ->TPwhere($where)
            ->query()
            ->affected_rows();
        return $result;
    }

    /**    lcx
     *     分页查询
     * @param array $where
     * @param int $page
     * @param int $pageSize
     * @param string $field
     * @param $join
     * @param array $order
     * @return mixed
     */
    public function getAll(array $where, string $field = '*', array $order = ['user_id' => 'ASC'], int $page = 1, int $pageSize = -1)
    {
        if ($pageSize < 0) {
            $result = $this->db->select($field)
                ->from($this->table)
                ->TPWhere($where)
                ->order($order)
                ->query()
                ->result_array();
        } else {
            $result = $this->db->select($field)
                ->from($this->table)
                ->TPWhere($where)
                ->order($order)
                ->page($pageSize, $page)
                ->query()
                ->result_array();
        }

        return $result;
    }

    /**
     *    数量查询
     * @param array $where
     * @param string $field
     * @return int
     * @throws \Server\CoreBase\SwooleException
     * @throws \Throwable
     */
    public function getCount(array $where, string $field = '*')
    {
        $result = $this->db->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->query()
            ->num_rows();
        return $result;
    }


    /**
     * 用户账户变动
     * @param $user_id 用户id
     * @param $money 变动金额,单位元
     * @param $type 变动类型 1:增加 2:减少
     * @param $account_type 变动账户类型 1:可兑换账户 2:冻结账户
     * @param $source 变动类型 1:渠道业绩结算 2:渠道推广员业绩结算 3:分成给渠道推广员 4兑换
     * @param $order_id 关联订单ID，若source=1,2,3时传工单ID
     *
     */
    public function changeAccount($user_id, $money, $source = 1, $order_id = '', $remarks = '', $type = 1, $account_type = 1, $add_time = '')
    {
        if (empty($user_id)) {
            return false;
        }
        if (empty($add_time)) {
            $add_time = time();
        }
        $customerAccountModel = get_instance()->loader->model('CustomerAccountModel', get_instance());
        $customerAccountBillModel = get_instance()->loader->model('CustomerAccountBillModel', get_instance());
        $money = formatMoney($money, 1);//将元转为分
        //判断是否创建了账户
        $account_info = $customerAccountModel->getOne(['user_id' => $user_id], 'id,total_money,available_money,frozen_money');
        if (!empty($account_info)) {
            $id = $account_info['id'];
            $total_money = $account_info['total_money'];
            $available_money = $account_info['available_money'];
            $frozen_money = $account_info['frozen_money'];
        } else {
            //创建账户
            $account_data['user_id'] = $user_id;
            $account_data['total_money'] = 0;
            $account_data['available_money'] = 0;
            $account_data['frozen_money'] = 0;
            $account_data['create_time'] = $add_time;
            $id = $customerAccountModel->add($account_data);

            $total_money = 0;
            $available_money = 0;
            $frozen_money = 0;
        }

        //更新账户余额
        if ($account_type == 1) {
            //可兑换账户
            switch ($type) {
                case 1:
                    $edit_account_data['total_money'] = $total_money + $money;
                    $edit_account_data['available_money'] = $available_money + $money;
                    break;

                case 2:
                    $edit_account_data['available_money'] = $available_money - $money;
                    break;
            }
        } else if ($account_type == 2) {
            //冻结账户
            switch ($type) {
                case 1:
                    $edit_account_data['frozen_money'] = $frozen_money + $money;
                    break;

                case 2:
                    $edit_account_data['frozen_money'] = $frozen_money - $money;
                    break;
            }
        }
        $edit_account_result = $customerAccountModel->save(['id' => $id], $edit_account_data);
        //写入账户记录
        $bill_data['user_id'] = $user_id;
        $bill_data['money'] = $money;
        $bill_data['change_type'] = $type;
        $bill_data['account_type'] = $account_type;
        $bill_data['source'] = $source;
        //$bill_data['work_order_id'] = $order_id;
        $bill_data['remarks'] = $remarks;
        $bill_data['create_time'] = $add_time;
        $add_bill_result = $customerAccountBillModel->add($bill_data);
        return $edit_account_result && $add_bill_result;
    }


    /**
     * 获取用户所属区域
     * @param $user_id
     * @param $pid
     * @param $provice_code
     * @param $city_code
     * @param $area_code
     * @param int $company_id
     * @return bool|null
     * @throws \Server\CoreBase\SwooleException
     * @throws \Throwable
     */
    public function getOwnArea($user_id, $pid = '', $provice_code = '', $city_code = '', $area_code = '', $company_id = 1)
    {
        //默认:根据地址划分
        $db = get_instance()->loader->mysql("mysqlPool", get_instance());
        $mysql = get_instance()->config['mysql'];
        $this->prefix = $mysql[$mysql['active']]['prefix'] ?? '';
        $map['province_id'] = $provice_code;
        $map['city_id'] = $city_code;
        $map['area_id'] = $area_code;
        $sql = "select * from " . $this->prefix . "administrative_info where locate('," . $area_code . ",',area_id) AND locate('," . $city_code . ",',city_id) AND locate('," . $provice_code . ",',province_id) AND company_id=" . $company_id;
        $info = $db->query($sql)->row();
        if (empty($info) || empty($info['a_id'])) {
            $sql_b = "select o_id as a_id from " . config('database.prefix') . "operation_info where locate('," . $provice_code . ",',province_id) AND company_id=" . $company_id;
            $info_b = $db->query($sql_b)->row();
            if (empty($info_b)) {
                $info = [];
            } else {
                $info['a_id'] = 0;
                $info['operation'] = $info_b[0]['a_id'];
            }
        }
        return $info;
    }


    /*
    * 获取用户的推荐人身份信息
    * @param 必选 $user_id int 用户ID
    * @param 必选 company_id int 公司ID
    * @return_param role int 角色3合伙人4市场推广
    * @return_param uid int 角色对应id
    */
    public function getUserBelongTo($user_id, $company_id = 1)
    {
        $customerModel = get_instance()->loader->model('CustomerModel', get_instance());
        $oaWorkersModel = get_instance()->loader->model('OaWorkersModel', get_instance());
        //查询用户推荐人信息
        $user_info = $customerModel->getOne(['user_id' => $user_id], 'source_is_valid,source');

        $data = [];
        if (!empty($user_info)) {//有推荐人用户推荐人的身份是否是市场推广

            $workers_info = $oaWorkersModel->getOne(['user_id' => $user_info['source'], 'is_delete' => 0], 'workers_id');
            if (!empty($workers_info)) {
                $data['role'] = 4;//市场推广
                $data['uid'] = $workers_info['workers_id'];
                return $data;
            } else {
                //如果推荐人不是市场推广，则查看推荐人属于哪个合伙人下
                $info = $this->getOwnArea($user_info['source'], '', '', '', '', $company_id);
                if (!empty($info)) {
                    $data['role'] = 3;//合伙人
                    $data['uid'] = $info['a_id'];
                    return $data;
                }

            }

        }

        return $data;

    }

    /**
     *   用户绑定手机号（只注册不登录）
     * @param array $where
     * @param telphone int 用户电话
     * @param login_type int 登录方式1微信,2百度
     * @param bind_type int 绑定类型 1验证码绑定,2一键授权绑定
     * @param code string 用户code(当bind_type=1必须)
     * @return int
     * @throws \Server\CoreBase\SwooleException
     * @throws \Throwable
     */
    public function bind($telphone, $user_data, $company_id = 1, $source_id = 0)
    {
        if (empty($telphone)) {
            return false;
        }
        $customerModel = get_instance()->loader->model('CustomerModel', get_instance());
        // 判断用户是否已经绑定过
        $user_info = $customerModel->getOne(array('telphone' => $telphone, 'user_belong_to_company' => $company_id), 'username,source,user_id,telphone,user_id,source_is_valid,openid');
        //默认推荐人
        $default_inviter = ConfigService::getBalanceConfig('', 'default_inviter', $company_id);
        // 已经绑定过直接更新token
        if (!empty($user_info)) {
            /**
             * 不存在推荐人，为默认推荐人
             */
            if (empty($user_info['source'])) {
                if ($source_id != 0) {
                    $user['source'] = $source_id;
                } else {
                    $user['source'] = $default_inviter['user_id'];
                    $user['source_is_default'] = 2;
                }
            }

        } else {
            if ($source_id != 0) {
                $user['source'] = $source_id;
                $user['source_is_valid'] = 2;
                $user['source_is_default'] = 1;

            } else {
                $user['source'] = $default_inviter['user_id'];
                $user['source_is_default'] = 2;
            }

        }
        $add_data = $user;
        //在boss系统用户中心注册
//        $bossResult = HttpService::requestBossApi(['company_id' => $company_id, 'telphone' => $telphone, 'app_sn' => $this->config->get('app_sn'), 'client' => 1], '/api/User/register');
//        if ($bossResult['code'] != 0) {//注册失败,直接抛出异常
//            throw new SwooleException("用户中心注册失败" . $bossResult['msg']);
//        }
//        $bossTokenData = JWT::decode($bossResult['data']['token'], $this->config->get('token_key'), array('HS256'));

//        $add_data['account'] = !empty($bossTokenData->account) ? $bossTokenData->account : CommonService::createSn('cloud_user_no', '', 1);
//        $add_data['uuid'] = $bossTokenData->uuid;
        $add_data['account'] = CommonService::createSn('oven_user_no', '', 1);
        $add_data['uuid'] = create_uuid();
        $add_data['user_class'] = 1;
        $add_data['telphone'] = $telphone;
        $add_data['username'] = $user_data['username'] ?? '';
        $add_data['nickname'] = urlencode($user_data['username']);
        $add_data['avatar_img'] = $user_data['avatar_img'] ?? '';
        $add_data['gender'] = $user_data['gender'] ?? 0;
        $add_data['country'] = $user_data['country'] ?? '';
        $add_data['province'] = $user_data['province'] ?? '';
        $add_data['city'] = $user_data['city'] ?? '';
        $add_data['area'] = $user_data['area'] ?? '';
        $add_data['user_address'] = $user_data['user_address'] ?? '';
        $add_data['province_code'] = $user_data['province_code'] ?? '';
        $add_data['city_code'] = $user_data['city_code'] ?? '';
        $add_data['area_code'] = $user_data['area_code'] ?? '';
        $add_data['bind_time'] = time();
        $add_data['bind_type'] = 2;
        $add_data['user_belong_to_company'] = $company_id;

        if (empty($user_info)) {
            //如果用户未绑定直接注册新用户
            $add_data['openid'] = '';
            $add_data['add_time'] = time();
            $user_id = $customerModel->add($add_data);
            //新用户注册送券
            $this->sendCoupon($user_info['user_id'], $add_data['source'], $company_id);
            if (!empty($source_id)) {
                $this->addInviteRecord($user_info['user_id'], $source_id, '', 2, 0);
            }

        } else {
            $customerModel->save(array('user_id' => $user_info['user_id']), $user);
            $user_id = $user_info['user_id'];
        }

        return $user_id;

    }


    /**
     * @desc   添加邀请好友记录或者修改状态
     * @param 无
     * @date   2018-07-18
     * @author lcx
     */
    public function addInviteRecord($user_id, $pid, $openid = '', $status = 2, $way = 0)
    {
        $customerModel = get_instance()->loader->model('CustomerModel', get_instance());
        $inviteModel = get_instance()->loader->model('InvitationModel', get_instance());

        $data['user_id'] = $user_id;
        $data['pid'] = $pid;
        $data['create_time'] = time();
        $data['user_openid'] = $openid;
        $data['status'] = $status;
        $data['way'] = $way;
        //判断邀请人是否存在
        $invite_info = $customerModel->getOne(array('user_id' => $pid), 'user_id');
        if (empty($invite_info)) {
            return false;
        }
        //获取邀请人的身份
        $identity = $this->getUserIdentity($invite_info['user_id']);
        $data['identity'] = $identity['identity'];

        //判断是否已经存在该条记录
        //$map['user_openid'] = $openid;
        $map['user_id'] = $user_id;
        $map['pid'] = $pid;
        $info = $inviteModel->getOne($map, 'status,invite_id');
        if (empty($info)) {
            $res = $inviteModel->add($data);
        } else {
            $res = true;
        }
        return $res;
    }


    /**
     * 获取用户身份信息(条件：一个用户只有一种身份的情况下)
     * @param 必选 $user_id int 用户ID
     * @return $identity int （0普通用户1市场推广2渠道3渠道推广员）
     */
    public function getUserIdentity($user_id)
    {
        $customerModel = get_instance()->loader->model('CustomerModel', get_instance());
        $channelModel = get_instance()->loader->model('ChannelModel', get_instance());
        $channelPromotersModel = get_instance()->loader->model('ChannelPromotersModel', get_instance());
        $oaWorkersModel = get_instance()->loader->model('OaWorkersModel', get_instance());


        if (empty($user_id)) {
            return false;
        }

        $where['user_id'] = $user_id;
        $where['is_deleted'] = 2;

        $res = [];
        //查询是否是渠道商
        $channel_info = $channelModel->getOne($where, 'id,status');
        if (!empty($channel_info)) {

            $res = [
                'identity' => 2,
                'identity_name' => '渠道商',
                'status' => $channel_info['status']
            ];

            return $res;
        }

        //查询是否是推广
        $promote_info = $channelPromotersModel->getOne($where, 'channel_promoter_id,level,status');
        if (!empty($promote_info)) {
            $res = [
                'identity' => 3,//渠道推广员
                'level' => $promote_info['level'],//渠道推广员等级
                'identity_name' => $promote_info['level'] == 1 ? '团长' : '团员',
                'status' => $promote_info['status']
            ];

            return $res;
        }

        unset($where);
        $where['user_id'] = $user_id;
        $where['is_delete'] = 0;
        $where['position'] = ['NOT IN', [1, 2]];//业务和直推人

        //查询是否是市场推广
        $oa_workers_info = $oaWorkersModel->getOne($where, 'workers_id,workers_status,position');
        if (!empty($oa_workers_info)) {

            $res = [
                'identity' => 1,
                'identity_name' => '市场推广',
                'status' => $oa_workers_info['workers_status']
            ];

            return $res;
        }

        unset($where);
        $where['user_id'] = $user_id;
        $where['is_delete'] = 0;

        //查询是否是普通用户
        $user_info = $customerModel->getOne($where, 'user_id,account_status');
        if (empty($user_info)) {
            $res = [
                'identity' => -1,
                'identity_name' => '非公司用户',
            ];

            return $res;

        }

        $res = [
            'identity' => 0,
            'identity_name' => '普通用户',
            'status' => $user_info['account_status']
        ];

        return $res;

    }


    //新用户注册送券
    public function sendCoupon($user_id, $source, $company_id)
    {
        $couponCateModel = get_instance()->loader->model('CouponCateModel', get_instance());
        $couponModel = get_instance()->loader->model('CouponModel', get_instance());


        $key = ConfigService::getConfig('register_coupon', true, $company_id);//获取配置
        if ($key == 1) {
            //查询该公司是否有新用户安装券
            $coupon_cate = $couponCateModel->getOne(['put_type' => 1, 'company_id' => $company_id]);
            if (!empty($coupon_cate) && $coupon_cate['receive_num'] < $coupon_cate['total_num'] && $coupon_cate['send_start_time'] < time() && $coupon_cate['send_end_time'] > time()) {
                //条件：有余券而且发放时间没有结束才可以赠送
                //计算优惠券有效期
                if ($coupon_cate['time_type'] == 1) {
                    //1领取后X天内有效
                    $valid_time_start = time();
                    $valid_time_end = time() + $coupon_cate['use_days'] * 86400;
                } else {
                    //2固定时间段有效
                    $valid_time_start = $coupon_cate['use_start_time'];
                    $valid_time_end = $coupon_cate['use_end_time'];
                }
                //开启送券
                $add_data = [
                    'uid' => $user_id,//用户id
                    'cid' => $coupon_cate['id'],//优惠券对应分类id',
                    'code' => generate_promotion_code(),
                    'type' => 1,//1新用户注册
                    'status' => 1,//待使用
                    'send_time' => time(),//领取时间
                    'valid_time_start' => $valid_time_start,
                    'valid_time_end' => $valid_time_end,
                    'pid' => $source ?? '',
                    'note' => '新用户注册送券'

                ];

                //查询用户领取优惠券信息
                $coupon_info = $couponModel->getOne(['cid' => $coupon_cate['id'], 'uid' => $user_id], 'coupon_id');
                if (empty($coupon_info)) {
                    $couponModel->add($add_data);
                }
            }
        }
    }


    /**
     * 查询团队人数
     * @param $user_id 用户ID
     */
    public function getTeam($user_id){
        $customer_data = $this->getAll(['source'=>$user_id],'user_id,realname,username,telphone,is_dealer');
        if (empty($customer_data)) return [];
        $workers_position = $this->config->get('workers_position');
        foreach ($customer_data as $key=>$value){
            $customer_data[$key]['username'] = empty($value['realname']) ? $value['username'] : $value['realname'];
            $customer_data[$key]['position_name'] = '用户';
            if ($value['is_dealer'] == 1){
                $oa_workers_data = $this->OaWorkersModel->getOne(['user_id'=>$value['user_id']]);
                if (!empty($oa_workers_data)){
                    $customer_data[$key]['position_name'] = ($workers_position[$oa_workers_data['position']] ?? '') . '市场推广';
                }
            }
            //查询团队人数
            $customer_data[$key]['team_number'] = $this->invitationModel->count(['pid'=>$value['user_id']]);
            unset($customer_data[$key]['realname']);
            unset($customer_data[$key]['is_dealer']);
        }
        return $customer_data;
    }



    //添加用户
    public function addUser($param)
    {
        $workers_id = $param['workers_id'];
        $workers_info = $this->OaWorkersModel->getOne(['workers_id' => $workers_id], 'user_id');//市场推广
        $source = $workers_info['user_id'] ?? 0;
        //添加甲方公司账号
        $userinfo = $this->getOne(['telphone' => $param['login_phone']], 'user_id,account,telphone');
        if (empty($userinfo)) {
            $item['realname'] = $param['shop_name'];
            $item['password'] = md5(substr($param['login_phone'],-6));
            $item['source'] = $source;
            $item['source_is_valid'] = 2;
            $item['source_is_default'] = 1;
            $item['account'] = CommonService::createSn('oven_user_no', '', 1);
            $item['add_time'] = time();
            $item['user_belong_to_company'] = $param['company_id'];
            $item['telphone'] = $param['login_phone']; //临时值
            $item['user_role'] = 1;
            $related_id = $this->add($item);
            $signing_user_account = $item['account']; //甲方公司账号
            $signing_user_pwd = substr($param['login_phone'],-6); //甲方公司密码
            $signing_user_tel = $param['login_phone'];
            unset($item);
        } else {
            $signing_user_account = $userinfo['account']; //甲方公司账号
            $related_id = $userinfo['user_id'];
            $signing_user_pwd = '密码未更改,请使用原有密码'; //甲方公司密码
            $signing_user_tel = $userinfo['telphone'];
        }

        //邀请记录
        $item['user_id'] = $related_id;
        $item['type'] = 2;
        $item['pid'] = $source;
        $item['status'] = 1;
        $item['create_time'] = time();
        $item['way'] = 5;
        $this->invitationModel->add($item);
        $contract_info = $param;
        $contract_info['user_id'] = $related_id;
        $code = $this->addCustomerCode($contract_info);
        //添加用户和户号关系表
        if ($code) {
            unset($item);
            $item['customer_code'] = $code;
            $item['user_id'] = $related_id;
            $item['create_time'] = time();
            $item['is_owner'] = 2;
            $item['status'] = 1;
            $this->customerBindEquipmentModel->addCustomerBindEquipment($item);
        }
        return ['related_id' => $related_id, 'code'=>$code, 'signing_user_account' => $signing_user_account, 'signing_user_pwd' => $signing_user_pwd, 'signing_user_tel' => $signing_user_tel];
    }

    //添加户号
    public function addCustomerCode($contract_info)
    {
        $add_data['code'] = CommonService::createSn('oven_customer_code', '', 4);
        //        $add_data['code'] = rand(10000000, 99999999);
        $add_data['code_name'] = $contract_info['shop_name'];
        $add_data['company_id'] = $contract_info['company_id'];
        $add_data['contract_id'] = $contract_info['contract_id'] ?? 0;
        $add_data['identification_id'] = $contract_info['identification_id'] ?? 0;
        $add_data['user_id'] = $contract_info['user_id'];
        $add_data['actual_transformation_num'] = 0;
        $add_data['create_time'] = time();
        $add_data['state'] = 1;
        $result = $this->CustomerCodeModel->add($add_data);
        if ($result) {
            return $add_data['code'];
        }
        return false;
    }



}