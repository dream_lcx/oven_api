<?php
namespace app\Models;

use Server\CoreBase\Model;

/**  YSF
 *   优惠券 模型
 *   Date: 2018/12/4
 * Class CouponModel
 * @package app\Models
 */
class CouponModel extends Model
{
    // 表名
    protected $table = 'coupon';
    protected $alias = 'coupon c';

    // 连表列表查询
    public function getJoinAll(array $where, array $join, int $page, int $pageSize, string $field, array $order)
    {
        $result = $this->db->select($field)
                ->from($this->alias)
                ->TPWhere($where)
                ->TPJoin($join)
                ->page($pageSize, $page)
                ->order($order)
                ->query()
                ->result_array();

        return $result;
    }

    // 连表单条查询
    public function getJoinOne(array $where, array $join, string $field)
    {
        $result = $this->db->select($field)
                ->from($this->alias)
                ->TPWhere($where)
                ->TPJoin($join)
                ->query()
                ->row();
        return $result;
    }

    // 单表单条查询
    public function getOne(array $where, string $field='*')
    {
        $result = $this->db->select($field)
                ->from($this->table)
                ->TPWhere($where)
                ->query()
                ->row();
        return $result;
    }

    //添加优惠券
    public function add(array $data)
    {
        $result = $this->db->insert($this->table)
            ->set($data)
            ->query()
            ->insert_id();
        return $result;
    }

    // 优惠券修改
    public function edit(array $where, array $data)
    {
        $result = $this->db->update($this->table)
                ->set($data)
                ->TPWhere($where)
                ->query()
                ->affected_rows();
        return $result;
    }

    // 单表列表查询
    public function getAll(array $where, string $field='*')
    {
        $result = $this->db->select($field)
                    ->from($this->table)
                    ->TPWhere($where)
                    ->query()
                    ->result_array();
        return $result;
    }

    /**
     * @desc   统计数量
     * @param  无
     * @date   2018-07-18
     * @author lcx
     * @param  array      $data [description]
     */
    public function count($map,$join=[]){
        $result = $this->db->select('*')
            ->from($this->alias)
            ->TPWhere($map)
            ->TPJoin($join)
            ->query()
            ->num_rows();
        return $result;
    }

    /**
     * @desc  更新信息
     * @param 无
     * @date   2018-07-24
     * @param array $where [description]
     * @param array $data [description]
     * @return [type]            [description]
     * @author lcx
     */
    public function save(array $where, array $data)
    {
        $result = $this->db->update($this->table)
            ->set($data)
            ->TPwhere($where)
            ->query()
            ->affected_rows();
        return $result;
    }


    /**
     * @desc   获取优惠券信息
     * @param  无
     * @date   2020-11-18
     * @author tx
     * @param  coupon_id array 优惠券id
     * @param  type int 1转化2不转化
     */
    public function getCouponList($coupon_id,$type=1){
        //查询优惠券信息
        if(!empty($coupon_id)){
            $join=[
                ['coupon_cate e','e.id=c.cid','left']
            ];
            $field='c.coupon_id,
                    c.type,
                    c.code,
                    c.status,
                    c.use_time,
                    c.valid_time_start,
                    c.valid_time_end,
                    e.money';

            $coupon_data=$this->getJoinAll(['c.coupon_id'=>['IN',$coupon_id]],$join,1,100,$field,['c.send_time'=>'DESC']);
            if(!empty($coupon_data)){
                if($type == 1){
                    foreach ($coupon_data as $k1=>$v1){
                        $coupon_data[$k1]['use_time'] = !empty($v1['use_time']) ? date('Y-m-d', $v1['use_time']) : '--';
                        $coupon_data[$k1]['valid_time_start'] = !empty($v1['valid_time_start']) ? date('Y-m-d', $v1['valid_time_start']) : '--';
                        $coupon_data[$k1]['valid_time_end'] = !empty($v1['valid_time_end']) ? date('Y-m-d', $v1['valid_time_end']) : '--';
                        $coupon_data[$k1]['money'] =formatMoney($v1['money'],2);
                    }
                }
                return $coupon_data;
            }
        }

        return [];
    }



}