<?php
/**
 * Created by PhpStorm.
 * User: LG
 * Date: 2018/7/31
 * Time: 15:59
 */

namespace app\Models;


use Server\CoreBase\Model;

class RemoteFileModel extends Model
{
    protected $remote_file = 'remote_file';

    /**
     * 查询单条远程文件信息
     * @author ligang
     * @param array $where      查询条件
     * @param string $field     查询字段
     * @param string $co        查询类型
     * @return null
     * @throws \Throwable
     * @date 2018/7/31 16:00
     */
    public function findRemoteFile(array $where,string $field='*',string $co = 'AND')
    {
        $result = $this->db
            ->select($field)
            ->from($this->remote_file)
            ->TPWhere($where,$co)
            ->query()
            ->row();
        return $result;
    }
}