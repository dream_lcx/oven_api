<?php
namespace app\Models;

use Server\CoreBase\Model;

/**   YSF
 *    主板租赁套餐 Model
 *    Date: 2018/7/18
 * Class RentalPackageModel
 * @package app\Models
 */
class RentalPackageModel extends Model
{
    // 表名
    protected $dbName = 'equipment_rental_package';

    /**   YSF
     *    根据主板ID 查询套餐信息
     * @param array $where   查询条件
     * @param string $field  查询字段
     * @return mixed
     */
    public function getAll(array $where, string $field)
    {
        $result = $this->db->select($field)
                    ->from($this->dbName)
                    ->TPWhere($where)
                    ->query()
                    ->result_array();
        return $result;
    }
      /**
     * @desc  查询一条数据
     * @param  无
     * @date   2018-07-18
     * @author lcx
     * @param  array      $where [description]
     * @param  string     $field [description]
     * @return [type]            [description]
     */
    public function getOne(array $where, string $field="*"){
        $result = $this->db
            ->select($field)
            ->from($this->dbName)
            ->TPWhere($where)
            ->query()
            ->row();
        return $result;       
    }

}