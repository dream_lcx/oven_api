<?php
namespace app\Models;
use Noodlehaus\Exception;
use Server\CoreBase\Model;
use Server\CoreBase\SwooleException;

class EquipmentsPartsModel extends Model
{
    protected $table = 'equipments_parts';
    /**
     * @desc  查询一条数据
     * @param  无
     * @date   2018-07-18
     * @author lcx
     * @param  array      $where [description]
     * @param  string     $field [description]
     * @return [type]            [description]
     */
    public function getOne(array $where, string $field="*"){
        $result = $this->db
            ->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->query()
            ->row();
        return $result;       
    }
     /**    lcx
     *     分页查询
     * @param array $where
     * @param int $page
     * @param int $pageSize
     * @param string $field
     * @param $join
     * @param array $order
     * @return mixed
     */
    public function getAll(array $where, string $field = '*', array $join=array(),array $order = ['rq_equipments_parts.create_time' => 'DESC'],int $page=1,int $pagesize=-1)
    {
        if($pagesize<=0){
             $result = $this->db->select($field)
                ->from($this->table)
                ->TPWhere($where)
                ->TPJoin($join)
                ->query()
                ->result_array();
        }else{
            $result = $this->db->select($field)
               ->from($this->table)
               ->TPWhere($where)
               ->TPJoin($join)
               ->page($pagesize,$page)
               ->query()
               ->result_array();
        }
       

        return $result;
    }
    /**
     * @desc   添加信息
     * @param  无
     * @date   2018-07-18
     * @author lcx
     * @param  array      $data [description]
     */
    public function add(array $data){             
        $id = $this->db->insert($this->table)
            ->set($data)
            ->query()
            ->insert_id();       
        return $id;
    }
    /**
     * @desc  更新信息
     * @param  无
     * @date   2018-07-18
     * @author lcx
     * @param  array      $where [description]
     * @param  array      $data  [description]
     * @return [type]            [description]
     */
    public function save(array $where,array $data){             
        $result = $this->db->update($this->table)
            ->set($data)
            ->TPwhere($where)
            ->query()
            ->affected_rows();
        return $result;
    }

    // 连表查询
    public function getJoinAll(array $where)
    {
        $join = [
            ['parts p', 'p.parts_id = a.parts_id', 'left join']
        ];
        $field = 'a.parts_id,a.parts_number,p.parts_name';
        $result = $this->db->select($field)
                    ->from('equipments_parts a')
                    ->TPWhere($where)
                    ->TPJoin($join)
                    ->query()
                    ->result_array();
        return $result;
    }
     /**
     * 删除
     * @param array $where 条件
     * @return type
     */
    public function del(array $where){
        $result = $this->db->delete()
            ->from($this->table)
            ->TPwhere($where)
            ->query()
            ->affected_rows();               
        return $result;
    }

}