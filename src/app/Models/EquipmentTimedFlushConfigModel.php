<?php

namespace app\Models;

use Server\CoreBase\Model;

/**
 * 主板定时冲洗时间配置
 */
class EquipmentTimedFlushConfigModel extends Model {

    protected $table = 'equipment_timed_flush_config';

    /**   lcx
     * 查询列表
     * @param array $where  查询条件
     * @param int $page     当前页
     * @param int $pageSize 每页条数
     * @param array $join   连表
     * @param string $field 查询字段
     * @param array $order  排序方式
     * @return mixed
     */
    public function getAll(array $where = array(), string $field = '*', array $order = ['sort' => 'ASC']) {
        $result = $this->db->select($field)
                ->from($this->table)
                ->TPWhere($where)
                ->order($order)
                ->query()
                ->result_array();
        return $result;
    }

}
