<?php
namespace app\Models;

use Server\CoreBase\Model;

/**   YSF
 *    续费订单 Model
 *    Date: 2018/8/3
 * Class RenewOrderModel
 * @package app\Models
 */
class RenewOrderModel extends Model
{
    // 表名
    protected $dbName = 'renew_order';

    /**   YSF
     *    添加一条订单
     * @param array $data 数据
     * @return mixed
     */
    public function add(array $data)
    {
        $result = $this->db->insert($this->dbName)
                    ->set($data)
                    ->query()
                    ->insert_id();
        return $result;
    }

    /**   YSF
     *    单条查询
     * @param array $where   查询条件
     * @param string $field  查询字段
     * @return null
     */
    public function getOne(array $where, string $field = '*',$join=[])
    {
        $result = $this->db->select($field)
                    ->from($this->dbName)
                    ->TPWhere($where)
                    ->TPJoin($join)
                    ->query()
                    ->row();
        return $result;
    }

    /**
     * @param array $where 修改条件
     * @param array $data  数据
     * @return mixed
     */
    public function edit(array $where, array $data)
    {

        $result = $this->db->update($this->dbName)
                    ->set($data)
                    ->TPWhere($where)
                    ->query()
                    ->affected_rows();
        return $result;
    }


    public function getAll(array $where, int $page=1, int $pageSize=10, string $field = '*', array $join=[],array $order = ['renew_time' => 'DESC'])
    {
        $result = $this->db->select($field)
            ->from($this->dbName)
            ->TPWhere($where)
            ->TPJoin($join)
            ->page($pageSize,$page)
            ->order($order)
            ->query()
            ->result_array();
        return $result;
    }


}