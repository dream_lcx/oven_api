<?php
namespace app\Models;

use Server\CoreBase\Model;

/**   YSF
 *    三级联动 地址 Model
 *    Date: 2018/7/18
 * Class AreaModel
 * @package app\Models
 */
class AreaModel extends Model
{
    // 表名
    protected $dbName = 'areas';

    /**   YSF
     *    地区查询
     * @param array $where   查询条件
     * @param string $field  查询字段
     * @return mixed
     */
    public function getAll(array $where, string $field = '*',$order=[])
    {
        $result = $this->db->select($field)
                    ->from($this->dbName)
                    ->TPWhere($where)
                    ->order($order)
                    ->query()
                    ->result_array();
        return $result;
    }
     /**
     * @desc  查询一条数据
     * @param  无
     * @date   2018-07-18
     * @author lcx
     * @param  array      $where [description]
     * @param  string     $field [description]
     * @return [type]            [description]
     */
    public function getOne(array $where, string $field="*"){
        $result = $this->db
            ->select($field)
            ->from($this->dbName)
            ->TPWhere($where)
            ->query()
            ->row();
        return $result;       
    }
	/**
     * @desc  更新信息
     * @param  无
     * @date   2018-07-18
     * @author lcx
     * @param  array      $where [description]
     * @param  array      $data  [description]
     * @return [type]            [description]
     */
    public function save(array $where,array $data){             
        $result = $this->db->update($this->dbName)
            ->set($data)
            ->TPwhere($where)
            ->query()
            ->affected_rows();               
        return $result;
    }

}