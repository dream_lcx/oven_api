<?php
namespace app\Models;
use Server\CoreBase\Model;

/**  YSF
 *   分享模板 模型
 *   Date: 2018/12/3
 * Class SharingsModel
 * @package app\Models
 */
class SharingsModel extends Model
{
    // 表名
    protected $table = 'sharing';
    //分享记录表
    protected $shareLogTable = 'share_log';

    // 列表查询
    public function getAll(array $where, int $page, int $pageSize, string $field, array $order)
    {
        $result = $this->db->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->page($pageSize, $page)
            ->order($order)
            ->query()
            ->result_array();

        return $result;
    }

    // 添加操作
    public function add(array $data)
    {
        $result = $this->db->insert($this->table)
            ->set($data)
            ->query()
            ->insert_id();

        return $result;
    }

    // 删除操作
    public function del(array $where)
    {
        $value =  $this->db->delete()
                ->from($this->table)
                ->TPWhere($where)
                ->query()
                ->affected_rows();
        return $value;
    }

    /*----------------分享记录--------------------*/

    /**
     *  插入数据
     * @author bg
     * @param array $data
     * @return mixed
     * @date 2019-04-19 上午 10:47
     */
    public function insertShareLog(array $data){
        $result = $this->db->insert($this->shareLogTable)
            ->set($data)
            ->query()
            ->insert_id();
        return $result;
    }

}