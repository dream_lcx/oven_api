<?php
/**
 * Created by PhpStorm.
 * User: LG
 * Date: 2018/12/24
 * Time: 16:22
 */

namespace app\Models;


use Server\CoreBase\Model;

/**
 * 共享模型
 * Class Shared
 * @package app\Models
 */
class SharedModel extends Model
{
    protected $sharer_list = 'sharer_list';
    protected $shared_message = 'shared_message';

    /**
     * 插入共享人
     * @author ligang
     * @param array $data
     * @return mixed
     * @throws \Server\CoreBase\SwooleException
     * @throws \Throwable
     * @date 2018/12/24 16:23
     */
    public function insertSharerList(array $data) {
        $result = $this->db
            ->insert($this->sharer_list)
            ->set($data)
            ->query()
            ->insert_id();
        return $result;
    }

    /**
     * 插入共享消息
     * @author ligang
     * @param array $data
     * @return mixed
     * @throws \Server\CoreBase\SwooleException
     * @throws \Throwable
     * @date 2018/12/24 16:24
     */
    public function insertSharedMessage(array $data) {
        $result = $this->db
            ->insert($this->shared_message)
            ->set($data)
            ->query()
            ->insert_id();
        return $result;
    }

    /**
     * 获取共享列表
     * @author ligang
     * @param array $where
     * @param array $join
     * @param int $page
     * @param int $pageSize
     * @param string $field
     * @param array $order
     * @return mixed
     * @throws \Server\CoreBase\SwooleException
     * @throws \Throwable
     * @date 2018/12/25 11:26
     */
    public function selectSharerList(array $where = [],array $join = [],int $page = 1, int $pageSize = 10, string $field = '*', array $order = []) {
        if (!empty($join)){
            $result = $this->db
                ->select($field)
                ->from($this->sharer_list)
                ->TPWhere($where)
                ->TPJoin($join)
                ->page($pageSize, $page)
                ->order($order)
                ->query()
                ->result_array();
            return $result;
        }
        $result = $this->db
            ->select($field)
            ->from($this->sharer_list)
            ->TPWhere($where)
            ->page($pageSize, $page)
            ->order($order)
            ->query()
            ->result_array();
        return $result;
    }

    /**
     * 获取共享列表
     * @author ligang
     * @param array $where
     * @param array $join
     * @param int $page
     * @param int $pageSize
     * @param string $field
     * @param array $order
     * @return mixed
     * @throws \Server\CoreBase\SwooleException
     * @throws \Throwable
     * @date 2018/12/25 11:26
     */
    public function selectSharerMessage(array $where = [],array $join = [],int $page = 1, int $pageSize = 10, string $field = '*', array $order = []) {
        if (!empty($join)){
            $result = $this->db
                ->select($field)
                ->from($this->shared_message)
                ->TPWhere($where)
                ->TPJoin($join)
                ->page($pageSize, $page)
                ->order($order)
                ->query()
                ->result_array();
            return $result;
        }
        $result = $this->db
            ->select($field)
            ->from($this->shared_message)
            ->TPWhere($where)
            ->page($pageSize, $page)
            ->order($order)
            ->query()
            ->result_array();
        return $result;
    }

    /**
     * 查找单条共享消息
     * @author ligang
     * @param array $where
     * @param array $join
     * @param string $field
     * @param string $connector
     * @return null
     * @throws \Server\CoreBase\SwooleException
     * @throws \Throwable
     * @date 2018/12/25 17:22
     */
    public function findSharedMessage(array $where,string $field = '*',string $connector = 'AND',array $join = [])
    {
        if (!empty($join)){
            $result = $this->db
                ->select($field)
                ->from($this->shared_message)
                ->TPWhere($where, $connector)
                ->TPJoin($join)
                ->query()
                ->row();
            return $result;
        }
        $result = $this->db
            ->select($field)
            ->from($this->shared_message)
            ->TPWhere($where, $connector)
            ->query()
            ->row();
        return $result;
    }

    /**
     * 查找单条共享列表
     * @author ligang
     * @param array $where
     * @param array $join
     * @param string $field
     * @param string $connector
     * @return null
     * @throws \Server\CoreBase\SwooleException
     * @throws \Throwable
     * @date 2018/12/25 11:30
     */
    public function findSharerList(array $where,array $join = [],string $field = '*',string $connector = 'AND')
    {
        if (!empty($join)){
            $result = $this->db
                ->select($field)
                ->from($this->sharer_list)
                ->TPWhere($where, $connector)
                ->TPJoin($join)
                ->query()
                ->row();
            return $result;
        }
        $result = $this->db
            ->select($field)
            ->from($this->sharer_list)
            ->TPWhere($where, $connector)
            ->query()
            ->row();
        return $result;
    }

    /**
     * 更新
     * @author ligang
     * @param array $data
     * @param array $where
     * @return mixed
     * @throws \Server\CoreBase\SwooleException
     * @throws \Throwable
     * @date 2018/12/25 9:52
     */
    public function updateSharedMessage(array $data, array $where) {
        $result = $this->db
            ->update($this->shared_message)
            ->set($data)
            ->TPWhere($where)
            ->query()
            ->affected_rows();
        return $result;
    }

    /**
     *
     * @author ligang
     * @param array $where
     * @param array $join
     * @param int $page
     * @param int $pageSize
     * @param string $field
     * @param array $order
     * @param string $connector
     * @return mixed
     * @throws \Server\CoreBase\SwooleException
     * @throws \Throwable
     * @date 2018/12/25 16:25
     */
    public function selectJoinCoSharerMessage(array $where = [],array $join = [],int $page = 1, int $pageSize = 10, string $field = '*', array $order = [],string $connector = 'AND') {
        $result = $this->db
            ->select($field)
            ->from($this->shared_message)
            ->TPWhere($where,$connector)
            ->TPJoin($join)
            ->page($pageSize, $page)
            ->order($order)
            ->query()
            ->result_array();
        return $result;
    }
}