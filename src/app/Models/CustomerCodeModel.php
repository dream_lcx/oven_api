<?php

namespace app\Models;

use app\Services\Common\ReturnCodeService;
use Server\CoreBase\Model;

/**
 * 用户户号管理模型
 */
class CustomerCodeModel extends Model
{
    protected $table = 'customer_code';

    /**
     * @desc  查询一条用户数据
     * @param 无
     * @date   2018-07-18
     * @param array $where [description]
     * @param string $field [description]
     * @return [type]            [description]
     * @author lcx
     */
    public function getOne(array $where, string $field = "*",$join=[],$table='')
    {
        if(empty($table)){
            $table = $this->table;
        }
        $result = $this->db
            ->select($field)
            ->from($table)
            ->TPJoin($join)
            ->TPWhere($where)
            ->query()
            ->row();
        return $result;
    }

    /**
     * @desc   添加用户信息
     * @param 无
     * @date   2018-07-18
     * @param array $data [description]
     * @author lcx
     */
    public function add(array $data)
    {
        $id = $this->db->insert($this->table)
            ->set($data)
            ->query()
            ->insert_id();
        return $id;
    }

    /**
     * @desc  更新用户信息
     * @param 无
     * @date   2018-07-18
     * @param array $where [description]
     * @param array $data [description]
     * @return [type]            [description]
     * @author lcx
     */
    public function save(array $where, array $data)
    {
        $result = $this->db->update($this->table)
            ->set($data)
            ->TPwhere($where)
            ->query()
            ->affected_rows();
        return $result;
    }

    /**    lcx
     *     分页查询
     * @param array $where
     * @param int $page
     * @param int $pageSize
     * @param string $field
     * @param $join
     * @param array $order
     * @return mixed
     */
    public function getAll(array $where, string $field = '*', array $order = ['code_id' => 'ASC'], int $page = 1, int $pageSize = -1)
    {
        if ($pageSize < 0) {
            $result = $this->db->select($field)
                ->from($this->table)
                ->TPWhere($where)
                ->order($order)
                ->query()
                ->result_array();
        } else {
            $result = $this->db->select($field)
                ->from($this->table)
                ->TPWhere($where)
                ->order($order)
                ->page($pageSize, $page)
                ->query()
                ->result_array();
        }

        return $result;
    }

    /**
     *    数量查询
     * @param array $where
     * @param string $field
     * @return int
     * @throws \Server\CoreBase\SwooleException
     * @throws \Throwable
     */
    public function getCount(array $where, string $field = '*')
    {
        $result = $this->db->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->query()
            ->num_rows();
        return $result;
    }

    /**
     *    查询用户所有的户号
     * @param array $where
     * @param string $field
     * @return int
     * @throws \Server\CoreBase\SwooleException
     * @throws \Throwable
     */
    public function getUserCode($user_id,$field='*'){
        $data=$this->getAll(['user_id'=>$user_id],$field);
        if (empty($data)) {
            return returnResult(ReturnCodeService::NO_DATA, '暂无数据');
        }
        $code_id=[];
        $code=[];
        foreach ($data as $k=>$v){
            array_push($code_id,$v['code_id']);
            array_push($code,$v['code']);
        }
        $result=[
            'code_id'=>$code_id,
            'code'=>$code,
            'user_id'=>$user_id,
            'data'=>$data
        ];
        return returnResult(ReturnCodeService::SUCCESS, '获取成功', $result);

    }

    /**
     *    根据户号查询合同信息等
     * @param string $code_no 户号
     * @param string $field
     * @return int
     * @throws \Server\CoreBase\SwooleException
     * @throws \Throwable
     */
    public function getCodeContractInfo($code_no,$code_id=''){
        if(empty($code_no) && empty($code_id)){
            return returnResult(ReturnCodeService::NO_DATA, '缺少户号参数');
        }
        $EquipmentListsModel = get_instance()->loader->model('EquipmentListsModel', get_instance());
        $ContractEquipmentModel = get_instance()->loader->model('ContractEquipmentModel', get_instance());

        //查询户号信息
        $where = [];
        if(!empty($code_no)){
            $where['code'] = $code_no;
        }
        if(!empty($code_id)){
            $where['code_id'] = $code_id;
        }
        $code_info=$this->getOne($where,'*');
        if(empty($code_info)){
            return returnResult(ReturnCodeService::NO_DATA, '户号信息不存在');
        }
        //查询主板信息
        $eq_info=$EquipmentListsModel->getOne(['customer_code'=>$code_info['code']],'equipment_id,equipments_id,machine_id');
        if(empty($eq_info)){
            return returnResult(ReturnCodeService::NO_DATA, '暂无主板信息');
        }

        //查询合同主板信息
        $eq_contract_info=$ContractEquipmentModel->getOne(['equipment_id'=>$eq_info['equipment_id']],'contract_id');
        if(empty($eq_contract_info)){
            return returnResult(ReturnCodeService::NO_DATA, '暂无主板合同信息');
        }
        $data=$code_info;
        $data['equipment_id']=$eq_info['equipment_id'];
        $data['equipments_id']=$eq_info['equipments_id'];
        $data['machine_id']=$eq_info['machine_id'];
        $data['contract_id']=$eq_contract_info['contract_id'];

        return returnResult(ReturnCodeService::SUCCESS, '获取成功', $data);

    }







}