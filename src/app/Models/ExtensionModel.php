<?php

namespace app\Models;

use app\Services\Common\ReturnCodeService;
use Server\CoreBase\Model;

/**
 * 推广升级、返佣===1.0版本结算（已作废）
 * Class ExtensionModel
 * @package app\Models
 */
class ExtensionModel extends Model
{
    // 表名
    protected $dbName = 'oa_workers'; //市场推广表

    /**
     * @desc  查询一条数据
     * @param 无
     * @date   2018-07-18
     * @param array $where [description]
     * @param string $field [description]
     * @return [type]            [description]
     * @author lcx
     */
    public function getOne(array $where, string $field = "*", string $dbName)
    {
        $result = $this->db->select($field)
            ->from($dbName)
            ->TPWhere($where)
            ->query()
            ->row();
        return $result;
    }

    /**    YSF
     *     获取评价列表
     * @param array $where 查询条件
     * @param array $join 连表
     * @param string $field 查询字段
     * @return mixed
     */
    public function getAll(array $where, array $join, string $field, string $dbName)
    {
        $result = $this->db->select($field)
            ->from($dbName)
            ->TPWhere($where)
            ->TPJoin($join)
            ->query()
            ->result_array();
        return $result;
    }

    /**
     * @desc   添加信息
     * @param 无
     * @date   2018-07-18
     * @param array $data [description]
     * @author lcx
     */
    public function add(array $data, string $dbName)
    {
        $id = $this->db->insert($dbName)
            ->set($data)
            ->query()
            ->insert_id();
        return $id;
    }

    /**
     * @desc  更新信息
     * @param 无
     * @date   2018-07-18
     * @param array $where [description]
     * @param array $data [description]
     * @return [type]            [description]
     * @author lcx
     */
    public function save(array $where, array $data, string $dbName)
    {
        $result = $this->db->update($dbName)
            ->set($data)
            ->TPwhere($where)
            ->query()
            ->affected_rows();
        return $result;
    }

    /**
     * @title 计算团队业绩
     * Author: Xuleni
     * DateTime: 2020/10/21 10:03
     * @param int $user_id 用户ID
     * @param int $company_id 公司编码
     */
    public function teamPerformance(string $user_id, int $company_id)
    {
        //查询升级条件
        $balance_config = $this->upgradeStandard('upgrade_standard', 'value,state', $company_id);
        if ($balance_config['state'] == 0) return returnResult(ReturnCodeService::FAIL, '升级配置已关闭', '升级配置已关闭');
        $value_data = json_decode($balance_config['value'], true);
        $this->getArrays($user_id, $value_data); //递归处理团队业绩
        return returnResult(ReturnCodeService::SUCCESS, '统计业绩成功', '统计业绩成功');
    }

    /**
     * @title 获取推广信息
     * Author: Xuleni
     * DateTime: 2020/10/22 10:37
     */
    public function upgradeStandard(string $key, string $field, int $company_id)
    {
        $config_where['`key`'] = $key;
        $config_where['is_delete'] = 0;
        $config_where['company_id'] = $company_id;
        $balance_config = $this->getOne($config_where, $field, 'oa_balance_config');
        if (empty($balance_config)) return returnResult(ReturnCodeService::FAIL, '升级条件不存在', '升级条件不存在');
        return $balance_config;
    }

    /**
     * @title 递归处理团队业绩
     * Author: Xuleni
     * DateTime: 2020/10/22 9:20
     * @param $user_id 用户编码
     * @param $value_data //升级条件
     * @param int $num 循环次数
     */
    public function getArrays($user_id, $value_data, $num = 1)
    {
        $user_data_where['user_id'] = $user_id;
        $user_data = $this->getOne($user_data_where, 'source,is_dealer', 'customer');
        //如果存在则继续查找
        if (empty($user_data['source'])) return returnResult(ReturnCodeService::FAIL, '执行次数' . $num, $user_id . '无推荐人');
        if ($user_data['is_dealer'] == 1) {
            //查询团队人数
            $user_data_A = $this->queryPid($user_data['source']);
            //查询团队业绩
            $stove_core_achievement_A = $this->statisticsStoveCore($user_data_A);
            //判断业绩是否达标、升级
            $this->upgrade($value_data, $stove_core_achievement_A ?? 0, $user_data['source']);
        }
        //循环三次
        if ($num < 3) {
            if (!empty($user_data['source'])) $this->getArrays($user_data['source'], $value_data, $num + 1); //递归查找上级
        }
    }

    /**
     * @title 判断业绩，是否满足升级条件
     * Author: Xuleni
     * DateTime: 2020/10/21 14:41
     * @param $value_data 升级条件 一维数组
     * @param $achievement 业绩
     * @param $achievement 用户ID
     */
    public function upgrade($value_data, $achievement, $user_id)
    {
        $position = 0;
        foreach ($value_data as $key => $value) {
            if ($value <= $achievement) $position = $key; //对比满足的升级条件
        }
        if ($position > 0) {
            //修改等级
            $status = false;
            $this->db->begin(function () use (&$user_id, $position, &$status, $achievement) {
                //查询当前等级
                $position_user_where = ['user_id' => $user_id, 'is_delete' => 0];
                $position_user = $this->getOne($position_user_where, 'position', 'oa_workers');
                //修改等级
                if ($this->save(['user_id' => $user_id], ['position' => $position], 'oa_workers')) {
                    //添加记录
                    $workers_data = ['user_id' => $user_id, 'before_upgrade_grade' => $position_user['position'], 'after_upgrade' => $position, 'creation_time' => time()];
                    $this->add($workers_data, 'upgrade_record_log');
                }
                $status = true;
            }, function ($e) {
                return returnResult(ReturnCodeService::FAIL, '升级用户等级失败', '升级用户等级失败');
            });
        } else {
            return returnResult(ReturnCodeService::FAIL, $user_id . '用户未达到升级条件', $user_id . '用户未达到升级条件');
        }
        if ($status) {
            return returnResult(ReturnCodeService::SUCCESS, '升级用户等级成功', '升级用户等级成功');
        }
    }

    /**
     * @title 查询团队安装灶芯数量
     * Author: Xuleni
     * DateTime: 2020/10/21 14:31
     * @param $user_data 团队用户user_id 一维数组
     */
    public function statisticsStoveCore($user_data)
    {
        /**
         *  customer_code表actual_transformation_num字段和rq_energy_saving_data_test表cooker_num字段值一致。
         * 在第二次采集信息（Engineer/Workorder/addTestDataCooker）修改的
         */
        $code_where['user_id'] = ['IN', $user_data];
        $code_where['state'] = 1;
        $code_data = $this->getAll($code_where, [], "SUM(actual_transformation_num) as 'num'", 'customer_code');
        return $code_data[0]['num'];
    }

    /**
     * @title 查询用户团队
     * Author: Xuleni
     * DateTime: 2020/10/21 14:19
     * @param $pid 用户编码
     */
    public function queryPid($pid)
    {
        $data_where['source'] = $pid;
        $data = $this->getAll($data_where, [], 'user_id', 'customer');
        $a = [];
        foreach ($data as $key => $value) $a[] = $value['user_id'];
        return $a;
    }


    /**
     * @title 添加、更新合同关系表
     * Author: Xuleni
     * DateTime: 2020/10/22 14:20
     * @param string $contract_no
     */
    public function contractualRelationship(string $contract_no)
    {
        //查询合同信息
        $contract_where = ['contract_no' => $contract_no];
        $contract_data = $this->getOne($contract_where, 'contract_id,user_id,company_id', 'contract');
        if (empty($contract_data)) return returnResult(ReturnCodeService::FAIL, '合同不存在');
        $contract_rank_data = [];
        //直推
        $push_data = $this->getRecommender($contract_data['user_id']);
        if (!empty($push_data)) {
            //查询直推返佣比例、推广名称
            $upgrade_standard_data = $this->upgradeStandard('upgrade_standard', 'value,state,value_note', $contract_data['company_id']);
            $value_1 = json_decode($upgrade_standard_data['value'], true);
            $value_note_data = ['0' => '普通用户', '3' => '青铜', '4' => '白银', '5' => '黄金', '6' => '钻石'];
            $contract_rank_data['push_id'] = $push_data['user_id']; //直推人ID
            $contract_rank_data['push_name'] = $push_data['name'];  //直推人姓名
            $contract_rank_data['manage_id'] = $push_data['user_id'];// 市场推广ID
            $contract_rank_data['push_position'] = $push_data['position'];// 直推人等级
            $contract_rank_data['push_position_name'] = $value_note_data[$push_data['position']];// 直推人等级
            if ($push_data['position'] == 0) {
                $contract_rank_data['manage_position'] = 0;// 市场推广ID
                $contract_rank_data['manage_position_name'] = $value_note_data[$push_data['position']];// 市场推广等级名称
                $contract_rank_data['manage_name'] = $push_data['name'];// 市场推广名称
                $contract_rank_data['manage_money'] = 0; //市场推广分佣比例
            } else {
                foreach ($value_1 as $k => $v) {
                    if ($push_data['position'] == $k) {
                        $contract_rank_data['manage_position'] = $k;// 市场推广ID
                        $contract_rank_data['manage_position_name'] = $value_note_data[$k];// 市场推广等级名称
                        $contract_rank_data['manage_name'] = $push_data['name'];// 市场推广名称
                        $contract_rank_data['manage_money'] = $v; //市场推广分佣比例
                    }
                }
            }
            //间推
            $interposition = $this->getRecommender($push_data['user_id']);
            if (!empty($interposition)) {
                $contract_rank_data['interposition_id'] = $interposition['user_id']; //间推人ID
                $contract_rank_data['interposition_name'] = $interposition['name']; //间推人姓名
                $contract_rank_data['interposition_position'] = $interposition['position']; //间推人等级
                $contract_rank_data['interposition_position_name'] = $value_note_data[$interposition['position']]; //间推人等级名称
            }
        }
        //查询分佣比例
        $rebate_rules_data = $this->upgradeStandard('rebate_rules', 'value,state,value_note', $contract_data['company_id']);
        $value_2 = json_decode($rebate_rules_data['value'], true);
        //查询讲解人
        if ($contract_data['contract_id']) {
            $explainer = $this->getOne(['contract_id' => $contract_data['contract_id']], 'explainer', 'work_order');
            if (!empty($explainer['explainer'])) {
                $explainer_user = explode('-', $explainer['explainer']);
                $explainer_name = $this->getOne(['user_id' => $explainer_user[1]], 'realname', 'customer');
                $contract_rank_data['explainer_name'] = $explainer_name['realname'];
                $contract_rank_data['explainer_id'] = $explainer_user[1];
                $contract_rank_data['explainer_type'] = $explainer_user[0] == 'D' ? 1 : 2;
                $contract_rank_data['explainer_money'] = $value_2['explain']; //分解人比例
            }
        }
        $account = $this->upgradeStandard('account', 'value,state,value_note', $contract_data['company_id']);
        $value_3 = json_decode($account['value'], true);
        //市场总监
        $gm = $position_data = $this->getOne(['workers_number' => $value_3['gm_account_number']], 'workers_name,user_id', 'oa_workers');
        $contract_rank_data['gm_id'] = $gm['user_id']; //市场总监ID
        $contract_rank_data['gm_name'] = $gm['workers_name'];//市场总监名称
        $contract_rank_data['gm_money'] = $value_2['chief_inspector']; //市场总监比例
        //市场助理
        $gma = $position_data = $this->getOne(['workers_number' => $value_3['gma_account_number']], 'workers_name,user_id', 'oa_workers');
        $contract_rank_data['gma_id'] = $gma['user_id']; //市场助理ID
        $contract_rank_data['gma_name'] = $gma['workers_name']; //市场助理名称
        $contract_rank_data['gma_money'] = $value_2['help']; //市场助理比例
        $contract_rank_data['push_additional_money'] = $value_2['push']; //直推额外比例
        $contract_rank_data['interposition_additional_money'] = $value_2['interposition']; //间推额外比例
        //合同关系是否存在
        if (empty($this->getOne(['contract_no' => $contract_no], 'contract_no', 'contract_rank'))) {
            //添加
            $user_position = $this->getOne(['user_id' => $contract_data['user_id']], 'position', 'oa_workers');
            $contract_rank_data['contract_user_id'] = $contract_data['user_id'];
            $contract_rank_data['contract_no'] = $contract_no;
            $contract_rank_data['contract_user_identity'] = $user_position['position'] == '' ? 0 : $this->config->get('position')[$user_position['position']];
            $msg = $this->add($contract_rank_data, 'contract_rank');
        } else {
            //修改
            $msg = $this->save(['contract_no' => $contract_no], $contract_rank_data, 'contract_rank');
        }
        if ($msg) {
            return returnResult(ReturnCodeService::SUCCESS, '操作成功');
        } else {
            return returnResult(ReturnCodeService::FAIL, '操作失败');
        }
    }

    /**
     * @title 查找上级
     * Author: Xuleni
     * DateTime: 2020/10/22 10:31
     * @param string $user_id 用户编码
     */
    public function getRecommender(string $user_id)
    {
        $where = ['user_id' => $user_id];
        $pid = $this->getOne($where, 'source', 'customer');
        if (empty($pid['source'])) return [];
        $user_name = $this->getOne(['user_id' => $pid['source']], 'realname,source', 'customer');
        $position_data = $this->getOne(['user_id' => $pid['source']], 'position', 'oa_workers');
        if (empty($position_data)) $position = $position_data['position'];
        return ['user_id' => $pid['source'], 'name' => $user_name['realname'], 'position' => $position ?? 0];
    }


    /**
     * Author: Xuleni
     * DateTime: 2020/10/22 14:38
     * @title 计算佣金
     * @param $contract_no 合同编码
     * @param $payment_amount 缴费金额
     * @param $pay_id 缴费订单ID
     * @param $company_id 公司
     */
    public function settlement(array $bill_id)
    {
        //查询合同编码
        $join = [
            ['customer_code a', 'rq_customer_code_bill.customer_code = a.code', 'left join'],   // 户号表
            ['contract b', 'b.contract_id = a.contract_id', 'left join']   // 合同表
        ];
        $field = "b.contract_no,a.company_id,money,bill_id";
        $customer_code_bill_data = $this->getAll(['bill_id' => ['IN', $bill_id], 'is_settlement' => 0], $join, $field, 'customer_code_bill');
        if (empty($customer_code_bill_data)) return returnResult(ReturnCodeService::SUCCESS, '缴费信息不存在');
        //查询关系
        $field = "manage_position_name,push_id,push_name,interposition_id,interposition_name,manage_id,manage_position,manage_name,manage_money,explainer_id,explainer_name,explainer_money,gma_id,gma_name,gma_money,gm_id,gm_name,gm_money,push_additional_money,interposition_additional_money";
        $contract_rank_data = $this->getOne(['contract_no' => $customer_code_bill_data[0]['contract_no']], $field, 'contract_rank');
        if (empty($contract_rank_data)) return returnResult(ReturnCodeService::FAIL, '合同不存在');
        //循环处理佣金
        foreach ($customer_code_bill_data as $key => $value) {
            $money = formatMoney($value['money'], 2);
            if ($money > 0) {
                $this->db->begin(function () use (&$value, &$contract_rank_data, &$money) {
                    //结算记录
                    $achievement_id = $this->achievementLog($value['bill_id'], $value['contract_no']);
                    //推荐奖励分佣方法
                    $this->extensionCents($contract_rank_data, $achievement_id, $money, $value['company_id']);
                    //工程人员分佣方法
                    $this->engineeringStaffCents($contract_rank_data, $achievement_id, $money, $value['company_id']);
                });
            }
            //修改缴费订单分佣状态
            $this->save(['bill_id' => $value['bill_id']], ['is_settlement' => 1], 'customer_code_bill');
        }
        return returnResult(ReturnCodeService::FAIL, '操作成功');
    }


    /**
     * @title 工程人员分佣方法  讲解人、市场总监、市场助理
     * Author: Xuleni
     * DateTime: 2020/10/30 10:34
     * @param $contract_rank_data 返佣规则
     * @param $achievement_id 结算记录ID
     * @param $payment_amount 公司ID
     * @param $company_id 金额
     */
    public function engineeringStaffCents($contract_rank_data, $achievement_id, $payment_amount, $company_id)
    {
        if (!empty($contract_rank_data['explainer_id'])) {
            $explainer_money = ($payment_amount * $contract_rank_data['explainer_money']) / 100; //分佣金额：缴费金额 * 比例
            $explainer_workers_id = $this->getOne(['user_id' => $contract_rank_data['explainer_id']], 'workers_id', 'oa_workers');
            $explainer_workers_log = ['achievement_id' => $achievement_id, 'workers_position' => 9, 'money' => $explainer_money, 'explainer_id' => $contract_rank_data['explainer_id'], 'workers_id' => $explainer_workers_id['workers_id']];
            $this->achievementWorkersLog($explainer_workers_log); //添加分佣日志
            $explainer_remarks = "讲解人：" . $contract_rank_data['explainer_name'] . "获得金额：" . $explainer_money . '元；';
            $explainer_update_balance = ['user_id' => $contract_rank_data['explainer_id'], 'money' => $explainer_money, 'remarks' => $explainer_remarks];
            $this->seveMoney($explainer_update_balance);//更新余额
            //工程人员业绩
            $oaEngineersPerformance_data = ['engineers_id' => $contract_rank_data['explainer_id'], 'achievement_id' => $achievement_id, 'money' => $explainer_money];
            $this->oaEngineersPerformance_log($oaEngineersPerformance_data);
        }
        //获取市场总监、助理用户账号
        $account = $this->upgradeStandard('account', 'value,state,value_note', $company_id);
        $account_data = json_decode($account['value'], true);
        //市场助理
        $gma_money = ($payment_amount * $contract_rank_data['gma_money']) / 100; //市场助理：缴费金额 * 比例
        $gma_workers_id = $this->getOne(['workers_number' => $account_data['gma_account_number']], 'workers_id', 'oa_workers');
        $gma_workers_log = ['achievement_id' => $achievement_id, 'workers_position' => 11, 'money' => $gma_money, 'workers_number' => $account_data['gma_account_number'], 'workers_id' => $gma_workers_id['workers_id']];
        $this->achievementWorkersLog($gma_workers_log);//添加分佣日志
        $gma_remarks = $contract_rank_data['gma_name'] . "获得金额：" . $gma_money . "元；";
        //查询市场助理用户ID
        $gma_update_balance = ['user_id' => $contract_rank_data['gma_id'], 'workers_number' => $account_data['gma_account_number'], 'money' => $gma_money, 'remarks' => $gma_remarks];
        $this->seveMoney($gma_update_balance, 1);//更新余额
        //市场总监
        $gm_money = ($payment_amount * $contract_rank_data['gm_money']) / 100; //市场总监：缴费金额 * 比例
        $gm_workers_id = $this->getOne(['workers_number' => $account_data['gm_account_number']], 'workers_id', 'oa_workers');
        $gm_workers_log = ['achievement_id' => $achievement_id, 'workers_position' => 12, 'money' => $gm_money, 'workers_number' => $account_data['gm_account_number'], 'workers_id' => $gm_workers_id['workers_id']];
        $this->achievementWorkersLog($gm_workers_log); //添加分佣日志
        $gm_remarks = $contract_rank_data['gm_name'] . "获得金额：" . $gm_money . "元";
        $gm_update_balance = ['user_id' => $contract_rank_data['gm_id'], 'workers_number' => $account_data['gm_account_number'], 'money' => $gm_money, 'remarks' => $gm_remarks];
        $this->seveMoney($gm_update_balance, 1);//更新余额
    }

    /**
     * @title 推荐奖励分佣方法  直推、间推、直推间推额外奖励
     * Author: Xuleni
     * DateTime: 2020/10/30 10:28
     * @param $contract_rank_data 返佣规则
     * @param $achievement_id 结算记录ID
     * @param $company_id 公司ID
     * @param $payment_amount 金额
     */
    public function extensionCents($contract_rank_data, $achievement_id, $payment_amount, $company_id)
    {
        if (!empty($contract_rank_data['push_id'])) {
            $push_money = 0;//默认返佣金额
            $push_remarks = ''; //默认备注
            $upgrade_standard_data = $this->upgradeStandard('rebate_rules', 'value,state,value_note', $company_id);
            $value_1 = json_decode($upgrade_standard_data['value'], true);
            $proportion = 0;//查询比例
            foreach ($value_1 as $k => $v) {
                if ($contract_rank_data['manage_position'] == $k) {
                    $proportion = $v;
                    break;
                }
            }
            if ($proportion > 0) {
                $manage_money = ($payment_amount * $proportion) / 100;//分佣金额：缴费金额 * 推广等级比例
                $push_remarks .= "市场推广等级：" . $contract_rank_data['manage_position_name'] . "，所得金额：" . $manage_money . '元；';
                $push_money += $manage_money; //累加推广金额
                $push_workers_id = $this->getOne(['user_id' => $contract_rank_data['push_id']], 'workers_id', 'oa_workers');
                $push_workers_log = ['achievement_id' => $achievement_id, 'workers_position' => $contract_rank_data['manage_position'], 'money' => $manage_money, 'workers_id' => $push_workers_id['workers_id']];
                $this->achievementWorkersLog($push_workers_log); //添加分佣日志
            } else {
                //普通身份优惠券
                $this->add_coupon($contract_rank_data, $company_id);
            }
            //是否是钻石推广等级
            if ($contract_rank_data['manage_position'] == 6) {
                //给直推返佣
                $push_additional_money = ($payment_amount * $contract_rank_data['push_additional_money']) / 100; //分佣金额 缴费金额 * 直推额外比例
                $push_remarks .= "直推额外金额：" . $push_additional_money . '元；';
                $push_money += $push_additional_money;
                $manage_workers_id = $this->getOne(['user_id' => $contract_rank_data['push_id']], 'workers_id', 'oa_workers');
                $manage_workers_log = ['achievement_id' => $achievement_id, 'workers_position' => 7, 'money' => $push_additional_money, 'workers_id' => $manage_workers_id['workers_id']];
                $this->achievementWorkersLog($manage_workers_log); //添加分佣日志
                //间推是否存在
                if (!empty($contract_rank_data['interposition_id'])) {
                    $interposition_additional_money = ($payment_amount * $contract_rank_data['interposition_additional_money']) / 100;//分佣金额 缴费金额 * 间推额外比例
                    $manage_workers_id = $this->getOne(['user_id' => $contract_rank_data['interposition_id']], 'workers_id', 'oa_workers');
                    $interposition_workers_log = ['achievement_id' => $achievement_id, 'workers_position' => 8, 'money' => $interposition_additional_money, 'workers_id' => $manage_workers_id['workers_id']];
                    $this->achievementWorkersLog($interposition_workers_log); //添加分佣日志
                    $interposition_remarks = "间推额外金额：" . $interposition_additional_money;
                    $interposition_update_balance = ['user_id' => $contract_rank_data['interposition_id'], 'money' => $interposition_additional_money, 'remarks' => $interposition_remarks];
                    $this->seveMoney($interposition_update_balance);//更新余额
                }
            }
            //给直推、市场推广返佣
            $push_update_balance = ['user_id' => $contract_rank_data['push_id'], 'money' => $push_money, 'remarks' => $push_remarks];
            $this->seveMoney($push_update_balance);
        }
    }

    /**
     * @title 添加优惠券
     * Author: Xuleni
     * DateTime: 2020/10/30 14:05
     * @param $contract_rank_data
     * @param $company_id
     */
    public function add_coupon($contract_rank_data, $company_id)
    {
        $coupon_cate_where = ['company_id' => $company_id, 'put_type' => 5, 'status' => 1];
        $coupon_cate_data = $this->getOne($coupon_cate_where, 'id,time_type,use_days,use_start_time,use_end_time,receive_num,total_num,send_start_time,send_end_time', 'coupon_cate');
        if (!empty($coupon_cate_data) && $coupon_cate_data['receive_num'] < $coupon_cate_data['total_num'] && $coupon_cate_data['send_start_time'] < time() && $coupon_cate_data['send_end_time'] > time()) {
            //计算优惠券有效期
            if ($coupon_cate_data['time_type'] == 1) {
                //1领取后X天内有效
                $valid_time_start = time();
                $valid_time_end = time() + $coupon_cate_data['use_days'] * 86400;
            } else {
                //2固定时间段有效
                $valid_time_start = $coupon_cate_data['use_start_time'];
                $valid_time_end = $coupon_cate_data['use_end_time'];
            }
            $coupon_data = [
                'cid' => $coupon_cate_data['id'],
                'uid' => $contract_rank_data['push_id'],
                'code' => generate_promotion_code(),
                'type' => 5,//缴费赠送
                'status' => 1,//待使用
                'send_time' => time(),//领取时间
                'valid_time_start' => $valid_time_start,
                'valid_time_end' => $valid_time_end,
                'pid' => $contract_rank_data['interposition_id'] ?? '',
                'note' => '推荐用户续费送券'
            ];
            //添加优惠券
            $this->add($coupon_data, 'coupon');
        }
    }

    /**
     * showdoc
     * Author: Xuleni
     * DateTime: 2020/10/22 18:01
     * @title 更新余额、添加日志
     * @param array $data
     */
    public function seveMoney(array $data, int $type = 0)
    {
        if ($data['money'] > 0) {
            $workers_data = $this->getOne(['user_id' => $data['user_id']], 'total_money,temp_money', 'oa_workers');
            $workers_data['total_money'] += $data['money'];
            $workers_data['temp_money'] += $data['money'];
            $workers_where['user_id'] = $data['user_id'];
            if ($type == 1) $workers_where['workers_number'] = $data['workers_number'];
            $this->save($workers_where, $workers_data, 'oa_workers');
            //写入账变记录
            $account_log = [
                'user_id' => $data['user_id'],
                'user_type' => 1,
                'source' => 2,
                'account_type' => 1,
                'money' => $data['money'],
                'note' => $data['remarks'],
                'add_time' => time(),
            ];
            if ($this->add($account_log, 'funds_record')) {
                return returnResult(ReturnCodeService::SUCCESS, '成功');
            } else {
                return returnResult(ReturnCodeService::FAIL, '失败');
            }
        }
    }

    /**
     * Author: Xuleni
     * DateTime: 2020/10/22 16:47
     * @title 添加结算记录
     * @param int $pay_id
     * @param string $contract_no
     */
    public function achievementLog(int $pay_id, string $contract_no)
    {
        $data = [
            'achievement_work_order_type' => 5,
            'from_renew_order_id' => $pay_id,
            'contract_number' => $contract_no,
            'add_time' => time()
        ];
        $id = $this->add($data, 'oa_achievement');
        return $id;
    }


    /**
     * Author: Xuleni
     * DateTime: 2020/10/22 17:01
     * @title 分佣记录
     * @param array $data
     */
    public function achievementWorkersLog(array $data)
    {
        $achievement_workers_data = [
            'achievement_id' => $data['achievement_id'],
            'source' => 1,
            'role' => 1,
            'workers_id' => $data['workers_id'],
            'sort' => 1,
            'is_available' => 2,
            'workers_position' => $data['workers_position'], //1
            'money' => $data['money'], //10
            'add_time' => time()
        ];
        $id = $this->add($achievement_workers_data, 'oa_achievement_workers');
        return $id;
    }

    /**
     * @title 工程人员业绩记录
     * Author: Xuleni
     * DateTime: 2020/11/2 11:11
     * @param array $data
     * @return mixed
     */
    public function oaEngineersPerformance_log(array $data)
    {
        $oa_engineers_performance_data = [
            'engineers_id' => $data['engineers_id'], //工程人员ID
            'achievement_id' => $data['achievement_id'],//业绩表ID
            'money' => $data['money'],//金额
            'is_available' => 2,
            'add_time' => time()
        ];
        $id = $this->add($oa_engineers_performance_data, 'oa_engineers_performance');
        return $id;
    }


    /**
     * Author: Xuleni
     * DateTime: 2020/11/3 11:45
     * @title 工程新装结算
     * @param string $contract_no
     */
    public function newClothesPrice(string $contract_id)
    {
        //查询合同
        $contract_data = $this->getOne(['contract_id' => $contract_id], 'company_id,status', 'contract');
        if (empty($contract_data)) return returnResult(ReturnCodeService::FAIL, '合同不存在');
        if ($contract_data['status'] != 4) return returnResult(ReturnCodeService::FAIL, '合同状态不正确');
        //查询计算比例
        $balance_config = $this->upgradeStandard('rebate_rules', 'value', $contract_data['company_id']);
        $rebate_rules = json_decode($balance_config['value'], true);
        $settlement_price = $rebate_rules['settlement_price'];
        //查询灶芯数量
        $energy_data = $this->getOne(['contract_id' => $contract_id,'type'=>1], 'work_order_id,cooker_num', 'energy_saving_data_test');
        $num = $energy_data['cooker_num']; //灶芯数量
        if (empty($energy_data)) return returnResult(ReturnCodeService::FAIL, '数据不存在');
        //查询工程人员
        $work_engineering_relationship = $this->getAll(['work_order_id' => $energy_data['work_order_id']], [], 'work_order_id,repair_id', 'work_engineering_relationship');
        if (empty($work_engineering_relationship)) return returnResult(ReturnCodeService::FAIL, '工程人员不存在');
        //工程人员数量
        $work_engineering_relationship_num = count($work_engineering_relationship);
        $money = ($num * $settlement_price) / $work_engineering_relationship_num;
//        var_dump('数量：' . $num . '_金额：' . $settlement_price . '工程数量：' . $work_engineering_relationship_num);
        foreach ($work_engineering_relationship as $key => $value) {
            $data = ['money' => round($money,2), 'engineers_id' => $value['repair_id'], 'work_order_id' => $value['work_order_id']];
            $this->addEngineeringMoney($data);
        }
    }

    public function addEngineeringMoney(array $data)
    {
        if ($data['money'] > 0) {
            //更新金额
            $workers_data = $this->getOne(['engineers_id' => $data['engineers_id']], 'temp_money', 'engineers');
//            var_dump('工程ID:' . $data['engineers_id'] . '更新前金额：' . $workers_data['temp_money']);
            $workers_data['temp_money'] += $data['money'];
//            var_dump('工程ID:' . $data['engineers_id'] . '更新后金额：' . $workers_data['temp_money']);
            $this->save(['engineers_id' => $data['engineers_id']], $workers_data, 'engineers');
            //写入记录
            $engineer_bill_data = [
                'engineers_id' => $data['engineers_id'],
                'work_order_id' => $data['work_order_id'],
                'money' => $data['money'],
                'pay_type' => 2,
                'pay_time' => time(),
            ];
//            var_dump(json_encode($data));
            $this->add($engineer_bill_data, 'engineer_bill');
        }

    }


}