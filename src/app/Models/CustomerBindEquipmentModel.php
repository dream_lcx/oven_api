<?php
namespace app\Models;

use Server\CoreBase\Model;

/**   YSF
 *    用户绑定主板记录 Model
 *    Date: 2018/7/25
 * Class CustomerBindEquipmentModel
 * @package app\Models
 */
class CustomerBindEquipmentModel extends Model
{
    // 表名
    protected $table = 'customer_bind_equipment';

    /**   YSF
     *    添加用户绑定记录
     * @param array $data 数据
     * @return mixed
     */
    public function addCustomerBindEquipment(array $data)
    {
        $result = $this->db->insert($this->table)
            ->set($data)
            ->query()
            ->insert_id();
        return $result;
    }

    /**   YSF
     *    查询用户绑定主板列表
     * @param array $where   查询条件
     * @param int $page      当前页
     * @param int $pageSize  每页条数
     * @param array $join    连表
     * @param string $field  查询字段
     * @param array $order   排序方式
     * @param $dbName        主表
     * @return mixed
     */
    public function userBindEquipmentList(array $where, int $page, int $pageSize,array $join, string $field, array $order, $dbName,$group='')
    {
        if(!empty($group)){
            $result = $this->db->select($field)
                ->from($dbName)
                ->TPWhere($where)
                ->TPJoin($join)
                ->page($pageSize, $page)
                ->order($order)
                ->groupBy($group)
                ->query()
                ->result_array();
        }else{
            $result = $this->db->select($field)
                ->from($dbName)
                ->TPWhere($where)
                ->TPJoin($join)
                ->page($pageSize, $page)
                ->order($order)
                ->query()
                ->result_array();
        }

        return $result;
    }
       /**
     * @desc  查询一条数据
     * @param  无
     * @date   2018-07-18
     * @author lcx
     * @param  array      $where [description]
     * @param  string     $field [description]
     * @return [type]            [description]
     */
    public function getOne(array $where, string $field="*"){
        $result = $this->db
            ->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->query()
            ->row();
        return $result;       
    }

    /**
     *
     * @author ligang
     * @param array $where
     * @param string $field
     * @param array $join
     * @return null
     * @throws \Server\CoreBase\SwooleException
     * @throws \Throwable
     * @date 2018/12/26 13:56
     */
    public function findBindInfo(array $where, string $field="*",array $join = []){
        $result = $this->db
            ->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->TPJoin($join)
            ->query()
            ->row();
        return $result;
    }
    
    /**    lcx
     *     查询所有
     * @param array $where
     * @param int $page
     * @param int $pageSize
     * @param string $field
     * @param $join
     * @param array $order
     * @return mixed
     */
    public function getAll(array $where, string $field = '*',array $join=[])
    {
        $result = $this->db->select($field)
                ->from($this->table)
                ->TPJoin($join)
                ->TPWhere($where)
                ->query()
                ->result_array();

        return $result;
    }
     /**
     * @desc   
     * @param  无
     * @date   2018-07-18
     * @author lcx
     * @param  array      $data [description]
     */
    public function count(array $map,array $join=array()){
         $result = $this->db->select('*')
                ->from($this->table)
                ->TPWhere($map)
                ->TPJoin($join)
                ->query()
                ->num_rows();
         return $result;
    }
     /**
     * @desc  更新信息
     * @param  无
     * @date   2018-07-18
     * @author lcx
     * @param  array      $where [description]
     * @param  array      $data  [description]
     * @return [type]            [description]
     */
    public function save(array $where,array $data){             
        $result = $this->db->update($this->table)
            ->set($data)
            ->TPwhere($where)
            ->query()
            ->affected_rows();               
        return $result;
    }

}