<?php
namespace app\Models;
use Server\CoreBase\Model;
/**
 * 用户身份证信息
 */
class ContractIdcardModel extends Model
{
    protected $table = 'customer_idcard';
    /**
     * @desc   添加信息
     * @param  无
     * @date   2018-07-20
     * @author lcx
     * @param  array      $data [description]
     */
    public function add(array $data){             
        $id = $this->db->insert($this->table)
            ->set($data)
            ->query()
            ->insert_id();   
        return $id;
    }
 
 
}
