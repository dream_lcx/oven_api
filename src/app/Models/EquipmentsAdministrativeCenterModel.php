<?php

namespace app\Models;

use Server\CoreBase\Model;

/**
 * 产品所属运营/行政中心模型
 */
class EquipmentsAdministrativeCenterModel extends Model {

    protected $table = 'equipments_administrative_center';

    /**
     * @desc  查询一条数据
     * @param  无
     * @date   2018-07-18
     * @author lcx
     * @param  array      $where [description]
     * @param  string     $field [description]
     * @return [type]            [description]
     */
    public function getOne(array $where, string $field = "*") {
        $result = $this->db
            ->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->query()
            ->row();
        return $result;
    }


    /**   YSF
     *    分页
     * @param array $where   查询条件
     * @param int $page      当前页码
     * @param int $pageSize  每页数量
     * @param string $field  查询字段
     * @param array $order   排序方式
     * @return mixed
     */
    public function getAll(array $where, int $page, int $pageSize, string $field = '*', array $join = array(), array $order = ['id' => 'DESC']) {
        $result = $this->db->select($field)
            ->from($this->table)
            ->TPJoin($join)
            ->TPWhere($where)
            ->page($pageSize, $page)
            ->order($order)
            ->query()
            ->result_array();

        return $result;
    }
    /**   YSF
     *    全部
     * @param array $where   查询条件
     * @param string $field  查询字段
     * @return mixed
     */
    public function getLists(array $where,string $field = '*') {
        $result = $this->db->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->query()
            ->result_array();
        return $result;
    }

    // 获取总数量
    public function getCount(array $where, string $field = '*', array $join = array()) {
        $result = $this->db->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->TPJoin($join)
            ->query()
            ->num_rows();
        return $result;
    }

}
