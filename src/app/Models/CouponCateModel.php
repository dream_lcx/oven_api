<?php
namespace app\Models;

use Server\CoreBase\Model;

/**  YSF
 *   优惠券分类 模型
 *   Date: 2018/12/4
 * Class CouponCateModel
 * @package app\Models
 */
class CouponCateModel extends Model
{
    // 表名
    protected $table = 'coupon_cate';

    // 单条查询
    public function getOne(array $where, string $field='*')
    {
        $result = $this->db->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->query()
            ->row();
        return $result;
    }

    // 编辑操作
    public function edit(array $where, array $data)
    {
        $result = $this->db->update($this->table)
                ->set($data)
                ->TPWhere($where)
                ->query()
                ->affected_rows();
        return $result;
    }

    // 单表列表查询
    public function getAll(array $where, string $field='*')
    {
        $result = $this->db->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->query()
            ->result_array();
        return $result;
    }

    // 连表列表查询
    public function getJoinAll(array $where, array $join, int $page, int $pageSize, string $field, array $order)
    {
        $result = $this->db->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->TPJoin($join)
            ->page($pageSize, $page)
            ->order($order)
            ->query()
            ->result_array();

        return $result;
    }
    //添加
    public function add(array $data)
    {
        $result = $this->db->insert($this->table)
            ->set($data)
            ->query()
            ->insert_id();
        return $result;
    }
}