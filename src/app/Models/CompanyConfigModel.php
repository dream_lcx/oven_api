<?php
namespace app\Models;
use Server\CoreBase\Model;
/**
 * 公司配置信息
 */
class CompanyConfigModel extends Model
{
    protected $table = 'company_config';
    public function getAll(array $where, int $page=1, int $pageSize=10, string $field = '*', array $join=[],array $order = ['create_time' => 'DESC'])
    {
        $result = $this->db->select($field)
                ->from($this->table)
                ->TPWhere($where)
                ->TPJoin($join)
                ->page($pageSize,$page)
                ->order($order)
                ->query()
                ->result_array();
        return $result;
    }
    /**
     * @desc  查询一条数据
     * @param  无
     * @date   2018-07-18
     * @author lcx
     * @param  array      $where [description]
     * @param  string     $field [description]
     * @return [type]            [description]
     */
    public function getOne(array $where, string $field="*"){
        $result = $this->db
            ->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->query()
            ->row();
        return $result;       
    }
    /**
     * @desc   统计数量
     * @param  无
     * @date   2018-12-6
     * @author lcx
     * @param  array      $data [description]
     */
    public function count($map, $join=[]) {
        $result = $this->db->select('*')
                ->from($this->table)
                ->TPWhere($map)
                ->TPJoin($join)
                ->query()
                ->num_rows();
        return $result;
    }
    /**
     * @desc   添加信息
     * @param  无
     * @date   2018-07-18
     * @author lcx
     * @param  array      $data [description]
     */
    public function add(array $data){             
        $id = $this->db->insert($this->table)
            ->set($data)
            ->query()
            ->insert_id();   
        return $id;
    }
    /**
     * @desc  更新信息
     * @param  无
     * @date   2018-07-18
     * @author lcx
     * @param  array      $where [description]
     * @param  array      $data  [description]
     * @return [type]            [description]
     */
    public function save(array $where,array $data){             
        $result = $this->db->update($this->table)
            ->set($data)
            ->TPwhere($where)
            ->query()
            ->affected_rows();               
        return $result;
    }
 
}