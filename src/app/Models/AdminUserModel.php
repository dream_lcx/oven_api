<?php
namespace app\Models;
use Server\CoreBase\Model;
/**
 * 管理员
 */
class AdminUserModel extends Model
{
    protected $table = 'admin_user';
    /**
     * @desc  查询一条数据
     * @param  无
     * @date   2018-07-18
     * @author lcx
     * @param  array      $where [description]
     * @param  string     $field [description]
     * @return [type]            [description]
     */
    public function getOne(array $where, string $field="*"){
        $result = $this->db
            ->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->query()
            ->row();
        return $result;       
    }
    /**
     * @desc   添加信息
     * @param  无
     * @date   2018-07-18
     * @author lcx
     * @param  array      $data [description]
     */
    public function add(array $data){             
        $id = $this->db->insert($this->table)
            ->set($data)
            ->query()
            ->insert_id();   
        return $id;
    }
    /**
     * @desc  更新信息
     * @param  无
     * @date   2018-07-18
     * @author lcx
     * @param  array      $where [description]
     * @param  array      $data  [description]
     * @return [type]            [description]
     */
    public function save(array $where,array $data){
        $result = $this->db->update($this->table)
            ->set($data)
            ->TPwhere($where)
            ->query()
            ->affected_rows();
        return $result;
    }
    /**
     * @desc   统计数量
     * @param  无
     * @date   2018-12-6
     * @author lcx
     * @param  array      $data [description]
     */
    public function count($map, $join=[]) {
        $result = $this->db->select('*')
            ->from($this->table)
            ->TPWhere($map)
            ->TPJoin($join)
            ->query()
            ->num_rows();
        return $result;
    }
    /**    lcx
     *     分页查询
     * @param array $where
     * @param string $field
     * @param $join
     * @param array $order
     * @return mixed
     */
    public function getAll(array $where, string $field = '*',array $join=array(),array $order = [])
    {
        $result = $this->db->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->TPJoin($join)
            ->order($order)
            ->query()
            ->result_array();
        return $result;
    }

}