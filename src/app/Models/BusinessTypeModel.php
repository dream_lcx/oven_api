<?php
namespace app\Models;
use Server\CoreBase\Model;
/**
 * 业务类型
 */
class BusinessTypeModel extends Model
{
    protected $table = 'business_type';
    /**
     * @desc  查询一条数据
     * @param  无
     * @date   2018-07-18
     * @author lcx
     * @param  array      $where [description]
     * @param  string     $field [description]
     * @return [type]            [description]
     */
    public function getOne(array $where, string $field="*"){
        $result = $this->db
            ->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->query()
            ->row();
        return $result;       
    }
     /**
     * @desc   查询所有
     * @param  无
     * @date   2018-07-20
     * @author lcx
     * @param  array      $data [description]
     */
    public function getAll(array $where, string $field = '*',array $order = ['sort' => 'ASC'])
    {
        $result = $this->db->select($field)
                ->from($this->table)
                ->TPWhere($where)
                ->order($order)
                ->query()
                ->result_array();
        return $result;
    }

    /**
     * @desc   添加信息
     * @param  无
     * @date   2018-07-18
     * @author lcx
     * @param  array      $data [description]
     */
    public function add(array $data){
        $id = $this->db->insert($this->table)
            ->set($data)
            ->query()
            ->insert_id();
        return $id;
    }

 
}