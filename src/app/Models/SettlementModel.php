<?php


namespace app\Models;


use app\Library\SLog;
use app\Services\Common\CommonService;
use app\Services\Common\ConfigService;
use app\Services\Common\ReturnCodeService;
use app\Services\Log\FileLogService;
use Server\CoreBase\Model;

class SettlementModel extends Model
{
    //配置项
    /***********持续收益20%*********/
    const DIRECT_PUSH_WORKER_RATE = 0.2; //若直推的是市场推广,获得20%的现金
    const DIRECT_PUSH_USER_RATE = 0.1;//若直推的是普通用户,获得10%的优惠券,向上找市场推广获得剩下的10%
    /***********讲解收益2%*************/
    const EXPLAIN_RATE = 0.02;//讲解人获得
    const TWOEXPLANATIONS = 0.01;//讲解人获得

    /**********工程新装==仅试用安装或改造安装时有该效益*************/
    const ENGINEER_INSTALL_MONEY_EYE = 200;//安装200元/眼
    const ENGINEER_TRAFFICL_MONEY_PERSON = 0;//交通补贴50元/人/次

    /***********工程维护===划到区域总管名下从第二月开始*********************/
    const ENGINEER_MAINTAIN_MONEY_EYE_MONTH = 5;//5元/月/眼

    /***************区域承包商维护补贴*********************/
    const PROVINCE_REGION_MAINTAIN_MONEY_EYE_YEAR = 50;//省级维护补贴,50/年/眼
    const CITY_REGION_MAINTAIN_MONEY_EYE_YEAR = 45;//市级维护补贴,45/年/眼
    const AREA_REGION_MAINTAIN_MONEY_EYE_YEAR = 40;//区级维护补贴,40/年/眼

    /*****************工程部长、工程经理维护补贴****************/
    const ENGINEER_DEPARTMENTMINISTER_MONEY_EYE_YEAR = 5;//工程部长维护补贴 5/年/眼
    const ENGINEER_DEPARTMENTMASTER_MONEY_EYE_YEAR = 5;//工程经理维护补贴 5/年/眼


    /****************公司拨付***********************/
    const COMPANY_PAY_WORKER_RATE = 0.35;//公司拨付市场推广35%；
    const COMPANY_PAY_INSTALL_MONEY_EYE = 200;//公司拨付安装200/眼
    const COMPANY_PAY_MAINTAIN_MONEY_EYE_YEAR = 120;//公司拨付售后维护120/眼

    /***************区域承包商享受收益,推荐人收益**********************/
    const PROVINCE_REGION_RATE = 0.3;//省级享受收益
    const CITY_REGION_RATE = 0.25;//市级享受收益
    const AREA_REGION_RATE = 0.2;//区级享受收益
    const PROVINCE_REGION_PUSH_RATE = 0.03;//省级推荐人享受收益
    const CITY_REGION_PUSH_RATE = 0.02;//市级推荐人享受收益
    const AREA_REGION_PUSH_RATE = 0.01;//区级推荐人享受收益

    /***************阶梯增长*********************/
    const PROVINCE_WORKER_STEP_RATE = 0.02;//省级推广服务承包
    const CITY_WORKER_STEP_RATE = 0.02;//市级推广服务承包
    const AREA_WORKER_STEP_RATE = 0.02;//区级推广服务承包
    const PROVINCE_WORKER_PUSH_RATE = 0.01;//推荐收益1-1%

    /*******************市场分成*******************/
    const MARKET_CHIEF_RATE = 0.01;//市场总监1%
    const MARKET_ASSISANT_RATE = 0.01;//市场助理1%


    protected $customerCodeBillModel;
    protected $customerCodeModel;
    protected $customerModel;
    protected $oaWorkersModel;
    protected $oaWorkersBillModel;
    protected $couponModel;
    protected $couponCateModel;
    protected $achievementModel;
    protected $engineersModel;
    protected $engineersBillModel;
    protected $adminAccountModel;
    protected $adminAccountBillModel;
    protected $oaAchievementWorkersModel;


    public function initialization(&$context)
    {
        parent::initialization($context);
        $this->customerCodeBillModel = $this->loader->model('CustomerCodeBillModel', $this);
        $this->customerCodeModel = $this->loader->model('CustomerCodeModel', $this);
        $this->customerModel = $this->loader->model('CustomerModel', $this);
        $this->oaWorkersModel = $this->loader->model('OaWorkersModel', $this);
        $this->oaWorkersBillModel = $this->loader->model('OaWorkersBillModel', $this);
        $this->oaAchievementWorkersModel = $this->loader->model('OaAchievementWorkersModel', $this);
        $this->couponModel = $this->loader->model('CouponModel', $this);
        $this->couponCateModel = $this->loader->model('CouponCateModel', $this);
        $this->achievementModel = $this->loader->model('AchievementModel', $this);
        $this->engineersModel = $this->loader->model('EngineersModel', $this);
        $this->engineerBillModel = $this->loader->model('EngineerBillModel', $this);
        $this->adminAccountModel = $this->loader->model('AdminAccountModel', $this);
        $this->adminAccountBillModel = $this->loader->model('AdminAccountBillModel', $this);
        $this->engineersBillModel = $this->loader->model('EngineersBillModel', $this);


    }

    //结算
    public function settlement($bill_id)
    {
        if (empty($bill_id)) {
            return returnResult(-1000, "结算失败!缺少参数bill_id");
        }
        $bill_info = $this->customerCodeBillModel->getOne(['bill_id' => $bill_id], '*');
        if ($bill_info['status'] == 1) {
            return returnResult(-1000, "结算失败!账单未支付");
        }
        if ($bill_info['is_settlement'] == 1) {
            return returnResult(-1000, "结算失败!账单已结算,请勿重复结算");
        }
        if ($bill_info['money'] == 0) {
            return returnResult(1000, "结算金额为0,无须进行结算");
        }
        $code_info = $this->customerCodeModel->getOne(['code' => $bill_info['customer_code'], 'is_delete' => 0], 'user_id,code,contract_id,actual_transformation_num');
        if (empty($code_info)) {
            return returnResult(-1000, "结算失败!户号信息不存在");
        }
        $bill_info['contract_id'] = $code_info['contract_id'];
        $bill_info['user_id'] = $code_info['user_id'];
        $bill_info['actual_transformation_num'] = $code_info['actual_transformation_num'];
        $user_info = $this->customerModel->getOne(['user_id' => $code_info['user_id'], 'is_delete' => 0], 'user_belong_to_company,source,user_id,is_dealer');
        if (empty($user_info)) {
            return returnResult(-1000, "推广结算关系获取失败!用户不存在");
        }
        $company_id = $user_info['user_belong_to_company'];
        $status = false;
        $this->db->begin(function () use (&$status, $bill_info, $bill_id, $company_id, $user_info) {
            //新增业绩
            $achievement_result = $this->addAchievement($bill_info);
            if ($achievement_result['code'] != 1000) {
                var_dump($achievement_result['msg']);
                return false;
            }
            $achievement_id = $achievement_result['data'];
            $this->monthSettlement($achievement_id, $user_info, $bill_info, $bill_info['money'], $company_id);
            //更新账单结算状态
            $this->customerCodeBillModel->save(['bill_id' => $bill_id], ['settle_time' => time(), 'is_settlement' => 1]);
            $this->upgradeTeam($bill_info['user_id']);
            $status = true;
        }, function ($e, $bill_id) {
            FileLogService::WriteLog(SLog::ERROR_LOG, 'settlement', '结算失败:' . $bill_id . '>>>错误原因:' . $e->error);
            return returnResult(ReturnCodeService::FAIL, "结算失败,请重试!" . $e->error);
        });
        if ($status) {
            return returnResult(1000, "结算成功");
        }
        return returnResult(-1000, "结算失败");
    }


    /**
     * 月账单结算,每月缴费进行结算
     * @param $achievement_id
     * @param $user_info
     * @param $bill_info
     * @param $money
     * @param $total_account_info
     * @param $company_id
     * 返回参数说明:
     *  type: 1:持续收益-直推 2:持续收益-间推 3:阶梯增长-省级市场推广 4:阶梯增长-市级市场推广 5:阶梯增长-区级市场推广 6:阶梯增长-省级市场推广直推收益 7:阶梯增长-省级市场推广间推收益
     *  8:讲解 9:承包商享受收益-省级承包商 10:承包商享受收益-市级承包商 11:承包商享受收益-区级承包商 12:承包商推荐人收益-省级承包商 13:承包商推荐人收益-市级承包商 14:承包商推荐人收益-区级承包商
     * 15:承包商维护补贴-省级承包商 16:承包商维护补贴-市级承包商 17:承包商维护补贴-区级承包商 18:工程新装收益 19:工程新装交通补贴 20:维护补贴 21:自我超越-第一次 22：自我超越-第二次 23：自我超越-第三次 24：自我超越-第四次 25：工程部长维护补贴 26:工程经理维护补贴
     * role:  1:市场推广 2:工程 3:区级区域承包商 4:总后台 5：市级区域承包商 6：省级区域承包商
     */
    public function monthSettlement($achievement_id, $user_info, $bill_info, $money, $company_id)
    {

        $result = [];
        $direct_push_result = $this->directPushSettlement($achievement_id, $user_info, $bill_info, $money, $company_id);//持续收益
        if ($direct_push_result['code'] == 1000) {
            $result = array_merge($result, $direct_push_result['data']);
        }

        $marketing_result = $this->marketingSettlement($user_info, $money);//阶梯增长
        if ($marketing_result['code'] == 1000) {
            $result = array_merge($result, $marketing_result['data']);
        } else {
            var_dump($marketing_result);
        }

        //记录升级之前的结算关系--便于后期统计多的部分
        $marketing_before_result = $this->marketingSettlement($user_info, $money, true);//阶梯增长
        if ($marketing_before_result['code'] == 1000) {
            if (!empty($marketing_before_result['data'])) {
                foreach ($marketing_before_result['data'] as $k => $v) {
                    //添加业绩记录
                    $this->addAchievementWorkersBefore($achievement_id, $v);
                }
            }

        }
        $explain_result = $this->explainSettlement($bill_info, $money);//讲解收益
        if ($explain_result['code'] == 1000) {
            $result = array_merge($result, $explain_result['data']);
        }

        $agent_result = $this->agentSettlement($bill_info);//区域承包商收益
        if ($agent_result['code'] == 1000) {
            $result = array_merge($result, $agent_result['data']);
        }
        $team_reward_result = $this->team_reward($bill_info);
        if ($team_reward_result['code'] == 1000) {
            $result = array_merge($result, $team_reward_result['data']);
        }

        $engineering_subsidy_result = $this->engineeringSubsidySettlement($bill_info);//维护补贴,次月开始
        if ($engineering_subsidy_result['code'] == 1000) {
            $result = array_merge($result, $engineering_subsidy_result['data']);
        }

        $agent_subsidy_result = $this->agentSubsidySettlement($bill_info);//区域承包商维护补贴,次月开始
        if ($agent_subsidy_result['code'] == 1000) {
            $result = array_merge($result, $agent_subsidy_result['data']);
        } else {
            var_dump($agent_subsidy_result);
        }

        $engineer_department_result = $this->engineerDepartmentMinisterSettlement($bill_info);//工程部部长维护补贴,次月开始
        if ($engineer_department_result['code'] == 1000) {
            $result = array_merge($result, $engineer_department_result['data']);
        } else {
            var_dump($engineer_department_result);
        }

        $engineer_department_manager_result = $this->engineerDepartmentManagerSettlement($bill_info);//工程部经理维护补贴,次月开始
        if ($engineer_department_manager_result['code'] == 1000) {
            $result = array_merge($result, $engineer_department_manager_result['data']);
        } else {
            var_dump($engineer_department_manager_result);
        }


        $engineering_install_result = $this->engineeringInstallSettlement($bill_info, $achievement_id);//新装师傅结算
        if ($engineering_install_result['code'] == 1000) {
            $result = array_merge($result, $engineering_install_result['data']);
        }
        $market_result = $this->marketSettlement($bill_info);//市场分成
        if ($market_result['code'] == 1000) {
            $result = array_merge($result, $market_result['data']);
        }
        if (empty($result)) {
            return returnResult(-1000, "未查询到任何结算信息");
        }
    
      
        //统计账户金额
        $role_group_result = array_group_by($result, 'role');//按角色分组
        $account_data = [];
        if (!empty($role_group_result)) {
            foreach ($role_group_result as $k => $v) {
                $uid_group_result = array_group_by($v, 'uid');//按用户分组
                foreach ($uid_group_result as $kk => $vv) {
                    $item = [];
                    $item['role'] = $vv[0]['role'];
                    $item['uid'] = $vv[0]['uid'];
                    $item['money'] = array_sum(array_column($vv, 'money'));
                    $account_data[] = $item;
                    unset($item);
                }
            }
        }

        if (!empty($account_data)) {
            foreach ($account_data as $k => $v) {
                //更新金额
                $this->updateAccount($v['uid'], $v['money'], $v['role'], 0, 1);
            }
            //更新资金记录
            foreach ($result as $k1 => $v1) {
                $this->updateAccount($v1['uid'], $v1['money'], $v1['role'], $v1['type'], 2);
            }
        }
        foreach ($result as $k => $v) {
            //添加业绩记录
            $this->addAchievementWorkers($achievement_id, $v);
        }
        return returnResult(1000, "结算成功");
    }


    /**
     * 阶梯增长
     * @param $achievement_id
     * @param $user_info
     * @param $money
     * @param $total_account_info
     * @param $company_id
     * @return array
     */
    public function marketingSettlement($user_info, $money, $before = false)
    {

        $rate['marketing_one'] = self::AREA_WORKER_STEP_RATE;//区县级
        $rate['marketing_two'] = self::CITY_WORKER_STEP_RATE;//市级
        $rate['marketing_three'] = self::PROVINCE_WORKER_STEP_RATE;//省级
        $rate['marketing_four'] = self::PROVINCE_WORKER_PUSH_RATE;//直推省级推广
        $return = [];
        $top_id = 0;
        $result = [];
        $group = [];
        $result_relation = $this->stepAllocation($user_info['source'], 1, $result, 1, $group, $before);
        if (!$result_relation) {
            returnResult(-1000, "关系存在闭环");
        }
        foreach ($result as $k => $v) {
            if ($v['position'] == 1) {
                continue;
            }
            $item = [];
            $top_id = $v['position'] == 4 ? $v['user_id'] : 0;
            $item['uid'] = $v['workers_id'];
            $cur_rate = 0;
            $type = 5;//区级市场推广
            if ($v['position'] == 2) {
                $cur_rate = $rate['marketing_one'];
            } else if ($v['position'] == 3) {
                $cur_rate = $rate['marketing_two'];
                if ($v['index'] == 1) {
                    $cur_rate += $rate['marketing_one'];
                }
                $type = 4;//市级市场推广
            } else if ($v['position'] == 4) {
                $cur_rate = $rate['marketing_three'];
                if ($v['index'] == 1) {
                    $cur_rate += $rate['marketing_one'] + $rate['marketing_two'];
                } else if ($v['index'] == 2) {
                    $cur_rate += $rate['marketing_one'];
                }
                $type = 3;//省级市场推广
            }
            $a = ($type == 3 ? '省' : '市');
            $item['rate'] = $cur_rate;
            $item['money'] = $cur_rate * $money;
            $item['workers_position'] = $v['position'];
            $item['workers_before_position'] = $v['before_position'];
            $item['type'] = $type;
            $item['remarks'] = $a . '级市场推广';
            $item['role'] = 1;
            array_push($return, $item);
            unset($item);

        }
        //若存在省级市场推广,向上找两个省级推广(直推)分1%
        if (!empty($top_id)) {
            $user_info = $this->customerModel->getOne(['user_id' => $top_id], 'source');
            if (empty($user_info) || empty($user_info['source'])) {
                return returnResult(-1000, "不存在1-1%");
            }
            $level_result = $this->upWorkers($user_info['source'], 4, $before);
            if ($level_result['code'] == 1000 && $level_result['data']['position'] == 4) {
                $top_id = $level_result['data']['user_id'];
                $item['uid'] = $level_result['data']['workers_id'];
                $item['rate'] = $rate['marketing_four'];
                $item['money'] = $rate['marketing_four'] * $money;
                $item['workers_position'] = $level_result['data']['position'];
                $item['workers_before_position'] = $level_result['data']['before_position'];
                $item['type'] = 6;//省级市场推广直推收益
                $item['role'] = 1;
                $item['remarks'] = '省级市场推广直推收益';
                array_push($return, $item);
                unset($level_result);
                unset($item);
                $user_info = $this->customerModel->getOne(['user_id' => $top_id], 'source');
                if (empty($user_info) || empty($user_info['source'])) {
                    return returnResult(-1000, "不存在1-1%");
                }
                $level_result = $this->upWorkers($user_info['source'], 4, $before);
                if ($level_result['code'] == 1000 && $level_result['data']['position'] == 4) {
                    $item['uid'] = $level_result['data']['workers_id'];
                    $item['rate'] = $rate['marketing_four'];
                    $item['money'] = $rate['marketing_four'] * $money;
                    $item['workers_position'] = $level_result['data']['position'];
                    $item['workers_before_position'] = $level_result['data']['before_position'];
                    $item['type'] = 7;//省级市场推广间推收益
                    $item['role'] = 1;
                    $item['remarks'] = '省级市场推广间推';
                    array_push($return, $item);
                    unset($level_result);
                    unset($item);
                }

            }
        }
        return returnResult(1000, "结算成功", $return);
    }


    /**
     * 持续收益20%
     * 直推10%+10%
     * 1.若有直推，直推人是市场推广，直接获得Y=X*20%；若直推人是普通用户
     * 2.若有直推,直推人是普通用户,用户获得10%的券,向上找第一个市场推广获得10%
     * @param $achievement_id
     * @param $user_info
     * @param $money
     * @param $total_account_info
     * @param $company_id
     * @return array
     */
    public function directPushSettlement($achievement_id, $user_info, $bill_info, $money, $company_id)
    {
        $rate['direct_push_worker'] = self::DIRECT_PUSH_WORKER_RATE;//若直推的是市场推广,获得20%的现金
        $rate['direct_push_user'] = self::DIRECT_PUSH_USER_RATE;//若直推的是普通用户,获得10%的优惠券,向上找市场推广获得剩下的10%
        $item = [];
        $explain_rate = 0;
        $explain_result = $this->explainSettlement($bill_info, $money);
        if ($explain_result['code'] == 1000) {
            $explain_rate = self::EXPLAIN_RATE;
        }
        $result = [];
        if (!empty($user_info['source'])) {
            //直推人是否是市场推广
            $pid_info = $this->customerModel->getOne(['user_id' => $user_info['source'], 'is_delete' => 0], 'user_id,is_dealer');
            if (!empty($pid_info)) {
                if ($pid_info['is_dealer'] == 1) {
                    $workers_info = $this->oaWorkersModel->getOne(['user_id' => $pid_info['user_id']], 'workers_id,position');
                    if (!empty($workers_info)) {
                        $item['uid'] = $workers_info['workers_id'];
                        $item['rate'] = $rate['direct_push_worker'] - $explain_rate;
                        $item['money'] = $item['rate'] * $money;
                        $item['workers_position'] = $workers_info['position'];
                        $item['type'] = 1;//直推
                        $item['role'] = 1;
                        $item['remarks'] = '市场推广：直推';
                        array_push($result, $item);
                    }
                } else {
                    //用户获得10%的券start
                   $coupon_cate_id = $this->couponCateId($company_id);
                   $add_data['uid'] = $pid_info['user_id'];
                   $add_data['cid'] = $coupon_cate_id;
                   $add_data['code'] = generate_promotion_code();
                   $add_data['type'] = 5;
                   $add_data['c_money'] = $rate['direct_push_user'] * $money;
                   $add_data['send_time'] = time();
                   $add_data['valid_time_start'] = time();
                   $add_data['valid_time_end'] = time() + 86400 * 180;
                   $add_data['from_achievement_id'] = $achievement_id;
                   $this->couponModel->add($add_data);
                    //向上找市场推广获得10%end

                    //直推不是市场推广分积分。 2021年11月1号 许乐霓----2022.4.25修改回返券
                    // $item['uid'] = $pid_info['user_id'];
                    // $item['rate'] = $rate['direct_push_user'] - $explain_rate;
                    // $item['money'] = $item['rate'] * $money;
                    // $item['workers_position'] = 0;
                    // $item['type'] = 1;//直推
                    // $item['role'] = 1;
                    // $item['remarks'] = '市场推广：直推';
                    // array_push($result, $item);
                    $up_result = $this->upWorkers($pid_info['user_id'], 0);
                    $item['rate'] = $rate['direct_push_worker'] - $rate['direct_push_user'];
                    if ($up_result['code'] == 1000) {
                        $item['uid'] = $up_result['data']['workers_id'];
                        $item['rate'] = $rate['direct_push_worker'] - $rate['direct_push_user'] - $explain_rate;
                        $item['money'] = $item['rate'] * $money;
                        $item['workers_position'] = $up_result['data']['position'];
                        $item['type'] = 2;//间推
                        $item['role'] = 1;
                        $item['remarks'] = '市场推广：间推';
                        array_push($result, $item);
                    }
                }
            }
        }
        /******持续收益20%结束******/
        if (empty($item)) {
            return returnResult(-1003, "暂无数据");
        }
        return returnResult(1000, "结算成功", $result);
    }

    /**
     * 讲解结算===从持续收益中扣去取（直推市场推广的账户扣）
     */
    public function explainSettlement($bill_info, $money)
    {
        $return = [];
        $this->achievementModel->table = 'contract';
        $contract_info = $this->achievementModel->findData(['contract_id' => $bill_info['contract_id']], 'contract_id,explainer');
        if (empty($contract_info) || empty($contract_info['explainer'])) {
            return returnResult(-1000, "不存在讲解");
        }
        $explainer_array = explode(',', $contract_info['explainer']);
        if (empty($explainer_array)) {
            return returnResult(-1000, '不存在讲解');
        }
        //讲解比例、如果出现连个对半分 2021年11月1日修改  许乐霓
        $rate['explain'] = self::EXPLAIN_RATE;
        $explain = round(self::EXPLAIN_RATE/count($explainer_array),2);
        $explain_money =  round(($rate['explain'] * $money)/count($explainer_array),2);
        foreach ($explainer_array as $key => $value) {
            $explainer = explode('-', $value);
            $role = $explainer[0];
            $uid = $explainer[1];
            if ($role == 'D') {
                //市场推广讲解
                $workers_info = $this->oaWorkersModel->getOne(['user_id' => $uid], 'workers_id,position');
                $item['uid'] = $workers_info['workers_id'];
                $item['rate'] = $explain;
                $item['money'] = $explain_money;
                $item['workers_position'] = $workers_info['position'];
                $item['type'] = 8;//讲解收益
                $item['role'] = 1;
                $item['remarks'] = '市场讲解';
            } else {
                //工程讲解
                $item['uid'] = $uid;
                $item['rate'] = $explain;
                $item['money'] = $explain_money;
                $item['workers_position'] = 1;
                $item['role'] = 2;
                $item['type'] = 8;//讲解收益
                $item['remarks'] = '工程讲解';
            }
            array_push($return, $item);
        }
        if (empty($return)) {
            return returnResult(-1003, "暂无数据");
        }
        return returnResult(1000, "结算成功", $return);
    }

    //查询新装情况
    public function installData($contract_id)
    {

        $this->achievementModel->table = 'work_order';
        $join = [
            ['work_order_business wob', 'wob.work_order_id=rq_work_order.work_order_id', 'left'],
            ['contract c', 'c.contract_id=rq_work_order.contract_id', 'left'],
        ];
        //查询最新安装的眼数
        $where['c.contract_id'] = $contract_id;
        $where['wob.business_id'] = ['IN', [3, 9]];
        $where['work_order_status'] = ['IN', [8, 9, 10]];
        $where['is_settlement'] = 0;
        $where['c.status'] = ['IN', [11, 12]];
        $work_order_info = $this->achievementModel->selectData($where, 'rq_work_order.work_order_id,actual_stove_number', $join);
        $new_install_cooker_num = 0;
        if (!empty($work_order_info)) {
            $new_install_cooker_num = array_sum(array_column($work_order_info, 'actual_stove_number'));//炉具眼数
        }
        unset($where);
        unset($work_order_info);

        //查询维护的眼数
        //查询炉具眼数
        $this->achievementModel->table = 'energy_saving_data_test';
        $saving_data = $this->achievementModel->selectData(['contract_id' => $contract_id, 'status' => 1, 'type' => 3], 'cooker_num', [], ['type' => 'DESC']);
        if (empty($saving_data)) {
            return returnResult(-1000, "不存在采集信息");
        }
        $total_cooker_num = array_sum(array_column($saving_data, 'cooker_num'));//累计安装眼数
        $maintain_cooker_num = $total_cooker_num - $new_install_cooker_num;//维护眼数
        //查询工程
        $where['c.contract_id'] = $contract_id;
        $where['wob.business_id'] = ['IN', [3, 9]];
        $where['work_order_status'] = ['IN', [8, 9, 10]];
        $where['c.status'] = ['IN', [11, 12]];
        $this->achievementModel->table = 'work_order';
        $work_order_info = $this->achievementModel->selectData($where, 'rq_work_order.work_order_id,actual_stove_number', $join);
        $engineer_num = 0;
        $engineers = [];
        $work_order_id = 0;
        if (!empty($work_order_info)) {
            $work_order_id = array_column($work_order_info, 'work_order_id');
            $this->achievementModel->table = 'work_engineering_relationship';
            $engineer_list = $this->achievementModel->selectData(['work_order_id' => ['IN', $work_order_id], 'state' => 1], 'repair_id');
            if (empty($engineer_list)) {
                return returnResult(-1000, "不存在工程");
            }
            $engineers = array_unique(array_column($engineer_list, 'repair_id'));
            $engineer_num = count($engineers);

        }


        $data = ['new_install_cooker_num' => $new_install_cooker_num, 'total_cooker_num' => $total_cooker_num, 'maintain_cooker_num' => $maintain_cooker_num, 'engineer_num' => $engineer_num, 'engineers' => $engineers, 'work_order_id' => $work_order_id];
        return returnResult(1000, 'ok', $data);
    }

    /**
     * 工程新装结算
     * @param $bill_info
     * @return array|bool
     */
    public function engineeringInstallSettlement($bill_info, $achievement_id)
    {
        $return = [];
        $config['install_money'] = formatMoney(self::ENGINEER_INSTALL_MONEY_EYE, 1);//安装 200/眼
        $config['trafficl_money'] = formatMoney(self::ENGINEER_TRAFFICL_MONEY_PERSON, 1);//交通补贴50元/人次
        $install_data_result = $this->installData($bill_info['contract_id']);

        if ($install_data_result['code'] != 1000) {
            return returnResult($install_data_result['code'], $install_data_result['msg']);
        }
        $install_data = $install_data_result['data'];
        if ($install_data['new_install_cooker_num'] <= 0) {
            return returnResult(-1003, "暂无需要结算的新装");
        }
        $install_money = $install_data['new_install_cooker_num'] * $config['install_money'];//获得新装费用
        $trafficl_money = $config['trafficl_money'] * $install_data['engineer_num'];//获得交通补贴
        $manager = $this->getProjectManager($bill_info['contract_id']);

        $item = [];
        $item['uid'] = $manager['engineers_id'];
        $item['rate'] = 0;
        $item['money'] = $install_money;
        $item['workers_position'] = 1;
        $item['role'] = 2;
        $item['type'] = 18;//工程安装收益
        $item['remarks'] = '工程新装收益';
        array_push($return, $item);
        unset($item);
        if (!empty($trafficl_money)) {
            $item = [];
            $item['uid'] = $manager['engineers_id'];
            $item['rate'] = 0;
            $item['money'] = $trafficl_money;
            $item['workers_position'] = 1;
            $item['role'] = 2;
            $item['type'] = 19;//工程交通补贴
            $item['remarks'] = '工程交通补贴';
            array_push($return, $item);
        }

        //更新工单结算状态
        if (!empty($install_data['work_order_id'])) {
            $this->achievementModel->table = 'work_order';
            $this->achievementModel->updateData(['work_order_id' => ['IN', $install_data['work_order_id']]], ['is_settlement' => 1, 'achievement_id' => $achievement_id]);
        }
        if (empty($return)) {
            return returnResult(-1003, "暂无数据");
        }
        return returnResult(1000, "结算成功", $return);
    }

    /**
     * 维护补贴
     * @param $bill_info
     * @return array|bool
     */
    public function engineeringSubsidySettlement($bill_info)
    {
        $return = [];
        $config['money'] = formatMoney(self::ENGINEER_MAINTAIN_MONEY_EYE_MONTH, 1);//维护补贴 5元/眼/月
        //获取维护眼数
        $install_data_result = $this->installData($bill_info['contract_id']);
        if ($install_data_result['code'] != 1000) {
            return returnResult($install_data_result['code'], $install_data_result['msg']);
        }
        $install_data = $install_data_result['data'];
        if ($install_data['maintain_cooker_num'] <= 0) {
            return returnResult(-1003, "暂无需要结算的维护补贴");
        }
        $money = $install_data['maintain_cooker_num'] * $config['money'];
        $manager = $this->getProjectManager($bill_info['contract_id']);
        $item = [];
        $item['uid'] = $manager['engineers_id'];
        $item['rate'] = 0;
        $item['money'] = $money;
        $item['workers_position'] = 1;
        $item['role'] = 2;
        $item['type'] = 20;//维护补贴
        $item['remarks'] = '维护补贴';
        array_push($return, $item);

        if (empty($return)) {
            return returnResult(-1003, "暂无数据");
        }
        return returnResult(1000, "结算成功", $return);
    }

    //获取区域总管
    public function getProjectManager($contract_id)
    {
        $this->achievementModel->table = 'contract';
        $contract_info = $this->achievementModel->findData(['contract_id' => $contract_id], 'company_id,contract_id,administrative_id,operation_id');
        if (empty($contract_info)) {
            return false;
        }
        //判断该区域是否存在区域总管,不存在划分到公司总部的区域总管
        $engineers_info = $this->engineersModel->getOne(['administrative_id' => $contract_info['administrative_id'], 'operation_id' => $contract_info['operation_id'], 'role' => 4], 'engineers_id');
        if (!empty($engineers_info)) {
            return $engineers_info;
        }
        //如果不存在创建总管账号
        $add_data['role'] = 4;
        $add_data['engineers_name'] = '区域总管';
        $add_data['engineers_number'] = $this->createEngineerNumber();
        $password = setPassword(trim(123456));
        $add_data['engineers_password'] = $password['password'];
        $add_data['engineers_slat'] = $password['strict'];
        $add_data['administrative_id'] = $contract_info['administrative_id'];
        $add_data['operation_id'] = $contract_info['operation_id'];
        $add_data['reg_time'] = time();
        $add_data['total_money'] = 0;
        $add_data['money'] = 0;
        $add_data['temp_money'] = 0;
        $engineers_id = $this->engineersModel->add($add_data);
        $add_data['engineers_id'] = $engineers_id;
        $add_company_data['engineers_id'] = $engineers_id;
        $add_company_data['company_id'] = $contract_info['company_id'];
        $add_company_data['create_time'] = time();
        $this->achievementModel->table = 'engineers_to_company';
        $this->achievementModel->insertData($add_company_data);
        return $add_data;
    }

    function createEngineerNumber()
    {
        do {
            $str = mt_rand(10000000, 99999999);
            $vo = $this->engineersModel->getOne(['engineers_number' => $str], 'engineers_id');
            if (empty($vo)) {
                break;
            }
        } while (true);
        return $str;
    }


    /**
     * 区域承包商维护补贴
     * @param $bill_info
     * @return array
     */
    public function agentSubsidySettlement($bill_info)
    {
        $config['one_money'] = formatMoney(self::PROVINCE_REGION_MAINTAIN_MONEY_EYE_YEAR, 1);//单位分 省级维护补贴 50元/眼/年 换算成X分/眼/月
        $config['two_money'] = formatMoney(self::CITY_REGION_MAINTAIN_MONEY_EYE_YEAR, 1);//单位分 市级维护补贴 45元/眼/年 换算成X分/眼/月
        $config['three_money'] = formatMoney(self::AREA_REGION_MAINTAIN_MONEY_EYE_YEAR, 1);//单位分 区级级维护补贴 40元/眼/年  换算成X分/眼/月
        $team = $this->agentTeam($bill_info);
        $money = 0;
        if (!empty($team['three'])) {
            $team['three']['money'] = ceil($config['three_money']/12);
            $money = $config['three_money'];
        }
        if (!empty($team['two'])) {
            $team['two']['money'] = ceil(($config['two_money'] - $money)/12);
            $money = $config['two_money'];
        }
        if (!empty($team['one'])) {
            $team['one']['money'] = ceil(($config['one_money'] - $money)/12);
        }
        $return = [];
        //获取维护眼数
        $install_data_result = $this->installData($bill_info['contract_id']);
        if ($install_data_result['code'] != 1000) {
            return returnResult($install_data_result['code'], $install_data_result['msg']);
        }
        $install_data = $install_data_result['data'];
        if ($install_data['maintain_cooker_num'] <= 0) {
            return returnResult(-1003, "暂无需要结算的维护补贴");
        }
        $cooker_num = $install_data['maintain_cooker_num'];//炉具眼数
        foreach ($team as $k => $v) {
            if (!empty($v)) {
                $item = [];
                $item['uid'] = $v['id'];
                $item['rate'] = 0;
                $item['money'] = $v['money'] * $cooker_num;
                $item['workers_position'] = 2;
                $item['role'] = 3;
                $item['type'] = 17;//区级承包商维护补贴
                $item['remarks'] = '维护补贴收益';
                if ($k == 'one' || $k == 'two') {
                    $item['workers_position'] = ($k == 'one' ? 4 : 3);
                    $item['type'] = ($k == 'one' ? 15 : 16);//15:省级承包商维护补贴 16:市级承包商维护补贴
                    $item['role'] = ($k == 'one' ? 6 : 5);//6:省级，5市级
                }
                array_push($return, $item);
                unset($item);
            }

        }
        if (empty($return)) {
            return returnResult(-1003, "暂无数据");
        }
        return returnResult(1000, "结算成功", $return);
    }

    /**
     * 工程部部长维护补贴==5元/眼/年
     * @param $bill_info
     * @return array
     */
    public function engineerDepartmentMinisterSettlement($bill_info)
    {
        $config['money'] = ceil(formatMoney(self::ENGINEER_DEPARTMENTMINISTER_MONEY_EYE_YEAR, 1) / 12);//单位分
        $return = [];
        //获取维护眼数
        $install_data_result = $this->installData($bill_info['contract_id']);
        if ($install_data_result['code'] != 1000) {
            return returnResult($install_data_result['code'], $install_data_result['msg']);
        }
        $install_data = $install_data_result['data'];
        if ($install_data['maintain_cooker_num'] <= 0) {
            return returnResult(-1003, "暂无需要结算的维护补贴");
        }
        $cooker_num = $install_data['maintain_cooker_num'];//炉具眼数
        $workers_info = $this->getConfigAccount(5, '工程部部长');
        $item = [];
        $item['uid'] = $workers_info['workers_id'];
        $item['rate'] = 0;
        $item['money'] = $config['money'] * $cooker_num;
        $item['workers_position'] = 1;
        $item['role'] = 1;
        $item['type'] = 25;
        $item['remarks'] = '维护补贴收益';
        array_push($return, $item);
        if (empty($return)) {
            return returnResult(-1003, "暂无数据");
        }
        return returnResult(1000, "结算成功", $return);
    }

    /**
     * 工程部经理维护补贴==5元/眼/年
     * @param $bill_info
     * @return array
     */
    public function engineerDepartmentManagerSettlement($bill_info)
    {
        $config['money'] = ceil(formatMoney(self::ENGINEER_DEPARTMENTMASTER_MONEY_EYE_YEAR, 1) / 12);//单位分
        $return = [];
        //获取维护眼数
        $install_data_result = $this->installData($bill_info['contract_id']);
        if ($install_data_result['code'] != 1000) {
            return returnResult($install_data_result['code'], $install_data_result['msg']);
        }
        $install_data = $install_data_result['data'];
        if ($install_data['maintain_cooker_num'] <= 0) {
            return returnResult(-1003, "暂无需要结算的维护补贴");
        }
        $cooker_num = $install_data['maintain_cooker_num'];//炉具眼数
        $workers_info = $this->getConfigAccount(6, '工程部经理');
        $item = [];
        $item['uid'] = $workers_info['workers_id'];//工程部经理ID,oa_workers表的workers_id
        $item['rate'] = 0;
        $item['money'] = $config['money'] * $cooker_num;
        $item['workers_position'] = 1;
        $item['role'] = 1;
        $item['type'] = 26;
        $item['remarks'] = '维护补贴收益';
        array_push($return, $item);
        if (empty($return)) {
            return returnResult(-1003, "暂无数据");
        }
        return returnResult(1000, "结算成功", $return);
    }

    //获取工程部部长和工程部经理ID
    public function getConfigAccount($position, $workers_name)
    {
        $workers_info = $this->oaWorkersModel->getOne(['position' => $position], 'workers_id');
        if (!empty($workers_info)) {
            return $workers_info;
        }

        //若不存在创建账号--用户和员工
        $account = CommonService::createSn('oven_user_no', '', 1);
        $user_data['account'] = $account;
        $user_data['realname'] = $workers_name;
        $user_data['add_time'] = time();
        $user_data['user_role'] = 4;
        $user_id = $this->customerModel->add($user_data);
        $workers_data['user_id'] = $user_id;
        $workers_data['position'] = $position;
        $workers_data['workers_name'] = $workers_name;
        $workers_data['workers_number'] = $account;
        $workers_data['temp_money'] = 0;
        $workers_data['total_money'] = 0;
        $workers_data['cash_available'] = 0;
        $workers_data['add_time'] = time();
        $workers_id = $this->oaWorkersModel->add($workers_data);
        $workers_data['workers_id'] = $workers_id;
        return $workers_data;

    }

    /**
     * 区域承包商收益：享受收益，推荐人收益
     * @param $bill_info
     * @param int $money
     * @return array
     */
    public function agentSettlement($bill_info)
    {
        $install_data_result = $this->installData($bill_info['contract_id']);
        if ($install_data_result['code'] != 1000) {
            return returnResult($install_data_result['code'], $install_data_result['msg']);
        }
        $install_data = $install_data_result['data'];
        $new_install_cooker_num = $install_data['new_install_cooker_num'];
        $maintain_cooker_num = $install_data['maintain_cooker_num'];
        //区域承包商享受比例是除开公司拨付后
        $money = $bill_info['money'] * (1 - self::COMPANY_PAY_WORKER_RATE) - $maintain_cooker_num * ceil(formatMoney(self::COMPANY_PAY_MAINTAIN_MONEY_EYE_YEAR, 1) / 12) - $new_install_cooker_num * formatMoney(self::COMPANY_PAY_INSTALL_MONEY_EYE, 1);
        //省级享受收益5%,市级5%,区县级20%
        $rate['one'] = self::PROVINCE_REGION_RATE;
        $rate['two'] = self::CITY_REGION_RATE;
        $rate['three'] = self::AREA_REGION_RATE;
        //推荐人收益
        $push_rate['one'] = self::PROVINCE_REGION_PUSH_RATE;
        $push_rate['two'] = self::CITY_REGION_PUSH_RATE;
        $push_rate['three'] = self::AREA_REGION_PUSH_RATE;
        $team = $this->agentTeam($bill_info);
        //计算各等级收益比例
        $total_rate = 0;
        $total_push_rate = 0;
        if (!empty($team['three'])) {
            $team['three']['rate'] = $rate['three'];
            $team['three']['source_rate'] = empty($team['three']['source']) ? 0 : $push_rate['three'];
            $total_rate = $rate['three'];
            $total_push_rate = $team['three']['source_rate'];
        }
        if (!empty($team['two'])) {
            $team['two']['rate'] = $rate['two'] - $total_rate;
            $team['two']['source_rate'] = empty($team['two']['source']) ? 0 : ($push_rate['two'] - $total_push_rate);
            $total_rate = $rate['two'];
            $total_push_rate = $team['two']['source_rate'];
        }
        if (!empty($team['one'])) {
            $team['one']['rate'] = $rate['one'] - $total_rate;
            $team['one']['source_rate'] = empty($team['one']['source']) ? 0 : ($push_rate['one'] - $total_push_rate);
        }
        var_dump("rate");
        var_dump($team);
        $return = [];
        foreach ($team as $k => $v) {
            if (!empty($v)) {
                $item = [];
                $item['uid'] = $v['id'];
                $item['rate'] = $v['rate'];
                $item['money'] = $money * $v['rate'];
                $item['workers_position'] = 2;
                $item['role'] = 3;
                $item['type'] = 11;//区级承包商享受收益
                $item['remarks'] = '承包商享受';
                if ($k == 'one' || $k == 'two') {
                    $item['workers_position'] = ($k == 'one' ? 4 : 3);
                    $item['type'] = ($k == 'one' ? 9 : 10);//9:省级承包商享受收益 10:市级承包商享受收益
                    $item['role'] = ($k == 'one' ? 6 : 5);//6:省级，5市级
                }
                array_push($return, $item);
                unset($item);
                //推荐人收益
                if (!empty($v['source'])) {
                    $source = explode('-', $v['source']);
                    $source_role = $source[0];
                    $source_id = $source[1];
                    $item = [];
                    $item['uid'] = $source_id;
                    $item['rate'] = $v['source_rate'];
                    $item['money'] = $money * $v['source_rate'];
                    if ($source_role == 'O' || $source_role == 'S' || $source_role == 'A') {
                        $item['workers_position'] = 4;
                        if ($source_role != 'O') {
                            $item['workers_position'] = $source_role == 'S' ? 3 : 2;
                        }
                        $item['role'] = 3;
                    } else if ($source_role == 'E') {
                        $item['role'] = 2;
                        $item['workers_position'] = 1;
                    } else if ($source_role == 'W') {
                        $item['role'] = 1;
                        $info = $this->oaWorkersModel->getOne(['workers_id' => $source_id], 'position');
                        $item['workers_position'] = $info['position'];
                    }

                    $item['type'] = 14;//区级承包商推荐人收益
                    if ($k == 'one' || $k == 'two') {
                        $item['type'] = ($k == 'one' ? 12 : 13);//12:省级承包商推荐人收益 13:市级承包商推荐人收益】
                    }
                    $item['remarks'] = '承包商推荐人';
                    array_push($return, $item);
                    unset($item);
                }
            }

        }
        if (empty($return)) {
            return returnResult(-1003, "暂无数据");
        }
        return returnResult(1000, "结算成功", $return);
    }

    /**
     * 区域承包团队
     */
    public function agentTeam($bill_info)
    {
        $this->achievementModel->table = 'contract';
        $contract_info = $this->achievementModel->findData(['contract_id' => $bill_info['contract_id']], 'operation_id,administrative_id');
        if (empty($contract_info)) {
            return false;
        }
        $level_three = [];//区县级承包商
        $level_two = [];//市级承包商
        $level_one = [];//省级承包商
        if (!empty($contract_info['administrative_id'])) {
            $this->achievementModel->table = 'administrative_info';
            $data = $this->achievementModel->findData(['a_id' => $contract_info['administrative_id']], 'a_id,source');
            if (!empty($data)) {
                $item['id'] = $data['a_id'];
                $item['source'] = $data['source'];
                $level_three = $item;
                unset($item);
            }
            unset($data);
        }
        if (!empty($contract_info['operation_id'])) {
            $this->achievementModel->table = 'operation_info';
            $data = $this->achievementModel->findData(['o_id' => $contract_info['operation_id']], 'o_id,pid,source');
            if (!empty($data)) {
                $item['id'] = $data['o_id'];
                $item['source'] = $data['source'];
                if ($data['pid'] > 0) {
                    $level_two = $item;
                    $pid = $data['pid'];
                    unset($data);
                    unset($item);
                    //查询一级
                    $data = $this->achievementModel->findData(['o_id' => $pid], 'o_id,pid,source');
                    if (!empty($data)) {
                        $item['id'] = $data['o_id'];
                        $item['source'] = $data['source'];
                        $level_one = $item;
                    }
                } else {
                    $level_one = $item;
                }
            }
        }
        return ['one' => $level_one, 'two' => $level_two, 'three' => $level_three];

    }


    //自我超越-团队奖励
    public function team_reward($bill_info)
    {
        $config = [
            ['num' => 30, 'money' => 1000, 'type' => 21],
            ['num' => 100, 'money' => 3000, 'type' => 22],
            ['num' => 300, 'money' => 10000, 'type' => 23],
            ['num' => 500, 'money' => 15000, 'type' => 24],
        ];
        $user_id = $bill_info['user_id'];
        $parent = [];
        $return = [];
        $parent_team = $this->searchParent($user_id, $parent);
        if (!empty($parent_team)) {
            //计算每个人是否达到奖励标准
            foreach ($parent_team as $k => $v) {
                $son = [];
                $son_team = $this->searchSon($v, $son);
                //统计团队炉具安装数量
                if (!empty($son_team)) {
                    $workers_info = $this->oaWorkersModel->getOne(['user_id' => $v], 'workers_id,position');
                    if (empty($workers_info)) {
                        continue;
                    }
                    $code_data = $this->customerCodeModel->getAll(['user_id' => ['IN', $son_team], 'state' => 2], "SUM(actual_transformation_num) as 'num'");
                    $num = $code_data[0]['num'];//团队安装数量
                    foreach ($config as $kk => $vv) {
                        if ($vv['num'] == $num) {
                            //每个人每种奖励只能享受一次
                            $info = $this->oaAchievementWorkersModel->getOne(['role' => 1, 'workers_id' => $workers_info['workers_id'], 'source' => $vv['type']], 'id');
                            if (!empty($info)) {
                                continue;
                            }
                            $item = [];
                            $item['uid'] = $workers_info['workers_id'];
                            $item['rate'] = 0;
                            $item['money'] = $vv['money'];
                            $item['workers_position'] = $workers_info['position'];
                            $item['role'] = 1;
                            $item['type'] = $vv['type'];
                            $item['remarks'] = '团队安装数量达到' . $num . '眼';
                            array_push($return, $item);
                            unset($item);
                        }
                    }
                }

            }
        }
        if (empty($return)) {
            return returnResult(-1000, "暂无数据");
        }
        return returnResult(1000, "OK", $return);
    }

    //市场总监、市场助理分成
    public function marketSettlement($bill_info)
    {
        $user_id = $bill_info['user_id'];
        $pid = $user_id;
        $return = [];
        //市场助理
        $level_result = $this->upWorkers($user_id, 7);
        if ($level_result['code'] == 1000) {
            $pid = $level_result['data']['user_id'];
            $item = [];
            $item['uid'] = $level_result['data']['workers_id'];
            $item['rate'] = 0;
            $item['money'] = self::MARKET_ASSISANT_RATE * $bill_info['money'];
            $item['workers_position'] = 7;
            $item['role'] = 1;
            $item['type'] = 27;
            $item['remarks'] = '市场助理收益';
            array_push($return, $item);
        }
        unset($level_result);
        //市场总监
        $level_result = $this->upWorkers($pid, 8);
        if ($level_result['code'] == 1000) {
            $item = [];
            $item['uid'] = $level_result['data']['workers_id'];
            $item['rate'] = 0;
            $item['money'] = self::MARKET_CHIEF_RATE * $bill_info['money'];
            $item['workers_position'] = 8;
            $item['role'] = 1;
            $item['type'] = 28;
            $item['remarks'] = '市场总监收益';
            array_push($return, $item);
        }
        if (empty($return)) {
            return returnResult(-1003, "暂无数据");
        }
        return returnResult(1000, "结算成功", $return);

    }

    //找儿子
    public function searchSon($user_id, &$son)
    {
        $user_info = $this->customerModel->getAll(['source' => $user_id, 'is_delete' => 0], 'user_id');
        if (empty($user_info)) {
            return $son;
        }
        $user_ids = array_column($user_info, 'user_id');
        $son = array_merge($son, $user_ids);
        foreach ($user_info as $k => $v) {
            return $this->searchSon($v['user_id'], $son);
        }


    }

    //找父级，直到找到推荐人为0时
    public function searchParent($user_id, &$parent)
    {
        $user_info = $this->customerModel->getOne(['user_id' => $user_id, 'is_delete' => 0], 'source,user_id,is_dealer');
        if (empty($user_info)) {
            return returnResult(-1000, $user_id . "用户信息错误或者不存在上级");
        }
        if ($user_info['source'] > 0) {
            array_push($parent, $user_info['source']);
            return $this->searchParent($user_info['source'], $parent);
        }
        return $parent;
    }

    /**
     * 向上找对应等级的市场推广
     * @param $user_id
     * @param $postion 0:市场推广,3：普通市场推广 4:区级市场推广 5：市级市场推广 6：省级市场推广
     */
    public function upWorkers($user_id, $postion = 0, $before = false)
    {
        $workers_info = $this->oaWorkersModel->getOne(['user_id' => $user_id], 'user_id,workers_id,position,position_change_mode');
        if (!empty($workers_info)) {
            $position = $workers_info['position'];
            $workers_info['before_position'] = 0;
            if ($before) {
                //若是手动升级的获取升级前的等级
                if ($workers_info['position_change_mode'] == 2) {
                    //获取升级前的等级
                    $this->achievementModel->table = 'oa_workers_upgrade_log';
                    $log = $this->achievementModel->selectPageData(['workers_id' => $workers_info['workers_id'], 'change_mode' => 2, 'after_upgrade' => $workers_info['position']], 'before_upgrade_grade', [], 1, 1, ['create_time' => 'DESC']);
                    $workers_info['before_position'] = empty($log) ? 1 : $log[0]['before_upgrade_grade'];
                    $position = $workers_info['before_position'];
                }
            }
            if ($postion == 0 || $postion == $position) {
                return returnResult(1000, "获取成功", $workers_info);
            }

        }


        $user_info = $this->customerModel->getOne(['user_id' => $user_id, 'is_delete' => 0], 'source,user_id,is_dealer');
        if (empty($user_info) || $user_info['source'] == 0) {
            return returnResult(-1000, $user_id . "用户信息错误或者不存在上级");
        }
        return $this->upWorkers($user_info['source'], $postion, $before);
    }


    /**
     * 获取结算金额分配配置，账号配置(若产品管理时进行了配置则取该配置，反之取默认值（配置文件）)
     * @param $equipments_config 产品配置信息,包含信息:grade_money ,ticket_money,coupon_valid_time(均为配置ID,产品管理时设置)
     * @param $company_id 公司ID
     */
    public function balanceConf($company_id)
    {
        $config = [];
        //账号
        $account = ConfigService::getBalanceConfig('', 'account', $company_id);
        $config['account'] = $account;
        return returnResult(ReturnCodeService::SUCCESS, '获取成功', $config);
    }

    /**
     * 获取结算返券类型ID
     * @param $company_id
     * @return mixed
     */
    public function couponCateId($company_id)
    {
        $data = $this->couponCateModel->getOne(['company_id' => $company_id, 'put_type' => 5, 'status' => 1], 'id');
        if (!empty($data)) {
            return $data['id'];
        }
        $add_data['company_id'] = $company_id;
        $add_data['condition'] = $company_id;
        $add_data['name'] = '结算返券';
        $add_data['coupon_type'] = 1;
        $add_data['put_type'] = 5;
        $add_data['time_type'] = 1;
        $add_data['use_days'] = 100;
        $add_data['use_start_time'] = time();
        $add_data['use_end_time'] = strtotime('+100 day');
        $add_data['money'] = 0;//根据具体订单结算金额
        $add_data['add_time'] = time();
        var_dump($add_data);
        $id = $this->couponCateModel->add($add_data);
        return $id;
    }


    /**
     * 获取阶梯增长数据
     * @param $user_id
     * @param int $position
     * @param $result
     * @param int $index
     * @return array
     */
    public function stepAllocation($user_id, $position = 4, &$result, $index = 1, &$group, $before = false)
    {
        $first_workers = $this->upWorkers($user_id, $position, $before);
        $pid = $user_id;
        if ($first_workers['code'] == 1000) {
            $pid = $first_workers['data']['user_id'];
            if (in_array($pid, $group)) {
                //该用户的推荐人已经在组里面，说明该层级关系已经闭环
                return false;
            }
            array_push($group, $pid);
            $first_workers['data']['index'] = $index;
            $position = $first_workers['data']['position'];
            $result[] = $first_workers['data'];
            $index += 1;
        }
        if ($position < 4) {
            $position += 1;
            return $this->stepAllocation($pid, $position, $result, $index, $group, $before);
        }
        return $result;

    }

    /**
     * 获取直推人信息
     * @param $user_id
     * @return array
     */
    public function direct_push_person($user_id)
    {
        $user_info = $this->customerModel->getOne(['user_id' => $user_id, 'is_delete' => 0], 'user_id,source,is_dealer');
        if (empty($user_info) || empty($user_info['source'])) {
            return returnResult(-1000, "不存在直推或者用户信息错误");
        }
        $workers_info = $this->oaWorkersModel->getOne(['user_id' => $user_info['source']], 'user_id,workers_id,position');
        if (empty($workers_info)) {
            return returnResult(-1000, "直推人非市场推广");
        }
        return returnResult(1000, "获取成功", $workers_info);
    }

    //升级
    public function upgradeTeam($user_id)
    {
        //查询直推
        $user_info = $this->customerModel->getOne(['user_id' => $user_id, 'is_delete' => 0], 'user_id,source,is_dealer');

        if (empty($user_info) || empty($user_info['source'])) {
            return returnResult(-1000, "不存在直推或者用户信息错误");
        }
        $pid = $user_info['source'];
        $this->upgrade($pid);//直推升级
        //查询间推
        $pid_info = $this->customerModel->getOne(['user_id' => $pid, 'is_delete' => 0], 'user_id,source,is_dealer');
        if (!empty($pid_info['source'])) {
            $p_pid = $pid_info['source'];
            $this->upgrade($p_pid);//间推升级
        }

    }

    public function upgrade($pid)
    {
        $config['upgrade_standard'] = [0, 100, 300, 500];//升级标准 分别对应普通，区级，市级,省级升级标准（炉具数量）
        $workers_info = $this->oaWorkersModel->getOne(['user_id' => $pid], 'user_id,workers_id,position,position_change_mode');
        if (empty($workers_info)) {
            return returnResult(-1000, "直推人非市场推广");
        }

        if ($workers_info['position'] == 6) {
            return returnResult(-1000, "已经是最高等级,无法再升级");
        }
        $result = $this->teamInstall($pid);
        if ($result['code'] != 1000) {
            return returnResult(-1000, "暂不升级");
        }
        $num = $result['data'];//炉具数量
        $position = $workers_info['position'];
        $cur_index = $this->sorts($config['upgrade_standard'], $num);
        $cur_position = $cur_index;
        if ($workers_info['position'] > $cur_position) {
            return returnResult(-1000, "暂不进行降级");
        }
        if ($cur_index <= 0) {
            $cur_position = 1;
        } else if ($cur_index > 3) {
            $cur_position = 4;
        }
        if ($workers_info['position_change_mode'] == 2 && $cur_position < $position) {
            $cur_position = $position;//若手动升级的不进行降级
        }
        if ($cur_position != $position) {
            //修改等级
            $this->oaWorkersModel->save(['workers_id' => $workers_info['workers_id']], ['position' => $cur_position, 'position_change_mode' => 1]);
            //添加记录
            $workers_data = ['workers_id' => $workers_info['workers_id'], 'before_upgrade_grade' => $workers_info['position'], 'after_upgrade' => $cur_position, 'create_time' => time()];
            $this->achievementModel->table = 'oa_workers_upgrade_log';
            $this->achievementModel->insertData($workers_data);
        }

    }

    public function sorts($stage_data, $stage_num)
    {
        array_push($stage_data, $stage_num);
        $data = array_unique($stage_data);
        sort($data);
        return array_search($stage_num, $data);
    }

    public function teamInstall($user_id)
    {
        $result = $this->team($user_id);
        if ($result['code'] != 1000) {
            return returnResult(1000, "无数据", 0);
        }
        $member = $result['data'];
        $data = $this->customerCodeModel->getAll(['user_id' => ['IN', $member], 'state' => 1], 'actual_transformation_num');
        if (empty($data)) {
            return returnResult(1000, "无数据", 0);
        }
        $num = array_sum(array_column($data, 'actual_transformation_num'));
        return returnResult(1000, "获取成功", $num);
    }

    public function team($user_id)
    {
        $team_id = [];
        //查询直推
        $user_son = $this->customerModel->getAll(['source' => $user_id, 'is_delete' => 0], 'user_id,source,is_dealer');
        if (empty($user_son)) {
            return returnResult(-1000, "无团队");
        }
        $user_id_son = array_column($user_son, 'user_id');
        $team_id = array_merge($team_id, $user_id_son);
        //查询间推
        $user_son_son = $this->customerModel->getAll(['source' => ['IN', $user_id_son], 'is_delete' => 0], 'user_id,source,is_dealer');
        if (!empty($user_son_son)) {
            $user_id_son_son = array_column($user_son_son, 'user_id');
            $team_id = array_merge($team_id, $user_id_son_son);
        }
        $workers_info = $this->oaWorkersModel->getOne(['user_id' => $user_id], 'user_id,workers_id,position');
        if (empty($workers_info)) {
            return returnResult(-1000, "直推人非市场推广");
        }
        $position = $workers_info['position'];
        $team = [];
        array_push($team, $user_id);
        //若同级不属于团队人员
        foreach ($team_id as $k => $v) {
            $team_info = $this->oaWorkersModel->getOne(['user_id' => $v], 'user_id,workers_id,position');
            if (!empty($team_info)) {
                if ($team_info['position'] >= $position) {
                    continue;
                }
            }
            array_push($team, $v);
        }

        return returnResult(1000, "团队", $team);
    }

    /**
     * 新增业绩信息
     * @param $bill_info：customer_code户号 ,bill_id账单ID
     * @return mixed
     */
    public function addAchievement($bill_info)
    {
        $install_data_result = $this->installData($bill_info['contract_id']);
        if ($install_data_result['code'] != 1000) {
            return returnResult($install_data_result['code'], $install_data_result['msg']);
        }
        $install_data = $install_data_result['data'];
        $add_data['engineers'] = json_encode($install_data['engineers']);
        $add_data['achievement_type'] = 2;
        $add_data['customer_code'] = $bill_info['customer_code'];
        $add_data['form_bill_id'] = $bill_info['bill_id'];
        $add_data['state'] = 1;
        $add_data['add_time'] = time();
        $this->achievementModel->table = 'oa_achievement';
        $achievement_id = $this->achievementModel->insertData($add_data);
        return returnResult(1000, "添加结算记录成功", $achievement_id);
    }

    //添加业绩明细
    public function addAchievementWorkers($achievement_id, $item)
    {
        $data['achievement_id'] = $achievement_id;
        $data['source'] = $item['type'];
        $data['role'] = $item['role'];
        if ($item['role'] == 5 || $item['role'] == 6) {
            $data['role'] = 3;//区域承包商保存一个角色
        }
        $data['workers_id'] = $item['uid'];
        $data['workers_position'] = $item['workers_position'];
        $data['money'] = $item['money'];
        $data['add_time'] = time();
        $return = $this->oaAchievementWorkersModel->add($data);
        return returnResult(1000, "结算成功", $return);
    }

    //添加业绩明细-手动升级之前
    public function addAchievementWorkersBefore($achievement_id, $item)
    {
        $data['achievement_id'] = $achievement_id;
        $data['source'] = $item['type'];
        $data['role'] = $item['role'];
        if ($item['role'] == 5 || $item['role'] == 6) {
            $data['role'] = 3;//区域承包商保存一个角色
        }
        $data['workers_id'] = $item['uid'];
        $data['workers_position'] = $item['workers_position'];
        $data['workers_before_position'] = empty($item['workers_before_position']) ? 0 : $item['workers_before_position'];
        $data['money'] = $item['money'];
        $data['add_time'] = time();
        $this->achievementModel->table = 'oa_achievement_workers_before';
        $return = $this->achievementModel->insertData($data);
        return returnResult(1000, "结算成功", $return);
    }

    /**
     * showdoc
     * Author: Xuleni
     * DateTime: 2020/11/23 9:21
     * @catalog
     * @title 更新金额、添加日志
     * @param $uid uid
     * @param $money 金额
     * @param $role 角色
     * @param $type 类型
     * @param $operation 操作类型 1更新金额 2更新记录
     */
    public function updateAccount($uid, $money, $role, $type = 0, $operation)
    {

        $log_data = ['money' => $money, 'change_type' => 1, 'account_type' => 2, 'is_unfreeze' => 2, 'source' => 1, 'create_time' => time(), 'type' => $type];
        switch ($role) {
            case 1: //市场推广
                $workers_info = $this->oaWorkersModel->getOne(['workers_id' => $uid], 'workers_id,position,temp_money,total_money');
                if (!empty($workers_info)) {
                    if ($operation == 1) {
                        //查询市场推广等级
                        //更新金额
                        $temp_money = $workers_info['temp_money'] + $money;
                        $total_money = $workers_info['total_money'] + $money;
                        $this->oaWorkersModel->save(['workers_id' => $workers_info['workers_id']], ['temp_money' => $temp_money, 'total_money' => $total_money]);
                    } else {
                        $log_data['role'] = $role;
                        $log_data['workers_id'] = $workers_info['workers_id'];
                        $log_data['position'] = $workers_info['position'];
                        $this->oaWorkersBillModel->add($log_data);
                    }
                }
                break;
            case 2: //工程
                $engineers = $this->engineersModel->getOne(['engineers_id' => $uid], 'engineers_id,temp_money,total_money');
                if (!empty($engineers)) {
                    if ($operation == 1) {
                        //更新金额
                        $temp_money = $engineers['temp_money'] + $money;
                        $total_money = $engineers['total_money'] + $money;
                        $this->engineersModel->save(['engineers_id' => $uid], ['temp_money' => $temp_money, 'total_money' => $total_money]);
                    } else {
                        //更新金额
                        $log_data['engineers_id'] = $uid;
                        $this->engineersBillModel->add($log_data);
                    }
                }
                break;
            case 3: //承包商
            case 4: //总后台
            case 5: //承包商
            case 6: //承包商
                $set_role = 1;
                if ($role == 3) {
                    $set_role = 5;
                } else if ($role == 5) {
                    $set_role = 4;
                } else if ($role == 6) {
                    $set_role = 3;
                }
                if ($operation == 1) {
                    $admin_account = $this->adminAccountModel->getOne(['aid' => $uid, 'role' => $set_role], 'frozen_money,total_money');
                    if (!empty($admin_account)) {
                        //更新金额
                        $temp_money = $admin_account['frozen_money'] + $money;
                        $total_money = $admin_account['total_money'] + $money;
                        $this->adminAccountModel->save(['aid' => $uid, 'role' => $set_role], ['frozen_money' => $temp_money, 'total_money' => $total_money]);
                    } else {
                        //更新金额
                        $temp_money = $money;
                        $total_money = $money;
                        $this->adminAccountModel->add(['frozen_money' => $temp_money, 'total_money' => $total_money, 'aid' => $uid, 'role' => $set_role]);
                    }

                } else {
                    $log_data['aid'] = $uid;
                    $log_data['role'] = $set_role;
                    $this->adminAccountBillModel->add($log_data);
                }
                break;
            default:
                return returnResult(-1000, "更新金额类型");
        }
        return returnResult(1000, "结算成功", 1);
    }

    //创建公司、市场总监、市场助理
    public function initAccount()
    {
        //公司
        $workers_info = $this->oaWorkersModel->getOne(['workers_number' => '88888887'], 'workers_id,user_id');
        if (empty($workers_info)) {
            $account = '88888887';
            $user_data['account'] = $account;
            $user_data['realname'] = '公司';
            $user_data['add_time'] = time();
            $user_data['user_role'] = 2;
            $user_data['source'] = 0;
            $user_data['telphone'] = 18888888880;
            $pid = $this->customerModel->add($user_data);
            $workers_data['user_id'] = $pid;
            $workers_data['position'] = 1;
            $workers_data['workers_name'] = '公司';
            $workers_data['workers_number'] = $account;
            $workers_data['workers_phone'] = 18888888880;
            $workers_data['temp_money'] = 0;
            $workers_data['total_money'] = 0;
            $workers_data['cash_available'] = 0;
            $workers_data['add_time'] = time();
            $workers_data['workers_push_people'] = 0;
            $this->oaWorkersModel->add($workers_data);
        } else {
            $pid = $workers_info['user_id'];
        }

        //创建市场总监
        $workers_info = $this->oaWorkersModel->getOne(['workers_number' => '88888889'], 'workers_id,user_id');
        if (empty($workers_info)) {
            $account = '88888889';
            $user_data['account'] = $account;
            $user_data['realname'] = '市场总监';
            $user_data['add_time'] = time();
            $user_data['user_role'] = 2;
            $user_data['source'] = $pid;
            $user_data['telphone'] = 18888888889;
            $user_id = $this->customerModel->add($user_data);
            $workers_data['user_id'] = $pid;
            $workers_data['position'] = 8;
            $workers_data['workers_name'] = '市场总监';
            $workers_data['workers_number'] = $account;
            $workers_data['workers_phone'] = 18888888889;
            $workers_data['workers_push_people'] = $pid;
            $workers_data['temp_money'] = 0;
            $workers_data['total_money'] = 0;
            $workers_data['cash_available'] = 0;
            $workers_data['add_time'] = time();
            $this->oaWorkersModel->add($workers_data);
        } else {
            $user_id = $workers_info['user_id'];
        }
        $pid = $user_id;

        //创建市场助理
        $workers_info = $this->oaWorkersModel->getOne(['workers_number' => '88888888'], 'workers_id,user_id');
        if (empty($workers_info)) {
            $account = '88888888';
            $user_data['account'] = $account;
            $user_data['realname'] = '默认推荐人';
            $user_data['add_time'] = time();
            $user_data['user_role'] = 2;
            $user_data['source'] = $pid;
            $user_data['telphone'] = 18888888888;
            $user_id = $this->customerModel->add($user_data);
            $workers_data['user_id'] = $user_id;
            $workers_data['position'] = 7;
            $workers_data['workers_name'] = '默认推荐人';
            $workers_data['workers_number'] = $account;
            $workers_data['workers_phone'] = 18888888888;
            $workers_data['workers_push_people'] = $pid;
            $workers_data['temp_money'] = 0;
            $workers_data['total_money'] = 0;
            $workers_data['cash_available'] = 0;
            $workers_data['add_time'] = time();
            $this->oaWorkersModel->add($workers_data);

        }
        return true;

    }


}