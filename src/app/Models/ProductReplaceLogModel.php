<?php
namespace app\Models;
use Server\CoreBase\Model;

/**   TX
 *    产品替换记录 Model
 *    Date: 2018/7/18
 * Class EquipmentModel
 * @package app\Models
 */
class ProductReplaceLogModel extends Model
{
    // 表名
    protected $dbName = 'product_replace_log';

    /**   YSF
     *    主板分类列表--分页
     * @param array $where   查询条件
     * @param int $page      当前页码
     * @param int $pageSize  每页数量
     * @param string $field  查询字段
     * @param array $order   排序方式
     * @return mixed
     */
    public function getAll(array $where, int $page, int $pageSize, string $field = '*', array $order = ['equipments_id' => 'DESC'],array $join)
    {
        $result = $this->db->select($field)
            ->from($this->dbName)
            ->TPWhere($where)
            ->TPJoin($join)
            ->page($pageSize, $page)
            ->order($order)
            ->query()
            ->result_array();

        return $result;
    }

    // 获取总数量
    public function getCount(array $where, string $field = '*')
    {
        $result = $this->db->select($field)
            ->from($this->dbName)
            ->TPWhere($where)
            ->query()
            ->num_rows();
        return $result;
    }

    //计数
    public function count(array $map, array $join = [])
    {
        $result = $this->db->select('*')
            ->from($this->dbName)
            ->TPWhere($map)
            ->TPJoin($join)
            ->query()
            ->num_rows();
        return $result;
    }

    /**   YSF
     *    主板分类详情
     * @param array $where  查询条件
     * @param string $field 查询字段
     * @return null
     */
    public function getDetail(array $where, string $field='*')
    {
        $result = $this->db->select($field)
            ->from($this->dbName)
            ->TPWhere($where)
            ->query()
            ->row();
        return $result;
    }


    /**
     * @desc  更新信息
     * @param  无
     * @date   2018-07-18
     * @author lcx
     * @param  array      $where [description]
     * @param  array      $data  [description]
     * @return [type]            [description]
     */
    public function save(array $where,array $data){
        $result = $this->db->update($this->dbName)
            ->set($data)
            ->TPwhere($where)
            ->query()
            ->affected_rows();
        return $result;
    }

    /**
     * @desc  查询一条数据
     * @param  无
     * @date   2020-2-25
     * @author tx
     * @param  array      $where [description]
     * @param  string     $field [description]
     * @return [type]            [description]
     */
    public function getOne(array $where, string $field="*",array $join=array()){
        $result = $this->db
            ->select($field)
            ->from($this->dbName)
            ->TPWhere($where)
            ->TPJoin($join)
            ->query()
            ->row();
        return $result;
    }

    /**
     * @desc   添加信息
     * @param  无
     * @date   2018-07-24
     * @author lcx
     * @param  array      $data [description]
     */
    public function add(array $data){
        $id = $this->db->insert($this->dbName)
            ->set($data)
            ->query()
            ->insert_id();
        return $id;
    }


}