<?php

namespace app\Models;

use app\Services\Common\CommonService;
use app\Services\Common\ConfigService;
use app\Services\Device\DeviceService;
use Noodlehaus\Exception;
use Server\CoreBase\Model;
use Server\CoreBase\SwooleException;

/**
 * 合同
 */
class ContractModel extends Model
{
    protected $table = 'contract';
    protected $customerCodeModel;
    protected $customerCodeBillModel;
    protected $AchievementModel;

    public function initialization(&$context)
    {
        parent::initialization($context);
        $this->customerCodeModel = $this->loader->model('CustomerCodeModel', $this);
        $this->customerCodeBillModel = $this->loader->model('CustomerCodeBillModel', $this);
        $this->AchievementModel = $this->loader->model('AchievementModel', $this);
    }

    /**
     * @desc  查询一条数据
     * @param 无
     * @date   2018-07-24
     * @param array $where [description]
     * @param string $field [description]
     * @return [type]            [description]
     * @author lcx
     */
    public function getOne(array $where, string $field = "*", array $join = [])
    {
        $result = $this->db
            ->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->TPJoin($join)
            ->query()
            ->row();
        return $result;
    }

    /**
     * @desc   添加信息
     * @param 无
     * @date   2018-07-24
     * @param array $data [description]
     * @author lcx
     */
    public function add(array $data)
    {
        $id = $this->db->insert($this->table)
            ->set($data)
            ->query()
            ->insert_id();
        return $id;
    }

    /**
     * @desc  更新信息
     * @param 无
     * @date   2018-07-24
     * @param array $where [description]
     * @param array $data [description]
     * @return [type]            [description]
     * @author lcx
     */
    public function save(array $where, array $data)
    {
        $result = $this->db->update($this->table)
            ->set($data)
            ->TPwhere($where)
            ->query()
            ->affected_rows();
        return $result;
    }

    /**    lcx
     *     分页查询
     * @param array $where
     * @param string $field
     * @param $join
     * @param array $order
     * @return mixed
     */
    public function getAll(array $where, string $field = '*', array $order = ['add_time' => 'DESC'], int $page = 1, int $pageSize = -1)
    {

        if ($pageSize < 0) {
            $result = $this->db->select($field)
                ->from($this->table)
                ->TPWhere($where)
                ->order($order)
                ->query()
                ->result_array();
        } else {
            $result = $this->db->select($field)
                ->from($this->table)
                ->TPWhere($where)
                ->page($pageSize, $page)
                ->order($order)
                ->query()
                ->result_array();
        }
        return $result;
    }

    //生成合同
    public function addContract(array $add_data, int $user_id, int $a_id, int $o_id, int $company_id = 1)
    {
        //收费标准
        $gas_fee_standard = '[{"pay_unit_price":"180","pay_multi_unit_price":"150","id":"1","name":"\u7092\u7076"},{"pay_unit_price":"240","pay_multi_unit_price":"200","id":"2","name":"\u5927\u9505\u7076"}]';
        $data['contract_no'] = CommonService::createSn('cloud_contract_no');
        $data['contact_type'] = 3; //2电子合同-电子签名 1电子合同-确认同意 3纸质合同
        $data['user_id'] = $user_id;
        $data['company_id'] = $company_id;
        $data['business_id'] = 3;
        $data['administrative_id'] = $a_id;
        $data['operation_id'] = $o_id;
        $data['shop_name'] = $add_data['shop_name'] ?? '';
        $data['explainer'] = $add_data['explainer'] ?? '';
        $data['workers_id'] = $add_data['workers_id'] ?? '';
        $data['identification_id'] = $add_data['identification_id'] ?? '0';
        $data['province'] = $add_data['province'] ?? '';
        $data['city'] = $add_data['city'] ?? '';
        $data['area'] = $add_data['area'] ?? '';
        $data['province_code'] = $add_data['province_code'] ?? '';
        $data['city_code'] = $add_data['city_code'] ?? '';
        $data['area_code'] = $add_data['area_code'] ?? '';
        $data['address'] = $add_data['address'] ?? '';
        $data['transform_stove_number'] = $add_data['transform_stove_number'] ?? 0;
        $data['login_phone'] = $add_data['login_phone'] ?? '';
        $data['transform_end_time'] = $add_data['transform_end_time'] ?? '';
        $data['first_party_assign_contact_name'] = $add_data['first_party_assign_contact_name'] ?? '';
        $data['first_party_assign_contact_phone'] = $add_data['first_party_assign_contact_phone'] ?? '';
        $data['first_party_name'] = $add_data['first_party_name'] ?? '';
        $data['first_party_address'] = $add_data['first_party_address'] ?? '';
        $data['first_party_contact_name'] = $add_data['first_party_contact_name'] ?? '';
        $data['first_party_contact_phone'] = $add_data['first_party_contact_phone'] ?? '';
        $data['related_name'] = $add_data['related_name'] ?? '';
        $data['type'] = 4;
        $data['status'] = 4;//合同状态 审核通过
        $data['gas_fee_standard'] = $gas_fee_standard;
        $data['add_time'] = time();
        $data['add_time'] = time();
        $contract_id = $this->add($data);
        return $contract_id;
    }

    /**
     * 合同下发套餐
     * @param $contract_id
     * @return mixed
     */
    public function activateDevice($contract_id, $device_arr = [])
    {
        if (empty($device_arr) || !is_array($device_arr)) {
            return returnResult(-1000, "设备编号信息必须是数组");
        }
        $contract_info = $this->getOne(['contract_id' => $contract_id], 'company_id,effect_time,status');
        if (empty($contract_info) || !in_array($contract_info['status'], [4, 11, 12])) {
            return returnResult(-1000, "合同不存在或者未生效,请先签署合同");
        }
        $customerCode = $this->customerCodeModel->getOne(['contract_id' => $contract_id], 'code');
        if (empty($customerCode)) {
            return returnResult(-1000, "户号不存在");
        }
        $bill_info = $this->customerCodeBillModel->getAll(['customer_code' => $customerCode['code'], 'status' => 2, 'is_delete' => 0], 'cycle_end_time', ['cycle_end_time' => 'DESC']);
        $time = $contract_info['effect_time'];
        if (!empty($bill_info)) {
            $time = $bill_info[0]['cycle_end_time'];
        }
        //由于绑定设备时还未移交,默认下发当前时间
        if (empty($time)) {
            $time = time();
        }
        $month_date = getthemonth(date('Y-m-d', $time));
        $expire_date = $month_date[1];//下个月1号到期
        $shutdown_date = date('Y-m-15', strtotime('next month', $time));//下个月15号停机
        $custome_code = $customerCode['code'];
        $dev_mode = ConfigService::getConfig('develop_mode', false, $contract_info['company_id']);
        $qrcode = $this->config->get('equipment_qrcode');
        if ($dev_mode == 1) {
            $qrcode = $this->config->get('debug_config.equipment_qrcode');
        }
        foreach ($device_arr as $k => $v) {
            $params = ['sn' => $v, 'expire_date' => $expire_date, 'shutdown_date' => $shutdown_date];
            DeviceService::Thrash($params, 'binding_package');
            \co::sleep(2);
            $qrcode_params = ['sn' => $v, 'content' => $qrcode . $custome_code];
            DeviceService::Thrash($qrcode_params, 'set_qrcode');
        }
        return returnResult(1000, "操作成功");
    }

    /*
     * 自定义下发套餐
     */
    public function customActivateDevice($contract_id, $device_arr = [], $expire_date = '', $shutdown_date = '')
    {
        if (empty($device_arr) || !is_array($device_arr)) {
            return returnResult(-1000, "设备编号信息必须是数组");
        }
        $contract_info = $this->getOne(['contract_id' => $contract_id], 'company_id,effect_time,status');
        if (empty($contract_info) || !in_array($contract_info['status'], [4, 11, 12])) {
            return returnResult(-1000, "合同不存在或者未生效,请先签署合同");
        }
        $customerCode = $this->customerCodeModel->getOne(['contract_id' => $contract_id], 'code');
        if (empty($customerCode)) {
            return returnResult(-1000, "户号不存在");
        }
        $custome_code = $customerCode['code'];
        $dev_mode = ConfigService::getConfig('develop_mode', false, $contract_info['company_id']);
        $qrcode = $this->config->get('equipment_qrcode');
        if ($dev_mode == 1) {
            $qrcode = $this->config->get('debug_config.equipment_qrcode');
        }
        //修改到期时间
        $this->save(['contract_id' => $contract_id], ['effect_time' => strtotime($expire_date)]);
        foreach ($device_arr as $k => $v) {
            $params = ['sn' => $v, 'expire_date' => $expire_date, 'shutdown_date' => $shutdown_date];
            DeviceService::Thrash($params, 'binding_package');
            \co::sleep(2);
            $qrcode_params = ['sn' => $v, 'content' => $qrcode . $custome_code];
            DeviceService::Thrash($qrcode_params, 'set_qrcode');
        }
        return returnResult(1000, "操作成功");
    }


    /*
     * 获取ICCID卡
     */
    public function getIccId($sn)
    {
        $params = ['sn' => $sn];
        DeviceService::Thrash($params, 'iccid');
        return returnResult(1000, "操作成功");
    }


    /*
     * 获取运行时长
     */
    public function getReadRunningTime($sn)
    {
        $params = ['sn' => $sn];
        DeviceService::Thrash($params, 'read_running_time');
        return returnResult(1000, "操作成功");
    }

    public function clearRunningTime($sn)
    {
        $params = ['sn' => $sn];
        DeviceService::Thrash($params, 'clear_running_time');
        return returnResult(1000, "操作成功");
    }


    //查询合同附件
    public function enclosure(string $contract_id)
    {
        $contract_data = $this->getOne(['contract_id' => $contract_id], 'contract_no');
        if (empty($contract_data)) return returnResult(-1000, '暂无数据');
        $this->AchievementModel->table = "energy_saving_data_test";
        $energy_where = ['contract_id' => $contract_id, 'status' => 1, 'is_merged' => 1, 'type' => ['<>', 3]];
        $energy_field = "id,type,work_order_type";
        $energy_data = $this->AchievementModel->selectData($energy_where, $energy_field, [], ['work_order_type' => 'asc', 'type' => 'asc', 'create_time' => 'desc']);
        $default_data = [['id' => 0, 'work_order_type' => 0, 'type' => 0, 'name' => '能源管理合同']];
        $data = [];
        if (!empty($energy_data)) {
            foreach ($energy_data as $k => $v) {

                $data[$k]['id'] = $v['id'];
                $data[$k]['type'] = $v['type'];
                $data[$k]['work_order_type'] = $v['work_order_type'];
                // $name = ($v['work_order_type'] == 1 ? '试用' : '改造');
                // if ($v['type'] == 1) $name .= '燃烧器节能数据测试确认单';
                // if ($v['type'] == 2) $name .= '财产移交使用保管清单';
                // if ($v['type'] == 3) $name = '节能效益分享确认单';
                $prefix = '';
                // if ($v['work_order_type'] == 1 && $v['type'] == 1) {
                //     $prefix = '附件1';
                // } else if ($v['work_order_type'] == 1 && $v['type'] == 2) {
                //     $prefix = '附件2';
                // } else if ($v['work_order_type'] == 2 && $v['type'] == 1) {
                //     $prefix = '附件3';
                // } else if ($v['work_order_type'] == 2 && $v['type'] == 2) {
                //     $prefix = '附件4';
                // } else if ($v['type'] == 3) {
                //     $prefix = '附件5';
                // }
                if ($v['type'] == 1) $name = '炉具节能数据确认单';
                if ($v['type'] == 2) $name = '财产移交使用保管清单';
                if ($v['work_order_type'] == 2 && $v['type'] == 1) {
                    $prefix = '附件1';
                } else if ($v['work_order_type'] == 2 && $v['type'] == 2) {
                    $prefix = '附件2';
                } else if ($v['type'] == 3) {
                    $prefix = '附件3';
                }
                $data[$k]['name'] = $name;
                $data[$k]['prefix'] = $prefix;
               
            }
        }
        return ['data' => ['A' => $default_data, 'B' => $data], 'contract_no' => $contract_data['contract_no'] ?? 0];
    }

    /**查询合同附件*/
    public function contractEnclosure(string $contract_id, $type = 1)
    {
        $contract_data = $this->getOne(['contract_id' => $contract_id], 'contract_enclosure,enclosure');
        if (empty($contract_data)) return returnResult(-1000, '暂无附件信息');
        $contract_enclosure = $type == 1 ? json_decode($contract_data['contract_enclosure'], true) : json_decode($contract_data['enclosure'], true);
        if (empty($contract_enclosure)) return [];
        foreach ($contract_enclosure as $key => $value) {
            $contract_enclosure[$key]['new_name_url'] = UploadImgPath($value['new_name']);
        }
        return $contract_enclosure;
    }


    public function contractType($contract_id, $work_order_id, $type = 1)
    {
        if (empty($contract_id)) {
            return ['name' => '', 'code' => '', 'prefix' => ''];
        }
        $contract_data = $this->getOne(['contract_id' => $contract_id], 'type');
        if (empty($contract_data)) return returnResult(-1000, '暂无合同信息');
        $energy_where = ['contract_id' => $contract_id, 'status' => ['IN', [1, 3]], 'type' => $type];
        $energy_field = "id,work_order_type,type";
        $this->AchievementModel->table = "energy_saving_data_test";
        $data = $this->AchievementModel->findData($energy_where, $energy_field, [], []);

        //处理合同code
        $contract_type_code = $this->config->get('contract_type_code')[$contract_data['type']] ?? '';
        $contract_enclosure_code = $this->config->get('contract_enclosure_code')[$data['work_order_type']] ?? '';
        $code = ($data['type'] == 3) ? $contract_type_code : $contract_type_code . '-' . $contract_enclosure_code;//保管清单和测试确认单：合同类型code-附件类型code,分享确认单:合同类型code
        $name = ($data['work_order_type'] == 1 ? "试用" : "改造");
        $prefix = '';
        // if ($data['work_order_type'] == 1 && $data['type'] == 1) {
        //     $prefix = '附件1';
        // } else if ($data['work_order_type'] == 1 && $data['type'] == 2) {
        //     $prefix = '附件2';
        // } else if ($data['work_order_type'] == 2 && $data['type'] == 1) {
        //     $prefix = '附件3';

        // } else if ($data['work_order_type'] == 2 && $data['type'] == 2) {
        //     $prefix = '附件4';
        // } else if ($data['type'] == 3) {
        //     $prefix = '附件5';
        //     $name = '';
        // }
        if ($data['work_order_type'] == 2 && $data['type'] == 1) {
            $prefix = '附件1';
        } else if ($data['work_order_type'] == 2 && $data['type'] == 2) {
            $prefix = '附件2';
        } else if ($data['type'] == 3) {
            $prefix = '附件3';
            $name = '';
        }

        return ['name' => $name, 'code' => $code, 'prefix' => $prefix];
    }


}