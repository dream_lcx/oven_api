<?php
namespace app\Models;
use Server\CoreBase\Model;
/**
 * 渠道模型
 */
class ToDoListModel extends Model
{
    protected $table = 'to_do_list';
    /**
     * @desc  查询一条数据
     * @param  无
     * @date   2020-2-25
     * @author tx
     * @param  array      $where [description]
     * @param  string     $field [description]
     * @return [type]            [description]
     */
    public function getOne(array $where, string $field="*",array $join=array()){
        $result = $this->db
            ->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->TPJoin($join)
            ->query()
            ->row();
        return $result;
    }

    /**    lcx
     *     分页查询
     * @param array $where
     * @param int $page
     * @param int $pageSize
     * @param string $field
     * @param $join
     * @param array $order
     * @return mixed
     */
    public function getAll(array $where, int $page, int $pageSize, string $field = '*', $join,array $order = ['rq_channel.create_time' => 'DESC'])
    {
        $result = $this->db->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->TPJoin($join)
            ->page($pageSize,$page)
            ->order($order)
            ->query()
            ->result_array();

        return $result;
    }
    /**
     * @desc   统计数量
     * @param  无
     * @date   2018-07-18
     * @author lcx
     * @param  array      $data [description]
     */
    public function count($map,$join=[]){
        $result = $this->db->select('*')
            ->from($this->table)
            ->TPWhere($map)
            ->TPJoin($join)
            ->query()
            ->num_rows();
        return $result;
    }
    /**
     * @desc   添加信息
     * @param  无
     * @date   2018-07-18
     * @author lcx
     * @param  array      $data [description]
     */
    public function add(array $data){
        $id = $this->db->insert($this->table)
            ->set($data)
            ->query()
            ->insert_id();
        return $id;
    }
    /**
     * @desc  更新信息
     * @param  无
     * @date   2018-07-18
     * @author lcx
     * @param  array      $where [description]
     * @param  array      $data  [description]
     * @return [type]            [description]
     */
    public function save(array $where,array $data){
        $result = $this->db->update($this->table)
            ->set($data)
            ->TPwhere($where)
            ->query()
            ->affected_rows();
        return $result;
    }



}