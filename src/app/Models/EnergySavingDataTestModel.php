<?php

namespace app\Models;

use Server\CoreBase\Model;

class EnergySavingDataTestModel extends Model
{
    // 表名
    protected $table = 'energy_saving_data_test';

    /**
     * @desc  查询一条数据
     * @param 无
     * @date   2018-07-18
     * @param array $where [description]
     * @param string $field [description]
     * @return [type]            [description]
     * @author lcx
     */
    public function getOne(array $where, string $field = "*", $order = [])
    {
        $result = $this->db
            ->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->order($order)
            ->query()
            ->row();
        return $result;
    }

    /**
     * 获取所有数据
     * @param array $where
     * @param string $field
     * @return type
     */
    public function getAll(array $where, string $field = '*', array $join = array(), $page = 1, $pageSisz = 10000, array $order = array())
    {
        $result = $this->db->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->TPJoin($join)
            ->page($pageSisz, $page)
            ->order($order)
            ->query()
            ->result_array();
        return $result;
    }

    /**
     * @desc  更新信息
     * @param 无
     * @date    2018-08-24
     * @param array $where [description]
     * @param array $data [description]
     * @return [type]            [description]
     * @author lcx
     */
    public function save(array $where, array $data)
    {
        $result = $this->db->update($this->table)
            ->set($data)
            ->TPwhere($where)
            ->query()
            ->affected_rows();
        return $result;
    }


    /**
     * @desc   统计数量
     * @param 无
     * @date   2018-12-6
     * @param array $data [description]
     * @author lcx
     */
    public function count($where, $join = [])
    {
        $result = $this->db->select('*')
            ->from($this->table)
            ->TPWhere($where)
            ->TPJoin($join)
            ->query()
            ->num_rows();
        return $result;
    }

    /**
     * @desc   添加信息
     * @param 无
     * @date   2018-07-20
     * @param array $data [description]
     * @author lcx
     */
    public function add(array $data)
    {
        $id = $this->db->insert($this->table)
            ->set($data)
            ->query()
            ->insert_id();
        return $id;
    }

    // 安装中状态
    public function installation_status($work_order_id, $is_transfer, $is_bind_equipment, $business_id)
    {
        $where = ['work_order_id' => $work_order_id, 'type' => ['<>', 3]];
        $energy_data = $this->getAll($where, '*', [], 1, 999999, ['id' => 'ASC']);
        if (empty($energy_data)) return '';
        $is_a = true;
        //判断用户是否签字
        foreach ($energy_data as $key => $value) {
            if (empty($value['esign_user_sign']) && empty($value['esign_user_sign_time'])) {
                $is_a = false;
                break;
            }
        }
        $installation_status = '';
        if ($is_transfer == 1) {
            return '';
        }
        if ($business_id != 4 && $is_bind_equipment == 1 && $is_a == false) {
            $installation_status = '待用户签字';
        } elseif ($business_id != 4 && $is_bind_equipment == 1 && $is_a == true) {
            $is_b = true;
            //判断工程是否签字
            foreach ($energy_data as $key => $value) {
                if (empty($value['engineering_esign_user_sign']) && empty($value['engineering_user_sign_time'])) {
                    $is_b = false;
                    break;
                }
            }
            if ($is_b == false) $installation_status = '待工程签字';
        } else if ($business_id == 4 && $is_a == false) {
            $installation_status = '待用户签字';
        } else if ($business_id == 4 && $is_a == true) {
            $is_b = true;
            //判断工程是否签字
            foreach ($energy_data as $key => $value) {
                if (empty($value['engineering_esign_user_sign']) && empty($value['engineering_user_sign_time'])) {
                    $is_b = false;
                    break;
                }
            }
            if ($is_b == false) $installation_status = '待工程签字';
        }
        if (!empty($installation_status)) $installation_status = ',' . $installation_status;
        return $installation_status;
    }


    // 安装中状态(弃用)
    public function installation_status_qiyon($work_order_id, $is_transfer)
    {
        $where = ['work_order_id' => $work_order_id, 'type' => ['<>', 3]];
        $energy_data = $this->getAll($where, '*', [], 1, 999999, ['id' => 'ASC']);
        if (empty($energy_data)) return '';
        $name_array = [1 => '测试确认单', 2 => '保管清单', 3 => '分享确认单'];
        $installation_status = '';
        foreach ($energy_data as $key => $value) {
            $name = $name_array[$value['type']];
            if (empty($value['esign_user_sign']) && $value['type'] == 2) {
                $installation_status = $name . '待用户签字';
                break;
            } elseif (!empty($value['esign_user_sign']) && $value['type'] == 2 && empty($value['engineering_esign_user_sign'])) {
                $installation_status = '待工程签字';
                break;
            }
            if (!empty($value['esign_user_sign'])) continue; //判断用户是否签字。用户已签字跳出当前循环，进入下次循环
            if (!empty($value['engineering_esign_user_sign']) && $value['type'] == 2) break; //判断工程人员是否签字。工程已签字，结束循环
            if (in_array($value['type'], [1, 2])) {
                $cooker_data = json_decode($value['cooker_data'], true);
                if ($value['type'] == 2) $cooker_data = $cooker_data['data'];
                foreach ($cooker_data as $k => $v) {
                    //判断用户是否确认
                    if (!empty($v['is_confirm']) && $v['is_confirm'] == 1) {
                        $installation_status = $name . '待用户确认';
                        break;
                    }
                    //测试确认单采集数据是否确认
                    if (!empty($v['is_confirm']) && $v['is_confirm'] == 2 && $value['type'] == 1 && $is_transfer == 1) {
                        $installation_status = $name . '待发起移交';
                    }
                }
                if (!empty($installation_status)) break; //返回字段不为空则跳出循环返回结果
            }
            if (empty($value['esign_user_sign'])) {
                $installation_status = $name . '待用户签字';
                break;
            }
        }
        if (!empty($installation_status)) $installation_status = ',' . $installation_status;
        return $installation_status;
    }


}