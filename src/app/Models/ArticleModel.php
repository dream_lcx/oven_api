<?php
namespace app\Models;

use Server\CoreBase\Model;

/**  YSF
 *   文章模型
 *   Date: 2019/1/23
 * Class ArticleModel
 * @package app\Models
 */
class ArticleModel extends Model
{
    // 表名
    protected $name = 'article';

    // 连表列表查询
    public function getJoinAll(array $where, int $page, int $pageSize, string $field = '*', array $join,array $order = ['c.create_time' => 'DESC'])
    {
        $result = $this->db->select($field)
                ->from('article a')
                ->TPWhere($where)
                ->TPJoin($join)
                ->page($pageSize, $page)
                ->order($order)
                ->query()
                ->result_array();

        return $result;
    }

    // 单表单条查询
    public function getOne(array $where=[], string $field='*')
    {
        $result = $this->db->select($field)
                ->from($this->name)
                ->TPWhere($where)
                ->query()
                ->row();
        return $result;
    }






}