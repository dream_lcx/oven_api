<?php
namespace app\Models;

use Server\CoreBase\Model;

/**   YSF
 *    主板净水量记录表  Model
 *    Date: 2018/7/25
 * Class EquipmentWaterRecordModel
 * @package app\Models
 */
class EquipmentWaterRecordModel extends Model
{
    // 表名
    protected $dbName = 'equipment_water_record';

    /**   YSF
     *    查询单条记录
     * @param array $where   查询条件
     * @param string $field  查询字段
     * @param array $order   排序方式
     * @return null
     */
    public function getOne(array $where, string $field = '*', array $order = [])
    {
        $result = $this->db->select($field)
                        ->from($this->dbName)
                        ->TPWhere($where)
                        ->order($order)
                        ->query()
                        ->row();
        return $result;
    }



}