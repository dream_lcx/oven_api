<?php
namespace app\Models;

use Server\CoreBase\Model;

/**  YSF
 *   金句 模型
 *   Date: 2018/12/3
 * Class SentenceModel
 * @package app\Models
 */
class SentenceModel extends Model
{
    // 表名
    protected $table = 'sentence';

    // 列表查询
    public function getAll(array $where, int $page, int $pageSize, string $field='*', array $order = ['sort' => 'DESC'])
    {
        $result = $this->db->select($field)
                ->from($this->table)
                ->TPWhere($where)
                ->page($pageSize, $page)
                ->order($order)
                ->query()
                ->result_array();

        return $result;
    }


}