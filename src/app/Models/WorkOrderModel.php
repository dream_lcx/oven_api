<?php

namespace app\Models;

use Server\CoreBase\Model;

/**   YSF
 *    工单  Model
 *    Date: 2018/7/18
 * Class WorkOrderModel
 * @package app\Models
 */
class WorkOrderModel extends Model
{
    // 表名
    protected $dbName = 'work_order';

    /**   YSF
     *    获取维修人员我的工单
     * @param array $where 查询条件
     * @param array $join 连表
     * @param int $page 当前页
     * @param int $pageSize 每页条数
     * @param string $field 查询字段
     * @param $order          排序方式
     * @return mixed
     */
    public function getAll(array $where, array $join, int $page, int $pageSize, string $field, $order)
    {
        if ($pageSize < 0) {
            $result = $this->db->select($field)
                ->from($this->dbName)
                ->TPJoin($join)
                ->TPWhere($where)
                ->order($order)
                ->query()
                ->result_array();
        } else {
            $result = $this->db->select($field)
                ->from($this->dbName)
                ->TPJoin($join)
                ->TPWhere($where)
                ->page($pageSize, $page)
                ->order($order)
                ->query()
                ->result_array();
        }
        return $result;
    }
    /**   YSF
     *    获取维修人员我的工单
     * @param array $where 查询条件
     * @param array $join 连表
     * @param int $page 当前页
     * @param int $pageSize 每页条数
     * @param string $field 查询字段
     * @param $order          排序方式
     * @return mixed
     */
    public function getGroupAll(array $where, array $join, int $page, int $pageSize, string $field, $order, $group = 'rq_work_order.work_order_id')
    {
        if ($pageSize < 0) {
            $result = $this->db->select($field)
                ->from($this->dbName)
                ->TPJoin($join)
                ->TPWhere($where)
                ->order($order)
                ->groupBy($group)
                ->query()
                ->result_array();
        } else {
            $result = $this->db->select($field)
                ->from($this->dbName)
                ->TPJoin($join)
                ->TPWhere($where)
                ->page($pageSize, $page)
                ->order($order)
                ->groupBy($group)
                ->query()
                ->result_array();
        }


        return $result;
    }
    public function searchList(array $where, array $join, int $page, int $pageSize, string $field, $order, $con = 'AND')
    {
        $result = $this->db->select($field)
            ->from($this->dbName)
            ->TPJoin($join)
            ->TPWhere($where, $con)
            ->page($pageSize, $page)
            ->order($order)
            ->query()
            ->result_array();
        return $result;
    }

    /**   YSF
     *    获取工单详情
     * @param array $where 查询条件
     * @param array $join 连表操作
     * @param string $field 查询字段
     * @return null
     */
    public function getWorkDetail(array $where, array $join = array(), string $field = '*')
    {
        $result = $this->db->select($field)
            ->from($this->dbName)
            ->TPJoin($join)
            ->TPWhere($where)
            ->query()
            ->row();
        return $result;
    }

    /**   YSF
     *    修改工单
     * @param array $where 条件
     * @param array $data 数据
     * @return mixed
     */
    public function editWork(array $where, array $data)
    {
        $result = $this->db->update($this->dbName)
            ->set($data)
            ->TPWhere($where)
            ->query()
            ->affected_rows();
        return $result;
    }

    /**
     * @desc   添加信息
     * @param 无
     * @date   2018-07-18
     * @param array $data [description]
     * @author lcx
     */
    public function add(array $data)
    {
        $id = $this->db->insert($this->dbName)
            ->set($data)
            ->query()
            ->insert_id();
        return $id;
    }

    /**   YSF
     *    统计数量
     * @param array $where 查询条件
     * @param string $field 查询字段
     * @return int
     */
    public function getCount(array $where, string $field = '*', array $join = [])
    {
        $result = $this->db->select($field)
            ->from($this->dbName)
            ->TPWhere($where)
            ->TPJoin($join)
            ->query()
            ->num_rows();
        return $result;
    }

    /**   YSF
     *    获取订单
     * @param array $where 查询条件
     * @param int $page 当前页
     * @param int $pageSize 每页条数
     * @param array $join 连表
     * @param string $field 查询字段
     * @param array $order 排序方式
     * @param string $dbName 主表
     * @return mixed
     */
    public function workAll(array $where, int $page, int $pageSize, array $join, string $field, array $order, string $dbName = 'work_order a', $group = 'a.work_order_id')
    {
        $result = $this->db->select($field)
            ->from($dbName)
            ->TPWhere($where)
            ->TPJoin($join)
            ->page($pageSize, $page)
            ->order($order)
            ->groupBy($group)
            ->query()
            ->result_array();
        return $result;
    }

    //查询所有工单，不分页
    public function getAllLists(array $where, array $join, string $field, array $order)
    {
        $result = $this->db->select($field)
            ->from($this->dbName)
            ->TPWhere($where)
            ->TPJoin($join)
            ->order($order)
            ->query()
            ->result_array();
        return $result;
    }

    /**   YSF
     *    获取单条数据
     * @param array $where 查询条件
     * @param string $field 查询字段
     * @return null
     */
    public function getOne(array $where, string $field = '*', array $join = array())
    {
        $result = $this->db->select($field)
            ->from($this->dbName)
            ->TPJoin($join)
            ->TPWhere($where)
            ->query()
            ->row();
        return $result;
    }

    /**
     * @desc  更新信息
     * @param 无
     * @date   2018-07-18
     * @param array $where [description]
     * @param array $data [description]
     * @return [type]            [description]
     * @author lcx
     */
    public function save(array $where, array $data)
    {
        $result = $this->db->update($this->dbName)
            ->set($data)
            ->TPwhere($where)
            ->query()
            ->affected_rows();
        return $result;
    }


    /**
     * 累计安装炉具数量
     * @param $contract_id 合同ID
     */
    public function total_installed_quantity($contract_id, $businessid)
    {
        $work_order_where = ['contract_id' => $contract_id, 'work_order_status' => ['not in', [11, 12, 13]], 'business_id' => $businessid];
        $work_order_join = [['work_order_business s', 's.work_order_id = rq_work_order.work_order_id']];
        $work_order_field = "stove_number,work_order_status,actual_stove_number";
        $work_order_ = $this->getAll($work_order_where, $work_order_join, 1, -1,$work_order_field,[]);
        $work_num = 0; //工单安装数据
        if (!empty($work_order_)) {
            foreach ($work_order_ as $key => $value) {
                if (!in_array($value['work_order_status'], [10, 11, 12, 13])) {
                    $work_num += $value['stove_number'];
                } elseif ($value['work_order_status'] == 10) {
                    $work_num += $value['actual_stove_number']; //安装成功后加上实际安装数量
                }
            }
        }
        return $work_num;
    }


}