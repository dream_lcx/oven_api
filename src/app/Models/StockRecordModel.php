<?php
namespace app\Models;
use Server\CoreBase\Model;

/**  TX
 *   库存记录模板 模型
 *   Date: 2020/7/21
 * Class StockModel
 * @package app\Models
 */
class StockRecordModel extends Model
{
    // 表名
    protected $table = 'stock_record';

    // 列表查询
    public function getAll(array $where, string $field='*',$join=[],int $page=1,int $pageSize= 999,array $order=[])
    {
        $result = $this->db->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->TPJoin($join)
            ->page($pageSize,$page)
            ->order($order)
            ->query()
            ->result_array();
        return $result;
    }

    // 获取总数量
    public function getCount(array $where, string $field = '*',$join=[])
    {
        $result = $this->db->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->TPJoin($join)
            ->query()
            ->num_rows();
        return $result;
    }

    /**
     * @desc   添加信息
     * @param  无
     * @date   2018-07-18
     * @author lcx
     * @param  array      $data [description]
     */
    public function add(array $data){
        $id = $this->db->insert($this->table)
            ->set($data)
            ->query()
            ->insert_id();
        return $id;
    }


    /**
     * @desc  更新信息
     * @param  无
     * @date   2018-07-18
     * @author lcx
     * @param  array      $where [description]
     * @param  array      $data  [description]
     * @return [type]            [description]
     */
    public function save(array $where,array $data){
        $result = $this->db->update($this->table)
            ->set($data)
            ->TPwhere($where)
            ->query()
            ->affected_rows();
        return $result;
    }

    /**
     * @desc  查询一条数据
     * @param  无
     * @date   2018-07-18
     * @author lcx
     * @param  array      $where [description]
     * @param  string     $field [description]
     * @return [type]            [description]
     */
    public function getOne(array $where, string $field="*",array $join=array()){
        $result = $this->db
            ->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->TPJoin($join)
            ->query()
            ->row();
        return $result;
    }





}