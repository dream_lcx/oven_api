<?php

namespace app\Models;


use Server\CoreBase\Model;

class StoveManagementModel extends Model
{
    //炉具管理
    // 表名
    protected $table = 'stove_management';

    public function getAll(array $where, string $field = '*', $join = [], array $order = [])
    {
        $result = $this->db->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->TPJoin($join)
            ->order($order)
            ->query()
            ->result_array();
        return $result;
    }

    /**
     * @desc   添加信息
     * @param 无
     * @date   2018-07-18
     * @param array $data [description]
     * @author lcx
     */
    public function add(array $data, string $table)
    {
        $id = $this->db->insert($table)
            ->set($data)
            ->query()
            ->insert_id();
        return $id;
    }

    /**
     * @desc  更新信息
     * @param 无
     * @date   2018-07-18
     * @param array $where [description]
     * @param array $data [description]
     * @return [type]            [description]
     * @author lcx
     */
    public function save(array $where, array $data)
    {
        $result = $this->db->update($this->table)
            ->set($data)
            ->TPwhere($where)
            ->query()
            ->affected_rows();
        return $result;
    }

    /**
     * @desc  查询一条数据
     * @param 无
     * @date   2018-07-18
     * @param array $where [description]
     * @param string $field [description]
     * @return [type]            [description]
     * @author lcx
     */
    public function getOne(array $where, string $field = "*", array $join = array(),array $order =[])
    {
        $result = $this->db
            ->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->TPJoin($join)
            ->order($order)
            ->query()
            ->row();
        return $result;
    }


    /**
     * showdoc
     * Author: Xuleni
     * DateTime: 2020/11/11 16:38
     * 入参{"id":1,"company_id":18,"u_id":1,"stock_type":1,"remarks":"\u6d4b\u8bd5","operating_num":1,"work_order_id":1,"type":0}
     * @title 更新炉具库存
     */
    public function save_stock(array $data)
    {
        $stove_management_data = $this->getOne(['id' => $data['id']], 'delivery_num,stock_num');
        if (empty($stove_management_data)) return returnResult(-1000, '暂无炉具数据');
        if ($data['stock_type'] == 1) {
            //增加库存
            $save_data = [
                'stock_num' => $stove_management_data['stock_num'] - $data['operating_num'],   //累计库存
                'delivery_num' => $stove_management_data['delivery_num'] + $data['operating_num'] //出库数量
            ];
            $return_msg = $this->save(['id' => $data['id']], $save_data);
        } else {
            //减少库存
            $save_data = [
                'stock_num' => $stove_management_data['stock_num'] + $data['operating_num'],   //累计库存
                'delivery_num' => $stove_management_data['delivery_num'] - $data['operating_num'] //出库数量
            ];
            $return_msg = $this->save(['id' => $data['id']], $save_data);
        }
        if ($return_msg) {
            $stoveManagement_data = [
                'company_id' => $data['company_id'],
                'u_id' => $data['user_id'],
                'stock_type' => $data['stock_type'],
                'remarks' => $data['remarks'],
                'operating_num' => $data['operating_num'],
                'work_order_id' => $data['work_order_id'],
                'type' => $data['type'],
                'id'=>$data['id'],
            ];
            $this->add_stoveManagement($stoveManagement_data);
            return returnResult(1000, '操作成功');
        } else {
            return returnResult(-1000, '操作失败');
        }
    }



    /**
     * Author: Xuleni
     * @title  添加操作日志
     */
    public function add_stoveManagement(array $data)
    {
        $stoveManagement_log = [
            'company_id' => $data['company_id'],
            'u_id' => $data['u_id'],
            'stock_type' => $data['stock_type'],
            'operating_type' => 2,
            'remarks' => $data['remarks'],
            'operating_num' => $data['operating_num'],
            'work_order_id' => $data['work_order_id'],
            'type' => $data['type'],
            'stove_management_id' => $data['id'],
            'create_time' => time()
        ];
        $return = $this->add($stoveManagement_log, 'stove_management_log');
        return $return;
    }

}
