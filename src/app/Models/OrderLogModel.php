<?php
namespace app\Models;
use Server\CoreBase\Model;
/**
 * 订单日志模型
 */
class OrderLogModel extends Model
{
    protected $table = 'orders_log';
    /**
     * @desc   添加信息
     * @param  无
     * @date   2018-07-18
     * @author lcx
     * @param  array      $data [description]
     */
    public function add(array $data){             
        $id = $this->db->insert($this->table)
            ->set($data)
            ->query()
            ->insert_id();   
        return $id;
    }

    /**   YSF
     *    查询列表
     * @param array $where  查询条件
     * @param int $page     当前页
     * @param int $pageSize 每页条数
     * @param array $join   连表
     * @param string $field 查询字段
     * @param array $order  排序方式
     * @param string $dbName 主表
     * @return mixed
     */
    public function getAll(array $where, int $page, int $pageSize, array $join, string $field, array $order, string $dbName)
    {
        if($pageSize<0){
             $result = $this->db->select($field)
                        ->from($dbName)
                        ->TPWhere($where)
                        ->TPJoin($join)
                        ->order($order)
                        ->query()
                        ->result_array();
        }else{
             $result = $this->db->select($field)
                        ->from($dbName)
                        ->TPWhere($where)
                        ->TPJoin($join)
                        ->page($pageSize, $page)
                        ->order($order)
                        ->query()
                        ->result_array();
        }
       
        return $result;
    }


 
}