<?php
namespace app\Models;
use Noodlehaus\Exception;
use Server\CoreBase\Model;
use Server\CoreBase\SwooleException;
/**
 * 工单主板
 */
class WorkEquipmentModel extends Model
{
    protected $table = 'work_equipment';
    /**
     * @desc  查询一条数据
     * @param  无
     * @date   2018-07-24
     * @author lcx
     * @param  array      $where [description]
     * @param  string     $field [description]
     * @return [type]            [description]
     */
    public function getOne(array $where, string $field="*",array $join=array()){
        $result = $this->db
            ->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->TPJoin($join)
            ->query()
            ->row();
        return $result;       
    }
    /**
     * @desc   添加信息
     * @param  无
     * @date   2018-07-24
     * @author lcx
     * @param  array      $data [description]
     */
    public function add(array $data){             
        $id = $this->db->insert($this->table)
            ->set($data)
            ->query()
            ->insert_id();       
        return $id;
    }
        /**
     * @desc   统计数量
     * @param  无
     * @date   2018-07-18
     * @author lcx
     * @param  array      $data [description]
     */
    public function count($map){
         $result = $this->db->select('*')
                ->from($this->table)
                ->TPWhere($map)
                ->query()
                ->num_rows();
         return $result;
    }
    /**
     * @desc  更新信息
     * @param  无
     * @date   2018-07-24
     * @author lcx
     * @param  array      $where [description]
     * @param  array      $data  [description]
     * @return [type]            [description]
     */
    public function save(array $where,array $data){             
        $result = $this->db->update($this->table)
            ->set($data)
            ->TPwhere($where)
            ->query()
            ->affected_rows();               
        return $result;
    }
     /**    lcx
     *     分页查询
     * @param array $where
     * @param string $field
     * @param $join
     * @param array $order
     * @return mixed
     */
    public function getAll(array $where, string $field = '*',array $order = ['create_time' => 'ASC'],array $join=array())
    {
        $result = $this->db->select($field)
                ->from($this->table)
                ->TPWhere($where)
                ->order($order)
                ->TPJoin($join)
                ->query()
                ->result_array();
        return $result;
    }
 
}
