<?php
namespace app\Models;

use Server\CoreBase\Model;

/**  YSF
 *   用户地址表 模型
 *   Date: 2019/4/24
 * Class CustomerAddressModel
 * @package app\Models
 */
class CustomerAddressModel extends Model
{
    // 表名
    protected $name = 'customer_address';

    // 单条查询
    public function getOne(array $where, string $field="*"){
        $result = $this->db->select($field)
                ->from($this->name)
                ->TPWhere($where)
                ->query()
                ->row();
        return $result;
    }









}