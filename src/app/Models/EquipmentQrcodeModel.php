<?php
namespace app\Models;
use Server\CoreBase\Model;

/**   YSF
 *    产品分类 Model
 *    Date: 2018/7/18
 * Class EquipmentModel
 * @package app\Models
 */
class EquipmentQrcodeModel extends Model
{
    // 表名
    protected $dbName = 'equipment_qrcode';

    /**   YSF
     *    主板分类列表--分页
     * @param array $where   查询条件
     * @param int $page      当前页码
     * @param int $pageSize  每页数量
     * @param string $field  查询字段
     * @param array $order   排序方式
     * @return mixed
     */
    public function getAll(array $where, int $page, int $pageSize, string $field = '*', array $order = ['create_time' => 'DESC'],$join=[])
    {
        $result = $this->db->select($field)
            ->from($this->dbName)
            ->TPWhere($where)
            ->TPJoin($join)
            ->page($pageSize, $page)
            ->order($order)
            ->query()
            ->result_array();

        return $result;
    }

    // 获取总数量
    public function getCount(array $where, string $field = '*')
    {
        $result = $this->db->select($field)
            ->from($this->dbName)
            ->TPWhere($where)
            ->query()
            ->num_rows();
        return $result;
    }

    /**   YSF
     *    主板分类详情
     * @param array $where  查询条件
     * @param string $field 查询字段
     * @return null
     */
    public function getDetail(array $where, string $field='*')
    {
        $result = $this->db->select($field)
            ->from($this->dbName)
            ->TPWhere($where)
            ->query()
            ->row();
        return $result;
    }


    /**
     * @desc  更新信息
     * @param  无
     * @date   2018-07-18
     * @author lcx
     * @param  array      $where [description]
     * @param  array      $data  [description]
     * @return [type]            [description]
     */
    public function save(array $where,array $data){
        $result = $this->db->update($this->dbName)
            ->set($data)
            ->TPwhere($where)
            ->query()
            ->affected_rows();
        return $result;
    }

    /**
     * @desc  查询一条数据
     * @param  无
     * @date   2020-2-25
     * @author tx
     * @param  array      $where [description]
     * @param  string     $field [description]
     * @return [type]            [description]
     */
    public function getOne(array $where, string $field="*",array $join=array()){
        $result = $this->db
            ->select($field)
            ->from($this->dbName)
            ->TPWhere($where)
            ->TPJoin($join)
            ->query()
            ->row();
        return $result;
    }

    /**
     * @desc   添加信息
     * @param 无
     * @date   2018-07-24
     * @param array $data [description]
     * @author lcx
     */
    public function add(array $data)
    {
        $id = $this->db->insert($this->dbName)
            ->set($data)
            ->query()
            ->insert_id();
        return $id;
    }

    /**
     * @desc   生成主板二维码
     * @param 无
     * @date   2020/8/25
     * @param array $data [description]
     * @author tx
     */
    public function createQrNo($qrcode,$prefix){
        if(empty($qrcode)){
            $qrcode_no=createQrcodeNo();
        }else{
            if(strlen($qrcode)==2){
                $qrcode_no=createQrcodeNo($qrcode);

            }else if(strlen($qrcode)==8){
                $qrcode_no=$qrcode;
            }
        }
        $qrcode_no=$prefix.$qrcode_no;
        $qr_info=self::getOne(['qrcode_no'=>$qrcode_no],'id');
        if(empty($qr_info)){
            return $qrcode_no;
        }
        return self::createQrNo($qrcode,$prefix);

    }


}