<?php

namespace app\Models;

use Server\CoreBase\Model;

/**
 * 工单-额外服务关联表
 */
class WorkAdditionalServiceModel extends Model {

    // 表名
    protected $name = 'work_addtional_service';

    // 列表获取
    public function getAll(string $field = '*', int $page, int $pageSize, array $order, array $where = array(), array $join = array()) {
        if ($pageSize < 0) {
            $result = $this->db->select($field)
                    ->from($this->name)
                    ->TPWhere($where)
                    ->TPJoin($join)
                    ->order($order)
                    ->query()
                    ->result_array();
        } else {
            $result = $this->db->select($field)
                    ->from($this->name)
                    ->TPWhere($where)
                    ->TPJoin($join)
                    ->page($pageSize, $page)
                    ->order($order)
                    ->query()
                    ->result_array();
        }

        return $result;
    }

    /**
     * @desc  查询一条数据
     * @param  无
     * @date   2018-07-24
     * @author lcx
     * @param  array      $where [description]
     * @param  string     $field [description]
     * @return [type]            [description]
     */
    public function getOne(array $where, string $field = "*") {
        $result = $this->db
                ->select($field)
                ->from($this->name)
                ->TPWhere($where)
                ->query()
                ->row();
        return $result;
    }

    /** lcx 
     * 新增
     * @param array $data 数组,添加的数据
     * @return type
     * @date 20180823
     */
    public function add(array $data) {
        $id = $this->db->insert($this->name)
                ->set($data)
                ->query()
                ->insert_id();
        return $id;
    }
     /**
     * 删除
     * @param array $where 条件
     * @return type
     */
    public function del(array $where){
        $result = $this->db->delete()
            ->from($this->name)
            ->TPwhere($where)
            ->query()
            ->affected_rows();               
        return $result;
    }
}
