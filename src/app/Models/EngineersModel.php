<?php
namespace app\Models;

use Server\CoreBase\Model;

/**   YSF
 *    维修人员  Model
 *    Date: 2018/7/19
 * Class EngineersModel
 * @package app\Models
 */
class EngineersModel extends Model
{
    // 表名
    protected $dbName = 'engineers';

    /**   YSF
     *    获取工程人员所有工单
     * @param array $where    查询条件
     * @param array $join     连表
     * @param int $page       当前页
     * @param int $pageSize   每页条数
     * @param string $field   查询字段
     * @param $order          排序方式
     * @return mixed
     */
    public function getAll(array $where, array $join, int $page, int $pageSize, string $field,$order)
    {
        if($pageSize<0){
            $result = $this->db->select($field)
                ->from($this->dbName)
                ->TPJoin($join)
                ->TPWhere($where)
                ->order($order)
                ->query()
                ->result_array();
        }else{
            $result = $this->db->select($field)
                ->from($this->dbName)
                ->TPJoin($join)
                ->TPWhere($where)
                ->page($pageSize, $page)
                ->order($order)
                ->query()
                ->result_array();
        }


        return $result;
    }

    /**   YSF
     *    查询单条数据
     * @param array $where  查询条件
     * @param string $field 查询字段
     * @return null
     */
    public function getOne(array $where, string $field = '*',array $join=[])
    {
        $result = $this->db->select($field)
                    ->from($this->dbName)
                    ->TPJoin($join)
                    ->TPWhere($where)
                    ->query()
                    ->row();
        return $result;
    }

    /**   YSF
     *    连表查询单条数据
     * @param array $where   查询条件
     * @param array $join    连表操作
     * @param string $field  查询字段
     * @return null
     */
    public function joinOne(array $where, array $join ,string $field = '*')
    {
        $result = $this->db->select($field)
                    ->from($this->dbName)
                    ->TPWhere($where)
                    ->TPJoin($join)
                    ->query()
                    ->row();
        return $result;
    }
    
      /**
     * @desc   添加信息
     * @param  无
     * @date   2018-07-24
     * @author lcx
     * @param  array      $data [description]
     */
    public function add(array $data){             
        $id = $this->db->insert($this->dbName)
            ->set($data)
            ->query()
            ->insert_id();       
        return $id;
    }
    /**
     * @desc  更新信息
     * @param  无
     * @date   2018-07-24
     * @author lcx
     * @param  array      $where [description]
     * @param  array      $data  [description]
     * @return [type]            [description]
     */
    public function save(array $where,array $data){             
        $result = $this->db->update($this->dbName)
            ->set($data)
            ->TPwhere($where)
            ->query()
            ->affected_rows();               
        return $result;
    }


}