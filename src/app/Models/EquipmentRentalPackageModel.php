<?php
namespace app\Models;
use Noodlehaus\Exception;
use Server\CoreBase\Model;
use Server\CoreBase\SwooleException;

class EquipmentRentalPackageModel extends Model
{
    protected $table = 'equipment_rental_package';
    /**
     * @desc  查询一条数据
     * @param  无
     * @date   2018-07-18
     * @author lcx
     * @param  array      $where [description]
     * @param  string     $field [description]
     * @return [type]            [description]
     */
    public function getOne(array $where, string $field="*"){
        $result = $this->db
            ->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->query()
            ->row();
        return $result;
    }

    /**   YSF
     *    主板分类列表--分页
     * @param array $where   查询条件
     * @param int $page      当前页码
     * @param int $pageSize  每页数量
     * @param string $field  查询字段
     * @param array $order   排序方式
     * @return mixed
     */
    public function getAll(array $where, int $page, int $pageSize, string $field = '*', array $order = ['equipments_id' => 'DESC'],array $join)
    {
        $result = $this->db->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->TPJoin($join)
            ->page($pageSize, $page)
            ->order($order)
            ->query()
            ->result_array();

        return $result;
    }




}