<?php
/**
 * Created by PhpStorm.
 * User: LG
 * Date: 2019/1/9
 * Time: 15:02
 */

namespace app\Models;


use Server\CoreBase\Model;

/**
 * 结算公用模型
 * Class AchievementModel
 * @package app\Models
 */
class AchievementModel extends Model
{
    public $table;

    /**
     * 查询单条数据
     * @author ligang
     * @param array $where
     * @param string $field
     * @param array $order
     * @return null
     * @throws \Server\CoreBase\SwooleException
     * @throws \Throwable
     * @date 2019/4/29 15:27
     */
    public function findData(array $where,string $field ='*',array $order = [],array $join = [])
    {
        $result = $this->db
            ->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->TPJoin($join)
            ->order($order)
            ->query()
            ->row();
        return $result;
    }

    /**
     * 插入数据
     * @author ligang
     * @param array $data
     * @return mixed
     * @throws \Server\CoreBase\SwooleException
     * @throws \Throwable
     * @date 2019/1/10 10:15
     */
    public function insertData(array $data)
    {
        $id = $this->db
            ->insert($this->table)
            ->set($data)
            ->query()
            ->insert_id();
        return $id;
    }

    /**
     * 分页
     * @author ligang
     * @param array $where
     * @param string $field
     * @param array $join
     * @param int $row
     * @param int $page
     * @param array $order
     * @return mixed
     * @throws \Server\CoreBase\SwooleException
     * @throws \Throwable
     * @date 2019/1/10 15:08
     */
    public function selectPageData(array $where = [],string $field = '*',array $join = [],int $row = 10,int $page = 1,array $order = [],string $group='')
    {
        $result = $this->db
            ->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->TPJoin($join)
            ->page($row,$page)
            ->order($order)
            ->groupBy($group)
            ->query()
            ->result_array();
        return $result;
    }

    /**
     * 单条关联查询
     * @author ligang
     * @param array $where
     * @param string $field
     * @param array $join
     * @param array $order
     * @return null
     * @throws \Server\CoreBase\SwooleException
     * @throws \Throwable
     * @date 2019/1/10 15:29
     */
    public function findJoinData(array $where = [],string $field = '*',array $join = [],array $order = [])
    {
        $result = $this->db
            ->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->TPJoin($join)
            ->order($order)
            ->query()
            ->row();
        return $result;
    }
    /**
     * 更新数据
     * @author ligang
     * @param array $where
     * @param array $data
     * @return mixed
     * @throws \Server\CoreBase\SwooleException
     * @throws \Throwable
     * @date 2019/1/10 16:36
     */
    public function updateData(array $where,array $data)
    {
        $result = $this->db
            ->update($this->table)
            ->set($data)
            ->TPwhere($where)
            ->query()
            ->affected_rows();
        return $result;
    }

    /**
     * 查询
     * @author ligang
     * @param array $where
     * @param string $field
     * @param array $join
     * @param int $row
     * @param int $page
     * @param array $order
     * @return mixed
     * @throws \Server\CoreBase\SwooleException
     * @throws \Throwable
     * @date 2019/1/10 15:08
     */
    public function selectData(array $where = [],string $field = '*',array $join = [],array $order = [],string $group='')
    {
        if(!empty($group)){
            $result = $this->db
                ->select($field)
                ->from($this->table)
                ->TPWhere($where)
                ->TPJoin($join)
                ->order($order)
                ->groupBy($group)
                ->query()
                ->result_array();
            return $result;
        }else{
            $result = $this->db
                ->select($field)
                ->from($this->table)
                ->TPWhere($where)
                ->TPJoin($join)
                ->order($order)
                ->query()
                ->result_array();
            return $result;
        }

    }

    /**
     * 物理删除数据
     * @author ligang
     * @param array $where
     * @return mixed
     * @throws \Server\CoreBase\SwooleException
     * @throws \Throwable
     * @date 2019/4/19 11:30
     */
    public function deleteData(array $where)
    {
        $result = $this->db
            ->from($this->table)
            ->TPWhere($where)
            ->delete()
            ->query()
            ->affected_rows();
        return $result;
    }

    /**
     * 字段自增
     * @author ligang
     * @param array $where
     * @param string $field
     * @param int $number
     * @return mixed
     * @throws \Server\CoreBase\SwooleException
     * @throws \Throwable
     * @date 2019/6/24 15:16
     */
    public function setInc(array $where,string $field,int $number)
    {
        $result = $this->db
            ->update($this->table)
            ->TPWhere($where)
            ->set($field,$field.'+'.$number,false)
            ->query()
            ->affected_rows();
        return $result;
    }

    /**
     * 字段自减
     * @author ligang
     * @param array $where
     * @param string $field
     * @param int $number
     * @return mixed
     * @throws \Server\CoreBase\SwooleException
     * @throws \Throwable
     * @date 2019/6/24 15:23
     */
    public function setDec(array $where,string $field,int $number)
    {
        $result = $this->db
            ->update($this->table)
            ->TPWhere($where)
            ->set($field,$field.'-'.$number,false)
            ->query()
            ->affected_rows();
        return $result;
    }


    /**
     * 统计数量--新增
     * @author tx
     * @param array $where
     * @param string $field
     * @param int $number
     * @return mixed
     * @throws \Server\CoreBase\SwooleException
     * @throws \Throwable
     * @date 2019/6/24 15:23
     */
    public function countData($where, $join = [])
    {
        $result = $this->db->select('*')
            ->from($this->table)
            ->TPWhere($where)
            ->TPJoin($join)
            ->query()
            ->num_rows();
        return $result;
    }



}