<?php
namespace app\Models;

use Server\CoreBase\Model;

/**  YSF
 *   配件 Model
 *   Date: 2018/7/19
 * Class PartsModel
 * @package app\Models
 */
class PartsModel extends Model
{
    // 表名
    protected $dbName = 'parts';
    protected $workParts = 'work_parts';

    /**   YSF
     *    配件列表
     * @param array $where   查询条件
     * @param int $page      当前页
     * @param int $pageSize  每页条数
     * @param string $field  查询字段
     * @param array $order   排序方式
     * @return mixed
     */
    public function getAll(array $where, int $page, int $pageSize, string $field, array $order)
    {
        $result = $this->db->select($field)
                    ->from($this->dbName)
                    ->TPWhere($where)
                    ->page($pageSize, $page)
                    ->order($order)
                    ->query()
                    ->result_array();
        return $result;
    }

    /**   YSF
     *    工单配件表添加数据
     * @param int $work_order_id  工单ID
     * @param array $partsArr     配件数据  ['配件ID1'=>'数量1','配件ID2'=>'数量2']
     * @return mixed
     */
    public function addWorkParts(int $work_order_id, array $partsArr)
    {
        foreach ($partsArr as $k => $v)
        {
            $data = ['parts_id' => $v['parts_id'], 'parts_number' => $v['parts_number'], 'work_order_id' => $work_order_id];
            $result = $this->db->insert($this->workParts)
                ->set($data)
                ->query()
                ->insert_id();
        }
        return $result;
    }

    // 增加工单配件数量
    public function addWorkPartsNum(int $work_order_id, string $parts_id, string $parts_number,$res)
    {
        // 数量+
        $data['parts_number'] = $parts_number + $res['parts_number'];
        $where['work_order_id'] = $work_order_id;
        $where['parts_id'] = $parts_id;
        $result = $this->db->update($this->workParts)
                ->set($data)
                ->TPwhere($where)
                ->query()
                ->affected_rows();
        return $result;
    }
    // 增加工单配件数据
    public function addWorkPartsData(int $work_order_id, string $parts_id, string $parts_number)
    {
        $data['parts_id'] = $parts_id;
        $data['parts_number'] = $parts_number;
        $data['work_order_id'] = $work_order_id;
        $result = $this->db->insert($this->workParts)
                ->set($data)
                ->query()
                ->insert_id();
        return $result;
    }

    /**   YSF
     *    查询单条配件信息
     * @param array $where   查询条件
     * @param string $field  查询字段
     * @return null
     */
    public function getOne(array $where, string $field = '*')
    {
        $reuslt = $this->db->select($field)
                        ->from($this->dbName)
                        ->TPWhere($where)
                        ->query()
                        ->row();
        return $reuslt;
    }
     /**
     * @desc  更新信息
     * @param  无
     * @date   2018-07-18
     * @author lcx
     * @param  array      $where [description]
     * @param  array      $data  [description]
     * @return [type]            [description]
     */
    public function save(array $where,array $data){             
        $result = $this->db->update($this->dbName)
            ->set($data)
            ->TPwhere($where)
            ->query()
            ->affected_rows();               
        return $result;
    }

    // 连表查询数据
    public function getJoinAll(array $where, string $field, array $join)
    {
        $result = $this->db->select($field)
                    ->from($this->dbName)
                    ->TPWhere($where)
                    ->TPJoin($join)
                    ->query()
                    ->result_array();
        return $result;
    }

    // 获取单条工单配件表数据
    public function getWorkPartsOne(array $where, string $field = '*')
    {
        $reuslt = $this->db->select($field)
            ->from($this->workParts)
            ->TPWhere($where)
            ->query()
            ->row();
        return $reuslt;
    }

}