<?php
namespace app\Models;
use Server\CoreBase\Model;
/**
 * 主板升级日志--主板列表
 */
class EquipmentUpgradeLogEquipment extends Model
{
    protected $table = 'equipment_upgrade_log_equipment';
    /**
     * @desc  查询一条数据
     * @param  无
     * @date   2018-07-18
     * @author lcx
     * @param  array      $where [description]
     * @param  string     $field [description]
     * @return [type]            [description]
     */
    public function getOne(array $where, string $field="*",$order = ['create_time'=>'DESC']){
        $result = $this->db
            ->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->order($order)
            ->query()
            ->row();
        return $result;       
    }
    /**
     * @desc   添加信息
     * @param  无
     * @date   2018-07-18
     * @author lcx
     * @param  array      $data [description]
     */
    public function add(array $data){             
        $id = $this->db->insert($this->table)
            ->set($data)
            ->query()
            ->insert_id();   
        return $id;
    }
    /**
     * @desc  更新信息
     * @param  无
     * @date   2018-07-18
     * @author lcx
     * @param  array      $where [description]
     * @param  array      $data  [description]
     * @return [type]            [description]
     */
    public function save(array $where,array $data){             
        $result = $this->db->update($this->table)
            ->set($data)
            ->TPwhere($where)
            ->query()
            ->affected_rows();               
        return $result;
    }
 
}
 
