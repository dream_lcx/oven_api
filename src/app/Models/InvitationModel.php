<?php
namespace app\Models;
use Server\CoreBase\Model;
/**
 * 邀请好友记录
 */
class InvitationModel extends Model
{
    protected $table = 'invitation_record';
    protected $table_a = 'invitation_record a';
    /**
     * @desc  查询一条数据
     * @param  无
     * @date   2018-07-18
     * @author lcx
     * @param  array      $where [description]g
     * @param  string     $field [description]
     * @return [type]            [description]
     */
    public function getOne(array $where, string $field="*"){
        $result = $this->db
            ->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->query()
            ->row();
        return $result;
    }
    /**
     * @desc   添加信息
     * @param  无
     * @date   2018-07-18
     * @author lcx
     * @param  array      $data [description]
     */
    public function add(array $data){
        $id = $this->db->insert($this->table)
            ->set($data)
            ->query()
            ->insert_id();
        return $id;
    }
    /**
     * @desc  更新信息
     * @param  无
     * @date   2018-07-18
     * @author lcx
     * @param  array      $where [description]
     * @param  array      $data  [description]
     * @return [type]            [description]
     */
    public function save(array $where,array $data){
        $result = $this->db->update($this->table)
            ->set($data)
            ->TPwhere($where)
            ->query()
            ->affected_rows();
        return $result;
    }
    // 计数
    public function getJoinCount(array $map=[],array $join=[])
    {
        $result = $this->db->select('*')
            ->from($this->table_a)
            ->TPWhere($map)
            ->TPJoin($join)
            ->query()
            ->num_rows();
        return $result;
    }

    // 连表列表查询
    public function getJoinAll(array $where=[],array $join=[],string $field='*',int $page=1,int $pageSize=-1,array $order=[])
    {
        if($pageSize>0){
            $value = $this->db->select($field)
                ->from($this->table_a)
                ->TPWhere($where)
                ->TPJoin($join)
                ->page($pageSize, $page)
                ->order($order)
                ->query()
                ->result_array();
        }else{
            $value = $this->db->select($field)
                ->from($this->table_a)
                ->TPWhere($where)
                ->TPJoin($join)
                ->order($order)
                ->query()
                ->result_array();
        }

        return $value;
    }

    /**
     * @desc   添加邀请好友记录或者修改状态
     * @param 无
     * @date   2018-07-18
     * @author lcx
     */
    public function addInviteRecord($user_id, $pid, $openid, $status = 1, $way = 0)
    {
        $data['user_id'] = $user_id;
        $data['pid'] = $pid;
        $data['create_time'] = time();
        $data['user_openid'] = $openid;
        $data['status'] = $status;
        $data['way'] = $way;
        //判断是否已经存在该条记录
        $map['user_openid'] = $openid;
        $map['pid'] = $pid;
        $info = $this->getOne($map, 'status,invite_id');
        $res = false;
        if ($status == 2) {
            $data['update_time'] = time();
            $res = $this->save(array('openid' => $openid), array('source' => $pid, 'source_is_valid' => 2));
        }

        if (empty($info)) {
            $res = $this->add($data);
        } else if ($info['status'] != $status && $info['status'] == 1) {
            $res = $this->save(array('invite_id' => $info['invite_id']), array('status' => $status, 'update_time' => time(), 'user_id' => $user_id));
        } else {
            $res = true;
        }
        return $res;
    }


    /**
     * @desc   统计数量
     * @param  无
     * @date   2018-12-6
     * @author lcx
     * @param  array      $data [description]
     */
    public function count($map, $join=[]) {
        $result = $this->db->select('*')
            ->from($this->table)
            ->TPWhere($map)
            ->TPJoin($join)
            ->query()
            ->num_rows();
        return $result;
    }



}