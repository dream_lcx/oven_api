<?php
namespace app\Models;
use Server\CoreBase\Model;
/**
 * 工单-业务类型关联
 */
class WorkOrderBusinessModel extends Model
{
    protected $table = 'work_order_business';
    /** lcx 
     * 新增
     * @param array $data 数组,添加的数据
     * @return type
     * @date 20180823
     */
    public function add(array $data){             
        $id = $this->db->insert($this->table)
            ->set($data)
            ->query()
            ->insert_id();   
        return $id;
    }

    /**   lcx
     *    查询列表
     * @param array $where    查询条件
     * @param array $join     连表
     * @param string $field   查询字段
     * @param array $order    排序方式
     * @param string $dbName  主表
     * @return mixed
     * @date 20180823
     */
    public function getAll(array $where,array $join, string $field, array $order= ['work_order_business_id' => 'DESC'])
    {
        $result = $this->db->select($field)
                    ->from($this->table)
                    ->TPWhere($where)
                    ->TPJoin($join)
                    ->order($order)
                    ->query()
                    ->result_array();
        return $result;
    }
       /**
     * @desc  查询一条数据
     * @param  无
     * @date   2018-07-24
     * @author lcx
     * @param  array      $where [description]
     * @param  string     $field [description]
     * @return [type]            [description]
     */
    public function getOne(array $where, string $field="*"){
        $result = $this->db
            ->select($field)
            ->from($this->table)
            ->TPWhere($where)
            ->query()
            ->row();
        return $result;       
    }
     /**
     * @desc  更新信息
     * @param  无
     * @date   2018-07-24
     * @author lcx
     * @param  array      $where [description]
     * @param  array      $data  [description]
     * @return [type]            [description]
     */
    public function save(array $where,array $data){             
        $result = $this->db->update($this->table)
            ->set($data)
            ->TPwhere($where)
            ->query()
            ->affected_rows();               
        return $result;
    }
    /**
     * 删除
     * @param array $where 条件
     * @return type
     */
    public function del(array $where){
        $result = $this->db->delete()
            ->from($this->table)
            ->TPwhere($where)
            ->query()
            ->affected_rows();               
        return $result;
    }
    
 
 
}