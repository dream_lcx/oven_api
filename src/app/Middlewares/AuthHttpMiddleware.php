<?php

/**
 * Created by PhpStorm.
 * User: bg
 * Date: 2018/3/12 0012
 * Time:      11:04
 */

namespace app\Middlewares;

use Server\Components\Middleware\HttpMiddleware;
use Firebase\JWT\JWT;

/*
 * 自定义中间件处理是否授权
 */

class AuthHttpMiddleware extends HttpMiddleware {

    public function __construct() {
        parent::__construct();
    }

    private $module;
    private $controller;
    private $action;

    public function before_handle() {
        $this->request->user_id = '';
		$this->request->client = $this->request->header['client'] ?? '';//终端 1微信小程序 2支付宝小程序 3百度小程序
		$this->request->company = $this->request->header['company'] ?? '';//公司ID,1重庆润泉
		$this->request->role = $this->request->header['role'] ?? '';//1总后台 2运营 3行政 4用户 5工程 6市场推广1
        if (!$this->filter()) {
            $token = $this->request->header['token'] ?? '';
            if (empty($token)) {
                return $this->jsonend(-1005, 'token为空');
            }
            $key = $this->config['token_key'];
            try {
                $user_info = JWT::decode($token, $key, array('HS256'));
            } catch (\UnexpectedValueException $ex) {
                return $this->jsonend(-1005, '用户信息错误1',$ex->getMessage());
            }
            if (empty($user_info)) {
                return $this->jsonend(-1005, '用户信息错误2');
            }
            if ($user_info->expire < time()) {
                return $this->jsonend(-1005, '用户信息错误3');
            }
//            if (empty($user_info->tel ?? '')) {
//                return $this->jsonend(-1004, '未登录');
//            }
            if (isset($user_info->username)){
                $this->request->username = $user_info->username;
            }
            //保存用户信息
            if (!empty($user_info->role) && $user_info->role==5) {
                $this->request->user_id = $user_info->engineer_id;
            } else {
                $this->request->user_id = $user_info->user_id;
            }

        }
    }

    public function after_handle($path) {
        //$this->jsonend(-1000,'请求错误~');
        // TODO: Implement after_handle() method.
    }

    //验证需要登录 的接口
    private function filter() {
        $route = explode('/', $this->request->server['path_info']);
        $count = count($route);
        if ($count == 4) {
            $this->action = $route[$count - 1] ?? null;
            $this->controller = $route[$count - 2] ?? null;
            $this->module = $route[$count - 3] ?? null;
        } else {
            return true;
        }
        //不需要验证的板块
        if ($this->module == 'Common' || $this->module == 'House'||$this->module == 'Boss') {
            return true;
        }

        $interceptor = $this->config->get('custom.interceptor');
        if (key_exists($this->module, $interceptor) && key_exists($this->controller, $interceptor[$this->module]) && in_array($this->action, $interceptor[$this->module][$this->controller])) {
            return true;
        } else {
            return false;
        }
    }

    private function jsonend($code = '', $message = '', $data = '') {
        $output = array('code' => $code, 'message' => $message, 'data' => $data);
        if (is_array($output) || is_object($output)) {
            $this->response->header('Content-Type', 'textml; charset=UTF-8');
            $this->response->header('Access-Control-Allow-Origin', '*');
            $this->response->header("Access-Control-Allow-Headers", " Origin, X-Requested-With, Content-Type, Accept,token,company");
            $this->response->header("Access-Control-Allow-Methods", " POST,OPTIONS");
            $this->response->header('Content-Type', 'application/json;charset=UTF-8');
            $output = json_encode($output, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            //$output = "<pre>$output</pre>";
        }
        $this->response->end($output);
        $this->interrupt();
    }

}
