<?php

/**
 * Created by PhpStorm.
 * User: soosoogoo
 * Date: 2017/11/29
 * Time: 22:06
 */

namespace app\Library;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use app\Library\Factory as LF;

class SLog {

    static $instance;
    static $STHinstance;

    const DEBUG_LOG = 'Debug';
    const ERROR_LOG = 'error';
    const LOG_SUFFIX = '.log';
    const CARD_LOG = 'card';
    const DEVICE_LOG = 'device';
    const CONTRACT_LOG = 'contract';  //合同续费失败
    const DEVICE_HEARTBEAT_LOG = 'device_heartbeat'; //心跳
    const DEVICE_DATA_SYNC = 'device_data_sync'; //数据同步
    const DEVICE_OFFLINE_LOG = 'device_offline'; //离线
    const DEVICE_WORK_STATE_LOG = 'device_work_state'; //工作状态
    const DEVICE_FILTER_ELEMENT_LOG = 'device_filter_element'; //滤芯
    const DEVICE_WITH_WATER_LOG = 'device_with_water'; //用水量
    const DEVICE_TIME_SYNC_LOG = 'device_time_sync'; //时间同步
    const FACE_API_LOG = 'face_api'; //人脸识别
    const PLAN_TO_MONITOR = 'plan_to_monitor'; //主板下发套餐监控
    const DEVICE_SYNC_REDIS = 'device_sync_redis'; // 监控主板和redis同步

    public static function logConf() {
        return array(
            self::CONTRACT_LOG => array(LOG_PATH . '/contract' . DS, Logger::INFO),
            self::CARD_LOG => array(LOG_PATH . '/water/card' . DS, Logger::INFO),
            self::DEVICE_LOG => array(LOG_PATH . '/device' . DS, Logger::INFO),
            self::DEVICE_HEARTBEAT_LOG => array(LOG_PATH . '/device/heartbeat' . DS, Logger::INFO),
            self::PLAN_TO_MONITOR => array(LOG_PATH . '/device/plan_to_monitor' . DS, Logger::INFO),
            self::DEVICE_DATA_SYNC => array(LOG_PATH . '/device/data_sync' . DS, Logger::INFO),
            self::DEVICE_OFFLINE_LOG => array(LOG_PATH . '/device/offline' . DS, Logger::INFO),
            self::DEVICE_WORK_STATE_LOG => array(LOG_PATH . '/device/work_state' . DS, Logger::INFO),
            self::DEVICE_FILTER_ELEMENT_LOG => array(LOG_PATH . '/device/filter_element' . DS, Logger::INFO),
            self::DEVICE_WITH_WATER_LOG => array(LOG_PATH . '/device/with_water' . DS, Logger::INFO),
            self::DEVICE_TIME_SYNC_LOG => array(LOG_PATH . '/device/time_sync' . DS, Logger::INFO),
            self::DEVICE_SYNC_REDIS => array(LOG_PATH . '/device/device_sync_redis' . DS, Logger::INFO),
            self::DEBUG_LOG => array(LOG_PATH . '/debug/' . DS, Logger::INFO),
            self::FACE_API_LOG => array(LOG_PATH . '/debug/' . DS, Logger::INFO),
            self::ERROR_LOG => array(LOG_PATH . '/error/'. DS, Logger::INFO),
        );
    }

    public static function getSTH($path, $logType) {
        //StreamHandler($path.$index.self::LOG_SUFFIX, $logType)
        if (!isset(self::$STHinstance[$path])) {
            self::$STHinstance[$path] = new StreamHandler($path, $logType);
        }
        return self::$STHinstance[$path];
    }

    /**
     * static pool
     * @return object
     */
    public static function SL($type, $index = 'default') {
        $c = $type . $index;
        if (!isset(self::$instance[$c])) {
            list($path, $logType) = self::logConf()[$type];

            //此处简单处理,日志可以写往不同通道,分发处理,可以写进程监听等等
            //详情见https://github.com/Seldaek/monolog  文档
            $log = new Logger($type);
            //设置日志格式
            $stream_handler = self::getSTH($path . $index . self::LOG_SUFFIX, $logType);
            //$stream_handler->setFormatter(LF::getInstance('\\app\\Library\\CliFormatter'));
            $log->pushHandler($stream_handler);

            self::$instance[$c] = $log;
        }
        return self::$instance[$c];
    }

    /**
     * HTTP日志
     * static pool
     * @return object
     */
    public static function SLT($type = self::DEBUG_LOG, $method = false) {
        $method = $method ?? ucfirst(Request::getCtrl()) . '-' . Request::getMethod();
        if (!isset(self::$instance[$method])) {
            list($path, $logType) = self::logConf()[self::DEBUG_LOG];

            //此处简单处理,日志可以写往不同通道,分发处理,可以写进程监听等等
            //详情见https://github.com/Seldaek/monolog  文档
            $log = new Logger($type);
            //设置日志格式
            $stream_handler = self::getSTH($path . $method . self::LOG_SUFFIX, $logType);
            $stream_handler->setFormatter(LF::getInstance('\\Library\\CliFormatter'));
            $log->pushHandler($stream_handler);

            self::$instance[$method] = $log;
        }
        return self::$instance[$method];
    }

}
