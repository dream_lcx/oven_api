<?php
/**
 * User: Administrator
 * Date: 2017/2/8
 */
header("Content-type: text/html; charset=utf-8"); //设置输出编码格式

echo 'phpversion ', phpversion(), '\r\n<br/>';

//自动捕获异常
set_exception_handler(function ($e) {
    file_put_contents(
        __DIR__ . '/exception.log',
        date('Y-m-d H:i:s') . ' - ' . $e . PHP_EOL . PHP_EOL,
        FILE_APPEND
    );
});

include("../API/eSignOpenAPI.php");


use tech\core\eSign;
use tech\constants\PersonArea;
use tech\constants\PersonTemplateType;
use tech\constants\OrganizeTemplateType;
use tech\constants\SealColor;
use tech\constants\UserType;
use tech\constants\OrganRegType;
use tech\constants\SignType;
use tech\constants\LicenseType;
use tech\core\Util;

//实例化
$esign = null;
try {
    $esign = new eSign();
} catch (Exception $e) {
    echo $e->getMessage();
    exit;
}

//短信验证码 发送到指定手机
/*$res = $esign->sendSignCodeToMobile('DDF39879590F407BA027D8834634800F', '13588366603');
var_dump($res);
exit;*/

//平台用户签
userSignPDF();

//批量签署
//userMultiSignPDF();

//平台自身签署
//selfSignPDF();


//短信批量签署
function userMultiSignPDF()
{
    global $esign;
    $accountId = 'DDF39879590F407BA027D8834634800F';

    //通过调用印章模板接口、获取印章数据
    //$seal = addOrgTemplateSeal($accountId);
    //$sealData = $seal['imageBase64'];

    //直接获取印章图片
    $sealImg = file_get_contents('3.png');
    $sealData = base64_encode($sealImg);

    //$sealData = '' //允许印章图片为空

    //待签署文档1
    $fileBean1 = array(
        'srcPdfFile' => 'E:\pdf\13958ddc26324a3492f9264f351bde93.pdf',
        'dstPdfFile' => 'E:\pdf\13958ddc26324a3492f9264f351bde93-dst.pdf',
        'fileName' => '',
        'ownerPassword' => ''
    );
    //待签署文档2
    $fileBean2 = array(
        'srcPdfFile' => 'E:\测试.pdf',
        'dstPdfFile' => 'E:\测试-dst.pdf',
        'fileName' => '',
        'ownerPassword' => ''
    );
    //批量签署参数拼装
    $signParams = array(
        0 => array(
            'signType' => SignType::SINGLE,
            'fileBean' => $fileBean1,
            'signPos' => array(
                'posPage' => 1,
                'posX' => 100,
                'posY' => 100,
                'key' => '',
                'width' => ''
            )
        ),
        1 => array(
            'signType' => SignType::KEYWORD,
            'fileBean' => $fileBean2,
            'signPos' => array(
                'posPage' => 0,
                'posX' => 0,
                'posY' => 0,
                'key' => 'DESCRIPTION',
                'width' => ''
            )
        )
    );
    //调用 8.1发送签署短信验证码 $esign->sendSignCode($accountId)， $mobile 为空
    //调用 8.2发送签署短信验证码（指定手机号）$esign->sendSignCodeToMobile($accountId,$mobile)， $mobile 不能为空
    $mobile = '13588366603';
    $code = '126117';
    $res = $esign->userMutilSignPDF($accountId, $signParams, $sealData, $mobile, $code);
    print_r(Util::jsonEncode($res));
}

function userSignPDF()
{
    global $esign;

    //通过调用印章模板、获取印章数据
    //$seal = addOrgTemplateSeal();
    //$sealData = $seal['imageBase64'];

    //通过印章图片，获取
    $sealImg = file_get_contents('3.png');
    $sealData = base64_encode($sealImg);
    //$sealData = '' //允许印章图片为空

    //$sealData = addPersonalTemplateSealLocal();

    $accountId = '340B605B8ED5440685530E1A1D40794B';
    $signType = SignType::SINGLE;
    $signPos = array(
        'posPage' => 12,
        'posX' => 200,
        'posY' => 0,
        'key' => '',
        'width' => 159,
        'cacellingSign' => true,//是否二维码签署
        'addSignTime' => true,//是否显示签署时间
    );
    $signFile = array(
        'srcPdfFile' => 'E:\pdf\13958ddc26324a3492f9264f351bde93.pdf',
        'dstPdfFile' => 'E:\pdf\13958ddc26324a3492f9264f351bde93-dst.pdf',
        'fileName' => '',
        'ownerPassword' => ''
    );
    //$mobile = '13588366603';
    //$code = '358237';
    //$res = $esign->userSafeMobileSignPDF($accountId, $signFile, $signPos, $signType, $sealData,$mobile, $code, $stream = false);
    $res = $esign->userSignPDF($accountId, $signFile, $signPos, $signType, $sealData, $stream = false);
    var_dump($res);
    //$esign->selfSignPDF();
}

//平台自身签署
function selfSignPDF()
{
    global $esign;

    $sealId = 0;
    $signType = SignType::KEYWORD;
    $signPos = array(
        'posPage' => 1,
        'posX' =>  100,
        'posY' => '',
        'key' =>  '甲方（盖章）',
        'width' => '120',
        'cacellingSign' => true,
        'addSignTime' => true,
    );
    $signFile = array(
        'srcPdfFile' => 'E:\pdf\13958ddc26324a3492f9264f351bde93.pdf',
        'dstPdfFile' => 'E:\pdf\13958ddc26324a3492f9264f351bde93.pdf-final.pdf',
        'fileName' => '13958ddc26324a3492f9264f351bde93.pdf',
        'ownerPassword' => ''
    );
    $res = $esign->selfSignPDF($signFile, $signPos, $sealId, $signType, $stream = true);
    var_dump($res);
    //$esign->selfSignPDF();
}

function addOrgTemplateSeal($accountId)
{
    global $esign;

    $ret = $esign->addTemplateSeal(
        $accountId,
        $templateType = OrganizeTemplateType::OVAL,
        $color = SealColor::RED,
        $hText = '合同专用',
        $qText = '测试章'
    );
    print_r($ret);
    return $ret;
}


function addOrganizeTemplateSealLocal()
{
    global $esign;

    $text = '企业印章测试';
    $ret = $esign->addOrganizeSealLocal(
        $text,
        $templateType = strtoupper(OrganizeTemplateType::STAR),
        $color = strtoupper(SealColor::BLUE),
        $hText = '合同专用',
        $qText = '测试章'
    );
    print_r($ret);
    return $ret;
}

function addPersonalTemplateSealLocal()
{
    global $esign;

    $text = '测试';
    $ret = $esign->addPersonAccount(
        $text,
        $templateType = PersonTemplateType::HWLS,
        $color = SealColor::BLUE
    );
    print_r($ret);
    return $ret['sealData'];
}

