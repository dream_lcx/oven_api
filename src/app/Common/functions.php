<?php

use app\Services\Common\ReturnCodeService;


/**
 * 浏览器友好的变量输出
 * @param mixed $var 变量
 * @param boolean $echo 是否输出 默认为true 如果为false 则返回输出字符串
 * @param string $label 标签 默认为空
 * @plaram integer       $flags htmlspecialchars flags
 * @return void|string
 */
function dump($var, $echo = true, $label = null, $flags = ENT_SUBSTITUTE)
{
    $label = (null === $label) ? '' : rtrim($label) . ':';
    ob_start();
    var_dump($var);
    $output = ob_get_clean();
    $output = preg_replace('/\]\=\>\n(\s+)/m', '] => ', $output);
    $output = $label . $output;
    if ($echo) {
        echo $output;
        return;
    } else {
        return $output;
    }
}

/**
 * 处理钱的格式 元转成分 ，分转成元
 * 元转分 formatMoney（$money,1）
 * 分转成元 formatMoney（$money,2)
 * @param int $money 钱
 * @param int $unit 单位1 是元 2是分
 * @param int $point 小数点位数
 */
function formatMoney($money, $unit = 2, $point = 2)
{
    if (empty($money)) {
        return 0;
    }
    if ($unit == 1) {//将元转成分
        $money = intval($money * 100);
    } else {
        $reg = '/^[0-9][0-9]*$/';
        $money = $money / 100; //将分转成元
        if ($point == 0 || preg_match($reg, $money)) {
            $money = (int)$money;
        } else {
            $money = sprintf('%.' . $point . 'f', $money);
        }
    }
    return $money;
}

///**
// * 处理钱的格式
// * @param int $money 钱 单位分
// * @param int $unit 单位1 是元 2是分
// * @param int $point 小数点位数
// */
//function formatMoney($money, $unit = 2, $point = 0) {
//    if ($unit == 1) {
//        $money = $money / 100;
//    }
//    if ($point == 0) {
//        $money = (int) $money;
//    } else {
//        $money = number_format($money, $point);
//    }
//    return $money;
//}
// 日期转换
function dateFormat($time)
{
    return date('Y-m-d H:i:s', $time);
}

// 日期转换1
function dateFormats($time)
{
    return date('Y-m-d', $time);
}

/**
 * @desc   根据经纬度计算距离
 * @param 无
 * @date   2018-07-17
 * @param  [type]     $lat1 纬度1
 * @param  [type]     $lng1 经度1
 * @param  [type]     $lat2 纬度2
 * @param  [type]     $lng2 经度2
 * @return [type]           [description]
 * @author lcx
 */
function getDistance($lat1, $lng1, $lat2, $lng2)
{
    if (empty($lat1) || empty($lng1) || empty($lat2) || empty($lng2)) {
        return 0;
    }
//    $earthRadius = 6367000; //approximate radius of earth in meters 
//
//    $lat1 = ($lat1 * pi() ) / 180;
//    $lng1 = ($lng1 * pi() ) / 180;
//
//    $lat2 = ($lat2 * pi() ) / 180;
//    $lng2 = ($lng2 * pi() ) / 180;
//
//    $calcLongitude = $lng2 - $lng1;
//    $calcLatitude = $lat2 - $lat1;
//    $stepOne = pow(sin($calcLatitude / 2), 2) + cos($lat1) * cos($lat2) * pow(sin($calcLongitude / 2), 2);
//    $stepTwo = 2 * asin(min(1, sqrt($stepOne)));
//    $calculatedDistance = $earthRadius * $stepTwo;
//    return round($calculatedDistance);
    $EARTH_RADIUS = 6378.137;

    $radLat1 = rad($lat1);
    $radLat2 = rad($lat2);
    $a = $radLat1 - $radLat2;
    $b = rad($lng1) - rad($lng2);
    $s = 2 * asin(sqrt(pow(sin($a / 2), 2) + cos($radLat1) * cos($radLat2) * pow(sin($b / 2), 2)));
    $s = $s * $EARTH_RADIUS;
    $s = round($s * 10000) / 10000;

    return $s * 1000;
}

function rad($d)
{
    return $d * M_PI / 180.0;
}

/* * **************************************二维数组操作************************************ */

//二维数组分页
function arrPage($arr, $page, $indexinpage)
{
    $page = is_int($page) != 0 ? $page : 1; //当前页数
    $indexinpage = is_int($indexinpage) != 0 ? $indexinpage : 5; //每页显示几条
    $newarr = array_slice($arr, ($page - 1) * $indexinpage, $indexinpage);
    return $newarr;
}

//二维数组去掉重复值
function array_unique_fb($array2D)
{
    foreach ($array2D as $v) {
        $v = join(',', $v); //降维,也可以用implode,将一维数组转换为用逗号连接的字符串
        $temp[] = $v;
    }
    $temp = array_unique($temp); //去掉重复的字符串,也就是重复的一维数组
    foreach ($temp as $k => $v) {
        $temp[$k] = explode(',', $v); //再将拆开的数组重新组装
    }
    return $temp;
}

//二维数组查找某一个值，并返回
function searchArray($array, $key1, $value1, $key2 = "", $value2 = "")
{
    $arr = array();
    foreach ($array as $keyp => $valuep) {
        if (empty($key2) || empty($value2)) {
            if ($valuep[$key1] == $value1) {
                array_push($arr, $valuep);
            }
        } else if (empty($key1) || empty($value1)) {
            if ($valuep[$key2] == $value2) {
                array_push($arr, $valuep);
            }
        } else {
            if ($valuep[$key1] == $value1 && $valuep[$key2] == $value2) {
                array_push($arr, $valuep);
            }
        }
    }
    return $arr;
}

//二维数组查找key1字段，并返回key2字段的值
function searchArray1($array, $key1, $value1, $key2)
{
    foreach ($array as $k => $v) {
        if (!empty($key1) && !empty($value1)) {
            if ($v[$key1] == $value1) {
                return $v[$key2];
            }
        }
    }
}

/**
 * 根据指定字段排序二维数组，保留原有键值(降序)
 * @param $arr @输入二维数组
 * @param $var @要排序的字段名
 * return array
 */
function mymArrsort($arr, $var, $sort = 'DESC')
{
    $tmp = array();
    $rst = array();
    foreach ($arr as $key => $trim) {
        $tmp[$key] = $trim[$var];
    }
    if (strtolower($sort) == 'asc') {
        asort($tmp); //升序
    } else {
        arsort($tmp); //降序
    }

    $i = 0;
    foreach ($tmp as $key1 => $trim1) {
        $rst[$i] = $arr[$key1];
        $i++;
    }
    return $rst;
}

/**
 * @desc从数组随机取几条数据
 * @param $data array数组
 * @param $limit Int条数
 * @return $arr array随机取出来的数据重组的数组
 */
function getRandInArray($data, $limit)
{
    // 如果查询条数小于总条数，随机取出查询条数
    if (count($data) > $limit) {
        $temp = array_rand($data, $limit);
        // 如果$this->parm['num']==1,返回$temp非数组，故此处需要判断处理
        if (is_array($temp)) {
            // 重组数组
            foreach ($temp as $val) {
                $arr[] = $data[$val];
            }
        } else {
            $arr[] = $data[$temp];
        }
    } else {
        $arr = $data;
    }
    return $arr;
}

//获取二维数组中某一列的值集合,PHP版本大于5.5
function getArrayColumn($arr, $field)
{
    return array_column($arr, $field);
}

//两个数组的差集,返回出现在第一个数组$arr1中但其他输入数组$arr2中没有的值
function getArrayDiff($arr1, $arr2)
{
    return array_diff($arr1, $arr2);
}

/* * ********************************其他************************* */

/**
 * 传递参数 生成编号
 * @param string $head
 * @return string
 */
function pay_sn($head)
{
    return $head . date('Ymd') . substr(implode(NULL, array_map('ord', str_split(substr(uniqid(), 7, 13), 1))), 0, 8);
    // return strtoupper($head . dechex(date('m'))) . date('d') . substr(time(), - 5) . substr(microtime(), 2, 5) . sprintf('%02d', rand(0, 99));
}

/**
 * 密码加密
 * @param String $password
 * @param String $salt
 * @return string
 */
function toPassword($password, $salt = "USER123456")
{
    $password_code = md5(md5($password) . $salt);
    return $password_code;
}

/* * ******************************************第三方接口************************ */

/**
 * 手机归属地查询
 * @param string $phone
 * @return array|string
 */
function attribution($phone)
{
    $url = "http://apis.juhe.cn/mobile/get?phone=" . $phone . "&key=d87d7e35d6ffc35dfb0fc4ac7ae12d9a";
    $result = file_get_contents($url);
    $result = json_decode($result, true);
    return $result['result']['province'];
}

/* * ******************************************特殊验证*********************************** */

/**
 * 验证手机号是否正确
 *
 * @param INT $mobile
 * @return bool
 * @author
 *
 */
function isMobile($mobile)
{
    return preg_match('#^1[3-9][\d]{9}$#', $mobile) ? true : false;
}


/**
 * 同时验证移动号和座机号
 *
 * @param INT $mobile
 * @author
 *
 */
function isPhone($phone)
{
    return preg_match('/((\d{11})|^((\d{7,8})|(\d{4}|\d{3})-(\d{7,8})|(\d{4}|\d{3})-(\d{7,8})-(\d{4}|\d{3}|\d{2}|\d{1})|(\d{7,8})-(\d{4}|\d{3}|\d{2}|\d{1}))$)/', $phone) ? true : false;
}


/**
 * 验证邮箱
 * @param String $email
 * @return bool
 */
function isEmail($email)
{
    $pattern = "/^([0-9A-Za-z\\-_\\.]+)@([0-9a-z]+\\.[a-z]{2,3}(\\.[a-z]{2})?)$/i";
    if (preg_match($pattern, $email)) {
        return true;
    }
    return false;
}

// 判断是否是微信浏览器
function isWeixin()
{
    if (strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false) {
        return true;
    } else {
        return false;
    }
}

/**
 * 判断身份证号码格式
 * @param string $SFZ 身份证号码
 * @return bool
 */
function FunIsSFZ($SFZ)
{
    $Run = @preg_match('/^(\d{15}$|^\d{18}$|^\d{17}(\d|X|x))$/', $SFZ);
    return $Run;
}

/**
 * 判断URL是否已经包含协议
 * @param String $url
 */
function urlIsAll($url)
{
    $str = "http://";
    $strs = "https://";
    if (strpos($url, $str) === false && strpos($url, $strs) === false) {
        return false;
    }
    return true;
}

/**
 * @Author   lcx
 * @DateTime 2018-03-29
 * @desc 判断是否是数字，字母
 * @param
 * @param    [type]     $image_file [description]
 * @return   [type]                 [description]
 */
function isNumberOrLetter($variable)
{
    return preg_match('/^[A-Za-z0-9]+$/', $variable) ? true : false;
}

/**
 * 验证密码是否正确
 * $input 用户输入的密码 123456
 * $pwd 用户设置的密码 加密字符串
 * $strict 盐
 */
function checkPwd($input, $pwd, $strict)
{
    $new_pwd = md5(md5($input) . $strict);
    if ($new_pwd == $pwd) {
        return true;
    }
    return false;
}

/**
 * 字符串截取，支持中文和其他编码
 */
function msubstr($str, $start = 0, $length, $charset = "utf-8", $suffix = true)
{
    if (function_exists("mb_substr"))
        $slice = mb_substr($str, $start, $length, $charset);
    elseif (function_exists('iconv_substr')) {
        $slice = iconv_substr($str, $start, $length, $charset);
        if (false === $slice) {
            $slice = '';
        }
    } else {
        $re['utf-8'] = "/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|[\xe0-\xef][\x80-\xbf]{2}|[\xf0-\xff][\x80-\xbf]{3}/";
        $re['gb2312'] = "/[\x01-\x7f]|[\xb0-\xf7][\xa0-\xfe]/";
        $re['gbk'] = "/[\x01-\x7f]|[\x81-\xfe][\x40-\xfe]/";
        $re['big5'] = "/[\x01-\x7f]|[\x81-\xfe]([\x40-\x7e]|\xa1-\xfe])/";
        preg_match_all($re[$charset], $str, $match);
        $slice = join("", array_slice($match[0], $start, $length));
    }
    return $suffix ? $slice . '...' : $slice;
}

/**
 * @Author   lcx
 * @DateTime 2018-03-29
 * @desc 对图片进行base64解码输出
 * @param
 * @param    [type]     $image_file [description]
 * @return   [type]                 [description]
 */
function base64EncodeImage($ImageFile)
{
    if (file_exists($ImageFile) || is_file($ImageFile)) {
        $base64_image = '';
        $image_info = getimagesize($ImageFile);
        $image_data = fread(fopen($ImageFile, 'r'), filesize($ImageFile));
        $base64_image = 'data:' . $image_info['mime'] . ';base64,' . chunk_split(base64_encode($image_data));
        return $base64_image;
    } else {
        return false;
    }
}

function convertBaseimg($base, $img_file = '')
{
    $date = date('Ymd') . '/';
    file_exists(WWW_DIR . '/user_spread_qrcode/') || mkdir(WWW_DIR . '/user_spread_qrcode/', 0777);
    $img_path = WWW_DIR . '/user_spread_qrcode/' . $date;
    file_exists($img_path) || mkdir($img_path, 0777);
    if (empty($img_file)) {
        $img_file = uniqid() . mt_rand(100000, 999999) . '.jpg';
    }
    // 图片数据
    $base = str_replace('data:image/png;base64,', '', $base);
    $base = str_replace('data:image/jpeg;base64,', '', $base);
    $base = str_replace('data:image/jpg;base64,', '', $base);
    $base = str_replace('data:image/gif;base64,', '', $base);
    $base = str_replace(' ', '+', $base);
    $data = base64_decode($base);
    // 保存图片
    $success = file_put_contents($img_path . $img_file, $data);
    // 图片路径
    // $url = "http://" . $_SERVER['HTTP_HOST'] . str_replace('index.php', '', $_SERVER['SCRIPT_NAME']) . substr($img_path, 2) . $img_file;
    $url = $date . $img_file;
    return $url;
}

/* * *******************************过滤函数****************************** */

/**
 * 返回经addslashes处理过的字符串或数组
 * @param $string 需要处理的字符串或数组
 * @return mixed
 */
function new_addslashes($string)
{
    if (!is_array($string))
        return addslashes($string);
    foreach ($string as $key => $val)
        $string[$key] = new_addslashes($val);
    return $string;
}

/**
 * 返回经stripslashes处理过的字符串或数组
 * @param $string 需要处理的字符串或数组
 * @return mixed
 */
function new_stripslashes($string)
{
    if (!is_array($string))
        return stripslashes($string);
    foreach ($string as $key => $val)
        $string[$key] = new_stripslashes($val);
    return $string;
}

/**
 * 安全过滤函数
 *
 * @param $string
 * @return string
 */
function safe_replace($string)
{
    $string = str_replace('%20', '', $string);
    $string = str_replace('%27', '', $string);
    $string = str_replace('%2527', '', $string);
    // $string = str_replace('*','',$string);
    // $string = str_replace('"','&quot;',$string);
    // $string = str_replace("'",'',$string);
    // $string = str_replace('"','',$string);
    $string = str_replace(';', '', $string);
    $string = str_replace('<', '&lt;', $string);
    $string = str_replace('>', '&gt;', $string);
    $string = str_replace("{", '', $string);
    $string = str_replace('}', '', $string);
    $string = str_replace('\\', '', $string);
    return $string;
}

/**
 * xss过滤函数
 *
 * @param $string
 * @return string
 */
function remove_xss($string)
{
    $string = preg_replace('/[\x00-\x08\x0B\x0C\x0E-\x1F\x7F]+/S', '', $string);

    $parm1 = array('javascript', 'vbscript', 'expression', 'applet', 'meta', 'xml', 'blink', 'link', 'script', 'embed', 'object', 'iframe', 'frame', 'frameset', 'ilayer', 'layer', 'bgsound', 'title', 'base');

    $parm2 = array('onabort', 'onactivate', 'onafterprint', 'onafterupdate', 'onbeforeactivate', 'onbeforecopy', 'onbeforecut', 'onbeforedeactivate', 'onbeforeeditfocus', 'onbeforepaste', 'onbeforeprint', 'onbeforeunload', 'onbeforeupdate', 'onblur', 'onbounce', 'oncellchange', 'onchange', 'onclick', 'oncontextmenu', 'oncontrolselect', 'oncopy', 'oncut', 'ondataavailable', 'ondatasetchanged', 'ondatasetcomplete', 'ondblclick', 'ondeactivate', 'ondrag', 'ondragend', 'ondragenter', 'ondragleave', 'ondragover', 'ondragstart', 'ondrop', 'onerror', 'onerrorupdate', 'onfilterchange', 'onfinish', 'onfocus', 'onfocusin', 'onfocusout', 'onhelp', 'onkeydown', 'onkeypress', 'onkeyup', 'onlayoutcomplete', 'onload', 'onlosecapture', 'onmousedown', 'onmouseenter', 'onmouseleave', 'onmousemove', 'onmouseout', 'onmouseover', 'onmouseup', 'onmousewheel', 'onmove', 'onmoveend', 'onmovestart', 'onpaste', 'onpropertychange', 'onreadystatechange', 'onreset', 'onresize', 'onresizeend', 'onresizestart', 'onrowenter', 'onrowexit', 'onrowsdelete', 'onrowsinserted', 'onscroll', 'onselect', 'onselectionchange', 'onselectstart', 'onstart', 'onstop', 'onsubmit', 'onunload');

    $parm = array_merge($parm1, $parm2);

    for ($i = 0; $i < sizeof($parm); $i++) {
        $pattern = '/';
        for ($j = 0; $j < strlen($parm[$i]); $j++) {
            if ($j > 0) {
                $pattern .= '(';
                $pattern .= '(&#[x|X]0([9][a][b]);?)?';
                $pattern .= '|(&#0([9][10][13]);?)?';
                $pattern .= ')?';
            }
            $pattern .= $parm[$i][$j];
        }
        $pattern .= '/i';
        $string = preg_replace($pattern, ' ', $string);
    }
    return $string;
}

//删除空格--待优化
function trimAll($str)
{
    $str = trim($str);
    $str = explode(' ', $str);
    $str = implode('', $str);
    return $str;
}

/**
 * 旋转图片
 * @param type $src 图片完整路径
 * @param type $direction 1顺时针90   2 逆时针90
 */
function imgturn($src, $direction = 2)
{
    $ext = pathinfo($src)['extension'];
    switch ($ext) {
        case 'gif':
            $img = imagecreatefromgif($src);
            break;
        case 'jpg':
        case 'jpeg':
            $img = imagecreatefromjpeg($src);
            break;
        case 'png':
            $img = imagecreatefrompng($src);
            break;
        default:
            throw new Exception('图片格式错误!');
            break;
    }
    $width = imagesx($img);
    $height = imagesy($img);
    $img2 = imagecreate($height, $width);
    //顺时针旋转90度
    if ($direction == 1) {
        for ($x = 0; $x < $width; $x++) {
            for ($y = 0; $y < $height; $y++) {
                imagecopy($img2, $img, $height - 1 - $y, $x, $x, $y, 1, 1);
            }
        }
    } else if ($direction == 2) {
        //逆时针旋转90度
        for ($x = 0; $x < $height; $x++) {
            for ($y = 0; $y < $width; $y++) {
                imagecopy($img2, $img, $x, $y, $width - 1 - $y, $x, 1, 1);
            }
        }
    }
    switch ($ext) {
        case 'jpg':
        case "jpeg":
            imagejpeg($img2, $src, 100);
            break;

        case "gif":
            imagegif($img2, $src, 100);
            break;

        case "png":
            imagepng($img2, $src);
            break;

        default:
            throw new Exception('图片格式错误!');
            break;
    }
    imagedestroy($img);
    imagedestroy($img2);
}

/**
 * PDF2PNG
 * @param $pdf  待处理的PDF文件
 * @param $path 待保存的图片路径
 * @param $page 待导出的页面 -1为全部 0为第一页 1为第二页
 * @return      保存好的图片路径和文件名
 */
function pdf2png($pdf, $path, $page = 0)
{

    file_exists($path) || mkdir($path, 0777);
    if (!extension_loaded('imagick')) {
        echo '没有找到imagick！';
        return false;
    }
    if (!file_exists($pdf)) {
        echo '没有找到pdf';
        return false;
    }
    $im = new Imagick();
    $im->setResolution(120, 120);   //设置图像分辨率
    $im->setCompressionQuality(80); //压缩比
    $im->readImage($pdf . "[" . $page . "]"); //设置读取pdf的第一页
    //$im->thumbnailImage(200, 100, true); // 改变图像的大小
    $im->scaleImage(200, 100, true); //缩放大小图像
    $filename = $path . "/" . time() . '.png';
    if ($im->writeImage($filename) == true) {
        $Return = $filename;
    }
    return $Return;
}

/**
 * 生成密码
 * @param  [type] $password      [密码]
 */
function setPassword($password)
{
    if ($password) {
        $data = array();
        $chars = "0123456789";
        $str = "";
        for ($i = 0; $i < 8; $i++) {
            $str .= substr($chars, mt_rand(1, strlen($chars) - 1), 1);
        }
        $data['strict'] = $str;
        $data['password'] = md5(md5($password) . $data['strict']);
        return $data;
    } else {
        return false;
    }
}

/**
 * 数字金额转换成中文大写金额的函数
 * String Int $num 要转换的小写数字或小写字符串
 * return 大写字母
 * 小数位为两位
 * */
function num_to_rmb($num)
{
    $c1 = "零壹贰叁肆伍陆柒捌玖";
    $c2 = "分角元拾佰仟万拾佰仟亿";
    //精确到分后面就不要了，所以只留两个小数位
    $num = round($num, 2);
    //将数字转化为整数
    $num = $num * 100;
    if (strlen($num) > 10) {
        return "金额太大，请检查";
    }
    $i = 0;
    $c = "";
    while (1) {
        if ($i == 0) {
            //获取最后一位数字
            $n = substr($num, strlen($num) - 1, 1);
        } else {
            $n = $num % 10;
        }
        //每次将最后一位数字转化为中文
        $p1 = substr($c1, 3 * $n, 3);
        $p2 = substr($c2, 3 * $i, 3);
        if ($n != '0' || ($n == '0' && ($p2 == '亿' || $p2 == '万' || $p2 == '元'))) {
            $c = $p1 . $p2 . $c;
        } else {
            $c = $p1 . $c;
        }

        $i = $i + 1;
        //去掉数字最后一位了
        $num = $num / 10;
        $num = (int)$num;
        //结束循环
        if ($num == 0) {
            break;
        }
    }

    $j = 0;
    $slen = strlen($c);
    while ($j < $slen) {
        //utf8一个汉字相当3个字符
        $m = substr($c, $j, 6);
        //处理数字中很多0的情况,每次循环去掉一个汉字“零”
        if ($m == '零元' || $m == '零万' || $m == '零亿' || $m == '零零') {
            $left = substr($c, 0, $j);
            $right = substr($c, $j + 3);
            $c = $left . $right;
            $j = $j - 3;
            $slen = $slen - 3;
        }
        $j = $j + 3;
    }
    //这个是为了去掉类似23.0中最后一个“零”字
    if (substr($c, strlen($c) - 3, 3) == '零') {
        $c = substr($c, 0, strlen($c) - 3);
    }
    //将处理的汉字加上“整”
    if (empty($c)) {
        return "零元整";
    } else {
        return $c . "整";
    }
}

/**
 * 上传文件到七牛
 * @param string $file_name
 * @param string $file_path
 * @return mixed
 * @throws Exception
 * @date 2019/1/10 12:02
 * @author ligang
 */
function uploadToQiNiu(string $file_name, string $file_path)
{
    $accessKey = get_instance()->config->get('qiniu.accessKey');
    $secrectKey = get_instance()->config->get('qiniu.secrectKey');
    $bucket = get_instance()->config->get('qiniu.bucket');
    $url = get_instance()->config->get('qiniu.qiniu_url');

    $auth = new \Qiniu\Auth($accessKey, $secrectKey);
    // 生成上传 Token
    $token = $auth->uploadToken($bucket);

    $uploadMgr = new \Qiniu\Storage\UploadManager();
    $key = $file_name; //上传后文件名称
    list($ret, $err) = $uploadMgr->putFile($token, $key, $file_path);
    if ($err !== null) {
        return false;
    } else {
        return true;
    }
}

/**
 * 字符串替换
 * @param string $string 被替换的字符
 * @param string $re_str 替换的字符
 * @param bool $rule 规则（true：等长度替换，false：不等长度替换）
 * @param int $length 长度（首位长度）
 * @param int $end_len 末尾长度
 * @return string
 * @date 2018/4/23 11:02
 * @author ligang
 */
function stringReplace(string $string, string $re_str = '*', bool $rule = true, int $length = 4, int $end_len = 0)
{
    if (empty($string)) {
        return false;
    }
    $end_len = $end_len == 0 ? $length : $end_len;
    $str_len = strlen($string);
    if ($rule) {
        $tmp = '';
        for ($i = 1; $i <= $str_len - $length * 2; $i++) {
            $tmp .= $re_str;
        }
        return substr($string, 0, $length) . $tmp . substr($string, -$end_len);
    } else {
        $tmp = '';
        for ($i = 1; $i <= $length; $i++) {
            $tmp .= $re_str;
        }
        return substr($string, 0, $length) . $tmp . substr($string, -$end_len);
    }
}

/**
 * 路径拼接
 * @param String $url
 * @param int $type 图片类型 1用户头像 2其他图片 3视频
 */
function UploadImgPath($url, $type = '')
{
    //头像为空时处理
    if (empty($url)) {
        switch ($type) {
            case 1:
                $url = get_instance()->config->get('qiniu.qiniu_url') . 'default/default_headimg.png';
                break;
            default:
                $url = "";
                break;
        }
        return $url;
    }
    $str = "http://";
    $strs = "https://";
    if (strpos($url, $str) === false && strpos($url, $strs) === false) {
        if($type==3){//视频
            $url = get_instance()->config->get('qiniu.qiniu_url') . $url;
        }else{
            $url = get_instance()->config->get('qiniu.qiniu_url') . $url . '?' . get_instance()->config->get('qiniu.picture_style_one');
        }

        //$url = get_instance()->config->get('qiniu.qiniu_url') . $url;
    }
    return $url;
}

//根据身份证获取信息
function getIdCardInfo($idcard_number)
{
    //判断合法性
    if (empty($idcard_number) || !FunIsSFZ($idcard_number)) {
        return '';
    }
    //获取性别
    $s = substr($idcard_number, -2, 1);
    $data['sex'] = $s % 2 ? 1 : 2;
    $data['birth'] = strlen($idcard_number) == 15 ? ('19' . substr($idcard_number, 6, 6)) : substr($idcard_number, 6, 8);
    $data['birth_time'] = strtotime($data['birth']);
    return $data;
}

//方法返回结果集
function returnResult($code, $msg, $data = [], $count = -1, $allPage = -1, $page = 1, $pageSize = -1)
{
    return ['code' => $code, 'msg' => $msg, 'data' => $data, 'count' => $count, 'allPage' => $allPage, 'page' => $page, 'pageSize' => $pageSize];
}

function getTokenUserInfo($token)
{
    if (empty($token)) {
        return false;
    }

    try {
        $key = get_instance()->config->get('token_key');
        $user_info = \Firebase\JWT\JWT::decode($token, $key, array('HS256'));
        if (empty($user_info)) {
            return false;
        }
        if ($user_info->expire < time()) {
            return false;
        }
        if (empty($user_info->tel ?? '')) {
            return false;
        }
        //保存用户信息
        return (array)$user_info;
    } catch (\Exception $ex) {
        return false;
    }

}

//获取当前月份的前一月
function getMonth($sign)
{
    //得到系统的年月
    $tmp_date = date("Ym");
    //切割出年份
    $tmp_year = substr($tmp_date, 0, 4);
    //切割出月份
    $tmp_mon = substr($tmp_date, 4, 2);
    // 得到当前月份的下几月
    $tmp_nextmonth = mktime(0, 0, 0, $tmp_mon + $sign, 1, $tmp_year);
    // 得到当前月份的前几月
    $tmp_forwardmonth = mktime(0, 0, 0, $tmp_mon - $sign, 1, $tmp_year);
    return $fm_next_month = date("m", $tmp_forwardmonth);
}

function getTheMonth($date)
{
    $firstday = date('Y-m-01', strtotime($date));
    $lastday = date('Y-m-d', strtotime("$firstday +1 month -1 day"));
    return array($firstday, $lastday);
}

/**
 * 验证真实姓名只能中文
 *
 * @param INT $mobile
 * @return bool
 * @author
 *
 */
function isRealName($name)
{
    return preg_match('/^[\x{4e00}-\x{9fa5}]+$/u', $name) ? true : false;
}


/*
 *匹配汉字、字母、数字、下划线、破折号
 */
function isChsDash($word)
{
    return preg_match('/^[\x{4e00}-\x{9fa5}a-zA-Z0-9\_\-]+$/u', $word) ? true : false;

}

/*
 * 身份证校验
 * @ligang
 * @return  bool
 */

function checkIdCard($idCard)
{
    $result = preg_match('/^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$|^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X|x)$/', $idCard);
    if ($result) {
        return true;
    } else {
        return false;
    }
}


/*
 * 和当前时间比较(只比较年月)
 */

function compareTime($time)
{
    $now = strtotime(date('Y-m'));
    $expire = strtotime(date('Y-m', $time));
    if ($now > $expire) {
        return true;
    } else {
        return false;
    }
}

function downloadOriginFile($url, $save_dir = '', $filename = '', $type = 0)
{
    if (trim($url) == '') {
        return false;
    }
    if (trim($save_dir) == '') {
        $save_dir = './';
    }
    if (0 !== strrpos($save_dir, '/')) {
        $save_dir .= '/';
    }
    //创建保存目录
    if (!file_exists($save_dir) && !mkdir($save_dir, 0777, true)) {
        return false;
    }
    //获取远程文件所采用的方法
    if ($type) {
        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $content = curl_exec($ch);
        curl_close($ch);
    } else {
        ob_start();
        readfile($url);
        $content = ob_get_contents();
        ob_end_clean();
    }
    //echo $content;
    $size = strlen($content);
    //文件大小
    $fp2 = @fopen($save_dir . $filename, 'a');
    fwrite($fp2, $content);
    fclose($fp2);
    unset($content, $url);
    return array(
        'file_name' => $filename,
        'save_path' => $save_dir . $filename,
        'file_size' => $size
    );
}

//判断一个值是否在二维数组里面
function deep_in_array($value, $array)
{
    foreach ($array as $item) {
        if (!is_array($item)) {
            if ($item == $value) {
                return true;
            } else {
                continue;
            }
        }

        if (in_array($value, $item)) {
            return true;
        } else if (deep_in_array($value, $item)) {
            return true;
        }
    }
    return false;
}

//将银行卡加密成*，只显示后四位
function passwordBank($string)
{
    $len = strlen($string);
    if ($len <= 4) {
        return $string;
    }

    $str = str_repeat('*', $len - 4) . substr($string, -4);

    return $str;
}

//只显示银行卡后四位
function substrBank($string)
{
    $len = strlen($string);
    if ($len <= 4) {
        return $string;
    }

    $str = substr($string, -4);

    return $str;

}

/**
 * 获取某时间所在月份最后一天
 * @param $time 时间戳
 */
function getMonthLastDay($time)
{
    if (empty($time)) {
        return false;
    }
    $begin_date = date('Y-m-01', $time);
    $last_date = date('Y-m-d', strtotime("$begin_date +1 month -1 day")) . ' 23:59:59';
    return $last_date;
}


//验证银行卡 16-19位
function checkBankNo($bank_no)
{
    if (preg_match('/^([1-9]{1})(\d{15,18})$/', $bank_no) > 0) {
        return true;
    }

    return false;
}

/**
 * 生成券码  优惠券码以C开头 提货码以D开头 邀请好友以I开头
 * @param $code_length 券码长度
 * @param $prefix 前缀
 */
function generate_promotion_code($code_length = 6, $prefix = 'C')
{
    $characters = "123456789ABCDEFGHIJKLMNPQRSTUVWXYZabcdefghijklmnpqrstuvwxyz";
    $code = "";
    for ($i = 0; $i < $code_length - 1; $i++) {
        $code .= $characters[mt_rand(0, strlen($characters) - 1)];
    }
    $code = $prefix . $code;

    return $code;

}

/*
 *  随机生成系统账号
 *  @author ligang
 *  @return int
 */

function randAccountNo()
{
    $str = '';
    for ($i = 1; $i <= 8; $i++) {
        $str .= mt_rand(0, 9);
    }
    return $str;
}

/* 数据按日期分组 */
function groupData($data, $field)
{
    $curyear = date('Y');
    $visit_list = [];
    foreach ($data as &$v) {
        if ($curyear == date('Y', $v[$field])) {
            $date = date('m月d日', $v[$field]);
        } else {
            $date = date('Y年m月d日', $v[$field]);
        }
        $v['date'] = $date;
        $visit_list[] = $v;
    }
    return $visit_list;

}

//根据key 给二维数组分组
function array_group_by($arr, $key)
{
    $grouped = [];
    foreach ($arr as $value) {
        $grouped[$value[$key]][] = $value;
    }
// Recursively build a nested grouping if more parameters are supplied
// Each grouped array value is grouped according to the next sequential key
    if (func_num_args() > 2) {
        $args = func_get_args();
        foreach ($grouped as $key => $value) {
            $parms = array_merge([$value], array_slice($args, 2, func_num_args()));
            $grouped[$key] = call_user_func_array('array_group_by', $parms);
        }
    }
    return $grouped;
}


/**
 * 计算兑换手续费,手续费从兑换金额中从扣取
 * @param $param
 * @return array
 */
function serviceFee($param)
{
    var_dump($param);
    $balance = floatval($param['balance']);//账户余额
    $money = floatval($param['money']);//兑换金额
    $min_money = floatval($param['min_money'] ?? 0);//最低兑换金额
    $max_money = floatval($param['max_money'] ?? 0);//最大兑换金额
    $min_fee = floatval($param['min_fee'] ?? 0);//最低手续费
    $max_fee = floatval($param['max_fee'] ?? 0);//最高手续费
    $fee_rate = floatval($param['fee_rate'] ?? 0);//费率,小数
    $account_type = $param['account_type'] ?? 1;//账户类型1:银行卡 2:微信
    if (empty($money) || $money < 0) {
        return returnResult(ReturnCodeService::FAIL, "请输入兑换金额");
    }
    $reg = '/^([1-9]\d*|0)(\.\d{1,3})?$/';
    if (!preg_match($reg, $money)) {
        return returnResult(ReturnCodeService::FAIL, '请输入正确的兑换金额');
    }
    if (empty($balance)) {
        return returnResult(ReturnCodeService::FAIL, "余额不足");
    }
    //判断兑换额度
    if ($money < $min_money) {
        return returnResult(ReturnCodeService::FAIL, '兑换的最低额度为：' . $min_money);
    }
    if ($max_money > 0 && $money > $max_money) {
        return returnResult(ReturnCodeService::FAIL, '兑换的最高额度为：' . $max_money);
    }
    //计算手续费
    $fee = 0;//非银行卡不收手续费
    if ($account_type == 1) {
        $fee = $money * $fee_rate;
        if ($fee_rate > 0 && $fee < $min_fee) {
            $fee = $min_fee;
        } else if ($fee_rate > 0 && $fee > $max_fee) {
            $fee = $max_fee;
        }
    }

    //两种情况影响以下两个值1:手续费从兑换金额中扣，2:手续费额外扣
    $deduct_money = $money;
    $remit_money = $money - $fee;
    if ($balance < $deduct_money) {
        return returnResult(ReturnCodeService::FAIL, "余额不足");
    }
    $return['fee'] = formatMoney($fee, 1);
    $return['money'] = formatMoney($money, 1);
    $return['deduct_money'] = formatMoney($deduct_money, 1);
    $return['remit_money'] = formatMoney($remit_money, 1);
    return returnResult(ReturnCodeService::SUCCESS, '计算成功', $return);

}

/*
 * 生成二维码编号
 */
function createQrcodeNo($prefix = '')
{
    if (!empty($prefix)) {
        $qrcode_no = $prefix . mt_rand(100000, 999999);
    } else {
        $qrcode_no = mt_rand(10000000, 99999999);
    }
    return $qrcode_no;

}

//生成缴费单号
function createFeeSn($prefix = '')
{
    $fee_order_model = get_instance()->loader->model('FeeOrderModel', get_instance());
    do {
        $str = $prefix . date('Ymd') . substr(implode(NULL, array_map('ord', str_split(substr(uniqid(), 7, 13), 1))), 0, 8);
        $vo = $fee_order_model->getOne(['order_sn' => $str], 'id');
        if (empty($vo)) {
            break;
        }
    } while (true);
    return $str;

}


//验证各种数据类型方法
function pregMatchInput($string, $type = '')
{
    $string = trim($string);
    if (empty($string))
        return false;
    switch ($type) {
        case '':
            break;
        case 'name':
            $match = "/^[\x{4e00}-\x{9fa5}a-zA-Z0-9\_\-]+$/u";//名称验证,只能是汉字或字母或数字或下划线
            break;
        case 'account':
            $match = "/^[a-zA-Z0-9_@-ÿ][a-zA-Z0-9_@-ÿ]+$/";//账号验证,两位以上的字母,数字,或者下划线
            break;
        case 'password':
            $match = '/^[0-9a-z_$]{6,16}$/i';//密码验证 6-16位的数字字母下划线组成
            break;
        case 'mobile_phone':
            $match = '#^1[3-9][\d]{9}$#';//手机号格式验证
            break;
        case 'phone':
            $match = '/((\d{11})|^((\d{7,8})|(\d{4}|\d{3})-(\d{7,8})|(\d{4}|\d{3})-(\d{7,8})-(\d{4}|\d{3}|\d{2}|\d{1})|(\d{7,8})-(\d{4}|\d{3}|\d{2}|\d{1}))$)/';//电话验证包括座机
            break;
        case 'id_card':
            $match = '/^(\d{15}$|^\d{18}$|^\d{17}(\d|X|x))$/';//身份证号格式验证
            break;
        case 'email':
            $match = '/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/';//邮箱格式验证
            break;
        case 'device_no':
            $match = "/^[0-9a-z]{2,36}$/i";//主板编号验证,两位以上的字母,数字
            break;
        case 'float':
            $match = '/^\d+(\.\d+)?$/';//非负浮点数验证 可以验证所有的正整数和正小数 包括0
            break;
        case 'bank_no':
            $match = '/^([1-9]{1})(\d{14}|\d{18})$/';//银行卡号
            break;
        case 'price':
            $match = '/(?!^0*(\.0{1,2})?$)^\d{1,13}(\.\d{1,2})?$/';//价格
            break;
    }
    return preg_match($match, $string) ? true : false;
}

/**
 * 时间 H:i:s 转 s
 * @User yaokai
 * @param $his
 * @return float|int
 */
function HisToS($his)
{
    $str = explode(':', $his);

    $len = count($str);

    if ($len == 3) {
        $time = $str[0] * 3600 + $str[1] * 60 + $str[2];
    } elseif ($len == 2) {
        $time = $str[0] * 60 + $str[1];
    } elseif ($len == 1) {
        $time = $str[0];
    } else {
        $time = 0;
    }
    return $time;
}

/**
 * 时间 s 转 H:i:s
 * @User yaokai
 * @param $seconds
 * @return string
 */
function SToHis($seconds)
{
    $seconds = (int)$seconds;
    $time = '';
    if ($seconds > 3600) {
        if ($seconds > 86400) {
            $days = (int)($seconds / 86400);
            $seconds = $seconds % 86400;//取余
            $time .= $days . " 天 ";
        }
        $hours = intval($seconds / 3600);
        $minutes = $seconds % 3600;//取余下秒数
        $time .= $hours . " 小时 " . gmstrftime('%M 分钟 %S 秒', $minutes);
    } elseif ($seconds > 60) {
        $time = gmstrftime('%M 分钟 %S 秒', $seconds);
    } else {
        $time = gmstrftime('%S 秒', $seconds);
    }
    return $time;
}

/**
 * 计算秒数
 * @User yaokai
 * @param $seconds
 * @param $type
 * @return string
 */
function HiToS($num, $type)
{
    $num = (int)$num;
    if ($type == 1) {
        //小时->秒
        $seconds = $num * 3600;
    } else {
        //分->秒
        $seconds = $num * 60;
    }
    return $seconds;
}

/**
 * 计算小时/分钟
 * @User yaokai
 * @param $seconds
 * @param $type
 * @return string
 */
function SToHi($seconds, $type = 1)
{
    $seconds = (int)$seconds;
    if ($type == 1) {
        //秒->小时
        $num = round($seconds / 3600, 2);
    } else {
        //秒->分钟
        $num = round($seconds / 60, 2);
    }
    return $num;
}

/**
 * 计算服务费开始支付日期
 * @param $time
 * @return false|string
 */
function paymentStartDate($time)
{
    //如是1-15号则在本月16号开始，若是15号后则从次月1号开始生效
    $day = date('d', $time);
    if ($day < 16) {
        $date = date('Y年m月16日', $time);
    } else {
        $firstday = date("Y-m-01", $time);
        $date = date("Y-m-01", strtotime("$firstday +1 month"));
    }
    return $date;
}


